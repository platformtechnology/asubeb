<div class="row">
    <div class="span3">

        <?= form_open(site_url('web/pages/checkresult'), 'class="form-horizontal"'); ?>
        <table cellpadding="5">
            <tr>
                <td colspan="2">
                    <strong><h3>Check Result</h3></strong>
                    <hr/>
                    <?php if ($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                </td>
            </tr>
            <tr>
                <td >
                    <?php
                    $select_options = array();
                    foreach ($exams as $exam) {
                        $select_options[$exam->examid] = $exam->examname;
                    }
                    $select_options[''] = "------ Exam ------";
                    echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'style="background-color: #f7f1f1;" required="required"');
                    ?>
                    <br/>
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    $already_selected_value = $this->input->post('examyear') ? $this->input->post('examyear') : $activeyear;
                    $start_year = $startyear;
                    print '<select name="examyear" style="background-color: #f7f1f1;" required="required">';
                    print '<option value="" ' . ($already_selected_value == '' ? 'selected="selected"' : '') . '>--- Year ---</option>';
                    for ($x = $start_year; $x <= date('Y'); $x++) {
                        print '<option value="' . $x . '"' . ($x == $already_selected_value ? ' selected="selected"' : '') . '>' . $x . '</option>';
                    }
                    print '</select>';
                    ?>
                    <br/>

                </td>
            </tr>
            <tr>
                <td>
                    <input name="examno" placeholder="Examination Number" value="<?= set_value('examno') ?>" style="background-color: #f7f1f1;" type="text" required="required" />
                    <br/>
                </td>
            </tr>
            <tr>
                <td>
                    <input name="pin" placeholder="Card PIN" style="background-color: #f7f1f1;" type="password" required="required" />
                </td>
            </tr>
            <tr>
                <td>
                    
                    <button type="submit" class="btn btn-primary"><i class="icon-upload icon-white"></i> Check</button>
                    
                </td>
            </tr>
        </table>
        <br/>
        <a href="<?= str_replace('http://', 'http://oldresults.', $current_url) ?>" target="_blank">Check 2013 - 2014 results</a>
        <?= form_close(); ?>

        <br/>
        <!--                 NEWS SECTION-->
        <div class="accordion" id="accordion2">
            <strong><h4>Current News</h4></strong>
            <hr/>
            <?php
            if (count($edc_news)):
                $sn = 0;
                $id = 'collapse' . $sn;
                foreach ($edc_news as $news) {
                    ?>
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?= $id; ?>">
                                <?= $news->newstitle; ?>
                            </a>
                        </div>
                        <div id="<?= $id; ?>" class="accordion-body collapse" style="height: 0px;">
                            <div class="accordion-inner">
                                <?= $news->news; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    ++$sn;
                    $id = 'collapse' . $sn;
                }
            endif;
            ?>
            <br/>
            <a href="<?= site_url('news') ?>"> &nbsp;All News ...</a>
        </div>		
        

        <div class="thumbnail">
            <a href="http://www.goalsplatform.com/" target="_blank"><img src="<?= get_img('goalplatform.jpg', $web = true); ?>" title="Goals Platform" /></a>
            <div class="caption">
                <h5>Goals Platform</h5>
                <p style="text-align: justify;"> 
                    Goals Platform is an online Non-Governmental professional driven Organisation aimed at helping youths achieve their dreams by providing Professional mentorship, Job Opportunities, Scholarships, Grants and other SME facilities.
                    <a href="http://www.goalsplatform.com/" target="_blank">Register Today</a>
                </p>
            </div>
        </div>
    </div>

    <div class="span9">
        <div id="myCarousel" class="carousel slide homCar" >
            <div class="thumbnail">
                <div class="carousel-inner">
                    <?php
                    if (count($sliders)):
                        $sn = 0;
                        foreach ($sliders as $slider) {
                            ?>
                            <div class="item <?= $sn == 0 ? 'active' : ''?>">
                                <img src="<?= base_url('resources/web/sliders/'.$slider) ?>" height="289px" width="700px" alt="#"/>
                            </div>
                            <?php
                            $sn++;
                        }
                    endif;
                    ?>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"></a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next"></a>
        </div>
        <div class="well">
            <!--            <h2>Welcome</h2>
                            <br/>	-->
            <?= $edc_info->homeinfo; ?>
        </div>
    </div>
</div>