

    <div class="row" id="printdiv">
            <div class="span12">
                <div class="page-header">
                    <h3 style="text-align: center; color: #0077b3;">
                        
                        REGISTRATION FOR <?= strtoupper($exam_detail->examdesc . ' (' . $exam_detail->examname . ')') ;?> 
                    </h3>
                </div>
            </div>
    
        <p style="text-align: center;">
        <h4 style="text-align: center;">
            Dear Applicant, 
            <br/>
            Registration for the selected Exam is <strong>Closed.</strong> 
            <br/>
            The deadline was <span style="color: crimson;"><?=$closuredata->closedate; ?></span>

            <br/> <br/>
            
            <a href="<?=  site_url('registration');?>" class="btn btn-warning"><i class="icon-share icon-white"></i> Back</a> 
        </h4>
        </p>

    </div>



 