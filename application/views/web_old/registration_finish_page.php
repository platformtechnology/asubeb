
<?php $closedata = $this->registration_model->getCloseDate_Id($exam_detail->examid); ?>
    <div class="row" id="printdiv">
            <div class="span12">
                <div class="page-header">
                    <h3 style="text-align: center; color: #0077b3;">
                        <a href="#" title="Examination Development Centre"><img src="<?=$edclogo;?>" alt="Logo"/></a><br/>
                        REGISTRATION FOR <br/>THE <?= strtoupper($exam_detail->examdesc. ' ' . $candidate_info->examyear) ;?> 
                    </h3>
                </div>
            </div>
    
        <p style="text-align: center;">
        <h5>
            Dear <?= $candidatename; ?>, Your Registration has been successfully completed!
            <br/>
            Your Examination Number is: <span style="color: crimson;"><?= $candidate_info->examno; ?></span><br/>
            You can <a href="<?= site_url('web/login/logout');?>">Login</a> anytime on/before <?= count($closedata) ? $closedata->closedate : '[close date]';?> to Edit your Registration Info.<br/><br/>

            Thank You!

            <br/> <br/>
            <button type="button" onclick="printDiv('printdiv')" class="btn btn-info"><i class="icon-print icon-white"></i> Print</button>
            <a href="<?=  site_url('web/login/logout');?>" class="btn btn-warning"><i class="icon-share icon-white"></i> Complete</a> 
        </h5>
        </p>

    </div>



 