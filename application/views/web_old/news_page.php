<div class="row">
	<div class="span3">
            <div class="thumbnail">
              <a href="http://www.goalsplatform.com/" target="_blank"><img src="<?= get_img('goalplatform.jpg', $isweb = true); ?>" title="free business template"  alt="free business template"/></a>
                <div class="caption">
                  <h5>Goals Platform</h5>
                  <p style="text-align: justify;"> 
                        Goals Platform is an online Non-Governmental professional driven Organisation aimed at helping youths achieve their dreams by providing Professional mentorship, Job Opportunities, Scholarships, Grants and other SME facilities.
                        <a href="http://www.goalsplatform.com/" target="_blank">Register Today</a>
                  </p>
                </div>
            </div>
	</div>
	
	<div class="span9">
            <ul class="breadcrumb">
                <li><a href="<?=  site_url('home');?>">Home</a> <span class="divider">/</span></li>
			<li class="active">News</li>
            </ul>
            <div class="caption">
                <h2>News</h2>
                
                <table class="table table-bordered">
                    <?php 
                        if(count($edc_news)):
                            foreach ($edc_news as $news) {
                    ?>
                    <tr>
                        <td>
                            <strong style="color:crimson;"><?=$news->newstitle;?></strong><br/><br/>
                             <?=$news->news;?>
                        </td>
                    </tr>
                    <?php
                            }
                        endif;
                    ?>
                </table>      
        </div>
	</div>
</div>