<div class="row">
    <div class="span3">

        <?= form_open('', 'class="form-horizontal"'); ?>
        <table cellpadding="5">
            <tr>
                <td>
                    <strong><h3>Exam Registration</h3></strong>
                    <hr/>
                    <?php if ($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?php if (strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                </td>
            </tr>

            <tr>
                <td >
                    <?php
                    $select_options = array();
                    foreach ($exams as $exam) {
                        $select_options[$exam->examid] = $exam->examname;
                    }
                    $select_options[''] = " ---- Exam ----";
                    echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'style="background-color: #f7f1f1;" required="required"');
                    ?>
                    <br/>
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    $already_selected_value = $this->input->post('examyear') ? $this->input->post('examyear') : $activeyear;
                    $start_year = $activeyear;
                    print '<select name="examyear" style="background-color: #f7f1f1;">';
                    print '<option value="" ' . ($already_selected_value == '' ? 'selected="selected"' : '') . '>Select Exam Year</option>';
                    for ($x = $start_year; $x <= $activeyear; $x++) {
                        print '<option value="' . $x . '"' . ($x == $already_selected_value ? ' selected="selected"' : '') . '>' . $x . '</option>';
                    }
                    print '</select>';
                    ?>
                    <br/>

                </td>
            </tr>
            <tr>
                <td>
                    <input type="password"  placeholder="Card PIN" name="cardpin" value="<?php echo set_value('cardpin'); ?>" style="background-color: #f7f1f1;" required="required" />
                </td>
            </tr>
            <tr>
                <td>
                    <input type="password"  placeholder="Form Number" name="formnumber" value="<?php echo set_value('formnumber'); ?>" style="background-color: #f7f1f1;" required="required" />
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit" class="btn btn-primary"><i class="icon-arrow-right icon-white"></i> Proceed</button>
                </td>
            </tr>

        </table>
        <?= form_close(); ?>

    </div>

    <div class="span9">
        <div id="myCarousel" class="carousel slide homCar" >
            <div class="thumbnail">
                <div class="carousel-inner">
                    <?php
                    if (count($sliders)):
                        $sn = 0;
                        foreach ($sliders as $slider) {
                            ?>
                            <div class="item <?= $sn == 0 ? 'active' : '' ?>">
                                <img src="<?= base_url('resources/web/sliders/' . $slider) ?>" height="289px" width="700px" alt="#"/>
                            </div>
                            <?php
                            $sn++;
                        }
                    endif;
                    ?>	 
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"></a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next"></a>
        </div>
    </div>

    <!--<div class="span12">
                    <div class="page-header">
                        <h3 style="text-align: center; color: #0077b3;">
                            SITE MAINTENANCE IN PROGRESS
                        </h3>
                    </div>
                </div>
        
            <p style="text-align: center;">
            <h4 style="text-align: center;">
                Dear Applicant, 
                <br/>
                This site is currently undergoing Maintenance processes and will be back online shortly.
                <br/>
                Kindly bear with us and apologies for any inconveniences. 
    
                <br/> <br/>
                
                <a href="http://edcabiastate.com" class="btn btn-warning"><i class="icon-share icon-white"></i> Back</a> 
            </h4>
            </p>-->
</div>