
    <div class="container"></div>
        <?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>
       <div class="page-content">
           <div class="row">
               <div class="container">
                   <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
               </div>
               <div class="container" id="printdiv">

                   <?php

                                if(count($candidate)):

                                    $passport = './passports/'.$candidate->candidateid.'.jpg';
                                    if(!file_exists($passport)) $passport = get_img('no_image.jpg');
                                    else $passport = base_url('passports/'.$candidate->candidateid.'.jpg');
                   ?>

                                   <div class="row">
                                        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                                             The Ministry Of Education<br/>
                                             <img src="<?= $edclogo; ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                                            <br/>
                                             <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>

                                        </h4>

                                        <br/>
                                        <div class="span2"></div>
                                        <div class="span8" style="background-color: white; padding: 20px; border-radius: 10px; border: 1px">

                                                <strong>CANDIDATE EXAMINATION REPORT</strong>
                                                 <hr/>

                                            <table class="table-condensed">

                                                    <tr>
                                                        <td colspan="2" align="center">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>EXAMNO</strong></td>
                                                        <td><?= $candidate->examno ?></td>
                                                        <td rowspan="5">
                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                <div class="fileupload-new thumbnail" style="width: 110px; height: 110px;"><img src="<?= $passport; ?>" width="100px" height="120px" alt="Passport" /></div>
                                                                 <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                              </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>CANDIDATE NAME</strong></td>
                                                        <td><?= strtoupper($candidate->firstname.' '.$candidate->othernames); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>EXAMINATION TYPE</strong></td>
                                                        <td><strong><?= strtoupper($examdetail->examdesc. ' (' . $examdetail->examname .') '. $examyear); ?></strong></td>
                                                    </tr>

                                                    <tr>
                                                        <td><strong>SCHOOL</strong></td>
                                                        <td><?= strtoupper($this->registration_model->get_school($candidate->schoolid))?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"><br/><strong>SUBJECT GRADES<hr/></strong></td>
                                                    </tr>
                                                     <tr>
                                                         <td><strong>SUBJECTS</strong></td>
                                                         <td><strong><?= $examdetail->hasposting ? 'SCORES' : 'GRADES'; ?></strong></td>
                                                    </tr>
                                                     <?php

                                                     $total = 0; $resultReleased = false;
                                                         if(count($allsubjects)):

                                                                foreach ($allsubjects as $subjs):
                                                                    $score_data = $this->report_model->get_score_per_subject($candidate->candidateid, $subjs->subjectid, $examid, $examyear, true);
                                                                    if ($score_data)
                                                                        $total += $score_data->total_score;
                                                                endforeach;
                                                                 if($total > 0) $resultReleased = true;
                                                         endif;

                                                     $compulsorySubjects = $this->report_model->getCompulsorySubjectsToPass($examid,$examyear);
                                                        $total = 0;

                                                        if($resultReleased):
                                                            if($examdetail->hasposting):
                                                                if(count($allsubjects)):

                                                                    foreach ($allsubjects as $subjs):
                                                                        $score_data = $this->report_model->get_score_per_subject($candidate->candidateid, $subjs->subjectid, $examid, $examyear, true);
                                                                        $total += $score_data->total_score;

                                                                        $score = $score_data->total_score;
                                                                        if((intval($score) == 0) && (intval($score_data->exam_score) < 0)) $score = '<span style="color: crimson;">ABS</span>';
                                                         ?>
                                                                        <tr>
                                                                            <td><strong><?= ($subjs->iscompulsory ? '*': '') .strtoupper($subjs->subjectname); ?></strong></td>
                                                                            <td><strong><?= $score ?></strong></td>
                                                                       </tr>
                                                         <?php
                                                                    endforeach;
                                                                endif;

                                                            else:
                                                                $registered_subjects = $this->reg_subjects_model->get_registered_subjects($candidate->candidateid, $examid, $examyear);
                                                                if(count($allsubjects)):
                                                                    foreach ($allsubjects as $subjs):
                                                          ?>
                                                                         <tr>
                                                                            <td><strong><?= strtoupper($subjs->subjectname); ?></strong></td>
                                                                            <?php
                                                                            if(count($registered_subjects)){
                                                                                foreach ($registered_subjects as $reg_subject) {
                                                                                   $found = false;
                                                                                   if($reg_subject->subjectid == $subjs->subjectid){
                                                                                       $remark_detail = $this->report_model->get_subject_remark_per_candidate($subjs->subjectid, $candidate->candidateid, $examid, $examyear);
                                                                                        ?>
                                                                                            <td><strong>
                                                                                        <?php
                                                                                        if (in_array($reg_subject->subjectid, $compulsorySubjects) && intval($remark_detail->exam_score) < 0 ) {
                                                                                           echo 'NES (TO RESIT)';
                                                                                        }
                                                                                        elseif (intval($remark_detail->exam_score) < 0)   { echo 'NES'; }
                                                                                        elseif (count($remark_detail)){ echo strtoupper($remark_detail->grade);}
                                                                                        else {echo '---';}
                                                                                        ?>
                                                                                        </strong></td>
                                                                                        <?php
                                                                                      // echo '<td><strong>'. (intval($remark_detail->exam_score) < 0 ? 'NES' : (count($remark_detail) ? strtoupper($remark_detail->grade) : '---') ) .'</strong></td>';

                                                                                       $found = true;
                                                                                       break;
                                                                                   }
                                                                               }

                                                                               if(!$found) echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';

                                                                            }else echo '<td title="" style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                                                            ?>
                                                                       </tr>
                                                          <?php
                                                                    endforeach;
                                                                endif;
                                                            endif;
                                                      ?>
                                                   <tr>
                                                        <td colspan="2" align="center">&nbsp;</td>
                                                    </tr>
                                                     <?php if($examdetail->hasposting): ?>
                                                        <tr>
                                                            <td><strong>TOTAL</strong></td>
                                                            <td><?= $total; ?></td>
                                                        </tr>
                                                    <?php endif;
                                                        $status = $this->report_model->get_passcount_basedon_criteria_per_candidate($candidate->candidateid, $examid, $examyear, $cutoff);
                                                    ?>
                                                    <?php if($examdetail->hasposting && $status == 'PASSED'):
                                                    //echo $candidate->placement;
                                                            $postedto = $this->registration_model->get_school($candidate->placement)
                                                      ?>
                                                        <tr>
                                                            <td><strong>SCHOOL POSTED</strong></td>
                                                            <td><strong><?= $postedto == '' ? 'Not Posted' : $postedto; ?></strong></td>
                                                        </tr>
                                                    <?php endif; ?>
                                                    <?php if($examdetail->hasposting){?>
                                                    <tr>
                                                        <td><strong>STATUS</strong></td>
                                                        <td><strong><?php echo $status =='PASSED' ? $status : 'FAILED'; ?></strong></td>
                                                        <td align="left"></td>
                                                    </tr>
                                                    <?php } else {?>
                                                    <tr>
                                                        <td><strong>STATUS</strong></td>
                                                        <td><strong><?php echo $status =='PASSED' ? $status : 'INCOMPLETE RESULT'; ?></strong></td>
                                                        <td align="left"></td>
                                                    </tr>
                                                    <?php } ?>

                                                    <tr>

                                                        <td align = "center" colspan = '3'> <img src="<?= base_url(str_replace('./', '', config_item('edc_logo_path')).$this->edcid.'_stamp.png') ?>" alt="Stamp" style="width: 150px; height: 120px" /> </td>
                                                    </tr>


                                                 <?php
                                                    else:

                                                        echo '<tr>
                                                                <td colspan="3">
                                                                <br/><br/>
                                                                    <strong style="color: crimson;">AWAITING RESULT - CHECK BACK LATER</strong>
                                                                    <hr/>
                                                                </td>
                                                              </tr>
                                                            ';

                                                    endif;

                                                  ?>
                                            </table>


                                        </div>

                                   </div>

                   <?php

                                endif;

                   ?>
                </div>
				<p style="page-break-after: always;"></p>
            </div>
        </div>

    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

   </body>
</html>
