
    <div class="container"></div>
        <?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>
       <div class="page-content">
           <div class="row">
               <div class="container">
                   <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
               </div>
               <div class="container" id="printdiv">
                   
                   <?php
                          
                                if(count($candidate)):
                                  
                                //GET ALL SUBJECTS THE CANDIDATE TOOK
                                    $subjectids = array();
                                    foreach($allsubjects as $subject) {
                                      $subjectids[] = $subject->subjectid; 
                                    }
                                
                                    
                                    $candidateids = $candidate->candidateid;
                                    $examid = $candidate->examid;
                                    $examyear = $candidate->examyear;
                                //$this->load->model('remark_model');
                                $remark = $this->report_model->get_subject_remark_per_candidate($subjectids, $candidateids, $examid, $examyear); 
                              
                                if(count($remark)) {
                                    $gradeObtained = $remark->total_score <= 0 ? 'NES' : $remark->grade; 
                                }
                                
                               
                         
                              
                                    $passport = './passports/'.$candidate->candidateid.'.jpg';
                                    if(!file_exists($passport)) $passport = get_img('no_image.jpg');
                                    else $passport = base_url('passports/'.$candidate->candidateid.'.jpg');
                   ?>
                   
                                   <div class="row">
                                        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                                             The Ministry of Education<br/>
                                             <img src="<?= $edclogo; ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                                            <br/>
                                             <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>

                                        </h4>

                                        <br/>
                                        <div class="span2"></div>
                                        <div class="span10" style="background-color: white; padding: 20px; border-radius: 10px; border: 1px">

                                                <strong>CANDIDATE EXAMINATION REPORT</strong>
                                                 <hr/>

                                            <table class="table-condensed" width = "100%">

                                                    <tr>
                                                        <td colspan="2" align="center">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>EXAM NUMBER</strong></td>
                                                        <td><?= $candidate->examno ?></td>
                                                        <td rowspan="5">
                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                <div class="fileupload-new thumbnail" style="width: 110px; height: 110px;"><img src="<?= $passport; ?>" width="100px" height="120px" alt="Passport" /></div>
                                                                 <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                              </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>CANDIDATE NAME:</strong></td>
                                                        <td><?= strtoupper($candidate->firstname.' '.$candidate->othernames); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>EXAMINATION TYPE:</strong></td>
                                                        <td><strong><?= strtoupper($examdetail->examdesc. ' (' . $examdetail->examname .') '. $examyear); ?></strong></td>
                                                    </tr>

                                                    <tr>
                                                        <td><strong>SCHOOL:</strong></td>
                                                        <td><?= strtoupper($this->registration_model->get_school($candidate->schoolid))?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>SUBJECT TAKEN:</strong></td>
                                                        <td ><strong>MATHEMATICS - ENGLISH - GENERAL KNOWLEDGE - IGBO </strong> </td>
                                                    </tr>
                                                     <tr>
                                                         <td><strong>GRADE OBTAINED</strong></td>
                                                         <td> <h1> <?= $gradeObtained; ?> </h1></td>
                                                    </tr>
                                                     <?php 
                                                     
                                                     $total = 0; $resultReleased = false;
          
                                                      ?>
                                                   <tr>
                                                        <td colspan="2" align="center">&nbsp;</td>
                                                    </tr>

                                                 
                      
                                                    <tr>
                                                        <td><strong>KEY</strong></td>
                                                        <td><strong> F:FAIL <br/>
                                                                      P:PASS <br/> 
                                                                      C:CREDIT <br/> 
                                                                      D:DISTINCTION <br/> 
                                                                      NES : NO EXAMINATION SCORES </strong></td>
                                                        <td align="left"></td>
                                                    </tr>
                                                    <tr>

                                                        <td align = "center" colspan = '3'> <img src="<?= base_url(str_replace('./', '', config_item('edc_logo_path')).$this->edcid.'_stamp.png') ?>" alt="Stamp" style="width: 150px; height: 120px" /> </td>
                                                    </tr>
                                                 
                                                  
                                                 <?php
                                                    else:
                                                        
                                                        echo '<tr>
                                                                <td colspan="3">
                                                                <br/><br/>
                                                                    <strong style="color: crimson;">AWAITING RESULT - CHECK BACK LATER</strong>
                                                                    <hr/>
                                                                </td>
                                                              </tr>
                                                            ';
                                                        
                                                    endif;
                                                   
                                                  ?>
                                            </table>
                                          

                                        </div>

                                   </div>
                </div>
				<p style="page-break-after: always;"></p>
            </div>
        </div>
     
    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>   
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script> 
    
   </body>
</html>
