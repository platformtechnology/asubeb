<div class="row">
	<div class="span4">
              <h4>My requests</h4>
                <hr/>
                <ul class="list_posts">
                    <?php 
                        if(count($support_data)){
                            echo '<table border="0">';
                            foreach($support_data as $data){
                                $subject = $data->subject;
                                $subjectLen = strlen($subject);
                                if($subjectLen > 24)
                                    $subject = substr($subject, 0, 24) . " ...";
                                
                                
                    ?>
                    
                        <tr>
                            <td>
                                <li>
                                    <a href="<?= site_url('web/support/requestdetails/'.$data->requestid.'/'.$data->phone) ?>" class="list-group-item" title="<?= $data->subject ?>">
                                        <strong><?= $subject ?></strong>
                                    </a>
                                </li>
                            </td>
                            <td><?= get_status($data->status) ?></td>
                        </tr>
                        
                   
                    <?php
                        }// end foreach

                     echo '</table>';

                    }else{
                        echo "No previous requests ... ";
                    }
                    ?>
               </ul>
	</div>
	
    <div class="span8">
	
	<div class="well">
            <strong><h4>Initiate a new Support Request</h4></strong>
        <hr/>
            <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
            <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
            <?= form_open(site_url('web/support/sendrequest'), 'class="form-horizontal"'); ?>
           
               <table width="50%" border="0" cellpadding="5" align="center" >
                   <input type="hidden" name="phone" value="<?= $phonenumber ?>" />
                <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>Fullname:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                     <input type="text" name="name" placeholder="Enter Your Name" value="<?= set_value('name'); ?>" class="input-xlarge" required="required" />
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>Subject:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                          <input type="text" name="subject" placeholder="Enter Subject of Request" value="<?= set_value('subject'); ?>" class="input-xlarge" required="required" />
                     </label>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>Request:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <textarea name="request" class="input-xlarge" rows="8" wrap="true" required="required" style="width: 350px"><?= set_value('request'); ?></textarea>
                     
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top">&nbsp;</td>
                  <td align="left" valign="top" style="text-align: left;">
                    <button type="submit" class="btn btn-primary"><i class="icon-comment icon-white"></i> Send</button>
                      <a href="<?=  site_url('home');?>" class="btn btn-warning"><i class="icon-arrow-left icon-white"></i> Cancel</a>
                  </td>
                </tr>
                
              </table>
        
           
            <?= form_close();?>
	</div>
        
    </div>
</div>