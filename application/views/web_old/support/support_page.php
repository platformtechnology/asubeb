<div class="row">
	<div class="span5">

		<div class="accordion" id="accordion2">
                     <strong><h4>FAQ'S</h4></strong>
                     <hr/>

                    <div class="accordion-group">
                      <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#1">
                             I forgot to print my registration detail.
                            </a>
                      </div>
                      <div id="1" class="accordion-body collapse" style="height: 0px;">
                            <div class="accordion-inner">
                             Navigate to "Candidate's Login", enter your registration number and pin
                             then click the submit button. You will see your printable profile again.
                            </div>
                      </div>

                      <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#2">
                             I registered a candidate twice for the same exam.
                            </a>
                      </div>
                      <div id="2" class="accordion-body collapse" style="height: 0px;">
                            <div class="accordion-inner">
                             You will be required to send a support request with the candidates details including Examno and pin.
                             The candidate's record will be removed from the system
                             BUT you will be required to purchase a NEW PIN and re-register the candidate.
                            </div>
                      </div>

                     <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#3">
                             I mistakenly registered a candidate in another school.
                            </a>
                      </div>
                      <div id="3" class="accordion-body collapse" style="height: 0px;">
                            <div class="accordion-inner">
                             Send a support request with the candidates details including Examno and pin.
                             The candidate's record will be removed from the system
                             BUT you will be required to purchase a NEW PIN and re-register the candidate.
                            </div>
                      </div>


                     <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#4">
                             I can see the record of a candidate i did not register in my school.
                            </a>
                      </div>
                      <div id="4" class="accordion-body collapse" style="height: 0px;">
                            <div class="accordion-inner">
                             This occurs when a candidate mistakenly chooses another school (in this case your school) during registration.
                             Ignore such a record as it will be removed from the system in no time. However, kindly raise a support request.
                            </div>
                      </div>

                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#5">
                             Examno is not serial in the comprehensive list - some are missing (e.g 001, 002, 004). Is this a major problem?
                            </a>
                      </div>
                      <div id="5" class="accordion-body collapse" style="height: 0px;">
                            <div class="accordion-inner">
                             No, it is not a major problem.
                             This only occurs when a candidate's has been previously removed from the system (on your request as the case may be).
                             The order in which the rest of the examno appears is valid for the candidates. <strong>N.B:</strong> Do not assume that 004 is now 003 in such cases.

                            </div>
                      </div>
                    </div>

		 </div>

	</div>

    <div class="span7">
	<strong><h4>Client Support Portal</h4></strong>
        <hr/>
	<div class="well">
            <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
            <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
            <?= form_open('', 'class="form-horizontal"'); ?>
            <div class="container">
                Enter Your Phone Number:<br/>
                <input type="text" name="phonenum" placeholder="Enter Phone Number" value="<?php echo set_value('phonenum'); ?>" class="input-xlarge" required="required" />
                <br/>
                <span style="color: crimson; font-size: 12px;">Your phone number is used to track your requests</span>
                <br/><br/>
                <button type="submit" class="btn btn-info"><i class="icon-share icon-white"></i> Enter</button>
            <a target="_blank" href="<?=  site_url('web/schools/confirm_admin_number');?>" class="btn btn-success"><i class="icon-arrow-right icon-white"></i> Click here to confirm Admin Phone</a>
            </div>
            <?= form_close();?>
	</div>

    </div>
</div>