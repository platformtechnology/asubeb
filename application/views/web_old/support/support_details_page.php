<div class="row">
	<div class="span4">
              <h4>My requests</h4>
                <hr/>
                <ul class="list_posts">
                    <?php 
                        if(count($support_data)){
                            echo '<table border="0">';
                            foreach($support_data as $data){
                                $subject = $data->subject;
                                $subjectLen = strlen($subject);
                                if($subjectLen > 24)
                                    $subject = substr($subject, 0, 24) . " ...";
                                
                                
                    ?>
                    
                        <tr>
                            <td>
                                <li>
                                    <a href="<?= site_url('web/support/requestdetails/'.$data->requestid.'/'.$data->phone) ?>" class="list-group-item" title="<?= $data->subject ?>">
                                        <strong><?= $subject ?></strong>
                                    </a>
                                </li>
                            </td>
                            <td><?= get_status($data->status) ?></td>
                        </tr>
                        
                   
                    <?php
                        }// end foreach

                     echo '</table>';

                    }else{
                        echo "No previous requests ... ";
                    }
                    ?>
               </ul>
	</div>
	
    <div class="span8">
	<?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
	<div class="well">
           
                <h4><i class="icon icon-align-justify"></i> <?= ucwords(strtolower($request_data->subject)); ?></h4>
                <strong>RID:<span style="color: crimson;"> #<?= $request_data->requestid; ?></span></strong><br/>
                <span><strong>Status:</strong> <?= get_status($request_data->status); ?></span><br/>
                <span><strong>Date:</strong> <?= substr($request_data->datecreated , 0, 16); ?></span><br/><br/>
                <table border="0" cellpadding="5">
                    <tr>
                        <td valign="top"><strong>Request:</strong></td>
                        <td valign="top"> <?= $request_data->request; ?></td>
                    </tr>
                </table>
                           
        <hr/>
        <?php 
        
            $comments = $this->support_comment_model->get_comments($request_data->requestid); 
            if(count($comments)){
                echo ' <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Replies</th>
                            </tr>
                        </thead> 
                        <tbody>';
       
                foreach ($comments as $comment) {
                   echo "<tr>"
                            . "<td>";
                            echo "<strong>".$comment->owner."</strong> | ". substr($comment->datecreated , 0, 16). "<br/>";
                            echo '<span style="font-size: 13px; color:#2f5e9e; font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;">';
                            echo $comment->comment;
                            echo '</span>'.'<br/>';
                            echo "</td>"
                        . "</tr>";
                } 
                
                echo   '</tbody>
                 </table>';
            }
        ?>
            <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
            <?php if($this->session->flashdata('Cmsg')) echo get_success($this->session->flashdata('Cmsg')); ?>
            <?= form_open(site_url('web/support/reply'), 'class="form-horizontal"'); ?>
           
               <table width="100%" border="0" cellpadding="5" align="center" >
                <tr>
                   <input type="hidden" value="<?= $request_data->phone; ?>" name="phone" />
                   <input type="hidden" value="<?= $request_data->requestid; ?>" name="requestid" />
                   
                  <td align="left" valign="top" style="text-align: left;">
                      <strong>Reply:</strong><br/>
                      <textarea name="comment" class="input-xlarge" rows="6" wrap="true" required="required" style="width: 100%"><?= set_value('comment'); ?></textarea>
                     
                  </td>
                </tr>
                <tr>
                 
                  <td align="left" valign="top" style="text-align: left;">
                    <button type="submit" class="btn btn-primary"><i class="icon-comment icon-white"></i> Post</button>
                  </td>
                </tr>
                
              </table>
        
           
            <?= form_close();?>
	</div>
        
    </div>
</div>
