
<?php if($this->uri->segment(4) == 'success') echo '<div class="span12"><strong>If Updated Passport Is Not Displaying, Kindly <a href="'. site_url('registration/confirm/'.$reg_info->candidateid) .'">Refresh This Page</a></strong></div><br/><br/>'; ?>
<div id="printdiv">
<div class="row">
	<div class="span12">
            <div class="page-header">
                <h3 style="text-align: center; color: #0077b3;">
                    <a href="#" title="Examination Development Centre"><img src="<?=$edclogo;?>" alt="Logo"/></a><br/>
                    REGISTRATION FOR <br/>THE <?= strtoupper($exam_detail->examdesc. ' ' . $reg_info->examyear) ;?>
                </h3>
            </div>
	</div>
</div>
<div class="row">
     <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
     <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
    
    <div class="span12 well">
       <table class="table table-bordered" width="100%">
           <?php
            if (strtotime(substr($reg_info->datecreated, 0,10)) > strtotime('2016-02-25') && $reg_info->examid == "ss97q7bi08"):

           ?>
           <tr>
               <td></td>
               <td align = "center"> <center> <span style="color:crimson;font-size:20px;text-align: center;"> <strong> LATE REGISTRATION </strong> </span> </center> </td>
               <td></td>
           </tr>
           <tr>

           <?php
           endif;
            if($exam_detail->haszone){
           ?>
               <td><strong>Zone:</strong></td>
               <td><?= $this->registration_model->get_zone($reg_info->zoneid); ?> </td>

           <?php
                }
                else
                {
            ?>
                <td><strong>Local Govt Area:</strong></td>
                <td><?= $this->registration_model->get_lga($reg_info->lgaid); ?> </td>
            <?php
                }
            ?>
              <td rowspan="4" valign="top">
                  <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="fileupload-new thumbnail" style="width: 150px; height: 112px;"><img src="<?= $passport; ?>" alt="Passport" /></div>
                       <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                    </div>
              </td>
            </tr>

            <tr>
             <td><strong>Present School:</strong></td>
              <td><?= $this->registration_model->get_school($reg_info->schoolid); ?> </td>
            </tr>
            <tr>
              <td><strong>Examination Centre:</strong></td>
              <td><?= $this->registration_model->get_school($reg_info->centreid); ?> </td>

            </tr>

            <tr>
                <td> <strong> Examination Number: </strong></td>
              <td align="center" valign="top"><strong> <span style="color:crimson;"><?= $reg_info->examno; ?></span></strong></td>
            </tr>
            <tr>
              <td> <strong> Pin: </strong> </td>
              <td align="center" valign="top"><strong><span style="color:crimson;"><?php  $pin = $this->registration_model->get_pin($reg_info->candidateid);  echo $pin;?></span></strong></td>
            </tr>
            <tr>
              <td> <strong>Form Number: </strong></td>
              <td align="center" valign="top"><strong> <span style="color:crimson;"><?= $this->registration_model->get_form_number($pin);?></span></strong></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
               <td>&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Surname:</strong></td>
              <td><?= $reg_info->firstname; ?></td>
               <td>&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Othernames:</strong></td>
              <td><?= $reg_info->othernames; ?></td>
              <td valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Gender:</strong></td>
              <td><?= ($reg_info->gender == 'M' ? 'Male' : 'Female') ;?></td>
              <td valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Date of Birth:</strong></td>
              <td><?= $reg_info->dob; ?></td>
              <td valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Phone Number:</strong></td>
              <td><?= $reg_info->phone; ?></td>
              <td valign="top">&nbsp;</td>
            </tr>
            <tr>
             <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td valign="top">&nbsp;</td>
            </tr>

            <tr>
            <td><strong>Registered Subjects:</strong></td>
            <td>
                  <?php
                    if(count($subjects)):
                        $sn = 0;
                        foreach ($subjects as $subject) {

                            echo ++$sn .'. <strong>'.$this->registration_model->get_subject($subject->subjectid);
                            if($subject->armid != '0'){
                                $armdata = $this->registration_model->get_sub_subject($subject->armid, true);
                                echo ' (' . ucwords(strtolower($armdata->armname)) . ') ';
                            }
                            echo '</strong><br/> ';
                        }
                    endif;
                   ?>
              </td>
              <td valign="top">&nbsp;</td>
            </tr>

            <tr>
             <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td valign="top">&nbsp;  </td>
            </tr>
             <?php if($exam_detail->hasposting == 1): ?>
                <tr>
                  <td><strong>First Choice:</strong></td>
                  <td><?= $this->registration_model->get_school($reg_info->firstchoice); ?> </td>
                  <td valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td><strong>Second Choice:</strong></td>
                  <td><?= $this->registration_model->get_school($reg_info->secondchoice); ?> </td>
                  <td valign="top">&nbsp;</td>
                </tr>
             <?php endif; ?>

          </table>
        </div>

</div>

</div>
 <div class="row">
     <div class="span12">
     <p>
      <a href="<?=  site_url('registration/finish/'.$reg_info->candidateid);?>" class="btn btn-primary"><i class="icon-ok icon-white"></i> Finish</a>
      <a href="<?=  site_url('registration/register/'.$reg_info->examid.'/'.$reg_info->examyear.'/'. $enc_pin .'/' .$reg_info->candidateid);?>" class="btn btn-warning"><i class="icon-edit icon-white"></i> Edit</a>
      <button type="button" onclick="printDiv('printdiv')" class="btn btn-info"><i class="icon-print icon-white"></i> Print</button>
  </p>
  </div>
</div>