<div class="row">

	<div class="span12">
            <ul class="breadcrumb">
                <li><a href="<?=  site_url('home');?>">Home</a> <span class="divider">/</span></li>
			<li class="active">Timetables</li>
            </ul>
            <div class="caption">
                <h2>Timetables</h2>


                    <?php
                        if(count($edc_timetables)):
                            foreach ($edc_timetables as $value) {
                    ?>
                <div style="background: white;padding: 1em;" id = "printArea">
                    <div align = "center">
                        <h3>  <strong style="color:blue;"><?=$value->timetable_title;?></strong> </h3>
                    </div>
                    <div align ="center">
                             <?=$value->timetable_content;?>
                    </div>

                </div>
                    <div align ="center">
                        <div class="row">
                        <div class="span12">
                            <div class="span6 pull-right" style="margin-bottom: 1em">
                        <button class="btn btn-primary" onclick="PrintDiv()"> <i class="icon icon-print icon-white"></i> Print Timetable </div>
                             <?php if (!empty($value->timetable_file)) { ?>
                        <div class="span6 pull-right">
                        <a href="<?php echo base_url($value->timetable_file) ?>"> <button class="btn btn-success"> <i class="icon icon-download icon-white"></i> Download Timetable </button></a>
                        </div>
                                 <?php }?>
                    </div>
                    </div>
                    </div>
                    <?php
                            }
                        endif;
                    ?>

        </div>
	</div>
</div>
<script type="text/javascript">
    function PrintDiv(){
        var old_data = document.body.innerHTML;
        var print_data = document.getElementById('printArea');

        newWin= window.open("");
        newWin.document.write(print_data.outerHTML);
        newWin.print();
        newWin.close();

    }
</script>