﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?= $edcname ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="<?php echo base_url('resources/web/css/bootstrap.min.css'); ?>" rel="stylesheet"/>
    
    <link href="<?php echo base_url('resources/web/css/style.css'); ?>" rel="stylesheet"/>

    <?= $page_level_styles; ?>
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    
    <style type="text/css">
        .css-vertical{
            filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                 -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
             -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
              -ms-transform: rotate(-90.0deg);  /* IE9+ */
               -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
          -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                  transform: rotate(-90.0deg);  /* Standard */
                  white-space:nowrap;
        }
    </style>
    
    <script lang="javascript" type="text/javascript">
        window.addEventListener('load', function () {
            var rotates = document.getElementsByClassName('css-vertical');
            for (var i = 0; i < rotates.length; i++) {
                rotates[i].style.height = rotates[i].offsetWidth + 'px';
            }
        });
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
    </script>

  </head>
  
  <body data-offset="40" style="background-color: #fff;" >
     <header class="header" style="background-color: <?= $edc_data->themecolor ?>;">
        <div class="container">
            <div class="row">
                <h4 class="span6">
                    <?= get_edc_logo($edclogo, $edcname, $web = true); ?>
                </h4>
            </div>
        </div>
    </header>
    <div class="shadow-top"></div>
    <?php 
        if($this->uri->uri_string() != "web/schools/filter") 
            echo '<div class="container">';
    ?>
    
   