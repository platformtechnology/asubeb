


<footer class="footer">
    <hr class="soften"/>
    <p>&copy; Copyright <?= date('Y') . ', ' ?> <a href="<?= base_url('admin') ?>" target="_blank"><?= ucwords(strtolower($edcname)) ?></a> <br/><br/></p>
 </footer>
</div>
    <?php 
   // if(isset($print) && $print) echo '<script type="text/javascript"> printDiv("printdivx"); </script>';
?>
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="<?php //echo base_url('resources/web/js/jquery-1.8.3.min.js'); ?>"></script>-->
    <script src="<?php echo base_url('resources/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('resources/web/js/bootstrap.min.js'); ?>"></script>
    <?= $page_level_scripts; ?>
    <script>
        $('.carousel').carousel();
    </script>
    <script>
        $(document).ready(function() {
                $('#example').dataTable();
        } );
    </script>
   
    
  </body>
</html>

