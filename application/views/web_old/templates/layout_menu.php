  <!-- Navbar
    ================================================== -->
<div class="container">
    <div class="navbar">
  <div class="navbar-inner">
	<div class="container">
	  <div class="nav-collapse">
		<ul class="nav">
                    <?php $curr_url = $this->uri->segment(1); ?>

                    <?php if(!$this->session->userdata('loggedin') && !$this->session->userdata('staff_loggedin') && !$this->session->userdata('school_loggedin')): ?>
                        <li class="<?= ($curr_url == 'home' || !$curr_url) ? 'active' : ''; ?>"><a href="<?=  site_url('home'); ?>">Home	</a></li>
                    <?php endif; ?>

                    <?php if(!$this->session->userdata('staff_loggedin') && !$this->session->userdata('school_loggedin')): ?>
                    <li class="<?= ($curr_url == 'registration') ? 'active' : ''; ?>"><a href="<?=  site_url('registration'); ?>">Registration</a></li>
                    <?php endif; ?>
                    <!--<li class="<?= ($curr_url == 'about') ? 'active' : ''; ?>"><a href="<?=  site_url('about'); ?>">About</a></li>-->
                    <!--<li class="<?= ($curr_url == 'news') ? 'active' : ''; ?>"><a href="<?=  site_url('news'); ?>">News</a></li>-->
                    <?php if (count($timetable_data)) {
                        ?>
                                        <li class="<?= ($curr_url == 'timetable') ? 'active' : ''; ?>"><a href="<?=  site_url('web/timetable'); ?>">Examination Timetables</a></li>
                    <?php
                    }
                    ?>
                    <!--<li class=""><a href="#">Confirm Regno</a></li>-->
                    <?php if($this->session->userdata('candidate_loggedin')): ?>
                        <li class=""><a href="<?=  site_url('web/login/logout'); ?>">Logout</a></li>
                    <?php elseif($this->session->userdata('staff_loggedin')): ?>
                        <li class=""><a href="<?=  site_url('web/staff/logout'); ?>">Logout</a></li>
                    <?php elseif($this->session->userdata('school_loggedin')): ?>
                        <li class=""><a href="<?=  site_url('web/schools/logout'); ?>">Logout</a></li>
                    <?php else: ?>
                        <!--<li class="<?= ($this->uri->segment(2) == 'staff') ? 'active' : ''; ?>"><a href="<?=  site_url('web/staff'); ?>">Administration</a></li>-->
                        <li class="<?= ($this->uri->segment(2) == 'login') ? 'active' : ''; ?>"><a href="<?=  site_url('web/login'); ?>">Candidates Login</a></li>
                        <li class="<?= ($this->uri->segment(2) == 'schools') ? 'active' : ''; ?>"><a href="<?=  site_url('web/schools'); ?>">Schools Login</a></li>
                    <?php endif; ?>
		</ul>


              <ul class="nav pull-right">
                <li class=""><a href="<?= $current_url; ?>"><i class="icon icon-arrow-left"></i> Main Site </a></li>
		<li class="<?= ($this->uri->segment(2) == 'support') ? 'active' : ''; ?>">
                    <a href="<?= site_url('web/support'); ?>"><i class="icon icon-comment"></i> Support </a>
		  </li>
		</ul>
	  </div><!-- /.nav-collapse -->
	</div>
  </div><!-- /navbar-inner -->
</div>
</div>
<!-- ======================================================================================================================== -->

