
<div class="row">
	<div class="span12">
            <div class="page-header">
                <h3 style="text-align: center; color: #0077b3;">
                    REGISTRATION FOR <br/>THE <?= strtoupper($exam_detail->examdesc. ' ' . $exam_year) ;?>
                </h3>
            </div>
	</div>
</div>
<div class="row">
     <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
     <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>

    <?php if(trim($candidate_detail->firstname) == false){ ?>
            <div class="span12">
               <?php// form_open('', 'class="form-horizontal" role="form"'); ?>
                <form action ="" class ="form-horizontal">
                    <div class="col-md-5">
                        <div class="form-group">
                            <input type="text" style="font-weight: bold; color: crimson;" name="former_regnum" class="form-control" value="<?= set_value('former_regnum') ?>" placeholder="Enter Previous Regnum" required="required" />
                            <button type="submit" class="btn btn-info"><i class="icon-arrow-down icon-white"></i> Fetch Profile</button>
                            <br/><strong>If you have registered for another exam before, enter your regnumber to fetch your profile </strong>
                         </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                         </div>
                    </div>

                <?= form_close(); ?>
            </div>
    <?php } ?>

    <div class="span12 well">
        <?php //form_open_multipart('', 'class="form-horizontal"'); ?>
        <form action ="" enctype="multipart/form-data" method = "post">

      <table class="table table-bordered">
            <tr>

              <?php
                if($exam_detail->haszone)
                {
              ?>
                    <td><strong>Zone:</strong></td>
                    <td>
                        <?php
                        $disabled = ((trim($candidate_detail->firstname) != false) && !$this->session->userdata('oldprofile')) ? 'disabled' : '';
                          $select_options = array();
                          foreach ($zones as $zone) {
                              $select_options[$zone->zoneid] = strtoupper($zone->zonename);
                          }
                          $select_options[''] = "------ Select Zone ------";
                          echo form_dropdown('zoneid', $select_options, $this->input->post('zoneid') ? $this->input->post('zoneid') : $candidate_detail->zoneid, ' onchange="makeZoneReq()" class="input-xlarge" required="required" id="zoneid" '. $disabled);
                        ?>

                </td>
              <?php
                }
                else{
               ?>
                   <td><strong>Local Govt Area:</strong></td>
                    <td>
                        <?php
                        $disabled = ((trim($candidate_detail->firstname) != false) && !$this->session->userdata('oldprofile')) ? 'disabled' : '';
                              $select_options = array();
                              $select_options[''] = "------ Select LGA ------";
                                foreach ($lgs as $lg) {
                                    $select_options[$lg->lgaid] = $lg->lganame;
                                }
                              echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : $candidate_detail->lgaid, 'id="lgaid" class="input-xlarge" required="required" ' . $disabled);
                          ?>
                    </td>
               <?php
                }
               ?>

              <td><strong>Date of birth:</strong></td>
              <td><!--<div class="bfh-datepicker" data-format="y-m-d" data-date="<?php //echo set_value('dob', $candidate_detail->dob); ?>" data-name="dob" data-date="2016-01-01"></div>
                  <div class="bfh-datepicker" data-format="y-m-d" data-date="<?php //echo set_value('dob', $candidate_detail->dob); ?>" data-name="dob" data-date="2004-01-01"></div>-->
    <div class="input-append date" id="dp3" data-date="2012-02-12" data-date-format="yyyy-mm-dd">

      <input class="input-xlarge datepicker" name ="dob"  size="16" type="text" value="<?php echo set_value('dob', $candidate_detail->dob); ?>">

    </div>
              </td>
            </tr>
            <tr>
             <td><strong>Present School:</strong></td>
              <td>
                  <?php
                        //IF CANDIDATE HAS ALREADY REGISTERED DISABLED SCHOOL DROP DOWN LIST
                        $disabled = ((trim($candidate_detail->firstname) != false) && !$this->session->userdata('oldprofile')) ? 'disabled' : '';
                        $select_options = array();
                        $select_options[''] = "---- Select School ----";
                        //IF CANDIDATE HAS REGISTERED
                        if(trim($candidate_detail->firstname) != false){
                              foreach ($schools as $school) {
                                  $select_options[$school->schoolid] = $school->schoolname;
                              }
                        }
                        echo form_dropdown('schoolid', $select_options, $candidate_detail->schoolid, 'id="schoolid" class="input-xlarge"  required="required" ' . $disabled);
                    ?>
                  <span id="loader"></span>
              </td>
              <td><strong>Phone:</strong></td>
              <td><input type="text" name="phone" value="<?php echo set_value('phone', $candidate_detail->phone); ?>" class="input-xlarge"  required="required"/></td>
            </tr>
            <tr>
                <td><strong>&nbsp;</strong></td>
                <td>&nbsp;</td>

                <td>&nbsp;</td>
                <td>&nbsp;</td>

            </tr>
            <tr>
              <td><strong>Surname:</strong></td>
              <td><input type="text" name="firstname" value="<?php echo set_value('firstname', $candidate_detail->firstname); ?>" placeholder="Enter Your Surname" class="input-xlarge" required="required" /></td>
              <td><strong>Passport:</strong></td>
              <td rowspan="4">
                  <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="fileupload-new thumbnail" style="width: 150px; height: 112px;"><img src="<?= trim($candidate_detail->firstname) == false ? get_img('no_image.jpg') : base_url('passports/'.$candidate_detail->candidateid.'.jpg'); ?>" alt="Screen Shot" />
                      </div>
                       <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                       <div>

                           <span class="btn btn-file btn-default"><span class="fileupload-new"><input type="file" name="passport"/></span><span class="fileupload-exists">Change</span><input type="file" /></span>
                           <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                       </div>
                    </div>
                  <span style="color: crimson;">JPG or PNG file not more than <strong>50Kb</strong></span>
              </td>
            </tr>
            <tr>
              <td><strong>Othernames:</strong></td>
              <td><input type="text" name="othernames" value="<?php echo set_value('othernames', $candidate_detail->othernames); ?>" placeholder="Enter Your Other Names" class="input-xlarge" required="required" /></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Gender:</strong></td>
              <td><?php
                        $select_options = array();
                        $select_options['M'] = "Male";
                        $select_options['F'] = "Female";
                        echo form_dropdown('gender', $select_options, $this->input->post('gender') ? $this->input->post('gender') : $candidate_detail->gender, 'id="gender" class="input-xlarge" required="required" ');
                    ?></td>
             <td>&nbsp;</td>
            </tr>
            <tr>
             <td>&nbsp;</td>
              <td>&nbsp;</td>
               <td>&nbsp;</td>
            </tr>

            <tr>
            <td><strong>Subjects:</strong></td>
            <td colspan="3">
                  <?php
                    $count = 0;
                    foreach ($subjects as $subject) {
                        $subjBuffer = array();

                        if(isset($registered_subjects)){

                            foreach ($registered_subjects as $reg_subject) {
                                if($reg_subject->subjectid == $subject->subjectid){

                                    $sub_data = $this->registration_model->get_sub_subject($subject->subjectid);
                                    echo (count($sub_data) ? '<br/><br/>' : '')
                                     . '<input name="subject[]" type="checkbox" value="'.$subject->subjectid.'" ' . (($subject->iscompulsory == 1) ? 'disabled="disabled"' : '') . ' checked="checked" /><strong>' . ucwords(strtolower($subject->subjectname)) . '</strong> &nbsp;';

                                    $subjBuffer[] = $reg_subject->subjectid;
                                    echo show_sub($sub_data, $subject->subjectid, $reg_subject->armid);
                                }
                            }
                        }
                        else{
                            $sub_data = $this->registration_model->get_sub_subject($subject->subjectid);
                            echo (count($sub_data) ? '<br/><br/>' : '') . '<input name="subject[]" type="checkbox" value="'.$subject->subjectid.'" ' . (($subject->iscompulsory == 1) ? 'checked="checked" disabled="disabled"' : '') . ' /><strong>' . ucwords(strtolower($subject->subjectname)) . '</strong> &nbsp;';

                            echo show_sub($sub_data, $subject->subjectid);
                        }

                        if(isset($registered_subjects) && !in_array($subject->subjectid, $subjBuffer)){

                            $sub_data = $this->registration_model->get_sub_subject($subject->subjectid);
                            echo (count($sub_data) ? '<br/><br/>' : '') . '<input name="subject[]" type="checkbox" value="'.$subject->subjectid.'" ' . (($subject->iscompulsory == 1) ? 'checked="checked" disabled="disabled"' : '') . ' /><strong>' . ucwords(strtolower($subject->subjectname)) . '</strong> &nbsp;';
                            echo show_sub($sub_data, $subject->subjectid);
                        }

                        $sub_data = $this->registration_model->get_sub_subject($subject->subjectid);

                        if(!count($sub_data)){
                             ++$count;
                            if(($count % 2) === 0) echo '<br/><br/>';
                        }

                    }

                    function show_sub($sub_data, $subjectid, $armid = null){

                        $CI =& get_instance();

                        if(count($sub_data)){
                            $select_options = array();
                              foreach ($sub_data as $arm) {
                                  $select_options[$arm->armid] = ucwords(strtolower($arm->armname));
                              }
                            $ddl = form_dropdown($subjectid.'armid', $select_options, $CI->input->post('armid') ? $CI->input->post('armid') : $armid, 'id="armid" class="input-xlarge" required="required"');

                            $ddl .= '<br/><br/>';

                            return $ddl;
                        }

                        return '';
                    }
                   ?>
              </td>
            </tr>

            <tr>
             <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;  </td>
            </tr>
            <tr>
              <?php if($exam_detail->hasposting == 1): ?>
              <td><strong>First Choice:</strong></td>
              <td> <?php
                        $select_options = array();
                        $select_options[''] = "---- Select 1st Choice ----";
                        if(trim($candidate_detail->firstname) != false){
                           foreach ($choice1 as $choice) {
                                $select_options[$choice->schoolid] = $choice->schoolname;
                            }
                        }
                        $selected = $this->input->post('firstchoice') ? $this->input->post('firstchoice') : $candidate_detail->firstchoice;
                        echo form_dropdown('firstchoice', $select_options,$selected , 'id="firstchoice" class="input-xlarge" required="required"');
                    ?>
                  &nbsp; <strong>/ <span id="schoolcode1" style="color: crimson">No Code</span></strong>
              </td>
              <?php else: ?>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
              <?php endif; ?>

              <td>&nbsp;</td>
              <td>
                  <button type="submit" class="btn btn-primary"><i class="icon-upload icon-white"></i> Save & Proceed</button>
                  <a href="<?= ($this->session->userdata('loggedin') ? site_url('registration/confirm/'.$candidate_detail->candidateid) : site_url('registration')); ?>" class="btn btn-warning"><i class="icon-remove icon-white"></i> Cancel</a>
              </td>
            </tr>
            <tr>
               <?php if($exam_detail->hasposting == 1): ?>
                <td><strong>Second Choice:</strong></td>
                <td> <?php
                          $select_options = array();
                          $select_options[''] = "---- Select 2nd Choice ----";
                            foreach ($choice2 as $choice) {
                                $select_options[$choice->schoolid] = $choice->schoolname;
                            }
                          echo form_dropdown('secondchoice', $select_options, $this->input->post('secondchoice') ? $this->input->post('secondchoice') : $candidate_detail->secondchoice, 'id="secondchoice" class="input-xlarge" required="required"');
                      ?>
                     &nbsp; <strong>/ <span id="schoolcode2" style="color: crimson">No Code</span></strong>
                </td>
               <?php else: ?>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
              <?php endif; ?>
              <td>&nbsp;</td>
             <td>&nbsp;</td>
            </tr>
      </table>
        </form>
        <?php // form_close(); ?>
    </div>
</div>

  <script type="text/javascript">
     (function() {
        var httpRequest;

       //zoneddl = document.getElementById("zoneid")
//        zoneddl.onchange = function() {
//            var target_urlx = "<?php // site_url('registration/school_ajaxdrop_byzone?issec='.(($exam_detail->hassecschool == 1) ? '1' : '0').'&zoneid='); ?>";
//            document.getElementById("loader").innerHTML = "<img src='<?php // get_img('loader.gif') ?>' />";
//            makeRequest( target_urlx + zoneddl.value, 'schoolid' );
//        };
        lgaddl = document.getElementById("lgaid");
        //POPULATE PRESENT SCHOOL DROP DOWN
        lgaddl.onchange = function() {

            // GET PRESENT SCHOOL DROPDOWN OPTIONS
            var target_url1 = "<?= site_url('registration/school_ajaxdrop?issec='.(($exam_detail->hassecschool == 1) ? '1' : '0').'&lgaid='); ?>";
            var choice1url = "<?= site_url('registration/sec_school_ajaxdrop?issec=1&lgaid='); ?>";
            document.getElementById("loader").innerHTML = "<img src='<?= get_img('loader.gif') ?>' />";
            makeRequest( target_url1 + lgaddl.value, 'schoolid');
            makeSecSchoolRequest(choice1url + lgaddl.value, 'firstchoice');


        };


        firstchoiceddl = document.getElementById("firstchoice");
        firstchoiceddl.onchange = function() {
            var target_url2 = "<?= site_url('registration/code_ajaxdrop?schoolid='); ?>";
            makeCodeRequest( target_url2 + firstchoiceddl.value, 'schoolcode1');

        };

        secondchoiceddl = document.getElementById("secondchoice");
        secondchoiceddl.onchange = function() {
            var target_url3 = "<?= site_url('registration/code_ajaxdrop?schoolid='); ?>";
            makeCodeRequest( target_url3 + secondchoiceddl.value, 'schoolcode2');

        };

         function makeCodeRequest(url, targetid) {
            httpRequest = getHttpObject();

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = function(){
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        //var data = JSON.parse(httpRequest.response);
                        document.getElementById(targetid).innerHTML = httpRequest.response;

                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            httpRequest.send();
        }


        function makeRequest(url, targetid) {

            httpRequest = getHttpObject();

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = function(){
                if (httpRequest.readyState === 4) {

                    if (httpRequest.status === 200) {

                        var data = JSON.parse(httpRequest.response);
                        var select = document.getElementById(targetid);
                        document.getElementById("loader").innerHTML = "";
                        if(emptySelect(select)){
                            var el = document.createElement("option");
                                    el.textContent = '---- Select School ----';
                                    el.value = '';
                                    select.appendChild(el);

                            for (var i = 0; i < data.lgas.length; i++){
                                var el = document.createElement("option");
                                    el.textContent = data.lgas[i].lganame.toUpperCase();
                                    el.value = data.lgas[i].lgaid;
                                    select.appendChild(el);
                            }
                        }
                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            httpRequest.send();
        }

        function makeSecSchoolRequest(secschoolurl, targetid) {

            ajaxRequest = getHttpObject();

            if (!ajaxRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            ajaxRequest.onreadystatechange = function(){
                if (ajaxRequest.readyState === 4) {
                    if (ajaxRequest.status === 200) {

                        var secschooldata = JSON.parse(ajaxRequest.response);
                       // window.alert(secschooldata);
                        var select = document.getElementById(targetid);
                        //alert(secschooldata);
                       // document.getElementById("loader").innerHTML = "";
                        if(emptySelected(select)){
                            var el = document.createElement("option");
                                    el.textContent = '---- Select School ----';
                                    el.value = '';
                                    select.appendChild(el);

                            for (var i = 0; i < secschooldata.secschoollgas.length; i++){
                                var el = document.createElement("option");
                                    el.textContent = secschooldata.secschoollgas[i].secschoollganame.toUpperCase();
                                    el.value = secschooldata.secschoollgas[i].secschoollgaid;
                                    select.appendChild(el);
                            }
                        }
                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            ajaxRequest.open('GET', secschoolurl);
            ajaxRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            ajaxRequest.send();
        }

 function makeSecSchool2Request(secschoolurl, targetid) {

            ajaxRequest2 = getHttpObject();

            if (!ajaxRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            ajaxRequest2.onreadystatechange = function(){
                if (ajaxRequest2.readyState === 4) {
                    if (ajaxRequest2.status === 200) {

                        var secschooldata = JSON.parse(ajaxRequest2.response);
                       // window.alert(secschooldata);
                        var select = document.getElementById(targetid);
                        //alert(secschooldata);
                       // document.getElementById("loader").innerHTML = "";
                        if(emptySelected(select)){
                            var el = document.createElement("option");
                                    el.textContent = '---- Select School ----';
                                    el.value = '';
                                    select.appendChild(el);

                            for (var i = 0; i < secschooldata.secschoollgas.length; i++){
                                var el = document.createElement("option");
                                    el.textContent = secschooldata.secschoollgas[i].secschoollganame.toUpperCase();
                                    el.value = secschooldata.secschoollgas[i].secschoollgaid;
                                    select.appendChild(el);
                            }
                        }
                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            ajaxRequest2.open('GET', secschoolurl);
            ajaxRequest2.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            ajaxRequest2.send();
        }
        function emptySelect(select_object){
            var i;
            for(i=select_object.options.length-1;i>=0;i--){
                select_object.remove(i);
            }
            return true;
        }

          function emptySelected(select_object){
            var i;
            for(i=select_object.options.length-1;i>=0;i--){
                select_object.remove(i);
            }
            return true;
        }

         function getHttpObject(){

            var xmlhtp;

            if (window.ActiveXObject)
            {
                var aVersions = [
                  "MSXML2.XMLHttp.9.0","MSXML2.XMLHttp.8.0", "MSXML2.XMLHttp.7.0",
                  "MSXML2.XMLHttp.6.0","MSXML2.XMLHttp.5.0","MSXML2.XMLHttp.4.0",
                  "MSXML2.XMLHttp.3.0","MSXML2.XMLHttp","Microsoft.XMLHttp"
                ];

                for (var i = 0; i < aVersions.length; i++)
                {
                    try
                    {
                        xmlhtp = new ActiveXObject(aVersions[i]);
                        return xmlhtp;
                    } catch (e) {}
                }
            }
            else  if (typeof XMLHttpRequest != 'undefined')
            {
                    xmlhtp = new XMLHttpRequest();
                    return xmlhtp;
            }
            throw new Error("XMLHttp object could be created.");

        }

    })();




    </script>

  <script type="text/javascript">



      function makeZoneReq(){
          zoneid = document.getElementById("zoneid").value;
          var target_urlx = "<?= site_url('registration/school_ajaxdrop_byzone?issec='.(($exam_detail->hassecschool == 1) ? '1' : '0').'&zoneid='); ?>";
            document.getElementById("loader").innerHTML = "<img src='<?= get_img('loader.gif') ?>' />";
            makeZoneRequest( target_urlx + zoneid, 'schoolid' );
      }

     function makeZoneRequest(url, targetid) {

        var httpRequest = getAjaxHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }

         httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {

                     var data = JSON.parse(httpRequest.response);
                        var select = document.getElementById(targetid);
                        document.getElementById("loader").innerHTML = "";
                        if(emptySelect(select)){
                            var el = document.createElement("option");
                                    el.textContent = '---- Select School ----';
                                    el.value = '';
                                    select.appendChild(el);

                            for (var i = 0; i < data.lgas.length; i++){
                                var el = document.createElement("option");
                                    el.textContent = data.lgas[i].lganame.toUpperCase();
                                    el.value = data.lgas[i].lgaid;
                                    select.appendChild(el);
                            }
                        }

                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById("loader").innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        httpRequest.send();
    }

//
        function emptySelect(select_object){
            var i;
            for(i=select_object.options.length-1;i>=0;i--){
                select_object.remove(i);
            }
            return true;
        }


    function getAjaxObject(){

        var xmlhtp;

        if (window.ActiveXObject)
        {
            var aVersions = [
              "MSXML2.XMLHttp.9.0","MSXML2.XMLHttp.8.0", "MSXML2.XMLHttp.7.0",
              "MSXML2.XMLHttp.6.0","MSXML2.XMLHttp.5.0","MSXML2.XMLHttp.4.0",
              "MSXML2.XMLHttp.3.0","MSXML2.XMLHttp","Microsoft.XMLHttp"
            ];

            for (var i = 0; i < aVersions.length; i++)
            {
                try
                {
                    xmlhtp = new ActiveXObject(aVersions[i]);
                    return xmlhtp;
                } catch (e) {}
            }
        }
        else  if (typeof XMLHttpRequest != 'undefined')
        {
                xmlhtp = new XMLHttpRequest();
                return xmlhtp;
        }
    	throw new Error("XMLHttp object could be created.");

    }








</script>