<div class="row">
    <div class="span12 well" style="text-align: center;">
         <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
        <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
        <?= form_open('', 'class="form-horizontal"'); ?>
           <table width="50%" border="0" align="center" >
               <tr>
                   <td style="text-align: center; color: #0077b3;" colspan="2">
                       <h4>Confirm Admin Number</h4>
                   </td>
               </tr>
               <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>Select Lga/Zone:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                          <?php
                            $select_options = array();
                            $select_options[''] = "-----------------Lgas-----------------";
                              foreach ($lgs as $lg) {
                                  $select_options[$lg->lgaid] = strtoupper($lg->lganame);
                              }
                              $select_options['0'] = "--------------Zones-----------------";
                              foreach ($zones as $zone) {
                                  $select_options[$zone->zoneid] = strtoupper($zone->zonename);
                              }
                            echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : '', 'id="lgaid" onchange = "loadSchools()" class="input-xlarge" style = "width:100%" required="required"');
                        ?>
                     </label>
                  </td>
                </tr>
               <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>Select School:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                         <div class="" id = "schoolid">
                             <select class="form-control" style = "width:100%">
                                 <option> ::::Select School Name:::: </option>
                             </select>
                         </div>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top">&nbsp;</td>
                  <td align="left" valign="top" style="text-align: left;">
                    <button type="submit" class="btn btn-primary"><i class="icon-share-alt icon-white"></i> Sign In</button>
                      
                  </td>
                </tr>

              </table>

        <?= form_close();?>
    </div>
</div>

<script language ="javascript" type = "text/javascript">

    function loadSchools() {
        var lgaid = document.getElementById('lgaid').value;
        var url = "<?php echo site_url('Web/schools/getSchools')?>" + "/" + lgaid;

    $.ajax({
      url: url,
      type: 'get',
      success: function(data) {
          $('#schoolid').html(data);
      }
    }); // end ajax call
    }
</script>



