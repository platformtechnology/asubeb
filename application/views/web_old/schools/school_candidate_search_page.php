

        
         <div id="printdiv">
                       
                        <div class="row">
                            <div class="span12">
                                <strong>SCHOOL:</strong> <span style="text-decoration: underline"><?=$school_detail->schoolname; ?></span> 
                                <?php if(!$exam_detail->haszone){ ?>| <strong>L.G.A:</strong> <?= $this->school_model->get_lga($school_detail->lgaid); ?> <?php } ?>
                                | <strong>ZONE:</strong> <?= $this->db->where('zoneid', $school_detail->zoneid)->get('t_zones')->row()->zonename; ?>
                                <br/>
                                <strong>SCHOOL CODE = <?=$school_detail->schoolcode; ?></strong> 
                            </div>
                        </div>

<br/>
                 <table class="table table-bordered table-condensed">
                     
                          <thead>
                            <tr>
                              <th>S/N</th>
                              <th width="250px">CANDIDATE'S NAME IN BLOCK LETTERS (Surname First)</th>
                              <th width="150px">D.O.B</th>
                              <th>SEX</th>
                              <th>EXAM NUMBER</th>
                              <?php
                               $coldata = $this->db->where('edcid', $edc_detail->edcid)->get('t_printlayout')->result();
                               if(count($exam_subjects)){
                                   foreach ($exam_subjects as $subject){
                                       if(count($coldata)){
                                            foreach ($coldata as $col){
                                                $found = false;
                                                if($subject->subjectid == $col->subjectid){
                                                    echo '<th colspan="' . $col->numofcols .'">' . ucwords(strtolower($subject->subjectname)) . '</th>';
                                                    $found = true;
                                                    break;
                                                }
                                            }
                                            
                                            if(!$found) echo '<th>' . ucwords(strtolower($subject->subjectname)) . '</th>';
                                       }
                                       else echo '<th>' . ucwords(strtolower($subject->subjectname)) . '</th>';
                                   }
                                }
                               
                              ?>
                            </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td>&nbsp;</td>
                                  <td width="250px">&nbsp;</td>
                                  <td width="150px">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                   <?php
                                   $tcols = 0;
                                    if(count($exam_subjects)){
                                        foreach ($exam_subjects as $subject){
                                            
                                            if(count($coldata)){
                                                foreach ($coldata as $col){
                                                     $found = false;
                                                    if($subject->subjectid == $col->subjectid){
                                                        $cols = $col->numofcols;
                                                        $tcols += $cols;

                                                        for ($i = 0; $i < $cols; $i++) {
                                                            echo '<td align="right" style="padding: 0;">'
                                                                . '<p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">'
                                                                    . '<input type="text" value="CA. CUM." style="width: 80px; border: 0; font-weight: bold" border="0" />'
                                                                    //.'CA. CUM.'
                                                                . '</p>'
                                                           . '</td>';
                                                        }
                                                        
                                                        $found = true;
                                                        break;
                                                        
                                                    } 
                                                }
                                                
                                                 if(!$found) echo '<td align="right" style="padding: 0;">'
                                                            . '<p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">'
                                                                //. '<input type="text" value="CA. CUM. AV." style="width: 60px; border: 0; font-weight: bold" border="0" />'
                                                                .'CA. CUM.'
                                                            . '</p>'
                                                       . '</td>';
                                                 
                                            }else{
                                                echo '<td align="right" style="padding: 0;">'
                                                    . '<p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">'
                                                        //. '<input type="text" value="CA. CUM. AV." style="width: 60px; border: 0; font-weight: bold" border="0" />'
                                                        .'CA. CUM.'
                                                    . '</p>'
                                               . '</td>';
                                            }
                                             
                                        }
                                              
                                     }

                                   ?>
                              </tr>


                               <?php 
                                if(count($exam_candidates)): 
                                    $sn = 0;
                                    foreach ($exam_candidates as $registrant):
                                        $name = strtoupper($registrant->firstname) . ' ' . strtoupper($registrant->othernames);
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td width="250px"><?= $name ?></td>
                                    <td width="150px"><?= $registrant->dob; ?></td>
                                    <td><?= strtoupper($registrant->gender); ?></td>
                                    <td><?= $registrant->examno; ?></td>
                                     <?php
                                     $tcols = 0;
                                    if(count($exam_subjects)){
                                        foreach ($exam_subjects as $subject){
                                            
                                            if(count($coldata)){
                                                foreach ($coldata as $col){
                                                    $found = false;
                                                    if($subject->subjectid == $col->subjectid){
                                                        $cols = $col->numofcols;
                                                        $tcols += $cols;

                                                        for ($i = 0; $i < $cols; $i++) {
                                                            echo '<td>&nbsp;</td>';
                                                        }
                                                        
                                                         $found = true;
                                                         break;
                                                         
                                                    }
                                                }
                                                
                                                 if(!$found) echo '<td>&nbsp;</td>';
                                            }else{
                                                 echo '<td>&nbsp;</td>';
                                            }
                                            
                                        }
                                     }
                                     
//                                     for ($i = 0; $i < $tcols; $i++) {
//                                         echo '<td>&nbsp;</td>';
//                                    }
                                   ?>
                                </tr> 
                                    <?php endforeach;?>
                                <?php endif; ?>
                          </tbody>
                        </table>        
                 
                    <table border="0">
                        <tr>
                            <td>Name of School Head:</td>
                            <td width="600px">_____________________________________</td>
                            <td>Phone No.</td>
                            <td>________________</td>
                        </tr>
                    </table>
              </div>  
    <?php 
         

           