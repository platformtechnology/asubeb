﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?= $edcname ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/css/bootstrap.min.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('resources/css/style.css'); ?>" rel="stylesheet"/>

    <?= $page_level_styles; ?>
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->

    <style type="text/css">
        .css-vertical{
            filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                 -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
             -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
              -ms-transform: rotate(-90.0deg);  /* IE9+ */
               -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
          -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                  transform: rotate(-90.0deg);  /* Standard */
                  white-space:nowrap;
        }
    </style>

    <script lang="javascript" type="text/javascript">
        window.addEventListener('load', function () {
            var rotates = document.getElementsByClassName('css-vertical');
            for (var i = 0; i < rotates.length; i++) {
                rotates[i].style.height = rotates[i].offsetWidth + 'px';
            }
        });
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
    </script>

  </head>
  <body data-offset="40" style="background-color: #fff;">
  <?php
    if (isset($school_detail) && count($school_detail)){
        foreach ($school_detail as $schools):
//        $this->db->where('schoolid', $schools->schoolid)
//                            ->where('examid', $examid)
//                            ->where('examyear', $examyear)
//                            ->where('edcid', config_item('edcid'))
//                            ->order_by('examno', 'asc');
        $sql = "select t_candidates.* from t_candidates "
                . "inner join t_pins on t_pins.candidateid = t_candidates.candidateid "
                . "where t_candidates.schoolid = '" . $schools->schoolid . "' "
                . "and t_candidates.edcid = '" . config_item('edcid') . "' "
                . "and t_candidates.examid = '" . $examid . "' "
                . "and t_candidates.examyear = '" . $examyear . "' "
                . "order by examno asc ";

       $exam_candidates = $this->db->query($sql)->result();
//       echo strtoupper($schools->schoolname); //. " - " . count($exam_candidates)."<br/>";
//       continue;
      if(count($exam_candidates)):
  ?>
        <header class="header" style="background-color: #0081c2;">
        <div class="container">
            <div class="row">
                <h4 class="span6">
                    <?= get_edc_logo($edclogo, $edcname); ?>
                </h4>
            </div>
        </div>
    </header>
    <div class="shadow-top"></div>
         <div id="printdiv">

                <div class="row">
                    <div class="span12">
                        <strong>SCHOOL:</strong> <span style="text-decoration: underline"><?=$schools->schoolname; ?></span>
                        <?php if(!$exam_detail->haszone){ ?>| <strong>L.G.A:</strong> <?= $this->school_model->get_lga($schools->lgaid); ?> <?php } ?>
                        | <strong>ZONE:</strong> <?= $this->db->where('zoneid', $schools->zoneid)->get('t_zones')->row()->zonename; ?>
                        <br/>
                        <strong>SCHOOL CODE = <?=$schools->schoolcode; ?></strong>
                    </div>
                </div>

                <br/>
                 <table class="table table-bordered table-condensed">

                          <thead>
                            <tr>
                              <th>S/N</th>
                              <th width="250px">CANDIDATE'S NAME IN BLOCK LETTERS (Surname First)</th>
                              <th width="150px">D.O.B</th>
                              <th>SEX</th>
                              <th>EXAM NUMBER</th>
                              <th colspan="2">Cultural And Creative Arts</th>
                              <th>French</th>
                              <th>Business Studies</th>
                              <th colspan="3">Pre-vocational Studies</th>
                              <th colspan="2">Basic Science And Technology</th>
                              <?php
                               $coldata = $this->db->where('edcid', $edc_detail->edcid)->get('t_printlayout')->result();
//                               if(count($exam_subjects)){
//                                   foreach ($exam_subjects as $subject){
//                                       if(count($coldata)){
//                                            foreach ($coldata as $col){
//                                                $found = false;
//                                                if($subject->subjectid == $col->subjectid){
//                                                    echo '<th colspan="' . $col->numofcols .'">' . ucwords(strtolower($subject->subjectname)) . '</th>';
//                                                    $found = true;
//                                                    break;
//                                                }
//                                            }
//
//                                            if(!$found) echo '<th>' . ucwords(strtolower($subject->subjectname)) . '</th>';
//                                       }
//                                       else echo '<th>' . ucwords(strtolower($subject->subjectname)) . '</th>';
//                                   }
//                                }
                              ?>
                            </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td>&nbsp;</td>
                                  <td width="250px">&nbsp;</td>
                                  <td width="150px">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                         LOCAL CRAFTS
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                          MUSIC
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                          &nbsp;
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                         &nbsp;
                                      </p>
                                  </td>

                                   <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                         AGRIC.
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                          HOME ECONS.
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                          ENTERPRENEURSHIP.
                                      </p>
                                  </td>

                                   <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                         INFO TECH
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                          BASIC TECH
                                      </p>
                                  </td>

                              </tr>


                               <?php
                                if(count($exam_candidates)):
                                    $sn = 0;
                                    foreach ($exam_candidates as $registrant):
                                        $name = strtoupper($registrant->firstname) . ' ' . strtoupper($registrant->othernames);
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td width="250px"><?= $name ?></td>
                                    <td width="150px"><?= $registrant->dob; ?></td>
                                    <td><?= strtoupper($registrant->gender); ?></td>
                                    <td><?= $registrant->examno; ?></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>

                                </tr>
                                    <?php endforeach;?>
                                <?php endif; ?>
                          </tbody>
                        </table>

                    <table border="0">
                        <tr>
                            <td>Name of School Head:</td>
                            <td width="600px">_____________________________________</td>
                            <td>Phone No.</td>
                            <td>________________</td>
                        </tr>
                    </table>
              </div>

         <p style="page-break-before: always;"></p>

 <?php
                    endif;
        endforeach;
    }else{

        echo form_open('', 'class="form-horizontal" role="form" target="_blank"');

        echo '<br/>';echo '<br/>';
        echo "School: ";

       echo  '<div class="bfh-selectbox" data-name="schoolid" data-value="'. ($this->input->post('schoolid') ? $this->input->post('schoolid') : '2') .'" data-filter="true">';

                          //echo '<div data-value="2">----- All Schools-----</div>';
                            $schools_option = array();
                          foreach ($schools as $lg) {
                             $schools_option[$lg->schoolid] = strtoupper($lg->schoolname);
                          }
                          echo form_dropdown('schoolid',$schools_option,set_value('schoolid'),'class = "form-control');

        echo '</div>';


         echo '<br/>';echo '<br/>';
         echo "Exam: ";
            $select_options = array();
          $select_options[''] = "------ Select Exam ------";
            foreach ($examx as $exam) {
                $select_options[$exam->examid] = $exam->examname;
            }
         echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'id="examid" class="input-xlarge" required="required" ');

           echo '<br/>';echo '<br/>';
         echo "Year: ";
         $already_selected_value = $this->input->post('examyear') ? $this->input->post('examyear') : $activeyear;
        $start_year = $activeyear;
         print '<select name="examyear">';
         print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>Select Exam Year</option>';
         for ($x = $start_year; $x <= $activeyear; $x++) {
             print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
         }
         print '</select>';

         echo '<br/>';echo '<br/>';
         echo '<button type="submit" class="btn btn-primary"><i class="icon-print icon-white"></i> Print</button>';
        echo form_close();
    }
  ?>
          <script src="<?php echo base_url('resources/js/jquery-1.8.3.min.js'); ?>"></script>
    <script src="<?php echo base_url('resources/js/bootstrap.min.js'); ?>"></script>
         <?= $page_level_scripts; ?>
  </body>
</html>
