<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?= $edcname ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="<?php echo base_url('resources/css/bootstrap.min.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('resources/css/style.css'); ?>" rel="stylesheet"/>

    <?= $page_level_styles; ?>
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <script lang="javascript" type="text/javascript">
               function printn() {
                    window.print();
                    var target = "<?= site_url('schools/candidates'); ?>";
                    window.location = target;
               }

    </script>
    <style type="text/css">
    .vertical
    {
        -webkit-transform:rotate(-90deg);
        border-style: solid;
        border-width: 1px;
        border-color: #f7eaea;
        height:110px; 

}
    </style>
  </head>
  <body data-offset="40" onload="" style="background-color: #fff;">
    
    <div class="shadow-top"></div>
    <div class="container">
    <div class="row">
            <div class="span12">
                <div class="page-header">
<!--                    <h1 class="span6"><a href="#" title="Examination Development Centre"><img src="<?=$edclogo;?>" alt="Logo"/></a></h1>-->
                    <h4 style="text-align: center; color: #0077b3;">
                        <?= $edcname . ' ' . $this->db->where('zoneid', $school_detail->zoneid)->get('t_zones')->row()->zonename; ?><BR/>
                        <?= strtoupper($exam_detail->examdesc. ' (' . $exam_detail->examname . ') ' . $this->session->userdata('examyear')) ;?> <BR/>
                        CANDIDATES INFORMATION
                    </h4>
                    <strong>SCHOOL:</strong> <?=$school_detail->schoolname; ?> | <strong>L.G.A:</strong> <?=$this->db->where('lgaid', $school_detail->lgaid)->get('t_lgas')->row()->lganame; ?>
                    | <strong>ZONE:</strong> <?= $this->db->where('zoneid', $school_detail->zoneid)->get('t_zones')->row()->zonename; ?>
                    <br/>
                    <strong>CA = CA. CUM. AV.</strong> 
                </div>
            </div>
    </div>
<div class="row">
    <div class="span12">
       <table class="table table-bordered table-condensed">
          <thead>
            <tr>
              <th>S/N</th>
              <th width="250px">CANDIDATE'S NAME IN BLOCK LETTERS (Surname First)</th>
              <th width="100px">D.O.B</th>
              <th>SEX</th>
              <th>EXAM NUMBER</th>
              <?php
               if(count($exam_subjects)){
                   foreach ($exam_subjects as $subject){
                       echo '<th>' . $subject->subjectname . '</th>';
                   }
                }
              ?>
            </tr>
          </thead>
          <tbody>
              <tr>
                  <td>&nbsp;</td>
                  <td width="250px">&nbsp;</td>
                  <td width="100px">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                   <?php
                    if(count($exam_subjects)){
                        foreach ($exam_subjects as $subject){
                            echo '<td class=""><strong>CA.</strong></td>';
                        }
                     }
                   ?>
              </tr>
              
              
               <?php 
                if(count($exam_candidates)): 
                    $sn = 0;
                    foreach ($exam_candidates as $registrant):
                        $name = strtoupper($registrant->firstname) . ' ' . strtoupper($registrant->othernames);
                ?>
                <tr>
                    <td><?= ++$sn; ?></td>
                    <td width="250px"><?= $name ?></td>
                    <td width="100px"><?= $registrant->dob; ?></td>
                    <td><?= strtoupper($registrant->gender); ?></td>
                    <td><?= $registrant->examno; ?></td>
                     <?php
                    if(count($exam_subjects)){
                        foreach ($exam_subjects as $subject){
                            echo '<td>&nbsp;</td>';
                        }
                     }
                   ?>
                </tr> 
                <?php endforeach;?>
                <?php endif; ?>
          </tbody>
        </table>
    </div>
 
  </div>
</div>
    
 </body>
</html>