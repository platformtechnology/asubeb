<?php

if (count($school_data))
{
    ?>
<div class="row">
    <div class="span12 well" style="text-align: center;">
         <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
        <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
        <?= form_open('', 'class="form-horizontal"'); ?>
        <table width="50%" class="table table-bordered" align="center" >
               <tr>
                   <td style="text-align: center; color: #0077b3;" colspan="2">
                       <h4> Admin Number Found</h4>
                   </td>
               </tr>

               <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>School Name</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                         <?php echo $school_data->schoolname ?>
                  </td>
                </tr>
               <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>School Code</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                         <?php echo $school_data->schoolcode ?>
                  </td>
                </tr>
               <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>Admin Number</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <h1>  <?php echo $school_data->phone ?> <h1>
                  </td>
                </tr>
              </table>
    </div>
</div>

<?php
}
else {
redirect('Web/Schools');
}
?>

