
<div class="container">
  <div class="row">
      <div class="span12">
          <div class="page-header">
                <h3 style="text-align: center; color: #0077b3;">
                    <a href="#" title="Examination Development Centre"><?= $this->session->userdata('school_name'); ?> <!--<img src="<?=$edclogo;?>" alt="Logo"/>--></a><br/>
                </h3>
            </div>


         <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
        <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
        <?= form_open(site_url('web/schools/filter'), 'class="form-horizontal" role="form" target="_blank"'); ?>
          <h4><i class="icon-search"></i> Search For Candidates</h4>
          

          <table width="50%" border="0" align="center" class="table table-bordered" >
                <tr>
                    <td align="right" valign="top" style="text-align: right;"><strong>Exam Type:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                           <?php
                              $select_options = array();
                              foreach ($exams as $exam) {
                                  $select_options[$exam->examid] = $exam->examname;
                              }
                              $select_options[''] = "--- Select Exam ---";
                              echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'required = "required" class="input-xlarge"');
                            ?>

                     </label>
                  </td>
                  <td align="right" valign="top" style="text-align: right;"><strong>Exam Year:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                           <?php
                            $already_selected_value = $this->input->post('examyear') ? $this->input->post('examyear') : $activeyear;
                            $start_year = $startyear;
                             print '<select name="examyear" class="input-xlarge" required = "required" >';
                             print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>..::All Year::..</option>';
                             for ($x = $start_year; $x <= $activeyear; $x++) {
                                 print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                             }
                             print '</select>';
                           ?>

                     </label>
                  </td>
                  <td><strong>Sheet Type</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                           <?php
                            $options = array('CA'=>'CA Sheet','Practical'=>'Practical Sheet');
                            echo form_dropdown('sheet_type',$options,set_value('sheet_type','CA'),'class = "form-control" required = "required" ');
                           ?>

                     </label>
                  </td>
                </tr>

                <tr>
                    <td style="text-align: center;" colspan="6" valign="top">

                      <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Search</button>
                      <a href="<?  site_url('web/schools/candidates');?>" class="btn btn-warning"><i class="icon-refresh icon-white"></i> Refresh</a>

                    </td>
                </tr>

              </table>

         <?= form_close(); ?>

      </div>
  </div>
</div>


  <div class="row">
	<div class="span12">

            <div class="caption">
                 <h4>Candidates <span style="font-size: 13px;">(Total Candidates: <?= $count; ?>)</span></h4>

                 <?php if(count($registrants)): ?>

                 <strong><?= $title; ?></strong>
                 <hr/>
                  <?= form_open(site_url('web/schools/export_to_excel'), 'class="form-horizontal" role="form"'); ?>
                    <button type="submit" class="btn btn-default"><img src="<?= get_img('excel_icon.png');?>" width="20px" height="20px" border="0" /><strong> Export To Excel</strong></button>
<!--                    <a href="" class="btn btn-default"><i class="icon-print"></i> <strong>Print</strong></a>-->
                 <?= form_close(); ?>
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('u_msg')); ?>

                    <table class="table table-bordered table-condensed">
                      <thead>
                        <tr>
                          <th>SN</th>
                          <th>Exam No</th>
                          <th>Candidate Name</th>
                          <th>Gender</th>
                          <th>Exam Type</th>
                          <th>Exam Year</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php
                            if(count($registrants)):
                                $sn = 0;
                                foreach ($registrants as $registrant):
                                $name = ucfirst(strtolower($registrant->firstname)) . ' ' . ucfirst(strtolower($registrant->othernames));
                            ?>
                            <tr>
                                <td><?= ++$sn; ?></td>
                                <td><?= $registrant->examno; ?></td>
                                <td><?php echo anchor(site_url('schools/view/' . $registrant->candidateid), $name, 'title="View Details"'); ?></td>
                                <td><?= $registrant->gender == 'M' ? 'Male' : 'Female'; ?></td>
                                <td><?= $this->registration_model->getExamName_From_Id($registrant->examid); ?></td>
                                <td><?= $registrant->examyear; ?></td>
                                <td>
                                    <?= anchor(site_url('web/schools/view/' . $registrant->candidateid), '<i class="icon-eye-open"></i>', 'title="View Details"'); ?>
                                    <?= anchor(site_url('web/schools/view/' . $registrant->candidateid.'/print'), '<i class="icon-print"></i>', 'title="Print Details"'); ?>

                                </td>
                            </tr>
                            <?php endforeach;?>
                            <?php endif; ?>
                      </tbody>
                    </table>

                 <?php echo $this->pagination->create_links();?>

                 <?php endif; ?>
            </div>
	</div>
</div>

<script>

function setColCookie(){

    subject = document.getElementById('subj');
    numofCols = document.getElementById('divnum').value;
    if(numofCols > 1){
        createCookie(subject.value, numofCols);
        alert("Done");
    }else{
        var date = new Date();
        date.setTime(date.getTime()-(60*1000));
        document.cookie = subject.value+"="+numofCols+"; expires="+date.toGMTString() + "; path=/";
        alert("Done");
    }
}

function createCookie(name, value) {
   var date = new Date();
   date.setTime(date.getTime()+(60*1000));
   var expires = "; expires="+date.toGMTString();

   document.cookie = name+"="+value+expires+"; path=/";
}
</script>