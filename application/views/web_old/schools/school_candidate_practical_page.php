<script lang="javascript" type="text/javascript">
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;

            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
    </script>
        <button class ="btn btn-primary"onclick ="printDiv('printdiv')"> <span class = "glyphicon glyphicon-print"> </span> Print </button>
         <div id="printdiv">
                <div class="container" align = "center">
                    <div class="span12" style = "margin-left:20px">
                        <center> <h2> <?php echo $edcname ?> </h2>
                            <h4> Score Sheet for Practical Subjects </h4>
                        </center>
                    </div>
                </div>

                        <div class="row">
                            <div class="span12">
                                <strong>SCHOOL:</strong> <span style="text-decoration: underline"><?=$school_detail->schoolname; ?></span>
                                <?php if(!$exam_detail->haszone){ ?>| <strong>L.G.A:</strong> <?= $this->school_model->get_lga($school_detail->lgaid); ?> <?php } ?>
                                | <strong>ZONE:</strong> <?= $this->db->where('zoneid', $school_detail->zoneid)->get('t_zones')->row()->zonename; ?>
                                <br/>
                                <strong>SCHOOL CODE = <?=$school_detail->schoolcode; ?></strong>
                                  <br/>
                                <strong>EXAM NAME = <?=$exam_detail->examname; ?></strong>
                                  <br/>
                                <strong>EXAM YEAR = <?=$examyear; ?></strong>
                            </div>
                        </div>

<br/>
                 <table class="table table-bordered table-condensed">

                          <thead>
                            <tr>
                              <th>S/N</th>
                              <th width="250px">CANDIDATE'S NAME IN BLOCK LETTERS (Surname First)</th>

                              <th>SEX</th>
                              <th>EXAM NUMBER</th>
                              <th>Basic Science And Technology</th>
                              <th>Business Studies</th>
                              <th>Cultural And Creative Arts</th>
                              <th>French</th>
                              <th>Pre-vocational Studies</th>

                             </tr>
                          </thead>
                          <tbody>

                               <?php
                                if(count($exam_candidates)):
                                    $sn = 0;
                                    foreach ($exam_candidates as $registrant):
                                        $name = strtoupper($registrant->firstname) . ' ' . strtoupper($registrant->othernames);
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td width="250px"><?= $name ?></td>
                                    <td><?= strtoupper($registrant->gender); ?></td>
                                    <td><?= $registrant->examno; ?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                    <?php endforeach;?>
                                <?php endif; ?>
                          </tbody>
                        </table>

                    <table border="0">
                        <tr>
                            <td>Name of School Head:</td>
                            <td width="600px">_____________________________________</td>
                            <td>Phone No.</td>
                            <td>________________</td>
                        </tr>
                    </table>
              </div>
    <?php


