
    <p>
            <a href="<?=  site_url('web/schools/candidates');?>" class="btn btn-primary"><i class="icon-arrow-left icon-white"></i> Back</a> 
            <button type="button" onclick="printDiv('printdivx')" class="btn btn-info"><i class="icon-print icon-white"></i> Print</button>
        </p>
<div id="printdivx">
<div class="row">
	<div class="span12">
            <div class="page-header">
                <h3 style="text-align: center; color: #0077b3;">
                    <?= get_edc_logo($edclogo, $edcname) ?>
                </h3>
            </div>
	</div>
</div>
<div class="row">
    <div class="span12">
        
       <table class="table table-bordered" width="100%">
           <tr>
                  <td><strong>Registered Exam:</strong></td>
                  <td><strong><?= $exam_detail->examname; ?></strong></td>
                  <td rowspan="4" valign="top">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="fileupload-new thumbnail" style="width: 150px; height: 112px;"><img src="<?= $passport; ?>" alt="Passport" /></div>
                       <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                    </div>              
                  </td>
                </tr>
                <tr>
                  <td><strong>Exam Year:</strong></td>
                  <td><strong><?= $reg_info->examyear; ?></strong></td>
                </tr> 
                <tr>
                  <td>&nbsp;</td>
                 <td>&nbsp;</td>
                </tr> 
           <tr>
                <?php
                    if($exam_detail->haszone){
                   ?>
                       <td><strong>Zone:</strong></td>
                       <td><?= $this->registration_model->get_zone($reg_info->zoneid); ?> </td>

                   <?php
                        }
                        else
                        {
                    ?>
                        <td><strong>Local Govt Area:</strong></td>
                        <td><?= $this->registration_model->get_lga($reg_info->lgaid); ?> </td>
                    <?php
                        }
                    ?>  
             
            </tr>
            <tr>
             <td><strong>School:</strong></td>
              <td><?= $this->registration_model->get_school($reg_info->schoolid); ?> </td>
               <td>&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Exam Centre:</strong></td>
              <td><?= $this->registration_model->get_school($reg_info->centreid); ?> </td>
               <td>&nbsp;</td>  
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td align="center" valign="top"><strong>ExamNo: <span style="color:crimson;"><?= $reg_info->examno; ?></span></strong></td>
               <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
               <td>&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Firstname:</strong></td>
              <td><?= $reg_info->firstname; ?></td>
               <td>&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Othernames:</strong></td>
              <td><?= $reg_info->othernames; ?></td>
              <td valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Gender:</strong></td>
              <td><?= ($reg_info->gender == 'M' ? 'Male' : 'Female') ;?></td>
              <td valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Dob:</strong></td>
              <td><?= $reg_info->dob; ?></td>
              <td valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Phone:</strong></td>
              <td><?= $reg_info->phone; ?></td>
              <td valign="top">&nbsp;</td>
            </tr>
            <tr>
             <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td valign="top">&nbsp;</td>
            </tr>
            
            <tr>
            <td><strong>Reg. Subjects:</strong></td>
            <td>
                  <?php                 
                    if(count($subjects)):
                        $sn = 0;
                        foreach ($subjects as $subject) {
                            echo ++$sn .'. <strong>'.$this->registration_model->get_subject($subject->subjectid);
                                if($subject->armid != '0'){
                                    $armdata = $this->db->get_where('t_subjects_arms', array('armid' => $subject->armid))->row();
                                    echo ' (' . ucwords(strtolower($armdata->armname)) . ') ';
                                }
                            echo ' </strong><br/> ';
                        }
                    endif;
                   ?>
              </td>
              <td valign="top">&nbsp;</td>
            </tr>
            
            <tr>
             <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td valign="top">&nbsp;  </td>
            </tr>
             <?php 
                if($exam_detail->hasposting == 1):
              ?>
            <tr>
              <td><strong>First Choice:</strong></td>
              <td><?= $this->registration_model->get_school($reg_info->firstchoice); ?> </td>
              <td valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Second Choice:</strong></td>
              <td><?= $this->registration_model->get_school($reg_info->secondchoice); ?> </td>
              <td valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td valign="top">&nbsp;</td>
            </tr>
            <?php
                endif;
              ?>
          </table>
        </div>
 
</div>

</div>
 <div class="row">
     <div class="span12">
     <p>
      <a href="<?=  site_url('web/schools/candidates');?>" class="btn btn-primary"><i class="icon-arrow-left icon-white"></i> Back</a> 
      <button type="button" onclick="printDiv('printdivx')" class="btn btn-info"><i class="icon-print icon-white"></i> Print</button>
  </p>
  </div>   
</div>   