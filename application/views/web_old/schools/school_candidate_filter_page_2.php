﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?= $edcname ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="<?php echo base_url('resources/css/bootstrap.min.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('resources/css/style.css'); ?>" rel="stylesheet"/>

    <?= $page_level_styles; ?>
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    
    <style type="text/css">
        .css-vertical{
            filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                 -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
             -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
              -ms-transform: rotate(-90.0deg);  /* IE9+ */
               -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
          -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                  transform: rotate(-90.0deg);  /* Standard */
                  white-space:nowrap;
        }
    </style>
    
    <script lang="javascript" type="text/javascript">
        window.addEventListener('load', function () {
            var rotates = document.getElementsByClassName('css-vertical');
            for (var i = 0; i < rotates.length; i++) {
                rotates[i].style.height = rotates[i].offsetWidth + 'px';
            }
        });
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
    </script>

  </head>
  <body data-offset="40" style="background-color: #fff;">
  <?php 
    if (isset($school_detail) && count($school_detail)){
       
        foreach ($school_detail as $schools): 
//        $this->db->where('schoolid', $schools->schoolid)
//                            ->where('examid', $examid)
//                            ->where('examyear', $examyear)
//                            ->where('edcid', config_item('edcid'))
//                            ->order_by('examno', 'asc');
        $sql = "select t_candidates.* from t_candidates "
                . "where t_candidates.schoolid = '" . $schools->schoolid . "' "
                . "and t_candidates.edcid = '" . config_item('edcid') . "' " 
                . "and t_candidates.examid = '" . $examid . "' " 
                . "and t_candidates.examyear = '" . $examyear . "' " 
                . "order by t_candidates.lgaid, t_candidates.examno ";
    
       $exam_candidates = $this->db->query($sql)->result();
//       echo strtoupper($schools->lganame) . " - " . count($exam_candidates)."<br/>";
//       continue;
      if(count($exam_candidates)):
  ?>      
        <header class="header" style="background-color: #0081c2;">
        <div class="container">
            <div class="row">
                <h4 class="span6">
                    <?= get_edc_logo($edclogo, $edcname); ?>
                </h4>
            </div>
        </div>
    </header>
    <div class="shadow-top"></div>
         <div id="printdiv">
                        
                <div class="row">
                    <div class="span12">
                        <strong><?= $title; ?></strong><br/>
                        <strong>SCHOOL:</strong> <span style="text-decoration: underline"><?= strtoupper($schools->schoolname); ?></span> 
                        <?php if(!$exam_detail->haszone){ ?>| <strong>L.G.A:</strong> <?= $this->school_model->get_lga($schools->lgaid); ?> <?php } ?>
                        | <strong>ZONE:</strong> <?= $this->db->where('zoneid', $schools->zoneid)->get('t_zones')->row()->zonename; ?>
                        <br/>
                        <strong>SCHOOL CODE = <?=$schools->schoolcode; ?></strong> 
                    </div>
                </div>

                <br/>
                 <table class="table table-bordered table-condensed">
                     
                          <thead>
                            <tr>
                              <th>S/N</th>
                              <th>EXAM NUMBER</th>
                              <th width="300px">CANDIDATE'S NAME IN BLOCK LETTERS (Surname First)</th>
                              <th>SEX</th>
                              <th>ATTENDANCE</th>
                              <?php if($examid == '2n11sray1c') echo '<th>MARK</th>'; ?>
                            </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td>&nbsp;</td>
                                  <td width="300px">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td> 
                                  <?php if($examid == '2n11sray1c') echo '<td>&nbsp;</td>'; ?>
                                  
                              </tr>


                               <?php 
                                if(count($exam_candidates)): 
                                    $sn = 0;
                                    foreach ($exam_candidates as $registrant):
                                        $name = strtoupper($registrant->firstname) . ' ' . strtoupper($registrant->othernames);
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td><?= $registrant->examno; ?></td>
                                    <td width="250px"><?= $name ?></td>
                                    <td><?= strtoupper($registrant->gender); ?></td>
                                    <td>&nbsp;</td>  
                                    <?php if($examid == '2n11sray1c') echo '<td>&nbsp;</td>'; ?>
                                </tr> 
                                    <?php endforeach;?>
                                <?php endif; ?>
                          </tbody>
                        </table>        
                 
                    <table border="0">
                        <tr>
                            <td>Name of School Head:</td>
                            <td width="600px">_____________________________________</td>
                            <td>Phone No.</td>
                            <td>________________</td>
                        </tr>
                    </table>
              </div>
         
         <p style="page-break-before: always;"></p>

 <?php 
        endif;
     endforeach;
    }else{
        echo '<h3>PRINTING ' . $this->registration_model->getExamName_From_Id($examid) . ' EXAM ATTENDANCE</h3>';
        echo form_open('', 'class="form-horizontal" role="form"'); 
        
        echo '<br/>';echo '<br/>';
        echo "Local Govt.: ";
          $select_options = array();
          $select_options[''] = "------ Select LGA ------";
            foreach ($lgas as $lg) {
                $select_options[$lg->lgaid] = $lg->lganame;
            }
         echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : '', 'id="lgaid" class="input-xlarge" required="required" '); 
         
         echo '<br/>';echo '<br/>';
         
           echo "School: ";
          $select_options = array();
          $select_options[''] = "------ ALL SCHOOLS ------";
            foreach ($schools as $school) {
                $select_options[$school->schoolid] = $school->schoolname;
            }
         echo form_dropdown('schoolid', $select_options, $this->input->post('schoolid') ? $this->input->post('schoolid') : '', 'id="schoolid" class="input-xlarge" '); 
         
         echo '<br/>';echo '<br/>';
         
         echo "Exam: ";
            $select_options = array();
            $select_options[''] = "------ Select Exam ------";
            foreach ($examx as $exam) {
                if($exam->examid == $examid)
                $select_options[$exam->examid] = $exam->examname;
            }
         echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'id="examid" class="input-xlarge" required="required" '); 
         
           echo '<br/>';echo '<br/>';
         echo "Year: ";
         $already_selected_value = $this->input->post('examyear') ? $this->input->post('examyear') : $activeyear;
        $start_year = $activeyear;
         print '<select name="examyear">';
         print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>Select Exam Year</option>';
         for ($x = $start_year; $x <= $activeyear; $x++) {
             print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
         }
         print '</select>';
              
         echo '<br/>';echo '<br/>';
         echo '<button type="submit" class="btn btn-primary"><i class="icon-print icon-white"></i> Print</button>';
        echo form_close();
    }
  ?> 

         
  <script type="text/javascript">
     (function() {
        var httpRequest;
        
        lgaddl = document.getElementById("lgaid");
        
        lgaddl.onchange = function() { 
            var target_url3 = "<?= site_url('schools/school_ajaxdrop?lgaid='); ?>";
            makeRequest(target_url3 + lgaddl.value , 'schoolid'); 
        };
        
        function makeRequest(url, targetid) {
            httpRequest = getHttpObject();

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = function(){
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        //alert(httpRequest.response);
                        var data = JSON.parse(httpRequest.response);
                        var select = document.getElementById(targetid);
                        if(emptySelect(select)){
                            var el = document.createElement("option");
                                    
                            el.textContent = '---- ALL SCHOOLS ----';
                            el.value = '';
                            select.appendChild(el);
                                    
                            for (var i = 0; i < data.lgas.length; i++){
                                var el = document.createElement("option");
                                    el.textContent = data.lgas[i].lganame;
                                    el.value = data.lgas[i].lgaid;
                                    select.appendChild(el);
                            }
                            
                        }
                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
            httpRequest.send();
        }

        function emptySelect(select_object){
            var i;
            for(i=select_object.options.length-1;i>=0;i--){
                select_object.remove(i);
            }
            return true;
        }
        
         function getHttpObject(){

            var xmlhtp;

            if (window.ActiveXObject) 
            {    
                var aVersions = [ 
                  "MSXML2.XMLHttp.9.0","MSXML2.XMLHttp.8.0", "MSXML2.XMLHttp.7.0",
                  "MSXML2.XMLHttp.6.0","MSXML2.XMLHttp.5.0","MSXML2.XMLHttp.4.0",
                  "MSXML2.XMLHttp.3.0","MSXML2.XMLHttp","Microsoft.XMLHttp"
                ];

                for (var i = 0; i < aVersions.length; i++) 
                {
                    try 
                    { 
                        xmlhtp = new ActiveXObject(aVersions[i]);
                        return xmlhtp;
                    } catch (e) {}
                }
            }
            else  if (typeof XMLHttpRequest != 'undefined') 
            {
                    xmlhtp = new XMLHttpRequest();			
                    return xmlhtp;
            } 
            throw new Error("XMLHttp object could be created.");

        }
    })();


    </script>
  </body>
</html>
           