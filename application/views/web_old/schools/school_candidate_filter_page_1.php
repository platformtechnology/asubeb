﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?= $edcname ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="<?php echo base_url('resources/css/bootstrap.min.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('resources/css/style.css'); ?>" rel="stylesheet"/>

    <?= $page_level_styles; ?>
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    
    <style type="text/css">
        .css-vertical{
            filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                 -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
             -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
              -ms-transform: rotate(-90.0deg);  /* IE9+ */
               -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
          -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                  transform: rotate(-90.0deg);  /* Standard */
                  white-space:nowrap;
        }
    </style>
    
    <script lang="javascript" type="text/javascript">
        window.addEventListener('load', function () {
            var rotates = document.getElementsByClassName('css-vertical');
            for (var i = 0; i < rotates.length; i++) {
                rotates[i].style.height = rotates[i].offsetWidth + 'px';
            }
        });
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
    </script>

  </head>
  <body data-offset="40" style="background-color: #fff;">
  <?php 
    if (count($school_detail))
        foreach ($school_detail as $schools): 
        $this->db->where('schoolid', $schools->schoolid)
                            ->where('examid', $examid)
                            ->where('examyear', $examyear)
                            ->where('edcid', config_item('edcid'))
                            ->order_by('examno', 'asc');
       $exam_candidates = $this->registration_model->get_all();
      if(count($exam_candidates)):
  ?>      
        <header class="header" style="background-color: #0081c2;">
        <div class="container">
            <div class="row">
                <h4 class="span6">
                    <?= get_edc_logo($edclogo, $edcname); ?>
                </h4>
            </div>
        </div>
    </header>
    <div class="shadow-top"></div>
         <div id="printdiv">
                        
                <div class="row">
                    <div class="span12">
                        <strong>SCHOOL:</strong> <span style="text-decoration: underline"><?=$schools->schoolname; ?></span> 
                        <?php if(!$exam_detail->haszone){ ?>| <strong>L.G.A:</strong> <?= $this->school_model->get_lga($schools->lgaid); ?> <?php } ?>
                        | <strong>ZONE:</strong> <?= $this->db->where('zoneid', $schools->zoneid)->get('t_zones')->row()->zonename; ?>
                        <br/>
                        <strong>SCHOOL CODE = <?=$schools->schoolcode; ?></strong> 
                    </div>
                </div>

                <br/>
                 <table class="table table-bordered table-condensed">
                     
                          <thead>
                            <tr>
                              <th>S/N</th>
                              <th width="250px">CANDIDATE'S NAME IN BLOCK LETTERS (Surname First)</th>
                              <th width="150px">D.O.B</th>
                              <th>SEX</th>
                              <th>EXAM NUMBER</th>
                              <?php
                               $coldata = $this->db->where('edcid', $edc_detail->edcid)->get('t_printlayout')->result();
                               if(count($exam_subjects)){
                                   foreach ($exam_subjects as $subject){
                                       if(count($coldata)){
                                            foreach ($coldata as $col){
                                                $found = false;
                                                if($subject->subjectid == $col->subjectid){
                                                    echo '<th colspan="' . $col->numofcols .'">' . ucwords(strtolower($subject->subjectname)) . '</th>';
                                                    $found = true;
                                                    break;
                                                }
                                            }
                                            
                                            if(!$found) echo '<th>' . ucwords(strtolower($subject->subjectname)) . '</th>';
                                       }
                                       else echo '<th>' . ucwords(strtolower($subject->subjectname)) . '</th>';
                                   }
                                }
                              ?>
                            </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td>&nbsp;</td>
                                  <td width="250px">&nbsp;</td>
                                  <td width="150px">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                   <?php
                                   $tcols = 0;
                                    if(count($exam_subjects)){
                                        foreach ($exam_subjects as $subject){
                                            
                                            if(count($coldata)){
                                                foreach ($coldata as $col){
                                                     $found = false;
                                                    if($subject->subjectid == $col->subjectid){
                                                        $cols = $col->numofcols;
                                                        $tcols += $cols;

                                                        for ($i = 0; $i < $cols; $i++) {
                                                            echo '<td align="right" style="padding: 0;">'
                                                                . '<p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">'
                                                                    . '<input type="text" value="CA. CUM." style="width: 80px; border: 0; font-weight: bold" border="0" />'
                                                                    //.'CA. CUM.'
                                                                . '</p>'
                                                           . '</td>';
                                                        }
                                                        
                                                        $found = true;
                                                        break;
                                                        
                                                    } 
                                                }
                                                
                                                 if(!$found) echo '<td align="right" style="padding: 0;">'
                                                            . '<p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">'
                                                                //. '<input type="text" value="CA. CUM. AV." style="width: 60px; border: 0; font-weight: bold" border="0" />'
                                                                .'CA. CUM.'
                                                            . '</p>'
                                                       . '</td>';
                                                 
                                            }else{
                                                echo '<td align="right" width="150px" style="padding: 0;">'
                                                    . '<p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">'
                                                        //. '<input type="text" value="CA. CUM. AV." style="width: 60px; border: 0; font-weight: bold" border="0" />'
                                                        .'CA. CUM.'
                                                    . '</p>'
                                               . '</td>';
                                            }
                                             
                                        }
                                              
                                     }
                                     
                                   ?>
                              </tr>


                               <?php 
                                if(count($exam_candidates)): 
                                    $sn = 0;
                                    foreach ($exam_candidates as $registrant):
                                        $name = strtoupper($registrant->firstname) . ' ' . strtoupper($registrant->othernames);
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td width="250px"><?= $name ?></td>
                                    <td width="150px"><?= $registrant->dob; ?></td>
                                    <td><?= strtoupper($registrant->gender); ?></td>
                                    <td><?= $registrant->examno; ?></td>
                                     <?php
                                     $tcols = 0;
                                    if(count($exam_subjects)){
                                        foreach ($exam_subjects as $subject){
                                            
                                            if(count($coldata)){
                                                foreach ($coldata as $col){
                                                    $found = false;
                                                    if($subject->subjectid == $col->subjectid){
                                                        $cols = $col->numofcols;
                                                        $tcols += $cols;

                                                        for ($i = 0; $i < $cols; $i++) {
                                                            echo '<td>&nbsp;</td>';
                                                        }
                                                        
                                                         $found = true;
                                                         break;
                                                         
                                                    }
                                                }
                                                
                                                 if(!$found) echo '<td>&nbsp;</td>';
                                            }else{
                                                 echo '<td>&nbsp;</td>';
                                            }
                                            
                                        }
                                     }
                                     
//                                     for ($i = 0; $i < $tcols; $i++) {
//                                         echo '<td>&nbsp;</td>';
//                                    }
                                   ?>
                                </tr> 
                                    <?php endforeach;?>
                                <?php endif; ?>
                          </tbody>
                        </table>        
                 
                    <table border="0">
                        <tr>
                            <td>Name of School Head:</td>
                            <td width="600px">_____________________________________</td>
                            <td>Phone No.</td>
                            <td>________________</td>
                        </tr>
                    </table>
              </div>
         
         <p style="page-break-before: always;"></p>

 <?php 
                    endif;
        endforeach;
  ?> 
  </body>
</html>
           