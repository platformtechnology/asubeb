<div class="row">
    <div class="span12 well" style="text-align: center;">
         <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
        <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
        <?= form_open('', 'class="form-horizontal"'); ?>
           <table width="50%" border="0" align="center" >
               <tr>
                   <td style="text-align: center; color: #0077b3;" colspan="2">
                       <h4>School Login</h4>
                   </td>
               </tr>
               <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>Select Lga/Zone:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                          <?php
                            $select_options = array();
                            $select_options[''] = "-----------------Lgas-----------------";
                              foreach ($lgs as $lg) {
                                  $select_options[$lg->lgaid] = strtoupper($lg->lganame);
                              }
                              $select_options['0'] = "--------------Zones-----------------";
                              foreach ($zones as $zone) {
                                  $select_options[$zone->zoneid] = strtoupper($zone->zonename);
                              }
                            echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : '', 'id="lgaid" class="input-xlarge" required="required"');
                        ?>
                     </label>
                  </td>
                </tr>
                 <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>School Code:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                          <input type="text" name="schoolcode" placeholder="Enter Your School Code" value="<?php echo set_value('schoolcode'); ?>" class="input-xlarge" required="required" />
                     </label>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>Genre:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                          <select name="genre" id="genre">
                              <option selected="true" value="isprimary" class="input-xlarge">Primary</option>
                              <option value="issecondary" class="input-xlarge">Secondary</option>
                          </select>
                     </label>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>Pass Code:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                          <input type="password" name="passcode" placeholder="Enter Authentication Code" value="<?php echo set_value('passcode'); ?>" class="input-xlarge" required="required" />
                     </label>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>Admin Phone:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                          <input type="text" name="phone" placeholder="Enter Admin's Phone" value="<?php echo set_value('phone'); ?>" class="input-xlarge" required="required" />
                     </label>
                      <a  target="_blank" href="<?php echo site_url('web/schools/confirm_admin_number')?>"<h4>Click Here to Confirm Admin Number</h4> </a>
                  </td>
                </tr>
                <tr>
                    <td align="right" valign="top"><a target="_blank" href="<?=  site_url('web/schools/confirm_admin_number');?>" class="btn btn-info"><i class="icon-arrow-right icon-white"></i> Confirm Admin Phone</a></td>
                  <td align="left" valign="top" style="text-align: left;">
                    <button type="submit" class="btn btn-primary"><i class="icon-share-alt icon-white"></i> Sign In</button>
                      <a href="<?=  site_url('registration');?>" class="btn btn-warning"><i class="icon-arrow-left icon-white"></i> Cancel</a>

                  </td>
                </tr>

              </table>

        <?= form_close();?>
    </div>
</div>


