<div class="row">
	<div class="span3">
            
<!--                 NEWS SECTION-->
		<div class="accordion" id="accordion2">
                    <?php 
                        if(count($edc_news)):
                            $sn = 0;
                            $id = 'collapse'.$sn;
                            foreach ($edc_news as $news) {
                    ?>
                                <div class="accordion-group">
                                  <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#<?=$id;?>">
                                         <?=$news->newstitle;?>
                                        </a>
                                  </div>
                                  <div id="<?=$id;?>" class="accordion-body collapse" style="height: 0px;">
                                        <div class="accordion-inner">
                                         <?=$news->news;?>
                                        </div>
                                  </div>
                                </div>
                    <?php
                                ++$sn;
                                $id = 'collapse'.$sn;
                            }
                        endif;
                    ?>
			
		 </div>		
                 
                <div class="thumbnail">
                      <a href="http://www.goalsplatform.com/" target="_blank"><img src="<?= get_img('goalplatform.jpg', $isweb = true); ?>" title="free business template"  alt="free business template"/></a>
			<div class="caption">
			  <h5>Goals Platform</h5>
                          <p style="text-align: justify;"> 
				Goals Platform is an online Non-Governmental professional driven Organisation aimed at helping youths achieve their dreams by providing Professional mentorship, Job Opportunities, Scholarships, Grants and other SME facilities.
                                <a href="http://www.goalsplatform.com/" target="_blank">Register Today</a>
			  </p>
			</div>
		  </div>
	</div>
	
	<div class="span9">
            <ul class="breadcrumb">
                <li><a href="<?=  site_url('home');?>">Home</a> <span class="divider">/</span></li>
			<li class="active">About</li>
            </ul>
            <div class="caption">
                <h2>About</h2>
                <br/>
                <?= $edc_info->aboutinfo; ?>       
        </div>
	</div>
</div>