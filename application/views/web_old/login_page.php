

<!--<div class="row">
	<div class="span12">
            <div class="page-header">
                
                    <?= get_edc_logo($edclogo, $edcname); ?>
                
            </div>
	</div>
</div>-->
<div class="row">
    <div class="span12 well" style="text-align: center;">
         <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
        <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
        <?= form_open('', 'class="form-horizontal"'); ?>
           <table width="50%" border="0" align="center" class="table table-bordered">
               <tr>
                   <td style="text-align: center; color: #0077b3;" colspan="2">
                       <h4>Login to Edit/Print Your Registration Detail</h4>
                     <!--                       All Edits Must be completed on/before 31st December 2014. <br/><br/>-->
                       
                   </td>
               </tr>

                
                <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>Examination Number:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                          <input type="text" name="examno" placeholder="Enter Examination Number" value="<?php echo set_value('examno'); ?>" class="input-xlarge" required="required" />
                     </label>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" style="text-align: right;"><strong>PIN:</strong></td>
                  <td align="left" valign="top" style="text-align: left;">
                      <label>
                          <input type="password" name="pin"  placeholder="Enter PIN" value="<?php echo set_value('pin'); ?>" class="input-xlarge" required="required" />
                     </label>
                  </td>
                </tr>
                
                <tr>
                  <td align="right" valign="top">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                
                <tr>
                  <td align="right" valign="top">&nbsp;</td>
                  <td align="left" valign="top" style="text-align: left;">
                    <button type="submit" class="btn btn-primary"><i class="icon-share-alt icon-white"></i> Sign In</button>
                      <a href="<?=  site_url('registration');?>" class="btn btn-warning"><i class="icon-arrow-left icon-white"></i> Cancel</a>
                  </td>
                </tr>
                
              </table>
        
        <?= form_close();?>
    </div>
</div>


 