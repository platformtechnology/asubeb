
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-6 panel-primary">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-edit"></i> Print Selected Posting Schools</strong>
                </div>
            </div>
             <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Select Zone</label>
                        <div class="col-sm-8">
                            <?php
                              $select_options = array();
                              $select_options['All'] = 'ALL ZONES';
                              foreach ($zones as $zone) {
                                  $select_options[$zone->zoneid] = $zone->zonename;
                              }
                              $select_options[''] = "---- Select Zone ----";
                              echo form_dropdown('zoneid', $select_options, set_value('zoneid'), 'class="form-control" required="required"');
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-chevron-right"></i> Proceed</button>
                      </div>
                    </div>
                <?php echo form_close(); ?>
             </div>
    </div>
</div>