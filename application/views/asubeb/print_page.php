<!DOCTYPE html>
<html>
  <head>
    <title><?= $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script lang="javascript" type="text/javascript">
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            //document.body.innerHTML = oldPage;
            
             var target = "<?= $url ?>";
            window.location = target;
        }
            
    </script>
  </head>
   <body onload="printDiv('printdiv')">
  	
       <div class="page-content">

           <div class="row">
               
               <div class="container" id="printdiv">
                    <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                         THE MINISTRTY OF EDUCATION
                         <br/>
                         <img src="<?= $edc_logo?>" alt="Edc Logo" /> <?= strtoupper($edc_detail->edcname) ?>
                         <br/>
                         <?= $title ?> 
                    </h4>
                    
                   <br/>
                   <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                        <?= $report; ?>                          
                   </div>
                </div>
            </div>
        </div>
     
    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>   
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script> 
    
   </body>
</html>
