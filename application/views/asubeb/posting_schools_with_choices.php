<div class="row">
    <div class="col-md-12 panel-primary">

            <div class="content-box-header panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> <strong>Selected Posting Schools with Number of Candidates that Selected the School as First/Second/Third Choices</strong></div>
            </div>
            <div class="content-box-large box-with-header">
                <div class="row">
                    <div class="col-md-12" id = "printArea">
                        <table class="table table-bordered table-condensed" id="" border = "1" width = "100%">
                            <caption> <h4> ANAMBRA STATE UNIVERSAL BASIC EDUCATION BOARD </h4><br/>
                                LIST OF SELECTED POSTING SCHOOLS WITH NUMBER OF CANDIDATES THAT SELECTED THE SCHOOL AS 1ST/2ND/3RD CHOICES
                                <?php
                                    if(isset($zonename)):
                                        ?>
                                <h4 style="color:crimson"> ZONE NAME : <?php echo $zonename; ?> ZONE </h4>
                                <?php
                                    endif;
                                ?>
                            </caption>
                      <thead>
                        <tr>
                          <th width = "">S/N</th>
                          <th width = "50%">School Name</th>
                          <th>Zone</th>
                          <th>School Capacity</th>
                          <th>Total 1st Choice</th>
                          <th>Total 2nd Choice</th>
                          <th>Total 3rd Choice</th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php
                            if(count($schools)):
                                $sn = 1;
                                $total_firstchoice = 0;
                                $total_secondchoice = 0;
                                $total_thirdchoice = 0;
                                $total_capacity = 0;
                                foreach ($schools as $school):

                                    $this->db->select('count(candidateid)');
                                    $this->db->where('firstchoice',$school->schoolid);
                                    $first_choice_count = $this->db->get('t_candidates')->row()->count;

                                    $this->db->select('count(candidateid)');
                                    $this->db->where('secondchoice',$school->schoolid);
                                    $second_choice_count = $this->db->get('t_candidates')->row()->count;

                                    $this->db->select('count(candidateid)');
                                    $this->db->where('thirdchoice',$school->schoolid);
                                    $third_choice_count = $this->db->get('t_candidates')->row()->count;
                            ?>
                            <tr>
                                <td><?php echo $sn;?></td>
                                <td><?php echo strtoupper($school->schoolname); ?> (<?= $school->schoolcode; ?> )</td>
                                <td><?= $school->zonename; ?></td>
                                <td><?= $school->school_capacity; ?></td>
                                <td><?= $first_choice_count; ?></td>
                                <td><?= $second_choice_count; ?></td>
                                <td><?= $third_choice_count; ?></td>
                            </tr>
                            <?php
                                $total_firstchoice = $total_firstchoice + $first_choice_count;
                                $total_secondchoice = $total_secondchoice + $second_choice_count;
                                $total_thirdchoice = $total_thirdchoice + $third_choice_count;
                                $total_capacity = $total_capacity + $school->school_capacity;
                                $sn++;
                                endforeach;?>
                            <?php endif; ?>
                      </tbody>
                      <tr>
                                <td></td>
                                <td></td>
                                <td><strong> TOTAL </strong></td>
                                <td><strong> <?php echo $total_capacity; ?> </strong></td>
                                <td><strong> <?php echo $total_firstchoice; ?> </strong></td>
                                <td><strong> <?php echo $total_secondchoice; ?> </strong></td>
                                <td><strong> <?php echo $total_thirdchoice; ?> </strong></td>

                      </tr>
                    </table>
                    </div>
                    <button class="btn btn-primary" onclick="PrintDiv()"> <i class="glyphicon glyphicon-print"></i> Print </button>
              </div>
            </div>
    </div>
</div>


