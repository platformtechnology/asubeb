<!DOCTYPE html>
<html>
  <head>
    <title><?= $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">

	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
			                <h6><?=$edc_detail->edcname?></h6>
			                <div class="social">
                                            <img src="<?= $edc_logo; ?>" height="70px" align="absmiddle" alt="EDC LOGO" />
                                            <div class="division">
                                                <hr class="left">
                                                <span>Login</span>
                                                <hr class="right">
                                            </div>
                                        </div>
                                        <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                                        <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                                        
                                        <?= form_open(site_url('asubeb/login'), 'class="form-signin"'); ?>
                                            <input type="email" name="email" class="form-control" placeholder="Email address" required="required" autofocus>
                                            <input type="password" name="password" class="form-control" placeholder="Password" required="required">
                                            <div class="action">
                                                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                                            </div>  
                                        <?= form_close() ?>
			            </div>
			        </div>

			        <div class="already">
                                    <p>&COPY; <?= config_item('product_name'); ?></p>
			        </div>
			    </div>
			</div>
		</div>
	</div>



    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script>
  </body>
</html>