<?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-6 panel-primary">
        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
            CUTOFF MARK SETTINGS/ANALYSIS FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear;
; ?>
        </h4>
        <hr/>
        <div class="content-box-header panel-heading">
            <div class="panel-title">
                <strong><i class="glyphicon glyphicon-edit"></i> Set/Analyse Cutoff Mark</strong>
            </div>
        </div>
        <div class="content-box-large box-with-header">
            <?php if ($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
            <?php if (strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
             <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">CutOff Mark:</label>
                <div class="col-sm-8">
                    <input type="text" name="cutoff" value="<?= set_value('cutoff', (count($cutoff) ? $cutoff->cutoff : 0)); ?>" class="form-control" placeholder="Enter CutOff Mark" required="required">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-adjust"></i> Set Cutoff</button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <strong>You can Analyse and set the Cutoff Mark for this exam here.</strong>
                </div>
            </div>
        <?php echo form_close(); ?>
        </div>
    </div>

</div>

<script lang="javascript" type="text/javascript">

    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
                "<html><head></head><body>" +
                divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }

</script>
