<?php
$curr_url = $this->uri->segment(2);
$priviledges = $this->session->userdata('user_role');
$priviledges = explode("^", $priviledges);
$user_privileges = $user_privilege;
$activeurl = uri_string();
?>

<div class="sidebar content-box" style="display: block;">
    <ul class="nav">

	  <li <?php if ($curr_url == 'dashboard' || !$curr_url) echo 'class="current"'; ?>><a href="<?= site_url('asubeb/dashboard') ?>"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>

		 <li class="submenu <?= $curr_url == 'secondaryschools'? ' current open': ''; ?>">
		    <a href="#"><i class="glyphicon glyphicon-list-alt"></i> Schools <span class="caret pull-right"></span></a>
			  <ul>
				<li <?= $activeurl == 'asubeb/secondaryschools/save'? 'class="current"': ''; ?>> <a href="<?= site_url('asubeb/secondaryschools/save'); ?>"> Secondary Schools</a></li>
				<li <?= $activeurl == 'asubeb/secondaryschools/posting_schools_with_choices'? 'class="current"': ''; ?>> <a href="<?= site_url('asubeb/secondaryschools/selected_schools'); ?>"> Print Selected Posting Schools</a></li>
              </ul>
		 </li>

		 <li class="submenu <?= $curr_url == 'candidates' || $curr_url == 'randomisepost'? ' current open': ''; ?>">
		 <a href="#"><i class="glyphicon glyphicon-registration-mark"></i> Candidates <span class="caret pull-right"></span></a>
			<ul>
				 <li <?= $activeurl == 'asubeb/candidates/searchcandidates'? 'class="current"': ''; ?>> <a href="<?= site_url('asubeb/candidates/searchcandidates'); ?>">View Candidates</a> </li>
				 <li <?= $activeurl == 'asubeb/registration/edit_school_of_choice_index'? 'class="current"': ''; ?>> <a href="<?= site_url('asubeb/registration/edit_school_of_choice_index'); ?>">Edit School of Choice</a> </li>
                                 <?php if($user_privilege == 'SuperAdmin'):?>
                                 <li <?= $activeurl == 'asubeb/candidates/searchpendingcandidates'? 'class="current"': ''; ?>> <a href="<?= site_url('asubeb/candidates/searchpendingcandidates'); ?>"> Pending Candidates</a> </li>
				 <li <?= $activeurl == 'asubeb/randomisepost'? 'class="current"': ''; ?>> <a href="<?= site_url('asubeb/randomisepost'); ?>">Perform Posting</a> </li>
                                 <?php endif;?>
                 <li <?= $activeurl == 'asubeb/candidates/postingslip'? 'class="current"': ''; ?>> <a href="<?= site_url('asubeb/candidates/postingslip'); ?>"> Posting Slip</a> </li>
            </ul>
		 </li>
<?php if($user_privilege == 'SuperAdmin'):?>
		 <li >
		      <a href="<?= site_url('asubeb/reports'); ?>"><i class="glyphicon glyphicon-list-alt"></i> Reports
		 </li>
		 <li class="submenu <?= $curr_url == 'remarks'? ' current open': ''; ?>">
		      <a href="<?= site_url('asubeb/remarks'); ?>"><i class="glyphicon glyphicon-record"></i> Remarks <span class="caret pull-right"></span></a>
				 <ul>
				    <li <?= $activeurl=='asubeb/remarks'? 'class="current"': ''; ?>> <a href="<?= site_url('asubeb/remarks'); ?>"> Set Cutoff</a> </li>
                 </ul>
		 </li>
<?php endif;?>

    </ul>
</div>

<blockquote style="font-weight: bold; font-size: 11px"><img src="<?= get_img('edevpro.png'); ?>" height="30px" border="0" />
    <br/>&copy; Platform Technology Ltd</blockquote>

