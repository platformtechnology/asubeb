

    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>   
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <?= $page_level_scripts; ?>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script>   
      
    <script>
        $(document).ready(function() {
                 $('#example').dataTable({
			"iDisplayLength": 50
		  }); 
        } );
 
    </script>
     <?php if(isset($reg_exams_by_year)): ?>
         <script>
            
            new Morris.Bar({
                    element: 'reg_exams_div',
                    data: <?= isset($reg_exams_by_year) ? $reg_exams_by_year : "''"; ?>,
                    xkey: 'examyear',
                    ykeys: <?= isset($ykey) ? $ykey : "''"; ?>,
                    labels: <?= isset($exam_series_name) ? $exam_series_name : "'Series A'"; ?>,
                    hideHover: 'auto'
                  });
          </script>
          
     <?php endif; ?>  
          
<script type="text/javascript">
    function PrintDiv(){
        var old_data = document.body.innerHTML;
        var print_data = document.getElementById('printArea');

        newWin= window.open("");
        newWin.document.write(print_data.outerHTML);
        newWin.print();
        newWin.close();

    }
</script>


  </body>
</html>

