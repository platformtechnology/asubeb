<!DOCTYPE html>
<html>
  <head>
    <title><?= $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .css-vertical
		       {
                filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
                -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
                -ms-transform: rotate(-90.0deg);  /* IE9+ */
                -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
                -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                 transform: rotate(-90.0deg);  /* Standard */
                 white-space:nowrap;
               }
    </style>

    <script lang="javascript" type="text/javascript">
        window.addEventListener('load', function () {
            var rotates = document.getElementsByClassName('css-vertical');
            for (var i = 0; i < rotates.length; i++) {
               rotates[i].style.height = (rotates[i].offsetWidth) + 'px';
            }
        });

        function printDiv(divID)
		{
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }

    </script>
  </head>
    <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-8">
	              <div class="logo" style="width: 370px;">
                           <div style="float: left; margin-right: 10px;"><img src="<?= get_img('edc_logos/'.$edc_detail->edclogo)?>" alt="Edc Logo" border="0" /> </div>
                           <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
	              </div>
	           </div>

                   <div class="col-md-4 pull-right">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="<?= site_url('admin/password/change');?>">Change Password</a></li>
                                  <li><a href="<?= site_url('admin/login/logout');?>">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>
        <?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>
       <div class="page-content">
           <div class="row">
               <div class="container">
                   <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
               </div>
               <div class="container" id="printdiv">

                   <?php


					   $thesubjects = "";

					   foreach ($allsubjects as $subject)
					   {
						   $allsubjectids[] = $subject->subjectid;

						  $thesubjects.='<td valign="bottom" height="100"> <p class="css-vertical-text"><strong>' . ucwords(strtolower($subject->subjectname)) . '</strong></p></td>';
					   }

				   $school_id = array();
				   $temp_array = array();
				   $candidate_ids = array();

	              	foreach ($c_scores as $key=> $candidate)
					 {

				         $candidate_ids[] = $candidate->candidateid;

						  $school_ids[] = $candidate->schoolid;

							   if(!in_array($candidate->candidateid,$temp_array))
								 {
									   $temp_array[$candidate->candidateid] = array
											   (
														   'candidateid' => $candidate->candidateid,
														   'examno' => $candidate->examno,
														   'fullname' => strtoupper($candidate->firstname.' '.$candidate->othernames),
														   'placement' => $candidate->placement,
														   'schoolid' => $candidate->schoolid,


											   );
								 }

							   $candidate_score[$candidate->candidateid][$candidate->subjectid]['exam_score'] = $candidate->exam_score;
							   $candidate_score[$candidate->candidateid][$candidate->subjectid]['ca_score'] = $candidate->ca_score;
							   $candidate_score[$candidate->candidateid][$candidate->subjectid]['practical_score'] = $candidate->practical_score;
							   $candidate_score[$candidate->candidateid][$candidate->subjectid]['total_score'] = $candidate->total_score;


		                }

			  // i.e get all failed or pass status at once

			  $candidate_id = array_unique($candidate_ids);

			  $pass = $this->report_model->getPasscountBasedonCriteriaPerCandidate($candidate_id, $examid, $examyear, $cutoff);

		      $schools = $this->report_model->selectAllSchoolsById(array_unique($school_ids));

			  foreach($schools as $value)
					{
						$dschools[$value["schoolid"]]["schoolname"] = $value["schoolname"];
						$dschools[$value["schoolid"]]["schoolcode"] = $value["schoolcode"];

					}

		    // get all registered candidates all in one place
		    $registered = $this->report_model->get_registered_subjects($candidate_id,$examid, $examyear);

		    foreach($registered as $key => $value)
		    {

			    $registered_subj[$value->candidateid]['subjects'][] = $value->subjectid;

		    }

		  // In the case of exams like GPT
		   if(!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone)
			{

				 $remarks = $this->report_model->get_subject_remark_for_candidates($allsubjectids, $candidate_id, $examid, $examyear);

	        }
			else
			{
				// get all candidates remark for all in one place

			  $remark_detail = $this->report_model->get_subject_remark_for_candidates($allsubjectids, $candidate_id, $examid, $examyear);

			  foreach($remark_detail as $key=>$value)
				    {

					   $remarks[$value->candidateid][$value->subjectid] =  '<strong>'.(intval(count($value) ? $value->exam_score : 0) < 0 ? '<span style="font-size: 10px; color: crimson;">ABS </span>' : (count($value) ? strtoupper($value->grade) : '***') ).'</strong>';

					   $remarks[$value->candidateid]['subjectkeys'][] = $value->subjectid;

				    }


			}

         foreach ($temp_array as $candidate)
		   {
                    $passport = './passports/'.$candidate['candidateid'].'.jpg';

					 if(!file_exists($passport)) $passport = get_img('no_image.jpg');
                       else $passport = base_url('passports/'.$candidate['candidateid'].'.jpg');
                   ?>

                                   <div class="row">
                                        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                                             THE MINISTRY OF EDUCATION<br/>
                                             <img src="<?= get_img('edc_logos/'.$edc_detail->edclogo)?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                                            <br/>
                                             <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>

                                        </h4>

                                        <br/>
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8" style="background-color: white; padding: 20px; border-radius: 10px">

                                            <div align = "center"> <strong>CANDIDATE PLACEMENT REPORT</strong> </div>
                                                 <hr/>
                                            <table class="table-condensed">
                                                    <tr>
                                                        <td colspan="2" align="center">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>EXAMINATION NUMBER</strong></td>
                                                        <td><?= $candidate['examno']; ?></td>
                                                        <td rowspan="5">
                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                <div class="fileupload-new thumbnail" style="width: 150px; height: 112px;"><img src="<?= $passport; ?>" width="140px" height="120px" alt="Passport" /></div>
                                                                 <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                              </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>CANDIDATE NAME</strong></td>
                                                        <td><?= $candidate['fullname']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>EXAMINATION TYPE</strong></td>
                                                        <td><strong><?= strtoupper($examdetail->examdesc. ' (' . $examdetail->examname .') '. $examyear); ?></strong></td>
                                                    </tr>

                                                    <tr>
                                                        <td><strong>SCHOOL</strong></td>
                                                        <td><?= strtoupper($dschools[$candidate['schoolid']]['schoolname']); ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2"><br/><strong>SUBJECT GRADES<br/></strong></td>
                                                    </tr>
                                                     <tr>
                                                         <td><strong>SUBJECTS</strong></td>
                                                         <td><strong><?= $examdetail->hasposting ? 'SCORES' : 'GRADES'; ?></strong></td>
                                                    </tr>

                                                     <?php

													  $total = 0;
												       //If exam is GPT in case of ABIA EDC
													  if(!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone)
														{

														  if(count($allsubjects))
													      {

															foreach ($allsubjects as $subjs)
															{
															  echo '<tr>

															 <td><strong>'.strtoupper($subjs->subjectname).'</strong></td>';

															 //check if student sat for the exam so that u dont access non existing result
															 if(isset($registered_subj[$candidate['candidateid']]) && in_array($subjs->subjectid,$registered_subj[$candidate['candidateid']]['subjects']))
															 {
																 echo '<td>'.$remarks[$candidate['candidateid']][$subjs->subjectid].'</td>';

															 }
															 else
															 {

																 if(isset($registered_subj[$candidate['candidateid']])  && !in_array($subjs->subjectid , $registered_subj[$candidate['candidateid']]['subjects']))
																 {
																	 echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
																 }
																 else
																 {
																	 echo '<td style="font-size: 10px; color: crimson;"><strong>N.A</strong></td>';

																 }

															 }

															}

														  }

														  echo '</tr>';

													   }


		                                              elseif($examdetail->hasposting)
													   {

													   if(count($allsubjects))
													         {

													           foreach ($allsubjects as $subjs)
															       {

																	 $sumtotal = ($examdetail->hasca? get_individual_standard($candidate_score[$candidate['candidateid']][$subjs->subjectid]["ca_score"], $standarddata->standardca): 0) + get_individual_standard($candidate_score[$candidate['candidateid']][$subjs->subjectid]["exam_score"], $standarddata->standardexam) + ($examdetail->haspractical? get_individual_standard($candidate[$candidate['candidateid']][$subjs->subjectid]["practical_score"], $standarddata->standardpractical):0);

																	 $score = $sumtotal;
																	 $total += $score;
																	 if((intval($score) == 0) && (intval($candidate_score[$candidate['candidateid']][$subjs->subjectid]['exam_score']) < 0)) $score = '<span style="color: crimson;">ABS</span>';

														     ?>

                                                                    <tr>
                                                                        <td><strong><?= ($subjs->iscompulsory ? '*': '') .strtoupper($subjs->subjectname); ?></strong></td>
                                                                        <td><strong><?= $score ?></strong></td>
                                                                   </tr>
                                                     <?php
													               }
														      }
		                                               }

                                                    else
													 {

													    foreach ($allsubjects as $subjects)
														 {
														     ?>

														 <tr>
															 <td><strong><?= ($subjects->iscompulsory ? '*': '') .strtoupper($subjects->subjectname); ?></strong></td>

															<?php

															 if(isset($remarks[$candidate['candidateid']]) && in_array($subjects->subjectid,$remarks[$candidate['candidateid']]['subjectkeys']))
															 {
																 echo '<td>'.$remarks[$candidate['candidateid']][$subjects->subjectid].'</td>';

															 }

															 else
															 {

																 if(isset($registered_subj[$candidate['candidateid']]) && !in_array($subjects->subjectid , $registered_subj[$candidate['candidateid']]['subjects']))
																 {
																	 echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
																 }
																 else
																 {
																	 echo '<td style="font-size: 10px; color: crimson;"><strong>N.A</strong></td>';

																 }

															 }

                                                     echo '</tr> ';


													  }

											        }

											       ?>

                                                    <tr>
                                                        <td colspan="2" align="center">&nbsp;</td>
                                                    </tr>

                                                     <?php if($examdetail->hasposting)
													       {
													   ?>
                                                        <tr>
                                                            <td><strong>TOTAL</strong></td>
                                                            <td><?= $total; ?></td>
                                                        </tr>
												     <?php
														   }

                                                    ?>


                                                    <?php if($examdetail->hasposting && isset($pass[$candidate['candidateid']]) && $pass[$candidate['candidateid']]== 'PASSED'):
                                                            $postedto = $this->registration_model->get_school($candidate['placement']);
                                                      ?>
                                                        <tr>
                                                            <td><strong>SCHOOL POSTED</strong></td>
                                                            <td><strong><?= $postedto == '' ? 'Not Posted' : $postedto; ?></strong></td>
                                                        </tr>

                                                    <?php endif; ?>

                                                    <tr>
                                                        <td><strong>STATUS</strong></td>
                                                        <td><strong>

																<?php
																    // if exam is GPT in case of ABIA EDC
																	if(!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone)
																	{
																	    if(isset($remarks[$candidate['candidateid']]['grade'])) { echo $remarks[$candidate['candidateid']]['grade']." (".$remarks[$candidate['candidateid']]['remark'].")" ; } else { echo '****'; }

																	}
																	//if exam is not GPT
																	else
																	{
																      if(isset($pass[$candidate['candidateid']])){ echo $pass[$candidate['candidateid']]; }

																	}

																 ?>
														   </strong>

														</td>
                                                        <td align="left"><img src="<?= get_img('edc_logos/'.$this->edcid.'_stamp.png') ?>" alt="Stamp" style="width: 150px; height: 120px" /></td>
                                                    </tr>

                                            </table>

                                          <p style="page-break-before: always;"></p>

                                        </div>

                                   </div>

                               <?php

				          }
                   ?>
                </div>
            </div>
        </div>

    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

   </body>
</html>
