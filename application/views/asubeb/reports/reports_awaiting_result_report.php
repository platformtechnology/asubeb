<?php

function sort_by_examno($a, $b) {
    return $a['examno'] - $b['examno'];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php //echo base_url('resources/bootstrap/css/bootstrap.min.css');      ?>" rel="stylesheet">
        <link href="<?php //echo base_url('resources/css/styles.css');      ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .css-vertical{
                filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
                -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
                -ms-transform: rotate(-90.0deg);  /* IE9+ */
                -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
                -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                transform: rotate(-90.0deg);  /* Standard */
                white-space:nowrap;
            }
            .css-vertical-text {
                margin-bottom: 15px;
                color:#333;
                border:0px solid red;
                writing-mode:tb-rl;
                -webkit-transform:rotate(-90deg);
                -moz-transform:rotate(-90deg);
                -o-transform: rotate(-90deg);
                -ms-transform: rotate(-90deg);
                transform: rotate(-90deg);
                white-space:nowrap;
                display:block;
                bottom:0;
                width:20px;
                height:20px;
                font-family: 'Trebuchet MS', Helvetica, sans-serif;
                font-size:11px;
                font-weight:bold;
                /*text-align:left;*/
                /*text-shadow: 0px 0px 1px #333;*/

                filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
            }
        </style>

    </head>
    <body>

        <div class="page-content">
            <div class="row">
                <!--                <div class="container">
                                    <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
                                </div>-->
                <div id="printdiv">
                    <?php
                    $all_subj = count($allsubjects);
                    $temp_array = array();
                    $candidate_score = array();
                    foreach ($standard_report_scores as $dt) {
                        // just collect everything into male array, if result is not gender based
                        $temp_array[$dt->candidateid] = array(
                                    'candidateid' => $dt->candidateid,
                                    'examno'      => $dt->examno,
                                    'gender'      => $dt->gender,
                                    'fullname'    => $dt->fullname,
                                    'placement'   => $dt->placement,
                                    'schoolname'      => $dt->schoolname,
                                    'lganame'      => $dt->lganame,
                        );
                        $candidate_score[$dt->candidateid][$dt->subjectid]['exam_score'] = $dt->exam_score;
                        $candidate_score[$dt->candidateid][$dt->subjectid]['ca_score'] = $dt->ca_score;
                        $candidate_score[$dt->candidateid][$dt->subjectid]['practical_score'] = $dt->practical_score;
                        $candidate_score[$dt->candidateid][$dt->subjectid]['total_score'] = $dt->total_score;
                    }
                        $male_candidates = 0;
                        //$schoodata = $this->registration_model->get_school($schoolid, true);
                        ?>
                        <div class="row">
                                <center>
                                    <table border="0" align="center">
                                        <tr>
                                            <td>
                                                <img src="<?= $edc_logo ?>" alt="Edc Logo" />
                                            </td>
                                            <td>
                                                <h4 style="text-align: center; font-size: 20px; color: #0077b3; font-weight: bold">
                                                    THE MINISTRY OF EDUCATION<br/>
                            <?= ucwords(strtoupper($edc_detail->edcname)); ?><br/>
                            <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>
                                                </h4>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            <br/>
                            <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                                <table width="100%" border="1" cellpadding="5"  cellspacing="1" style="border-collapse:collapse; border-color:#000; font:Verdana, Geneva, sans-serif; font-size:10px">
                                    <tr style="font-weight: bold;">
                                        <td rowspan="2">SN</td>
                                        <td rowspan="2">EXAMNO</td>
                                        <td rowspan="2">NAME</td>
                                        <td rowspan="2">LGA</td>
                                        <td rowspan="2">SCHOOL</td>
                                        <td colspan="<?= count($allsubjects); ?>" align="center">SUBJECTS</td>
                                    </tr>
                                    <!--SUBJECTS HEADER ROW-->
                                    <tr height="200">
                                        <?php
                                            foreach ($allsubjects as $subject) {
                                                echo '<td valign="bottom"> <div class = "css-vertical-text"> <p class="css-vertical-text"><strong>' . ucwords(strtolower($subject->subjectname)) . '</strong></p> </div></td>';
                                            }
                                            ?>
                                    </tr>
                                    <!-- END SUBJECTS HEADER ROW-->

                                        <?php
                                        $row = 0;

                                        foreach ($temp_array as $candidateid => $candidate):

                                            echo '<tr style="font-size: 13px;">';
                                            echo '<td>' . ++$row . '</td>';
                                            echo '<td>' . $candidate['examno'] . '</td>';
                                            echo '<td>' . $candidate['fullname'] . '</td>';
                                            echo '<td>' . $candidate['lganame'] . '</td>';
                                            echo '<td>' . $candidate['schoolname'] . '</td>';
                                            //echo '<td>' . $candidate['gender']. '</td>';


                                            if ($all_subj >= 1) {

                                                $totalScores = 0;
                                                //$registered_subjects = $this->reg_subjects_model->get_registered_subjects($candidate['candidateid'], $examid, $examyear);
                                                //                                                if (count($registered_subjects)) {
                                                foreach ($allsubjects as $subjects) {
                                                    //                                                        foreach ($registered_subjects as $reg_subject) {
                                                    $found = false;
                                                    //                                                            if ($reg_subject->subjectid == $subjects->subjectid) {


                                                    if (!isset($candidate_score[$candidateid][$subjects->subjectid]['total_score']) || $candidate_score[$candidateid][$subjects->subjectid]['exam_score'] <= 0)
                                                        echo '<td><strong><span style="font-size: 10px; color: crimson;">ABS</span></strong></td>';
                                                    else {

                                                        echo '<td><strong>';
                                                        if (isset($candidate_score[$candidateid][$subjects->subjectid]['total_score'])) {
                                                            echo $examdetail->haspractical ? $candidate_score[$candidateid][$subjects->subjectid]['practical_score'] . ' + ' : '';
                                                            echo $examdetail->hasca ? $candidate_score[$candidateid][$subjects->subjectid]['ca_score'] . ' + ' : '';
                                                            echo $candidate_score[$candidateid][$subjects->subjectid]['exam_score'];
                                                        }
                                                        else {
                                                            echo "";
                                                        };
                                                        echo '</strong></td>';
                                                    }
                                                    $found = true;
                                                    //                                                                    break;
                                                    //                                                            }
                                                    //                                                        }
                                                    //                                                        if (!$found)
                                                    //                                                            echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                                }
                                                //                                                } else
                                                //                                                    echo '<td title="" style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                            }


                                            echo '</tr>';

                                        endforeach;
                                        ?>

                                </table>

                                <div style="page-break-inside:avoid;page-break-after:auto;">
                                    <center>
                                        <table align="center">
                                            <tr style="page-break-inside:avoid;page-break-after:auto;">
                                                <td align="left">
                                                    <br>
                                                    <br>
                                                    <br>
                                            </tr>
                                        </table>
                                    </center>
                                </div>
                            </div>
                        </div>

    <?php
    //End display of male candidates
    ?>
                        <div style="page-break-after:always"></div>
                </div>
            </div>

        </div>


        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

    </body>
</html>
