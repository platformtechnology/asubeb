<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
        <style type="text/css">
            @media print {
                html, body {
                    width: 643px;
                    height: 816px;
                }
            }
            @page {
                size: 643px 816px;  /* width height */
            }
            body{
                margin: 0px;
                padding: 0px;
                background-color: white;
            }
            #certificate_canvas{
                width: 643px;
                height: 816px;
            }
            #candidateName{
                font-family:"Lucida Calligraphy";
                font-size:30px;
                text-align: center;
                margin-top: 5.5cm;
            }
            #examNumber{
                font-size:20px;
                text-align: center;
                margin-top: 0.5cm;
            }
            #examYear{
                font-size:20px;
                text-align: center;
                margin-top: 0.1cm;
                margin-left: -5cm;
            }
            #schoolName{
                font-size:20px;
                text-align: center;
                margin-top: 0.5cm;
            }
            #resultArea{
                font-size: 30px;
                margin-top: 0.7cm;
                margin-left: -6cm;
                
            }
            #certNumber{
                font-size: 14px;
                margin-top: 3cm;
                margin-left: 12cm;
            }
            #passport{
                float:left;

                margin-left:-10cm;
                margin-top: -2.5cm;
            }
        </style>
    </head>
    <body>
        <?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row();
        ?>


        <div>
            <div>
                <div>

                    <?php
                    $lg = array();


                    foreach ($lgas as $lga) {
                        $lgids[] = $lga->lgaid;
                        $lg[$lga->lgaid]['lgaid'] = $lga->lgaid;
                        $lg[$lga->lgaid]['zonename'] = $lga->zonename;
                        $lg[$lga->lgaid]['lginitials'] = $lga->lgainitials;
                        $lg[$lga->lgaid]['lgname'] = $lga->lganame;
                    }

                    $thesubjects = "";

                    foreach ($allsubjects as $subject) {
                        $allsubjectids[] = $subject->subjectid;
                        $thesubjects.='<td valign="bottom" height="100"> <p class="css-vertical-text"><strong>' . ucwords(strtolower($subject->subjectname)) . '</strong></p></td>';
                    }


                    $all_subj = count($allsubjects);

                    // if school is not set

                    if (!isset($schoolid) || @trim($schoolid) == false) {

                        $schools = $this->report_model->get_schools_details_per_lga($lgids);

                        foreach ($schools as $key => $value) {
                            $schoolids[] = $value->schoolid;
                            $school[$value->schoolid]['lgaid'] = $value->lgaid;
                            $school[$value->schoolid]['zoneid'] = $value->zoneid;
                            $school[$value->schoolid]['schoolname'] = $value->schoolname;
                            $school[$value->schoolid]['schoolcode'] = $value->schoolcode;
                        }
                        // get all candidates for all these particular schools now

                        $candidates = $this->report_model->get_all_candidates_per_lga($lgids, $examid, $examyear);
                        foreach ($candidates as $key => $value) {
                            $candidateids[] = $value->candidateid;
                            $candidate[$value->schoolid][$value->candidateid]['candidateid'] = $value->candidateid;
                            $candidate[$value->schoolid][$value->candidateid]['firstname'] = $value->firstname;
                            $candidate[$value->schoolid][$value->candidateid]['othernames'] = $value->othernames;
                            $candidate[$value->schoolid][$value->candidateid]['examno'] = $value->examno;
                            $candidate[$value->schoolid][$value->candidateid]['gender'] = $value->gender;
                        }


                        // get all candidates remark for all in one place
                        // the if statement works for GPT students
                        if (!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone) {

                            $remarks = $this->report_model->get_subject_remark_for_candidates($allsubjectids, $candidateids, $examid, $examyear);
                        }
                        else {

                            $remark_detail = $this->report_model->get_subject_remark_for_candidates($allsubjectids, $candidateids, $examid, $examyear);

                            foreach ($remark_detail as $key => $value) {

                                $remarks[$value->candidateid][$value->subjectid] = $remarks[$value->candidateid][$value->subjectid] = '<strong>' . (intval(count($value) ? $value->exam_score : 0) < 0 ? '<span style="font-size: 10px; color: crimson;">NES </span>' : (count($value) ? strtoupper($value->grade) : '***') ) . '</strong>';

                                $remarks[$value->candidateid]['subjectkeys'][] = $value->subjectid;
                            }
                            //get all school pass status in one place before the loop
                            $pass = $this->report_model->getPasscountBasedonCriteriaPerCandidate($candidateids, $examid, $examyear, $cutoff);
                        }
                    }
                    foreach ($lg as $lgid => $lga) {
                        if (count($schools)) {
                            ?>
                            <div class="">
                                <?php
                                foreach ($schoolids as $dschool) {
                                    if (isset($candidate[$dschool]) && count($candidate[$dschool])) {
                                        ?>
                                        <?php
                                        $cert_serial = $page_number_start;
                                        foreach ($candidate[$dschool] as $candidateid => $dcandidate) {
                                            if (isset($remarks[$dcandidate['candidateid']])) {
                                                ?>
                                                <div id="certificate_canvas" style="width:100%; ">
                                                    <center>
                                                        <div id="certNumber">
                                                            <!---
                                                            <div id="passport">
                                                                <img src="<?php echo base_url('passports/angela.jpg') ?>" alt="" width = "170px" height="200px"/>
                                                            </div>
                                                            -->
                                                            <div>
                                                                <?php
                                                                $cert_number = str_pad($cert_serial, $pad_length = 7, $pad_string = 0, STR_PAD_LEFT);
                                                                echo $examyear . $cert_number
                                                                ?>
                                                            </div>

                                                        </div>
                                                        <div id="candidateName" >
                                                            <?php
                                                            $name = ucwords(strtolower($dcandidate['firstname'])) . ' ' . ucwords(strtolower($dcandidate['othernames']));
                                                            echo $name;
                                                            ?>
                                                        </div>
                                                        <div id="schoolName" >
                                                            <?php
                                                            echo $school[$dschool]['schoolname'];
                                                            ?>
                                                        </div>
                                                        <!--
                                                        <div id="examNumber" >
                                                            <?php
                                                            echo $dcandidate['examno'];
                                                            ?>
                                                        </div>
                                                        -->
                                                        <div id ="resultArea">
                                                                <?php
                                                                if (isset($remarks[$dcandidate['candidateid']]['grade'])) {
                                                                    echo $remarks[$dcandidate['candidateid']]['grade'];
                                                                }
                                                                ?>
                                                        </div>
                                                        <div id="examYear" >
                                                            <?php
                                                                echo substr($examyear, $start = 2)
                                                            ?>
                                                        </div>
                                                    </center>
                                                </div>
                                                <div style="page-break-after: always"></div>
                                                <?php
                                                $cert_serial++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>

        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

    </body>
</html>
