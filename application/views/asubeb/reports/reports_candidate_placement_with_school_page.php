<!DOCTYPE html>
<html>
  <head>
    <title><?= $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .css-vertical{
            filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                 -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
             -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
              -ms-transform: rotate(-90.0deg);  /* IE9+ */
               -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
          -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                  transform: rotate(-90.0deg);  /* Standard */
                  white-space:nowrap;
        }
    </style>
    <script lang="javascript" type="text/javascript">
        window.addEventListener('load', function () {
            var rotates = document.getElementsByClassName('css-vertical');
            for (var i = 0; i < rotates.length; i++) {
               rotates[i].style.height = (rotates[i].offsetWidth) + 'px';
            }
        });
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }

    </script>
  </head>
    <body>
  	<div class="header" style="background-color: <?= $edc_detail->themecolor ?>;">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-8">
	              <div class="logo" style="width: 370px;">
                           <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo?>" alt="Edc Logo" border="0" /> </div>
                           <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
	              </div>
	           </div>

                   <div class="col-md-4 pull-right">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="<?= site_url('admin/password/change');?>">Change Password</a></li>
                                  <li><a href="<?= site_url('admin/login/logout');?>">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>
         <?php
         $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row();
         $cutoff = $this->db
                    ->get_where('t_cutoff', array('examid'=>$examid, 'examyear'=>$examyear, 'edcid'=>$this->data['edc_detail']->edcid))
                    ->row()
                    ->cutoff;
            if(trim($cutoff) == false){
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('admin/reports/reporting/'.$examid.'/'.$examyear));
            }
         ?>
       <div class="page-content">
           <div class="row">
               <div class="container">
                   <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
               </div>
               <div id="printdiv">

                   <div class="row">
                        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                             THE MINISTRY OF EDUCATION<br/>
                             <img src="<?= $edc_logo?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                             <br/>
                             <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>
                        </h4>

                        <br/>
                        <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                            <h4>
                                <?php
                                    if($examdetail->haszone) echo '<strong>ZONE:</strong> ' . $zones[0]->zonename . '<br/>';
                                    else {
                                        echo '<strong>ZONE:</strong> ' . $lgas[0]->zonename . '<br/>';
                                        echo '<strong>LGA:</strong> ' . $lgas[0]->lganame . '<br/>';
                                    }
                                ?>

                                <strong>SCHOOL POSTED:</strong> <?= $school->schoolname; ?> <strong> CODE:</strong> <?= $school->schoolcode; ?><br/>
                                <strong>TOTAL CANDIDATES:</strong> <?= count($placement_data); ?><br/>
                            </h4>
                            <table width="100%" class="table table-bordered table-condensed">
                                <thead>
                                    <tr style="font-weight: bold;">
                                        <th>SN</th>
                                        <th>EXAMNO</th>
                                        <th>NAME</th>
                                        <th>GENDER</th>
                                        <th>PRESENT SCHOOL</th>
                                        <th>FIRST CHOICE</th>
                                        <th>SECOND CHOICE</th>
                                        <th>TOTAL SCORE</th>
                                    </tr>
                                </thead>

                                   <tbody>

                               <?php
$this->load->model('admin/report_model');
                                 $row = 0;
                                   foreach ($placement_data as $candidate):
                                     ++$row;

                                      $name = strtoupper($candidate->firstname . ' ' . $candidate->othernames);

                                      echo '<tr>';
                                        echo '<td>'.$row.'</td>';
                                        echo '<td>'.$candidate->examno.'</td>';
                                        echo '<td>'.$name.'</td>';
                                         echo '<td>'.$candidate->gender.'</td>';
                                        echo '<td>'.$candidate->schoolname.'</td>';
                                        echo '<td>'.$this->report_model->getSchoolName($candidate->firstchoice).'</td>';
                                        echo '<td>'.$this->report_model->getSchoolName($candidate->secondchoice).'</td>';
                                        echo '<td><strong>'. $candidate->total .'</strong></td>';
                                      echo '</tr>';

                                    ?>


                                <?php endforeach; ?>
                                   </tbody>
                            </table>
                        </div>
                       <p style="page-break-before: always;">

                   </div>

                </div>
            </div>
        </div>

    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

   </body>
</html>
