<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
@media print {
  html, body {
    width: 793px;
    height: 1077px;
  }
}
               @page {
      size: 793px 1077px;  /* width height */
   }
            body{
                margin: 0px;
                padding: 0px;
                background-color: white;
            }
            #certificate_canvas{
                width: 793px;
                height: 800px;
            }
            #candidateName{
                font-family:"Lucida Calligraphy";
                font-size:30px;
                text-align: center;
                margin-top: 6.5cm;
            }
            #examNumber{
                font-size:20px;
                text-align: center;
                margin-top: 0.5cm;
            }
            #examYear{
                font-size:20px;
                text-align: center;
                margin-top: 0.5cm;
                margin-left: 2.8cm;
            }
            #schoolName{
                font-size:16px;
                text-align: center;
                margin-top: 0.5cm;
            }
            #resultArea{
                font-size: 14px;
                margin-top: 3cm;
            }
            #certNumber{
                font-size: 14px;
                margin-top: 3cm;
                margin-left: 12cm;
            }
            #passport{
                float:left;

                margin-left:-10cm;
                margin-top: -1.5cm;
            }
        </style>
    </head>
    <body>
        <?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>

        <div class="">
            <div class="">
                <?php
                $lg = array();
                foreach ($lgas as $lga) {
                    $lgids[] = $lga->lgaid;
                    $lg[$lga->lgaid]['lgaid'] = $lga->lgaid;
                    $lg[$lga->lgaid]['zonename'] = $lga->zonename;
                    $lg[$lga->lgaid]['lginitials'] = $lga->lgainitials;
                    $lg[$lga->lgaid]['lgname'] = $lga->lganame;
                }


                $subjects_array = array();

                foreach ($allsubjects as $subject) {
                    $allsubjectids[] = $subject->subjectid;
                    $subjects_array[$subject->subjectid] = ucwords(strtolower($subject->subjectname));
                }

                $all_subj = count($allsubjects);

                // if school is not set

                if (!isset($schoolid) || @trim($schoolid) == false) {
                    //GET ALL SCHOOLS IN THE SELECTED LGA
                    $schools = $this->report_model->get_schools_details_per_lga($lgids);
                    foreach ($schools as $key => $value) {
                        $schoolids[] = $value->schoolid;
                        $school[$value->schoolid]['lgaid'] = $value->lgaid;
                        $school[$value->schoolid]['zoneid'] = $value->zoneid;
                        $school[$value->schoolid]['schoolname'] = $value->schoolname;
                        $school[$value->schoolid]['schoolcode'] = $value->schoolcode;
                    }

                    //GET ALL CANDIDATES IN THE SELECTED LGA
                    $candidates = $this->report_model->get_all_candidates_per_lga($lgids, $examid, $examyear);
                    foreach ($candidates as $key => $value) {
                        $candidateids[] = $value->candidateid;
                        $candidate[$value->schoolid][$value->candidateid]['candidateid'] = $value->candidateid;
                        $candidate[$value->schoolid][$value->candidateid]['firstname'] = $value->firstname;
                        $candidate[$value->schoolid][$value->candidateid]['othernames'] = $value->othernames;
                        $candidate[$value->schoolid][$value->candidateid]['examno'] = $value->examno;
                        $candidate[$value->schoolid][$value->candidateid]['gender'] = $value->gender;
                    }

                    //GET REMARKS FOR ALL CANDIDATES
                    $remark_detail = $this->report_model->get_subject_remark_for_candidates($allsubjectids, $candidateids, $examid, $examyear);
                    foreach ($remark_detail as $key => $value) {
                        $remarks[$value->candidateid][$value->subjectid] = $remarks[$value->candidateid][$value->subjectid] = '<strong>' . (intval(count($value) ? $value->exam_score : 0) < 0 ? '<span style="font-size: 10px; color: crimson;">ABS </span>' : (count($value) ? strtoupper($value->grade) : '***') ) . '</strong>';
                        $remarks[$value->candidateid]['subjectkeys'][] = $value->subjectid;
                    }

                    $resit_remark_detail = $this->resit_report_model->get_subject_remark_for_candidates($allsubjectids, $candidateids, $examid, $examyear);
                    //STORE RESIT REMARKS IN ARRAYS
                    foreach ($resit_remark_detail as $key => $value) {
                        $resit_remarks[$value->candidateid][$value->subjectid] = $resit_remarks[$value->candidateid][$value->subjectid] = '<strong>' . (intval(count($value) ? $value->exam_score : 0) < 0 ? '<span style="font-size: 10px; color: crimson;">ABS </span>' : ( (count($value) && $value->exam_score != 0)  ? strtoupper($value->grade) : '') ) . '</strong>';
                        $resit_remarks[$value->candidateid]['subjectkeys'][] = $value->subjectid;
                    }
                }

                $cert_serial_start = $page_number_start;
                foreach ($lg as $lgid => $lga) {
                    if (count($schools)) {
                        ?>
                        <div class="">
                            <?php
                            $cert_serial = $cert_serial_start;
                            foreach ($schoolids as $dschool) {
                                if (isset($candidate[$dschool]) && count($candidate[$dschool])) {
                                    ?>
                                    <?php
                                    
                                    foreach ($candidate[$dschool] as $candidateid => $dcandidate) {
                                        if (isset($remarks[$dcandidate['candidateid']])) {
                                            ?>
                                            <div id="certificate_canvas" style="width:100%; ">
                                                <center>
                                                    <div id="certNumber">
                                                        <div id="passport">
                                                            <?php
																$local_image_location = 'passports/'.$dcandidate['candidateid'].'.jpg';
																$web_image_source = "http://edcabiastate.com/passports/".$dcandidate['candidateid'].".jpg";
																if(file_exists($local_image_location)){
																	$img_location = base_url($local_image_location);
																}
																else{
																	$img_location = $web_image_source;
																}
															?>
                                                            <img src="<?php echo $img_location ?>" alt="" width = "170px" height="200px"/>
                                                        </div>
                                                        <div>
                                                                                                                <?php
                                                        $cert_number = str_pad($cert_serial, $pad_length = 7, $pad_string = 0,STR_PAD_LEFT);
                                                        echo $examyear.$cert_number?>
                                                        </div>

                                                    </div>
                                                    <div id="candidateName" >
                                                        <?php
                                                        $name = ucwords(strtolower($dcandidate['firstname'])) . ' ' . ucwords(strtolower($dcandidate['othernames']));
                                                        echo $name;
                                                        ?>
                                                    </div>
                                                    <div id="schoolName" >
                                                        <?php
                                                       echo $school[$dschool]['schoolname'];
                                                        ?>
                                                    </div>
                                                    <div id="examNumber" >
                                                        <?php
                                                        echo $dcandidate['examno'];
                                                        ?>
                                                    </div>
                                                    <div id="examYear" >
                                                        <?php
                                                        echo substr($examyear, $start = 2)
                                                        ?>
                                                    </div>
                                                    <div id ="resultArea">
                                                        <table border = "0" width = "80%">
                                                            <tr>
                                                                <?php
                                                                $subject_count = 0;
                                                                foreach ($allsubjects as $subjects) {
                                                                    $subject_count++;
                                                                    ?>

                                                                    <?php
                                                                    if (isset($remarks[$dcandidate['candidateid']])) {
                                                                        echo '<td> ';

                                                                       if ($subject_count % 2 == 0){
                                                                           echo '<span style = "margin-left:100px; font-size:15px">'.$subjects_array[$subjects->subjectid].'</span>';
                                                                       }
                                                                       else {
                                                                        echo  '<span style = "font-size:15px">'.$subjects_array[$subjects->subjectid].'</span>';
                                                                       }
																	   $subject_grade = isset($resit_remarks[$dcandidate['candidateid']][$subjects->subjectid])?$resit_remarks[$dcandidate['candidateid']][$subjects->subjectid]:$remarks[$dcandidate['candidateid']][$subjects->subjectid];


                                                                        echo '</td>';
                                                                        echo '<td > <span style = "margin-right:50px; font-size:15px">' . $subject_grade . '</span> </td>';
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                    if ($subject_count % 2 == 0) {
                                                                        ?>
                                                                    </tr> <tr>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </tr>
                                                            <?php
                                                        }

                                                    ?>
                                                </table>
                                            </div>
                                        </center>
                                    </div>
                            <div style="page-break-after: always"></div>
                                    <?php
                                    $cert_serial++;
                                }
                                }
                            }
                            ?>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>

        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

    </body>
</html>
