<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script lang="javascript" type="text/javascript">
            function printDiv(divID) {
                //Get the HTML of div
                var divElements = document.getElementById(divID).innerHTML;
                //Get the HTML of whole page
                var oldPage = document.body.innerHTML;

                //Reset the page's HTML with div's HTML only
                document.body.innerHTML =
                        "<html><head></head><body>" +
                        divElements + "</body>";

                //Print Page
                window.print();

                //Restore orignal HTML
                document.body.innerHTML = oldPage;
            }

        </script>
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="logo" style="width: 370px;">
                            <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo ?>" alt="Edc Logo" border="0" /> </div>
                            <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
                        </div>
                    </div>  

                    <div class="col-md-4 pull-right">
                        <div class="navbar navbar-inverse" role="banner">
                            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
                                        <ul class="dropdown-menu animated fadeInUp">
                                            <li><a href="<?= site_url('admin/password/change'); ?>">Change Password</a></li>
                                            <li><a href="<?= site_url('admin/login/logout'); ?>">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="container">
                    <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
                </div>
                <div class="container" id="printdiv">
                    <?php
                    foreach ($zones as $zone):
                        if (isset($schoolid) && trim($schoolid) == false) {
                            $schools = $this->report_model->get_schools_per_lga($zone->zoneid);
                            $flag = $this->db->get_where('t_candidates', array('zoneid' => $zone->zoneid, 'examid' => $examid, 'examyear' => $examyear))->result();
                        } else {
                            $schools = $schooldetails;
                            $flag = $this->db->get_where('t_candidates', array('schoolid' => $schools[0]->schoolid, 'examid' => $examid, 'examyear' => $examyear))->result();
                        }

                        if (count($flag)):
                            ?>
                            <div class="row">

                                <?php
                                foreach ($schools as $school):
                                    $candidates = $this->report_model->get_candidates_per_school_demacate_gender($school->schoolid, $examid, $examyear);
                                    $male_candidates = $candidates['MALE'];
                                    $female_candidates = $candidates['FEMALE'];
                                    ?>
                                    <?php if (count($male_candidates)): ?>
                                        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                                            THE MINISTRY OF EDUCATION<br/>
                                            <img src="<?= $edc_logo ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span><br/>
                                            <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>                             
                                        </h4>
                                        <br/>
                                        <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <strong>ZONE:</strong> <?= $zone->zonename; ?> <br/>
                                                    <strong>SCHOOL:</strong> <?= $school->schoolname; ?> <strong> Code:</strong> <?= $school->schoolcode; ?><br/>
                                                    <strong>TOTAL CANDIDATES:</strong> <?= count($male_candidates); ?><br/>
                                                </div>
                                            </div>


                                            <table width="100%" class="table table-bordered table-condensed table-hover" id="gradeTable">
                                                <thead>
                                                    <tr>
                                                        <th>Sn</th>
                                                        <th>&nbsp;</th>
                                                        <th>&nbsp;</th>
                                                        <th>&nbsp;</th>

                                                        <?php
                                                        if (count($allsubjects)) {
                                                            foreach ($allsubjects as $subjects) {
                                                                echo '<th style="font-size: 14px">' . ucwords(strtolower($subjects->subjectname)) . '</th>';
                                                            }
                                                        }
                                                        ?>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $row = 0;
                                                    $ca_num = 0;
                                                    $prac_num = 0;
                                                    $exam_num = 0;

                                                    foreach ($male_candidates as $candidate):
                                                        ++$row;

                                                        $name = strtoupper($candidate->firstname . ' ' . $candidate->othernames);

                                                        //PRACTICAL ROW
                                                        echo '<tr>';
                                                        echo '<td>' . $row . '</td>';
                                                        echo '<td>' . $candidate->examno . '</td>';
                                                        echo '<td>' . $name . '</td>';
                                                        echo '<td>' . $candidate->gender . '</td>';

                                                        if (count($allsubjects)) {

                                                            $registered_subjects = $this->reg_subjects_model->get_registered_subjects($candidate->candidateid, $examid, $examyear);
                                                            if (count($registered_subjects)) {
                                                                foreach ($allsubjects as $subjects) {
                                                                    foreach ($registered_subjects as $reg_subject) {
                                                                        $found = false;
                                                                        if ($reg_subject->subjectid == $subjects->subjectid) {

                                                                            $exam_data = array();
                                                                            foreach ($c_scores as $scoresvalue) {
                                                                                if (($scoresvalue->subjectid == $subjects->subjectid) && ($scoresvalue->candidateid == $candidate->candidateid)) {
                                                                                    $exam_data = $scoresvalue;
                                                                                    break 1;
                                                                                }
                                                                            }


                                                                            $practicalcore = !count($exam_data) ? 0 : $exam_data->practical_score;
                                                                            $examscore = !count($exam_data) ? 0 : $exam_data->exam_score;
                                                                            $cascore = !count($exam_data) ? 0 : $exam_data->ca_score;


                                                                            if ($subjects->haspractical == 1) {
                                                                                echo '<td>' . ($examdetail->hasca ? (($cascore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $cascore) . ' + ') : '') . ($examscore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $examscore) . ($examdetail->haspractical ? (' + ' . ($practicalcore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $practicalcore)) : '') . '</td>';
                                                                            } else {
                                                                                echo '<td>' . ($examdetail->hasca ? (($cascore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $cascore) . ' + ') : '') . ($examscore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $examscore) . ($examdetail->haspractical ? ' + <strong style="font-size: 10px; color: crimson;">NP</strong>' : '') . '</td>';
                                                                            }

                                                                            $found = true;
                                                                            break;
                                                                        }
                                                                    }
                                                                    if (!$found)
                                                                        echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                                                }
                                                            } else
                                                                echo '<td title="" style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                                        }

                                                        
                                                        echo '</tr>';
                                                        ?>

                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>


                                        </div>
                                    <?php endif; ?>
                                        <?php if (count($female_candidates)): ?>
                                        
                                        <p><br style="page-break-before: always;" clear="all" /></p>
                                        <p style="page-break-before: always;"></p>
                                        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                                            THE MINISTRY OF EDUCATION<br/>
                                            <img src="<?= $edc_logo ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span><br/>
                                            <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>                             
                                        </h4>
                                        <br/>
                                        <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <strong>ZONE:</strong> <?= $zone->zonename; ?> <br/>
                                                    <strong>SCHOOL:</strong> <?= $school->schoolname; ?> <strong> Code:</strong> <?= $school->schoolcode; ?><br/>
                                                    <strong>TOTAL CANDIDATES:</strong> <?= count($female_candidates); ?><br/>
                                                </div>
                                            </div>


                                            <table width="100%" class="table table-bordered table-condensed table-hover" id="gradeTable">
                                                <thead>
                                                    <tr>
                                                        <th>Sn</th>
                                                        <th>&nbsp;</th>
                                                        <th>&nbsp;</th>
                                                        <th>&nbsp;</th>

                                                        <?php
                                                        if (count($allsubjects)) {
                                                            foreach ($allsubjects as $subjects) {
                                                                echo '<th style="font-size: 14px">' . ucwords(strtolower($subjects->subjectname)) . '</th>';
                                                            }
                                                        }
                                                        ?>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $row = 0;
                                                    $ca_num = 0;
                                                    $prac_num = 0;
                                                    $exam_num = 0;

                                                    foreach ($female_candidates as $candidate):
                                                        ++$row;

                                                        $name = strtoupper($candidate->firstname . ' ' . $candidate->othernames);

                                                        //PRACTICAL ROW
                                                        echo '<tr>';
                                                        echo '<td>' . $row . '</td>';
                                                        echo '<td>' . $candidate->examno . '</td>';
                                                        echo '<td>' . $name . '</td>';
                                                        echo '<td>' . $candidate->gender . '</td>';

                                                        if (count($allsubjects)) {

                                                            $registered_subjects = $this->reg_subjects_model->get_registered_subjects($candidate->candidateid, $examid, $examyear);
                                                            if (count($registered_subjects)) {
                                                                foreach ($allsubjects as $subjects) {
                                                                    foreach ($registered_subjects as $reg_subject) {
                                                                        $found = false;
                                                                        if ($reg_subject->subjectid == $subjects->subjectid) {

                                                                            $exam_data = array();
                                                                            foreach ($c_scores as $scoresvalue) {
                                                                                if (($scoresvalue->subjectid == $subjects->subjectid) && ($scoresvalue->candidateid == $candidate->candidateid)) {
                                                                                    $exam_data = $scoresvalue;
                                                                                    break 1;
                                                                                }
                                                                            }


                                                                            $practicalcore = !count($exam_data) ? 0 : $exam_data->practical_score;
                                                                            $examscore = !count($exam_data) ? 0 : $exam_data->exam_score;
                                                                            $cascore = !count($exam_data) ? 0 : $exam_data->ca_score;


                                                                            if ($subjects->haspractical == 1) {
                                                                                echo '<td>' . ($examdetail->hasca ? (($cascore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $cascore) . ' + ') : '') . ($examscore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $examscore) . ($examdetail->haspractical ? (' + ' . ($practicalcore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $practicalcore)) : '') . '</td>';
                                                                            } else {
                                                                                echo '<td>' . ($examdetail->hasca ? (($cascore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $cascore) . ' + ') : '') . ($examscore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $examscore) . ($examdetail->haspractical ? ' + <strong style="font-size: 10px; color: crimson;">NP</strong>' : '') . '</td>';
                                                                            }

                                                                            $found = true;
                                                                            break;
                                                                        }
                                                                    }
                                                                    if (!$found)
                                                                        echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                                                }
                                                            } else
                                                                echo '<td title="" style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                                        }

                                                        
                                                        echo '</tr>';
                                                        ?>

                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>


                                        </div>
                                    <?php endif; ?>
                                    <p style="page-break-before: always;">
                                    <?php endforeach; ?>
                            </div>

                            <p style="page-break-before: always;"></p>
                            <?php
                        endif;
                    endforeach;
                    ?>

                </div>
            </div>
        </div>

        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>   
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script> 

    </body>
</html>
