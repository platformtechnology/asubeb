<!DOCTYPE html>
<html>
  <head>
    <title><?= $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .css-vertical{
            filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                 -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
             -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
              -ms-transform: rotate(-90.0deg);  /* IE9+ */
               -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
          -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                  transform: rotate(-90.0deg);  /* Standard */
                  white-space:nowrap;
        }
    </style>
    <script lang="javascript" type="text/javascript">
        window.addEventListener('load', function () {
            var rotates = document.getElementsByClassName('css-vertical');
            for (var i = 0; i < rotates.length; i++) {
               rotates[i].style.height = (rotates[i].offsetWidth) + 'px';
            }
        });
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
            
    </script>
  </head>
    <body>
  	<div class="header" style="background-color: <?= $edc_detail->themecolor ?>;">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-8">
	              <div class="logo" style="width: 370px;">
                           <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo?>" alt="Edc Logo" border="0" /> </div>
                           <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
	              </div>
	           </div>  
                    
                   <div class="col-md-4 pull-right">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="<?= site_url('admin/password/change');?>">Change Password</a></li>
                                  <li><a href="<?= site_url('admin/login/logout');?>">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>
        <?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>
       <div class="page-content">
           <div class="row">
               <div class="container">
                   <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
               </div>
               <div class="container" id="printdiv">

                   <div class="row">
                        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                             THE MINISTRY OF EDUCATION<br/>
                             <img src="<?= $edc_logo?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                             <br/>
                             <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>
                             
                        </h4>
                      
                        <br/>
                        <div class="col-md-2"></div>
                        <div class="col-md-8" style="background-color: white; padding: 20px; border-radius: 10px">
<!--                            <p>
                                <?php 
                                $schl = $this->registration_model->get_school($candidates[0]->schoolid, true); ?>
                                <strong>Zone:</strong> <?= $this->schools_model->get_zone_name($candidates[0]->zoneid); ?> <br/>
                                <strong>LGA:</strong> <?= $this->registration_model->get_lga($candidates[0]->lgaid); ?><br/>
                                <strong>School Name:</strong> <?= $schl->schoolname ?> <br/><strong>School Code:</strong> <?= $schl->schoolcode; ?><br/>
                                <hr/>
                            </p>-->
                           
                                <strong>CANDIDATE EXAMINATION REPORT</strong>
                                 <hr/>
                          
                            <table class="table-condensed">
                                
                                <?php $candidate = $candidates[0]; ?>
                              
                                    <tr>
                                        <td colspan="2" align="center">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><strong>EXAMINATION NUMBER</strong></td>
                                        <td><?= $candidate->examno ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>CANDIDATE NAME</strong></td>
                                        <td><?= strtoupper($candidate->firstname.' '.$candidate->othernames); ?></td>
                                    </tr>

                                    <tr>
                                        <td><strong>EXAMINATION TYPE</strong></td>
                                        <td><strong><?= strtoupper($examdetail->examdesc. ' (' . $examdetail->examname .') '. $examyear); ?></strong></td>
                                    </tr>

                                    <tr>
                                        <td><strong>SCHOOL</strong></td>
                                        <td><?= strtoupper($this->registration_model->get_school($candidates[0]->schoolid))?></td>
                                    </tr>
                                   
                                    <tr>
                                        <td colspan="2"><br/><strong>SUBJECT GRADES<br/></strong></td>
                                    </tr>
                                     <tr>
                                         <td><strong>SUBJECTS</strong></td>
                                         <td><strong><?= $examdetail->hasposting ? 'SCORES' : 'GRADES'; ?></strong></td>
                                    </tr>
                                     <?php 
                                        $total = 0;
                                        if($examdetail->hasposting): 
                                            if(count($allsubjects)):
                                                
                                                foreach ($allsubjects as $subjs):
                                                    $score = $this->report_model->get_score_per_subject($candidate->candidateid, $subjs->subjectid, $examid, $examyear);
                                                    $total += $score;
                                     ?>
                                                    <tr>
                                                        <td><strong><?= ($subjs->iscompulsory ? '*': '') .strtoupper($subjs->subjectname); ?></strong></td>
                                                        <td><strong><?= $score ?></strong></td>
                                                   </tr>
                                     <?php 
                                                endforeach;
                                            endif;
                                            
                                        else:
                                            $registered_subjects = $this->reg_subjects_model->get_registered_subjects($candidate->candidateid, $examid, $examyear); 
                                            if(count($allsubjects)):
                                                foreach ($allsubjects as $subjs):
                                      ?>
                                                   <tr>
                                                        <td><strong><?= ($subjs->iscompulsory ? '*': '') .strtoupper($subjs->subjectname); ?></strong></td>
                                                        <?php 
                                                        if(count($registered_subjects)){
                                                            foreach ($registered_subjects as $reg_subject) {
                                                               $found = false;
                                                               if($reg_subject->subjectid == $subjs->subjectid){
                                                                   $remark_detail = $this->report_model->get_subject_remark_per_candidate($subjs->subjectid, $candidate->candidateid, $examid, $examyear);

                                                                   echo '<td><strong>'.(count($remark_detail) ? strtoupper($remark_detail->grade) : '---').'</strong></td>';

                                                                   $found = true;
                                                                   break;
                                                               }
                                                           }

                                                           if(!$found) echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                                           
                                                        }else echo '<td title="" style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                                        ?>
                                                   </tr>
                                      <?php
                                                endforeach;
                                            endif;
                                        endif; 
                                      ?>
                                    <tr>
                                        <td colspan="2" align="center">&nbsp;</td>
                                    </tr>
                                     <?php if($examdetail->hasposting): ?>
                                        <tr>
                                            <td><strong>TOTAL</strong></td>
                                            <td><?= $total; ?></td>
                                        </tr>
                                    <?php endif; 
                                        $status = $this->report_model->get_passcount_basedon_criteria_per_candidate($candidate->candidateid, $examid, $examyear, $cutoff);
                                    ?>
                                    <?php if($examdetail->hasposting && $status == 'PASSED'): 
                                            $postedto = $this->registration_model->get_school($candidate->placement)
                                      ?>
                                        <tr>
                                            <td><strong>SCHOOL POSTED</strong></td>
                                            <td><strong><?= $postedto == '' ? 'Not Posted' : $postedto; ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                    <tr>
                                        <td><strong>STATUS</strong></td>
                                        <td><strong><?= $status; ?></strong></td>
                                        <td align="left"><img src="<?= get_img('edc_logos/'.$this->edcid.'_stamp.png') ?>" alt="Stamp" style="width: 150px; height: 120px" /></td>
                                    </tr>

                            </table>
                           
                          <p style="page-break-before: always;"></p>
                          
                        </div>
                      
                   </div>

                </div>
            </div>
        </div>
     
    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>   
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script> 
    
   </body>
</html>
