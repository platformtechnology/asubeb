<!DOCTYPE html>
<html>
  <head>
    <title><?= $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script lang="javascript" type="text/javascript">
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }

    </script>
  </head>
   <body>
  	<div class="header" style="background-color: <?= $edc_detail->themecolor ?>;">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-8">
	              <div class="logo" style="width: 370px;">
                           <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo?>" alt="Edc Logo" border="0" /> </div>
                           <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
	              </div>
	           </div>

                   <div class="col-md-4 pull-right">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="<?= site_url('admin/password/change');?>">Change Password</a></li>
                                  <li><a href="<?= site_url('admin/login/logout');?>">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>
       <?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>
       <div class="page-content">
           <div class="row">
               <div class="container">
                   <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
               </div>
               <div class="container" id="printdiv">
                   <?php
                        foreach($zones as $zone):
                            if(@trim($schoolid) == false){
                                $schools = $this->report_model->get_posting_schools_per_zone($zone->zoneid);
                            }
                            else $schools = $schooldetails;
                   ?>
                   <div class="row">

                       <?php
                            foreach($schools as $school):
                                $this->db->where('examyear',$examyear);
                                $this->db->where('examid',$examid);
                                $this->db->where('placement',$school->schoolid);
                                $candidates = $this->db->get('t_candidates')->result();

                       ?>
                        <br/>
                        <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                             THE MINISTRY OF EDUCATION<br/>
                             <img src="<?= $edc_logo?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                             <br/>
                             <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>
                             <br/> <?= strtoupper($zone->zonename) ?> ZONE

                        </h4>
                            <p style="font-size:16px">
                                <strong>ZONE:</strong> <?= $zone->zonename; ?> <br/>
                                <strong>SCHOOL POSTED TO:</strong> <?= $school->schoolname; ?> <strong> Code:</strong> <?= $school->schoolcode; ?><br/>
                                <strong>SCHOOL CAPACITY:</strong> <?= $school->school_capacity ?><br/>
                                <strong>TOTAL CANDIDATES POSTED:</strong> <?= count($candidates); ?><br/>
                            </p>
                            <table width="100%" class="table table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>EXAMNO</th>
                                        <th>NAME</th>
                                        <th>CURRENT SCHOOL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $sn = 0;
                                        foreach($candidates as $students):
                                            $name = ucwords($students->firstname . ' ' . $students->othernames);
                                    ?>
                                    <tr>
                                        <td><?= ++$sn; ?></td>
                                        <td><?= $students->examno ?></td>
                                        <td><?= $name ?></td>
                                        <td><?= $this->report_model->getSchoolName($students->schoolid) ?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <p style="page-break-before: always;"></p>
                       <?php endforeach; ?>
                   </div>

                    <p style="page-break-before: always;"></p>
                <?php
                    endforeach;
                ?>

                </div>
            </div>
        </div>

    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

   </body>
</html>
