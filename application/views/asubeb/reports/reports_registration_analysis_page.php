<!DOCTYPE html>
<html>
  <head>
    <title><?= $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .css-vertical{
            filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                 -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
             -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
              -ms-transform: rotate(-90.0deg);  /* IE9+ */
               -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
          -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                  transform: rotate(-90.0deg);  /* Standard */
        }
    </style>
    <script lang="javascript" type="text/javascript">
        window.addEventListener('load', function () {
            var rotates = document.getElementsByClassName('css-vertical');
            for (var i = 0; i < rotates.length; i++) {
                rotates[i].style.height = rotates[i].offsetWidth + 'px';
            }
        });
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
            
    </script>
  </head>
   <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-8">
	              <div class="logo" style="width: 370px;">
                           <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo?>" alt="Edc Logo" border="0" /> </div>
                           <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
	              </div>
	           </div>  
                    
                   <div class="col-md-4 pull-right">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="<?= site_url('admin/password/change');?>">Change Password</a></li>
                                  <li><a href="<?= site_url('admin/login/logout');?>">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>
       <div class="page-content">
           <div class="row">
               <div class="container">
                   <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
               </div>
               <div id="printdiv">
                   <?php
                        foreach($report_data as $lgadata):
                           $schools = $lgadata['schooldata'];
                   ?>
                   <div class="row">
                        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                             THE MINISTRY OF EDUCATION<br/>
                             <img src="<?= $edc_logo?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                             <br/>
                             <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($exam_detail->examname); ?> </span> <?= $examyear; ?> <br/>
                             <br/> <?= strtoupper($lgadata['lganame']) ?> LOCAL GOVERNMENT AREA
                             
                        </h4>

                       <br/>
                       <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                           <table width="100%" class="table-bordered">
                               
                               <tr  style="text-align: center; font-weight: bold;">
                                   <td rowspan="2" align="center" >SCHOOLS</td>
                                    <td rowspan="<?= 3 + count($schools); ?>" >&nbsp;</td>
                                    <td colspan="<?= (count($subjects)*2); ?>" align="center">SUBJECTS</td>
                              </tr>
                              
                            <!--SUBJECTS HEADER ROW-->
                              <tr>
                              
                                <?php 
                                   foreach ($subjects as $subject):
                                ?>
                                    <td valign="middle"><?= ucwords(strtolower($subject->subjectname))?></td>
                                    <td rowspan="<?= 2 + count($schools); ?>">&nbsp;</td>
                                    
                                <?php endforeach; ?>
                                
                              </tr>
                              <!-- END SUBJECTS HEADER ROW-->
                             
                               
                               <!--  EMPTY ROW-->
                               <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                               <!-- END EMPTY ROW-->
                               
                               
                               <!-- SCHOOL ROW-->
                               
                               <?php 
                               $total = array();
                                foreach($schools as $school):
                                ?>
                                <tr>
                                    <td><strong><?= $school['schoolname'] ?></strong></td>

                                     <?php 
                                        foreach ($subjects as $subjectdata):          
                                     ?>
                                              <td><strong><?= round($school[$subjectdata->subjectid]); ?></strong></td>
                                     <?php    
                                         endforeach; 
                                     ?>
                                </tr>   
                               <?php endforeach; ?>                               
                             
                               
                              
                           </table>                  
                       </div>
                   </div>
                   
                    <p style="page-break-before: always;"></p>
                   <?php endforeach; ?>
                   
                   
                   
                </div>
            </div>
        </div>
     
    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>   
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script> 
    
   </body>
</html>
