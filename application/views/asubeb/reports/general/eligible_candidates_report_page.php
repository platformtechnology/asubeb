<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script lang="javascript" type="text/javascript">
            function printDiv(divID) {
                //Get the HTML of div
                var divElements = document.getElementById(divID).innerHTML;
                //Get the HTML of whole page
                var oldPage = document.body.innerHTML;

                //Reset the page's HTML with div's HTML only
                document.body.innerHTML =
                        "<html><head></head><body>" +
                        divElements + "</body> </html>";

                //Print Page
                window.print();

                //Restore orignal HTML
                document.body.innerHTML = oldPage;
            }

        </script>
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="logo" style="width: 370px;">
                            <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo ?>" alt="Edc Logo" border="0" /> </div>
                            <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
                        </div>
                    </div>

                    <div class="col-md-4 pull-right">
                        <div class="navbar navbar-inverse" role="banner">
                            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
                                        <ul class="dropdown-menu animated fadeInUp">
                                            <li><a href="<?= site_url('admin/password/change'); ?>">Change Password</a></li>
                                            <li><a href="<?= site_url('admin/login/logout'); ?>">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="container">
                    <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
                </div>
                <div class="container" id="printdiv">
                    <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                        THE MINISTRY OF EDUCATION<br/>
                        <img src="<?= get_img('edc_logos/' . $edc_detail->edclogo) ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                        <br/>
                        <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($exam_detail->examname); ?> </span> <?= $examyear; ?>
                    </h4>

                    <br/>
                    <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                        <table class="table table-bordered">
                            <caption> <h2>Analysis using a cut-off mark of : <?php echo $cutoff; ?> </h2></caption>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>LOCAL GOVT NAME</th>
                                    <th>NO REGISTERED</th>
                                    <th>NO SAT</th>
                                    <th>NO ELIGIBLE FOR POSTING</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sn = 0;
                                $total_reg = $total_sat = $total_pass = $total_absent = $total_failed = 0;
                                $pass_count = 0;
                                if (count($report)) {
                                    $lg = array(); //HOLDS LGA FULL DATA
                                    $lgids = array(); //HOLDS LGA IDS
                                    foreach ($report as $value) {
                                        $lgids[] = $value->lgaid;
                                        $lg['lgid'][$value->lgaid] = $value->lgaid;
                                        $lg['lgname'][$value->lgaid] = $value->lganame;
                                        $lg['registeredcount'][$value->lgaid] = $value->registeredcount;
                                        $lg['numsat'][$value->lgaid] = $value->numsat;
                                    }
                                    //GPT AND TPE
                                    if ($exam_detail->hasposting) {
                                        $pass_count = $this->report_model->getLgaPassedCount(null, $examid, $examyear, $exam_detail->hasposting, $cutoff);
                                    }
                                    $finalPass = 0;
                                    foreach ($report as $value) {
                                        #get the number of people that passed
                                        //GPT
                                        if ($exam_detail->hasposting) {
                                            if (count($pass_count)) {
                                                foreach ($pass_count as $key => $passs) {
                                                    $found = false;
                                                    if ($value->lgaid == $passs['lgaid']) {
                                                        $finalPass = $passs['num'];
                                                        unset($pass_count[$key]);
                                                        $found = true;
                                                        break;
                                                    }
                                                }

                                                if (!$found)
                                                    $finalPass = 0;
                                            }
                                            else{
                                                    $finalPass = 0;
                                            }

                                        }
                                        //
                                        else {
                                            $finalPass = isset($pass_count[$value->lgaid]) ? $pass_count[$value->lgaid] : '0';
                                        }
                                        #Get the totals
                                        $total_reg += $value->registeredcount;
                                        $total_sat += $value->numsat;
                                        $total_pass += $finalPass;
                                        $total_absent += $absentia_count;


                                        $total_failed = $total_sat - $total_pass;
                                        ?>
                                        <tr>
                                            <td><?= ++$sn; ?></td>
                                            <td><?= strtoupper($value->lganame); ?></td>
                                            <td><strong><?= number_format($value->registeredcount); ?></strong></td>
                                            <td><strong><?= number_format($value->numsat); ?></strong></td>
                                            <td><strong><?= $finalPass >= 0 ? @number_format($finalPass):0; ?></strong></td>
                                        </tr>
                                        <?php
                                    }

                                   //$total_absent= $total_absent-1000;
                                 //  $total_pass = $total_pass + 1000;
                                 // $total_sat = $total_sat + 1000;
                                  //  ?>
                                <table align="center" class="table-bordered" cellpadding="7" width="70%">
                                    <thead>
                                        <tr>
                                            <th>TOTAL REGISTERED</th>
                                            <th>TOTAL PRESENT</th>
                                            <th colspan="2">TOTAL ELIGIBLE FOR POSTING</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?= number_format($total_reg); ?></td>
                                            <td><?= number_format($total_sat); ?></td>
                                            <td><?= number_format($total_pass); ?></td>
                                            <td style="font-weight: bolder;"><?= ($total_sat == 0 ) ? '0' : round((($total_pass / $total_sat) * 100),2) . '%' ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </tbody>
    <?php
}
?>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

    </body>
</html>
