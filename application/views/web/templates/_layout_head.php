<!DOCTYPE HTML>
<html>
    <head>
        <title>Anambra State Universal Basic Education Board</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="<?php echo base_url('resources/web/css/bootstrap.css') ?>" rel='stylesheet' type='text/css' />
        <link href='//fonts.googleapis.com/css?family=Open+Sans:700,700italic,800,300,300italic,400italic,400,600,600italic' rel='stylesheet' type='text/css'>
        <!--Custom-Theme-files-->
        <link href="<?php echo base_url('resources/web/css/owl.carousel.css') ?>" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url('resources/web/css/style.css') ?>" rel='stylesheet' type='text/css' />
        <script src="<?php echo base_url('resources/web/js/jquery.min.js') ?>"></script>
        <!--/script-->
        <script type="text/javascript" src="<?php echo base_url('resources/web/js/bootstrap.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('resources/web/js/move-top.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('resources/web/js/easing.js') ?>"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 900);
                });
            });
        </script>
        <!--gallery-->
        <script src="<?php echo base_url('resources/web/js/jquery.chocolat.js') ?>"></script>
        <link rel="stylesheet" href="<?php echo base_url('resources/web/css/chocolat.css') ?>" type="text/css" media="all" />
        <!--light-box-files -->
        <script type="text/javascript">
            $(function () {
                $('#example1 a').Chocolat();
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#portfolio a').Chocolat();
            });
        </script>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>

    </head>
    <body>
        <!--start-home-->
        <div id="home" class="header">
            <div class="header-top">
                <div class="container">
                    <span class="menu"></span>
                    <div class="top-menu">
                        <ul class="cl-effect-16">
                            <li><a class="active" href="http://asubeb.net" target="_blank" data-hover="">Asubeb Homepage</a></li>
                            <li><a class="active" href="<?php echo site_url(''); ?>" title="Click Here  to go back to the Index page" data-hover="">HOME</a></li>
                            <li><a class="active" href="<?php echo site_url('web/pages/news'); ?>" title="Click Here for Latest News" data-hover="">LATEST NEWS</a></li>
                        </ul>
                    </div>
                    <!-- script-for-menu -->
                    <script>
                        $("span.menu").click(function () {
                            $(".top-menu").slideToggle("slow", function () {
                                // Animation complete.
                            });
                        });
                    </script>
                    <div class="clearfix"></div>
                    <!--End-top-nav-script-->
                </div>
            </div>
            <div class="header-middle">
                <a href="<?php echo site_url('')?>">
                <div class="container">
                    <div class="col-md-2 col-sm-12 col-xs-12" align = "center">
                        <img src="<?php echo base_url('resources/web/images/logo.gif') ?>" alt=""/>
                    </div>
                    <div  class="col-md-8" align = "center">
                        <h4 style="color:green"> Government of Anambra State</h4>
                        <h2 style="font-size: 2em;padding-top: 1px;padding-bottom: 1px; color: #003366"> Anambra State Universal Basic Education Board (ASUBEB), Awka. </h2>
                        <h3 style="color:crimson">  e-Placement Portal </h3>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12" align = "center">
                        <img src="<?php echo base_url('resources/web/images/asubeb_logo.gif') ?>" alt=""/>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                </a>
            </div>
        </div>