       <!--/footer-->
        <div class="footer">
            <div class="container">
                <div class="footer-top">
                    <div class="col-md-4 footer-grid">
                        <h4>Contact Phone</h4>
                        <ul class="bottom">
                            <li><i class="glyphicon glyphicon-earphone"></i> Phone: +2348181718456 </li>
                        </ul>
                    </div>
                    <div class="col-md-4 footer-grid">
                        <h4>E-mail</h4>
                        <ul class="bottom">
                            <li><i class="glyphicon glyphicon-envelope"></i><a href="mailto:info@example.com">info@asubebanambra.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 footer-grid">
                        <h4>Address Location</h4>
                        <ul class="bottom">
                            <li><i class="glyphicon glyphicon-map-marker"></i>ASUBEB, Awka, Anambra State </li>

                        </ul>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <div class="copy">
            <p>Anambra Asubeb &copy; 2016. All Rights Reserved | Powered By <a href="http://platformtechltd.com/" title="Platform is a Leader in information technology solutions in Nigeria and across the Globe" data-toggle = "tooltip">The Platform Technology LTD</a></p>
        </div>
        <!--//footer-->
        <!--start-smooth-scrolling-->
        <script type="text/javascript">
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
            $(document).ready(function () {
                /*
                 var defaults = {
                 containerID: 'toTop', // fading element id
                 containerHoverID: 'toTopHover', // fading element hover id
                 scrollSpeed: 1200,
                 easingType: 'linear'
                 };
                 */

                $().UItoTop({easingType: 'easeOutQuart'});

            });
        </script>
        <a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>


    </body>
</html>