  <div style="background-color: #f4f4f4; padding: 2em;">
            <div class="container" >
                <div class ="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-4">
                            <div class ="panel panel-primary" >
                                <div class="panel-heading" style = "background: #003366;">
                                    <h4> Check Posting </h4>
                                </div>
                                <div class ="panel-body">
                                    <?php echo form_open(site_url('web/pages/checkresult'), 'class=""'); ?>
                                    <div class ="form-group col-md-12" style="margin-bottom: 5px">
                                            <label>
                                                Examination Type
                                            </label>
                                            <div>
                                                <?php
                                                    $exam_options = array();
                                                    foreach($exams as $exam):
                                                        if($exam->examid == '60djn6zke0'):
                                                            $exam_options[$exam->examid] = $exam->examname;
                                                        endif;
                                                    endforeach;
                                                    echo form_dropdown($name = "examid", $options = $exam_options,  set_value('examid'), $extra = 'class = "form-control" required = ""');
                                                ?>

                                            </div>
                                        </div>
                                        <div class ="form-group col-md-12" style="margin-bottom: 5px">
                                            <label>
                                                Examination Year
                                            </label>
                                            <div>
                                                <?php
                                                $start_year = 2016;
                                                $active_year = 2017;
                                                ?>
                                                <select class="form-control" name="examyear" required="" title="Select Examination Year" data-toggle="tooltip">
                                                    <option value=""> Select Exam Year </option>
                                                    <?php
                                                       for($year = $start_year; $year <= $active_year; $year++ ){
                                                    ?>
                                                    <option value="<?php echo $year ?>"> <?php echo $year ?> </option>
                                                    <?php
                                                       }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class ="form-group col-md-12" style="margin-bottom:5px !important;">
                                            <label>
                                                Examination Number
                                            </label>
                                            <div>
                                                <input type ="text" name="examno" class = "form-group" required="" title="Enter Exam Number Here" style = "width:100%" data-toggle="tooltip"/>
                                            </div>
                                        </div>
                                        <div class ="form-group col-md-12" style = "margin-top: -10px;">
                                            <label>
                                                Pin. Number
                                            </label>
                                            <div>
                                                <input type ="password" name="pin" class = "form-group" required="" title="Enter PIN Here" style = "width:100%" data-toggle="tooltip"/>
                                            </div>
                                        </div>
                                        <div class ="form-group col-md-12">
                                            <button class="btn btn-primary" type="submit" data-toggle="tooltip" title="Click Here to Check Posting"> Check Placement <i class="glyphicon glyphicon-eye-open"></i> </button>
                                            <button class="btn btn-danger" type="reset"> Reset <i class="glyphicon glyphicon-refresh"></i> </button>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <img src="<?php echo base_url('resources/web/images/asubeb_building.jpg') ?>" alt="" style = "width:100%" >
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>

        <!--welcome-->
        <div class="welcome">
            <div class="container">
                <div class="row">
                    <div class="col-md-4" style="max-height: 500px">
                        <h1>Latest News</h1>
                        <marquee direction ="up" scrolldelay="300">
                        <?php if(isset($existing_news) && !empty($existing_news)) :
                                foreach ($existing_news as $news):?>
                                <h4>
                                    <strong>
                                        <?php echo $news->newstitle; ?>
                                    </strong>
                                </h4>
                                <p>
                                    <?php echo substr($news->news,0,700); ?>... <a href=""> Read More </a>
                                </p>
                        <?php
                                endforeach;
                            endif;
                        ?>
                        </marquee>
                    </div>
                    <div class="col-md-8">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                How to Check your Placement
                            </div>
                            <div class="panel-body">
                                <ol style="line-height: 2em">
                                    <li> Select the Exam Type <span style="color:crimson"> (i.e TPE) </span></li>
                                    <li> Select the Exam Year </li>
                                    <li> Enter Examination Number  <span style="color:crimson">(This is  your 4-digit Centre Number /your 3-digit Candidate number e.g  4001/001 or NFE4002/001)</span></li>
                                    <li> Enter the Personal Identification Number(PIN) on your Scratch card </li>
                                    <li> Click on Check Placement to view your Placement</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//welcome-->
        <!--about-->
        <div class="about">
            <div class="container">
                <h3 class="tittle"></h3>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>

        <!-- requried-jsfiles-for owl -->
        <script src="js/owl.carousel.js"></script>
        <script>
                        $(document).ready(function () {
                            $("#owl-demo").owlCarousel({
                                items: 4,
                                lazyLoad: true,
                                autoPlay: false,
                                navigation: true,
                                navigationText: true,
                                pagination: false,
                            });
                        });
        </script>
        <!-- //requried-jsfiles-for owl -->
        <!--start-news-->
