
        <!--welcome-->
        <div class="welcome">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Latest News</h1>
                    </div>
                    <div class="col-md-12" style="max-height: 500px">
                        <?php if(isset($existing_news) && !empty($existing_news)) :
                                foreach ($existing_news as $news):?>
                                <div class="col-md-12">
                                        <h4>
                                            <strong>
                                                <?php echo $news->newstitle; ?>
                                            </strong>
                                            <div> <i class="glyphicon glyphicon-calendar"></i> <?php  echo substr($news->datemodified,0,10); ?></div>
                                        </h4>
                                        <p>
                                            <?php echo substr($news->news,0,700); ?>...<br/> <a href="<?php echo site_url('web/pages/news/'.$news->newsid); ?>"> <button class="btn btn-primary"> <i class="glyphicon glyphicon-eye-open"></i> Read More </button> </a>
                                        </p>
                                </div>
                        <?php
                                endforeach;
                            endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!--//welcome-->
        <!--about-->
        <div class="about">
            <div class="container">
                <h3 class="tittle"></h3>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>

        <!-- requried-jsfiles-for owl -->
        <script src="js/owl.carousel.js"></script>
        <script>
                        $(document).ready(function () {
                            $("#owl-demo").owlCarousel({
                                items: 4,
                                lazyLoad: true,
                                autoPlay: false,
                                navigation: true,
                                navigationText: true,
                                pagination: false,
                            });
                        });
        </script>
        <!-- //requried-jsfiles-for owl -->
        <!--start-news-->
