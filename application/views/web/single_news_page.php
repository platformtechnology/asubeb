
        <!--welcome-->
        <div class="welcome">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Latest News</h1>
                        <div> <a href="<?php echo site_url('web/pages/news'); ?>"> <button class="btn btn-primary"> <i class="glyphicon glyphicon-eye-open"></i> Show All News </button> </a>  </div>
                    </div>
                    <div class="col-md-12" style="padding:2em">
                        <?php if(isset($news) && !empty($news)) : ?>
                                <div class="col-md-12">
                                        <h4>
                                            <strong>
                                                <?php echo $news->newstitle; ?>
                                            </strong>
                                            <div> <i class="glyphicon glyphicon-calendar"></i> <?php  echo substr($news->datemodified,0,10); ?></div>
                                        <p>
                                            <?php echo $news->news; ?>
                                        </p>
                                        </h4>
                                </div>
                        <?php
                            endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!--//welcome-->
        <!--about-->
        <div class="about">
            <div class="container">
                <h3 class="tittle"></h3>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>

        <!-- requried-jsfiles-for owl -->
        <script src="js/owl.carousel.js"></script>
        <script>
                        $(document).ready(function () {
                            $("#owl-demo").owlCarousel({
                                items: 4,
                                lazyLoad: true,
                                autoPlay: false,
                                navigation: true,
                                navigationText: true,
                                pagination: false,
                            });
                        });
        </script>
        <!-- //requried-jsfiles-for owl -->
        <!--start-news-->
