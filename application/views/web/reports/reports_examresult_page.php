
<div class="container"></div>
<?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>
<div class="page-content">
    <div class="row">
        <div class="container" style="padding:1em">
            <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
            <div class=" pull-right">
                <a href="<?php echo site_url('web/pages')?>"><button class="label label-danger"><i class="glyphicon glyphicon-log-out"></i> Log Out</button></a>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="container" id="printdiv" style="border: #000 solid 2px; ">

            <?php
            if (count($candidate)):

                $passport = './passports/' . $candidate->candidateid . '.jpg';
                if (!file_exists($passport)){
                    $passport = get_img('no_image.jpg');
                }
                ?>

                <div class="row">
                    <div class="col-md-12" style="width:100%">

                    <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                        The Ministry Of Education<br/>
                        <img src="<?php echo base_url('resources/web/images/asubeb_logo.gif') ?>" alt="" width ="100px"/>
                        <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                        <img src="<?= $edclogo; ?>" alt="Edc Logo" width ="100px" />

                        <br/>
    <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>

                    </h4>
                    </div>

                    <br/>
                    <div class="span2"></div>
                    <div class="span8" style="background-color: white; padding: 20px; border-radius: 10px; border: 1px">
                        <div align ="center"> <strong>CANDIDATE PLACEMENT REPORT</strong> </div>
                        <hr/>

                        <table class="table-condensed">

                            <tr>
                                <td colspan="2" align="center">&nbsp;</td>
                            </tr>
                            <tr>
                                <td><strong>EXAMNO:</strong></td>
                                <td><?= $candidate->examno ?></td>
                                <td rowspan="5">
                                    <?php if (file_exists($passport)): ?>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 110px; height: 110px;"><img src="<?= $passport; ?>" width="100px" height="120px" alt="Passport" /></div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    </div>
                                    <?php
                                        endif;
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>CANDIDATE NAME:</strong></td>
                                <td style="color:black"> <strong > <?= strtoupper($candidate->firstname . ' ' . $candidate->othernames); ?> </strong> </td>
                            </tr>
                            <tr>
                                <td><strong>EXAMINATION TYPE:</strong></td>
                                <td><strong><?= strtoupper($examdetail->examdesc . ' (' . $examdetail->examname . ') ' . $examyear); ?></strong></td>
                            </tr>

                            <tr>
                                <td><strong>SCHOOL:</strong></td>
                                <td><?= strtoupper($this->registration_model->get_school($candidate->schoolid)) ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2"><br/><strong>SUBJECT GRADES<hr/></strong></td>
                            </tr>
                            <tr>
                                <td><strong>SUBJECTS</strong></td>
                                <td><strong><?= $examdetail->hasposting ? 'SCORES' : 'GRADES'; ?></strong></td>
                            </tr>
                            <?php
                            $total = 0;
                            $resultReleased = false;
                            if (count($allsubjects)):

                                foreach ($allsubjects as $subjs):
                                    $score_data = $this->report_model->get_score_per_subject($candidate->candidateid, $subjs->subjectid, $examid, $examyear, false);
                                    if ($score_data) {
                                        $total += $score_data;
                                    }
                                endforeach;
                                if ($total > 0)
                                    $resultReleased = true;
                            endif;

                            $compulsorySubjects = $this->report_model->getCompulsorySubjectsToPass($examid, $examyear);
                            $total = 0;

                            if ($resultReleased):
                                if ($examdetail->hasposting):
                                    if (count($allsubjects)):

                                        foreach ($allsubjects as $subjs):
                                            $score_data = $this->report_model->get_score_per_subject($candidate->candidateid, $subjs->subjectid, $examid, $examyear, false);
                                            $total += $score_data;

                                            $score = $score_data;
                                            if ((intval($score) == 0) && (intval($score_data->exam_score) < 0))
                                                $score = '<span style="color: crimson;">ABS</span>';
                                            ?>
                                            <tr>
                                                <td><strong><?= ($subjs->iscompulsory ? '*' : '') . strtoupper($subjs->subjectname); ?></strong></td>
                                                <td><strong><?= $score ?></strong></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                    <?php
                                endif;
                                ?>
                                <tr>
                                    <td colspan="2" align="center">&nbsp;</td>
                                </tr>
        <?php if ($examdetail->hasposting): ?>
                                    <tr>
                                        <td><strong>TOTAL</strong></td>
                                        <td> <strong> <?= $total; ?> </strong> </td>
                                    </tr>
                                <?php
                                endif;
                                $status = $this->report_model->get_passcount_basedon_criteria_per_candidate($candidate->candidateid, $examid, $examyear, $cutoff);
                                ?>
                                <?php
                                if ($examdetail->hasposting && $status == 'PASSED'):
                                    //echo $candidate->placement;
                                    $postedto = $this->registration_model->get_school($candidate->placement)
                                    ?>
                                    <tr>
                                        <td><strong>SCHOOL POSTED</strong></td>
                                        <td><strong><?= $postedto == '' ? 'Not Posted' : $postedto; ?></strong></td>
                                    </tr>
        <?php endif; ?>
                                <tr>

                                    <td align = "center" colspan = '3'> <img src="<?= base_url(str_replace('./', '', config_item('edc_logo_path')) . $this->edcid . '_stamp.png') ?>" alt="Stamp" style="width: 150px; height: 120px" /> </td>
                                </tr>


                                <?php
                            else:

                                echo '<tr>
                                                                <td colspan="3">
                                                                <br/><br/>
                                                                    <strong style="color: crimson;">AWAITING RESULT - CHECK BACK LATER</strong>
                                                                    <hr/>
                                                                </td>
                                                              </tr>
                                                            ';

                            endif;
                            ?>
                        </table>


                    </div>

                </div>

                <?php
            endif;
            ?>
        </div>
        <p style="page-break-after: always;"></p>
    </div>
</div>

<script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('resources/js/custom.js'); ?>"></script>

</body>
</html>
