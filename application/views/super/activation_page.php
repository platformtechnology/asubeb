<div class="row">
        <div class="col-md-12">
            <div class="content-box-header">
                <div class="panel-title">ACTIVATION REQUESTS</div>
            </div>
            <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <table class="table table-bordered table-condensed table-striped" id="example">
                  <thead>
                    <tr>
                      <th>SN</th>
                      <th>EDC NAME</th>
                      <th>TRIAL ID</th>
                      <th>STATUS</th>
                      <th>DATE REQUESTED</th>
                      <th>ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                        if(count($activations)): 
                            $sn = 0;
                            foreach ($activations as $activation):
                    ?>
                    <tr>
                        <td><?= ++$sn; ?></td>
                        <td><?= $activation->name; ?></td>
                        <td><?= $activation->trial_id; ?></td>
                        <td>
                            <?= 
                                ($activation->activated == 1 ? '<label class="label label-info">ACTIVATED</label>' 
                                : '<label class="label label-danger">NOT ACTIVE</label>'); 
                            ?>
                        </td>
                        <td><?= $activation->datecreated; ?></td>
                        
                        <td>
                            <?= 
                                (
                                    $activation->activated == 1 
                                    ? (
                                            '<a href="'. site_url('activateoffline/deactivate/'.$activation->trial_id) .'" title="Deactivate Request" onclick = "return confirm(\'Are You Sure You Want To DEACTIVATE This Software?\');"><i class="glyphicon glyphicon-remove"></i></a>'
                                          . ' | <a href="" title="View License" onclick = "alert(\''. $activation->license .'\');"><i class="glyphicon glyphicon-search"></i></a>'
                                      )
                                    : ( 
                                            '<a href="" data-toggle="modal" data-target="#exampleModal" data-whatever="'.$activation->trial_id.'" title="Activate Request" onclick = "return confirm(\'Are You Sure You Want To ACTIVATE This Software?\');"><i class="glyphicon glyphicon-ok"></i></a> | '
                                            . get_del_btn(site_url('activateoffline/delete/'.$activation->trial_id))
                                      )
                                      
                                )
                                ; 
                            ?>
                        </td>
                    </tr> 
                    <?php endforeach;?>
                    <?php endif; ?>
                  </tbody>
                </table>
            </div>
        </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">EDC Administrator Details</h4>
      </div>
      
       <?= form_open(site_url('activateoffline/activate/'), 'class="form-horizontal" role="form"'); ?>
      <div class="modal-body">
                        
                 <input type="hidden" name="trial_id" class="trialID" id="recipient-name">
          
                 The Default Password for the Admin is <strong style="color: crimson;">Password2$</strong>
                 <br/><br/>
                 <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Fullname:</label>
                    <div class="col-sm-10">
                        <input type="text" name="fullname" value="<?php echo set_value('fullname'); ?>" class="form-control fullname" placeholder="Administrator Fullname" required="required">
                    </div>
                 </div>
                 <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email:</label>
                    <div class="col-sm-10">
                        <input type="email" name="email" value="<?php echo set_value('email'); ?>" class="form-control" placeholder="Administrator Email" required="required">
                    </div>
                 </div>

                 <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Phone:</label>
                    <div class="col-sm-10">
                        <input type="text" name="phone" value="<?php echo set_value('phone'); ?>" class="form-control" placeholder="Administrator Phone" required="required">
                    </div>
                 </div>
                 
                 <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">EDCID:</label>
                    <div class="col-sm-10">
                        <input type="text" name="oldid" value="<?php echo set_value('oldid'); ?>" class="form-control" placeholder="Old Edc ID" /><br/>
                        Leave blank to generate a new ID
                    </div>
                 </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-saved"></i> Save & Activate</button>
      </div>
      <?= form_close(); ?>
    </div>
  </div>
</div>