<div class="sidebar content-box" style="display: block;">


    <ul class="nav">
<li <?php if ($curr_url == 'dashboard' || !$curr_url) echo 'class="current"'; ?>><a href="<?= site_url('super/dashboard') ?>"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
        <li class="submenu <?php if ($curr_url == 'edcs' || $curr_url == 'subjects' || $curr_url == 'printlayout' || $curr_url == 'subjectarms' || $curr_url == 'exams' || $curr_url == 'news' || $curr_url == 'registrants' || $curr_url == 'regdeadline' || $curr_url == 'schools' || $curr_url == 'edcinfo' || $curr_url == 'zones' || $curr_url == 'lgas' || $curr_url == 'schools_secondary' || $curr_url == 'schools_posting') echo 'current open'; ?>">
            <a href="#"><i class="glyphicon glyphicon-book"></i> EDCs<span class="caret pull-right"></span></a>
            <ul>
                <li <?php if ($curr_url == 'edcs') echo 'class="current"'; ?>><a href="<?= site_url('super/edcs/save'); ?>"><i class="glyphicon glyphicon-plus"></i> Add EDC</a></li>
                <li <?php if ($this->uri->uri_string() == 'edcs') echo 'class="current"'; ?>><a href="<?= site_url('super/edcs'); ?>"><i class="glyphicon glyphicon-search"></i> View All EDC</a></li>
            </ul>
        </li>
<?php
    if ($super_data->privilege == 'SuperAdmin') {
?>

        <?php $curr_url = $this->uri->segment(2); ?>




        <li <?php if ($curr_url == 'administrators') echo 'class="current"'; ?>><a href="<?= site_url('super/administrators/save') ?>"><i class="glyphicon glyphicon-screenshot"></i> EDC Admins</a></li>
        <li <?php if ($curr_url == 'schooladmin') echo 'class="current"'; ?>><a href="<?= site_url('super/schooladmin') ?>"><i class="glyphicon glyphicon-fullscreen"></i> School Admins</a></li>

        <li class="submenu <?php if ($curr_url == 'pinmanager' || $curr_url == 'pinstat') echo 'current open'; ?>">
            <a href="#"><i class="glyphicon glyphicon-log-in"></i> PIN Manager<span class="caret pull-right"></span></a>
            <ul>
                <li <?php if ($curr_url == 'pinmanager') echo 'class="current"'; ?>><a href="<?= site_url('super/pinmanager'); ?>"><i class="glyphicon glyphicon-upload"></i> Upload pin</a></li>
                <li <?php if ($curr_url == 'pinstat') echo 'class="current"'; ?>><a href="#"  data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-stats"></i> View Stat</a></li>

            </ul>
        </li>


        <li class="submenu <?php if ($curr_url == 'updates') echo 'current open'; ?>">
            <a href="#"><i class="glyphicon glyphicon-arrow-up"></i> EDC Updates<span class="caret pull-right"></span></a>
            <ul>
                <li <?php if ($curr_url == 'updates') echo 'class="current"'; ?>><a href="<?= site_url('super/updates/save'); ?>"><i class="glyphicon glyphicon-upload"></i> Add Update</a></li>
                <li <?php if ($curr_url == 'updates' && $this->uri->segment(2) == 'view') echo 'class="current"'; ?>><a href="<?= site_url('super/updates/view'); ?>"><i class="glyphicon glyphicon-th-list"></i> View Updates</a></li>
            </ul>
        </li>
        <li <?php if ($curr_url == 'synch_manager') echo 'class="current"'; ?>><a href="<?= site_url('super/synch_manager') ?>"><i class="glyphicon glyphicon-retweet"></i> Synch Requests</a></li>
        <li <?php if ($curr_url == 'activeyear') echo 'class="current"'; ?>><a href="<?= site_url('super/activeyear/save') ?>"><i class="glyphicon glyphicon-calendar"></i> Set Active Year</a></li>




<?php
}
elseif ($super_data->privilege == 'Support'){
    //VIEWS EXCLUSIVE TO SUPPORT USER
?>

        <li <?php if ($curr_url == 'schooladmin') echo 'class="current"'; ?>><a href="<?= site_url('super/schooladmin') ?>"><i class="glyphicon glyphicon-fullscreen"></i> School Admins</a></li>


<?php
}
?>
         <!---// GENERAL VIEWS--->
        <li class="submenu <?php if ($curr_url == 'pinmanager' || $curr_url == 'pinstat') echo 'current open'; ?>">
            <a href="#"><i class="glyphicon glyphicon-log-in"></i> Support <span class="caret pull-right"></span></a>
            <ul>
                <li <?php if ($curr_url == 'support') echo 'class="current"'; ?>><a href="<?= site_url('super/support') ?>"><i class="glyphicon glyphicon-comment"></i> Support Requests</a></li>
                <li <?php if ($curr_url == 'pinstat') echo 'class="current"'; ?>><a href="<?= site_url('super/registration') ?>" ><i class="glyphicon glyphicon-remove-sign"></i> Reg. Errors</a></li>
                <li <?php if ($curr_url == 'formnumber') echo 'class="current"'; ?>><a href="<?= site_url('super/formnumber') ?>" ><i class="glyphicon glyphicon-registration-mark"></i>Form Number Support</a></li>

            </ul>
        </li>
        <li <?= (($curr_url == 'search') ? 'class="current"' : '') ?>><a href="<?= site_url('super/search'); ?>"><i class="glyphicon glyphicon-search"></i> Search</a></li>
        <li <?php if ($curr_url == 'password') echo 'class="current"'; ?>><a href="<?= site_url('super/password/change') ?>"><i class="glyphicon glyphicon-lock"></i> Change Pass.</a></li>
        <li ><a href="<?= site_url('super/login/logout') ?>"><i class="glyphicon glyphicon-off"></i> LogOut</a></li>
            </ul>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Pin Search/Mgt</h4>
            </div>
            <?= form_open(site_url('super/pinstat/select'), 'class="form-horizontal"') ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="control-label col-lg-3">Select EDC</label>
                            <div class="col-lg-9">
                                <?php
                                $edcs = $this->db->get('t_edcs')->result();
                                if (count($edcs)) {
                                    $select_options = array();
                                    //$select_options[''] = '--- Select Edc ---';

                                    foreach ($edcs as $edc) {
                                        $select_options[$edc->edcid] = ucwords(strtolower($edc->edcname));
                                    }
                                    echo form_dropdown('edcid', $select_options, $this->input->post('edcid') ? $this->input->post('edcid') : '', 'class="form-control" required="required" style="font-weight: bold;"');
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Enter</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>