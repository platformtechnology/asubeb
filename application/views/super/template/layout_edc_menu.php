<?php $curr_url = $this->uri->segment(2); ?>
<div class="col-lg-6">
        <div class="row">
            <div class="col-md-12">
                                <ul class="nav nav-pills">
   <?php
    if ($super_data->privilege == 'SuperAdmin') {
?>

                    <li class="<?php if($curr_url == 'exams') echo 'active'; ?>"><a href="<?=  site_url('super/exams/save/'.$edcs->edcid);?>">Exams</a></li>
                    <li class="<?php if($curr_url == 'subjects') echo 'active'; ?>"><a href="<?=  site_url('super/subjects/save/'.$edcs->id);?>">Subjects</a></li>
                    <li class="<?php if($curr_url == 'subjectarms') echo 'active'; ?>"><a href="<?=  site_url('super/subjectarms/index/'.$edcs->edcid);?>">Subject Arms</a></li>
                    <li class="<?php if($curr_url == 'sliders') echo 'active'; ?>"><a href="<?=  site_url('super/sliders/save/'.$edcs->edcid);?>">Sliders</a></li>
                    <li class="<?php if($curr_url == 'edcinfo') echo 'active'; ?>"><a href="<?= site_url('super/edcinfo/save/'.$edcs->edcid.'/'); ?>">Home & About Info</a></li>
                    <li class="<?php if($curr_url == 'news') echo 'active'; ?>"><a href="<?= site_url('super/news/index/'.$edcs->edcid); ?>">News Info</a></li>
                    <li class="<?php if($curr_url == 'news') echo 'active'; ?>"><a href="<?= site_url('super/timetable/index/'.$edcs->edcid); ?>">Exam Timetable Info</a></li>
                    <li class="<?php if($curr_url == 'zones') echo 'active'; ?>"><a href="<?= site_url('super/zones/save/'.$edcs->edcid.'/'); ?>">Zones</a></li>
                    <li class="<?php if($curr_url == 'lgas') echo 'active'; ?>"><a href="<?= site_url('super/lgas/save/'.$edcs->edcid.'/'); ?>">LGA</a></li>
                    <li class="<?php if($curr_url == 'schools') echo 'active'; ?>"><a href="<?= site_url('super/schools/save/'.$edcs->edcid.'/'); ?>">Primary Schools</a></li>
                    <li class="<?php if($curr_url == 'schools_secondary') echo 'active'; ?>"><a href="<?= site_url('super/schools_secondary/save/'.$edcs->edcid.'/'); ?>">Secondary Schools</a></li>
                    <li class="<?php if($curr_url == 'placement') echo 'active'; ?>"><a href="<?= site_url('super/placement/fetch/'.$edcs->edcid.'/'); ?>">Edit Placement</a></li>
                    <li class="<?php if($curr_url == 'registrants') echo 'active'; ?>"><a href="<?= site_url('super/registrants/filter/'.$edcs->edcid.'/'); ?>">Candidate Report</a></li>
                    <li class="<?php if($curr_url == 'regdeadline') echo 'active'; ?>"><a href="<?= site_url('super/regdeadline/save/'.$edcs->edcid.'/'); ?>">Registration Deadline</a></li>
                    <li class="<?php if($curr_url == 'sms') echo 'active'; ?>"><a href="<?= site_url('super/sms/save/'.$edcs->edcid.'/'); ?>">Result to SMS</a></li>

<?php
 }
 else{
    ?>
                    <li class="<?php if($curr_url == 'registrants') echo 'active'; ?>"><a href="<?= site_url('super/registrants/filter/'.$edcs->edcid.'/'); ?>">Candidate Report</a></li>
                      <li class="<?php if($curr_url == 'schools') echo 'active'; ?>"><a href="<?= site_url('super/schools/save/'.$edcs->edcid.'/'); ?>">Primary Schools</a></li>
                    <li class="<?php if($curr_url == 'schools_secondary') echo 'active'; ?>"><a href="<?= site_url('super/schools_secondary/save/'.$edcs->edcid.'/'); ?>">Secondary Schools</a></li>
<?php
 }
 ?>
                       </ul>

            </div>
        </div>
        <?php if($curr_url == 'subjects' || $curr_url == 'subjectarms'): ?>
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <?= get_edc_logo($edclogo, $edcs->edcname); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
 <?php if($curr_url != 'subjects' && $curr_url != 'subjectarms'): ?>
           <div class="col-lg-6">
                <div class="row">
                    <div class="col-md-12">
                        <?= get_edc_logo($edclogo, $edcs->edcname); ?>
                    </div>
                </div>
           </div>
  <?php endif; ?>