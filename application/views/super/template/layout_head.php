<!DOCTYPE html>
<html>
  <head>
    <title><?= config_item('site_name') ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
     <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script> 
    
    <?= $page_level_styles; ?>
    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
   <body>
  	<div class="header" style="background-color: #2c3742; height:50px;">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-8">
	              <div class="logo">
                          <h3><a href="<?= site_url('dashboard'); ?>"><img src="<?= get_img('edevpro.png')?>" height="35px" align="absmiddle" alt="E-devPro Logo" /></a><span style="color: white; font-weight: bolder; font-size: 13px; letter-spacing: 1px;">V1.0.1</span></h3>
	              </div>
	           </div>
	           
	           <div class="col-md-4 pull-right">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $super_data->fullname ?><b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="<?= site_url('super/password/change');?>">Change Password</a></li>
                                  <li><a href="<?= site_url('super/login/logout');?>">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>