

     
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <?= $page_level_scripts; ?>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script>   
    <script>
        $(document).ready(function() {
                $('#example').dataTable({
			"iDisplayLength": 50
		  });
        } );
    </script>
    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('whatever') // Extract info from data-* attributes
            var modal = $(this)
            modal.find('.trialID').val(recipient)
          })
    </script>
    <?php 
     if(isset($reg_exams_by_year) || isset($pinstat_summary)){
         $this->load->view('super/template/layout_barchart_script');
     }
    ?>
  </body>
</html>

