
     
         <?php if(isset($reg_exams_by_year)): ?>
         <script>
            new Morris.Bar({
                    element: 'reg_exams_div',
                    data: <?= isset($reg_exams_by_year) ? $reg_exams_by_year : "''"; ?>,
                    xkey: 'examyear',
                    ykeys: ['examtype1', 'examtype2', 'examtype3'],
                    labels: <?= isset($exam_series_name) ? $exam_series_name : "'Series A'"; ?>,
                    hideHover: 'auto'
                  });
          </script>
         <?php endif; ?>   
         
          <?php if(isset($usedpin_summary)): ?>
         <script>
             new Morris.Line({
                element: 'usedpin_div',
                data: <?= isset($usedpin_summary) ? $usedpin_summary : "''"; ?>,
                xkey: 'edcname',
                ykeys: ['pincount'],
                hideHover: true,
                labels: <?= isset($usedpin_series) ? $usedpin_series : "'Series A'"; ?>
              });
           </script>
         <?php endif; ?>
          
           <?php if(isset($pinstat_summary)): ?>
            <script>
            new Morris.Bar({
                element: 'hero-bar',
                data: <?= isset($pinstat_summary) ? $pinstat_summary : "''"; ?>,
                xkey: 'edcname',
                ykeys: ['pincount'],
                labels: <?= isset($pinstat_series) ? $pinstat_series : "'Series A'"; ?>,
                barRatio: 0.4,
                xLabelMargin: 10,
                hideHover: true,
                barColors: ["#3d88ba"]
              });
            </script>
         <?php endif; ?>

