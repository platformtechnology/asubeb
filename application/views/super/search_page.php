<div class="row">
    <div class="col-md-12 panel-primary">
        
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-edit"></i> <strong>Search For Candidates</strong>               
                </div>
            </div>
            <div class="content-box-large box-with-header">
                <?= form_open('', 'class="form-horizontal" name="regform" id="regform" role="form"'); ?>
                <div class="row">
                    <div class="col-md-12">
                        
                    <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                                             
                         <div class="col-md-6">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-pull-1 col-sm-4 control-label">Search Parameter:</label>
                                <div class="col-sm-pull-1 col-sm-8">
                                    <input type="text" name="parameter" id="parameter" value="<?= set_value('parameter'); ?>" class="form-control" id="inputEmail3" placeholder="Enter ExamNo, FullName or Pin to search" onblur="performSearch(this)" />
                                </div>
                                <strong style="color: crimson;">Just type the exam no, name or pin of the candidate you are searching for.</strong>
                             </div>
                          </div>    
                    </div>
                    
                </div>
           
                <div class="row">
                    <div class="page-header">
                        <h3><i class="glyphicon glyphicon-edit"></i> Search Results </h3>
                    </div>
                    <div class="col-md-12">

                      <div id="display_res"> -- NOTHING FOUND -- </div>

                    </div>
                </div>
           
              <?= form_close(); ?>
            </div>
        
    </div>
</div>

<script type="text/javascript">
    
 function performSearch(param){
     parameter = param.value;
     isPinSearch = "0";
             
     if(parameter.trim() == ""){
         document.getElementById("display_res").innerHTML = " -- NOTHING FOUND -- ";
         return;
     }
     
     if(parameter.trim().length == 12 && !isNaN(parameter))isPinSearch = "1";
     
     var url_to_post = "<?= site_url('super/search/fetchCandidates?param='); ?>" + parameter + "&isPinSearch=" + isPinSearch;
     
     document.getElementById("display_res").innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
    
    makeRequestx(url_to_post, "display_res");
 }
 
 
  function makeRequestx(url, displayId) {
         
        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
                
         httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                   document.getElementById(displayId).innerHTML = httpRequest.response;
                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById(displayId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
        httpRequest.send();
    }
   
    function getHttpObject(){
        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
            httpRequest = new XMLHttpRequest();
            return httpRequest;
        } else if (window.ActiveXObject) { // IE
            try {
                httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                return httpRequest;
            } 
            catch (e) {
                try {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    return httpRequest;
                } 
                catch (e) {}
            }
        }

        return;
    }
</script>
