<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>

<br/>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> Setup LGAs</div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">LGA Name:</label>
                            <div class="col-sm-8">
                                <input type="text" name="lganame" value="<?php echo set_value('lganame', $lga_detail->lganame); ?>" class="form-control" id="inputEmail3" placeholder="Enter LGA Name" required="required">
                            </div>
                        </div>
                       <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Initials:</label>
                            <div class="col-sm-8">
                                <input type="text" name="lgainitials" value="<?php echo set_value('lgainitials', $lga_detail->lgainitials); ?>" class="form-control" id="inputEmail3" placeholder="Enter Initials" required="required">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Zone:</label>
                            <div class="col-sm-8">
                                 <?php 
                                      $select_options = array();
                                      foreach ($zones as $zone) {
                                          $select_options[$zone->zoneid] = $zone->zonename;
                                      }
                                      $select_options[''] = "Select Zone";
                                      echo form_dropdown('zoneid', $select_options, $this->input->post('zoneid') ? $this->input->post('zoneid') : $lga_detail->zoneid, 'class="form-control" required="required"');
                                    ?>
                            </div>
                        </div>
                         <div class="form-group">
                               <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                <a href="<?= site_url('super/lgas/save/'. $edcs->edcid);?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                              </div>
                            </div>
                    </div>
                 <?= form_close(); ?>
                
                <div class="col-md-7">
                    <table class="table table-bordered table-condensed" id="example">
                      <thead>
                        <tr>
                          <th>Sn</th>
                          <th>Lga Name</th>
                          <th>Zone Name</th>
                          <th>Lga Code</th>                       
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php 
                            if(count($lgas)): 
                                $sn = 0;
                                foreach ($lgas as $lga):
                            ?>
                            <tr>
                                <td><?= ++$sn; ?></td>
                                <td><?php echo anchor(site_url('super/lgas/save/' . $edcs->edcid . '/' . $lga->lgaid), $lga->lganame) ?></td>
                                <td><?= $lga->zonename; ?></td>
                                <td><?= $lga->lgainitials; ?></td>
                                <td>
                                    <?= get_edit_btn(site_url('super/lgas/save/' . $edcs->edcid . '/' . $lga->lgaid)); ?>
                                    <?= get_del_btn(site_url('super/lgas/delete/' . $edcs->edcid . '/' . $lga->lgaid)); ?>
                                </td>
                            </tr> 
                            <?php endforeach;?>
                            <?php endif; ?>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
