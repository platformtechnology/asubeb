<div class="row">

        <div class="col-md-8">
        <div class="content-box-large">
            
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-11">
                        <h4><i class="glyphicon glyphicon-comment"></i> <?= ucwords(strtolower($request_data->subject)); ?></h4>
                        <strong>RID:<span style="color: crimson;"> #<?= $request_data->requestid; ?></span></strong><br/>
                        <span><strong>Status:</strong> <?= get_status($request_data->status); ?></span><br/>
                        <span><strong>Date:</strong> <?= substr($request_data->datecreated , 0, 16); ?></span><br/><br/>
                        <table border="0" cellpadding="5">
                            <tr>
                                <td valign="top"><strong>Request:</strong></td>
                                <td valign="top"> <?= $request_data->request; ?></td>
                            </tr>
                        </table>
                     </div>
                </div>    
                    <hr/>
                    
                <div class="row">
                  <div class="col-lg-10">
                     <?php 
        
                        $comments = $this->support_comment_model->get_comments($request_data->requestid); 
                        if(count($comments)){
                            echo ' <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Replies</th>
                                        </tr>
                                    </thead> 
                                    <tbody>';

                            foreach ($comments as $comment) {
                               echo "<tr>"
                                        . "<td>";
                                        echo "<strong>".$comment->owner."</strong> | ". substr($comment->datecreated , 0, 16). "<br/>";
                                        echo '<span style="font-size: 13px; color:#2f5e9e; font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;">';
                                        echo $comment->comment;
                                        echo '</span>'.'<br/>';
                                        echo "</td>"
                                    . "</tr>";
                            } 

                            echo   '</tbody>
                             </table>';
                        }
                    ?>
                      
                     <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?= form_open(site_url('super/support/reply'), 'class="form-horizontal" role="form"'); ?>
                      
                            <input type="hidden" name="edcid" value="<?= $request_data->edcid?>" >
                            <input type="hidden" name="requestid" value="<?= $request_data->requestid?>" >
                            <div class="form-group">
                                <div class="col-sm-6">
                                 <label for="inputEmail3" class="control-label">Status:</label>
                                 
                                     <select name="status" class="form-control">
                                         <option value="Pending" <?= ($request_data->status == 'Pending' ? 'selected' : '' ) ?>>Pending</option>
                                         <option value="Processing" <?= ($request_data->status == 'Processing' ? 'selected' : '') ?>>Processing</option>
                                         <option value="Resolved" <?= ($request_data->status == 'Resolved' ? 'selected' : '' ) ?>>Resolved</option>
                                     </select>
                                </div>
                             </div>
                            <div class="form-group">
                                <div class="col-sm-9">
                                 <label for="inputEmail3" class="control-label">Reply:</label>
                                 <textarea name="comment" rows="6" wrap="true" class="form-control"><?= set_value('comment'); ?></textarea>
                                </div>
                            </div>
                                                      
                             <div class="form-group">
                                 <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-comment"></i> Post</button>
                                 <a href="<?= site_url('super/support'); ?>" class="btn btn-warning" > <i class="glyphicon glyphicon-arrow-left"></i> Back </a>
                             </div>
                          
                        <?= form_close();?> 
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>