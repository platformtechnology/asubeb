<div class="row">
    <div class="col-md-6">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title">
                   <i class="glyphicon glyphicon-calendar"></i> Set Active Year
                </div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?php echo form_open(site_url('super/activeyear/save/'.$year_detail->id), 'class="form-horizontal" role="form"'); ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Active Year:</label>
                        <div class="col-sm-8">
                            <input type="text" name="activeyear" class="form-control" value="<?php echo set_value('activeyear', $year_detail->activeyear); ?>" required="required" />
                        </div>
                        <strong>Setting/Updating Active Year would reset the count for result views</strong>
                    </div>
                
<!--                    <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Close Date:</label>
                            <div class="col-sm-8">
                                <div class="bfh-datepicker" data-format="d-m-y" data-date="<?php echo set_value('closedate', $year_detail->closedate); ?>" data-name="closedate" data-date="today"></div>
                            </div>
                        </div>-->
                    
                    <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-save"></i> Save</button>
                      </div>
                    </div>
                <?php echo form_close(); ?>
             </div>
        </div>
    </div>
</div>