<div class="row">

        <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-comment"></i> SUPPORT REQUESTS</div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('updated_msg')) echo get_success($this->session->flashdata('updated_msg')); ?>
                <table class="table table-striped table-hover table-condensed" id="example">
                  <thead>
                    <tr>
                      <th>Sn</th>
                      <th>RID</th>
                      <th>Subject</th>
                      <th>Status</th>
                      <th>Phone</th>
                      <th>Name</th>
                      <th>Request</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                        if(count($support_data)): 
                            $sn = 0;
                            foreach ($support_data as $data):
                    ?>
                    <tr>
                        <td><?= ++$sn; ?></td>
                        <td><?= $data->requestid; ?></td>
                        <td><a href="<?= site_url('super/support/requestdetails/'.$data->requestid)?>"><?= ucwords(strtolower($data->subject));?></a></td>
                        <td><?= get_status($data->status); ?></td>
                        <td><?= $data->phone; ?></td>
                        <td><?= ucwords(strtolower($data->name)); ?></td>
                        <td><?= $data->request; ?></td>
                        <td><?= get_del_btn(site_url('super/support/delete/'.$data->requestid)) ?></td>
                    </tr> 
                    <?php endforeach;?>
                    <?php endif; ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>