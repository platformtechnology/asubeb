
<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>

<br/>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> <strong>Edit Candidates Placement</strong>
                    <br/>
                    <strong>Note:</strong><span style="color: crimson"> Placements are for candidates that registered for placement dependent exams such as GPT</span>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" name="regform" id="regform" role="form"'); ?>
<!--                    <div class="col-md-4">

                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">LGA:</label>
                            <div class="col-sm-8">
                                 <?php
//                                    $select_options = array();
//                                    $select_options[''] = "---- Select ----";
//                                      foreach ($lgas as $lg) {
//                                          $select_options[$lg->lgaid] = $lg->lganame;
//                                      }
//                                    echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : '', 'id="lgaid" class="form-control" required="required"'); 
                                ?>
                            </div>
                        </div>

                    </div>-->
                        
                    <div class="col-md-5">
                       
                       <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Present School:</label>
                            <div class="col-sm-8">
                                <?php
                                    $select_options = array();
                                    $select_options[''] = "---- All Schools ----";
                                   // if(!empty($this->input->post('schoolid'))){
                                      foreach ($schools as $school) {
                                          $select_options[$school->schoolid] = $school->schoolname;
                                      }
                                    //}
                                    echo form_dropdown('schoolid', $select_options, $this->input->post('schoolid') ? $this->input->post('schoolid') : '', 'id="schoolid" class="form-control"'); 
                                ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Exam:</label>
                            <div class="col-sm-8">
                                    <?php 
                                      $select_options = array();
                                      foreach ($exams as $exam) {
                                          $select_options[$exam->examid] = strtoupper($exam->examname);
                                      }
                                      echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'class="form-control" required="required"');
                                    ?>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-md-2">
                       
                       <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Year:</label>
                            <div class="col-sm-8">
                                <?php 
                                     $already_selected_value = $this->input->post('examyear') ? $this->input->post('examyear') : $activeyear;
                                     $start_year = $startyear;
                                     print '<select name="examyear" class="form-control">';
                                     print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>Exam Year</option>';
                                     for ($x = $start_year; $x <= $activeyear; $x++) {
                                         print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                                     }
                                     print '</select>';
                                   ?>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-md-2">
                        
                         <div class="form-group">
                               <div class="col-sm-8">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-download"></i> Fetch Data</button>
                              </div>
                            </div>
                    </div>
                 <?= form_close(); ?>
                    </div>
                </div>
                
            <div class="row">
                <div class="page-header">
                    <h3><i class="glyphicon glyphicon-edit"></i> Edit Candidates Placement</h3>
                </div>
              <div class="col-md-12">
                    <table class="table table-bordered table-condensed" id="example">
                      <thead>
                        <tr>
                          <th>Sn</th>
                          <th>Exam No</th>
                          <th>Candidate's Name</th>
                          <th>School Placed</th>
                          <th>Action</th>   
                        </tr>
                      </thead>
                      <tbody>
                           <?php 
                           $sn = 0;
                            if(count($candidates)): 
                                foreach ($candidates as $candidate):
                                    $name = ucfirst(strtolower($candidate->firstname)) . ' ' . ucfirst(strtolower($candidate->othernames));
                            ?>
                            <tr style="font-size: 11px">
                                <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                                <td><?= ++$sn; ?></td>
                                <td><?= $candidate->examno; ?></td>
                                <td><?php echo anchor(site_url('super/registrants/view/' . $candidate->candidateid), $name, 'title="View Details"'); ?></td>
                                <td>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <?php
                                                $select_options = array();
                                                $select_options[''] = "...:::Not Placed Yet:::...";
                                                  foreach ($choiceschool as $choice) {
                                                      $select_options[$choice->schoolid] = $choice->schoolcode . ' - ' . $choice->schoolname;
                                                  }
                                                echo form_dropdown('placement'.$sn, $select_options, $candidate->placement ? $candidate->placement : '', 'id="placement'.$sn.'" class="form-control" style="height: 25px; font-size: 11px;" required="required"'); 
                                            ?>
                                            <span id="result<?= $sn ?>"></span>
                                        </div>
                                    </div>
                                </td>
                                <input type="hidden" name="candidateid<?= $sn?>" id="candidateid<?= $sn?>" value="<?= $candidate->candidateid?>" />
                                <td><button type="button" style="height: 25px; font-size: 11px" class="btn btn-sm btn-default" onclick="updatePlacement('<?= $sn ?>')"><i class="glyphicon glyphicon-upload"></i> Update</button></td>
                                <?= form_close(); ?>
                            </tr> 
                            <?php endforeach;?>
                            <?php endif; ?>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function updatePlacement(sn){
        candidateid = document.getElementById("candidateid"+sn).value;
        placement = document.getElementById("placement"+sn).value;
        
        if(placement.trim() == ""){
            alert("YOU MUST SELECT A SCHOOL FOR PLACEMENT");
            document.getElementById("result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }
        
        var url_to_post = "<?= site_url('super/placement/updateplacement?placement='); ?>";
        url_to_post = url_to_post + placement + "&candidateid="+candidateid;
        document.getElementById("result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
        makeRequestx(url_to_post, "result"+sn);
    }
    
    function makeRequestx(url, imgId) {
         
        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
                
         httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                   
                    var obj = JSON.parse(httpRequest.response);
                    if(obj.success == 1){
                        document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('available.png'); ?>"+'" />';
                    }else{
                        alert('Could Not Update');
                        document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                    }
                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
        httpRequest.send();
    }
//    
    function getHttpObject(){
        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
            httpRequest = new XMLHttpRequest();
            return httpRequest;
        } else if (window.ActiveXObject) { // IE
            try {
                httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                return httpRequest;
            } 
            catch (e) {
                try {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    return httpRequest;
                } 
                catch (e) {}
            }
        }

        return;
    }
</script>


  <script type="text/javascript">
     (function() {
        var httpRequest;
        
        lgaddl = document.getElementById("lgaid");
        lgaddl.onchange = function() { 
            var target_url3 = "<?= site_url('super/placement/school_ajaxdrop?lgaid='); ?>";
            //target_url1 = target_url1.replace(/\.[^/.]+$/, ""); //remove .html extension
            makeRequest(target_url3 + lgaddl.value, 'schoolid'); 
        };
        
        

        function makeRequest(url, targetid) {
            if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                httpRequest = new XMLHttpRequest();
            } else if (window.ActiveXObject) { // IE
                try {
                    httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                } 
                catch (e) {
                    try {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    } 
                    catch (e) {}
                }
            }

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = function(){
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        //alert(httpRequest.response);
                        var data = JSON.parse(httpRequest.response);
                        var select = document.getElementById(targetid);
                        if(emptySelect(select)){
                            var el = document.createElement("option");
                                    if(targetid == 'lgaid'){
                                        el.textContent = '---- All Local Govts ----';
                                    }else if(targetid == 'examid'){
                                         el.textContent = '---- All Exams ----';
                                    }else{
                                         el.textContent = '---- All Schools ----';
                                    }
                                    el.value = '';
                                    select.appendChild(el);
                                    
                            for (var i = 0; i < data.lgas.length; i++){
                                var el = document.createElement("option");
                                    el.textContent = data.lgas[i].lganame;
                                    el.value = data.lgas[i].lgaid;
                                    select.appendChild(el);
                            }
                        }
                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
            httpRequest.send();
        }

        function emptySelect(select_object){
            var i;
            for(i=select_object.options.length-1;i>=0;i--){
                select_object.remove(i);
            }
            return true;
        }
    })();


    </script>