<div class="row">
    <div class="col-md-6">
              
        <div class="row">
            <div class="col-md-12">
                <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title"><i class="glyphicon glyphicon-log-in"></i> No Of <strong>Used PIN</strong> Per EDC </div>
                        <div class="panel-options">
                            <a href="<?= site_url('super/dashboard');?>" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div id="hero-bar" style="height: 230px;"></div>
                        <br/><strong>Total Pin Used: <span style="color: crimson;"><?= $pinstat_usedpin_count->pincount ?></span></strong>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    
    <div class="col-md-6">
       
          <div class="row">
            <div class="col-md-12">
                <div class="content-box-header">
                    <div class="panel-title"><i class="glyphicon glyphicon-log-in"></i> Summary Of Uploaded <strong>PINs</strong></div>
                </div>
                <div class="content-box-large box-with-header">
                 <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Edc Name</th>
                        <th>Volume</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php 
                                $sn = 0; $totalP =0;
                                if(count($pin_upload_summary)): 
                                    foreach ($pin_upload_summary as $summary):
                            ?>
                            <tr>
                                <td><?= ++$sn ?></td>
                                <td><?= $summary->date; ?></td>
                                <td><?= $summary->edcname; ?></td>
                                <td><?= $summary->volume; ?></td>
                            </tr>
                            
                                   <?php 
                                        $totalP += $summary->volume;
                                    endforeach;
                                    ?>
                            <?php endif; ?>    
                    </tbody>
                 </table>
                    <strong>Total Pins Uploaded: <span style="color:crimson"><?=$totalP;?></span></strong>
                </div>
            </div>
        </div>
                   
    </div>
    
</div> <!--END MIDDLE ROW-->
