
<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>
</div>

<br/>

<div class="row">
    <div class="col-md-12">

            <div class="content-box-header">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> <strong>Setup Primary Schools</strong></div>
            </div>
            <div class="content-box-large box-with-header">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                    <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                    <?= form_open('', 'class="form-horizontal" name="regform" id="regform" role="form"'); ?>
                        <div class="row">
                            <div class="col-md-6">
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Zone:</label>
                                <div class="col-sm-8">
                                     <?php
                                          $select_options = array();
                                          foreach ($zones as $zone) {
                                              $select_options[$zone->zoneid] = $zone->zonename;
                                          }
                                          $select_options[''] = "---------------";
                                          echo form_dropdown('zoneid', $select_options, $this->input->post('zoneid') ? $this->input->post('zoneid') : $school_detail->zoneid, 'class="form-control" id="zoneid"');
                                        ?>
                                </div>
                            </div>

                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">LGA:</label>
                                <div class="col-sm-8">
                                    <?php
                                        $select_options = array();
                                        $select_options[''] = "--------------";

                                          foreach ($lgs as $lg) {
                                              $select_options[$lg->lgaid] = $lg->lganame;
                                          }

                                        echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : $school_detail->lgaid, 'id="lgaid" class="form-control"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                      <div class="col-sm-offset-2 col-sm-10">
                                        <label>
                                            <br/>
                                            <strong style="color: crimson">"Is Exam Centre"</strong> is used to indicate those schools that also serves as exam centres.
                                            <br/>
                                            If you are setting up a school that requires ONLY ZONE, select the zone but don't select any LGA.
                                        </label>
                                      </div>
                                </div>
                        </div>

                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 control-label">School Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="schoolname" value="<?php echo set_value('schoolname', $school_detail->schoolname); ?>" class="form-control" id="inputEmail3" placeholder="Enter Primary School Name" required="required">
                                    </div>
                                </div>
                               <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 control-label">School Code:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="schoolcode" id="schoolcode" value="<?php echo set_value('schoolcode', $school_detail->schoolcode); ?>" class="form-control"  placeholder="Enter School Code" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                         <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="iscentre" value="1" <?php echo ($school_detail->iscentre == 1 ? ' checked="true"' : '')?>> <strong>Is A Centre</strong>
                                            </label>
                                          </div>
                                    </div>
                                </div>

                                 <div class="form-group">
                                       <div class="col-sm-offset-4 col-sm-8">
                                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                        <a href="<?= site_url('super/schools/save/'. $edcs->edcid);?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                                      </div>
                                    </div>
                            </div>
                        </div>


                     <?= form_close(); ?>
                        </div>
                    </div>

                <div class="row">

                   <h3>All Primary Schools</h3>
                   <hr/>

                  <div class="col-md-12">
                      <a href="<?= site_url('super/schools/printPrimSchool/'.$edcs->edcid) ?>" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Print All</a><br/><br/>
                        <table class="table table-bordered table-condensed" id="example">
                          <thead>
                            <tr>
                              <th>School Name</th>
                              <th>Code</th>
                              <th>Lga</th>
                              <th>Zone</th>
                              <th>IsCentre</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                               <?php
                                if(count($schools)):
                                    foreach ($schools as $school):
                                ?>
                                <tr>
                                    <td><?php echo anchor(site_url('super/schools/save/' . $edcs->edcid . '/' . $school->schoolid), strtoupper($school->schoolname)) ?></td>
                                    <td><?= $school->schoolcode; ?></td>
                                    <td><?= $this->schools_model->get_lga_name($school->lgaid); ?></td>
                                    <td><?= $this->schools_model->get_zone_name($school->zoneid); ?></td>
                                    <td><?= $school->iscentre == 1 ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'; ?></td>
                                    <td>
                                        <?= get_edit_btn(site_url('super/schools/save/' . $edcs->edcid . '/' . $school->schoolid)); ?>
                                        <?= get_del_btn(site_url('super/schools/delete/' . $edcs->edcid . '/' . $school->schoolid)); ?>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                                <?php endif; ?>
                          </tbody>
                        </table>
                    </div>
                  </div>
                </div>
            </div>
    </div>
</div>

<script type="text/javascript">

    (function() {
        var httpRequest;
        dropper = document.getElementById("zoneid");
        dropper.onchange = function() {
            var target_url = "<?= site_url('super/schools/ajaxdrop?zoneid='); ?>";
            target_url = target_url.replace(/\.[^/.]+$/, ""); //remove .html extension
            makeRequest( target_url + dropper.value + "&edcid=" + "<?=$edcs->edcid?>" );
        };

        function makeRequest(url) {
           httpRequest = getHttpObject();

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = alertContents;
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            httpRequest.send();
        }

        function alertContents() {
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                    var data = JSON.parse(httpRequest.response);
                    var select = document.getElementById('lgaid');
                    if(emptySelect(select)){

                        var e2 = document.createElement("option");
                        e2.textContent = '----------------';
                        e2.value = '';
                        select.appendChild(e2);

                        for (var i = 0; i < data.lgas.length; i++){
                            var el = document.createElement("option");
                                el.textContent = data.lgas[i].lganame;
                                el.value = data.lgas[i].lgaid;
                                select.appendChild(el);
                        }
                    }
                } else {
                    alert('There was a problem with the request.');
                }
            }
        }

        function emptySelect(select_object){
            while(select_object.options.length > 0){
                select_object.remove(0);
            }
            return 1;
        }

        function getHttpObject(){

            var xmlhtp;

            if (window.ActiveXObject)
            {
                var aVersions = [
                  "MSXML2.XMLHttp.9.0","MSXML2.XMLHttp.8.0", "MSXML2.XMLHttp.7.0",
                  "MSXML2.XMLHttp.6.0","MSXML2.XMLHttp.5.0","MSXML2.XMLHttp.4.0",
                  "MSXML2.XMLHttp.3.0","MSXML2.XMLHttp","Microsoft.XMLHttp"
                ];

                for (var i = 0; i < aVersions.length; i++)
                {
                    try
                    {
                        xmlhtp = new ActiveXObject(aVersions[i]);
                        return xmlhtp;
                    } catch (e) {}
                }
            }
            else  if (typeof XMLHttpRequest != 'undefined')
            {
                    xmlhtp = new XMLHttpRequest();
                    return xmlhtp;
            }
            throw new Error("XMLHttp object could be created.");

        }
    })();
    </script>
    <script>
        function getSchoolCode(){
        $(document).ready(function(){
           $("#lgaid").change(function(){

               var lgaid = $("#lgaid").val();

               var url = "<?php echo site_url('super/schools/fetchLastSchoolCode/')?>"+"/" +lgaid;
			$.ajax({
			url:url,
			success:function(result){
				$("input#schoolcode").val(result)
			}
			})
        });
        });
        }
    </script>


