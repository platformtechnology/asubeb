<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>

<br/>

<div class="row">
    <div class="col-md-5">

        <div class="content-box-header">
            <div class="panel-title">
                <i class="glyphicon glyphicon-plus"></i> Upload Home Sliders
            </div>
        </div>
        <div class="content-box-large box-with-header">
            <div class="panel-body">
                <?php if ($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if ($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if (strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open_multipart('', 'class="form-horizontal" role="form"'); ?>
                <br />
                <div class="col-md-12">
                    <input type="hidden" name="edcid" value="<?= $edcs->edcid ?>" >
                    <div class="row">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Images:</label>
                            <div class="col-sm-9">
                                <input type="file" multiple="true" name="sliderimage[]" class="form-control" />
                                <br/>
                                You can select multiple Images - Hold down the Ctrl button while selecting the images to upload 
                                <br/> <br/>
                                Only jpg or png images allowed
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-arrow-up"></i> Upload</button>
                        </div>
                    </div>

                </div>

                <?= form_close(); ?>
            </div>
        </div>  

    </div>

    <div class="col-md-7">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-picture"></i> Slider Images</div>
            </div>
            <div class="panel-body">
                <?php if ($this->session->flashdata('updated_msg')) echo get_success($this->session->flashdata('updated_msg')); ?>
                <table class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($sliders)):
                            foreach ($sliders as $slider):
                                ?>
                                <tr>
                                    <td>
                                        <img src="<?= base_url('resources/web/sliders/'.$slider) ?>" height="50px" width="100px" />
                                    </td>
                                    <td>
                                        <?= get_del_btn(site_url('super/sliders/delete/' . $slider . '/' . $edcs->edcid)); ?>
                                    </td>
                                </tr> 
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
