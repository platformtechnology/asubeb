<div class="row">
    <div class="col-md-5">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo empty($admin_detail->fullname) ? '<i class="glyphicon glyphicon-plus"></i> Add Administrator' : '<i class="glyphicon glyphicon-edit"></i> Edit Admin Detail'; ?>
                </div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                    The Default Password for the Admin is <strong style="color: crimson;">Password2$</strong>
                    <br /><br />
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Fullname:</label>
                        <div class="col-sm-9">
                            <input type="text" name="fullname" value="<?php echo set_value('fullname', $admin_detail->fullname); ?>" class="form-control" id="inputEmail3" placeholder="Fullname" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Email:</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" value="<?php echo set_value('email', $admin_detail->email); ?>" class="form-control" id="inputEmail3" placeholder="Email" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Phone:</label>
                        <div class="col-sm-9">
                            <input type="text" name="phone" value="<?php echo set_value('phone', $admin_detail->phone); ?>" class="form-control" id="inputEmail3" placeholder="Phone" required="required">
                        </div>
                    </div>

                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label"> Privilege</label>
                        <div class="col-sm-9">
                            <?php
                                $privileges = array('SuperAdmin'=>'SuperAdmin','Support'=>'Support','Edc Admin'=>'EDC ADMIN');

                                echo form_dropdown('privilege',$privileges,'Support',$extra = 'class="form-control"');
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">EDC:</label>
                        <div class="col-sm-9">
                            <?php
                              $select_options = array();
                              foreach ($edcs as $edc) {
                                  $select_options[$edc->edcid] = $edc->edcname;
                              }
                              $select_options['platform'] = "Platform Technologies Limited";
                              echo form_dropdown('edcid', $select_options, $this->input->post('edcid') ? $this->input->post('edcid') : $admin_detail->edcid, 'class="form-control" required');
                            ?>
                        </div>
                    </div>
                <br/>
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                        <a href="<?= site_url('super/administrators/save');?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                      </div>
                    </div>
                    <?= form_close(); ?>
                </div>
             </div>
        </div>

        <div class="col-md-7">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title">All Administrators</div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('updated_msg')) echo get_success($this->session->flashdata('updated_msg')); ?>
                <table class="table table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th>Fullname</th>
                      <th>Email</th>
                      <th>EDC</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                        if(count($administrators)):
                            foreach ($administrators as $admin):
							if(trim($admin->fullname)!=FALSE):
                    ?>
                    <tr>
                        <td><?php echo anchor(site_url('super/administrators/save/'.$admin->userid), $admin->fullname) ?></td>
                        <td><?= $admin->email; ?></td>
                        <td><?= $this->users_model->get_edc_from_user($admin->userid); ?></td>
                        <td>
                            <?= get_del_btn(site_url('super/administrators/delete/'.$admin->userid)); ?>
                            <?= anchor(site_url('super/administrators/reset/'.$admin->userid), ' <i class="glyphicon glyphicon-refresh"></i>',  array(
                                            'title' => 'Reset Admin Password',
                                            'onclick' => "return confirm('You are about to reset the password of this user to the default. Are you sure you want to proceed?');"
                                            )); ?>
                        </td>
                    </tr>
                    <?php 
					endif;
					endforeach;?>
                    <?php endif; ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>