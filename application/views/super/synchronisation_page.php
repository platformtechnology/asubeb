<div class="row">
    <div class="col-md-12">
        <div class="content-box-header">
            <div class="panel-title">OFFLINE SYNCHRONISATION REQUESTS</div>
        </div>
        <div class="content-box-large box-with-header">

            <?php if ($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
            <?php if ($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
            <table class="table table-striped" id="example">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Edc</th>
                        <th>Date Sent</th>
                        <th>File Size</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($synchfiles)):
                        $sn = 0;
                        foreach ($synchfiles as $file):
                            ?>
                            <tr>
                                <td><?= ++$sn; ?></td>
                                <td><?= ucwords(strtolower($file->edcname)); ?></td>
                                <td><?= $file->datecreated ?></td>
                                <td><?= round($file->filesize / 1024, 2); ?>KB</td>
                                <td><?= $file->processed ? '<label class="label label-warning"><i class="glyphicon glyphicon-ok"></i>PROCESSED</label>' : '<label class="label label-danger">PENDING</label>'; ?></td>
                                <td>
                                    <a class="green" href="<?= site_url('super/synch_manager/download/' . $file->id); ?>" title="Download Synchronisation Detail">
                                        <i class="glyphicon glyphicon-download "></i>
                                    </a>

                                    | 

                                    <a class="red" href="<?= site_url('super/synch_manager/delete/' . $file->id); ?>" title="Delete Synchronisation Data" onclick="return confirm('Are you sure you wish to delete Synch Data?');">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </a>

                                    <?php
                                    if (!$file->processed) {
                                        ?>
                                        | 

                                        <a class="red" href="<?= site_url('super/synch_manager/process/' . $file->id); ?>" title="Process Synch Data" onclick="return confirm('Are you sure you wish to process this file?');">
                                            <i class="glyphicon glyphicon-cog"></i>
                                        </a>
                                        <?php }
                                    ?>
                                </td>
                            </tr> 
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

