<div class="row">
    <div class="col-md-12 panel-info">
        <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <?php echo empty($edc_detail->edcname) ? '<i class="glyphicon glyphicon-plus"></i> Add Exam Development Centre' : '<i class="glyphicon glyphicon-edit"></i> Edit Exam Development Centre'; ?>
                </div>
        </div>
        <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>

                <?= form_open_multipart('', 'class="form-horizontal" role="form"'); ?>
                    <div class="row">
                        <div class="col-lg-12">
                             <h4>Exam Development Centre - Details</h4>
                             <hr/>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Name:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="edcname" value="<?php echo set_value('edcname', $edc_detail->edcname); ?>" class="form-control" placeholder="Edc Name" required="required">
                                </div>
                             </div>
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Url:</label>
                                <div class="col-sm-10">
                                    <input type="url" name="edcweburl" value="<?php echo set_value('edcweburl', $edc_detail->edcweburl); ?>" class="form-control" placeholder="http://example.edc.com/" required="required">
                                </div>
                             </div>
                                                        
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Phone:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="edcphone" value="<?php echo set_value('edcphone', $edc_detail->edcphone); ?>" class="form-control" placeholder="Edc Contact Number" required="required">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Address:</label>
                                <div class="col-sm-10">
                                    <textarea type="text" name="edcaddress" class="form-control" placeholder="Edc Address" rows="2" required="required"><?php echo set_value('edcaddress', $edc_detail->edcaddress); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php 
                                    $logo = get_img('no_image.jpg');
                                    if(isset($edc_detail->edclogo)){
                                        $logo = get_img('edc_logos/'.$edc_detail->edclogo);
                                    }
                                ?>
                                <div class="col-sm-offset-2 col-sm-10">
                                    <strong>Logo</strong>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 150px; height: 112px;"><img src="<?= $logo; ?>" alt="Screen Shot" /></div>
                                       <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                       <div>
                                           <span class="btn btn-file btn-default"><span class="fileupload-new"><input type="file" name="edclogo"/></span><span class="fileupload-exists">Change</span><input type="file" /></span>
                                           <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                                       </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Theme Color:</label>
                                <div class="col-sm-10">
                                    <div class="bfh-colorpicker" data-name="themecolor" data-color="<?= set_value('themecolor', $edc_detail->themecolor); ?>" ></div>
                                </div>
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                    <?php 
                                        $stamp = get_img('no_image.jpg');
                                        if(file_exists('./resources/images/edc_logos/'.$edc_detail->edcid.'_stamp.png')){
                                            $stamp = get_img('edc_logos/'.$edc_detail->edcid.'_stamp.png');
                                        }
                                    ?>
                                    <div class="col-sm-8">
                                        <strong>Edc Stamp</strong>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 150px; height: 112px;"><img src="<?= $stamp; ?>" alt="Logo" /></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                           <div>
                                               <span class="btn btn-file btn-default"><span class="fileupload-new"><input type="file" name="stamp"/></span><span class="fileupload-exists">Change</span><input type="file" /></span>
                                               <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                             <div class="page-header"><h4>EDC Administrator Details</h4></div>
                             The Default Password for the Admin is <strong style="color: crimson;">Password2$</strong>
                             <br/><br/>
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Fullname:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="fullname" value="<?php echo set_value('fullname', $edc_detail->fullname); ?>" class="form-control" placeholder="Administrator Fullname" required="required">
                                </div>
                             </div>
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email:</label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" value="<?php echo set_value('email', $edc_detail->email); ?>" class="form-control" placeholder="Administrator Email" required="required">
                                </div>
                             </div>
                             
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Phone:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="phone" value="<?php echo set_value('phone', $edc_detail->phone); ?>" class="form-control" placeholder="Administrator Phone" required="required">
                                </div>
                             </div>
                             
                             <div class="form-group">
                              <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                <a href="<?= site_url('super/edcs');?>" class="btn btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                              </div>
                            </div>
                         </div>
                       </div>
                    </div>
            
                   
                <?= form_close(); ?>
             </div>
        </div>
    </div>