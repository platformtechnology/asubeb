<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>

<br/>

<div class="row">
    <div class="col-md-6">
        
            <div class="content-box-header">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-download-alt"></i> Download Result Data
                </div>
            </div>
            <div class="content-box-large box-with-header">
                 <div class="panel-body">
                    <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                    <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                        <br />
                        <div class="col-md-12">
                        <input type="hidden" name="edcid" value="<?= $edcs->edcid?>" >
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Exam Type:</label>
                            <div class="col-sm-8">
                                <?php 
                                  $select_options = array();
                                  foreach ($exams as $exam) {
                                      $select_options[$exam->examid] = strtoupper($exam->examname);
                                  }
                                  $select_options[''] = "----- Exam -----";
                                  echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'class="form-control" required="required"');
                                ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Exam Year:</label>
                           <div class="col-sm-8">
                              <?php 
                                    $already_selected_value = $this->input->post('examyear') ? $this->input->post('examyear') : $activeyear;
                                    $start_year = $startyear;
                                     print '<select name="examyear" class="form-control" required="required">';
                                     print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>----- Year -----</option>';
                                     for ($x = $start_year; $x <= $activeyear; $x++) {
                                         print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                                     }
                                     print '</select>';
                               ?>
                           </div>
                       </div> 
                       <div class="form-group">
                           <button type="submit" class="btn btn-small btn-primary btn-block"><i class="glyphicon glyphicon-download"></i> Download Result</button>
                       </div>
                        
                    </div>

                    <?= form_close(); ?>
                   </div>
                </div>  
        
    </div>
    
    <div class="col-md-6">
        
            <div class="content-box-header">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-refresh"></i> Resend Result
                </div>
            </div>
            <div class="content-box-large box-with-header">
                 <div class="panel-body">
                    <?php if($this->session->flashdata('searchmsg')) echo get_success($this->session->flashdata('searchmsg')); ?>
                    <?php if($this->session->flashdata('searcherror')) echo get_error($this->session->flashdata('searcherror')); ?>
                    
                    <?= form_open(site_url('super/sms/resend/'.$edcs->edcid), 'class="form-horizontal" role="form"'); ?>
                       
                        <div class="col-md-12">
                           
                                <div class="col-sm-offset-4 col-sm-8">
                                    <h5 style="color: crimson">You must be connected ONLINE to perform this operation.</h5>
                                 </div>
                           
                        <div class="form-group">
                           
                            <label for="inputEmail3" class="col-sm-4 control-label">Exam Type:</label>
                            <div class="col-sm-8">
                                <?php 
                                  $select_options = array();
                                  foreach ($exams as $exam) {
                                      $select_options[$exam->examid] = strtoupper($exam->examname);
                                  }
                                  $select_options[''] = "----- Exam -----";
                                  echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'class="form-control" required="required"');
                                ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Exam Year:</label>
                           <div class="col-sm-8">
                              <?php 
                                    $already_selected_value = $this->input->post('examyear') ? $this->input->post('examyear') : $activeyear;
                                    $start_year = $startyear;
                                     print '<select name="examyear" class="form-control" required="required">';
                                     print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>----- Year -----</option>';
                                     for ($x = $start_year; $x <= $activeyear; $x++) {
                                         print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                                     }
                                     print '</select>';
                               ?>
                           </div>
                       </div> 
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Exam Number:</label>
                            <div class="col-sm-8">
                                <input type="text" name="examno" value="<?php echo set_value('examno'); ?>" class="form-control" id="inputEmail3" placeholder="Enter ExamNo " required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Phone (Optional):</label>
                            <div class="col-sm-8">
                                <input type="text" name="phone" value="<?php echo set_value('phone'); ?>" class="form-control" id="inputEmail3" placeholder="Enter Optional Phone No " >
                                <br/>
                                If phone number is supplied, the previous one will be overwritten and the result will be sent to this new one
                            </div>
                        </div>
                       <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-sm btn-danger btn-block"><i class="glyphicon glyphicon-refresh"></i> Resend Result</button>
                              </div>
                            </div>
                        
                    </div>

                    <?= form_close(); ?>
                   </div>
                </div>  
        
    </div>
    
</div>
