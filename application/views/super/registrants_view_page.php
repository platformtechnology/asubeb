<div class="row" id="printdiv">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                 
                    <?= get_edc_logo($edclogo, $edcs->edcname); ?>
                   <br/> <br/> <br/>
                    <i class="glyphicon glyphicon-user"></i> Candidate Detail
               
            </div>
            <div class="panel-body">
               
                <table class="table table-bordered" width="100%">
                <tr>
                  <td><strong>Registered Exam:</strong></td>
                  <td><strong><?= $exam_info->examname; ?></strong></td>
                  <td rowspan="4" valign="top">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 150px; height: 112px;"><img src="<?= $passport; ?>" alt="Passport" /></div>
                             <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                          </div>              
                    </td>
                </tr>
                <tr>
                  <td><strong>Exam Year:</strong></td>
                  <td><strong><?= $reg_info->examyear; ?></strong></td>
                </tr> 
                <tr>
                  <td>&nbsp;</td>
                 <td>&nbsp;</td>
                </tr> 
                 <tr>
                    <td><strong>Local Govt Area:</strong></td>
                    <td><?= $this->registrant_model->get_lga($reg_info->lgaid); ?> </td>
                  </tr>
                  <tr>
                   <td><strong>School:</strong></td>
                    <td><?= $this->registrant_model->get_school($reg_info->schoolid); ?> </td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Exam Centre:</strong></td>
                    <td><?= $this->registrant_model->get_school($reg_info->centreid); ?> </td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td valign="top">
                        <strong>Exam No:  <span style="color:crimson;"><?= $reg_info->examno; ?></span></strong>
                    </td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td valign="top">
                        <strong>Pin No:  <span style="color:crimson;"><?= $candidate_pin_info->pin; ?></span></strong>
                    </td>
                    <td>&nbsp;</td>
                  </tr>
                   <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                     <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Firstname:</strong></td>
                    <td><?= $reg_info->firstname; ?></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Othernames:</strong></td>
                    <td><?= $reg_info->othernames; ?></td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Gender:</strong></td>
                    <td><?= ($reg_info->gender == 'M' ? 'Male' : 'Female') ;?></td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Dob:</strong></td>
                    <td><?= $reg_info->dob; ?></td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Phone:</strong></td>
                    <td><?= $reg_info->phone; ?></td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                   <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                  </tr>

                  <tr>
                  <td><strong>Subjects:</strong></td>
                  <td>
                        <?php 
                          if(count($subjects)):
                                $sn = 0;
                                foreach ($subjects as $subject) {
                                    echo ++$sn .'. <strong>'.$this->registrant_model->get_subject($subject->subjectid);
                                        if($subject->armid != '0'){
                                            $armdata = $this->db->get_where('t_subjects_arms', array('armid' => $subject->armid))->row();
                                            echo ' (' . ucwords(strtolower($armdata->armname)) . ') ';
                                        }
                                    echo ' </strong><br/> ';
                                }
                            endif;
                         ?>
                    </td>
                    <td valign="top">&nbsp;</td>
                  </tr>

                  <tr>
                   <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign="top">&nbsp;  </td>
                  </tr>
                  <?php 
                    if($exam_info->hasposting == 1):
                  ?>
                  <tr>
                    <td><strong>First Choice:</strong></td>
                    <td><?= $this->registrant_model->get_school($reg_info->firstchoice); ?> </td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Second Choice:</strong></td>
                    <td><?= $this->registrant_model->get_school($reg_info->secondchoice); ?> </td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <?php
                    endif;
                  ?>
                  
                </table>
        
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p><a href="<?=  site_url('super/registrants/filter/'.$edcs->edcid);?>" class="btn btn-primary"><i class="icon-arrow-left icon-white"></i> Back</a>
            <button type="button" onclick="printDiv('printdiv');" class="btn btn-warning"><i class="icon-print icon-white"></i> Print</button>
            <a href="<?=  site_url('super/registrants/delete/'.$edcs->edcid.'/'.$reg_info->candidateid);?>" class="btn btn-danger"><i class="icon-remove icon-white"></i> Delete</a>
        </p>
    </div>
</div>
<script lang="javascript" type="text/javascript">
            function printDiv(divID) {
                //Get the HTML of div
                var divElements = document.getElementById(divID).innerHTML;
                //Get the HTML of whole page
                var oldPage = document.body.innerHTML;

                //Reset the page's HTML with div's HTML only
                document.body.innerHTML =
                  "<html><head></head><body>" +
                  divElements + "</body>";

                //Print Page
                window.print();

                //Restore orignal HTML
                document.body.innerHTML = oldPage;
            }
            
    </script>