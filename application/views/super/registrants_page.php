
<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>
<br/>
<div class="row">
    <div class="col-md-12 panel-info">
        <div class="content-box-header panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-search"></i> Advanced Search</div>
        </div>
        
             <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                 <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Zone:</label>
                                <div class="col-sm-9">
                                     <?php 
                                          $select_options = array();
                                          foreach ($zones as $zone) {
                                              $select_options[$zone->zoneid] = $zone->zonename;
                                          }
                                          $select_options[''] = "---------------";
                                          echo form_dropdown('zoneid', $select_options, $this->input->post('zoneid') ? $this->input->post('zoneid') : '', 'class="form-control" id="zoneid"');
                                        ?>
                                </div>
                            </div>

                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">LGA:</label>
                                <div class="col-sm-9">
                                    <?php
                                        $select_options = array();
                                        $select_options[''] = "--------------";

                                          foreach ($lgas as $lg) {
                                              $select_options[$lg->lgaid] = $lg->lganame;
                                          }

                                        echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : '', 'id="lgaid" class="form-control" '); 
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                               <label for="inputEmail3" class="col-sm-3 control-label">School:</label>
                               <div class="col-sm-9">
                                   <?php
                                       $select_options = array();
                                       $select_options[''] = "---- All Schools ----";
                                       if(($this->input->post('schoolid'))){
                                         foreach ($schools as $school) {
                                             $select_options[$school->schoolid] = strtoupper($school->schoolname);
                                         }
                                       }
                                       echo form_dropdown('schoolid', $select_options, ($this->input->post('schoolid') ? $this->input->post('schoolid') : ''), 'id="schoolid" class="form-control"'); 
                                   ?>
                               </div>
                           </div>
                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                                   <label for="inputEmail3" class="col-sm-3 control-label">Exam Type:</label>
                                   <div class="col-sm-9">
                                       <?php 
                                         $select_options = array();
                                           foreach ($exams as $exam) {
                                               $select_options[$exam->examid] = $exam->examname;
                                           }
                                         $select_options[''] = "---- All Exams ----";
                                         echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'id="examid" class="form-control"');
                                       ?>
                                   </div>
                               </div>

                              <div class="form-group">
                                   <label for="inputEmail3" class="col-sm-3 control-label">Exam Year:</label>
                                   <div class="col-sm-9">
                                       <?php 
                                           $already_selected_value = ($this->input->post('examyear') ? $this->input->post('examyear') : $activeyear);
                                           $start_year = $startyear;
                                            print '<select name="examyear" class="form-control">';
                                            print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>..::All Year::..</option>';
                                            for ($x = $start_year; $x <= $activeyear; $x++) {
                                                print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                                            }
                                            print '</select>';
                                          ?>
                                   </div>
                               </div>

                           <div class="form-group">
                             <div class="col-sm-offset-3 col-sm-9">
                               <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-search"></i> Search</button>
                               <a href="<?= site_url('super/registrants/filter/'.$edcs->edcid);?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-refresh"></i> Refresh</a>
                               <a href="" data-toggle="modal" data-target="#exampleModal" title="Pro-Advanced Search" ><i class="glyphicon glyphicon-ok"></i> Pro-Advanced Search</a>
                             </div>
                           </div>
                        </div>
                     
                      </div>
                    <?= form_close(); ?>                
               
             </div>
        </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-user"></i> All Candidates</div>
            </div>
            <div class="panel-body">
                <?= form_open(site_url('super/registrants/export_to_excel'), 'class="form-horizontal" role="form"'); ?>
                    <button type="submit" class="btn btn-default"><img src="<?= get_img('excel_icon.png');?>" width="20px" height="20px" border="0" /><strong> Export To Excel</strong></button>
                    <a href="<?=  site_url('super/registrants/export_to_omr_format');?>" class="btn btn-default"><i class="glyphicon  glyphicon-list-alt"></i><strong> Get OMR Data</strong></a>
                     <a href="<?=  site_url('super/registrants/printdetails/'.$edcs->edcid);?>" class="btn btn-default"><i class="glyphicon  glyphicon-print"></i><strong> Print Details</strong></a>                     
                 <?= form_close(); ?>  
                    <br/><br/>
                    <table class="table table-bordered table-condensed" id="example">
                      <thead>
                        <tr>
                          <th>Exam No</th>
                          <th>Candidate Name</th>
                          <th>Gender</th>
                          <th>Exam Type</th>
                          <th>Exam Year</th>
<!--                          <th>EDC</th>-->
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php 
                            if(count($registrants)): 
                                foreach ($registrants as $registrant):
                                    $name = ucfirst(strtolower($registrant->firstname)) . ' ' . ucfirst(strtolower($registrant->othernames));
                            ?>
                                    <tr>
                                        <td><?= $registrant->examno; ?></td>
                                        <td><?php echo anchor(site_url('super/registrants/view/' . $registrant->candidateid), $name, 'title="View Details"'); ?></td>
                                        <td><?= $registrant->gender == 'M' ? 'Male' : 'Female'; ?></td>
                                        <td><?= $this->registrant_model->getExamName_From_Id($registrant->examid); ?></td>
                                         <td><?= $registrant->examyear; ?></td>
<!--                                        <td><?= $this->registrant_model->getEdcName_From_Id($registrant->edcid); ?></td>-->
                                        <td>
                                            <?= anchor(site_url('super/registrants/view/' . $registrant->candidateid), '<i class="glyphicon glyphicon-eye-open"></i>', 'title="View Details"'); ?>
                                            <?= anchor(site_url('super/registrants/printcandidate/' . $registrant->candidateid), '<i class="glyphicon glyphicon-print"></i>', 'title="Print Details"'); ?>
                                            <?= get_del_btn(site_url('super/registrants/delete/'. $registrant->edcid . '/' . $registrant->candidateid)); ?>
                                        </td>
                                    </tr> 
                                <?php endforeach;?>
                            <?php endif; ?>
                      </tbody>
                    </table>
                    
            </div>
        </div>
    </div>
</div>


    
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Pro-Advanced Search</h4>
      </div>
      
       <?= form_open(site_url('super/registrants/prosearch/'.$edcs->edcid), 'class="form-horizontal" role="form"'); ?>
      <div class="modal-body">
                        
                 <strong style="color: crimson;">The Pro-Advanced Search only fetches candidates in all schools for a particular exam in a particular Local Govt Area.</strong>
                 <br/><br/>
                <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">LGA:</label>
                                <div class="col-sm-9">
                                    <?php
                                        $select_options = array();
                                        $select_options[''] = "------ Select Lga --------";

                                          foreach ($lgas as $lg) {
                                              $select_options[$lg->lgaid] = $lg->lganame;
                                          }

                                        echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : '', ' required="required" class="form-control" '); 
                                    ?>
                                </div>
                            </div>
                             <div class="form-group">
                                   <label for="inputEmail3" class="col-sm-3 control-label">Exam Type:</label>
                                   <div class="col-sm-9">
                                       <?php 
                                         $select_options = array();
                                           foreach ($exams as $exam) {
                                               $select_options[$exam->examid] = $exam->examname;
                                           }
                                         $select_options[''] = "---- Select Exam ----";
                                         echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'id="examid" class="form-control"');
                                       ?>
                                   </div>
                               </div>

                              <div class="form-group">
                                   <label for="inputEmail3" class="col-sm-3 control-label">Exam Year:</label>
                                   <div class="col-sm-9">
                                       <?php 
                                           $already_selected_value = ($this->input->post('examyear') ? $this->input->post('examyear') : $activeyear);
                                           $start_year = $startyear;
                                            print '<select name="examyear" class="form-control">';
                                            print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>..::All Year::..</option>';
                                            for ($x = $start_year; $x <= $activeyear; $x++) {
                                                print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                                            }
                                            print '</select>';
                                          ?>
                                   </div>
                               </div>
                 
                  <div class="form-group">
                                   <label for="inputEmail3" class="col-sm-3 control-label">From Date:</label>
                                   <div class="col-sm-9">
                                       <div class="bfh-datepicker" data-format="y-m-d" data-name="fromdate" data-date="<?php echo set_value('fromdate', 'today'); ?>"></div>
                                   </div>
                               </div>
                 
                  <div class="form-group">
                                   <label for="inputEmail3" class="col-sm-3 control-label">From Time:</label>
                                   <div class="col-sm-9">
                                       <div class="bfh-timepicker" data-mode="24h" data-name="fromtime"></div>
                                   </div>
                               </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-saved"></i> Fetch </button>
      </div>
      <?= form_close(); ?>
    </div>
  </div>
</div>


  <script type="text/javascript">
     (function() {
        var httpRequest;
        
        lgaddl = document.getElementById("lgaid");
        zoneddl = document.getElementById("zoneid");
        
        lgaddl.onchange = function() { 
            var target_url3 = "<?= site_url('super/registrants/school_ajaxdrop?lgaid='); ?>";
            makeRequest(target_url3 + lgaddl.value + "&zoneid=" + zoneddl.value , 'schoolid'); 
        };
        
        zoneddl.onchange = function() { 
            var target_url = "<?= site_url('super/schools/ajaxdrop?zoneid='); ?>";
            makeRequest( target_url + zoneddl.value + "&edcid=" + "<?=$edcs->edcid?>", 'lgaid' ); 
        };
        
        function makeRequest(url, targetid) {
            httpRequest = getHttpObject();

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = function(){
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        //alert(httpRequest.response);
                        var data = JSON.parse(httpRequest.response);
                        var select = document.getElementById(targetid);
                        if(emptySelect(select)){
                            var el = document.createElement("option");
                                    if(targetid == 'lgaid') el.textContent = '-------------';
                                    else el.textContent = '---- All Schools ----';
                                    
                                    el.value = '';
                                    select.appendChild(el);
                                    
                            for (var i = 0; i < data.lgas.length; i++){
                                var el = document.createElement("option");
                                    el.textContent = data.lgas[i].lganame;
                                    el.value = data.lgas[i].lgaid;
                                    select.appendChild(el);
                            }
                            
                            //This implies that zone requested the change 
                            //and school drop down should also play a part
                           if(targetid == 'lgaid'){
                               var select2 = document.getElementById('schoolid');
                               var element = document.createElement("option");
                               emptySelect(select2);
                               
                               element.textContent = '---- All Schools ----';
                               element.value = '';
                               select2.appendChild(element);
                               
                               for (var i = 0; i < data.schools.length; i++){
                                    var element = document.createElement("option");
                                        element.textContent = data.schools[i].lganame;
                                        element.value = data.schools[i].lgaid;
                                        select2.appendChild(element);
                                }
                           }
                        }
                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
            httpRequest.send();
        }

        function emptySelect(select_object){
            var i;
            for(i=select_object.options.length-1;i>=0;i--){
                select_object.remove(i);
            }
            return true;
        }
        
         function getHttpObject(){

            var xmlhtp;

            if (window.ActiveXObject) 
            {    
                var aVersions = [ 
                  "MSXML2.XMLHttp.9.0","MSXML2.XMLHttp.8.0", "MSXML2.XMLHttp.7.0",
                  "MSXML2.XMLHttp.6.0","MSXML2.XMLHttp.5.0","MSXML2.XMLHttp.4.0",
                  "MSXML2.XMLHttp.3.0","MSXML2.XMLHttp","Microsoft.XMLHttp"
                ];

                for (var i = 0; i < aVersions.length; i++) 
                {
                    try 
                    { 
                        xmlhtp = new ActiveXObject(aVersions[i]);
                        return xmlhtp;
                    } catch (e) {}
                }
            }
            else  if (typeof XMLHttpRequest != 'undefined') 
            {
                    xmlhtp = new XMLHttpRequest();			
                    return xmlhtp;
            } 
            throw new Error("XMLHttp object could be created.");

        }
    })();


    </script>