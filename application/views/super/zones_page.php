<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>

<br/>

<div class="row">
    <div class="col-md-12">
       
            <div class="content-box-header">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> Setup Zones</div>
            </div>
         <div class="content-box-large box-with-header">
            <div class="panel-body">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Zone Name:</label>
                            <div class="col-sm-8">
                                <input type="text" name="zonename" value="<?php echo set_value('zonename', $zone_detail->zonename); ?>" class="form-control" id="inputEmail3" placeholder="Enter Zone Name" required="required">
                            </div>
                        </div>
                       
                         <div class="form-group">
                               <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                <a href="<?= site_url('super/zones/save/'. $edcs->edcid);?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                              </div>
                            </div>
                    </div>
                 <?= form_close(); ?>
                
                <div class="col-md-7">
                    <table class="table table-bordered table-condensed" id="example">
                      <thead>
                        <tr>
                          <th>sn</th>
                          <th>Zone Name</th>
                          <th>Date Created</th>                          
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php 
                            if(count($zones)): 
                                $sn = 0;
                                foreach ($zones as $zone):
                            ?>
                            <tr>
                                <td><?= ++$sn; ?></td>
                                <td><?php echo anchor(site_url('super/zones/save/' . $edcs->edcid . '/' . $zone->zoneid), $zone->zonename) ?></td>
                                <td><?= $zone->datecreated; ?></td>
                                <td>
                                    <?= get_edit_btn(site_url('super/zones/save/' . $edcs->edcid . '/' . $zone->zoneid)); ?>
                                    <?= get_del_btn(site_url('super/zones/delete/' . $edcs->edcid . '/' . $zone->zoneid)); ?>
                                </td>
                            </tr> 
                            <?php endforeach;?>
                            <?php endif; ?>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
