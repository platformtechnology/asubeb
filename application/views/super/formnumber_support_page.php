<div class="row">
    <div class="col-md-12">
        <div class ="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <span style="font-size: 18px"> <i class="glyphicon glyphicon-remove-sign"></i> Form Number Stat </span>
                    </div>
                    <div class="panel-body">
                            <?php if (count($form_number_stat)):?>
                        <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td> Info</td>
                                            <td> Details</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td> Total Form Number</td>
                                            <td> <?php echo $form_number_stat['total']?></td>
                                        </tr>
                                        <tr>
                                            <td> Total Form Number Used</td>
                                            <td> <?php echo $form_number_stat['used']?></td>
                                        </tr>
                                        <tr>
                                            <td> Total Form Number Remaining</td>
                                            <td> <?php echo $form_number_stat['total'] - $form_number_stat['used']?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            <?php endif;?>
                    </div>
                </div>
        </div>
                <div class ="col-md-6">
            <div class="content-box-large ">
                <div class="panel-heading">
                    <div class="panel-title">
                        <i class="glyphicon glyphicon-remove-sign"></i> Confirm Form Number
                    </div>
                </div>
                <div class="panel-body">

                    <?php echo $error_message ?>
                    <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label"> Enter Form Number</label>
                        <div class="col-sm-8">
                            <input type="text" name="formnumber" class="form-control" placeholder="Form Number" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-arrow-right"></i> Continue</button>
                        </div>
                    </div>


                </div>
            <?php echo form_close(); ?>

                    <?php
                    if (count($form_number_details)){
                        echo "<h4 class = 'alert alert-success'> Form Number Found!</h4>";
                    ?>
                <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <td> Form Number</td>
                            <td> <?php echo $form_number_details->formnumber ?></td>
                        </tr>
                        <tr>
                            <td> Used Pin</td>
                            <td> <?php echo $form_number_details->pin ?></td>
                        </tr>
                        <tr>
                            <td> Candidate Name</td>
                            <td> <?php echo $form_number_details->firstname .' ' .  $form_number_details->othernames?></td>
                        </tr>
                        <tr>
                            <td> School Name</td>
                            <td> <?php echo $form_number_details->schoolname ?></td>
                        </tr>
                        <tr>
                            <td> Exam Number</td>
                            <td> <?php echo $form_number_details->examno ?></td>
                        </tr>
                        <tr>
                            <td> Exam Registered For</td>
                            <td> <?php echo $form_number_details->examname ?></td>
                        </tr>
                        <tr>
                            <td> EDC</td>
                            <td> <?php echo $form_number_details->edcname ?></td>
                        </tr>
                    </table>

                    <?php
                    }
                    ?>
            </div>


        </div>

    </div>
</div>
