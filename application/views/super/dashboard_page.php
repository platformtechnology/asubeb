<?php
$edc_name = ucwords(strtolower($edc_name));
?>
<div class="container">
    <div class="col-md-offset-3 col-md-6">
        <div class="panel-body">
            <?= form_open('', 'id="chart_form" name="chart_form"'); ?>
            <div class="col-md-10">
                <div class="form-group">
                    <div class="col-sm-12">
                        <?php
                        if (count($edcs)) {
                            $select_options = array();
                            foreach ($edcs as $edc) {
                                $select_options[$edc->edcid] = $edc->edcname;
                            }
                            echo form_dropdown('edcid', $select_options, $this->input->post('edcid') ? $this->input->post('edcid') : $selectedEdc, 'class="form-control" style="font-weight: bold;"');
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <button type="submit" class="btn btn-info"><i class="glyphicon glyphicon-search"></i> View</button>
            </div>

            <?= form_close(); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">

        <div class="row">
            <div class="col-md-12">
                <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title"><i class="glyphicon glyphicon-stats"></i> No Of Candidates Per Exams Per Year - (<?= $edc_name ?>)</div>
                        <div class="panel-options">
                            <a href="<?= site_url('super/dashboard'); ?>" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div id="reg_exams_div" style="height: 230px;"></div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="content-box-header">
                    <div class="panel-title"><i class="glyphicon glyphicon-log-in"></i> Summary Of Uploaded <strong>PINs</strong></div>
                </div>
                <div class="content-box-large box-with-header">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Edc</th>
                                <th>Volume</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sn = 0;
                            $totalP = 0;
                            if (count($pin_upload_summary)):
                                foreach ($pin_upload_summary as $summary):
                                    ?>
                                    <tr>
                                        <td><?= ++$sn ?></td>
                                        <td><?= $summary->date; ?></td>
                                        <td><?= $summary->edcname; ?></td>
                                        <td><?= number_format($summary->volume); ?></td>
                                    </tr>

                                    <?php
                                    $totalP += $summary->volume;
                                endforeach;
                                ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                    <strong>Total Pins Uploaded: <span style="color:crimson"><?= number_format($totalP); ?></span></strong>
                </div>
            </div>
        </div>

    </div>


    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="content-box-header">
                    <div class="panel-title"><i class="glyphicon glyphicon-user"></i> Candidate Summary For <strong>Active Year</strong> (<?= $activeyear; ?>) - - (<?= $edc_name ?>)</div>
                </div>
                <div class="content-box-large box-with-header">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Exam Type</th>
                                <th>No of Candidates</th>
                                <th>Active Year</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sn = 0;
                            $totalC = 0;
                            if (count($candidate_summary)):
                                foreach ($candidate_summary as $summary):
                                    ?>
                                    <tr>
                                        <td><?= ++$sn ?></td>
                                        <td><?= $summary->examname; ?></td>
                                        <td><?= $summary->candidatecount; ?></td>
                                        <td><?= $summary->examyear; ?></td>
                                        <td><?php //$summary->resultchecked; ?></td>
                                    </tr>

                                    <?php
                                    $totalC += $summary->candidatecount;
                                endforeach;
                                ?>
<?php endif; ?>
                        </tbody>
                    </table>
                    <strong>Total Candidates: <span style="color:crimson"><?= $totalC; ?></span></strong>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="content-box-header">
                    <div class="panel-title"><i class="glyphicon glyphicon-log-in"></i> Summary Of Used <strong>PINs</strong></div>
                </div>
                <div class="content-box-large box-with-header">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Year</th>
                                <th>Edc</th>
                                <th>Volume</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sn = 0;
                            $totalP = 0;
                            if (count($pinstat_summary)):
                                foreach ($pinstat_summary as $summary):
                                    ?>
                                    <tr>
                                        <td><?= ++$sn ?></td>
                                        <td><?= $summary->examyear; ?></td>
                                        <td><?= $summary->edcname; ?></td>
                                        <td><?= number_format($summary->pincount); ?></td>
                                    </tr>

                                    <?php
                                    $totalP += $summary->pincount;
                                endforeach;
                                ?>
<?php endif; ?>
                        </tbody>
                    </table>
                    <strong>Total USED: <span style="color:crimson"><?= number_format($totalP); ?></span></strong>
                </div>
            </div>
        </div>
    </div>

</div> <!--END MIDDLE ROW-->
