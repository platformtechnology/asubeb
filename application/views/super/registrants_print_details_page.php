﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?= config_item('site_name') . ' - ' . $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
    
    <?= $page_level_styles; ?>
    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script lang="javascript" type="text/javascript">
            function printn() {
                    window.print();
                    var target = "<?= site_url('super/registrants/filter/'.$edcs->edcid); ?>";
                    window.location = target;
               }
    </script>
  </head>
  <body data-offset="40" onload="printn()" style="background-color: #fff;">   
    <div class="shadow-top"></div>
    <div class="container">
    <div class="row">
            <div class="span12">
                <div class="page-header">
                    <h4><?= get_edc_logo($edclogo, $edcs->edcname); ?> The Ministry Of Education, <?= $edcs->edcname;?></h4>
                    
                    <br/>
<!--                    <h3 style="color: #0077b3;">
                        CANDIDATES INFORMATION
                    </h3>-->
                </div>
            </div>
    </div>
<div class="row">
    <div class="span12">
       <table class="table table-bordered table-condensed">
          <thead>
            <tr>
              <th>Exam No</th>
              <th>Candidate Name</th>
              <th>Gender</th>
<!--              <th>Dob</th>
              <th>Phone</th>
              <th>Exam Registered</th>
              <th>Exam Year</th>
              <th>School</th>-->
              <?php
                foreach ($exam_subjects as $subject) {
                    echo '<th>'.ucwords(strtolower($subject->subjectname)).'</th>';
                }
              ?>
              <?php
//                if($exam_detail->hasposting == 1){
//                    echo '<th>Firstchoice</th>';
//                    echo '<th>Secondchoice</th>';
//                }
              ?>
            </tr>
          </thead>
          <tbody>
               <?php 
                if(count($printdata)): 
                    foreach ($printdata as $registrant):
                        $name = ucfirst(strtolower($registrant->firstname)) . ' ' . ucfirst(strtolower($registrant->othernames));
                ?>
                <tr>
                    <td><?= $registrant->examno; ?></td>
                    <td><?= $name ?></td>
                    <td><?= $registrant->gender == 'M' ? 'Male' : 'Female'; ?></td>
<!--                    <td><?= $registrant->dob; ?></td>
                    <td><?= $registrant->phone; ?></td>
                    <td><?= $this->registrant_model->getExamName_From_Id($registrant->examid); ?></td>
                    <td><?= $registrant->examyear; ?></td>
                    <td><?= $this->registrant_model->get_school($registrant->schoolid); ?></td>-->
                    <?php
                        foreach ($exam_subjects as $subject) {
                            echo '<th>&nbsp;</th>';
                        }
                      ?>
                    <?php
//                        if($exam_detail->hasposting == 1){
//                            echo '<td>'.$this->registration_model->get_school($registrant->firstchoice).'/<td>';
//                            echo '<td>'.$this->registration_model->get_school($registrant->secondchoice).'/<td>';;
//                        }
                    ?>
                </tr> 
                <?php endforeach;?>
                <?php endif; ?>
          </tbody>
        </table>
    </div>
 
  </div>
</div>

 </body>
</html>