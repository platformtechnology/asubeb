<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>

<br/>
<div class="row">
    <div class="col-md-6">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-cog"></i> Layout Settings</strong>
                </div>
            </div>
             <div class="content-box-large box-with-header">
                <?php if(isset($error)) echo get_error($error); ?>
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>

                 <hr/>
                       
                        <div class="form-group">
                            <label class="col-sm-offset-1 col-sm-4 control-label">SUBJECTS</label>
                            <label class="col-sm-4 control-label">NO OF COLS</label>
                        </div>
                        <input type="hidden" name="submitTracker" value="1" />
                        <div class="form-group">
                           
                                  <?php 
                                    $count = 0;
                                                                       
                                    foreach ($subjects as $subject) {
                                        $subjBuffer = array();
                                        
                                        if(!empty($criterias)){
                                                    
                                            foreach ($criterias as $criteria) {
                                                if($subject->subjectid == $criteria->subjectid){
                                                    echo ' <div class="col-sm-offset-2 col-sm-4">';
                                                    echo '<input name="subjectid[]" type="checkbox" value="'.$criteria->subjectid.'" checked="true"/><strong> ' . ucfirst($subject->subjectname) . '</strong> &nbsp;';
                                                    echo ' </div>';
                                                    echo ' <div class="col-sm-4">';
                                                    echo '<input type="text" class="form-control" name="numofcols[]" value="'. set_value('numofcols', $criteria->numofcols) .'" />';
                                                    echo ' </div>';
                                                    echo '<br/><br/><br/>';
                                                    $subjBuffer[] = $criteria->subjectid;
                                                   
                                                }
                                            }
                                            
                                        }else{
                                                echo ' <div class="col-sm-offset-2 col-sm-4">';
                                                    $str = '<input name="subjectid[]" type="checkbox" value="'.$subject->subjectid.'" ';

//                                                            if($this->input->post('subjectid')){
//                                                                foreach($this->input->post('subjectid') as $postedid){
//                                                                    if($subject->subjectid == $postedid){
//                                                                        $str .= 'checked="true" ';
//                                                                        break;
//                                                                    }
//                                                                }
//                                                            }

                                                       $str .=  '/><strong> ' . ucfirst($subject->subjectname) . '</strong> &nbsp;';
                                                    echo $str;
                                                    echo ' </div>';
                                                    echo ' <div class="col-sm-4">';
                                                    echo '<input type="text" class="form-control" name="numofcols[]" value="" />';
                                                    echo ' </div>';
                                                    echo '<br/><br/><br/>';
                                        }
                                        
                                        if(isset($criteria) && !in_array($subject->subjectid, $subjBuffer)){

                                                     echo ' <div class="col-sm-offset-2 col-sm-4">';
                                                    $str = '<input name="subjectid[]" type="checkbox" value="'.$subject->subjectid.'" ';

//                                                            if($this->input->post('subjectid')){
//                                                                foreach($this->input->post('subjectid') as $postedid){
//                                                                    if($subject->subjectid == $postedid){
//                                                                        $str .= 'checked="true" ';
//                                                                        break;
//                                                                    }
//                                                                }
//                                                            }

                                                       $str .=  '/><strong> ' . ucfirst($subject->subjectname) . '</strong> &nbsp;';
                                                    echo $str;
                                                    echo ' </div>';
                                                    echo ' <div class="col-sm-4">';
                                                    echo '<input type="text" class="form-control" name="numofcols[]" value="" />';
                                                    echo ' </div>';
                                                    echo '<br/><br/><br/>';
                                        }
                                    }
                                   ?>
                           
                        </div>
                 <hr/>
                        
                    <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-arrow-right"></i> Save Layout</button>
                      </div>
                    </div>
                <?php echo form_close(); ?>
             </div>
    </div>
    
</div>