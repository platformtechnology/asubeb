<div class="row">

        <div class="col-md-12">
        
            <div class="content-box-header">
                <div class="panel-title">All EDCs</div>
            </div>
            <div class="content-box-large box-with-header">
            <div class="panel-body">
                <?php if($this->session->flashdata('updated_msg')) echo get_success($this->session->flashdata('updated_msg')); ?>
                <table class="table table-bordered table-condensed table-striped" id="example">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>EDC NAME</th>
                      <th>EDC ID</th>
                      <th>EDC URL</th>
                      <th>PHONE</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                        if(count($edcs)): 
                            $sn = 0;
                            foreach ($edcs as $edc):
                    ?>
                    <tr>
                        <td><?= ++$sn; ?></td>
                        <td><?= '<a href="'. site_url('super/edcs/save/'.$edc->edcid) .'" data-rel="collapse" title="Edit EDC">' . $edc->edcname . '</a>'; ?></td>
                        <td><?= $edc->edcid; ?></td>
                        <td><?= $edc->edcweburl; ?></td>
                        <td><?= $edc->edcphone; ?></td>
                        <td>
                            <?= '<a href="'. site_url('super/edcs/save/'.$edc->edcid) .'" data-rel="collapse" title="Edit EDC"><i class="glyphicon glyphicon-edit"></i></a> '; ?>
                            <?= get_del_btn(site_url('super/edcs/delete/'.$edc->edcid)); ?>
                            <?= '<a href="'. site_url('super/subjects/save/'.$edc->id) .'" data-rel="collapse" title="Setup EDC"><i class="glyphicon glyphicon-cog"></i></a> '; ?>
                        </td>
                    </tr> 
                    <?php endforeach;?>
                    <?php endif; ?>
                  </tbody>
                </table>
            </div>
            </div>
    </div>
</div>