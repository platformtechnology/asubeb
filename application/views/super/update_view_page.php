
<div class="row">

        <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-th-list"></i> LIST OF EDC UPDATES</div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <table class="table table-striped table-hover table-condensed">
                  <thead>
                    <tr>
                      <th>EDC</th>
                      <th>FILE</th>
                      <th>DOWNLOADS</th>
                      <th>INSTRUCTION</th>
                      <th>ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                        if(count($edcs)): 
                            $sn = 0;
                            foreach ($edcs as $data):
                    ?>
                    <tr>
                        <td><a href="<?= site_url('super/updates/save/'.$data->edcid)?>"><?= strtoupper($data->edcname);?></a></td>
                        <td>
                            <a href="<?= site_url('super/updates/view/'.$data->edcid)?>" title="Download"><i class="glyphicon glyphicon-download"></i> </a>
                            <?= $data->edcid.'.zip'; ?>
                        </td>
                        <td><?= $data->downloaded; ?></td>
                        <td><?= $data->updateinstructions; ?></td>
                        <td><?= 
                            get_edit_btn(site_url('super/updates/save/'.$data->edcid)) . ' ' .
                            get_del_btn(site_url('super/updates/delete/'.$data->edcid)) ?>
                        </td>
                    </tr> 
                    <?php endforeach;?>
                    <?php endif; ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>