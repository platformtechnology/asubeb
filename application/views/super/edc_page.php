<div class="row">
    <div class="col-lg-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-book"></i> EDC Setup <br/>
                    <a href="<?=  site_url('super/edcs/save');?>" style="color: #0088cc"><i class="glyphicon glyphicon-plus"></i> Add Exam Development Centre (EDC) </a>
                </div>
                <div class="panel-options">
                    <a href="<?=  site_url('super/edcs');?>" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                </div>
            </div>
           
            <div class="panel-body">
                <div class="row">
                    <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <?php 
                        if(count($edcs)){
                            echo parse_edcs_for_display($edcs);
                        }else{
                            echo '<br/><strong>NO EDC SETUP YET!</strong>';
                        }
                    ?>
            </div>
        </div>
    </div>
    </div>
</div>