
<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>

<br/>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> News 
                <br/>
                    <a href="<?=  site_url('super/news/save/'.$edcs->edcid);?>" style="color: #0088cc"><i class="glyphicon glyphicon-plus"></i> Add News </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                <div class="page-header">
                    <h3>All News</h3>
                </div>
              <div class="col-md-12">
                  <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <table class="table table-bordered table-condensed" id="example">
                      <thead>
                        <tr>
                          <th>Sn</th>
                          <th>Title</th>
                          <th>News</th>
                          <th>DateCreated</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php 
                            if(count($news)): 
                                $sn = 0;
                                foreach ($news as $new):
                            ?>
                            <tr>
                                <td><?= ++$sn;; ?></td>
                                <td><?php echo anchor(site_url('super/news/save/' . $edcs->edcid . '/' . $new->newsid), $new->newstitle) ?></td>
                                <td><?= $new->news; ?></td>
                                <td><?= $new->datecreated; ?></td>
                                <td>
                                    <?= get_edit_btn(site_url('super/news/save/' . $edcs->edcid . '/' . $new->newsid)); ?>
                                    <?= get_del_btn(site_url('super/news/delete/' . $edcs->edcid . '/' . $new->newsid)); ?>
                                </td>
                            </tr> 
                            <?php endforeach;?>
                            <?php endif; ?>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
