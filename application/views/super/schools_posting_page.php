
<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>

<br/>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> <strong>Setup Posting Recipients </strong></div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" name="regform" id="regform" role="form"'); ?>
                    <div class="col-md-8">
                                                
                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">School Name:</label>
                            <div class="col-sm-8">
                                <input type="text" name="schoolname" value="<?php echo set_value('schoolname', $school_detail->schoolname); ?>" class="form-control" id="inputEmail3" placeholder="Enter Secondary School Name" required="required">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">School Code:</label>
                            <div class="col-sm-8">
                                <input type="text" name="schoolcode" value="<?php echo set_value('schoolcode', $school_detail->schoolcode); ?>" class="form-control" id="inputEmail3" placeholder="Enter School Code" required="required">
                            </div>
                        </div>
                        
                        <div class="form-group">
                               <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                <a href="<?= site_url('super/schools_posting/save/'. $edcs->edcid);?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                              </div>
                         </div>
                    </div>
                        
                 <?= form_close(); ?>
                    </div>
                </div>
                
            <div class="row">
                <div class="page-header">
                            <h3>Posting Recipients (Esp. GPT)</h3>
                        </div>
                      <div class="col-md-12">
                            <table class="table table-bordered table-condensed" id="example">
                              <thead>
                                <tr>
                                  <th>Sn</th>
                                  <th>School Name</th>
                                  <th>Code</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>
                                   <?php 
                                   $sn = 0;
                                    if(count($schools)): 
                                        foreach ($schools as $school):
                                    ?>
                                    <tr>
                                        <td><?= ++$sn; ?></td>
                                        <td><?php echo anchor(site_url('super/schools_posting/save/' . $edcs->edcid . '/' . $school->schoolid), $school->schoolname) ?></td>
                                        <td><?= $school->schoolcode; ?></td>
                                       <td>
                                            <?= get_edit_btn(site_url('super/schools_posting/save/' . $edcs->edcid . '/' . $school->schoolid)); ?>
                                            <?= get_del_btn(site_url('super/schools_posting/delete/' . $edcs->edcid . '/' . $school->schoolid)); ?>
                                        </td>
                                    </tr> 
                                    <?php endforeach;?>
                                    <?php endif; ?>
                              </tbody>
                            </table>
                        </div>               
                         
              </div>
            </div>
        </div>
    </div>
</div>
