<div class="row">
    <div class="col-md-7 panel-info">
        <div class="content-box-header panel-heading">
            <div class="panel-title"><i class="glyphicon glyphicon-upload"></i> Upload Pin In CSV Format</div>
        </div>
        <div class="content-box-large box-with-header">
            <?php if ($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
            <?php if ($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
            <?php if (strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>

            <?= form_open_multipart(site_url('super/pinmanager/uploadpin'), 'class="form-horizontal" role="form"'); ?>
            <div class="row">
                <div class="col-lg-12">
                    <h4>PIN UPLOAD</h4>
                    <hr/>
                    <label for="inputEmail3" class="col-sm-offset-2 col-sm-8">
                        Note: Pins are uploaded per edc for each active year.
                        <br/>Pins Are Uploaded For The Active Year Only!
                    </label>
                    <br/> <br/><br/>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Choose EDC:</label>
                        <div class="col-sm-8">
                            <?php
                            if (count($edcs)) {
                                $select_options = array();
                                $select_options[''] = '--- Select Edc ---';

                                foreach ($edcs as $edc) {
                                    $select_options[$edc->edcid] = ucwords(strtolower($edc->edcname));
                                }
                                echo form_dropdown('edcid', $select_options, $this->input->post('edcid') ? $this->input->post('edcid') : '', 'class="form-control" required="required" style="font-weight: bold;"');
                            }
                            ?>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Select Year:</label>
                         <div class="col-sm-8">
                        <?php
                        $already_selected_value = $activeyear;
                        $start_year = $startyear;
                        print '<select name="examyear" class="form-control" required="required" readonly = "readonly">';
                        for ($x = $start_year; $x <= $activeyear; $x++) {
                            print '<option value="' . $x . '"' . ($x == $already_selected_value ? ' selected="selected"' : '') . '>' . $x . '</option>';
                        }
                        print '</select>';
                        ?>
                         </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Select File:</label>
                        <div class="col-sm-8">
                            <input type="file" name="pinfile" required="required" class="form-control"/><br/>
                             *Only plain text files (.txt) is allowed for upload
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-upload"></i> Upload</button>
                            <a href="<?= site_url('super/pinmanager'); ?>" class="btn btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </div>

            <?= form_close(); ?>

        </div>
    </div>
</div>
