<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>

<br/>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> Setup EDC Website Info</div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                        <div class="form-group">
                            <label for="inputEmail3" class="control-label">HOME INFO:</label>
                            <BR/><BR/>
                            <div>
                                <textarea class="ckeditor" name="homeinfo"><?php echo set_value('homeinfo', $info_detail->homeinfo); ?></textarea>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label for="inputEmail3" class="control-label">ABOUT INFO:</label>
                            <BR/><BR/>
                            <div>
                                <textarea class="ckeditor" name="aboutinfo"><?php echo set_value('aboutinfo', $info_detail->aboutinfo); ?></textarea>
                            </div>
                        </div>
                
                         <div class="form-group">
                               
                                <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                <a href="<?= site_url('super/edcinfo/save/'. $edcs->edcid);?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                              
                         </div>
                 <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
