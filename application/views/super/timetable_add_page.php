<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>
</div>

<br/>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Add Timetable Detail</div>
            </div>
            <div class="panel-body">

                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open_multipart('', 'class="form-horizontal" role="form"'); ?>

                        <div class="form-group">
                            <label for="inputEmail3" class="control-label">TITLE:</label>
                            <BR/><BR/>
                            <div class="col-md-6">
                                <input type="text" name="timetable_title" value="<?php echo set_value('timetable_title', $info_detail->timetable_title); ?>" class="form-control" placeholder="The Timetable Title" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="control-label">RAW TIMETABLE (IN TABULAR FORMAT):</label>
                            <BR/><BR/>
                            <div>
                                <textarea class="ckeditor" name="timetable_content"><?php echo set_value('timetable_content', $info_detail->timetable_content); ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="control-label">Upload Timetable File in PDF or Doc format</label>
                            <BR/><BR/>
                            <div>
                                <input type="file" name="timetable_file">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="control-label">Exam Year</label>
                            <BR/><BR/>
                            <div>
                                <?php
                                    $year_option = array($active_year=>$active_year);
                                    echo form_dropdown($name = 'examyear', $options = $year_option, $selected = set_value('examyear',$info_detail->examyear), $extra = 'class = "form-control"');


                                ?>
                            </div>
                        </div>

                         <div class="form-group">

                                <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                <a href="<?= site_url('super/news/index/'.$edcs->edcid);?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>

                         </div>
                 <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
