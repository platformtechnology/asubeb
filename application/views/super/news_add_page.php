<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>

<br/>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Add News Detail</div>
            </div>
            <div class="panel-body">
                
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                        
                        <div class="form-group">
                            <label for="inputEmail3" class="control-label">TITLE:</label>
                            <BR/><BR/>
                            <div class="col-md-6">
                                <input type="text" name="newstitle" value="<?php echo set_value('newstitle', $info_detail->newstitle); ?>" class="form-control" placeholder="The News Title" required="required">
                            </div>
                        </div>
                
                        <div class="form-group">
                            <label for="inputEmail3" class="control-label">NEWS:</label>
                            <BR/><BR/>
                            <div>
                                <textarea class="ckeditor" name="news"><?php echo set_value('news', $info_detail->news); ?></textarea>
                            </div>
                        </div>
                                       
                         <div class="form-group">
                               
                                <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                <a href="<?= site_url('super/news/index/'.$edcs->edcid);?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                              
                         </div>
                 <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
