
<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>
</div>

<br/>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> Exam Timetables
                <br/>
                    <a href="<?=  site_url('super/timetable/save/'.$edcs->edcid);?>" style="color: #0088cc"><i class="glyphicon glyphicon-plus"></i> Add Exam Timetable </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                <div class="page-header">
                    <h3>All Exam Timetables</h3>
                </div>
              <div class="col-md-12">
                  <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <table class="table table-bordered table-condensed" id="example">
                      <thead>
                        <tr>
                          <th>Sn</th>
                          <th>Title</th>
                          <th>Content</th>
                          <th>Exam Year</th>
                          <th>File</th>
                          <th>DateCreated</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php
                            if(count($timetables)):
                                $sn = 0;
                                foreach ($timetables as $new):
                            ?>
                            <tr>
                                <td><?= ++$sn;; ?></td>
                                <td><?php echo anchor(site_url('super/timetable/save/' . $edcs->edcid . '/' . $new->timetable_id), $new->timetable_title) ?></td>
                                <td><?= $new->timetable_content; ?></td>
                                <td><?= $new->examyear; ?></td>
                                <td><?php echo !empty($new->timetable_file)? '<a href = "'.base_url($new->timetable_file).'" target = "_blank"> View / Download </a>':'Nil'; ?></td>
                                <td>
                                    <?= get_edit_btn(site_url('super/timetable/save/' . $edcs->edcid . '/' . $new->timetable_id)); ?>
                                    <?= get_del_btn(site_url('super/timetable/delete/' . $edcs->edcid . '/' . $new->timetable_id)); ?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                            <?php endif; ?>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
