<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>
    
    <div class="col-lg-6">
         <div class="row">
            <div class="col-md-12">
               <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title"><i class="glyphicon glyphicon-search"></i> Search For Subjects</div>
                    </div>
                   <div class="panel-body">
                       <?= form_open(site_url('super/subjects/search/'.$edcs->id), 'class="form-inline" role="form"'); ?>
                             <div class="form-group col-sm-4">
                                    <?php 
                                      $select_options = array();
                                      foreach ($exams as $exam) {
                                          $select_options[$exam->examid] = $exam->examname;
                                      }
                                      $select_options[''] = "-- Exam --";
                                      echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : $examid, 'class="form-control" required');
                                    ?>
                            </div>
                            <div class="form-group col-sm-offset-2 col-lg-4">
                                 <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-search"></i> Search</button>
                              </div>   
                       <?= form_close(); ?>
                   </div>
                </div>
            </div>
        </div>
    </div>
    <a href="<?=  site_url('super/subjects/index/'.$edcs->id);?>"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
</div>

<br/>
    
<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> Setup Exam Subjects For The EDCs</div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <div class="col-md-12">
                    <table class="table table-bordered table-condensed" id="example">
                      <thead>
                        <tr>
                            <th>ExamType</th>
                          <th>Subject</th>
                          <th>HasPractical</th>
                          <th>IsCompulsory</th>
                          
                          <th>DateCreated</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php 
                            if(!empty($search_subjects) && count($search_subjects)>0): 
                                foreach ($search_subjects as $subjects):
                            ?>
                            <tr>
                                <td><?= $this->subjects_model->getExamName_From_Id($subjects->examid); ?></td>
                                <td><?php echo anchor(site_url('super/subjects/save/' . $subjects->id . '/' . $subjects->subjectid), $subjects->subjectname) ?></td>
                                <td><?= $subjects->haspractical == 1 ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'; ?></td>
                                <td><?= $subjects->iscompulsory == 1 ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'; ?></td>
                                
                                <td><?= $subjects->datecreated; ?></td>
                                <td>
                                    <?= get_edit_btn(site_url('super/subjects/save/' . $subjects->id . '/' . $subjects->subjectid)); ?>
                                    <?= get_del_btn(site_url('super/subjects/delete/'. $subjects->id . '/' . $subjects->subjectid)); ?>
                                </td>
                            </tr> 
                            <?php endforeach;?>
                            <?php endif; ?>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
