
<div class="row">
    <div class="col-md-10">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-arrow-up"></i> EDC OFFLINE UPDATES</div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open_multipart('', 'class="form-horizontal" role="form"'); ?>
                <div class="row">    
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-pull-2 col-sm-3 control-label">EDC:</label>
                            <div class="col-md-pull-2 col-sm-9">
                                <?php 
                                  $select_options = array();
                                  $select_options[''] = '------ Select EDC ------ ';
                                  foreach ($edcs as $edc) {
                                      $select_options[$edc->edcid] = strtoupper($edc->edcname);
                                  }
                                  echo form_dropdown('edcid', $select_options, $this->input->post('edcid') ? $this->input->post('edcid') : $edc_detail->edcid, 'class="form-control" required="required"');
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-pull-2 col-sm-3 control-label">FILE:</label>
                            <div class="col-md-pull-2 col-sm-9">
                                <input type="file" name="updatefile" class="form-control" />
                                <br/>
                                <strong style="color: crimson;">Only Zipped Archives (.zip) are allowed for upload.</strong>
                            </div>
                        </div>
                    </div>
                   </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="control-label">INSTRUCTIONS:</label>
                            
                            <div>
                                <textarea class="ckeditor" name="updateinstructions"><?php echo set_value('updateinstructions', $edc_detail->updateinstructions); ?></textarea>
                            </div>
                        </div>

                
                         <div class="form-group">
                               
                                <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                <a href="<?= site_url('super/updates/save');?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                              
                         </div>
                 <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
