<div class="row">
    <div class="col-md-6">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title">
                   <i class="glyphicon glyphicon-lock"></i> Change Password
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td>S/N</td>
                        <td>Email</td>
                        <td>Fullname</td>
                        <td>Privilege</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($users)):
                            $sn =1;
                           foreach($users as $user):
                    ?>
                    <tr>
                        <td><?php echo $sn?></td>
                        <td><?php echo $user->email ?></td>
                        <td><?php echo $user->fullname ?></td>
                        <td><?php echo $user->privilege == '' ? 'EDC Admin' : $user->privilege;  ?></td>
                    </tr>
                    <?php
                        $sn++;
                        endforeach;
                        endif;
                    ?>
                    </tbody>
                </table>

             </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title">
                   <i class="glyphicon glyphicon-user"></i> Add New User
                </div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php  echo $error_message ?>
                <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label"> Email</label>
                        <div class="col-sm-8">
                            <input type="text" name="email" class="form-control" placeholder="email" required="required" value="<?php echo set_value('username')?>" />
                        </div>
                      </div>
                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label"> Password</label>
                        <div class="col-sm-8">
                            <input type="password" name="password" class="form-control" placeholder="Password" required="required" />
                        </div>
                      </div>
                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label"> Fullname</label>
                        <div class="col-sm-8">
                            <input type="text" name="fullname" class="form-control" placeholder="Fullname" required="required" />
                        </div>
                      </div>
                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label"> Phone Number</label>
                        <div class="col-sm-8">
                            <input type="text" name="phone" class="form-control" placeholder="Phone Number" required="required" />
                        </div>
                      </div>
                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label"> Privilege</label>
                        <div class="col-sm-8">
                            <?php
                                $privileges = array('SuperAdmin'=>'SuperAdmin','Support'=>'Support');

                                echo form_dropdown('privilege',$privileges,'Support',$extra = 'class="form-control"');
                            ?>
                        </div>
                      </div>


                    <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok"></i> Change</button>
                      </div>
                    </div>
                <?php echo form_close(); ?>
             </div>
        </div>
    </div>
</div>