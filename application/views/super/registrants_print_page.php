<!DOCTYPE html>
<html>
  <head>
    <title><?= config_item('site_name') . ' - ' . $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
    
    <?= $page_level_styles; ?>
    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script lang="javascript" type="text/javascript">
            function printn() {
                    window.print();
                    var target = "<?= site_url('super/registrants/filter/'.$edcs->edcid); ?>";
                    window.location = target;
               }
    </script>
  </head>
  <body onload="printn()">
<div class="row" id="printdiv">
    <div class="col-md-12">
        <div class="content-box-large">
             <div class="panel-heading">
                 
                    <?= get_edc_logo($edclogo, $edcs->edcname); ?>
                   <br/> <br/> <br/>
                    <i class="glyphicon glyphicon-user"></i> Candidate Detail
               
            </div>
            <div class="panel-body">
               
                <table class="table table-bordered" width="100%">
                <tr>
                  <td><strong>Registered Exam:</strong></td>
                  <td><strong><?= $exam_info->examname; ?></strong></td>
                  <td rowspan="4" valign="top">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 150px; height: 112px;"><img src="<?= $passport; ?>" alt="Passport" /></div>
                             <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                          </div>              
                    </td>
                </tr>
                <tr>
                  <td><strong>Exam Year:</strong></td>
                  <td><strong><?= $reg_info->examyear; ?></strong></td>
                </tr> 
                <tr>
                  <td>&nbsp;</td>
                 <td>&nbsp;</td>
                </tr> 
                 <tr>
                    <td><strong>Local Govt Area:</strong></td>
                    <td><?= $this->registrant_model->get_lga($reg_info->lgaid); ?> </td>
                  </tr>
                  <tr>
                   <td><strong>School:</strong></td>
                    <td><?= $this->registrant_model->get_school($reg_info->schoolid); ?> </td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Exam Centre:</strong></td>
                    <td><?= $this->registrant_model->get_school($reg_info->centreid); ?> </td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td valign="top">
                        <strong>Exam No:  <span style="color:crimson;"><?= $reg_info->examno; ?></span></strong>
                    </td>
                    <td>&nbsp;</td>
                  </tr>
                   <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                     <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Firstname:</strong></td>
                    <td><?= $reg_info->firstname; ?></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Othernames:</strong></td>
                    <td><?= $reg_info->othernames; ?></td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Gender:</strong></td>
                    <td><?= ($reg_info->gender == 'M' ? 'Male' : 'Female') ;?></td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Dob:</strong></td>
                    <td><?= $reg_info->dob; ?></td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Phone:</strong></td>
                    <td><?= $reg_info->phone; ?></td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                   <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                  </tr>

                  <tr>
                  <td><strong>Subjects:</strong></td>
                  <td>
                        <?php 
                          if(count($subjects)):
                                $sn = 0;
                                foreach ($subjects as $subject) {
                                    echo ++$sn .'. <strong>'.$this->registration_model->get_subject($subject->subjectid);
                                        if($subject->armid != '0'){
                                            $armdata = $this->db->get_where('t_subjects_arms', array('armid' => $subject->armid))->row();
                                            echo ' (' . ucwords(strtolower($armdata->armname)) . ') ';
                                        }
                                    echo ' </strong><br/> ';
                                }
                            endif;
                         ?>
                    </td>
                    <td valign="top">&nbsp;</td>
                  </tr>

                  <tr>
                   <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign="top">&nbsp;  </td>
                  </tr>
                  <?php 
                    if($exam_info->hasposting == 1):
                  ?>
                  <tr>
                    <td><strong>First Choice:</strong></td>
                    <td><?= $this->registrant_model->get_school($reg_info->firstchoice); ?> </td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><strong>Second Choice:</strong></td>
                    <td><?= $this->registrant_model->get_school($reg_info->secondchoice); ?> </td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <?php
                    endif;
                  ?>
                  
                </table>
        
            </div>
        </div>
    </div>
</div>
   </body>
</html>
