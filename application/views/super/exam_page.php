<div class="row">
    <?php $this->load->view('super/template/layout_edc_menu'); ?>  
</div>

<br/>

<div class="row">
    <div class="col-md-5">
        
            <div class="content-box-header">
                <div class="panel-title">
                    <?php echo empty($exam_detail->examname) ? '<i class="glyphicon glyphicon-plus"></i> Add Exam' : '<i class="glyphicon glyphicon-edit"></i> Edit Exam'; ?>
                </div>
            </div>
             <div class="content-box-large box-with-header">
                 <div class="panel-body">
                    <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                    <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                        <br />
                        <div class="col-md-12">
                        <input type="hidden" name="edcid" value="<?= $edcs->edcid?>" >
                        <div class="row">
                             <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-3 control-label">Exam Name:</label>
                                 <div class="col-sm-9">
                                     <input type="text" name="examname" value="<?php echo set_value('examname', $exam_detail->examname); ?>" class="form-control" id="inputEmail3" placeholder="E.g BECE" required="required">
                                 </div>
                             </div>
                            
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Desc.:</label>
                                <div class="col-sm-9">
                                    <textarea name="examdesc" class="form-control" placeholder="Decription" rows="2" required="required"><?php echo set_value('examdesc', $exam_detail->examdesc); ?></textarea>
                                </div>
                            </div>
                         </div>
                        
                         <div class="row">
                            <div class="col-sm-6">
                             <div class="form-group">
                                      <div class="col-sm-offset-2 col-sm-10">
                                           <div class="checkbox">
                                              <label>
                                                  <input type="checkbox" name="hasposting" value="1" <?php echo ($exam_detail->hasposting == 1 ? 'checked="true"' : '')?>> <strong>Has Posting</strong>
                                              </label>
                                            </div>
                                      </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-offset-2 col-sm-10">
                                      <div class="checkbox">
                                         <label>
                                             <input type="checkbox" name="hassecschool" value="1" <?php echo ($exam_detail->hassecschool == 1 ? 'checked="true"' : '')?>> <strong>For Secondary Schools</strong>
                                         </label>
                                       </div>
                                 </div>
                             </div>

                                <div class="form-group">
                                     <div class="col-sm-offset-2 col-sm-10">
                                          <div class="checkbox">
                                             <label>
                                                 <input type="checkbox" name="haszone" value="1" <?php echo ($exam_detail->haszone == 1 ? 'checked="true"' : '')?>> <strong>Use Only Zone</strong><br/>
                                             </label>
                                           </div>
                                     </div>
                                 </div>
                            </div>

                            <div class="col-sm-6">
                                 <div class="form-group">
                                 <div class="col-sm-12">
                                      <div class="checkbox">
                                         <label>
                                             <input type="checkbox" name="hasca" value="1" <?php echo ($exam_detail->hasca == 1 ? 'checked="true"' : '')?>> <strong>Allow CA Score Entry</strong>
                                         </label>
                                       </div>
                                 </div>
                             </div>

                             <div class="form-group">
                                 <div class=" col-sm-12">
                                      <div class="checkbox">
                                         <label>
                                             <input type="checkbox" name="haspractical" value="1" <?php echo ($exam_detail->haspractical == 1 ? 'checked="true"' : '')?>> <strong>Allow Practical Score</strong>
                                         </label>
                                       </div>
                                 </div>
                             </div>
                            </div>
                         </div>
                            
                        <br/>
                       <div class="row">
                            <div class="form-group">
                                    <div class="col-sm-12">
                                        <strong>Note: If "Has Posting" is selected, then REMARK SETTINGS would be disabled for the exam and CUTOFF MARK will be used for Analysis Instead!</strong>
                                    </div>
                                </div>

                            

                            <br/>
                            <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                <a href="<?= site_url('super/exams/save');?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                              </div>
                            </div>
                        </div>
                        
                        
                    </div>

                    <?= form_close(); ?>
                   </div>
                </div>  
        
        </div>
    
    <div class="col-md-7">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title">All Exams</div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('updated_msg')) echo get_success($this->session->flashdata('updated_msg')); ?>
                <table class="table table-bordered table-condensed" id="example">
                  <thead>
                    <tr>
                      <th>Exam Name</th>
                      <th>HasPosting</th>
                      <th>ForSecondary</th>
                      <th>HasCA</th>
                      <th>HasPractical</th>
                      <th>OnlyZone</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                        if(count($exams)): 
                            foreach ($exams as $exam):
                    ?>
                    <tr>
                        <td><?php echo anchor(site_url('super/exams/save/'.$edcs->edcid.'/'.$exam->examid), strtoupper($exam->examname)) ?></td>
                        <td><?= ($exam->hasposting == 1 ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'); ?></td>
                        <td><?= ($exam->hassecschool == 1 ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'); ?></td>
                        <td><?= ($exam->hasca == 1 ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'); ?></td>
                        <td><?= ($exam->haspractical == 1 ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'); ?></td>
                        <td><?= ($exam->haszone == 1 ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'); ?></td>
                        <td>
                            <?= get_edit_btn(site_url('super/exams/save/'.$edcs->edcid.'/'.$exam->examid)); ?>
                            <?= get_del_btn(site_url('super/exams/delete/'.$exam->examid.'/'.$edcs->edcid)); ?>
                        </td>
                    </tr> 
                    <?php endforeach;?>
                    <?php endif; ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>
