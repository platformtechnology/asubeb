<div class="row">

    <div class="col-md-12">
        <div class ="col-md-6">
            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title">
                        <i class="glyphicon glyphicon-remove-sign"></i> Registration Errors
                    </div>
                </div>
                <div class="panel-body">

                    <?php echo $error_message ?>
                    <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label"> Enter Candidates Pin Number</label>
                        <div class="col-sm-8">
                            <input type="text" name="pin" class="form-control" placeholder="Candidate Pin" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-arrow-right"></i> Continue</button>
                        </div>
                    </div>


                </div>
            </div>
            <?php echo form_close(); ?>
        </div>

        <div class ="col-md-6">
            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title">
                        <i class="glyphicon glyphicon-search"></i> Candidate Record
                    </div>
                </div>
                <div class="panel-body">
                     <?php if ($this->session->flashdata('success')) echo get_success($this->session->flashdata('success')); ?>
                    <?php if ($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?php if (@count($candidate_record)):?>
                    <table class = "table table-bordered">
                        <tr>
                            <td>
                                Candidate Name:
                            </td>
                            <td>
                                <?php echo $candidate_record->firstname . ' '.$candidate_record->othernames; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               Gender:
                            </td>
                            <td>
                                <?php echo $candidate_record->gender; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               Exam No:
                            </td>
                            <td>
                               <?php echo $candidate_record->examno; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               Exam Year:
                            </td>
                            <td>
                               <?php echo $candidate_record->examyear; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                School Registered:
                            </td>
                            <td>
                                <?php echo $candidate_record->schoolname; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Pin Number:
                            </td>
                            <td>
                                <?php echo $candidate_record->pin; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Form No:
                            </td>
                            <td>
                                <?php echo $candidate_record->formnumber; ?>
                            </td>
                        </tr>
                    </table>
                    <a href ="<?php echo site_url('super/registration/reset/'.$candidate_record->candidateid.'/'.$candidate_record->pin.'/'.$candidate_record->formnumber)?>" onclick="return(confirm('This is delete this candidate\'s entire registration record\n Do you want to proceed?'))"><button class="btn btn-danger"> <i class="glyphicon glyphicon-remove"></i> Reset Candidate </button> </a>

                    <?php
                    endif;
                    ?>
                </div>
            </div>
            
        </div>
    </div>
</div>
</div>