
<div class="row">
    <div class="col-md-6 panel-primary">
        
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                   <i class="glyphicon glyphicon-lock"></i> Change Password
                </div>
            </div>
            <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Current Password:</label>
                        <div class="col-sm-8">
                            <input type="password" name="oldpassword" class="form-control" placeholder="Current Password" required="required" />
                        </div>
                      </div>
     
                    <div class="form-group">
                      <label class="col-sm-4 control-label">New Password:</label>
                      <div class="col-sm-8">
                        <input type="password" name="newpassword" class="form-control" placeholder="New Password" required="required" />
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Confirm:</label>
                      <div class="col-sm-8">
                        <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm Password" required="required" />
                      </div>
                    </div>
                
                    <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok"></i> Change</button>
                      </div>
                    </div>
                <?php echo form_close(); ?>
             </div>
        
    </div>
</div>