
<h4 style="text-align: center; color: #0077b3; font-weight: bold">
    <span class="glyphicon glyphicon-edit"></span>
    MANAGE WEBSITE CONTENT
</h4>
<hr/>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-12 panel-primary">
                            <div class="content-box-header panel-heading">
                                <div class="panel-title">
                                    <strong><i class="glyphicon glyphicon-list"></i> POST NEWS</strong>
                                </div>
                            </div>
                            <div class="content-box-large box-with-header">
                                <div class="row">
                                    <?php if ($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                                    <?php if ($this->session->flashdata('success')) echo get_success($this->session->flashdata('success')); ?>
                                    <?php if (strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                                    <?php echo form_open($action = ''); ?>
                                    <div class="col-md-12" style="margin-bottom:2em">
                                        <div class="form-group" style="padding-bottom: 2em">
                                            <label for="inputEmail3" class="col-sm-4 control-label"> News Title</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="newstitle" required="" value="<?php echo set_value('newstitle',$news->newstitle); ?>"/>
                                            </div>
                                        </div>
                                        <div class="form-group" style="padding-bottom: 2em">
                                            <label for="inputEmail3" class="col-sm-4 control-label"> News Content</label>
                                            <div class="col-sm-8">
                                                <textarea name="news" required="" class="form-control"><?php echo set_value('news',$news->news); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12"  align = "right">
                                        <div class="col-md-6" align = "right">
                                            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-send"></i> Post News</button>
                                        </div>
                                        <div class="col-md-6" align = "left">
                                            <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Clear</button>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>



                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12 panel-primary">
                            <div class="content-box-header panel-heading">
                                <div class="panel-title">
                                    <strong><i class="glyphicon glyphicon-list"></i> EXISITING NEWS</strong>
                                </div>
                            </div>
                            <div class="content-box-large box-with-header">
                                <div class="row">
                                <div class="col-md-12">
                                        <?php if (isset($existing_news) && !empty($existing_news)) { ?>
                                            <table class="table table-bordered table-hover table-responsive">
                                                <thead>
                                                <th>S/N</th>
                                                <th>Title</th>
                                                <th>News</th>
                                                <th>Date Posted</th>
                                                <th>Action</th>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $sn = 1;
                                                    foreach ($existing_news as $news):
                                                        ?>
                                                        <tr>
                                                            <td> <?php echo $sn; ?></td>
                                                            <td> <?php echo $news->newstitle; ?></td>
                                                            <td> <?php echo substr($news->news,0,30); ?>....</td>
                                                            <td> <?php echo substr($news->datecreated,0,10); ?></td>
                                                            <td>
                                                                <?php
                                                                    echo get_edit_btn('admin/website/index/'.$news->newsid);
                                                                    echo get_del_btn('admin/website/index');
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $sn++;
                                                    endforeach;
                                                    ?>
                                                </tbody>

                                                </thead>
                                            </table>
                                            <?php
                                        }
                                        else {
                                            get_error($msg = "No Records Found");
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
