 <h4 style="text-align: center; color: #0077b3; font-weight: bold">
    EDIT CANDIDATES SCHOOL OF CHOICE FOR <br/>THE <?= strtoupper($exam_detail->examdesc. ' ('.$exam_detail->examname.') ' . $exam_year) ;?>
</h4>
<hr/>

<div class="row">
    <div class="col-md-12 panel-primary">

            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-edit"></i> <strong>Edit School of Choice</strong>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                <?= form_open('', 'class="form-horizontal" name="regform" id="regform" role="form"'); ?>
                <div class="row">
                    <div class="col-md-10">

                    <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>

<!--                        <div class="col-md-2"></div>-->
                        <div class="col-md-12">
                            <div class="col-md-5">
                              <div class="form-group">
                               <label for="inputEmail3" class="col-sm-3 control-label">LGA:</label>
                                <div class="col-sm-9">
                                    <?php
                                        $select_options = array();
                                        $select_options[''] = "----- Select LGA-----";

                                          foreach ($lgas as $lg) {
                                              $select_options[$lg->lgaid] = $lg->lganame;
                                          }

                                        echo form_dropdown('lgaid', $select_options, ($this->input->post('lgaid') ? $this->input->post('lgaid') : ($this->session->flashdata('donelga') ? $this->session->flashdata('donelga') : $candidate_detail->lgaid)), 'id="lgaid" class="form-control" ');
                                    ?>
                                </div>
                               </div>
                             </div>

                            <div class="col-md-7">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">School:</label>
                                <div class="col-sm-8">
                                    <?php
                                        $select_options = array();
                                        $select_options[''] = "----- Select A School -----";
                                        foreach ($schools as $school) {
                                            $select_options[$school->schoolid] = ucwords(strtolower($school->schoolname)) .' - '. $school->schoolcode;
                                        }
                                        echo form_dropdown('schoolid', $select_options, ($this->input->post('schoolid') ? $this->input->post('schoolid') : ($this->session->flashdata('done') ? $this->session->flashdata('done') : $candidate_detail->schoolid)), 'id="schoolid" class="form-control" onchange="displayCandidates()"');
                                    ?>

                                </div>
                            </div>
                            </div>
                        </div>

                    </div>

                </div>
            <div class="row">
              <div class="col-md-12">
                  <br/>
                  <div id="display_res"></div>


                </div>
              </div>

              <?= form_close(); ?>
            </div>

    </div>
</div>

<script type="text/javascript">

    (function() {
        var httpRequest;

        lgaddl = document.getElementById("lgaid");
        lgaddl.onchange = function() {
            var target_url1 = "<?= site_url('admin/registration/school_ajaxdrop?issec='.(($exam_detail->hassecschool == 1) ? '1' : '0').'&lgaid='); ?>";
            //target_url1 = target_url1.replace(/\.[^/.]+$/, ""); //remove .html extension
            makeRequest( target_url1 + lgaddl.value, 'schoolid');

        };

        function makeRequest(url, targetid) {
            if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                httpRequest = new XMLHttpRequest();
            } else if (window.ActiveXObject) { // IE
                try {
                    httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch (e) {
                    try {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    catch (e) {}
                }
            }

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = function(){
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        var data = JSON.parse(httpRequest.response);
                        var select = document.getElementById(targetid);
                        if(emptySelect(select)){
                            var el = document.createElement("option");
                                    el.textContent = '....Select....';
                                    el.value = '';
                                    select.appendChild(el);

                            for (var i = 0; i < data.lgas.length; i++){
                                var el = document.createElement("option");
                                    el.textContent = data.lgas[i].lganame;
                                    el.value = data.lgas[i].lgaid;
                                    select.appendChild(el);
                            }
                        }
                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            httpRequest.send();
        }

        function emptySelect(select_object){
            var i;
            for(i=select_object.options.length-1;i>=0;i--){
                select_object.remove(i);
            }
            return true;
        }
    })();

    function displayCandidates(sn){
        schoolid = document.getElementById("schoolid").value;

        if(schoolid.trim() == ""){
            alert("YOU MUST SELECT A SCHOOL");
            document.getElementById("display_res").innerHTML = '';
            return;
        }

        var url_to_post = "<?= site_url('admin/registration/fetchChoiceCandidates?schoolid='); ?>";
        url_to_post = url_to_post + schoolid + "&examid=" + "<?= $exam_detail->examid; ?>" + "&examyear=" + "<?= $exam_year ?>";
        document.getElementById("display_res").innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
        makeRequestx(url_to_post, "display_res");
    }

    function submitCandidates(){
        schoolid = document.getElementById("schoolid").value;

        if(schoolid.trim() == ""){
            alert("YOU MUST SELECT A SCHOOL");
            document.getElementById("display_res").innerHTML = '';
            return;
        }

        var url_to_post = "<?= site_url('admin/registration/fetchCandidates?schoolid='); ?>";
        url_to_post = url_to_post + schoolid + "&examid=" + "<?= $exam_detail->examid; ?>" + "&examyear=" + "<?= $exam_year ?>";
        document.getElementById("display_res").innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
        makeRequestx(url_to_post, "display_res");
    }

    function examPost(sn){
       exam_no = document.getElementById("examno"+sn).value;
       candidateid = document.getElementById("candidateid"+sn).value;
       var url_to_post = "<?= site_url('admin/registration/update_examno?exam_no='); ?>";
       url_to_post = url_to_post + exam_no + "&candidateid="+candidateid;
       document.getElementById("exam-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
       makeUpdateRequest(url_to_post, "exam-result"+sn);

    }

    function firstnamePost(sn){
       firstname = document.getElementById("firstname"+sn).value;
       candidateid = document.getElementById("candidateid"+sn).value;
       var url_to_post = "<?= site_url('admin/registration/update_firstname?firstname='); ?>";
       url_to_post = url_to_post + firstname + "&candidateid="+candidateid;
       document.getElementById("fname-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
       makeUpdateRequest(url_to_post, "fname-result"+sn);

    }

    function othernamePost(sn){
       othername = document.getElementById("othername"+sn).value;
       candidateid = document.getElementById("candidateid"+sn).value;
       var url_to_post = "<?= site_url('admin/registration/update_othernames?othernames='); ?>";
       url_to_post = url_to_post + othername + "&candidateid="+candidateid;
       document.getElementById("oname-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
       makeUpdateRequest(url_to_post, "oname-result"+sn);

    }

    function phonePost(sn){
       phone = document.getElementById("phone"+sn).value;
       candidateid = document.getElementById("candidateid"+sn).value;
       var url_to_post = "<?= site_url('admin/registration/update_phone?phone='); ?>";
       url_to_post = url_to_post + phone + "&candidateid="+candidateid;
       document.getElementById("phone-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
       makeUpdateRequest(url_to_post, "phone-result"+sn);

    }

   function makeUpdateRequest(url, imgId) {

        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }

         httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {

                    var obj = JSON.parse(httpRequest.response);
                    if(obj.success == 1){
                        document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('available.png'); ?>"+'" />';
                    }else{
                        alert('Could Not Update');
                        document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                    }
                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        httpRequest.send();
    }
//

    function makeRequestx(url, displayId) {

        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }

         httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                   document.getElementById(displayId).innerHTML = httpRequest.response;
                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById(displayId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        httpRequest.send();
    }
//
    function getHttpObject(){
        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
            httpRequest = new XMLHttpRequest();
            return httpRequest;
        } else if (window.ActiveXObject) { // IE
            try {
                httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                return httpRequest;
            }
            catch (e) {
                try {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    return httpRequest;
                }
                catch (e) {}
            }
        }

        return;
    }
    
    
function updateFirstChoice(index){

                firstChoice = document.getElementById("firstChoice"+index).value;
                candidateid = document.getElementById("candidateid"+index).value;
                $.ajax({url: "<?php echo site_url('admin/registration/updateFirstChoice') ?>/" + firstChoice + "/" + candidateid,
                            success: function (result_data) {
                            $('#firstChoiceResult'+index).html(result_data);
                    }
                });
}
function updateSecondChoice(index){

                secondChoice = document.getElementById("secondChoice"+index).value;
                candidateid = document.getElementById("candidateid"+index).value;
                $.ajax({url: "<?php echo site_url('admin/registration/updateSecondChoice') ?>/" + secondChoice + "/" + candidateid,
                            success: function (result_data) {
                            $('#secondChoiceResult'+index).html(result_data);
                    }
                });
}
function updateThirdChoice(index){

                thirdChoice = document.getElementById("thirdChoice"+index).value;
                candidateid = document.getElementById("candidateid"+index).value;
                $.ajax({url: "<?php echo site_url('admin/registration/updateThirdChoice') ?>/" + thirdChoice + "/" + candidateid,
                            success: function (result_data) {
                            $('#thirdChoiceResult'+index).html(result_data);
                    }
                });
}
</script>
