 <h4 style="text-align: center; color: #0077b3; font-weight: bold">
    RESET REGISTRATION ERRORS
</h4>
<hr/>

<div class="row">
    <div class="col-md-12 panel-primary">

            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-edit"></i> <strong>Register Students</strong>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                <div class="row">
                    <div class="col-md-10">

                    <?php if($this->session->flashdata('success')) echo get_success($this->session->flashdata('success')); ?>
                    <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>

<!--                        <div class="col-md-2"></div>-->
                        <div class="col-md-12">
                            <?php echo form_open('');?>
                            <div class="col-md-8">
                                <div class="col-md-9">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">School:</label>
                                <div class="col-sm-8">
                                    <?php
                                        $select_options = array();
                                        $select_options[''] = "----- Select A School -----";
                                        foreach ($schools as $school) {
                                            $select_options[$school->schoolid] = ucwords(strtolower($school->schoolname)) .' - '. $school->schoolcode;
                                        }
                                        echo form_dropdown('schoolid', $select_options,set_value('schoolid'), 'id="schoolid" class="form-control" required = "" onchange="displayCandidates()"');
                                    ?>
                                </div>
                            </div>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-primary" type="submit"> <span class="glyphicon glyphicon-chevron-right"></span> Proceed </button>
                                </div>
                            </div>
                               <?php echo form_close(); ?>
                        </div>

                    </div>

                </div>

<?php
if (isset($candidates) && count($candidates)) { ?>
            <div class="row">
                <div class="page-header">
                    <h3><i class="glyphicon glyphicon-edit"></i> Candidates Registration Details </h3>
                </div>
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <h2> School Name : <?php echo $schoolinfo->schoolname; ?> </h2>
                    <h4> School Code : <?php echo $schoolinfo->schoolcode; ?> </h4>
                    <h4> Exam : <?php echo $exam_detail->examname; ?> </h4>
                    <h4> Exam Year : <?php echo $exam_year; ?> </h4>
                    <h4> Total Candidates Registered : <?php echo count($candidates) ?> </h4>
                    <?php
                    $schoolid = $schoolinfo->schoolid;
                    $examid = $exam_detail->examid;
                    ?>
                    Did you make a Mistake? : <a onclick="return(confirm('Are you sure you want to proceed \n This action cannot be undone o!'))"href="<?php echo site_url('admin/registration/clear_registration_errors/'.$schoolid.'/'.$examid.'/'.$exam_year)?>"> <button class="btn btn-danger"> <span class="glyphicon glyphicon-remove"></span> Clear Registered Candidates in this School</button> </a>
                    <br/>
                  </div>
              <div class="col-md-12">

                  <table class="table table-bordered table-condensed" style="font-size: 12px">
                      <thead>
                        <tr>
                          <th>S/N</th>
                          <th>Surname</th>
                          <th>Othernames</th>
                          <th>Gender</th>
                          <th>DOB</th>
                          <th>Phone</th>
                          <th></th>
                        </tr>
                      </thead>
                     <?php
                        $sn = 1;
                        foreach ($candidates as $candidate): ?>
                        <tr>
                            <td> <?php echo $sn ?></td>
                            <td> <?php echo $candidate->firstname?></td>
                            <td> <?php echo $candidate->othernames?></td>
                            <td> <?php echo $candidate->gender?></td>
                            <td> <?php echo $candidate->dob?></td>
                            <td> <?php echo $candidate->phone?></td>
                            <td></td>
                        </tr>
                      <?php
                        $sn++;
                        endforeach; ?>
                      <tbody>
                      </tbody>
                    </table>
                  <?php }
                  else if (isset($candidates) && !count($candidates)) {
                  ?>
                  <div class="alert alert-warning"> No Candidates Registered for this Exam in the Selected School </div>
                  <?php
                  }
                  ?>
                  <br/>

                  <div id="display_res"></div>


                </div>
              </div>


            </div>

    </div>
</div>
