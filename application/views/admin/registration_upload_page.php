
<div class="row">
    <div class="col-md-6 panel-primary">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-upload"></i> UPLOAD CANDIDATES REGISTRATION DETAILS</strong>
                </div>
            </div>
        
             <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open_multipart('', 'class="form-horizontal" role="form"'); ?>
                        <div class="row">

                             <div class="col-md-12">
                                 <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 control-label">EXAM:</label>
                                          <div class="col-sm-9">
                                              <?php 
                                                $select_options = array();
                                                  foreach ($exams as $exam) {
                                                      $select_options[$exam->examid] = $exam->examname;
                                                  }
                                                $select_options[''] = "------- Select Exam -------";
                                                echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'id="examid" class="form-control" required="required"');
                                              ?>
                                          </div>
                                 </div>
                                 
                                 <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 control-label">YEAR:</label>
                                          <div class="col-sm-9">
                                              <?php 
                                                  $already_selected_value = ($this->input->post('examyear') ? $this->input->post('examyear') : $activeyear);
                                                  $start_year = $activeyear;
                                                   print '<select name="examyear" id="examyear" class="form-control" required="required">';
                                                   //print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>..::All Year::..</option>';
                                                   for ($x = $start_year; $x <= $activeyear; $x++) {
                                                       print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                                                   }
                                                   print '</select>';
                                                 ?>
                                          </div>
                                  </div>
                                 
                                 <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label">SCHOOL:</label>
                                      <div class="col-sm-9">
                                          <div class="bfh-selectbox" data-name="schoolid" data-value="<?= $this->input->post('schoolid') ? $this->input->post('schoolid') : '' ?>" data-filter="true">
                                            <?php 

                                             echo '<div data-value="">------ Select School ------</div>';
                                              foreach ($schools as $school) {
                                                echo '<div data-value="'.$school->schoolid.'">'. $school->schoolcode . ' - ' .strtoupper($school->schoolname). '</div>';
                                             }
                                           ?>
                                           </div>
                                      </div>
                                  </div>
                                 
                                  <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">EXCEL FILE</label>
                                      <div class="col-sm-9">
                                          <input type="file" name="regfile" class="form-control" required="required"/><br/>
                                          <strong>Only xls or xlsx file types are allowed for upload.</strong>
                                      </div>
                                    </div>
                                
                                  <div class="form-group">
                                      <div class="col-sm-12">
                                        <strong style="color: crimson;">
                                            When uploading candidate registration data, the system registers the student for all the subjects 
                                            of that exam and assumes default values for details such as GENDER, DOB and PHONE. 
                                            If the Exam being registered for is an Exam that requires Posting, then the system assigns 
                                            random schools as first and second choice.
                                        </strong>
                                      </div>
                                    </div>
                                 
                                    <div class="form-group">
                                      <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-upload"></i> Upload</button>
                                        <a href="" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-refresh"></i> Refresh</a>
                                      </div>
                                    </div>
                                 
                             </div>
                         
                         </div>
                       
                    <?= form_close(); ?>                
              </div>

    </div>
    
 <?php if($this->session->flashdata('msg')) { ?>   
    <div class="col-md-6">
        <div class="panel-title">
            <strong>REGISTRANT PROCESSING SUMMARY</strong>
        </div>
        <div class="content-box-large">
            <div class="panel-body">
                <table class="table table-bordered">
                    <tr>
                        <td>Total Successful</td>
                        <td><?= $this->session->flashdata('total'); ?></td>
                        <td> ---- </td>
                    </tr>
                   
                </table>
            </div>
        </div>
    </div>
 <?php } ?>
 </div>
