
<div class="row">
    <div class="col-md-5 panel-primary">
        
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><?php echo empty($deadline_detail->id) ? '<i class="glyphicon glyphicon-plus"></i> Add Exam Registration Deadline' : '<i class="glyphicon glyphicon-edit"></i> Edit Registration Deadline'; ?></strong>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                    <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Exam Type:</label>
                                <div class="col-sm-8">
                                    <?php 
                                      $select_options = array();
                                      foreach ($exams as $exam) {
                                          $select_options[$exam->examid] = $exam->examname;
                                      }
                                      $select_options[''] = "..::Select Exam Type::..";
                                      echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : $deadline_detail->examid, 'class="form-control" required="required"');
                                    ?>
                                </div>
                            </div>
                
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Deadline Date:</label>
                                <div class="col-sm-8">
                                    <div class="bfh-datepicker" data-format="d-m-y" data-date="<?php echo set_value('closedate', $deadline_detail->closedate); ?>" data-name="closedate" data-date="today"></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                  <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-save"></i> Save</button>
                                </div>
                              </div>
                    <?= form_close(); ?>
                </div>  
             
        </div>
    
        <div class="col-md-7 panel-primary">
        
            <div class="content-box-header panel-heading">
                <div class="panel-title">All Exam Registration Deadline</div>
            </div>
            <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('updated_msg')) echo get_success($this->session->flashdata('updated_msg')); ?>
                <table class="table table-bordered table-condensed" id="example">
                  <thead>
                    <tr>
                      <th>Exam Name</th>
                      <th>Registration Deadline</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                        if(count($deadlines)): 
                            foreach ($deadlines as $deadline):
                    ?>
                    <tr>
                        <td><?php echo anchor(site_url('admin/regdeadline/save/'.$deadline->id), $this->subjects_model->getExamName_From_Id($deadline->examid)) ?></td>
                        <td><?= $deadline->closedate ?></td>
                        <td>
                            <?= get_edit_btn(site_url('admin/regdeadline/save/'.$deadline->id)); ?>
                            <?= get_del_btn(site_url('admin/regdeadline/delete/'.$deadline->id)); ?>
                        </td>
                    </tr> 
                    <?php endforeach;?>
                    <?php endif; ?>
                  </tbody>
                </table>
            </div>
    
    </div>
</div>