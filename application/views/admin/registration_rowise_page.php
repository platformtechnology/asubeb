 <h4 style="text-align: center; color: #0077b3; font-weight: bold">
    REGISTRATION FOR <br/>THE <?= strtoupper($exam_detail->examdesc. ' ('.$exam_detail->examname.') ' . $exam_year) ;?> 
</h4>
<hr/>

<div class="row">
    <div class="col-md-12 panel-primary">
        
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-edit"></i> <strong>Register Students</strong>               
                </div>
            </div>
            <div class="content-box-large box-with-header">
                <?= form_open('', 'class="form-horizontal" name="regform" id="regform" role="form"'); ?>
                <div class="row">
                    <div class="col-md-10">
                        
                    <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                    
<!--                        <div class="col-md-2"></div>-->
                        <div class="col-md-12">
                            <div class="col-md-5">
                              <div class="form-group">
                               <label for="inputEmail3" class="col-sm-3 control-label">LGA:</label>
                                <div class="col-sm-9">
                                    <?php
                                        $select_options = array();
                                        $select_options[''] = "----- Select LGA-----";

                                          foreach ($lgas as $lg) {
                                              $select_options[$lg->lgaid] = $lg->lganame;
                                          }

                                        echo form_dropdown('lgaid', $select_options, ($this->input->post('lgaid') ? $this->input->post('lgaid') : ($this->session->flashdata('donelga') ? $this->session->flashdata('donelga') : $candidate_detail->lgaid)), 'id="lgaid" class="form-control" '); 
                                    ?>
                                </div>
                               </div>
                             </div> 
                            
                            <div class="col-md-7">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">School:</label>
                                <div class="col-sm-8">
                                    <?php
                                        $select_options = array();
                                        $select_options[''] = "----- Select A School -----";
                                        foreach ($schools as $school) {
                                            $select_options[$school->schoolid] = ucwords(strtolower($school->schoolname)) .' - '. $school->schoolcode;
                                        }
                                        echo form_dropdown('schoolid', $select_options, ($this->input->post('schoolid') ? $this->input->post('schoolid') : ($this->session->flashdata('done') ? $this->session->flashdata('done') : $candidate_detail->schoolid)), 'id="schoolid" class="form-control" onchange="displayCandidates()"'); 
                                    ?>
                                    
                                </div>
                            </div>
                            </div>
                        </div>
                
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-8">
                        <h5 style="text-align: center;"><strong>Note:</strong><span style="color: crimson"> Registering Candidates in a Rowise Manner automatically registers all the Subjects of the selected Exam (whether compulsory or not) for that candidate!</span></h5>
                    </div>
                </div>
                <?php if($exam_detail->hasposting):  ?>
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <h5 style="text-align: center;"><span style="color: crimson"> The Exam being registered for has posting enabled. Registering Candidates in a rowise manner automatically assigns a first choice or second choice school to the candidate!</span></h5>
                        </div>
                    </div>
                <?php endif; ?>
            <div class="row">
                <div class="page-header">
                    <h3><i class="glyphicon glyphicon-edit"></i> Register Candidates - <a href="<?= site_url('admin/registration/row/'.$exam_detail->examid.'/'.$exam_year);?>"><i class="glyphicon glyphicon-refresh"></i></a></h3>
                </div>
              <div class="col-md-12">
                  <table class="table table-bordered table-condensed" style="font-size: 12px">
                      <thead>
                        <tr>
                          <th>Surname</th>
                          <th>Othernames</th>
                          <th>Gender</th>
                          <th>DOB</th>   
                          <th>Phone</th>   
                          <th></th>   
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>
                                  <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="firstname" autofocus="true" class="form-control" value="<?= set_value('firstname', $candidate_detail->firstname) ?>" required="required"/>
                                        </div>
                                  </div>
                              </td>
                              <td>
                                  <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="othernames" class="form-control" value="<?= set_value('othernames', $candidate_detail->othernames) ?>" required="required"/>
                                        </div>
                                  </div>
                              </td>
                              <td>
                                  <div class="form-group">
                                        <div class="col-sm-12">
                                            <?php
                                                $select_options = array();
                                                $select_options['M'] = "Male";
                                                $select_options['F'] = "Female";
                                                echo form_dropdown('gender', $select_options, ($this->input->post('gender') ? $this->input->post('gender') : ($this->session->flashdata('donegender') ? $this->session->flashdata('donegender') : $candidate_detail->gender)), 'id="gender" class="form-control" required="required" '); 
                                            ?>
                                        </div>
                                  </div>
                              </td>
                              <td>
                                  
                                  <div class="bfh-datepicker" style="width: 150px" data-format="y-m-d" data-date="<?php echo set_value('dob', '2004-01-01'); ?>" data-name="dob"></div>
                                       
                                  
                              </td>
                              <td>
                                  <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="phone" class="form-control" value="<?= set_value('phone', $candidate_detail->phone) ?>" />
                                        </div>
                                  </div>
                              </td>
                              <td><button type="submit" class="btn btn-sm btn-default" ><i class="glyphicon glyphicon-save"></i> Save </button></td>
                          </tr>
                      </tbody>
                    </table>
                  
                  <br/>
                  
                  <div id="display_res"></div>
                  
                  
                </div>
              </div>
                
              <?= form_close(); ?>
            </div>
        
    </div>
</div>

<script type="text/javascript">
    
    (function() {
        var httpRequest;

        lgaddl = document.getElementById("lgaid");
        lgaddl.onchange = function() { 
            var target_url1 = "<?= site_url('admin/registration/school_ajaxdrop?issec='.(($exam_detail->hassecschool == 1) ? '1' : '0').'&lgaid='); ?>";
            //target_url1 = target_url1.replace(/\.[^/.]+$/, ""); //remove .html extension
            makeRequest( target_url1 + lgaddl.value, 'schoolid'); 
            
        };

        function makeRequest(url, targetid) {
            if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                httpRequest = new XMLHttpRequest();
            } else if (window.ActiveXObject) { // IE
                try {
                    httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                } 
                catch (e) {
                    try {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    } 
                    catch (e) {}
                }
            }

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = function(){
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        var data = JSON.parse(httpRequest.response);
                        var select = document.getElementById(targetid);
                        if(emptySelect(select)){
                            var el = document.createElement("option");
                                    el.textContent = '....Select....';
                                    el.value = '';
                                    select.appendChild(el);
                                    
                            for (var i = 0; i < data.lgas.length; i++){
                                var el = document.createElement("option");
                                    el.textContent = data.lgas[i].lganame;
                                    el.value = data.lgas[i].lgaid;
                                    select.appendChild(el);
                            }
                        }
                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
            httpRequest.send();
        }

        function emptySelect(select_object){
            var i;
            for(i=select_object.options.length-1;i>=0;i--){
                select_object.remove(i);
            }
            return true;
        }
    })();
    
    function displayCandidates(sn){
        schoolid = document.getElementById("schoolid").value;

        if(schoolid.trim() == ""){
            alert("YOU MUST SELECT A SCHOOL");
            document.getElementById("display_res").innerHTML = '';
            return;
        }
        
        var url_to_post = "<?= site_url('admin/registration/fetchCandidates?schoolid='); ?>";
        url_to_post = url_to_post + schoolid + "&examid=" + "<?= $exam_detail->examid; ?>" + "&examyear=" + "<?= $exam_year ?>";
        document.getElementById("display_res").innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
        makeRequestx(url_to_post, "display_res");
    }
    
    function submitCandidates(){
        schoolid = document.getElementById("schoolid").value;

        if(schoolid.trim() == ""){
            alert("YOU MUST SELECT A SCHOOL");
            document.getElementById("display_res").innerHTML = '';
            return;
        }
        
        var url_to_post = "<?= site_url('admin/registration/fetchCandidates?schoolid='); ?>";
        url_to_post = url_to_post + schoolid + "&examid=" + "<?= $exam_detail->examid; ?>" + "&examyear=" + "<?= $exam_year ?>";
        document.getElementById("display_res").innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
        makeRequestx(url_to_post, "display_res");
    }
    
    function examPost(sn){
       exam_no = document.getElementById("examno"+sn).value; 
       candidateid = document.getElementById("candidateid"+sn).value;
       var url_to_post = "<?= site_url('admin/registration/update_examno?exam_no='); ?>";
       url_to_post = url_to_post + exam_no + "&candidateid="+candidateid;
       document.getElementById("exam-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
       makeUpdateRequest(url_to_post, "exam-result"+sn);

    }
    
    function firstnamePost(sn){
       firstname = document.getElementById("firstname"+sn).value; 
       candidateid = document.getElementById("candidateid"+sn).value;
       var url_to_post = "<?= site_url('admin/registration/update_firstname?firstname='); ?>";
       url_to_post = url_to_post + firstname + "&candidateid="+candidateid;
       document.getElementById("fname-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
       makeUpdateRequest(url_to_post, "fname-result"+sn);

    }
    
    function othernamePost(sn){
       othername = document.getElementById("othername"+sn).value; 
       candidateid = document.getElementById("candidateid"+sn).value;
       var url_to_post = "<?= site_url('admin/registration/update_othernames?othernames='); ?>";
       url_to_post = url_to_post + othername + "&candidateid="+candidateid;
       document.getElementById("oname-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
       makeUpdateRequest(url_to_post, "oname-result"+sn);

    }
    
    function phonePost(sn){
       phone = document.getElementById("phone"+sn).value; 
       candidateid = document.getElementById("candidateid"+sn).value;
       var url_to_post = "<?= site_url('admin/registration/update_phone?phone='); ?>";
       url_to_post = url_to_post + phone + "&candidateid="+candidateid;
       document.getElementById("phone-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
       makeUpdateRequest(url_to_post, "phone-result"+sn);

    }
    
   function makeUpdateRequest(url, imgId) {
         
        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
                
         httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                   
                    var obj = JSON.parse(httpRequest.response);
                    if(obj.success == 1){
                        document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('available.png'); ?>"+'" />';
                    }else{
                        alert('Could Not Update');
                        document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                    }
                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
        httpRequest.send();
    }
//    
    
    function makeRequestx(url, displayId) {
         
        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
                
         httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                   document.getElementById(displayId).innerHTML = httpRequest.response;
                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById(displayId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
        httpRequest.send();
    }
//    
    function getHttpObject(){
        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
            httpRequest = new XMLHttpRequest();
            return httpRequest;
        } else if (window.ActiveXObject) { // IE
            try {
                httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                return httpRequest;
            } 
            catch (e) {
                try {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    return httpRequest;
                } 
                catch (e) {}
            }
        }

        return;
    }
</script>
