
<div class="row">
    <div class="col-md-12 panel-primary">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-certificate"></i> PERFORM GENERAL GRADING</strong>
                </div>
            </div>
        
             <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" role="form" target="_blank"'); ?>
                        <div class="row">
<!--                               <div class="col-md-3">
                                   <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label">Lga:</label>
                                      <div class="col-sm-9">
                                          <?php
//                                              $select_options = array();
//                                              $select_options[''] = "..::Select LGA::..";
//                                                foreach ($lgas as $lg) {
//                                                    $select_options[$lg->lgaid] = $lg->lganame;
//                                                }
//                                              echo form_dropdown('lgaid', $select_options, ($this->input->post('lgaid') ? $this->input->post('lgaid') : ''), 'id="lgaid" class="form-control" required="required"'); 
                                          ?>
                                      </div>
                                  </div>
                               </div>-->
                             <div class="col-md-5">
                                 <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 control-label">Exam:</label>
                                          <div class="col-sm-9">
                                              <?php 
                                                $select_options = array();
                                                  foreach ($exams as $exam) {
                                                      $select_options[$exam->examid] = $exam->examname;
                                                  }
                                                $select_options[''] = "------- Select An Exam -------";
                                                echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'id="examid" class="form-control" required="required"');
                                              ?>
                                          </div>
                                 </div>
                                 
                                 <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 control-label">Year:</label>
                                          <div class="col-sm-9">
                                              <?php 
                                                  $already_selected_value = ($this->input->post('examyear') ? $this->input->post('examyear') : $activeyear);
                                                  $start_year = 2015;
                                                   print '<select name="examyear" class="form-control" required="required">';
                                                   print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>..::All Year::..</option>';
                                                   for ($x = $start_year; $x <= $activeyear; $x++) {
                                                       print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                                                   }
                                                   print '</select>';
                                                 ?>
                                          </div>
                                  </div>
                                 
                                 <div class="form-group">
                                      <div class="col-sm-offset-2 col-sm-10">
                                        <label>
                                            <strong style="color: crimson">"General Grading"</strong> allows the entry of the scores of both CA, EXAM and 
                                            PRACTICAL for all subjects in a particular school.
                                        </label>
                                      </div>
                                 </div>
                             </div>
                            <div class="col-md-7">
                                   <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label">School:</label>
                                      <div class="col-sm-9">
                                                <?php
//                                              $select_options = array();
//                                              $select_options[''] = "------ Select A School ------";
//                                              
//                                                foreach ($schools as $school) {
//                                                    $select_options[$school->schoolid] = $school->schoolcode . ' / ' . $school->schoolname;
//                                                }
//                                              
//                                              echo form_dropdown('schoolid', $select_options, ($this->input->post('schoolid') ? $this->input->post('schoolid') : ''), 'id="schoolid" class="form-control" required="required"'); 
                                          ?>
                                          <div class="bfh-selectbox" data-name="schoolid" data-value="<?= $this->input->post('schoolid') ? $this->input->post('schoolid') : '' ?>" data-filter="true">
                                            <?php 

                                             echo '<div data-value="">------ Select A School ------</div>';
                                              foreach ($schools as $school) {
                                                echo '<div data-value="'.$school->schoolid.'">'. strtoupper($school->schoolcode . ' / ' . $school->schoolname). '</div>';
                                             }
                                           ?>
                                           </div>
                                      </div>
                                  </div>
                                
                                    <div class="form-group">
                                      <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-search"></i> Search</button>
                                        <a href="" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-refresh"></i> Refresh</a>
                                      </div>
                                    </div>
                                   
                                  
                                
                             </div>


                         </div>
                       
                    <?= form_close(); ?>                
              </div>

    </div>
</div>

