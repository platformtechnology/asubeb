<?php if(isset($candidates)): ?>
<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="row">
                <div class="col-md-12">
                    
                        <h5 style="text-align: center; color: #0077b3; font-weight: bold">
                           EXAM PRACTICAL GRADING SHEET FOR THE <?= strtoupper($exam_detail->examname. ' ' . $posted_values['examyear']) ;?> - <?= (count($allsubjects) > 1 ? "ALL SUBJECTS" : strtoupper($allsubjects[0]->subjectname)); ?>
                        </h5>
                   
                </div>
        </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <h5><strong>Exam: </strong> <?= strtoupper($exam_detail->examname. ' ' . $posted_values['examyear']) ;?> </h5>
                        <?php 
                            if($exam_detail->haszone)echo '<h5><strong>Zone: </strong> '  . $selected_school->zonename;
                            else echo ' <h5><strong>Local Govt: </strong> '  . $selected_school->lganame . ' (' . $selected_school->lgainitials . ') </h5>';
                        ?>
                        <h5><strong>School: </strong> <?=$this->registration_model->get_school($posted_values['schoolid'], true)->schoolname ?> (<strong>School Code: </strong><?=$this->registration_model->get_school($posted_values['schoolid'], true)->schoolcode ?>)</h5>
                        <h5><strong>Total Candidates: </strong><?= count($candidates); ?></h5>
                    </div>
                    <div class="col-md-6">
                        <h5 style="color:crimson"><strong>Note: </strong>Enter <strong>ABS</strong> or <strong>-1</strong> to Indicate That a Candidate Was Absent For The Exam!</h5>
                        <br/>
                        <h5 style="color:crimson"><strong>DR</strong> = CANDIDATE DID NOT REGISTER SUBJECT</h5>
                        <h5><strong>PRACTICAL GRADING FOR: </strong><?= (count($allsubjects) > 1 ? "ALL SUBJECTS" : strtoupper($allsubjects[0]->subjectname)); ?></h5>
                    </div>
                </div>
                
                
                <?= form_open('', 'class="form-horizontal" role="form"'); ?> 
                    <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                  
                    <!--<button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-upload"></i> Update</button>
                    <a href="" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                    <br/>
                    -->
                    <?php if(count($candidates)): ?>
                    <br/>
                    <table width="100%" class="table table-bordered table-condensed table-hover" id="gradeTable">
                        <thead>
                            <tr>
                              <th>Sn</th>
                              <th>&nbsp;</th>
                              <th>&nbsp;</th>
                              
                              <?php 
                                if(count($allsubjects)){
                                    foreach ($allsubjects as $subjects) {
                                        echo '<th style="font-size: 14px">'.  ucwords(strtolower($subjects->subjectname)).'</th>';
                                    }
                                }
                              ?>
                            </tr>
                        </thead>
                        <tbody>
                           <?php 
                                $row = 0;
                                $ca_num = 0;  $prac_num = 0; $exam_num = 0;
                                   echo '<input type="hidden" id="examid" name="examid" value="'.$posted_values['examid'].'" />';
                                   echo '<input type="hidden" id="examyear" name="examyear" value="'.$posted_values['examyear'].'" />';
                                    
                                foreach ($candidates as $candidate):
                                    ++$row;
                                    echo '<input type="hidden" id="candidateid'.$row.'" name="candidateid'.$row.'" value="'.$candidate->candidateid.'" />'; 
                                    
                                     $name = ucwords(strtolower($candidate->firstname)) . ' ' . ucwords(strtolower($candidate->othernames));
                                     
                                     //PRACTICAL ROW
                                      echo '<tr>';
                                        echo '<td>'.$row.'</td>';
                                        echo '<td>'.$candidate->examno.'</td>';
                                        echo '<td>'.anchor(site_url('admin/candidates/view/' . $candidate->candidateid), $name, 'title="View Profile"').'</td>';
                                     
                                     
                                            if(count($allsubjects)){
                                                
                                               $registered_subjects = $this->reg_subjects_model->get_registered_subjects($candidate->candidateid, $posted_values['examid'], $posted_values['examyear']);
                                               if(count($registered_subjects)){
                                                    foreach ($allsubjects as $subjects) {
                                                         foreach ($registered_subjects as $reg_subject) {
                                                             $found = false;
                                                             if($reg_subject->subjectid == $subjects->subjectid){
                                                                if($subjects->haspractical == 1){
                                                                    $exam_data = array();
                                                                    foreach ($c_scores as $scoresvalue) {
                                                                         if(($scoresvalue->subjectid == $subjects->subjectid) && ($scoresvalue->candidateid == $candidate->candidateid)){
                                                                             $exam_data = $scoresvalue;
                                                                             break 1;
                                                                         }
                                                                    }
                                                                    
                                                                    $practicalcore = !count($exam_data) ? 0 : $exam_data->practical_score;
                                                                    $examscore = !count($exam_data) ? 0 : $exam_data->exam_score;
                                                                    $cascore = !count($exam_data) ? 0 : $exam_data->ca_score;
                                                                    $maxdata = $maxscoredata[$subjects->subjectid];
                                                                    
                                                                     if($exam_detail->hasca) echo '<td title="Practical Score '.$subjects->subjectname.'">'.($cascore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $cascore.'&emsp;&emsp;'). ' + '.($examscore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $examscore.'&emsp;&emsp;'). ' + <input title="Practical Score For '.$subjects->subjectname.'" class="gradefield" name="practical'.++$prac_num.'" id="practical'.$prac_num.'" type="text" onblur="collectPracticalPost(\''.$prac_num.'\', \''.$row.'\')" value="'.($practicalcore < 0 ? 'ABS' : $practicalcore).'"  /> <span id="pracuser-result'.$prac_num.'"></span></td>';
                                                                     else echo '<td title="Practical Score '.$subjects->subjectname.'">'.($examscore < 0 ? '<span style="font-size: 10px; color: crimson;">ABS</span> &emsp;' : $examscore.'&emsp;&emsp;'). ' + <input title="Practical Score For '.$subjects->subjectname.'" class="gradefield" name="practical'.++$prac_num.'" id="practical'.$prac_num.'" type="text" onblur="collectPracticalPost(\''.$prac_num.'\', \''.$row.'\')" value="'.($practicalcore < 0 ? 'ABS' : $practicalcore).'"  /> <span id="pracuser-result'.$prac_num.'"></span></td>';
                                                                     
                                                                    echo '<input type="hidden" id="practical_subjectid'.$prac_num.'" name="practical_subjectid'.$prac_num.'" value="'.$subjects->subjectid.'" />'; 
                                                                    echo '<input type="hidden" id="maxpractical'.$prac_num.'" name="maxpractical'.$prac_num.'" value="'.$maxdata['maxpractical'].'" />'; 
                                                                }else{
                                                                   echo '<td title="No Practical For '.$subjects->subjectname.'" style="font-size: 10px; color: crimson;"><strong>NP</strong></td>'; 
                                                                }
                                                                 $found = true;
                                                                 break;
                                                             }
                                                         }
                                                         if(!$found) echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                                    }
                                               }else echo '<td title="" style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                           }
                                     echo '</tr>';
                                     
                           ?>
                                                       
                           <?php endforeach; ?>
                        </tbody>
                        
                         <tfoot>
                            <tr>
                              <th>Sn</th>
                              <th>&nbsp;</th>
                              <th>&nbsp;</th>
                              <?php 
                                if(count($allsubjects)){
                                    foreach ($allsubjects as $subjects) {
                                        echo '<th style="font-size: 14px">'.  ucwords(strtolower($subjects->subjectname)).'</th>';
                                    }
                                }
                              ?>
                            </tr>
                        </tfoot>
                      </table>
                    <br/><br/>
                    <?php endif; ?>
                    
                 <?= form_close(); ?>      
            </div>
        </div>
    </div>   
</div>
<?php endif; ?>

<script type="text/javascript">
 
    function collectCaPost(sn, row){
      ca_score = document.getElementById("ca"+sn).value;  
      candidateid = document.getElementById("candidateid"+row).value;
      subjectid = document.getElementById("ca_subjectid"+sn).value;
      examid = document.getElementById("examid").value;
      examyear = document.getElementById("examyear").value;
      
      maximum_ca = document.getElementById("maxca"+sn).value;
      
      if(maximum_ca == "0"){
          alert("Maximum Score For CA Of This Subject MUST BE SET First");
          document.getElementById("causer-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
          return;
      }
      
        if(ca_score.toLowerCase() == "abs"){
           ca_score = -1;
        }else if(isNaN(ca_score) || (ca_score.trim() == "")) {
            document.getElementById("causer-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }else if(ca_score > parseInt(maximum_ca)){
            alert("This CA Score Cannot Exceed Its Maximum Which is " + maximum_ca);
            document.getElementById("causer-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }
        
      var url_to_post = "<?= site_url('admin/grading/update_ca_score?ca_score='); ?>";
      url_to_post = url_to_post + ca_score + "&candidateid="+candidateid
              +"&subjectid="+subjectid+"&examid="+examid+"&examyear="+examyear;
      document.getElementById("causer-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
      
      makeRequestx(url_to_post, "causer-result"+sn);
     
    }
    
    function collectExamPost(sn, row){
        exam_score = document.getElementById("exam"+sn).value;  
        candidateid = document.getElementById("candidateid"+row).value;
        subjectid = document.getElementById("exam_subjectid"+sn).value;
        examid = document.getElementById("examid").value;
        examyear = document.getElementById("examyear").value;
        
        maximum_exam = document.getElementById("maxexam"+sn).value;
      
        if(maximum_exam == "0"){
            alert("Maximum Score For EXAM Of This Subject MUST BE SET First");
            document.getElementById("examuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }
      
        if(exam_score.toLowerCase() == "abs"){
           exam_score = -1;
        }else if(isNaN(exam_score) || (exam_score.trim() == "")) {
            document.getElementById("examuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }else if(exam_score > parseInt(maximum_exam)){
            alert("This EXAM Score Cannot Exceed Its Maximum Which is " + maximum_exam);
            document.getElementById("examuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }
        
        var url_to_post = "<?= site_url('admin/grading/update_exam_score?exam_score='); ?>";
        url_to_post = url_to_post + exam_score + "&candidateid="+candidateid
                +"&subjectid="+subjectid+"&examid="+examid+"&examyear="+examyear;
        document.getElementById("examuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
        makeRequestx(url_to_post, "examuser-result"+sn);
    }
    
    function collectPracticalPost(sn, row){
        practical_score = document.getElementById("practical"+sn).value;  
        candidateid = document.getElementById("candidateid"+row).value;
        subjectid = document.getElementById("practical_subjectid"+sn).value;
        examid = document.getElementById("examid").value;
        examyear = document.getElementById("examyear").value;
        
        maximum_prac = document.getElementById("maxpractical"+sn).value;
      
        if(maximum_prac == "0"){
            alert("Maximum Score For PRACTICAL Of This Subject MUST BE SET First");
            document.getElementById("pracuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }
        
        if(practical_score.toLowerCase() == "abs"){
           practical_score = -1;
        }else if(isNaN(practical_score) || (practical_score.trim() == "")) {
            document.getElementById("pracuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }else if(practical_score > parseInt(maximum_prac)){
            alert("This PRACTICAL Score Cannot Exceed Its Maximum Which is " + maximum_prac);
            document.getElementById("pracuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }
        
        var url_to_post = "<?= site_url('admin/grading/update_practical_score?practical_score='); ?>";
        url_to_post = url_to_post + practical_score + "&candidateid="+candidateid
                +"&subjectid="+subjectid+"&examid="+examid+"&examyear="+examyear;
        document.getElementById("pracuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
        makeRequestx(url_to_post, "pracuser-result"+sn);
    }
    
    function makeRequestx(url, imgId) {
         
        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
                
         httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                   
                    var obj = JSON.parse(httpRequest.response);
                    if(obj.success == 1){
                        document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('available.png'); ?>"+'" />';
                    }else{
                        alert('Could Not Update');
                        document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                    }
                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
        httpRequest.send();
    }
//    
    function getHttpObject(){

        var xmlhtp;
		
        if (window.ActiveXObject) 
        {    
            var aVersions = [ 
              "MSXML2.XMLHttp.9.0","MSXML2.XMLHttp.8.0", "MSXML2.XMLHttp.7.0",
              "MSXML2.XMLHttp.6.0","MSXML2.XMLHttp.5.0","MSXML2.XMLHttp.4.0",
              "MSXML2.XMLHttp.3.0","MSXML2.XMLHttp","Microsoft.XMLHttp"
            ];

            for (var i = 0; i < aVersions.length; i++) 
            {
                try 
                { 
                    xmlhtp = new ActiveXObject(aVersions[i]);
                    return xmlhtp;
                } catch (e) {}
            }
        }
        else  if (typeof XMLHttpRequest != 'undefined') 
        {
                xmlhtp = new XMLHttpRequest();			
                return xmlhtp;
        } 
    	throw new Error("XMLHttp object could be created.");
        
    }
</script>