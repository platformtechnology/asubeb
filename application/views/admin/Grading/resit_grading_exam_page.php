
<div class="row">
    <div class="col-md-12 panel-primary">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-certificate"></i> PERFORM RESIT EXAM GRADING</strong>
                </div>
            </div>
        
             <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" role="form" target="_blank"'); ?>
                        <div class="row">

                             <div class="col-md-5">
                                 <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 control-label">Exam:</label>
                                          <div class="col-sm-9">
                                              <?php 
                                                $select_options = array();
                                                  foreach ($exams as $exam) {
                                                      $select_options[$exam->examid] = $exam->examname;
                                                  }
                                                $select_options[''] = "------- Select An Exam -------";
                                                echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'id="examid" class="form-control" required="required"');
                                              ?>
                                          </div>
                                 </div>
                                 
                                 <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 control-label">Year:</label>
                                          <div class="col-sm-9">
                                              <?php 
                                                  $already_selected_value = ($this->input->post('examyear') ? $this->input->post('examyear') : $activeyear);
                                                  $start_year = $activeyear;
                                                   print '<select name="examyear" id="examyear" class="form-control" required="required">';
                                                   //print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>..::All Year::..</option>';
                                                   for ($x = $start_year; $x <= $activeyear; $x++) {
                                                       print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                                                   }
                                                   print '</select>';
                                                 ?>
                                          </div>
                                  </div>
                                 
                                 <div class="form-group">
                                      <div class="col-sm-offset-2 col-sm-10">
                                        <label>
                                            <strong style="color: crimson">"EXAM Grading"</strong> allows the entry of the scores of ONLY EXAM in a particular school. 
                                            You can select a particular subject to enable EXAM entry per Subject or select "---- All Subjects ----" to enable EXAM entry  for all subjects.
                                        </label>
                                      </div>
                                 </div>
                             </div>
                            <div class="col-md-7">
                                   <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label">School:</label>
                                      <div class="col-sm-9">
                                               <?php
//                                              $select_options = array();
//                                              $select_options[''] = "------ Select A School ------";
//                                              
//                                                foreach ($schools as $school) {
//                                                    $select_options[$school->schoolid] = $school->schoolcode . ' / ' . $school->schoolname;
//                                                }
//                                              
//                                              echo form_dropdown('schoolid', $select_options, ($this->input->post('schoolid') ? $this->input->post('schoolid') : ''), 'id="schoolid" class="form-control" required="required"'); 
                                          ?>
                                          <div class="bfh-selectbox" data-name="schoolid" data-value="<?= $this->input->post('schoolid') ? $this->input->post('schoolid') : '' ?>" data-filter="true">
                                            <?php 

                                             echo '<div data-value="">------ Select A School ------</div>';
                                              foreach ($schools as $school) {
                                                echo '<div data-value="'.$school->schoolid.'">'. strtoupper($school->schoolcode . ' / ' . $school->schoolname). '</div>';
                                             }
                                           ?>
                                           </div>
                                      </div>
                                  </div>
                                
                                <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label">Subjects:</label>
                                      <div class="col-sm-9">
                                          <?php
                                              $select_options = array();
                                              $select_options[''] = "------ All Subjects ------";                                              
                                              echo form_dropdown('subjectid', $select_options, ($this->input->post('subjectid') ? $this->input->post('subjectid') : ''), 'id="subjectid" class="form-control"'); 
                                          ?>
                                      </div>
                                  </div>
                                
                                    <div class="form-group">
                                      <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-search"></i> Search</button>
                                        <a href="" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-refresh"></i> Refresh</a>
                                      </div>
                                    </div>
                                   
                                  
                                
                             </div>


                         </div>
                       
                    <?= form_close(); ?>                
              </div>

    </div>
</div>

<script type="text/javascript">
 (function() {
            
    var httpRequest;

    examddl = document.getElementById("examid");
    examyearddl = document.getElementById("examyear");
    
    examddl.onchange = function() { 
        var target_url3 = "<?= site_url('admin/grading_exam/subjects_ajaxdrop?examid='); ?>";
        makeRequest(target_url3 + examddl.value + '&examyear=' + examyearddl.value, 'subjectid'); 
    };   
    
    

    function makeRequest(url, targetid) {
       httpRequest = getHttpObject();
        
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
        httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                    //alert(httpRequest.response);
                    var data = JSON.parse(httpRequest.response);
                    var select = document.getElementById(targetid);
                    if(emptySelect(select)){
                        var el = document.createElement("option");
                                el.textContent = '------ All Subjects ------';
                                el.value = '';
                                select.appendChild(el);

                        for (var i = 0; i < data.lgas.length; i++){
                            var el = document.createElement("option");
                                el.textContent = data.lgas[i].lganame;
                                el.value = data.lgas[i].lgaid;
                                select.appendChild(el);
                        }
                    }
                } else {
                    alert('There was a problem with the request.');
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
        httpRequest.send();
    }

    function emptySelect(select_object){
        var i;
        for(i=select_object.options.length-1;i>=0;i--){
            select_object.remove(i);
        }
        return true;
    }
    
    function getHttpObject(){

        var xmlhtp;
		
        if (window.ActiveXObject) 
        {    
            var aVersions = [ 
              "MSXML2.XMLHttp.9.0","MSXML2.XMLHttp.8.0", "MSXML2.XMLHttp.7.0",
              "MSXML2.XMLHttp.6.0","MSXML2.XMLHttp.5.0","MSXML2.XMLHttp.4.0",
              "MSXML2.XMLHttp.3.0","MSXML2.XMLHttp","Microsoft.XMLHttp"
            ];

            for (var i = 0; i < aVersions.length; i++) 
            {
                try 
                { 
                    xmlhtp = new ActiveXObject(aVersions[i]);
                    return xmlhtp;
                } catch (e) {}
            }
        }
        else  if (typeof XMLHttpRequest != 'undefined') 
        {
                xmlhtp = new XMLHttpRequest();			
                return xmlhtp;
        } 
    	throw new Error("XMLHttp object could be created.");
        
    }
})();


</script>

