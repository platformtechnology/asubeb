<?php //var_dump ($data['students_count_per_remark']); ?>


    <h4 style="text-align: center; color: #0077b3; font-weight: bold">
       SUBJECT DISTRIBUTED REMARK SETTING FOR <span style="font-size: 26px"><?= strtoupper($this->registration_model->getExamName_From_Id($examid)); ?> </span> <?= $examyear; ?>
<!--        <br/><br/>
        <a href="<?= site_url('admin/remarks/scoredistribution/'.$examid.'/'.$examyear) ?>" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-arrow-right"></i> Go to SCORE Distributed Remark Setting</a>-->
    </h4>
    <hr/>

<div class="row">
    <div class="col-md-6 panel-primary">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-edit"></i> Set Remarks</strong>
                </div>
            </div>
             <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('msg') || isset($msg)) echo get_success( isset($msg) ? $msg : ($this->session->flashdata('msg') ? $this->session->flashdata('msg') : '' ) ); ?>
                <?php if(validation_errors() || isset($errors)) echo get_error(validation_errors() || $errors); ?>
                <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Subject:</label>
                            <div class="col-sm-8">
                                <?php
                                  $select_options = array();
                                  foreach ($subjects as $subject) {
                                      $select_options[$subject->subjectid] = ucwords(strtolower($subject->subjectname));
                                  }
                                  $select_options[''] = "..:: Select Subject ::..";
                                  echo form_dropdown('subjectid', $select_options, $this->input->post('subjectid') ? $this->input->post('subjectid') : '', 'class="form-control" required="required"');
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Minimum Score:</label>
                           <div class="col-sm-8">
                              <input type="text" name="minimum" value="<?= set_value('minimum'); ?>" class="form-control" placeholder="Enter Minimum Score" required="required">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Maximum Score:</label>
                           <div class="col-sm-8">
                              <input type="text" name="maximum" value="<?= set_value('maximum'); ?>" class="form-control" placeholder="Enter Maximum Score" required="required">
                           </div>
                        </div>
                         <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Grade:</label>
                           <div class="col-sm-8">
                              <input type="text" name="grade" value="<?= set_value('grade'); ?>" class="form-control" placeholder="Enter Grade Equivalent" required="required">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Remark:</label>
                           <div class="col-sm-8">
                              <input type="text" name="remark" value="<?= set_value('remark'); ?>" class="form-control" placeholder="Enter Remark" required="required">
                           </div>
                        </div>

                    <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-send"></i> Submit</button>
                        <a href="<?= site_url('admin/remarks'); ?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                      </div>
                    </div>
                <?php echo form_close(); ?>
             </div>
    </div>

    <?php
        $row = 0;

        foreach($subjects as $subject):
            ++$row;
            $remarks = $this->remarks_model->get_subject_remarks($subject->subjectid, $examyear);

    ?>
        <div class="col-md-6 panel-info">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-arrow-right"></i> <?= strtoupper($subject->subjectname); ?> REMARKS</strong>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('msg'.$row)) echo get_success($this->session->flashdata('msg'.$row)); ?>
                 <?php if($this->session->flashdata('error'.$row)) echo get_error($this->session->flashdata('error'.$row)); ?>
                 <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>MIN</th>
                                <th>MAX</th>
                                <th>GRADE</th>
                                <th>REMARK</th>
                                <th>STUDENTS</th>
                                <th>%</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(count($remarks)):
                                    $sn = 0;
                                    foreach ($remarks as $remark):
                                        ++$sn;
                                        $data = $this->remarks_model->get_stat_per_remark($subject->subjectid, $examid, $examyear, $remark->minimum, $remark->maximum);

                                        echo form_open(site_url('admin/remarks/update_subject_remark/'.$examid.'/'.$examyear.'/'.$row), 'class="form-horizontal" role="form"');
                            ?>

                                        <tr>
                                            <td><?= '<input style="width: 50px" title="Miminum Score" class="gradefield" id="minimum" type="text" name="minimum" value="'.($remark->minimum).'" required="required" />'; ?></td>
                                            <td><?= '<input style="width: 50px" title="Maximum Score" class="gradefield" id="maximum" type="text" name="maximum" value="'.($remark->maximum).'" required="required" />'; ?></td>
                                            <td><?= '<input style="width: 50px" title="Grade" class="gradefield" id="grade" type="text" name="grade" value="'.($remark->grade).'" required="required" />'; ?></td>
                                            <td><?= '<input style="width: 100px" title="Remark" class="gradefield" id="remark" type="text" name="remark" value="'.($remark->remark).'" required="required" />'; ?></td>
                                            <td><?php echo $data['students_count_per_remark']; ?></td>
                                            <td><?php echo round($data['percentage_count_per_remark'], 2); ?>%</td>
                                            <td>
                                                <input type="hidden" name="remarkid" id="remarkid" value="<?= $remark->remarkid ?>" />
                                                <button type="submit" title="update remark" style="height: 20px; font-size: 10px"><i class="glyphicon glyphicon-upload"></i> </button>
                                                 <?= get_del_btn(site_url('admin/remarks/delete_subject_remark/'. $remark->remarkid.'/'.$examid.'/'.$examyear)); ?>
                                            </td>
                                        </tr>
                            <?php
                                        echo form_close();
                                    endforeach;
                            ?>
                                        <tr>
                                            <td colspan="3">&nbsp;</td>
                                            <td><strong>TOTAL: </strong></td>
                                            <td><strong><?php echo $this->remarks_model->total_students_per_subject($subject->subjectid, $examid, $examyear); ?></strong></td>
                                            <td><strong>100%</strong></td>
                                            <td>&nbsp;</td>
                                        </tr>
                            <?php
                                endif;
                            ?>
                        </tbody>
                 </table>
            </div>
         </div>

    <?php if(($row % 2) == 0) echo '</div><div class="row">'; ?>
    <?php endforeach; ?>
</div>