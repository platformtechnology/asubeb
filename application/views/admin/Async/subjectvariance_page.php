<?php
    $zero_remark = $this->remarks_model->get_stat_per_remark($subject_detail->subjectid, $examid, $examyear, '0', '0');
    $hundred_remark = $this->remarks_model->get_stat_per_remark($subject_detail->subjectid, $examid, $examyear, '0', '100');
?>

<div class="container">
    <h5><strong>Once You Print using the print button below, All entries will be cleared. - You can use Ctrl+P Instead </strong></h5>
   <button class="btn btn-inverse btn-sm" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> Print</button>
</div>
<div class="row" id="printdiv">
                <div class="page-header">
                    <div class="container">
                        <h4><i class="glyphicon glyphicon-bookmark"></i> <?= strtoupper($subject_detail->subjectname)?> </h4>

                            <h5>
                            <table class="table table-bordered" style="width: 200px">

                                    <tr>
                                        <td><strong><span style="text-decoration: overline;">X</span></strong></td>
                                        <td><input id="mean" type="text" value="<?= $mean ?>" style="border: 0px;" readonly="readonly"/></td>
                                        <td><strong>SD</strong></td>
                                        <td><input id="stddev" type="text" value="<?= $stddeviation ?>" style="border: 0px;" readonly="readonly"/></td>
                                    </tr>

                            </table>
                            </h5>
                         </h4>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="container">
                            <strong>OPTION 1 </strong>
                            <br/>
                            <table class="table table-bordered" style="width: 200px">

                                    <tr>
                                        <td><strong>N1</strong></td>
                                        <td><div id="n11"> --- </div></td>
                                       <td><strong>N2</strong></td>
                                        <td><div id="n21"> --- </div></td>
                                        <td><strong>N3</strong></td>
                                        <td><div id="n31"> --- </div></td>
                                    </tr>

                            </table>
                        </div>

                        <div class="col-md-6">

                            <input type="hidden" id="async_subjectid" value="<?= $subject_detail->subjectid; ?>" class="gradefield" />
                           <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>REMARK</th>
                                        <th>MIN</th>
                                        <th>MAX</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $ct_remarks = count($remarks);
                                        if($ct_remarks):
                                            $count = 0; $sn = 0;
                                            foreach ($remarks as $remark):
                                                ++$sn;
                                                $first_color = ($count == 0 ? 'style="background-color: #f7e3e3"' : '');
                                                $first_value = ($count == 0 ? 'readonly="readonly" disabled="true"' : 'onchange="updateVariance(this, \''.$sn.'\', \'1\')"');

                                                $last_color = (($count == $ct_remarks-1) ? 'style="background-color: #ebe9e9"' : '');
                                                $last_value = (($count == $ct_remarks-1) ? 'value="100" readonly="readonly" disabled="true"' : 'value="0" onchange="updateVariance(this, \''.$sn.'\', \'1\')"');

                                                $population = ($count == $ct_remarks-1) ? $total_candidates_per_subject - $zero_remark['students_count_per_remark'] : $zero_remark['students_count_per_remark'];
                                                $percentage = ($count == $ct_remarks-1) ? 100 - $zero_remark['percentage_count_per_remark'] : $zero_remark['percentage_count_per_remark'];
                                     ?>

                                                <tr>
                                                    <td><?= strtoupper($remark->remark)?></td>
                                                    <td <?= $first_color ?>> <input type="text" id="min<?= $sn; ?>1" value="0" class="gradefield" <?= $first_value; ?> /> </td>
                                                    <td <?= $last_color ?>><input type="text" id="max<?= $sn; ?>1" class="gradefield" <?= $last_value; ?> /></td>
                                                    <td><div id="population<?= $sn; ?>1"><?= $population; ?></div></td>
                                                    <td><div id="percentage<?= $sn; ?>1"><?= $percentage ?></div></td>
                                                </tr>

                                    <?php
                                                ++$count;
                                            endforeach;

                                    ?>
                                    <tr>
                                        <td><strong>TOTAL</strong></td>
                                        <td></td>
                                        <td><button type="button" onclick="updateGraph('<?=$sn?>', '1')" class="btn btn-default btn-sm" style="font-size: 11px;" ><i class="glyphicon glyphicon-refresh"></i> Refresh Graph</button></td>
                                        <td><strong><div id="totalpop<?= $sn; ?>1"><?= $total_candidates_per_subject; ?></div></strong></td>
                                        <td><strong>100%</strong></td>
                                    </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div id="graph1" style="height: 230px"></div>
                        </div>
                    </div>

                    <p style="page-break-before: always;"></p>

                    <div class="row">
                         <div class="container">
                            <strong>OPTION 2 </strong>
                            <br/>
                            <table class="table table-bordered" style="width: 200px">

                                    <tr>
                                        <td><strong>N1</strong></td>
                                        <td><div id="n12"> --- </div></td>
                                       <td><strong>N2</strong></td>
                                        <td><div id="n22"> --- </div></td>
                                        <td><strong>N3</strong></td>
                                        <td><div id="n32"> --- </div></td>
                                    </tr>

                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>REMARK</th>
                                        <th>MIN</th>
                                        <th>MAX</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $ct_remarks = count($remarks);
                                        if($ct_remarks):
                                            $count = 0; $sn = 0;
                                            foreach ($remarks as $remark):
                                                ++$sn;
                                                $first_color = ($count == 0 ? 'style="background-color: #f7e3e3"' : '');
                                                $first_value = ($count == 0 ? 'readonly="readonly" disabled="true"' : 'onkeyup="updateVariance(this, \''.$sn.'\', \'2\')"');

                                                $last_color = (($count == $ct_remarks-1) ? 'style="background-color: #ebe9e9"' : '');
                                                $last_value = (($count == $ct_remarks-1) ? 'value="100" readonly="readonly" disabled="true"' : 'value="0" onkeyup="updateVariance(this, \''.$sn.'\', \'2\')"');

                                                $population = '----';//($count == $ct_remarks-1) ? $total_candidates_per_subject - $zero_remark['students_count_per_remark'] : $zero_remark['students_count_per_remark'];
                                                $percentage = '----';//($count == $ct_remarks-1) ? 100 - $zero_remark['percentage_count_per_remark'] : $zero_remark['percentage_count_per_remark'];
                                     ?>

                                                <tr>
                                                    <td><?= strtoupper($remark->remark)?></td>
                                                    <td <?= $first_color ?>> <input type="text" id="min<?= $sn; ?>2" value="0" class="gradefield" <?= $first_value; ?> /> </td>
                                                    <td <?= $last_color ?>><input type="text" id="max<?= $sn; ?>2" class="gradefield" <?= $last_value; ?> /></td>
                                                    <td><div id="population<?= $sn; ?>2"><?= $population; ?></div></td>
                                                    <td><div id="percentage<?= $sn; ?>2"><?= $percentage ?></div></td>
                                                </tr>

                                    <?php
                                                ++$count;
                                            endforeach;
                                    ?>
                                    <tr>
                                        <td><strong>TOTAL</strong></td>
                                        <td></td>
                                        <td><button type="button" onclick="updateGraph('<?=$sn?>', '2')" class="btn btn-default btn-sm" style="font-size: 11px;" ><i class="glyphicon glyphicon-refresh"></i> Refresh Graph</button></td>
                                        <td><strong><div id="totalpop<?= $sn; ?>2"><?= $total_candidates_per_subject; ?></div></strong></td>
                                        <td><strong>100%</strong></td>
                                    </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                         <div class="col-md-6">
                             <div id="graph2" style="height: 230px"></div>
                         </div>
                    </div>

                    <p style="page-break-before: always;"></p>

                    <div class="row">
                         <div class="container">
                            <strong>OPTION 3 </strong>
                            <br/>
                            <table class="table table-bordered" style="width: 200px">

                                    <tr>
                                        <td><strong>N1</strong></td>
                                        <td><div id="n13"> --- </div></td>
                                       <td><strong>N2</strong></td>
                                        <td><div id="n23"> --- </div></td>
                                        <td><strong>N3</strong></td>
                                        <td><div id="n33"> --- </div></td>
                                    </tr>

                            </table>
                        </div>
                        <div class="col-md-6">
                           <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>REMARK</th>
                                        <th>MIN</th>
                                        <th>MAX</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $ct_remarks = count($remarks);
                                        if($ct_remarks):
                                            $count = 0; $sn = 0;
                                            foreach ($remarks as $remark):
                                                ++$sn;
                                                $first_color = ($count == 0 ? 'style="background-color: #f7e3e3"' : '');
                                                $first_value = ($count == 0 ? 'readonly="readonly" disabled="true"' : 'onkeyup="updateVariance(this, \''.$sn.'\', \'3\')"');

                                                $last_color = (($count == $ct_remarks-1) ? 'style="background-color: #ebe9e9"' : '');
                                                $last_value = (($count == $ct_remarks-1) ? 'value="100" readonly="readonly" disabled="true"' : 'value="0" onkeyup="updateVariance(this, \''.$sn.'\', \'3\')"');

                                                $population = '----';//($count == $ct_remarks-1) ? $total_candidates_per_subject - $zero_remark['students_count_per_remark'] : $zero_remark['students_count_per_remark'];
                                                $percentage = '----';//($count == $ct_remarks-1) ? 100 - $zero_remark['percentage_count_per_remark'] : $zero_remark['percentage_count_per_remark'];
                                     ?>

                                                <tr>
                                                    <td><?= strtoupper($remark->remark)?></td>
                                                    <td <?= $first_color ?>> <input type="text" id="min<?= $sn; ?>3" value="0" class="gradefield" <?= $first_value; ?> /> </td>
                                                    <td <?= $last_color ?>><input type="text" id="max<?= $sn; ?>3" class="gradefield" <?= $last_value; ?> /></td>
                                                    <td><div id="population<?= $sn; ?>3"><?= $population; ?></div></td>
                                                    <td><div id="percentage<?= $sn; ?>3"><?= $percentage ?></div></td>
                                                </tr>

                                    <?php
                                                ++$count;
                                            endforeach;
                                    ?>
                                    <tr>
                                        <td><strong>TOTAL</strong></td>
                                        <td></td>
                                        <td><button type="button" onclick="updateGraph('<?=$sn?>', '3')" class="btn btn-default btn-sm" style="font-size: 11px;" ><i class="glyphicon glyphicon-refresh"></i> Refresh Graph</button></td>
                                        <td><strong><div id="totalpop<?= $sn; ?>3"><?= $total_candidates_per_subject; ?></div></strong></td>
                                        <td><strong>100%</strong></td>
                                    </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                         <div class="col-md-6">
                             <div id="graph3" style="height: 230px"></div>
                         </div>
                    </div>

                    <p style="page-break-before: always;"></p>

                    <div class="row">
                         <div class="container">
                            <strong>OPTION 4 </strong>
                            <br/>
                            <table class="table table-bordered" style="width: 200px">

                                    <tr>
                                        <td><strong>N1</strong></td>
                                        <td><div id="n14"> --- </div></td>
                                       <td><strong>N2</strong></td>
                                        <td><div id="n24"> --- </div></td>
                                        <td><strong>N3</strong></td>
                                        <td><div id="n34"> --- </div></td>
                                    </tr>

                            </table>
                        </div>
                        <div class="col-md-6">
                          <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>REMARK</th>
                                        <th>MIN</th>
                                        <th>MAX</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $ct_remarks = count($remarks);
                                        if($ct_remarks):
                                            $count = 0; $sn = 0;
                                            foreach ($remarks as $remark):
                                                ++$sn;
                                                $first_color = ($count == 0 ? 'style="background-color: #f7e3e3"' : '');
                                                $first_value = ($count == 0 ? 'readonly="readonly" disabled="true"' : 'onkeyup="updateVariance(this, \''.$sn.'\', \'4\')"');

                                                $last_color = (($count == $ct_remarks-1) ? 'style="background-color: #ebe9e9"' : '');
                                                $last_value = (($count == $ct_remarks-1) ? 'value="100" readonly="readonly" disabled="true"' : 'value="0" onkeyup="updateVariance(this, \''.$sn.'\', \'4\')"');

                                                $population = '----';//($count == $ct_remarks-1) ? $total_candidates_per_subject - $zero_remark['students_count_per_remark'] : $zero_remark['students_count_per_remark'];
                                                $percentage = '----';//($count == $ct_remarks-1) ? 100 - $zero_remark['percentage_count_per_remark'] : $zero_remark['percentage_count_per_remark'];
                                     ?>

                                                <tr>
                                                    <td><?= strtoupper($remark->remark)?></td>
                                                    <td <?= $first_color ?>> <input type="text" id="min<?= $sn; ?>4" value="0" class="gradefield" <?= $first_value; ?> /> </td>
                                                    <td <?= $last_color ?>><input type="text" id="max<?= $sn; ?>4" class="gradefield" <?= $last_value; ?> /></td>
                                                    <td><div id="population<?= $sn; ?>4"><?= $population; ?></div></td>
                                                    <td><div id="percentage<?= $sn; ?>4"><?= $percentage ?></div></td>
                                                </tr>

                                    <?php
                                                ++$count;
                                            endforeach;
                                    ?>
                                    <tr>
                                        <td><strong>TOTAL</strong></td>
                                        <td></td>
                                        <td><button type="button" onclick="updateGraph('<?=$sn?>', '4')" class="btn btn-default btn-sm" style="font-size: 11px;" ><i class="glyphicon glyphicon-refresh"></i> Refresh Graph</button></td>
                                        <td><strong><div id="totalpop<?= $sn; ?>4"><?= $total_candidates_per_subject; ?></div></strong></td>
                                        <td><strong>100%</strong></td>
                                    </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                         <div class="col-md-6">
                              <div id="graph4" style="height: 230px"></div>
                         </div>
                    </div>
                    <p style="page-break-before: always;"></p>
                </div>
            </div>
