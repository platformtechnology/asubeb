           
    <div class="row">
        <div class="col-md-12 panel-primary">
            <div class="content-box-header panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> <strong>ALL SCHOOLS</strong></div>
            </div>
            <div class="content-box-large box-with-header">
                <div class="row">
                    <div class="col-md-12">
                        <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                        <h4>SEARCH FOR THE SCHOOL TO EDIT</h4>
                        <hr/>
                        <table class="table table-bordered table-condensed" id="example">
                          <thead>
                            <tr>
                              <th>School Name</th>
                              <th>School Code</th>
                              <th>LGA</th>
                              <th>Zone</th>   
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                               <?php 
                                if(count($schools)): 
                                    foreach ($schools as $school):
                                ?>
                                <tr>
                                    <td><?php echo anchor(site_url('admin/schools_edit/save/' .  $school->schoolid), $school->schoolname) ?></td>
                                    <td><?= $school->schoolcode; ?></td>
                                    <td><?= $this->schools_model->get_lga_name($school->lgaid); ?></td>
                                    <td><?= $this->schools_model->get_zone_name($school->zoneid); ?></td>
                                    <td>
                                        <?= get_edit_btn(site_url('admin/schools_edit/save/' . $school->schoolid)); ?>
                                    </td>
                                </tr> 
                                <?php endforeach;?>
                                <?php endif; ?>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



<script type="text/javascript">
    (function() {
        var httpRequest;
        dropper = document.getElementById("zoneid");
        dropper.onchange = function() { 
            var target_url = "<?= site_url('admin/schools/ajaxdrop?zoneid='); ?>";
            target_url = target_url.replace(/\.[^/.]+$/, ""); //remove .html extension
            makeRequest( target_url + dropper.value); 
        };

        function makeRequest(url) {
           httpRequest = getHttpObject();

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = alertContents;
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
            httpRequest.send();
        }

        function alertContents() {
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                    var data = JSON.parse(httpRequest.response);
                    var select = document.getElementById('lgaid');
                    if(emptySelect(select)){
                        
                        var e2 = document.createElement("option");
                        e2.textContent = '----------------';
                        e2.value = '';
                        select.appendChild(e2);
                    
                        for (var i = 0; i < data.lgas.length; i++){
                            var el = document.createElement("option");
                                el.textContent = data.lgas[i].lganame;
                                el.value = data.lgas[i].lgaid;
                                select.appendChild(el);
                        }
                    }
                } else {
                    alert('There was a problem with the request.');
                }
            }
        }

        function emptySelect(select_object){
            while(select_object.options.length > 0){                
                select_object.remove(0);
            }
            return 1;
        }
        
        function getHttpObject(){

            var xmlhtp;

            if (window.ActiveXObject) 
            {    
                var aVersions = [ 
                  "MSXML2.XMLHttp.9.0","MSXML2.XMLHttp.8.0", "MSXML2.XMLHttp.7.0",
                  "MSXML2.XMLHttp.6.0","MSXML2.XMLHttp.5.0","MSXML2.XMLHttp.4.0",
                  "MSXML2.XMLHttp.3.0","MSXML2.XMLHttp","Microsoft.XMLHttp"
                ];

                for (var i = 0; i < aVersions.length; i++) 
                {
                    try 
                    { 
                        xmlhtp = new ActiveXObject(aVersions[i]);
                        return xmlhtp;
                    } catch (e) {}
                }
            }
            else  if (typeof XMLHttpRequest != 'undefined') 
            {
                    xmlhtp = new XMLHttpRequest();			
                    return xmlhtp;
            } 
            throw new Error("XMLHttp object could be created.");

        }
    })();
    </script>  
