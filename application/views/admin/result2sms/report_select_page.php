
<h4 style="text-align: center; color: #0077b3; font-weight: bold">
    <?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>
    REPORT TO SMS SENDING FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?>
</h4>
<hr/>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 panel-primary">
                            <div class="content-box-header panel-heading">
                                <div class="panel-title">
                                    <strong><i class="glyphicon glyphicon-list"></i> RESULT TO SMS</strong>
                                </div>
                            </div>

                            <div class="content-box-large box-with-header">
                                <?php if ($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                                <?php if (strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                                <?= form_open(site_url('admin/results/dynamicreport/' . $examid . '/' . $examyear), 'class="form-horizontal" name="regform" target="_blank" id="regform" role="form"'); ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label"><?= $examdetail->haszone ? 'Select Zone' : 'Select Lga'; ?></label>
                                        <div class="col-sm-8">
                                            <?php
                                            if ($examdetail->haszone) {
                                                $select_options = array();
                                                foreach ($zones as $zone) {
                                                    $select_options[$zone->zoneid] = $zone->zonename;
                                                }
                                                $select_options[''] = "--- All Zones ---";
                                                echo form_dropdown('zoneid', $select_options, $this->input->post('zoneid') ? $this->input->post('zoneid') : $reportdetail->zoneid, 'class="form-control" id="zoneid"');
                                            } else {
                                                $select_options = array();
                                                foreach ($lgas as $lga) {
                                                    $select_options[$lga->lgaid] = $lga->lganame;
                                                }
                                                $select_options[''] = "--- All LGAs ---";
                                                echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : $reportdetail->lgaid, 'class="form-control" id="lgaid"');
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Select School:</label>
                                        <div class="col-sm-8">
                                            <?php
                                            $select_options = array();
                                            $select_options[''] = "--- All Schools ---";
                                            if (($this->input->post('schoolid'))) {
                                                foreach ($schools as $school) {
                                                    $select_options[$school->schoolid] = ucwords(strtolower($school->schoolname)) . ' - ' . $school->schoolcode;
                                                }
                                            }
                                            echo form_dropdown('schoolid', $select_options, ($this->input->post('schoolid') ? $this->input->post('schoolid') : ''), 'id="schoolid" class="form-control"');
                                            ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Which Result are you sending</label>
                                        <div class="col-sm-8">
                                            <?php
                                            $select_options = array();
                                            $select_options[''] = "----- Select Report -----";
                                            //$select_options['SSR'] = "RAW SCORE SHEET REPORT";
                                            $select_options['SSSR'] = "STANDARD SCORE SHEET REPORT";
                                            echo form_dropdown('reporttype', $select_options, ($this->input->post('reporttype') ? $this->input->post('reporttype') : 'CLR'), 'id="reporttype" class="form-control" required="required"');
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-12">
                                            <button type="submit" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-search"></i> View Report</button>
                                            <a href="<?= site_url('admin/reports') ?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
                                        </div>
                                    </div>
                                </div>


                                <?= form_close(); ?>
                            </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="analysisModal" tabindex="-1" role="dialog" aria-labelledby="analysisModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="analysisModalLabel">SUBJECT ANALYSIS REPORT</h4>
            </div>

            <?= form_open(site_url('admin/reports/subjectanalysis/' . $examid . '/' . $examyear), 'class="form-horizontal" role="form" target="_blank"'); ?>
            <div class="modal-body">
                <br/>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label"><?= $examdetail->haszone ? 'ZONE' : 'LGA' ?>:</label>
                    <div class="col-sm-9">
                        <?php
                        if ($examdetail->haszone) {
                            $select_options = array();
                            foreach ($zones as $zone) {
                                $select_options[$zone->zoneid] = $zone->zonename;
                            }
                            $select_options[''] = "--- SELECT ZONE ---";
                            echo form_dropdown('zoneid', $select_options, $this->input->post('zoneid') ? $this->input->post('zoneid') : $reportdetail->zoneid, 'class="form-control" id="zoneid"');
                        } else {
                            $select_options = array();
                            foreach ($lgas as $lga) {
                                $select_options[$lga->lgaid] = $lga->lganame;
                            }
                            $select_options[''] = "--- SELECT LGA ---";
                            echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : $reportdetail->lgaid, 'class="form-control" id="lgaid"');
                        }
                        ?>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-eye-open"></i> VIEW </button>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
</div>

<div class="modal fade" id="attendanceModal" tabindex="-1" role="dialog" aria-labelledby="attendanceModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="analysisModalLabel">ATTENDANCE REPORT</h4>
            </div>

            <?= form_open(site_url('admin/reports/attendance/' . $examdetail->examid . '/' . $examyear), 'class="form-horizontal" role="form" target="_blank"'); ?>
            <div class="modal-body">

                ATTENDANCE FOR <?= strtoupper($examdetail->examname); ?>  <?= $examyear; ?>
                <hr/>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label"><?= $examdetail->haszone ? 'ZONE' : 'LGA' ?>::</label>
                    <div class="col-sm-9">
                        <?php
                        if ($examdetail->haszone) {
                            $select_options = array();
                            foreach ($zones as $zone) {
                                $select_options[$zone->zoneid] = $zone->zonename;
                            }
                            $select_options[''] = "--- SELECT ZONE ---";
                            echo form_dropdown('attendancezoneid', $select_options, $this->input->post('attendancezoneid') ? $this->input->post('attendancezoneid') : '', 'class="form-control" required="required" id="attendancezoneid"');
                        } else {
                            $select_options = array();
                            foreach ($lgas as $lga) {
                                $select_options[$lga->lgaid] = $lga->lganame;
                            }
                            $select_options[''] = "--- SELECT LGA ---";
                            echo form_dropdown('attendancelgaid', $select_options, $this->input->post('attendancelgaid') ? $this->input->post('attendancelgaid') : '', 'class="form-control" required="required" id="attendancelgaid"');
                        }
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">School:</label>
                    <div class="col-sm-9">
                        <?php
                        $select_options = array();
                        $select_options[''] = "---- All Schools ----";
                        if (($this->input->post('attendanceschoolid'))) {
                            foreach ($schools as $school) {
                                $select_options[$school->schoolid] = $school->schoolname;
                            }
                        }
                        echo form_dropdown('attendanceschoolid', $select_options, $this->input->post('attendanceschoolid') ? $this->input->post('attendanceschoolid') : '', 'id="attendanceschoolid" class="form-control" ');
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Columns:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="columns" value="<?= set_value('columns'); ?>" />
                        <br/>
                        Type the additional columns you want to appear in the report. <br/>
                        The default columns are Examno, Name, Sex and Attendance<br/>
                        Separate multiple column name with comma (,),
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> VIEW </button>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
</div>


<script type="text/javascript">
    (function() {
        var httpRequest;

<?php
if ($examdetail->haszone) {
    ?>
            zoneddl = document.getElementById("zoneid");
		    examid  = "<?=  $examid; ?>";
		    examyear  = "<?=  $examyear; ?>";
            zoneddl.onchange = function() {
                var target_url = "<?= site_url('admin/reports/school_ajaxdrop_zone?zoneid='); ?>";
                makeRequest(target_url + zoneddl.value+'&examid='+examid+'&examyear='+examyear, 'schoolid');
            };

            attendancezoneddl = document.getElementById("attendancezoneid");
            attendancezoneddl.onchange = function() {
                var target_url = "<?= site_url('admin/reports/school_ajaxdrop_zone?zoneid='); ?>";
                makeRequest(target_url + attendancezoneddl.value, 'attendanceschoolid');
            };
    <?php
} else {
    ?>
            attendancelgaddl = document.getElementById("attendancelgaid");
            attendancelgaddl.onchange = function() {
                var target_url1 = "<?= site_url('admin/reports/school_ajaxdrop?lgaid='); ?>";
                makeRequest(target_url1 + attendancelgaddl.value, 'attendanceschoolid');
            };

	        examid  = "<?=  $examid; ?>";
            examyear  = "<?=  $examyear; ?>";
            lgaddl = document.getElementById("lgaid");
            lgaddl.onchange = function() {
                var target_url1 = "<?= site_url('admin/reports/school_ajaxdrop?lgaid='); ?>";
                makeRequest(target_url1 + lgaddl.value+'&examid='+examid+'&examyear='+examyear, 'schoolid');
            };



    <?php
}
?>

        function makeRequest(url, targetid) {
            httpRequest = getHttpObject();

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        //alert(httpRequest.response);
                        var data = JSON.parse(httpRequest.response);
                        var select = document.getElementById(targetid);
                        if (emptySelect(select)) {
                            var el = document.createElement("option");
                            if (targetid == 'lgaid')
                                el.textContent = '-------------';
                            else
                                el.textContent = '---- All Schools ----';

                            el.value = '';
                            select.appendChild(el);

                            for (var i = 0; i < data.lgas.length; i++) {
                                var el = document.createElement("option");
                                el.textContent = data.lgas[i].lganame;
                                el.value = data.lgas[i].lgaid;
                                select.appendChild(el);
                            }

                        }
                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            httpRequest.send();
        }

        function emptySelect(select_object) {
            var i;
            for (i = select_object.options.length - 1; i >= 0; i--) {
                select_object.remove(i);
            }
            return true;
        }

        function getHttpObject() {

            var xmlhtp;

            if (window.ActiveXObject)
            {
                var aVersions = [
                    "MSXML2.XMLHttp.9.0", "MSXML2.XMLHttp.8.0", "MSXML2.XMLHttp.7.0",
                    "MSXML2.XMLHttp.6.0", "MSXML2.XMLHttp.5.0", "MSXML2.XMLHttp.4.0",
                    "MSXML2.XMLHttp.3.0", "MSXML2.XMLHttp", "Microsoft.XMLHttp"
                ];

                for (var i = 0; i < aVersions.length; i++)
                {
                    try
                    {
                        xmlhtp = new ActiveXObject(aVersions[i]);
                        return xmlhtp;
                    } catch (e) {
                    }
                }
            }
            else if (typeof XMLHttpRequest != 'undefined')
            {
                xmlhtp = new XMLHttpRequest();
                return xmlhtp;
            }
            throw new Error("XMLHttp object could be created.");

        }
    })();


</script>
