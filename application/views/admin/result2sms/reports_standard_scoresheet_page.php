<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php //echo base_url('resources/bootstrap/css/bootstrap.min.css');      ?>" rel="stylesheet">
        <link href="<?php //echo base_url('resources/css/styles.css');      ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .css-vertical{
                filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
                -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
                -ms-transform: rotate(-90.0deg);  /* IE9+ */
                -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
                -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                transform: rotate(-90.0deg);  /* Standard */
                white-space:nowrap;
            }
            .css-vertical-text {
                margin-bottom:15px;
                color:#333;
                border:0px solid red;
                writing-mode:tb-rl;
                -webkit-transform:rotate(-90deg);
                -moz-transform:rotate(-90deg);
                -o-transform: rotate(-90deg);
                -ms-transform: rotate(-90deg);
                transform: rotate(-90deg);
                white-space:nowrap;
                display:block;
                bottom:0;
                width:20px;
                height:20px;
                font-family: 'Trebuchet MS', Helvetica, sans-serif;
                font-size:11px;
                font-weight:bold;
                /*text-align:left;*/
                /*text-shadow: 0px 0px 1px #333;*/

                filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
            }
        </style>

    </head>
    <body>

        <div class="page-content">
            <div class="row">
                <div id="printdiv">
                    <?php
                    if(count($query_data)) {
                            $candidate_score = array();
                            //GATHER SCORES IN ONE PLACE
                            foreach ($query_data as $dt) {
                                $candidate_score[$dt->candidateid]['candidate_name'] = $dt->fullname;
                                $candidate_score[$dt->candidateid]['candidate_phone'] = $dt->phone_number;
                                $candidate_score[$dt->candidateid][$dt->subjectid]['total_score'] = $dt->tscore;
                                $candidate_score[$dt->candidateid][$dt->subjectid]['exam_score'] = $dt->exam_score;
                            }
                            //BUILD AND SEND MESSAGE
                            $sent_count = 0;
                            foreach($candidate_score as $data){
                                $message = '';
                                $message .= 'TPE 2016 Score for '.$data['candidate_name'];
                                $total_score = 0;
                                foreach ($allsubjects as $subjects) {
                                    $total_score += $data[$subjects->subjectid]['total_score'];
                                }
                                $message .= ' Total Score = '.$total_score .'<br/>';
                                //SEND MESSAGE HERE
                                echo $message."<br/>";
                                //echo 'Sending Result for : '.$data['candidate_name']."<br/>";
                                $sent_count++;
                            }
                            echo '<h1> Total Messages Sent : '.$sent_count.' </h1>';
                            $candidate_id = array_unique($candidate_ids);
                            $school_id = array_unique($school_ids);
                    }
                    else{
                        echo get_error("No Data Supplied");
                    }
                    ?>
                </div>
            </div>
        </div>

        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

    </body>
</html>
