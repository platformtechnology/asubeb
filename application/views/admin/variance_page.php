
<div class="row">
    <div class="col-md-6 panel-primary">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-edit"></i> Select Exam To Set Variance For</strong>
                </div>
            </div>
             <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?php echo form_open('', 'class="form-horizontal" role="form" target="_blank"'); ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Exam Type:</label>
                        <div class="col-sm-8">
                            <?php 
                              $select_options = array();
                              foreach ($exams as $exam) {
                                  $select_options[$exam->examid] = $exam->examname;
                              }
                              $select_options[''] = "--- Select Exam Type ---";
                              echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'class="form-control" required="required"');
                            ?>
                        </div>
                    </div>   
                    <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Exam Year:</label>
                           <div class="col-sm-8">
                              <?php 
                                    $already_selected_value = $this->input->post('examyear') ? $this->input->post('examyear') : $activeyear;
                                    $start_year = $activeyear;
                                     print '<select name="examyear" class="form-control">';
                                     print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>Select Exam Year</option>';
                                     for ($x = $start_year; $x <= $activeyear; $x++) {
                                         print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                                     }
                                     print '</select>';
                               ?>
                           </div>
                       </div>  
                    <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-forward"></i> Proceed To Registration</button>
                      </div>
                    </div>
                <?php echo form_close(); ?>
             </div>
    </div>
</div>