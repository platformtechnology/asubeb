
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-6 panel-primary">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-edit"></i> Perform Randomised Posting</strong>
                </div>
            </div>
             <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                 <?php if(validation_errors()) echo get_error(validation_errors()); ?>
                 <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Exam Type:</label>
                        <div class="col-sm-8">
                            <?php 
                              $select_options = array();
                              foreach ($exams as $exam) {
                                  $select_options[$exam->examid] = $exam->examname;
                              }
                              $select_options[''] = "--- Select Exam Type ---";
                              echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'class="form-control" id="examid" required="required"');
                            ?>
                        </div>
                    </div>   
                    <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Exam Year:</label>
                           <div class="col-sm-8">
                              <?php 
                                    $already_selected_value = $this->input->post('examyear') ? $this->input->post('examyear') : $activeyear;
                                    $start_year = $activeyear;
                                     print '<select name="examyear" id="examyear"  class="form-control">';
                                     print '<option value="" ' .($already_selected_value == '' ? 'selected="selected"' : ''). '>--- Select Exam Year ---</option>';
                                     for ($x = $start_year; $x <= $activeyear; $x++) {
                                         print '<option value="'.$x.'"'.($x == $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                                     }
                                     print '</select>';
                               ?>
                           </div>
                       </div> 

                    <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-8">
                          <button type="submit" onclick="return confirm('ARE YOU SURE YOU WISH TO PERFORM POSTING AUTOMATICALLY?')" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-random"></i> PERFORM POSTING</button>
                          <button type="button" onclick="return resetPosts()" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-refresh"></i> RESET POSTS</button>
                      </div>
                    </div>
                <?php echo form_close(); ?>
             </div>
    </div>
</div>
<script type="text/javascript">

    function resetPosts(){
        
        examid = document.getElementById("examid").value;
        examyear = document.getElementById("examyear").value;
        
         if(examid.trim() == "" || examyear.trim() == ""){
            alert("INVALID DATA - EXAM AND YEAR MUST BE SELECTED");
            return false;
        }

        var url_to_post = "<?= site_url('admin/randomisepost/reset_posts?examid='); ?>";
        url_to_post = url_to_post + examid + "&examyear=" + examyear;
      
        makeRequestx(url_to_post);

    }
  
 function makeRequestx(url) {
         
        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
                
         httpRequest.onreadystatechange = function(){
           if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                   var obj = JSON.parse(httpRequest.response);
                   //alert(obj.success);
                   if(obj.err == "1"){
                      
                      window.location = "<?= site_url('admin/randomisepost') ?>";
                      return true;
                   }
                  
                } else {
                    alert(httpRequest.statusText);
                    return false;
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
        httpRequest.send();
    }
//    
    function getHttpObject(){
        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
            httpRequest = new XMLHttpRequest();
            return httpRequest;
        } else if (window.ActiveXObject) { // IE
            try {
                httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                return httpRequest;
            } 
            catch (e) {
                try {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    return httpRequest;
                } 
                catch (e) {}
            }
        }

        return;
    }
    
</script>