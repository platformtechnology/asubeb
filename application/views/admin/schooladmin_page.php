<div class="row">

        <div class="col-md-12 panel-primary">
            <div class="content-box-header panel-heading">
                        <div class="panel-title">All School Login Details</div>
             </div>
            
            <div class="content-box-large box-with-header">
            <div class="panel-body">
                <?php if($this->session->flashdata('updated_msg')) echo get_success($this->session->flashdata('updated_msg')); ?>
                <table class="table table-bordered table-condensed" id="example">
                  <thead>
                    <tr>
                      <th>#</th>
<!--                      <th>Edc</th>-->
                      <th>LGA</th>
                      <th>School Name</th>
                      <th>School Code</th>
                      <th>Pass Code</th>
                      <th>Phone</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                        if(count($schooladmins)): 
                            $sn = 0;
                            foreach ($schooladmins as $admins):
                    ?>
                    <tr>
                        <td><?= ++$sn; ?></td>
<!--                        <td><?= strtoupper($admins->edcname); ?></td>-->
                        <td><?= $admins->lganame; ?></td>
                        <td><?= $admins->schoolname; ?></td>
                        <td><?= $admins->schoolcode; ?></td>
                        <td>2480</td>
                        <td><?= $admins->phone; ?></td>
                    </tr> 
                    <?php endforeach;?>
                    <?php endif; ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>