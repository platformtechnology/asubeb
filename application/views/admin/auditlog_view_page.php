 <div class="row">    
            <div class="col-lg-8">
                <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title">
                             <h3><i class="glyphicon glyphicon-th"></i> <strong>LOG DETAILS</strong></h3>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="printDIV2">
                             <table width="100%" class="table table-bordered">
                                  <tr>
                                    <td valign="top"><strong>User's Name:</strong></td>
                                    <td align="left" valign="top">
                                        <?= anchor(site_url('admin/users/save/'.$audit->userid), ucwords($audit->fullname)); ?>
                                      </td>
                                  </tr>
                                  <tr>
                                    <td valign="top"><strong>User's Email:</strong></td>
                                    <td align="left" valign="top">
                                        <?= $audit->email; ?>
                                      </td>
                                  </tr>
                                  <tr>
                                    <td valign="top"><strong>User's Role:</strong></td>
                                    <td align="left" valign="top">
                                        <?= str_replace('^', ' ^ ', $audit->role); ?>
                                      </td>
                                  </tr>
                                  <tr>
                                    <td valign="top"><strong>Action Performed:</strong></td>
                                    <td align="left" valign="top">
                                        <?= $audit->message; ?>
                                      </td>
                                  </tr>
                                  <tr>
                                    <td valign="top"><strong>Action Type:</strong></td>
                                    <td align="left" valign="top">
                                        <?= $audit->actiontype; ?>
                                      </td>
                                  </tr>
                                  <tr>
                                    <td valign="top"><strong>Details:</strong></td>
                                    <td align="left" valign="top">
                                        <?= $audit->details; ?>
                                      </td>
                                  </tr>
                                  <tr>
                                    <td valign="top"><strong>Date Performed</strong></td>
                                    <td align="left" valign="top">
                                        <?= $audit->datecreated; ?>
                                      </td>
                                  </tr>
                                </table>
                          </div>
                        <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <button type="button" onclick="printDiv('printDIV2')" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-print"></i> Print</button>
                                        <a href="<?= site_url('admin/auditlog');?>" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-share"></i> Back</a>
                                    </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script lang="javascript" type="text/javascript">
            function printDiv(divID) {
                //Get the HTML of div
                var divElements = document.getElementById(divID).innerHTML;
                //Get the HTML of whole page
                var oldPage = document.body.innerHTML;

                //Reset the page's HTML with div's HTML only
                document.body.innerHTML =
                  "<html><head></head><body>" +
                  divElements + "</body>";

                //Print Page
                window.print();

                //Restore orignal HTML
                document.body.innerHTML = oldPage;
            }
            
    </script>