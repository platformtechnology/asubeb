
<div class="row">
    <div class="col-md-5 panel-primary">

            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <?php echo empty($admin_detail->fullname) ? '<i class="glyphicon glyphicon-plus"></i> Add Users' : '<i class="glyphicon glyphicon-edit"></i> Edit User'; ?>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                The Default Password for the User is <strong style="color: crimson;"><?=  config_item('default_edcadmin_pass')?></strong>
                    <br /><br />
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Fullname:</label>
                        <div class="col-sm-9">
                            <input type="text" name="fullname" value="<?php echo set_value('fullname', $admin_detail->fullname); ?>" class="form-control" id="inputEmail3" placeholder="Fullname" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Email:</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" value="<?php echo set_value('email', $admin_detail->email); ?>" class="form-control" id="inputEmail3" placeholder="Email" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Phone:</label>
                        <div class="col-sm-9">
                            <input type="text" name="phone" value="<?php echo set_value('phone', $admin_detail->phone); ?>" class="form-control" id="inputEmail3" placeholder="Phone" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Priviledges:</label>
                        <div class="col-sm-9">
                            <?php
//                              $select_options = array();
//                              $select_options['edcadmin'] = "Super Administrator";
//                              $select_options['edcoperator'] = "Edc Operators";
//                              echo form_dropdown('role', $select_options, $this->input->post('role') ? $this->input->post('role') : $admin_detail->role, 'class="form-control" required="required"');
                            ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                        $priviledges = array();
                                        if(!empty($admin_detail->fullname)){
                                            $role = $admin_detail->role;
                                            $priviledges = explode("^", $role);
                                        }
                                    ?>
                                    <input <?= (in_array('Super_Administrator', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" onchange="toggle(this)" name="Super_Administrator" id="Super_Administrator" value="Super_Administrator"> <strong>Super Administrator (Select All)</strong> <br/>
                                </div>
                            </div>

                            <br/>
                            <div class="row">
                                <div class="col-sm-6">

                                    <input <?= (in_array('Edc_Setup', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="Edc_Setup"> <strong>Edc Setup</strong><br/>
                                    <input <?= (in_array('Report_Viewing', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="Report_Viewing"> <strong>Report Viewing</strong><br/>
                                    <input <?= (in_array('User_Mgt', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="User_Mgt"> <strong>User Mgt</strong><br/>
                                    <input <?= (in_array('Grading', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="Grading"> <strong>Grading</strong><br/>

                                    <!--<input <?= (in_array('Active_Year', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="Active_Year"> <strong>Active Year</strong><br/>-->
                                    <!--<input <?= (in_array('Reg_Deadline', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="Reg_Deadline"> <strong>Reg. Deadline</strong><br/>-->
                                    <?php if(!config_item('isonline')){ ?>
                                            <input <?= (in_array('Database_Sync', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="Database_Sync"> <strong>Synchronisation</strong>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6">
                                    <input <?= (in_array('Statistics', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="Statistics"> <strong>Statistics</strong><br/>
                                    <input <?= (in_array('Posting', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="Posting"> <strong>Posting</strong><br/>
                                    <input <?= (in_array('Registration', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="Registration"> <strong>Registration</strong><br/>
                                    <input <?= (in_array('Arb_School', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="Arb_School"> <strong>Arbitrary School</strong> <br/>
                                    <input <?= (in_array('Asubeb', $priviledges) ? 'checked="true"' : ''); ?> type="checkbox" name="role[]" onchange="check_all(this)" value="Asubeb"> <strong>Asubeb</strong><br/>
                                </div>
                            </div>
                        </div>
                    </div>
                <br/>
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                        <a href="<?= site_url('admin/users/save');?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                      </div>
                    </div>
                    <?= form_close(); ?>
                </div>

        </div>

        <div class="col-md-7 panel-primary">

            <div class="content-box-header panel-heading">
                <div class="panel-title">All Users</div>
            </div>
            <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('updated_msg')) echo get_success($this->session->flashdata('updated_msg')); ?>
                <table class="table table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th>Fullname</th>
                      <th>Email</th>
                      <th>Priviledges</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                        if(count($administrators)):
                            foreach ($administrators as $admin):
                    ?>
                    <tr>
                        <td><?php echo anchor(site_url('admin/users/save/'.$admin->userid), $admin->fullname) ?></td>
                        <td><?= $admin->email; ?></td>
                        <td><?= str_replace('^', ' ^ ', $admin->role); ?></td>
                        <td>
                            <?= get_del_btn(site_url('admin/users/delete/'.$admin->userid)); ?>
                            <?= anchor(site_url('admin/users/reset/'.$admin->userid), ' <i class="glyphicon glyphicon-refresh"></i>',  array(
                                            'title' => 'Reset User\'s Password',
                                            'onclick' => "return confirm('You are about to reset the password of this user to the default. Are you sure you want to proceed?');"
                                            )); ?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    <?php endif; ?>
                  </tbody>
                </table>
            </div>

    </div>
</div>

<script type="text/javascript">
    function toggle(source) {
        checkboxes = document.getElementsByName('role[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
          checkboxes[i].checked = source.checked;
        }
    }

    function check_all(source) {
        if(source.checked === false){
           administrator = document.getElementById('Super_Administrator');
           administrator.checked = source.checked;
        }
        else{
            checkboxes = document.getElementsByName('role[]');
            var count = 0;
            for(var i=0, n=checkboxes.length;i<n;i++) {
              if(checkboxes[i].checked === true){
                  ++count;
              }
            }

            if(count == checkboxes.length){
                administrator = document.getElementById('Super_Administrator');
                administrator.checked = true;
            }
        }
    }
</script>