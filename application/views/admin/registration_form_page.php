
        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
            REGISTRATION FOR <br/>THE <?= strtoupper($exam_detail->examdesc. ' ('.$exam_detail->examname.') ' . $exam_year) ;?> 
        </h4>
        <hr/>

<div class="row">
    <div class="col-md-12 panel-primary">
        <div class="content-box-header panel-heading">
            <div class="panel-title">
                <strong><i class="glyphicon glyphicon-edit"></i> Register Students</strong>
            </div>
        </div>
        
        <div class="content-box-large box-with-header">
            <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
            <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>

            <?= form_open_multipart('', 'class="form-horizontal"'); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Local Govt Area:</label>
                            <div class="col-sm-8">
                                 <?php
                                    $select_options = array();
                                    $select_options[''] = "...:::Select LGA:::...";
            //                        if(!empty($candidate_detail->firstname)){
                                          foreach ($lgs as $lg) {
                                              $select_options[$lg->lgaid] = $lg->lganame;
                                          }
                                    //}
                                    echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : $candidate_detail->lgaid, 'id="lgaid" class="form-control" required="required" ' . (empty($candidate_detail->firstname) ?  '' : 'disabled="disabled"')); 
                                ?>
                            </div>
                        </div>   
                        
                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Present School:</label>
                            <div class="col-sm-8">
                                 <?php
                                    $select_options = array();
                                    $select_options[''] = "Select School";
                                    if(!empty($candidate_detail->firstname)){
                                          foreach ($schools as $school) {
                                              $select_options[$school->schoolid] = $school->schoolname;
                                          }
                                    }
                                    echo form_dropdown('schoolid', $select_options, $candidate_detail->schoolid, 'id="schoolid" class="form-control" required="required" ' . (empty($candidate_detail->firstname) ? '' : ' disabled="disabled"')); 
                                ?>
                            </div>
                        </div> 
                        
                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Surname:</label>
                            <div class="col-sm-8">
                                 <input type="text" name="firstname" value="<?php echo set_value('firstname', $candidate_detail->firstname); ?>" placeholder="Enter Your Surname" class="form-control" required="required" />
                            </div>
                        </div> 
                        
                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Othernames:</label>
                            <div class="col-sm-8">
                                 <input type="text" name="othernames" value="<?php echo set_value('othernames', $candidate_detail->othernames); ?>" placeholder="Enter Your Other Names" class="form-control" required="required" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Subjects:</label>
                            <div class="col-sm-8">
                                 <?php 
                                    $count = 0;
                                    foreach ($subjects as $subject) {
                                        $subjBuffer = array();

                                        if(isset($registered_subjects)){

                                            foreach ($registered_subjects as $reg_subject) {
                                                if($reg_subject->subjectid == $subject->subjectid){
                                                    echo '<input name="subject[]" type="checkbox" value="'.$subject->subjectid.'" ' . (($subject->iscompulsory == 1) ? 'disabled="disabled"' : '') . ' checked="checked" /><strong>' . ucfirst($subject->subjectname) . '</strong> &nbsp;';
                                                    $subjBuffer[] = $reg_subject->subjectid;

                                                }
                                            }
                                        }
                                        else{
                                            echo '<input name="subject[]" type="checkbox" value="'.$subject->subjectid.'" ' . (($subject->iscompulsory == 1) ? 'checked="checked" disabled="disabled"' : '') . ' /><strong>' . ucfirst($subject->subjectname) . '</strong> &nbsp;';
                                        }

                                        if(isset($registered_subjects) && !in_array($subject->subjectid, $subjBuffer)){

                                            echo '<input name="subject[]" type="checkbox" value="'.$subject->subjectid.'" ' . (($subject->iscompulsory == 1) ? 'checked="checked" disabled="disabled"' : '') . ' /><strong>' . ucfirst($subject->subjectname) . '</strong> &nbsp;';
                                        }

                                        ++$count;
                                        if(($count % 2) === 0) echo '<br/><br/>';

                                    }
                                   ?>
                            </div>
                        </div>
                        
                          <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Gender:</label>
                                <div class="col-sm-8">
                                     <?php
                                        $select_options = array();
                                        $select_options['M'] = "Male";
                                        $select_options['F'] = "Female";
                                        echo form_dropdown('gender', $select_options, $this->input->post('gender') ? $this->input->post('gender') : $candidate_detail->gender, 'id="gender" class="form-control" required="required" '); 
                                    ?>
                                </div>
                            </div>
                        
                        <?php if($exam_detail->hasposting == 1): ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">FirstChoice:</label>
                                <div class="col-sm-6">
                                    <?php
                                        $select_options = array();
                                        $select_options[''] = "...:::Select 1st Choice:::...";
                                          foreach ($choice1 as $choice) {
                                              $select_options[$choice->schoolid] = $choice->schoolname;
                                          }
                                        echo form_dropdown('firstchoice', $select_options, $this->input->post('firstchoice') ? $this->input->post('firstchoice') : $candidate_detail->firstchoice, 'id="firstchoice" class="form-control" required="required"'); 
                                    ?>
                                    
                                </div>
                                <label class="col-sm-2 control-label"><strong>/ <span id="schoolcode1" style="color: crimson">No Code</span></strong></label>
                                
                            </div>
                                                
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">SecondChoice:</label>
                                <div class="col-sm-6">
                                   <?php
                                        $select_options = array();
                                        $select_options[''] = "...:::Select 2nd Choice:::...";
                                          foreach ($choice2 as $choice) {
                                              $select_options[$choice->schoolid] = $choice->schoolname;
                                          }
                                        echo form_dropdown('secondchoice', $select_options, $this->input->post('secondchoice') ? $this->input->post('secondchoice') : $candidate_detail->secondchoice, 'id="secondchoice" class="form-control" required="required"'); 
                                    ?>
                                </div>
                                <label class="col-sm-2 control-label"><strong>/ <span id="schoolcode2" style="color: crimson">No Code</span></strong></label>
                            </div>
                        <?php endif; ?>
                         
                    </div>
                    
                    <div class="col-md-6">
                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Date of birth:</label>
                            <div class="col-sm-8">
                                 <div class="bfh-datepicker" data-format="y-m-d" data-date="<?php echo set_value('dob', $candidate_detail->dob); ?>" data-name="dob" data-date="2004-01-01"></div>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Phone:</label>
                            <div class="col-sm-8">
                                 <input type="text" name="phone" value="<?php echo set_value('phone', $candidate_detail->phone); ?>" class="form-control"  required="required"/>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Passport:</label>
                            <div class="col-sm-8">
                                <?php 
                                    $passport = get_img('no_image.jpg');
                                    if(file_exists('./resources/images/passports/'.$candidate_detail->candidateid.'.jpg')){
                                       $passport = base_url('resources/images/passports/'.$candidate_detail->candidateid.'.jpg'); 
                                    }
                                ?>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 150px; height: 112px;"><img src="<?= empty($candidate_detail->firstname) ? get_img('no_image.jpg') : $passport; ?>" alt="Screen Shot" />
                                    </div>
                                     <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                     <div>

                                         <span class="btn btn-file btn-default"><span class="fileupload-new"><input type="file" name="passport"/></span><span class="fileupload-exists">Change</span><input type="file" /></span>
                                         <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                                     </div>
                                  </div>
                                <span style="color: crimson;">JPG or PNG file not more than <strong>500Kb</strong></span>
                            </div>
                        </div>
                                                
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                   <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-saved"></i> Save & Proceed</button>
                                   <a href="<?= site_url('admin/registration'); ?>" class="btn btn-warning"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
                            </div>
                        </div>
                        <br/>
                         <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <a href="<?= site_url('admin/registration/row/'.$exam_detail->examid.'/'.$exam_year); ?>" ><i class="glyphicon glyphicon-list"></i> Register Candidates In a Rowise Manner</a>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
               
           <?= form_close(); ?>
        </div>
</div>
</div>
  <script type="text/javascript">
     (function() {
        var httpRequest;

        lgaddl = document.getElementById("lgaid");
        lgaddl.onchange = function() { 
            var target_url1 = "<?= site_url('admin/registration/school_ajaxdrop?issec='.(($exam_detail->hassecschool == 1) ? '1' : '0').'&lgaid='); ?>";
            //target_url1 = target_url1.replace(/\.[^/.]+$/, ""); //remove .html extension
            makeRequest( target_url1 + lgaddl.value, 'schoolid'); 
            
        };

        firstchoiceddl = document.getElementById("firstchoice");
        firstchoiceddl.onchange = function() { 
            var target_url2 = "<?= site_url('admin/registration/code_ajaxdrop?schoolid='); ?>";
            //target_url2 = target_url2.replace(/\.[^/.]+$/, ""); //remove .html extension
            makeCodeRequest( target_url2 + firstchoiceddl.value, 'schoolcode1'); 
            
        };
        
        secondchoiceddl = document.getElementById("secondchoice");
        secondchoiceddl.onchange = function() { 
            var target_url3 = "<?= site_url('admin/registration/code_ajaxdrop?schoolid='); ?>";
            //target_url3 = target_url3.replace(/\.[^/.]+$/, ""); //remove .html extension
            makeCodeRequest( target_url3 + secondchoiceddl.value, 'schoolcode2'); 
            
        };
        
         function makeCodeRequest(url, targetid) {
            if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                httpRequest = new XMLHttpRequest();
            } else if (window.ActiveXObject) { // IE
                try {
                    httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                } 
                catch (e) {
                    try {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    } 
                    catch (e) {}
                }
            }

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = function(){
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        //var data = JSON.parse(httpRequest.response);
                        document.getElementById(targetid).innerHTML = httpRequest.response;
                        
                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
            httpRequest.send();
        }


        function makeRequest(url, targetid) {
            if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                httpRequest = new XMLHttpRequest();
            } else if (window.ActiveXObject) { // IE
                try {
                    httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                } 
                catch (e) {
                    try {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    } 
                    catch (e) {}
                }
            }

            if (!httpRequest) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            httpRequest.onreadystatechange = function(){
                if (httpRequest.readyState === 4) {
                    if (httpRequest.status === 200) {
                        var data = JSON.parse(httpRequest.response);
                        var select = document.getElementById(targetid);
                        if(emptySelect(select)){
                            var el = document.createElement("option");
                                    el.textContent = '....Select....';
                                    el.value = '';
                                    select.appendChild(el);
                                    
                            for (var i = 0; i < data.lgas.length; i++){
                                var el = document.createElement("option");
                                    el.textContent = data.lgas[i].lganame;
                                    el.value = data.lgas[i].lgaid;
                                    select.appendChild(el);
                            }
                        }
                    } else {
                        alert('There was a problem with the request.');
                    }
                }
            };
            httpRequest.open('GET', url);
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
            httpRequest.send();
        }

        function emptySelect(select_object){
            var i;
            for(i=select_object.options.length-1;i>=0;i--){
                select_object.remove(i);
            }
            return true;
        }
    })();


    </script>