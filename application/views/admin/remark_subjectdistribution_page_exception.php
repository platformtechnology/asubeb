
<h4 style="text-align: center; color: #0077b3; font-weight: bold">
    SUBJECT DISTRIBUTED REMARK SETTING FOR <span style="font-size: 26px"><?= strtoupper($this->registration_model->getExamName_From_Id($examid)); ?> </span> <?= $examyear; ?> 
    <!--        <br/><br/>
            <a href="<?= site_url('admin/remarks/scoredistribution/' . $examid . '/' . $examyear) ?>" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-arrow-right"></i> Go to SCORE Distributed Remark Setting</a>-->
</h4>
<hr/>

<div class="row">

    <div class="col-md-12 panel-primary">
        <div class="content-box-header panel-heading">
            <div class="panel-title">
                <strong><i class="glyphicon glyphicon-edit"></i> <?= isset($edit) ? 'Edit' : 'Set'; ?> Remarks</strong>
            </div>
        </div>
        <div class="content-box-large box-with-header">
            <?php if ($this->session->flashdata('msg') || isset($msg)) echo get_success(isset($msg) ? $msg : ($this->session->flashdata('msg') ? $this->session->flashdata('msg') : '' ) ); ?>
            <?php if (validation_errors() || isset($errors)) echo get_error(validation_errors() || $errors); ?>
            <?php echo form_open('admin/remarks/scoredistributionexception', 'class="form-horizontal" role="form"'); ?>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Minimum Score:</label>
                <div class="col-sm-8">
                    <input type="text" name="minimum" value="<?= isset($edit) ? $subj->minimum : set_value('minimum'); ?>" class="form-control" placeholder="Enter Minimum Score" required="required">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Maximum Score:</label>
                <div class="col-sm-8">
                    <input type="text" name="maximum" value="<?= isset($edit) ? $subj->maximum : set_value('maximum'); ?>" class="form-control" placeholder="Enter Maximum Score" required="required">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Grade:</label>
                <div class="col-sm-8">
                    <input type="text" name="grade" value="<?= isset($edit) ? $subj->grade : set_value('grade'); ?>" class="form-control" placeholder="Enter Grade Equivalent" required="required">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Remark:</label>
                <div class="col-sm-8">
                    <input type="text" name="remark" value="<?= isset($edit) ? $subj->remark : set_value('remark'); ?>" class="form-control" placeholder="Enter Remark" required="required">
                    <input type="hidden" name="edcid" value="<?= isset($edit) ? $subj->edcid : $edcid; ?>" class="form-control"> 
                    <input type="hidden" name="remarkid" value="<?= isset($edit) ? $subj->remarkid : ''; ?>" class="form-control">
                    <input type="hidden" name="examid" value="<?= isset($edit) ? $subj->examid : $examid; ?>" class="form-control">
                    <input type="hidden" name="examyear" value="<?= isset($edit) ? $subj->examyear : $examyear; ?>" class="form-control">
                    <?= isset($edit) ? ' <input type="hidden" name="edit" value="edit">' : ''; ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-send"></i> <?= isset($edit) ? 'Update' : 'Submit'; ?> </button>
                    <a href="<?= site_url('admin/remarks'); ?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>

    <div class="col-md-12 panel-primary">
        <div class="content-box-header panel-heading">
            <div class="panel-title">
                <strong><i class="glyphicon glyphicon-trophy"></i> Remarks</strong>
            </div>
        </div>
        <div class="content-box-large box-with-header">
            <div id = "printArea">
            <table class="table" border = '1' width = "100%">

                <thead>
                <th> 
                    S/N
                </th>
                <th> 
                    Min Score
                </th>
                <th> 
                    Max Score
                </th>
                <th> 
                    Grade
                </th>
                <th> 
                    Remark
                </th>
                <th> 
                    Total 
                </th>
                <th> 
                    Percentage
                </th>

                <th> 
                    Action
                </th>

                </thead>


                <tbody>

                    <?php
                    $counter = 1;
                    $total_percentage = 0;
                    foreach ($subjects as $value):
                        $data = $this->remarks_model->get_exception_stat_per_remark($examid, $examyear, $value->minimum, $value->maximum);
                        ?>
                        <tr>

                            <td>
                                <?= $counter ?>
                            </td>
                            <td>
                                <?= $value->minimum; ?>
                            </td> 
                            <td>
                                <?= $value->maximum; ?>
                            </td>
                            <td>
                                <?= $value->grade; ?>
                            </td>

                            <td>
                                <?= $value->remark; ?>
                            </td>
                            <td>
                                <?php echo $data['students_count_per_remark']; ?>
                            </td>
                            <td>
                                <?php echo $data['percentage_count_per_remark']; ?> %
                            </td>
                            <td>
                                <?= get_edit_btn(site_url('admin/remarks/edit_remark_subjectdistribution_page_exception/' . $value->remarkid)); ?> &nbsp;&nbsp;


                                <?= get_del_btn(site_url('admin/remarks/delete_remark_subjectdistribution_page_exception/' . $value->remarkid . "/" . $value->examid . "/" . $value->examyear)); ?> 
                            </td>
                        </tr>
                        <?php
                       $total_percentage+=$data['percentage_count_per_remark'];
                        $counter++;
                    endforeach;
                    ?>
                    <tr> <td> </td> <td> </td> <td> </td>                             <td></td>
                        <td></td>
                        <td> <?php echo $data['total_students']; ?> </td> 
                        <td> <?php echo round($total_percentage) ?> %</td> 
                    </tr>
                </tbody>
            </table>
            
        </div>
        <button class = "btn btn-primary" onclick = "PrintDiv()"> Print</button>
        </div>
    </div>
</div>


          <script type = "text/javascript">
function PrintDiv(){
    alert('Ready to Print');
var oldData = document.body.innerHTML;
var printdata = document.getElementById('printArea').innerHTML;
document.write('<html> <head> </head><body>');
document.write(printdata);
document.write('</body></head></html>');
window.print();
window.location = "<?php echo current_url(); ?>";


    
}
</script>