
<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
             <div class="panel-body">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                        <div class="row">
                  
                            <div class="col-md-7">

                                   <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-5 control-label">Exam Type:</label>
                                          <div class="col-sm-7">
                                              <?php 
                                                $select_options = array();
                                                  foreach ($exams as $exam) {
                                                      $select_options[$exam->examid] = $exam->examname;
                                                  }
                                                $select_options[''] = "..:: Select Exam ::..";
                                                echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'id="examid" class="form-control" required="required"');
                                              ?>
                                          </div>
                                      </div>
                                </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                  <div class="col-sm-9">
                                    <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-search"></i> Search</button>
                                    <a href="" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-refresh"></i> Refresh</a>
                                  </div>
                                </div>
                           </div>
                            
                         </div>

                    <?= form_close(); ?>                
              </div>
                     
                     
        </div>
    </div>
</div>

<?php if(isset($subjects)): ?>
<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="row">
                <div class="col-md-12">
                    
                        <h5 style="text-align: center; color: #0077b3; font-weight: bold">
                            SETUP MAXIMUM SCORE FOR CA, PRACTICAL AND EXAM FOR SUBJECTS OF  <?= strtoupper($this->registration_model->getExamName_From_Id($posted_values['examid'])) ;?> 
                        </h5>
                   
                </div>
             </div>
            <div class="panel-body">
                <?= form_open('', 'class="form-horizontal" role="form"'); ?> 
                    
                    <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>

                    <table width="100%" class="table table-bordered table-condensed table-hover" id="gradeTable">
                        <thead>
                            <tr>
                                <th>Sn</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>Max. CA Score</th>
                                <th>Max. EXAM Score</th>
                                <th>Max. PRACTICAL Score</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                 $sn = 0;
                                 foreach ($subjects as $subject):
                                    $maxscoredata = $this->scores_margin_model->getMaxScores($subject->subjectid);
                                    if($maxscoredata == null) continue;
                                    
                            ?>
                            <tr>
                                <td><?= ++$sn; ?></td>
                                
                                <td style="font-weight: bold;"><?= ucwords($subject->subjectname); ?></td>
                                <td>&nbsp;</td>
                                <td title="Maximum CA Score for <?= ucwords($subject->subjectname); ?>">
                                    <?php if($exams_data->hasca == 1) echo '<input class="gradefield" id="maxca'.$sn.'" onKeyUp="collectCaPost(\''.$sn.'\')" type="text" name="maxca'.$sn.'" value="'.$maxscoredata->maxca.'" /> <span id="causer-result'.$sn.'"></span>'; 
                                    else echo '<span style="font-size: 10px; color: crimson;">CA Unsupported</span>'; ?>
                                </td>
                                <td title="Maximum EXAM Score for <?= ucwords($subject->subjectname); ?>"><?= '<input class="gradefield" id="maxexam'.$sn.'" onKeyUp="collectExamPost(\''.$sn.'\')" type="text" name="maxexam'.$sn.'" value="'.$maxscoredata->maxexam.'" /> <span id="examuser-result'.$sn.'"></span>'; ?></td>
                                <td>
                                    <?php 
                                    if($exams_data->haspractical == 1){
                                        if($subject->haspractical == 1)
                                            echo '<input  title="Maximum PRACTICAL Score for '. ucwords($subject->subjectname) .'" class="gradefield" id="maxpractical'.$sn.'" onKeyUp="collectPracticalPost(\''.$sn.'\')" type="text" name="maxpractical'.$sn.'" value="'.$maxscoredata->maxpractical.'" /> <span id="practicaluser-result'.$sn.'"></span>';
                                        else echo '<span style="font-size: 10px; color: crimson;">No Practical</span>';
                                    }else echo '<span style="font-size: 10px; color: crimson;">No Practical</span>';
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <?php echo '<input type="hidden" id="subjectid'.$sn.'" name="subjectid'.$sn.'" value="'.$subject->subjectid.'" />'; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <br/><br/>
                    
                 <?= form_close(); ?>      
            </div>
        </div>
    </div>   
</div>


<script type="text/javascript">
 
    function collectCaPost(sn){
      maxca = document.getElementById("maxca"+sn).value;  
      subjectid = document.getElementById("subjectid"+sn).value;
      
      //maxexam = document.getElementById("maxexam"+sn).value;
      //maxpractical = document.getElementById("maxpractical"+sn);
      
        
             if(isNaN(maxca) || (maxca.trim() == "")) {
                   document.getElementById("causer-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                   return;
             }else if(maxca < 0){
                 document.getElementById("causer-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                 return;
             }
//             else if(maxca > 70){
//                 alert("CA CANNOT BE GREATER THAN 70");
//                 document.getElementById("causer-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
//                 return;
//             }

//         if(checkOverallSum(maxca, maxexam, maxpractical, "causer-result"+sn)){
//             var url_to_post = "<?= site_url('admin/ca_exam/update_maxca_score?ca_score='); ?>";
//             url_to_post = url_to_post + maxca + "&subjectid=" + subjectid;
//
//             document.getElementById("causer-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
//
//             makeRequestx(url_to_post, "causer-result"+sn);
//         }
             var url_to_post = "<?= site_url('admin/ca_exam/update_maxca_score?ca_score='); ?>";
             url_to_post = url_to_post + maxca + "&subjectid=" + subjectid;

             document.getElementById("causer-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';

             makeRequestx(url_to_post, "causer-result"+sn);
    }
    
    function collectExamPost(sn){
        
        maxexam = document.getElementById("maxexam"+sn).value;  
        subjectid = document.getElementById("subjectid"+sn).value;
        
        //maxca = document.getElementById("maxca"+sn).value;
        //maxpractical = document.getElementById("maxpractical"+sn);

        
            if(isNaN(maxexam) || (maxexam.trim() == "")) {
                  document.getElementById("examuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                  return;
            }else if(maxexam < 0){
                document.getElementById("examuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                return;
            }
            else if(maxexam > 100){
                alert("EXAM CANNOT BE GREATER THAN 100");
                document.getElementById("examuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                return;
            }
            
//        if(checkOverallSum(maxca, maxexam, maxpractical, "examuser-result"+sn)){
//            var url_to_post = "<?= site_url('admin/ca_exam/update_maxexam_score?exam_score='); ?>";
//            url_to_post = url_to_post + maxexam + "&subjectid=" + subjectid;
//
//            document.getElementById("examuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
//
//            makeRequestx(url_to_post, "examuser-result"+sn);
//        }

            var url_to_post = "<?= site_url('admin/ca_exam/update_maxexam_score?exam_score='); ?>";
            url_to_post = url_to_post + maxexam + "&subjectid=" + subjectid;

            document.getElementById("examuser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';

            makeRequestx(url_to_post, "examuser-result"+sn);
    }
    
    function collectPracticalPost(sn){
        
        maxpractical = document.getElementById("maxpractical"+sn).value;  
        subjectid = document.getElementById("subjectid"+sn).value;
        
        //maxca = document.getElementById("maxca"+sn).value;
        //maxexam = document.getElementById("maxexam"+sn).value;

            
            if(isNaN(maxpractical) || (maxpractical.trim() == "")) {
                  document.getElementById("practicaluser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                  return;
            }else if(maxpractical < 0){
                document.getElementById("practicaluser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                return;
            }
            else if(maxpractical > 100){
                alert("PRACTICAL SCORE CANNOT BE GREATER THAN 100");
                document.getElementById("practicaluser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                return;
            }
        
//        if(checkOverallSum(maxca, maxexam,  document.getElementById("maxpractical"+sn), "practicaluser-result"+sn)){
//            var url_to_post = "<?= site_url('admin/ca_exam/update_maxpractical_score?prac_score='); ?>";
//            url_to_post = url_to_post + maxpractical + "&subjectid=" + subjectid;
//
//            document.getElementById("practicaluser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
//
//            makeRequestx(url_to_post, "practicaluser-result"+sn);
//        } 

         var url_to_post = "<?= site_url('admin/ca_exam/update_maxpractical_score?prac_score='); ?>";
         url_to_post = url_to_post + maxpractical + "&subjectid=" + subjectid;
         document.getElementById("practicaluser-result"+sn).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
         makeRequestx(url_to_post, "practicaluser-result"+sn);
    }
    
    function makeRequestx(url, imgId) {
         
        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
                
         httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                    var obj = JSON.parse(httpRequest.response);
                    if(obj.success == 1){
                        document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('available.png'); ?>"+'" />';
                    }else{
                        alert('Could Not Update');
                        document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                    }
                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById(imgId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
        httpRequest.send();
    }
//    
    function getHttpObject(){
        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
            httpRequest = new XMLHttpRequest();
            return httpRequest;
        } else if (window.ActiveXObject) { // IE
            try {
                httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                return httpRequest;
            } 
            catch (e) {
                try {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    return httpRequest;
                } 
                catch (e) {}
            }
        }

        return;
    }
    
    function checkOverallSum(ca, exam, practicalObj, notificationID){
        total = 0;
        if(practicalObj == null){
            total =  parseInt(exam) + parseInt(ca);
        }else{
              total =  parseInt(exam) + parseInt(ca) + parseInt(practicalObj.value);
        }
        //alert(total); exit;
        if(total > 100){
             alert("THE OVERALL MAXIMUM CANNOT BE GREATER THAN 100 - KINDLY AJUST THE SCORES ACCORDINGLY!");
             document.getElementById(notificationID).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
             return false;
        }
        else{
             return true;
        }
    }
</script>


<?php endif; ?>


