    <h4 style="text-align: center; color: #0077b3; font-weight: bold">
       CRITERIA SETTING FOR <span style="font-size: 26px"><?= strtoupper($this->registration_model->getExamName_From_Id($examid)); ?> </span> <?= $examyear; ?> 
    </h4>
    <hr/>
    
<div class="row">
    <?= $this->session->flashdata('error') ? get_error($this->session->flashdata('error')) : '' ?>
    <div class="col-md-6 panel-primary">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-cog"></i> SET OPTIONAL SUBJECTS CRITERIA</strong>
                </div>
            </div>
             <div class="content-box-large box-with-header">
                <?php if(isset($error)) echo get_error($error); ?>
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>

                 <hr/>
                        <div class="form-group">
                            <label class="col-sm-offset-1 col-sm-10 control-label">Select OPTIONAL SUBJECTS that must reach Minimum Score</label>
                        </div>
                 
                        <div class="form-group">
                            <label class="col-sm-offset-1 col-sm-4 control-label">SUBJECTS</label>
                            <label class="col-sm-4 control-label">PASS SCORE</label>
                        </div>
                 
                        <div class="form-group">
                           
                                  <?php 
                                    $count = 0;
                                    $optionalsid = '';
                                    
                                    foreach ($subjects as $subject) {
                                        $subjBuffer = array();
                                        
                                        if(!empty($optionals)){
                                                    
                                            foreach ($optionals as $optional) {
                                                if($subject->subjectid == $optional->subjectid){
                                                    echo ' <div class="col-sm-offset-2 col-sm-4">';
                                                    echo '<input name="subjectid[]" type="checkbox" value="'.$optional->subjectid.'" checked="true"/><strong> ' . ucfirst($subject->subjectname) . '</strong> &nbsp;';
                                                    echo ' </div>';
                                                    echo ' <div class="col-sm-4">';
                                                    echo '<input type="text" class="form-control" name="passscore[]" value="'. set_value('passscore', $optional->passscore) .'" />';
                                                    echo ' </div>';
                                                    echo '<br/><br/><br/>';
                                                    $subjBuffer[] = $optional->subjectid;
                                                    $optionalid = $optional->optionalid;
                                                }
                                            }
                                            
                                        }else{
                                                echo ' <div class="col-sm-offset-2 col-sm-4">';
                                                    $str = '<input name="subjectid[]" type="checkbox" value="'.$subject->subjectid.'" ';

                                                            if($this->input->post('subjectid')){
                                                                foreach($this->input->post('subjectid') as $postedid){
                                                                    if($subject->subjectid == $postedid){
                                                                        $str .= 'checked="true" ';
                                                                        break;
                                                                    }
                                                                }
                                                            }

                                                       $str .=  '/><strong> ' . ucfirst($subject->subjectname) . '</strong> &nbsp;';
                                                    echo $str;
                                                    echo ' </div>';
                                                    echo ' <div class="col-sm-4">';
                                                    echo '<input type="text" class="form-control" name="passscore[]" value="'. set_value('passscore') .'" />';
                                                    echo ' </div>';
                                                    echo '<br/><br/><br/>';
                                        }
                                        
                                        if(isset($optional) && !in_array($subject->subjectid, $subjBuffer)){

                                                     echo ' <div class="col-sm-offset-2 col-sm-4">';
                                                    $str = '<input name="subjectid[]" type="checkbox" value="'.$subject->subjectid.'" ';

                                                            if($this->input->post('subjectid')){
                                                                foreach($this->input->post('subjectid') as $postedid){
                                                                    if($subject->subjectid == $postedid){
                                                                        $str .= 'checked="true" ';
                                                                        break;
                                                                    }
                                                                }
                                                            }

                                                       $str .=  '/><strong> ' . ucfirst($subject->subjectname) . '</strong> &nbsp;';
                                                    echo $str;
                                                    echo ' </div>';
                                                    echo ' <div class="col-sm-4">';
                                                    echo '<input type="text" class="form-control" name="passscore[]" value="'. set_value('passscore') .'" />';
                                                    echo ' </div>';
                                                    echo '<br/><br/><br/>';
                                        }
                                    }
                                   ?>
                                                       <div class="col-sm-12">
                                                           Number of Optional Subjects :
                                 <?php
                                    $select_options = array();
                                    for($i=0; $i<=9; $i++) {
                                        $select_options[$i] = $i;
                                    }
                                    echo form_dropdown('arbitrarynum', $select_options, 'class="form-control" style="font-size: 11px"');
                                ?>

                            </div>
                        </div>
                 <hr/>
                        
                    <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-arrow-right"></i> Set Criteria</button>
                        <a href="<?= site_url('admin/optional') ?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-arrow-left"></i> Cancel</a>
                      </div>
                    </div>
                <?php echo form_close(); ?>
             </div>
    </div>
    
    <div class="col-md-6">
             <div class="content-box-large">
                <h4 style="text-align: center; color: #0077b3;">
                   Determining a Candidate's Pass Criteria
                </h4>
                <hr/>
             <ol>
                 <li>Select the compulsory subjects that candidates must pass</li>
                 <li>The Pass Mark for the selected Subjects should be specified</li>
                 <li>If candidates are to Pass any other arbitrary subjects together with the compulsory subjects, the number of arbitrary subjects and their pass score should be specified</li>
             </ol>
             <br/>
             <strong>E.g 1</strong> <br/>
             If Candidates must pass <strong>English</strong> and <strong>Mathematics</strong> and any other 5 subjects, Select <strong>English</strong> and <strong>Mathematics</strong>
             as compulsory subjects, specify their <strong>Pass Score</strong>, select <strong>5</strong> other subjects and enter a <strong>Pass Score</strong> for the 5 subjects.
             <br/><br/>
             <strong>E.g 2</strong> <br/>
             If Candidates must pass <strong>English</strong>, <strong>Mathematics</strong>, <strong>French</strong> and <strong>Business Studies</strong>, select <strong>English</strong>, <strong>Mathematics</strong>, <strong>French</strong> and <strong>Business Studies</strong> 
             as compulsory subjects and specify their <strong>Pass Score</strong>. Leave the arbitrary subject selection as <strong>0</strong>.
             
             <br/><br/>
             </div>
        
         
    </div>
</div>