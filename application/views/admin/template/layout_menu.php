<?php
$curr_url = $this->uri->segment(2);
$priviledges = $this->session->userdata('user_role');
$priviledges = explode("^", $priviledges);
?>

<?php
$arbitrary_school = '<li ' . (($curr_url == "") ? 'class="current"' : '') . '><a href="" data-toggle="modal" data-target="#exampleModal"><i class="glyphicon glyphicon-asterisk"></i> Arbitrary School</a></li>'
        . '<li ' . (($curr_url == "schools_edit") ? 'class="current"' : '') . '><a href="' . site_url("admin/schools_edit") . '"><i class="glyphicon glyphicon-edit"></i> Edit School Name</a></li>';

$edc_setup = '<li class="submenu ';
if ($curr_url == 'exams' || $curr_url == 'standardscore' || $curr_url == 'subjectarms' || $curr_url == 'schools_edit' || $curr_url == 'schools' || $curr_url == 'criteria' || $curr_url == 'ca_exam' || $curr_url == 'secondaryschools' || $curr_url == 'subjects' || $curr_url == 'zones' || $curr_url == 'lgas') {
    $edc_setup .= 'current open';
}

$edc_setup .= '">
                    <a href="#"><i class="glyphicon glyphicon-cog"></i> EDC Setup<span class="caret pull-right"></span></a>
                    <ul>
                        ';

$edc_setup_main = '<li ' . (($curr_url == "exams") ? 'class="current"' : '') . '><a href="' . site_url("admin/exams/save") . '"><i class="glyphicon glyphicon-tower"></i> Exam Types</a></li>
                        <li ' . (($curr_url == "ca_exam") ? 'class="current"' : '') . '><a href="' . site_url("admin/ca_exam") . '"><i class="glyphicon glyphicon-bookmark"></i> Max CA & Exam</a></li>
                        <li ' . (($curr_url == "standardscore") ? 'class="current"' : '') . '><a href="' . site_url("admin/standardscore") . '"><i class="glyphicon glyphicon-cog"></i> Standard Scores</a></li>
                        <li ' . (($curr_url == "criteria") ? 'class="current"' : '') . '><a href="' . site_url("admin/criteria") . '"><i class="glyphicon glyphicon-adjust"></i> Result Criteria</a></li>
                            <li ' . (($curr_url == "criteria") ? 'class="current"' : '') . '><a href="' . site_url("admin/optional") . '"><i class="glyphicon glyphicon-adjust"></i> Optional Result Criteria</a></li>
                        <li ' . (($curr_url == "subjects") ? 'class="current"' : '') . '><a href="' . site_url("admin/subjects/save") . '"><i class="glyphicon glyphicon-book"></i> Subjects</a></li>
                        <li ' . (($curr_url == "subjectarms") ? 'class="current"' : '') . '><a href="' . site_url("admin/subjectarms") . '"><i class="glyphicon glyphicon-book"></i> Subject Arms</a></li>
                        <li ' . (($curr_url == "zones") ? 'class="current"' : '') . '><a href="' . site_url("admin/zones/save") . '"><i class="glyphicon glyphicon-fullscreen"></i> Zones</a></li>
                        <li ' . (($curr_url == "lgas") ? 'class="current"' : '') . '><a href="' . site_url("admin/lgas/save") . '"><i class="glyphicon glyphicon-expand"></i> LGAs</a></li>
                        <li ' . (($curr_url == "schools") ? 'class="current"' : '') . '><a href="' . site_url("admin/schools/save") . '"><i class="glyphicon glyphicon-send"></i> Primary School</a></li>
                        <li ' . (($curr_url == "secondaryschools") ? 'class="current"' : '') . '><a href="' . site_url("admin/secondaryschools/save") . '"><i class="glyphicon glyphicon-export"></i> Secondary School</a></li>
                        <li ' . (($curr_url == "formnumber") ? 'class="current"' : '') . '><a href="' . site_url("admin/formnumber") . '"><i class="glyphicon glyphicon-"></i> Print Form Numbers</a></li> ';

$edc_setup_close = ' </ul>
                </li>';


$website_setup = '<li class="submenu ';
if ($curr_url == 'website') {
    $website_setup .= 'current open';
}

$website_setup .= '">
                    <a href="#"><i class="glyphicon glyphicon-globe"></i> Manage Website<span class="caret pull-right"></span></a>
                    <ul>
                        ';
$website_setup_main = '<li ' . (($curr_url == "exams") ? 'class="current"' : '') . '><a href="' . site_url("admin/website/index") . '"><i class="glyphicon glyphicon-tower"></i> Post News </a></li>';

$website_setup_close = ' </ul>
                </li>';
$pin_setup = '<li class="submenu ';
if ($curr_url == 'pinmanager') {
    $pin_setup .= 'current open';
}

$pin_setup .= '">
                    <a href="#"><i class="glyphicon glyphicon-qrcode"></i> Pin Manager<span class="caret pull-right"></span></a>
                    <ul>
                        ';
$pin_setup_main = '<li ' . (($curr_url == "pinmanager") ? 'class="current"' : '') . '><a href="' . site_url("admin/pinmanager/index") . '"><i class="glyphicon glyphicon-upload"></i> Upload Pins </a></li>
                        <li ' . (($curr_url == "pinstat") ? 'class="current"' : '') . '><a href="' . site_url("admin/pinstat/search/".$edc_detail->edcid) . '"><i class="glyphicon glyphicon-stats"></i> Pin Stats</a></li> ';

$pin_setup_close = ' </ul>
                </li>';

$report_main = '<li ' . (($curr_url == 'reports') ? 'class="current"' : '') . '><a href="' . site_url('admin/reports') . '"><i class="glyphicon glyphicon-hdd"></i> Reports</a></li>';
$report_view_candidate = '<li ' . (($curr_url == 'candidates') ? 'class="current"' : '') . '><a href="' . site_url('admin/candidates/search') . '"><i class="glyphicon glyphicon-eye-open"></i> View Candidates</a></li>';
$report_view_candidate = '<li ' . (($curr_url == 'candidates') ? 'class="current"' : '') . '><a href="' . site_url('admin/registration/edit_school_of_choice_index') . '"><i class="glyphicon glyphicon-edit"></i> Edit School of Choice</a></li>';
$report_view_candidate .= '<li ' . (($curr_url == 'search') ? 'class="current"' : '') . '><a href="' . site_url('admin/search') . '"><i class="glyphicon glyphicon-search"></i> Search</a></li>';
$report_view_candidate .= '<li ' . (($curr_url == 'registration') ? 'class="current"' : '') . '><a href="' . site_url('admin/registration/errors') . '"><i class="glyphicon glyphicon-remove"></i> Registration Errors</a></li>';
$report_view_candidate .= '<li ' . (($curr_url == 'registration') ? 'class="current"' : '') . '><a href="' . site_url('admin/gradesheet') . '"><i class="glyphicon glyphicon-file-o"></i> Print Practical Sheet</a></li>';

$result_sending_main = '<li ' . (($curr_url == 'results') ? 'class="current"' : '') . '><a href="' . site_url('admin/results') . '"><i class="glyphicon glyphicon-envelope"></i> Results 2 SMS</a></li>';

//$registratn = '';
$registratn = '<li ' . (($curr_url == 'registration') ? 'class="current"' : '') . '><a href="' . site_url("admin/registration") . '"><i class="glyphicon glyphicon-edit"></i> Registr Candidate</a></li>';
$registratn .= '<li ' . (($curr_url == 'registration_upload') ? 'class="current"' : '') . '><a href="' . site_url("admin/registration_upload") . '"><i class="glyphicon glyphicon-upload"></i> Upload Candidate</a></li>';

$activeyr = ''; //'<li '. (($curr_url == 'activeyear') ? 'class="current"' : '') .'><a href="'. site_url('admin/activeyear/save') . '"><i class="glyphicon glyphicon-calendar"></i> Set Active Year</a></li>';
$_deadline = ''; //'<li '. (($curr_url == 'regdeadline') ? 'class="current"' : '') .'><a href="'. site_url('admin/regdeadline/save') .'"><i class="glyphicon glyphicon-flash"></i> Registration Deadline</a></li>';

$user_mgt = '<li ' . (($curr_url == 'users') ? 'class="current"' : '') . '><a href="' . site_url('admin/users/save') . '"><i class="glyphicon glyphicon-user"></i> User Mgt.</a></li>';

$db_sync = '<li ' . (($curr_url == 'dbsync') ? 'class="current"' : '') . '><a href="' . site_url('admin/dbsync') . '"><i class="glyphicon glyphicon-bullhorn"></i> Synchronisation</a></li>';
if (config_item('isonline'))
    $db_sync = '';

$statistics = '<li class="submenu ';
if ($curr_url == 'remarks' || $curr_url == 'ranking' || $curr_url == 'variance') {
    $statistics .= 'current open';
}
$statistics .= '">
                    <a href="#"><i class="glyphicon glyphicon-stats"></i> Statistics<span class="caret pull-right"></span></a>
                    <ul>
                        <li ' . (($curr_url == 'variance') ? 'class="current"' : '') . '><a href="' . site_url('admin/variance') . '"><i class="glyphicon glyphicon-barcode"></i> Variance/Graphs</a></li>
                        <li ' . (($curr_url == 'remarks') ? 'class="current"' : '') . '><a href="' . site_url('admin/remarks') . '"><i class="glyphicon glyphicon-home"></i> Remark Settings</a></li>
                        <li ' . (($curr_url == 'resit_remarks') ? 'class="current"' : '') . '><a href="' . site_url('admin/resit_remarks') . '"><i class="glyphicon glyphicon-home"></i> Resit Remark Settings</a></li>

                        <li ' . (($curr_url == 'ranking') ? 'class="current"' : '') . '><a href="' . site_url('admin/ranking') . '"><i class="glyphicon glyphicon-sort"></i> Ranking</a></li>
                    </ul>
               </li>';

$grading = '<li class="submenu ';
if ($curr_url == 'grading' || $curr_url == 'manualscore' || $curr_url == 'grading_upload_scores' || $curr_url == 'grading_ca' || $curr_url == 'grading_exam' || $curr_url == 'grading_practical') {
    $grading .= 'current open';
}
$grading .= '">
                    <a href="#"><i class="glyphicon glyphicon-certificate"></i> Grading <span class="caret pull-right"></span></a>
                    <ul>';
$grading .= '<li ' . (($curr_url == 'grading') ? 'class="current"' : '') . '><a href="' . site_url('admin/grading') . '"><i class="glyphicon glyphicon-arrow-right"></i> General Grading</a></li>';
$grading .= '<li ' . (($curr_url == 'grading_ca') ? 'class="current"' : '') . '><a href="' . site_url('admin/grading_ca') . '"><i class="glyphicon glyphicon-arrow-right"></i> CA Grading</a></li>';
$grading .= '<li ' . (($curr_url == 'grading_exam') ? 'class="current"' : '') . '><a href="' . site_url('admin/grading_exam') . '"><i class="glyphicon glyphicon-arrow-right"></i> Exam Grading</a></li>';
$grading .= '<li ' . (($curr_url == 'grading_practical') ? 'class="current"' : '') . '><a href="' . site_url('admin/grading_practical') . '"><i class="glyphicon glyphicon-arrow-right"></i> Practical Grading</a></li>';
$grading .= '<li ' . (($curr_url == 'grading_upload_scores') ? 'class="current"' : '') . '><a href="' . site_url('admin/grading_upload_scores') . '"><i class="glyphicon glyphicon-upload"></i> Upload Scores</a></li>';
$grading .= '<li ' . (($curr_url == 'manualscore') ? 'class="current"' : '') . '><a href="' . site_url('admin/manualscore') . '"><i class="glyphicon glyphicon-edit"></i> Manual Entry</a></li>';
$grading .= '<li ' . (($curr_url == 'resit_grading_exam') ? 'class="current"' : '') . '><a href="' . site_url('admin/resit_grading_exam') . '"><i class="glyphicon glyphicon-arrow-right"></i> Resit Exam Grading</a></li>';
$grading .= '<li ' . (($curr_url == 'OMR_processing') ? 'class="current"' : '') . '><a href="' . site_url('admin/OMR_processing') . '"><i class="glyphicon glyphicon-cog"></i> Process OMR Scans</a></li>';

$grading.= '</ul> </li>';


$posting = '<li ' . (($curr_url == 'placement') ? 'class="current"' : '') . '><a href="' . site_url('admin/placement') . '"><i class="glyphicon glyphicon-random"></i> Edit Posting</a></li>'
        . '<li ' . (($curr_url == 'randomisepost') ? 'class="current"' : '') . '><a href="' . site_url('admin/randomisepost') . '"><i class="glyphicon glyphicon-random"></i> Perform Posting</a></li>';

$candidates_open = '<li class="submenu ';
if ($curr_url == 'registration' || $curr_url == 'registration_upload' || $curr_url == 'placement' || $curr_url == 'search' || $curr_url == 'distributepost' || $curr_url == 'randomisepost' || $curr_url == 'candidates') {
    $candidates_open .= 'current open';
}
$candidates_open .= '">
                    <a href="#"><i class="glyphicon glyphicon-list-alt"></i> Candidates<span class="caret pull-right"></span></a>
                    <ul>';

$candidates_close = '</ul>
                     </li>';

$audit_log = '<li ' . (($curr_url == 'auditlog') ? 'class="current"' : '') . '><a href="' . site_url('admin/auditlog') . '"><i class="glyphicon glyphicon-fullscreen"></i> Audit Log</a></li>';
?>

<div class="sidebar content-box" style="display: block;">
    <ul class="nav">
        <li <?php if ($curr_url == 'dashboard' || !$curr_url) echo 'class="current"'; ?>><a href="<?= site_url('admin/dashboard') ?>"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
<?php
if (in_array('Super_Administrator', $priviledges)) {
    echo $edc_setup . $edc_setup_main . $arbitrary_school . $edc_setup_close
      .$website_setup.$website_setup_main.$website_setup_close
      .$pin_setup.$pin_setup_main.$pin_setup_close
    . $candidates_open . $registratn . $report_view_candidate . $posting . $candidates_close
    . $grading
    . $statistics
    . $report_main
    .$result_sending_main
    . $activeyr
    . $_deadline
    . $audit_log
   . $db_sync
    . $user_mgt;

} else {
    if (in_array('Edc_Setup', $priviledges) || in_array('Arb_School', $priviledges)) {

        echo $edc_setup;
        if (in_array('Edc_Setup', $priviledges)) {
            echo $edc_setup_main;
        }
        if (in_array('Arb_School', $priviledges)) {
            echo $arbitrary_school;
        }
        echo $edc_setup_close;
    }

    if (in_array('Report_Viewing', $priviledges) || in_array('Posting', $priviledges) || in_array('Registration', $priviledges)) {

        echo $candidates_open;

        if (in_array('Registration', $priviledges)) {
            echo $registratn;
        }

        if (in_array('Report_Viewing', $priviledges)) {
            echo $report_view_candidate;
        }

        if (in_array('Posting', $priviledges)) {
            echo $posting;
        }

        echo $candidates_close;
    }


    if (in_array('Grading', $priviledges)) {
        echo $grading;
    }


    if (in_array('Statistics', $priviledges)) {
        echo $statistics;
    }

    if (in_array('Report_Viewing', $priviledges)) {
        echo $report_main;
    }

    if (in_array('Active_Year', $priviledges)) {
        echo $activeyr;
    }

    if (in_array('Reg_Deadline', $priviledges)) {
        echo $_deadline;
    }

    if (in_array('User_Mgt', $priviledges)) {
        echo $user_mgt;
    }

    if (in_array('Database_Sync', $priviledges)) {
        echo $db_sync;
    }
}
?>
        <li <?php if ($curr_url == 'schooladmin') echo 'class="current"'; ?>><a href="<?= site_url('admin/schooladmin') ?>"><i class="glyphicon glyphicon-fullscreen"></i> School Login.</a></li>
        <li <?php if ($curr_url == 'password') echo 'class="current"'; ?>><a href="<?= site_url('admin/password/change') ?>"><i class="glyphicon glyphicon-lock"></i> Change Pass.</a></li>

    </ul>
</div>
<blockquote style="font-weight: bold; font-size: 11px"><img src="<?= get_img('edevpro.png'); ?>" height="30px" border="0" />
    <br/>&copy; Platform Technology Ltd</blockquote>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Create New School</h4>
            </div>

<?= form_open(site_url('admin/schools/arbitrary'), 'class="form-horizontal" role="form"'); ?>
            <div class="modal-body">
                <strong style="color: crimson">This option is used add schools whose school code is not known.
                    If the school code already exist please do not use this option.</strong>
                <br/><br/>
                <strong style="color: crimson">"Allow Posting"</strong> is used to indicates those school that students could be posted to.
                This applies to Exams that requirement "Placement" like GPT.
                <br/><br/>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">LGA:</label>
                    <div class="col-sm-9">
<?php
$this->db->order_by('lganame');
$lgas = $this->db->where('edcid', $this->data['edc_detail']->edcid)->get('t_lgas')->result();
$select_options = array();
$select_options[''] = "------ Select Lga --------";

foreach ($lgas as $lg) {
    $select_options[$lg->lgaid] = $lg->lganame;
}

echo form_dropdown('lgaid', $select_options, $this->input->post('lgaid') ? $this->input->post('lgaid') : '', ' required="required" class="form-control" ');
?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Genre:</label>
                    <div class="col-sm-9">
<?php
$select_options = array();
$select_options['primary'] = "PRIMARY SCHOOL";
$select_options['secondary'] = "SECONDARY SCHOOL";
echo form_dropdown('genre', $select_options, $this->input->post('genre') ? $this->input->post('genre') : '', ' required="required" class="form-control" ');
?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">School Name:</label>
                    <div class="col-sm-9">
                        <input type="text" name="schoolname" value="<?php echo set_value('schoolname'); ?>" class="form-control" id="inputEmail3" placeholder="Enter Secondary School Name" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="ispostingschool" value="1"> <strong>Allow Posting</strong>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-saved"></i> Save </button>
            </div>
<?= form_close(); ?>
        </div>
    </div>
</div>
