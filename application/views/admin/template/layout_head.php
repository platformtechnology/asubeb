<!DOCTYPE html>
<html>
  <head>
    <title><?= $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
    
    <?= $page_level_styles; ?>
    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .gradefield{
            border: 0;
            width: 40px;
        }
        .blink {
            animation: blink 1s steps(5, start) infinite;
            -webkit-animation: blink 1s steps(5, start) infinite;
          }
          @keyframes blink {
            to {
              visibility: hidden;
            }
          }
          @-webkit-keyframes blink {
            to {
              visibility: hidden;
            }
          }
    </style>
    <script type="text/javascript">
        
        function display_c(){
            var refresh=1000;
            mytime=setTimeout('display_ct()',refresh);
        }

        function display_ct() {
            var x = new Date();
            var x1 = x.toDateString() + ', ' + x.getHours() + ':' + x.getMinutes() + ':' + x.getSeconds();//.toString();// changing the display to UTC string
            document.getElementById('time').innerHTML = x1;
            tt=display_c();
        } 
    </script>
    
  </head>
   <body onload="display_ct(); <?= ($this->session->flashdata('done') ? 'displayCandidates();' : 'displayCandidates();') ?>">
       <div class="header" style="background-color: <?= $edc_detail->themecolor ?>;">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-8">
                       <div class="logo" style="width: 370px;">
                           <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo; //get_img('edc_logos/'.$edc_detail->edclogo)?>" alt="Edc Logo" border="0" /> </div>
                          <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
	              </div>
	           </div>
	           
	           <div class="col-md-4 pull-right">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Good day dear <?= ucwords($this->session->userdata('user_name')); ?>
                                    <br/>Date: <span id="time"></span> <b class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                      <li><a href="<?= site_url('admin/password/change');?>">Change Password</a></li>
                                      <li><a href="<?= site_url('admin/login/logout');?>">Logout</a></li>
                                    </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>