<?php $this->load->view('admin/template/layout_head'); ?>
<div class="page-content">
    <?php if(isset($candidates) && ($this->uri->segment(2) == "grading" 
            || $this->uri->segment(2) == "grading_ca" 
            || $this->uri->segment(2) == "grading_exam"
            || $this->uri->segment(2) == "grading_practical"
            || ($this->uri->segment(2) == "variance"))): ?>
        <?php $this->load->view($subview); ?>       
    <?php 
               
        else:
            if(($this->uri->segment(2) == "variance") && ($this->uri->segment(3) == "settings")): 
                $this->load->view($subview); 
            else:
     ?>
    
        <div class="row">
            <div class="col-md-2">
                 <?php 
                   $this->load->view('admin/template/layout_menu');
                 ?>  
            </div>
            <div class="col-md-10">
                 <?php $this->load->view($subview); ?>   
            </div>   
        </div>
    
     <?php endif; ?>
   <?php endif; ?>
</div>
<?php $this->load->view('admin/template/layout_foot'); ?>
  

   


