
<div class="row">
    <div class="col-md-12 panel-primary">
        
            <div class="content-box-header panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> Setup Exam Subjects</div>
            </div>
             <div class="content-box-large box-with-header">
                 <div class="row">
                     <div class="col-md-5">
                            <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                            <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                            <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                            <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                        
                            <strong class="col-sm-12"><span style="color: crimson;">Room For CA Is Automatically Added Along Side Each Subject!</span></strong> 
                            <br/>
                            <br/>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Exam Subject:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="subjectname" value="<?php echo set_value('subjectname', $subject_detail->subjectname); ?>" class="form-control" id="inputEmail3" placeholder="Enter Exam Subject " required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Exam Type:</label>
                                <div class="col-sm-8">
                                    <?php 
                                      $select_options = array();
                                      foreach ($exams as $exam) {
                                          $select_options[$exam->examid] = $exam->examname;
                                      }
                                      $select_options[''] = "..::Select Exam Type::..";
                                      echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : $subject_detail->examid, 'class="form-control" required="required"');
                                    ?>
                                </div>
                            </div>

                                                      
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Priority/Order:</label>
                                <div class="col-sm-8">
                                     <?php 
                                      $select_options = array();
                                      for($i=1; $i<=15; $i++){
                                          $select_options[$i] = $i;
                                      }
                                      echo form_dropdown('priority', $select_options, $this->input->post('priority') ? $this->input->post('priority') : $subject_detail->priority, 'class="form-control"');
                                    ?>
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                     <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="haspractical" value="1" <?php echo ($subject_detail->haspractical == 1 ? 'checked="true"' : '')?>> <strong>Has Practical</strong>
                                        </label>
                                      </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                     <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="iscompulsory" value="1" <?php echo ($subject_detail->iscompulsory == 1 ? ' checked="true"' : '')?>> <strong>Is Compulsory</strong>
                                        </label>
                                      </div>
                                </div>
                            </div>
                            
                             <div class="form-group">
                                   <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                    <a href="<?= site_url('admin/subjects/save');?>" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                                  </div>
                                </div>
                        
                        <?= form_close(); ?>
                    </div>
                     
                    <div class="col-md-7">
                         <?php if($this->session->flashdata('updated_msg')) echo get_success($this->session->flashdata('updated_msg')); ?>
                        <table class="table table-bordered table-condensed" id="example">
                          <thead>
                            <tr>
                                <th>ExamType</th>
                              <th>Subject</th>
                              <th>HasPractical</th>
                              <th>IsCompulsory</th>
                              <th>Priority</th>
                              
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                               <?php 
                                if(count($all_subjects)): 
                                    foreach ($all_subjects as $subjects):
                                ?>
                                <tr>
                                    <td><?= $this->subjects_model->getExamName_From_Id($subjects->examid); ?></td>
                                    <td><?php echo anchor(site_url('admin/subjects/save/'.$subjects->subjectid), ucwords(strtolower($subjects->subjectname))) ?></td>
                                    <td><?= $subjects->haspractical == 1 ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'; ?></td>
                                    <td><?= $subjects->iscompulsory == 1 ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'; ?></td>
                                    <td><?= $subjects->priority; ?></td>
                                    
                                    
                                    <td>
                                        <?= get_edit_btn(site_url('admin/subjects/save/' . $subjects->subjectid)); ?>
                                        <?= get_del_btn(site_url('admin/subjects/delete/' . $subjects->subjectid)); ?>
                                    </td>
                                </tr> 
                                <?php endforeach;?>
                                <?php endif; ?>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        
    </div>
</div>
