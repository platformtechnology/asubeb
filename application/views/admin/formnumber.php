
<div class="row">

    <div class="col-md-6 panel-primary">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-edit"></i> Generate and Print Form Numbers</strong>
                </div>
            </div>
             <div class="content-box-large box-with-header">
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                <?php echo form_open('', 'class="form-horizontal"  target="_blank" role="form"'); ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Start Serial Numbering at:</label>
                        <div class="col-sm-8">
                            <input type ="number" name ="serial_start" title ="Enter the Begining of the Serial Number" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">End Serial Numbering at:</label>
                        <div class="col-sm-8">
                            <input type ="number" name ="serial_end" title ="Enter the End of the Serial" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-chevron-right "></i> Generate Form Numbers</button>
                      </div>
                    </div>
                <?php echo form_close(); ?>
             </div>
    </div>

    <div class="col-md-6 panel-primary">
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <strong><i class="glyphicon glyphicon-edit"></i> Instructions</strong>
                </div>
            </div>
             <div class="content-box-large box-with-header">
                 <p> Generate Form Numbers is simple<br/>
                    Assume I want to generate form numbers ranging from 6001-7000
                 </p>
                    <ol>
                        <li> I Enter 6001 in the Start Box</li>
                        <li> I Enter 7000 in the End Box</li>
                        <li> I Click on Generate Form Numbers</li>
                    </ol>

             </div>
    </div>

</div>