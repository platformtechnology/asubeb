<!DOCTYPE html>
<html>
  <head>
    <title><?= $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .css-vertical{
            filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                 -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
             -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
              -ms-transform: rotate(-90.0deg);  /* IE9+ */
               -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
          -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                  transform: rotate(-90.0deg);  /* Standard */
        }
    </style>
    <script lang="javascript" type="text/javascript">
        window.addEventListener('load', function () {
            var rotates = document.getElementsByClassName('css-vertical');
            for (var i = 0; i < rotates.length; i++) {
                rotates[i].style.height = rotates[i].offsetWidth + 'px';
            }
        });
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
            
    </script>
  </head>
   <body>
  	<div class="header" style="background-color: <?= $edc_detail->themecolor ?>;">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-8">
	              <div class="logo" style="width: 370px;">
                           <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo ?>" alt="Edc Logo" border="0" /> </div>
                           <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
	              </div>
	           </div>  
                    
                   <div class="col-md-4 pull-right">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="<?= site_url('admin/password/change');?>">Change Password</a></li>
                                  <li><a href="<?= site_url('admin/login/logout');?>">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>
       <div class="page-content">
           <div class="row">
               <div class="container">
                   <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
               </div>
               <div id="printdiv">
                   <?php
                        foreach($zones as $zone):
                            $schools = $this->report_model->get_schools_per_zone($zone->zoneid);
                   ?>
                   <div class="row">
                        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                             The Ministry Of Education<br/>
                             <img src="<?= $edc_logo ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                             <br/>
                             <?= $title ?> For <span style="font-size: 26px"><?= strtoupper($exam_detail->examname); ?> </span> <?= $examyear; ?> <br/>
                             <br/> <?= strtoupper($zone->zonename) ?> ZONE
                             
                        </h4>

                       <br/>
                       <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                           <table width="100%" class="table-bordered">
                               
                               <tr  style="text-align: center; font-weight: bold;">
                                   <td rowspan="2" align="center" >SCHOOLS</td>
                                    <td rowspan="<?= 4 + count($schools); ?>" >&nbsp;</td>
                                    <td colspan="<?= $span->span + (count($subjects)*2); ?>" align="center">SUBJECTS</td>
                              </tr>
                              
                            <!--SUBJECTS HEADER ROW-->
                              <tr>
                              
                                <?php 
                                   $sum_rmarks = 0;
                                   foreach ($subjects as $subject):
                                     $remarks = $this->report_model->get_remakrs_per_subject($subject->subjectid);
                                     $sum_rmarks += count($remarks);
                                ?>
                                  <td colspan="<?= count($remarks) ?>" valign="middle"> <div class="css-vertical" style="width: 130px; text-align: center;"><?= ucwords(strtolower($subject->subjectname))?></div></td>
                                    <td rowspan="<?= 3 + count($schools); ?>">&nbsp;</td>
                                    
                                <?php endforeach; ?>
                                
                              </tr>
                              <!-- END SUBJECTS HEADER ROW-->
                              
                               <!--REMARK GRADE HEADER ROW-->
                               <tr>
                                <td>&nbsp;</td>
                                <?php 
                                   foreach ($subjects as $subject):
                                     $remarks = $this->report_model->get_remakrs_per_subject($subject->subjectid);
                                        foreach ($remarks as $remark):
                                ?>
                                         <td><strong><?= $remark->grade; ?></strong></td>
                                <?php 
                                        endforeach; 
                                    endforeach; 
                                ?>
                              </tr>
                               <!-- END REMARK GRADE HEADER ROW-->
                               
                               <!--  EMPTY ROW-->
                               <tr>
                                <td>&nbsp;</td>
                                <?php 
                                    for($i=1; $i<= $sum_rmarks; $i++):
                                ?>
                                  <td>&nbsp;</td>
                                <?php endfor; ?>
                              </tr>
                               <!-- END EMPTY ROW-->
                               
                               
                               <!-- SCHOOL ROW-->
                               
                               <?php 
                                foreach($schools as $school):
                               ?>
                                <tr>
                                    <td><strong><?= $school->schoolname ?></strong></td>

                                     <?php 
                                        foreach ($subjects as $subject):
                                          $remarks = $this->report_model->get_remakrs_per_subject($subject->subjectid);
                                             foreach ($remarks as $remark):
                                                 $remark_stat = $this->remarks_model->get_stat_per_remark($subject->subjectid, $examid, $examyear, $remark->minimum, $remark->maximum, $school->schoolid);
                                                     
                                     ?>
                                              <td><strong><?= round($remark_stat['percentage_count_per_remark']); ?></strong></td>
                                     <?php 
                                             endforeach; 
                                         endforeach; 
                                     ?>
                                </tr>   
                               <?php endforeach; ?>                               
                             
                               
                               <!-- END SCHOOL ROW-->
                               
                           </table>                  
                       </div>
                   </div>
                   
                    <p style="page-break-before: always;"></p>
                   <?php endforeach; ?>
                   
                   
                   
                </div>
            </div>
        </div>
     
    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>   
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script> 
    
   </body>
</html>
