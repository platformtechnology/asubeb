<!DOCTYPE html>
<html>
  <head>
    <title><?= $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .css-vertical{
            filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                 -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
             -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
              -ms-transform: rotate(-90.0deg);  /* IE9+ */
               -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
          -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                  transform: rotate(-90.0deg);  /* Standard */
                  white-space:nowrap;
        }
    </style>
    
    <script lang="javascript" type="text/javascript">
        window.addEventListener('load', function () {
            var rotates = document.getElementsByClassName('css-vertical');
            for (var i = 0; i < rotates.length; i++) {
                rotates[i].style.height = rotates[i].offsetWidth + 'px';
            }
        });
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
    </script>
  </head>
  <body style="background-color: white">
 <?php 
    if (isset($school_detail) && count($school_detail)){
        foreach ($school_detail as $schools): 

            if($practical)$exam_candidates = $this->registration_model->get_practical_candidates($examid, $examyear, $schools->schoolid);
            else $exam_candidates = $this->registration_model->get_pro_practical_candidates($examid, $examyear, $schools->schoolid, $interval);
            
            if(count($exam_candidates)):
  ?> 
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-8">
	              <div class="logo" style="width: 370px;">
                           <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo?>" alt="Edc Logo" border="0" /> </div>
                           <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
	              </div>
	           </div>  
	        </div>
	     </div>
	</div>
       <div class="page-content">
           <div class="row">
                      
                <div class="row">
                    <div class="col-lg-12">
                        <strong>SCHOOL:</strong> <span style="text-decoration: underline"><?=$schools->schoolname; ?></span> 
                        <?php if(!$exam_detail->haszone){ ?>| <strong>L.G.A:</strong> <?= $this->registration_model->get_lga($schools->lgaid); ?> <?php } ?>
                        | <strong>ZONE:</strong> <?= $this->db->where('zoneid', $schools->zoneid)->get('t_zones')->row()->zonename; ?>
                        <br/>
                        <strong>SCHOOL CODE = <?=$schools->schoolcode; ?></strong> 
                    </div>
                </div>

                <br/>
                 <table class="table table-bordered table-condensed">
                     
                          <thead>
                            <tr>
                              <th>S/N</th>
                              <th width="250px">CANDIDATE'S NAME IN BLOCK LETTERS (Surname First)</th>
                              <th width="150px">D.O.B</th>
                              <th>SEX</th>
                              <th>EXAM NUMBER</th>
                              <th colspan="2">Cultural And Creative Arts</th>
                              <th>French</th>
                              <th>Business Studies</th>
                              <th colspan="3">Pre-vocational Studies</th>
                              <th colspan="2">Basic Science And Technology</th>
                              <?php
                               $coldata = $this->db->where('edcid', $edc_detail->edcid)->get('t_printlayout')->result();
                              ?>
                            </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td>&nbsp;</td>
                                  <td width="250px">&nbsp;</td>
                                  <td width="150px">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                         LOCAL CRAFTS
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                          MUSIC
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                          &nbsp;
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                         &nbsp;
                                      </p>
                                  </td>
                       
                                   <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                         AGRIC.
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                          HOME ECONS.
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                          ENTERPRENEURSHIP.
                                      </p>
                                  </td>
                                  
                                   <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                         INFO TECH
                                      </p>
                                  </td>
                                  <td align="right" style="padding: 0;">
                                      <p class="css-vertical" style="text-align: right; font-weight: bold; padding: 0;">
                                          BASIC TECH
                                      </p>
                                  </td>
                                   
                              </tr>


                               <?php 
                                if(count($exam_candidates)): 
                                    $sn = 0;
                                    foreach ($exam_candidates as $registrant):
                                        $name = strtoupper($registrant->firstname) . ' ' . strtoupper($registrant->othernames);
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td width="250px"><?= $name ?></td>
                                    <td width="150px"><?= $registrant->dob; ?></td>
                                    <td><?= strtoupper($registrant->gender); ?></td>
                                    <td><?= $registrant->examno; ?></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    
                                </tr> 
                                    <?php endforeach;?>
                                <?php endif; ?>
                          </tbody>
                        </table>        
                 
                    <table border="0">
                        <tr>
                            <td>Name of School Head:</td>
                            <td width="600px">_____________________________________</td>
                            <td>Phone No.</td>
                            <td>________________</td>
                        </tr>
                    </table>
            </div>
        </div>
   
 <?php
        else:
            echo "--- NOT FOUND ---";
        endif;
        
    endforeach;
    }else echo "--- NOT FOUND ---";
 ?>
    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>   
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script> 
    
   </body>
</html>
