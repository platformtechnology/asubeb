
<!DOCTYPE html>
<html>
  <head>
    <title><?= $site_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">

	<?=  $page_level_styles; ?>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .css-vertical{
            filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                 -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
             -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
              -ms-transform: rotate(-90.0deg);  /* IE9+ */
               -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
          -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                  transform: rotate(-90.0deg);  /* Standard */
                  white-space:nowrap;
        }

		.css-vertical-text {
		color:#333;
		border:0px solid red;
		writing-mode:tb-rl;
		-webkit-transform:rotate(-90deg);
		-moz-transform:rotate(-90deg);
		-o-transform: rotate(-90deg);
		-ms-transform: rotate(-90deg);
		transform: rotate(-90deg);
		white-space:nowrap;
		display:block;
		bottom:0;
		width:20px;
		height:20px;
		font-family: 'Trebuchet MS', Helvetica, sans-serif;
		font-size:11px;
		font-weight:bold;
	 /*text-align:left;*/
	 /*text-shadow: 0px 0px 1px #333;*/

		filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
		}
    </style>
    <script lang="javascript" type="text/javascript">
        window.addEventListener('load', function () {
            var rotates = document.getElementsByClassName('css-vertical');
            for (var i = 0; i < rotates.length; i++) {
               rotates[i].style.height = (rotates[i].offsetWidth) + 'px';
            }
        });
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }

    </script>
  </head>
    <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-8">
	              <div class="logo" style="width: 370px;">
                           <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo?>" alt="Edc Logo" border="0" /> </div>
                           <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
	              </div>
	           </div>

                   <div class="col-md-4 pull-right">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="<?= site_url('admin/password/change');?>">Change Password</a></li>
                                  <li><a href="<?= site_url('admin/login/logout');?>">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>
         <?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>


       <div class="page-content">
           <div class="row">
 <div class="row">
    <div class="col-md-12 panel-default">

            <div class="content-box-header panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-user"></i> <?= $title; ?></div>
            </div>
            <div class="content-box-large box-with-header">


				<div class="container">
					<button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
				</div>


			 <div id="printdiv">
                             <?php if(count($unposted_candidates)){?>
                             
                    <table class="table table-bordered table-condensed" id="example">
                        <caption align = "center"> <h4> <?= $title; ?> </h4>
                            <h5> LGA Name : <?php echo isset($lganame)?$lganame:'All'; ?> </h5>
                            <h5> School Name : <?php echo !empty($schooname)? $schooname :'All'; ?> </h5>
                            <h5> Total Candidates: <?php echo $total_count;  ?></h5>
                        </caption>
                      <thead>
                          <tr>
                              <td>
                                  S/N
                              </td>
                              <td>
                                 Candidate Name
                              </td>
                              <td>
                                Exam Number
                              </td>
                              <td>
                              LGA
                              </td>
                              <td>
                                Score
                              </td>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                          $sn = 1;
                          foreach($unposted_candidates as $data): ?>
                          <tr>
                              <td>
                                  <?php echo $sn; ?>
                              </td>
                              <td>
                                 <?php echo $data->firstname.' '.$data->othernames;?>
                              </td>
                              <td>
                                <?php echo $data->examno;?>
                              </td>
                              <td>
                                <?php echo $data->lganame;?>
                              </td>
                              <td>
                                <?php echo $data->totalscore;?>
                              </td>
                          </tr>
                          <?php
                          $sn++;
                          endforeach;?>
                      </tbody>
                    </table>
                             <?php
                             }
                             ?>
			    </div>

            </div>

    </div>
</div>


     </div>

</div>


    <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

	<?=  $page_level_scripts; ?>

   </body>
</html>
