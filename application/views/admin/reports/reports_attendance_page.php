<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script lang="javascript" type="text/javascript">
            function printDiv(divID) {
                //Get the HTML of div
                var divElements = document.getElementById(divID).innerHTML;
                //Get the HTML of whole page
                var oldPage = document.body.innerHTML;

                //Reset the page's HTML with div's HTML only
                document.body.innerHTML =
                        "<html><head></head><body>" +
                        divElements + "</body>";

                //Print Page
                window.print();

                //Restore orignal HTML
                document.body.innerHTML = oldPage;
            }

        </script>
    </head>
    <body>
        <div class="header" style="background-color: <?= $edc_detail->themecolor ?>;">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="logo" style="width: 370px;">
                            <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo ?>" alt="Edc Logo" border="0" /> </div>
                            <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
                        </div>
                    </div>

                    <div class="col-md-4 pull-right">
                        <div class="navbar navbar-inverse" role="banner">
                            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
                                        <ul class="dropdown-menu animated fadeInUp">
                                            <li><a href="<?= site_url('admin/password/change'); ?>">Change Password</a></li>
                                            <li><a href="<?= site_url('admin/login/logout'); ?>">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="container">
                   <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
               </div>
                <div class="container" id="printdiv">
                    <?php
                    $param = ($examdetail->haszone ? 'zoneid' : 'lgaid');
                    foreach ($group as $lga_zone_data) {
                        foreach ($attendance_data as $a_data) {
                            if ($lga_zone_data->$param == $a_data->$param) {
                                $this->db->where('edcid', $this->edcid);
                                $this->db->where('examid', $examid);
                                $this->db->where('examyear', $examyear);
                                $this->db->where('schoolid', $a_data->schoolid);
                                $this->db->order_by('examno');
                                $exam_candidates = $this->registration_model->get_all();
                                ?>

                                <div class="row">
                                    <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                                        THE MINISTRY OF EDUCATION<br/>
                                        <img src="<?= $edc_logo ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span><br/>
                                        <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>
                                    </h4>

                                    <br/>
                                    <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">


                                                <strong><?= $title; ?></strong><br/>
                                                <?php
                                                if ($examdetail->haszone) {
                                                    echo '<strong>ZONE:</strong> ' . strtoupper($a_data->zonename);
                                                } else
                                                    echo '<strong>LGA:</strong> ' . strtoupper($a_data->lganame);
                                                ?><br/>
                                                <strong>SCHOOL:</strong> <span style="text-decoration: underline"><?= strtoupper($a_data->schoolname); ?></span> <br/>
                                                <strong>SCHOOL CODE = <?= $a_data->schoolcode; ?></strong>
												<br/>
												<br/>
												<div class = "pull"> <strong>SUBJECT NAME: </strong>________________________________</div>

                                        <br/>
                                        <table class="table table-bordered table-condensed">

                                            <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th width = "10%">EXAM NUMBER</th>
                                                    <th width="40%">CANDIDATE'S NAME IN BLOCK LETTERS (Surname First)</th>
													<th width="10%"> ATTENDANCE</th>
													<th width="40%"> SCORE</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
													<th> </th>
													<th> </th>
                                                </tr>


                                                <?php
                                                $sn = 0;
                                                foreach ($exam_candidates as $registrant):
                                                    $name = strtoupper($registrant->firstname . ' ' . $registrant->othernames);
                                                    ?>
                                                    <tr>
                                                        <td><?= ++$sn; ?></td>
                                                        <td><?= $registrant->examno; ?> <?php if (strtotime(substr($registrant->datecreated, 0,10)) > strtotime('2016-05-26')){ echo "( Late Registration)";}?></td>
                                                        <td><?= $name ?> </td>
														<th> </th>
														<th> </th>
                                                    </tr>
                                                <?php endforeach; ?>

                                            </tbody>
                                        </table>

                                        <table border="0">
                                            <tr>
                                                <td>Name of School Head:</td>
                                                <td width="600px">_____________________________________</td>
                                                <td>Phone No.</td>
                                                <td>________________</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <p style="page-break-before: always;"></p>

                                <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>

        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

    </body>
</html>

