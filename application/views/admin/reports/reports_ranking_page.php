<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .css-vertical{
                filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
                -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
                -ms-transform: rotate(-90.0deg);  /* IE9+ */
                -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
                -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                transform: rotate(-90.0deg);  /* Standard */
                white-space:nowrap;
            }
        </style>
        <script lang="javascript" type="text/javascript">
            window.addEventListener('load', function() {
                var rotates = document.getElementsByClassName('css-vertical');
                for (var i = 0; i < rotates.length; i++) {
                    rotates[i].style.height = (rotates[i].offsetWidth) + 'px';
                }
            });
            function printDiv(divID) {
                //Get the HTML of div
                var divElements = document.getElementById(divID).innerHTML;
                //Get the HTML of whole page
                var oldPage = document.body.innerHTML;

                //Reset the page's HTML with div's HTML only
                document.body.innerHTML =
                        "<html><head></head><body>" +
                        divElements + "</body>";

                //Print Page
                window.print();

                //Restore orignal HTML
                document.body.innerHTML = oldPage;
            }

        </script>
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="logo" style="width: 370px;">
                            <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo ?>" alt="Edc Logo" border="0" /> </div>
                            <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
                        </div>
                    </div>  

                    <div class="col-md-4 pull-right">
                        <div class="navbar navbar-inverse" role="banner">
                            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
                                        <ul class="dropdown-menu animated fadeInUp">
                                            <li><a href="<?= site_url('admin/password/change'); ?>">Change Password</a></li>
                                            <li><a href="<?= site_url('admin/login/logout'); ?>">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="container">
                    <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
                </div>
                <div id="printdiv">

                    <?php if (isset($ranks)): ?>
                        <div class="row">
                            <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                                THE MINISTRY OF EDUCATION<br/>
                                <img src="<?= $edc_logo ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                                <br/>
                                <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>

                            </h4>
                        </div>
                        <br/>
                        <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                            <div class="panel-heading">
                                <h4>TOP RANKS</h4>
                                <hr/>
                            </div>
                            <table width="100%" class="table table-bordered table-condensed">
                                <tr style="font-weight: bold;">
                                    <td rowspan="2">SN</td>
                                    <td rowspan="2">EXAMNO</td>
                                    <td rowspan="2">NAME</td>
                                    <td rowspan="2">SEX</td>
                                    <td rowspan="2">PRESENT SCHOOL</td>
                                    <td rowspan="<?= 2 + count($ranks); ?>" >&nbsp;</td>
                                    <td colspan="<?= count($allsubjects); ?>" align="center">SUBJECTS</td>
                                    <td rowspan="<?= 2 + count($ranks); ?>" >&nbsp;</td>
                                    <?= $examdetail->hasposting ? '<td rowspan="2">TOTAL</td>' : '' ?>
                                    <td rowspan="2">STATUS</td>
                                </tr>
                                <!--SUBJECTS HEADER ROW-->
                                <tr>

                                    <?php
                                    foreach ($allsubjects as $subject) {
                                        echo '<td align="left"> <p class="css-vertical" style="text-align: right;">' . ucwords(strtoupper($subject->subjectname)) . '</p></td>';
                                    }
                                    ?>

                                </tr>
                                <!-- END SUBJECTS HEADER ROW-->
                                <?php
                                $row = 0;

                                foreach ($ranks as $candidate):

                                    $name = $candidate->firstname . ' ' . $candidate->othernames;

                                    echo '<tr>';
                                    echo '<td>' . ++$row . '</td>';
                                    echo '<td>' . $candidate->examno . '</td>';
                                    echo '<td>' . strtoupper($name) . '</td>';
                                    echo '<td>' . $candidate->gender . '</td>';
                                    echo '<td>' . $this->registration_model->get_school($candidate->schoolid) . '</td>';

                                    if (count($allsubjects)) {
                                        $totalScores = 0;
                                        $registered_subjects = $this->reg_subjects_model->get_registered_subjects($candidate->candidateid, $examid, $examyear);
                                        if (count($registered_subjects)) {
                                            foreach ($allsubjects as $subjects) {
                                                foreach ($registered_subjects as $reg_subject) {
                                                    $found = false;
                                                    if ($reg_subject->subjectid == $subjects->subjectid) {

                                                        if ($examdetail->hasposting) {
                                                            $exam_data = array();
                                                            foreach ($toprank_scores as $scoresvalue) {
                                                                if (($scoresvalue->subjectid == $subjects->subjectid) && ($scoresvalue->candidateid == $candidate->candidateid)) {
                                                                    $exam_data = $scoresvalue;
                                                                    break 1;
                                                                }
                                                            }
                                                            if ($exam_data->exam_score < 0)
                                                                echo '<td><strong><span style="font-size: 10px; color: crimson;">ABS</span></strong></td>';
                                                            else {
                                                                echo '<td><strong> ' . $sumtotal = get_individual_standard($exam_data->ca_score, $standarddata->standardca) + get_individual_standard($exam_data->exam_score, $standarddata->standardexam) + get_individual_standard($exam_data->practical_score, $standarddata->standardpractical) . '</strong></td>';
                                                                $totalScores += $sumtotal;
                                                            }
                                                        } else {
                                                            $remark_detail = $this->report_model->get_subject_remark_per_candidate($subjects->subjectid, $candidate->candidateid, $examid, $examyear);
                                                            if ($remark_detail->exam_score < 0) echo '<td style="font-size: 10px; color: crimson;"><strong>ABS</strong></td>';
                                                            else echo '<td><strong>' . (count($remark_detail) ? strtoupper($remark_detail->grade) : '---') . '</strong></td>';
                                                        }

                                                        $found = true;
                                                        break;
                                                    }
                                                }

                                                if (!$found)
                                                    echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                            }
                                        } else
                                            echo '<td title="" style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                    }

                                    $pass = $this->report_model->get_passcount_basedon_criteria_per_candidate($candidate->candidateid, $examid, $examyear, $cutoff);

                                    echo $examdetail->hasposting ? '<td><strong>' . $totalScores . '</strong></td>' : '';
                                    echo '<td><strong>' . $pass . '</strong></td>';
                                    echo '</tr>';
                                    ?>


                                    <?php
                                endforeach;
                                ?>
                            </table>    
                        </div>

                        <?php
                    endif;
                    ?>
                    <br/>
                     <p style="page-break-before: always;"></p>
                    <?php if (isset($lowranks)): ?>
                        <div class="row">
                            <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                                THE MINISTRY OF EDUCATION<br/>
                                <img src="<?= $edc_logo ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                                <br/>
                                <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>

                            </h4>
                        </div>
                        <br/>
                        <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                            <div class="panel-heading">
                                <h4>LOWEST RANKS</h4>
                                <hr/>
                            </div>
                          <table width="100%" class="table table-bordered table-condensed">
                                <tr style="font-weight: bold;">
                                    <td rowspan="2">SN</td>
                                    <td rowspan="2">EXAMNO</td>
                                    <td rowspan="2">NAME</td>
                                    <td rowspan="2">SEX</td>
                                    <td rowspan="2">PRESENT SCHOOL</td>
                                    <td rowspan="<?= 2 + count($ranks); ?>" >&nbsp;</td>
                                    <td colspan="<?= count($allsubjects); ?>" align="center">SUBJECTS</td>
                                    <td rowspan="<?= 2 + count($ranks); ?>" >&nbsp;</td>
                                    <?= $examdetail->hasposting ? '<td rowspan="2">TOTAL</td>' : '' ?>
                                    <td rowspan="2">STATUS</td>
                                </tr>
                                <!--SUBJECTS HEADER ROW-->
                                <tr>

                                    <?php
                                    foreach ($allsubjects as $subject) {
                                        echo '<td align="left"> <p class="css-vertical" style="text-align: right;">' . ucwords(strtoupper($subject->subjectname)) . '</p></td>';
                                    }
                                    ?>

                                </tr>
                                <!-- END SUBJECTS HEADER ROW-->
                                <?php
                                $row = 0;

                                foreach ($lowranks as $candidate):

                                    $name = $candidate->firstname . ' ' . $candidate->othernames;

                                    echo '<tr>';
                                    echo '<td>' . ++$row . '</td>';
                                    echo '<td>' . $candidate->examno . '</td>';
                                    echo '<td>' . strtoupper($name) . '</td>';
                                    echo '<td>' . $candidate->gender . '</td>';
                                    echo '<td>' . $this->registration_model->get_school($candidate->schoolid) . '</td>';

                                    if (count($allsubjects)) {
                                        $totalScores = 0;
                                        $registered_subjects = $this->reg_subjects_model->get_registered_subjects($candidate->candidateid, $examid, $examyear);
                                        if (count($registered_subjects)) {
                                            foreach ($allsubjects as $subjects) {
                                                foreach ($registered_subjects as $reg_subject) {
                                                    $found = false;
                                                    if ($reg_subject->subjectid == $subjects->subjectid) {

                                                        if ($examdetail->hasposting) {
                                                            $exam_data = array();
                                                            foreach ($lrank_scores as $scoresvalue) {
                                                                if (($scoresvalue->subjectid == $subjects->subjectid) && ($scoresvalue->candidateid == $candidate->candidateid)) {
                                                                    $exam_data = $scoresvalue;
                                                                    break 1;
                                                                }
                                                            }
                                                            if ($exam_data->exam_score < 0)
                                                                echo '<td><strong><span style="font-size: 10px; color: crimson;">ABS</span></strong></td>';
                                                            else {
                                                                echo '<td><strong> ' . $sumtotal = get_individual_standard($exam_data->ca_score, $standarddata->standardca) + get_individual_standard($exam_data->exam_score, $standarddata->standardexam) + get_individual_standard($exam_data->practical_score, $standarddata->standardpractical) . '</strong></td>';
                                                                $totalScores += $sumtotal;
                                                            }
                                                        } else {
                                                            $remark_detail = $this->report_model->get_subject_remark_per_candidate($subjects->subjectid, $candidate->candidateid, $examid, $examyear);
                                                            if ($remark_detail->exam_score < 0) echo '<td style="font-size: 10px; color: crimson;"><strong>ABS</strong></td>';
                                                            else echo '<td><strong>' . (count($remark_detail) ? strtoupper($remark_detail->grade) : '---') . '</strong></td>';
                                                        }

                                                        $found = true;
                                                        break;
                                                    }
                                                }

                                                if (!$found)
                                                    echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                            }
                                        } else
                                            echo '<td title="" style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                    }

                                    $pass = $this->report_model->get_passcount_basedon_criteria_per_candidate($candidate->candidateid, $examid, $examyear, $cutoff);

                                    echo $examdetail->hasposting ? '<td><strong>' . $totalScores . '</strong></td>' : '';
                                    echo '<td><strong>' . $pass . '</strong></td>';
                                    echo '</tr>';
                                    ?>


                                    <?php
                                endforeach;
                                ?>
                            </table>    
                        </div>
                        <?php
                    endif;
                    ?>


                </div>
            </div>
        </div>

        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>   
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script> 

    </body>
</html>
