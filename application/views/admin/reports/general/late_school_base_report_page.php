<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script lang="javascript" type="text/javascript">
            function printDiv(divID) {
                //Get the HTML of div
                var divElements = document.getElementById(divID).innerHTML;
                //Get the HTML of whole page
                var oldPage = document.body.innerHTML;

                //Reset the page's HTML with div's HTML only
                document.body.innerHTML =
                        "<html><head></head><body>" +
                        divElements + "</body>";

                //Print Page
                window.print();

                //Restore orignal HTML
                document.body.innerHTML = oldPage;
            }

        </script>
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="logo" style="width: 370px;">
                            <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo ?>" alt="Edc Logo" border="0" /> </div>
                            <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
                        </div>
                    </div>

                    <div class="col-md-4 pull-right">
                        <div class="navbar navbar-inverse" role="banner">
                            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
                                        <ul class="dropdown-menu animated fadeInUp">
                                            <li><a href="<?= site_url('admin/password/change'); ?>">Change Password</a></li>
                                            <li><a href="<?= site_url('admin/login/logout'); ?>">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="container">
                    <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
                </div>
                <div class="container" id="printdiv">
                    <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                        THE MINISTRY OF EDUCATION<br/>
                        <img src="<?= get_img('edc_logos/' . $edc_detail->edclogo) ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                        <br/>
                         <span style="font-size: 26px"><?= strtoupper($this->registration_model->getExamName_From_Id($examid)); ?> </span> <?= $examyear; ?>
                    </h4>

                    <br/>
                    <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                    <?php
                $exam_detail = $this->exam_model->get_all($examid);
                $this->data['title'] = 'SCHOOL/CENTRE BASED REPORT';
                $this->db->where('edcid',$this->edcid);
                $this->db->order_by('t_lgas.lganame ASC');
                $lgas = $this->db->get('t_lgas')->result();;

                      foreach($lgas as $lga):

                ?>
                    <?php


                $sql = "select " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname, schoolcode, count(*) as candidatecount from t_candidates
                        inner join t_schools on t_candidates.schoolid = t_schools.schoolid
                        inner join " . ($exam_detail->haszone ? "t_zones" : "t_lgas") . " on t_schools." . ($exam_detail->haszone ? "zoneid" : "lgaid") . " = " . ($exam_detail->haszone ? "t_zones.zoneid" : "t_lgas.lgaid") . "
                        where t_candidates.edcid = '" . $this->edcid . "'
                        and t_candidates.examid = '" . $examid . "'
                        and t_candidates.examyear = '" . $examyear . "'
                        and t_candidates.lgaid = '".$lga->lgaid."'

                        group by " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname, schoolcode
                        order by " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname";

                $report = $this->db->query($sql)->result();
                    ?>

                        <table class="table table-bordered" width = "100%">
                            <caption> <h3> LATE SCHOOL/CENTRE BASED REPORT </h3></caption>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <?php
                                    if ($exam_detail->haszone) echo '<th>ZONE NAME</th>';
                                    else echo '<th>LOCAL GOVT NAME</th>';
                                    ?>
                                    <th>SCHOOL NAME</th>
                                    <th>SCHOOL CODE</th>
                                    <th>CANDIDATES</th>
                                    <th>NO SCANNED</th>
                                    <th>SCANNED BY</th>
                                    <th>QC CHECKED</th>
                                    <th>CONFIRMED</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                 $sn = $cand_sum = 0;
                                if (count($report)) {
                                    foreach ($report as $value) {
                                        $cand_sum += $value->candidatecount;
                                        ?>
                                        <tr>
                                            <td><?= ++$sn; ?></td>
                                           <?php
                                            if ($exam_detail->haszone) echo '<td>'.$value->zonename.'</td>';
                                            else echo '<td>'.$value->lganame.'</td>';
                                            ?>
                                            <td><?= strtoupper($value->schoolname); ?></td>
                                            <td><?= strtoupper($value->schoolcode); ?></td>
                                            <td><?= number_format($value->candidatecount); ?></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                <tr style="font-weight: bold">
                                     <th>&nbsp;</th>
                                     <th>&nbsp;</th>
                                      <th>&nbsp;</th>
                                    <th>TOTAL</th>
                                    <th><?= number_format($cand_sum) ?></th>
                                </tr>
                            </tbody>
                        </table>
                        <div style="page-break-after: always"></div>
                        <?php
 endforeach;?>

                    </div>
                </div>
            </div>
        </div>

        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

    </body>
</html>
