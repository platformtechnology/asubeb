<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .css-vertical{
			color:#000000;
			border:0px solid red;
			writing-mode:tb-rl;
			-webkit-transform:rotate(-90deg);
			-moz-transform:rotate(-90deg);
			-o-transform: rotate(-90deg);
			-ms-transform: rotate(-90deg);
			transform: rotate(-90deg);
			white-space:nowrap;
			display:block;
			bottom:0;
			width:20px;
			height:20px;
			font-family: 'Trebuchet MS', Helvetica, sans-serif;
			font-size:15px;
			/*font-weight:bold;*/
			/*text-shadow: 0px 0px 1px #333;*/
			filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
            }
        </style>
        <script lang="javascript" type="text/javascript">

            function printDiv(divID) {
                //Get the HTML of div
                var divElements = document.getElementById(divID).innerHTML;
                //Get the HTML of whole page
                var oldPage = document.body.innerHTML;

                //Reset the page's HTML with div's HTML only
                document.body.innerHTML =
                        "<html><head></head><body>" +
                        divElements + "</body>";

                //Print Page
                window.print();

                //Restore orignal HTML
                document.body.innerHTML = oldPage;
            }

        </script>
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="logo" style="width: 370px;">
                            <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo ?>" alt="Edc Logo" border="0" /> </div>
                            <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
                        </div>
                    </div>

                    <div class="col-md-4 pull-right">
                        <div class="navbar navbar-inverse" role="banner">
                            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
                                        <ul class="dropdown-menu animated fadeInUp">
                                            <li><a href="<?= site_url('admin/password/change'); ?>">Change Password</a></li>
                                            <li><a href="<?= site_url('admin/login/logout'); ?>">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php
        // GET EXAM DETAIL
        $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>

        <div class="page-content">
            <div class="row">
                <div class="container">
                    <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
                </div>
                <div id="printdiv">

                    <?php
                    // TO OPTIMIZE THE SPEED OF REPORT GENERATION, ALL NECESSARY DETAILS WERE RETRIEVED PRIOR TO PRESENTATION
                    // THIS REPLACED HAVING TO USE FOREACH STATEMENTS FOR EACH CANDIDATE RECORD.
                    $z = array(); // AN ARRAY TO HOLD THE DETAILS OF ZONE(S)
                    foreach ($zones as $zone) {
                        $zoneids[] = $zone->zoneid;
                        $z[$zone->zoneid]['zonename'] = $zone->zonename;
                        $z[$zone->zoneid]['zoneid'] = $zone->zoneid;
                    }


                    // THIS CREATES <TD> ELEMENTS CONTAINING SUBJECTS NAMES AND ALSO AN ARRAY CONTAINING ALL SUBJECT IDS
                    $thesubjects = "";
                    foreach ($allsubjects as $subject) {
                        $allsubjectids[] = $subject->subjectid;
                        $thesubjects.='<td style = "vertical-align:bottom"> <div class="css-vertical"> <p class="css-vertical"> <strong>' . $subject->subjectname. '</strong> </p></div></td>';
                    }

                    //TOTAL SUBJECT COUNT -- FORGIVE THE NOMENCLATURE
                    $all_subj = count($allsubjects);



                    //IF A SINGLE SCHOOLID IS NOT SET, THEN WE KNOW WE ARE WORKING WITH MULTIPLE SCHOOLS

                    if (!isset($schoolid) || @trim($schoolid) == false) {
                        // GET DETAILS OF ALL SCHOOLS AND STORE IN ARRAYS
                        $schools = $this->report_model->get_all_schools_per_zone($zoneids);
                        foreach ($schools as $key => $value) {
                            $schoolids[] = $value->schoolid;
                            $school[$value->schoolid]['lgaid'] = $value->lgaid;
                            $school[$value->schoolid]['zoneid'] = $value->zoneid;
                            $school[$value->schoolid]['schoolname'] = $value->schoolname;
                            $school[$value->schoolid]['schoolcode'] = $value->schoolcode;
                        }

                        // get all candidates for all these particular schools now

                        $candidates = $this->report_model->get_all_candidates_per_lga($lgids, $examid, $examyear);

                        foreach ($candidates as $key => $value) {
                            $candidateids[] = $value->candidateid;
                            $candidate[$value->schoolid][$value->candidateid]['candidateid'] = $value->candidateid;
                            $candidate[$value->schoolid][$value->candidateid]['firstname'] = $value->firstname;
                            $candidate[$value->schoolid][$value->candidateid]['othernames'] = $value->othernames;
                            $candidate[$value->schoolid][$value->candidateid]['examno'] = $value->examno;
                            $candidate[$value->schoolid][$value->candidateid]['gender'] = $value->gender;
                        }


                        // get all candidates remark for all in one place
                        // the if statement works for GPT students
                        if (!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone) {

                            $remarks = $this->report_model->get_subject_remark_for_candidates($allsubjectids, $candidateids, $examid, $examyear);
                        } else {

                            $remark_detail = $this->report_model->get_subject_remark_for_candidates($allsubjectids, $candidateids, $examid, $examyear);


                            foreach ($remark_detail as $key => $value) {

                                $remarks[$value->candidateid][$value->subjectid] = $remarks[$value->candidateid][$value->subjectid] = '<strong>' . (intval(count($value) ? $value->exam_score : 0) < 0 ? '<span style="font-size: 10px; color: crimson;">ABS </span>' : (count($value) ? strtoupper($value->grade) : '***') ) . '</strong>';

                                $remarks[$value->candidateid]['subjectkeys'][] = $value->subjectid;
                            }

                            //get all school pass status in one place before the loop
                            $pass = $this->report_model->getPasscountBasedonCriteriaPerCandidate($candidateids, $examid, $examyear, $cutoff);
                        }
                    }
                    else {

                        // SCHOOLID IS SET
                        $schools = $schooldetails;
                        $schoolids[] = $schooldetails[0]->schoolid; // GET SCHOOL ID FROM THE STD OBJECT
                        $school[$schooldetails[0]->schoolid]['lgaid'] = $schooldetails[0]->lgaid;
                        $school[$schooldetails[0]->schoolid]['zoneid'] = $schooldetails[0]->zoneid;
                        $school[$schooldetails[0]->schoolid]['schoolname'] = $schooldetails[0]->schoolname;
                        $school[$schooldetails[0]->schoolid]['schoolcode'] = $schooldetails[0]->schoolcode;


                        // get all candidates for all the particular schools now
                        $this->db->order_by('examno ASC');
                        $candidates = $this->db->get_where('t_candidates', array('schoolid' => $schooldetails[0]->schoolid, 'examid' => $examid, 'examyear' => $examyear))->result();


                        foreach ($candidates as $key => $value) {
                            $candidateids[] = $value->candidateid;
                            $candidate[$value->schoolid][$value->candidateid]['candidateid'] = $value->candidateid;
                            $candidate[$value->schoolid][$value->candidateid]['firstname'] = $value->firstname;
                            $candidate[$value->schoolid][$value->candidateid]['othernames'] = $value->othernames;
                            $candidate[$value->schoolid][$value->candidateid]['examno'] = $value->examno;
                            $candidate[$value->schoolid][$value->candidateid]['gender'] = $value->gender;
                        }


                        // the if statement works for GPT students
                        if (!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone) {

                            $remarks = $this->report_model->get_subject_remark_for_candidates($allsubjectids, $candidateids, $examid, $examyear);
                        }
                        else {
                            // get all candidates remark for all in one place
                            $remark_detail = $this->report_model->get_subject_remark_for_candidates($allsubjectids, $candidateids, $examid, $examyear);

                            foreach ($remark_detail as $key => $value) {

                                $remarks[$value->candidateid][$value->subjectid] = $remarks[$value->candidateid][$value->subjectid] = '<strong>' . (intval(count($value) ? $value->exam_score : 0) < 0 ? '<span style="font-size: 10px; color: crimson;">ABS </span>' : ( (count($value) && $value->ca_score != 0 && $value->exam_score != 0)  ? strtoupper($value->grade) : '') ) . '</strong>';
                                $remarks[$value->candidateid]['subjectkeys'][] = $value->subjectid;
                            }

                            //get all school pass status in one place before the loop
                            $pass = $this->report_model->getPasscountBasedonCriteriaPerCandidate($candidateids, $examid, $examyear, $cutoff);
                        }
                    }

                    $registered = $this->report_model->get_registered_subjects($candidateids, $examid, $examyear);

                    foreach ($registered as $key => $value)
					{
                        $registered_subj[$value->candidateid]['subjects'][] = $value->subjectid;
                    }


                    foreach ($zoneids as $key => $zna):


                        if (count($schools)):
                            ?>

                            <div class="row">
                                <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                                    ANAMBRA STATE MINISTRY OF EDUCATION<br/>
                                    <img src="<?= $edc_logo ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                                    <br/>
                                     RESULT FOR BASIC EDUCATION CERTIFICATE EXAMINATION (<span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span>) <?= $examyear; ?> <br/>
                                    <!-- <br/> <?php //strtoupper($z[$zna]['zonename']); ?> ZONE -->

                                </h4>


                                <?php
                                foreach ($schoolids as $dschool):
                                    if (isset($candidate[$dschool]) && count($candidate[$dschool])):
                                        ?>
                                        <br/>
                                        <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                                            <p style="font-size:16px;">
                                                <strong>ZONE:</strong> <?= $z[$zna]['zonename']; ?> <br/>
                                                <strong>SCHOOL:</strong> <?= $school[$dschool]['schoolname']; ?> <strong>
											   <br/>CODE:</strong> <?= $school[$dschool]['schoolcode'] ?><br/>
                                                <strong>TOTAL CANDIDATE(S):</strong> <?= count($candidate[$dschool]); ?><br/>
                                            </p>
                                            <table width="100%" border = "1">
                                                <tr style="font-weight: bold;">
                                                    <td rowspan="2">SN</td>
                                                    <td rowspan="2">EXAM NO</td>
                                                    <td rowspan="2">NAME</td>
                                                    <!--<td rowspan="2">GENDER</td>-->
                                                    <td rowspan="<?= 2 + count($candidate[$dschool]); ?>" >&nbsp;</td>
                                                    <td colspan="<?= count($allsubjects); ?>" align="center">SUBJECTS</td>
                                                    <!--<td rowspan="<?php //2 + count($candidate[$dschool]); ?>" >&nbsp;</td>-->
                                                    <td rowspan="2">STATUS</td>
                                                </tr>
                                                <!--SUBJECTS HEADER ROW-->
                                                <tr style = "height:200px;vertical-align = 'none'">
                                                   <!---- DISPLAY SUBJECTS IN A TABLE HEADER------------------------------->
                                                    <?php
                                                    echo $thesubjects;
                                                    ?>

                                                </tr>
                                                <!-- END SUBJECTS HEADER ROW-->
                                                <?php
                                                $row = 0;
                                                foreach ($candidate[$dschool] as $candidateid => $dcandidate):
                                                    ++$row;

                                                    $name = ucwords(strtolower($dcandidate['firstname'])) . ' ' . ucwords(strtolower($dcandidate['othernames']));

                                                    //PRACTICAL ROW
                                                    echo '<tr>';
                                                    echo '<td>' . $row . '</td>';
                                                    echo '<td style="font-size:15px;">' . $dcandidate['examno'] . '</td>';
                                                    echo '<td style="font-size:15px;">' . $name . '</td>';
                                                   // echo '<td>' . $dcandidate['gender'] . '</td>';

                                                    foreach ($allsubjects as $subjects) {
                                                        //check if student sat for the exam so that u dont access non existing result
                                                        if (isset($remarks[$dcandidate['candidateid']]) && in_array($subjects->subjectid, $remarks[$dcandidate['candidateid']]['subjectkeys'])) {
                                                            echo '<td style="font-size:15px;">' . $remarks[$dcandidate['candidateid']][$subjects->subjectid] . '</td>';
                                                        } else {


                                                            if (isset($registered_subj[$dcandidate['candidateid']]) && !in_array($subjects->subjectid, $registered_subj[$dcandidate['candidateid']]['subjects'])) {
                                                                echo '<td style="font-size: 10px; color: crimson;"><strong></strong></td>';
                                                            } else {
                                                                echo '<td style="font-size: 10px;"><strong>  </strong></td>';
                                                            }
                                                        }
                                                    }

                                                    //If exam is GPT
                                                    if (!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone) {
                                                        echo '<td><strong>';
                                                        if (isset($remarks[$dcandidate['candidateid']]['grade'])) {
                                                            echo $remarks[$dcandidate['candidateid']]['grade'];
                                                        } else {
                                                            echo '****';
                                                        };
                                                        echo '</strong></td>';
                                                    }
                                                    //if exam is not GPT
                                                    else {
                                                        echo '<td><strong>';
                                                        if (isset($pass[$dcandidate['candidateid']])) {
                                                            echo $pass[$dcandidate['candidateid']];
                                                        }
                                                        echo '</strong></td>';
                                                    }
                                                    echo '</tr>';
                                                    ?>

                                                <?php endforeach; ?>

                                            </table>
                                            <div align = "center">
                                                <br/>
                                                <br/>
                                                <br/>

                                             __________________________________________ <br/>
                                            Director's Signature and Stamp </div>
                                        </div>
                                        <p style="page-break-after: always;">
                                            <?php
                                        endif;
                                    endforeach;
                                    ?>
                            </div>


                            <?php
                        endif;
                    endforeach;
                    ?>

                </div>
            </div>
        </div>

        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

    </body>
</html>
