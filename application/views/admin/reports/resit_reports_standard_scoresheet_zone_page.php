<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php //echo base_url('resources/bootstrap/css/bootstrap.min.css');     ?>" rel="stylesheet">
        <link href="<?php //echo base_url('resources/css/styles.css');     ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .css-vertical{
                filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
                -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
                -ms-transform: rotate(-90.0deg);  /* IE9+ */
                -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
                -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                transform: rotate(-90.0deg);  /* Standard */
                white-space:nowrap;
            }
            .css-vertical-text {
                color:#333;
                border:0px solid red;
                writing-mode:tb-rl;
                -webkit-transform:rotate(-90deg);
                -moz-transform:rotate(-90deg);
                -o-transform: rotate(-90deg);
                -ms-transform: rotate(-90deg);
                transform: rotate(-90deg);
                white-space:nowrap;
                display:block;
                bottom:0;
                width:20px;
                height:20px;
                font-family: 'Trebuchet MS', Helvetica, sans-serif;
                font-size:11px;
                font-weight:bold;
                /*text-align:left;*/
                /*text-shadow: 0px 0px 1px #333;*/

                filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);	
            }
        </style>

    </head>
    <body>

        <div class="page-content">
            <div class="row">
                <div id="printdiv">
                    <?php
                    foreach ($standard_report_scores as $schoolid => $rdata):
                        $male_candidates = isset($rdata['MALE']) ? $rdata['MALE'] : array();
                        $female_candidates = isset($rdata['FEMALE']) ? $rdata['FEMALE'] : array();
                        $schoodata = $this->registration_model->get_school($schoolid, true);

                        if($separategender == 0) $male_candidates = array_merge ($male_candidates, $female_candidates);
                        if (count($male_candidates)):
                            ?>
                            <div class="row">

                                <center>
                                    <table border="0" align="center">
                                        <tr>
                                            <td>
                                                <img src="<?= $edc_logo ?>" alt="Edc Logo" />
                                            </td>
                                            <td>
                                                <h4 style="text-align: center; font-size: 20px; color: #0077b3; font-weight: bold">
                                                    THE MINISTRY OF EDUCATION<br/>
                                                    <?= ucwords(strtoupper($edc_detail->edcname)); ?><br/>
                                                    <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>
                                                </h4>
                                            </td>
                                        </tr>
                                    </table>
                                </center>



                                <br/>
                                <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                                    <p style="font-size: 14px">
                                        <strong>ZONE:</strong> <?= $this->registration_model->get_zone($schoodata->zoneid); ?> <br/>
                                        <strong>SCHOOL:</strong> <?= $schoodata->schoolname; ?> <br/>
                                        <strong>SCHOOL CODE:</strong> <?= $schoodata->schoolcode; ?><br/>

                                    </p>
                                    <table width="100%" border="1" cellpadding="5" cellspacing="1" style="border-collapse:collapse; border-color:#000; font:Verdana, Geneva, sans-serif; font-size:10px">
                                        <tr style="font-weight: bold;">
                                            <td rowspan="2">SN</td>
                                            <td rowspan="2">EXAMNO</td>
                                            <td rowspan="2">NAME</td>
                                            <!--<td rowspan="2">GENDER</td>--->
                                            <td rowspan="<?= 2 + count($male_candidates); ?>" >&nbsp;</td>
                                            <td colspan="<?= count($allsubjects); ?>" align="center">SUBJECTS</td>
                                            <td rowspan="<?= 2 + count($male_candidates); ?>" >&nbsp;</td>
                                            <?= $examdetail->hasposting ? '<td rowspan="2">TOTAL</td>' : ''; ?>
                                            <?= !$excludefailure ? '<td rowspan="2">STATUS</td>' : ''; ?>
                                            <?= $examdetail->hasposting ? '<td rowspan="2" width="30%">SCHOOL POSTED</td>' : ''; ?>
                                        </tr>
                                        <!--SUBJECTS HEADER ROW-->
                                        <tr>

                                            <?php
                                            foreach ($allsubjects as $subject) {
                                                echo '<td valign="bottom" height="100"> <p class="css-vertical-text"><strong>' . ucwords(strtolower($subject->subjectname)) . '</strong></p></td>';
                                            }
                                            ?>

                                        </tr>
                                        <!-- END SUBJECTS HEADER ROW-->
                                        <?php
                                        $row = 0;
                                        foreach ($male_candidates as $candidate):

                                            $pass = $this->report_model->get_passcount_basedon_criteria_per_candidate($candidate['candidateid'], $examid, $examyear, $cutoff);
                                            if ($excludefailure) {
                                                if ($pass != 'PASSED')
                                                    continue;
                                            }

                                            echo '<tr style="font-size: 13px;">';
                                            echo '<td>' . ++$row . '</td>';
                                            echo '<td> ' . $candidate['examno'] . '</td>';
                                            echo '<td>' . $candidate['fullname'] . '</td>';
                                            //echo '<td>' . $candidate['gender'] . '</td>';

                                            if (count($allsubjects)) {

                                                $totalScores = 0;
                                                //$registered_subjects = $this->reg_subjects_model->get_registered_subjects($candidate['candidateid'], $examid, $examyear);
//                                                if (count($registered_subjects)) {
                                                foreach ($allsubjects as $subjects) {
//                                                        foreach ($registered_subjects as $reg_subject) {
                                                    $found = false;
//                                                            if ($reg_subject->subjectid == $subjects->subjectid) {


                                                    if ($candidate['subjects'][$subjects->subjectid] < 0)
                                                        echo '<td><strong><span style="font-size: 10px; color: crimson;">--</span></strong></td>';
                                                    else {

                                                        echo '<td><strong> ' . $candidate['subjects'][$subjects->subjectid] . '</strong></td>';
                                                        $totalScores += $candidate['subjects'][$subjects->subjectid];
                                                    }
                                                    $found = true;
//                                                                    break;
//                                                            }
//                                                        }
//                                                        if (!$found)
//                                                            echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                                }
//                                                } else
//                                                    echo '<td title="" style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                            }

                                            echo $examdetail->hasposting ? '<td><strong>' . $totalScores . '</strong></td>' : '';
                                            echo!$excludefailure ? '<td><strong>' . $pass . '</strong></td>' : '';

                                            if ($examdetail->hasposting) {
                                                if ($pass == 'PASSED') {
                                                    $postedto = $this->registration_model->get_school($candidate['placement']);
                                                    echo '<td><strong>' . $postedto . '</strong></td>';
                                                } else
                                                    echo '<td><strong>-------</strong></td>';
                                            }

                                            echo '</tr>';

                                        endforeach;
                                        ?>

                                    </table>   
                                    <div style="page-break-inside:avoid;page-break-after:auto;">
                                        <center>
                                            <table align="center">
                                                <tr style="page-break-inside:avoid;page-break-after:auto;">
                                                    <td align="left">
                                                        <br>
                                                        <br>
                                                        <br>

                                                        <br>_______________________________________<br>&nbsp;&nbsp;&nbsp;THE DIRECTOR'S SIGNATURE AND STAMP<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= ''//date('l dS \of F Y')  ?></td>
                                                </tr>
                                            </table>
                                        </center>
                                    </div>
                                </div>
                            </div>

                            <?php
                        endif;
                        //End display of male candidates
                        ?>
                    <div style="page-break-after:always"></div>
                    <?php
                        
                        if (count($female_candidates) && ($separategender == 1)):
                            ?>
                            
                <!--                            <p><br style="page-break-before: always;" clear="all" /></p>
                                            <p style="page-break-before: always;"></p>-->
                            <div class="row">

                                <center>
                                    <table border="0" align="center">
                                        <tr>
                                            <td>
                                                <img src="<?= $edc_logo ?>" alt="Edc Logo" />
                                            </td>
                                            <td>
                                                <h4 style="text-align: center; font-size: 20px; color: #0077b3; font-weight: bold">
                                                    THE MINISTRY OF EDUCATION<br/>
                                                    <?= ucwords(strtoupper($edc_detail->edcname)); ?><br/>
                                                    <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>
                                                </h4>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                                <br/>
                                <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                                    <p style="font-size: 14px">
                                        <strong>ZONE:</strong> <?= $this->registration_model->get_zone($schoodata->zoneid); ?> <br/>
                                        <strong>SCHOOL:</strong> <?= $schoodata->schoolname; ?> <br/>
                                        <strong>SCHOOL CODE:</strong> <?= $schoodata->schoolcode; ?><br/>

                                    </p>
                                    <table width="100%" border="1" cellpadding="5" cellspacing="1" style="border-collapse:collapse; border-color:#000; font:Verdana, Geneva, sans-serif; font-size:10px">
                                        <tr style="font-weight: bold;">
                                            <td rowspan="2">SN</td>
                                            <td rowspan="2">EXAMNO</td>
                                            <td rowspan="2">NAME</td>
                                            <td rowspan="2">GENDER</td>
                                            <td rowspan="<?= 2 + count($female_candidates); ?>" >&nbsp;</td>
                                            <td colspan="<?= count($allsubjects); ?>" align="center">SUBJECTS</td>
                                            <td rowspan="<?= 2 + count($female_candidates); ?>" >&nbsp;</td>
                                            <?= $examdetail->hasposting ? '<td rowspan="2">TOTAL</td>' : ''; ?>
                                            <?= !$excludefailure ? '<td rowspan="2">STATUS</td>' : ''; ?>
                                            <?= $examdetail->hasposting ? '<td rowspan="2" width="30%">SCHOOL POSTED</td>' : ''; ?>
                                        </tr>
                                        <!--SUBJECTS HEADER ROW-->
                                        <tr>

                                            <?php
                                            foreach ($allsubjects as $subject) {
                                                echo '<td valign="bottom" height="100"> <p class="css-vertical-text"><strong>' . ucwords(strtolower($subject->subjectname)) . '</strong></p></td>';
                                            }
                                            ?>

                                        </tr>
                                        <!-- END SUBJECTS HEADER ROW-->
                                        <?php
                                        $row = 0;
                                        foreach ($female_candidates as $candidate):

                                            $pass = $this->report_model->get_passcount_basedon_criteria_per_candidate($candidate['candidateid'], $examid, $examyear, $cutoff);
                                            if ($excludefailure) {
                                                if ($pass != 'PASSED')
                                                    continue;
                                            }

                                            echo '<tr style="font-size: 13px;">';
                                            echo '<td>' . ++$row . '</td>';
                                            echo '<td>' . $candidate['examno'] . '</td>';
                                            echo '<td>' . $candidate['fullname'] . '</td>';
                                            echo '<td>' . $candidate['gender'] . '</td>';

                                            if (count($allsubjects)) {

                                                $totalScores = 0;
//                                                $registered_subjects = $this->reg_subjects_model->get_registered_subjects($candidate['candidateid'], $examid, $examyear);
//                                                if (count($registered_subjects)) {
                                                foreach ($allsubjects as $subjects) {
//                                                        foreach ($registered_subjects as $reg_subject) {
                                                    $found = false;
//                                                            if ($reg_subject->subjectid == $subjects->subjectid) {

                                                    if ($candidate['subjects'][$subjects->subjectid] < 0)
                                                        echo '<td><strong><span style="font-size: 10px; color: crimson;">--</span></strong></td>';
                                                    else {

                                                        echo '<td><strong> ' . $candidate['subjects'][$subjects->subjectid] . '</strong></td>';
                                                        $totalScores += $candidate['subjects'][$subjects->subjectid];
                                                    }
                                                    $found = true;
//                                                                break;
//                                                            }
//                                                        }
//                                                        if (!$found)
//                                                            echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                                }
//                                                } else
//                                                    echo '<td title="" style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                                            }

                                            echo $examdetail->hasposting ? '<td><strong>' . $totalScores . '</strong></td>' : '';
                                            echo!$excludefailure ? '<td><strong>' . $pass . '</strong></td>' : '';

                                            if ($examdetail->hasposting) {
                                                if ($pass == 'PASSED') {
                                                    $postedto = $this->registration_model->get_school($candidate['placement']);
                                                    echo '<td><strong>' . $postedto . '</strong></td>';
                                                } else
                                                    echo '<td><strong>-------</strong></td>';
                                            }

                                            echo '</tr>';

                                        endforeach;
                                        ?>

                                    </table>  
                                    <div style="page-break-inside:avoid;page-break-after:auto;">
                                        <center>
                                            <table align="center">
                                                <tr style="page-break-inside:avoid;page-break-after:auto;">
                                                    <td align="left">
                                                        <br>
                                                        <br>
                                                        <br>

                                                        <br>_______________________________________<br>&nbsp;&nbsp;&nbsp;THE DIRECTOR'S SIGNATURE AND STAMP<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= ''//date('l dS \of F Y')  ?></td>
                                                </tr>
                                            </table>
                                        </center>
                                    </div>
                                </div>
                            </div>

                            <?php
                        endif;
                        //end display of FEMALE candidates
                        ?>
                        <?php
                    endforeach;
                    ?>

                </div>
            </div>
        </div>

        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>   
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script> 

    </body>
</html>
