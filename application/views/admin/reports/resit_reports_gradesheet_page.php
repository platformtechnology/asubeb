<!DOCTYPE html>
<html>
    <head>
        <title><?= $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('resources/css/styles.css'); ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .css-vertical{
                filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
                -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
                -ms-transform: rotate(-90.0deg);  /* IE9+ */
                -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
                -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                transform: rotate(-90.0deg);  /* Standard */
                white-space:nowrap;
            }
        </style>
        <script lang="javascript" type="text/javascript">
            window.addEventListener('load', function () {
                var rotates = document.getElementsByClassName('css-vertical');
                for (var i = 0; i < rotates.length; i++) {
                    rotates[i].style.height = (rotates[i].offsetWidth) + 'px';
                }
            });
            function printDiv(divID) {
                //Get the HTML of div
                var divElements = document.getElementById(divID).innerHTML;
                //Get the HTML of whole page
                var oldPage = document.body.innerHTML;

                //Reset the page's HTML with div's HTML only
                document.body.innerHTML =
                        "<html><head></head><body>" +
                        divElements + "</body>";

                //Print Page
                window.print();

                //Restore orignal HTML
                document.body.innerHTML = oldPage;
            }

        </script>
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="logo" style="width: 370px;">
                            <div style="float: left; margin-right: 10px;"><img src="<?= $edc_logo ?>" alt="Edc Logo" border="0" /> </div>
                            <span style="color: white; font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtolower($edc_detail->edcname)); ?></span>
                        </div>
                    </div>

                    <div class="col-md-4 pull-right">
                        <div class="navbar navbar-inverse" role="banner">
                            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('user_name'); ?> <b class="caret"></b></a>
                                        <ul class="dropdown-menu animated fadeInUp">
                                            <li><a href="<?= site_url('admin/password/change'); ?>">Change Password</a></li>
                                            <li><a href="<?= site_url('admin/login/logout'); ?>">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>


        <div class="page-content">
            <div class="row">
                <div class="container">
                    <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
                </div>
                <div id="printdiv">

                    <?php
                    $lg = array();


                    foreach ($lgas as $lga) {
                        $lgids[] = $lga->lgaid;
                        $lg[$lga->lgaid]['lgaid'] = $lga->lgaid;
                        $lg[$lga->lgaid]['zonename'] = $lga->zonename;
                        $lg[$lga->lgaid]['lginitials'] = $lga->lgainitials;
                        $lg[$lga->lgaid]['lgname'] = $lga->lganame;
                    }


                    $thesubjects = "";

                    foreach ($allsubjects as $subject) {
                        $allsubjectids[] = $subject->subjectid;
                        $thesubjects.='<td valign="bottom" height="100"> <p class="css-vertical-text"><strong>' . ucwords(strtolower($subject->subjectname)) . '</strong></p></td>';
                    }


                    $all_subj = count($allsubjects);

                    // if school is not set

                    if (!isset($schoolid) || @trim($schoolid) == false) {

                        $schools = $this->resit_report_model->get_schools_details_per_lga($lgids);

                        foreach ($schools as $key => $value) {
                            $schoolids[] = $value->schoolid;
                            $school[$value->schoolid]['lgaid'] = $value->lgaid;
                            $school[$value->schoolid]['zoneid'] = $value->zoneid;
                            $school[$value->schoolid]['schoolname'] = $value->schoolname;
                            $school[$value->schoolid]['schoolcode'] = $value->schoolcode;
                        }

                        // get all candidates for all these particular schools now

                        $candidates = $this->resit_report_model->get_all_candidates_per_lga($lgids, $examid, $examyear);


                        foreach ($candidates as $key => $value) {
                            $candidateids[] = $value->candidateid;
                            $candidate[$value->schoolid][$value->candidateid]['candidateid'] = $value->candidateid;
                            $candidate[$value->schoolid][$value->candidateid]['firstname'] = $value->firstname;
                            $candidate[$value->schoolid][$value->candidateid]['othernames'] = $value->othernames;
                            $candidate[$value->schoolid][$value->candidateid]['examno'] = $value->examno;
                            $candidate[$value->schoolid][$value->candidateid]['gender'] = $value->gender;
                        }


                        // get all candidates remark for all in one place

                        $remark_detail = $this->resit_report_model->get_subject_remark_for_candidates($allsubjectids, $candidateids, $examid, $examyear);


                        foreach ($remark_detail as $key => $value) {
                            $remarks[$value->candidateid][$value->subjectid] = $remarks[$value->candidateid][$value->subjectid] = '<strong>' . (intval(count($value) ? $value->exam_score : 0) < 0 ? '<span style="font-size: 10px; color: crimson;"> </span>' : (count($value) ? strtoupper($value->grade) : '***') ) . '</strong>';

                            $remarks[$value->candidateid]['subjectkeys'][] = $value->subjectid;
                        }

                        //get all school pass status in one place before the loop
                        $pass = $this->resit_report_model->getPasscountBasedonCriteriaPerCandidate($candidateids, $examid, $examyear, $cutoff);
                    } else {

                        $schools = $schooldetails;

                        $schoolids[] = $schooldetails[0]->schoolid;
                        $school[$schooldetails[0]->schoolid]['lgaid'] = $schooldetails[0]->lgaid;
                        $school[$schooldetails[0]->schoolid]['zoneid'] = $schooldetails[0]->zoneid;
                        $school[$schooldetails[0]->schoolid]['schoolname'] = $schooldetails[0]->schoolname;
                        $school[$schooldetails[0]->schoolid]['schoolcode'] = $schooldetails[0]->schoolcode;


                        // get all candidates for all the particular schools now
                        $this->db->order_by('examno asc');
                        $candidates = $this->db->get_where('t_candidates', array('schoolid' => $schooldetails[0]->schoolid, 'examid' => $examid, 'examyear' => $examyear))->result();


                        foreach ($candidates as $key => $value) {
                            $candidateids[] = $value->candidateid;
                            $candidate[$value->schoolid][$value->candidateid]['candidateid'] = $value->candidateid;
                            $candidate[$value->schoolid][$value->candidateid]['firstname'] = $value->firstname;
                            $candidate[$value->schoolid][$value->candidateid]['othernames'] = $value->othernames;
                            $candidate[$value->schoolid][$value->candidateid]['examno'] = $value->examno;
                            $candidate[$value->schoolid][$value->candidateid]['gender'] = $value->gender;
                        }

                        // get all candidates remark for all in one place

                        $remark_detail = $this->resit_report_model->get_subject_remark_for_candidates($allsubjectids, $candidateids, $examid, $examyear);


                        foreach ($remark_detail as $key => $value) {

                            $remarks[$value->candidateid][$value->subjectid] = '<strong>' . (intval(count($value) ? $value->exam_score : 0) <= 0 ? '<span style="font-size: 10px; color: crimson;"> </span>' : (count($value) ? strtoupper($value->grade) : '***') ) . '</strong>';
                            $remarks[$value->candidateid]['subjectkeys'][] = $value->subjectid;
                        }

                        //get all school pass status in one place before the loop
                        $pass = $this->resit_report_model->getPasscountBasedonCriteriaPerCandidate($candidateids, $examid, $examyear, $cutoff);
                    }

                    $registered = $this->resit_report_model->get_registered_subjects($candidateids, $examid, $examyear);

                    foreach ($registered as $key => $value) {
                        $registered_subj[$value->candidateid]['subjects'][] = $value->subjectid;
                    }

                    foreach ($lg as $lgid => $lga):


                        if (count($schools)):
                            ?>
                            <div class="row">
                                <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                                    THE MINISTRY OF EDUCATION<br/>
                                    <img src="<?= $edc_logo ?>" alt="Edc Logo" /> <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                                    <br/>
                            <?= $title ?> FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear; ?> <br/>
                                    <br/> <?= strtoupper($lga['lgname']); ?> LOCAL GOVERNMENT AREA

                                </h4>
                            <?php
                            foreach ($schoolids as $dschool):


                                if (isset($candidate[$dschool]) && count($candidate[$dschool])):
                                    ?>
                                        <br/>
                                        <div class="col-md-12" style="background-color: white; padding: 20px; border-radius: 10px">
                                            <p>
                                                <strong>ZONE:</strong> <?= $lga['zonename']; ?> <br/>
                                                <strong>LGA:</strong> <?= $lga['lgname']; ?><br/>
                                                <strong>SCHOOL:</strong> <?= $school[$dschool]['schoolname']; ?> <strong> Code:</strong> <?= $school[$dschool]['schoolcode'] ?><br/>
                                                <strong>TOTAL CANDIDATE:</strong> <?= count($candidate[$dschool]); ?><br/>
                                            </p>
                                            <table width="100%" class="table table-bordered table-condensed">
                                                <tr style="font-weight: bold;">
                                                    <td rowspan="2">SN</td>
                                                    <td rowspan="2">EXAMNO</td>
                                                    <td rowspan="2">NAME</td>
                                                    <td rowspan="2">GENDER</td>
                                                    <td rowspan="<?= 2 + count($candidate[$dschool]); ?>" >&nbsp;</td>
                                                    <td colspan="<?= count($allsubjects); ?>" align="center">SUBJECTS</td>
                                                    <td rowspan="<?= 2 + count($candidate[$dschool]); ?>" >&nbsp;</td>
                                                    <td rowspan="2"></td>
                                                </tr>
                                                <!--SUBJECTS HEADER ROW-->
                                                <tr>

                <?php
                echo $thesubjects;
                ?>

                                                </tr>
                                                <!-- END SUBJECTS HEADER ROW-->
                <?php
                $row = 0;
                foreach ($candidate[$dschool] as $candidateid => $dcandidate):
                    ++$row;

                    $name = ucwords(strtolower($dcandidate['firstname'])) . ' ' . ucwords(strtolower($dcandidate['othernames']));
                        if(count($remarks[$dcandidate['candidateid']])) {
                    //PRACTICAL ROW
                    echo '<tr>';
                    echo '<td>' . $row . '</td>';

                    echo '<td>' . $dcandidate['examno'] . '</td>';

                    echo '<td>' . $name
                            .'</td>';

                    echo '<td>' . $dcandidate['gender'] . '</td>';


                    foreach ($allsubjects as $subjects) {

                        //check if student sat for the exam so that u dont access non existing result
                        if (isset($remarks[$dcandidate['candidateid']])) {
                            echo '<td>' . $remarks[$dcandidate['candidateid']][$subjects->subjectid] . '</td>';
                        } else {


                            if (isset($registered_subj[$dcandidate['candidateid']]) && !in_array($subjects->subjectid, $registered_subj[$dcandidate['candidateid']]['subjects'])) {
                                echo '<td style="font-size: 10px; color: crimson;"><strong>DR</strong></td>';
                            } else {
                                echo '<td style="font-size: 10px; color: crimson;"><strong></strong></td>';
                            }
                        }
                    }

                    echo '<td><strong>';
                    //if (isset($pass[$dcandidate['candidateid']])) {
                      //  echo $pass[$dcandidate['candidateid']];
                    //};
                    echo '</strong></td>';

                    echo '</tr>';
                        }
                    ?>


                                                <?php endforeach; ?>
                                                <tr>

                                                    <td align = "center" colspan = '16'> <img src="<?= base_url(str_replace('./', '', config_item('edc_logo_path')) . $this->edcid . '_stamp.png') ?>" alt="Stamp" style="width: 150px; height: 120px" /> </td>
                                                </tr>
                                            </table>
                                            
                                        </div>
                                        
                                        <p style="page-break-after: always;">
                                                <?php
                                            endif;
                                        endforeach;
                                        ?>
                            </div>

                                        <?php
                                    endif;
                                endforeach;
                                ?>

                </div>
            </div>
        </div>

        <script src="<?= base_url('resources/js/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('resources/js/custom.js'); ?>"></script>

    </body>
</html>
