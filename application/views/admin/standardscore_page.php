
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-6 panel-primary">
        <div class="content-box-header panel-heading">
            <div class="panel-title">
                <strong><i class="glyphicon glyphicon-edit"></i> Select Exam To Standardise Scores For</strong>
            </div>
        </div>
        <div class="content-box-large box-with-header">
            <?php if ($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
            <?php if (strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
            <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Exam Type:</label>
                <div class="col-sm-8">
                    <?php
                    $select_options = array();
                    foreach ($exams as $exam) {
                        $select_options[$exam->examid] = $exam->examname;
                    }
                    $select_options[''] = "..:: Select Exam Type ::..";
                    echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : '', 'class="form-control" required="required"');
                    ?>
                </div>
            </div>   
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Exam Year:</label>
                <div class="col-sm-8">
                    <?php
                    $already_selected_value = $this->input->post('examyear') ? $this->input->post('examyear') : $activeyear;
                    $start_year = $activeyear;
                    print '<select name="examyear" class="form-control">';
                    print '<option value="" ' . ($already_selected_value == '' ? 'selected="selected"' : '') . '>Select Exam Year</option>';
                    for ($x = $start_year; $x <= $activeyear; $x++) {
                        print '<option value="' . $x . '"' . ($x == $already_selected_value ? ' selected="selected"' : '') . '>' . $x . '</option>';
                    }
                    print '</select>';
                    ?>
                </div>
            </div>  
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-arrow-right"></i> Standardise Score</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<?php if (isset($standardscoredetail)): ?>

    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-body" id="printdiv">
                    <?php if (isset($success)) echo '<div class="alert alert-info">'.$success.'</div>'; ?>
                    <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                        SCORE STANDARDISATION OF
                        <br/>
                        <?= $examyear; ?> <span><?= strtoupper($examdetail->examdesc) . ' (' . strtoupper($examdetail->examname) . ') '; ?> </span><br/>
                        <hr/>
                    </h4>
                    <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                    <input type="hidden" name="examid" value="<?= $examdetail->examid ?>" />
                    <input type="hidden" name="examyear" value="<?= $examyear ?>" />
                    <div class="col-md-offset-3 col-md-6">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>CA:</td>
                                <td>
                                    <?php if ($examdetail->hasca) { ?>
                                        <input type="text" class="input-sm" name="standardca" value="<?= set_value('standardca', $standardscoredetail->standardca); ?>" required="required"/>%
                                        <?php
                                    } else
                                        echo '<strong>NO CA</strong>';
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>EXAM:</td>
                                <td>
                                    <input type="text" class="input-sm" name="standardexam" value="<?= set_value('standardexam', $standardscoredetail->standardexam); ?>" required="required"/>%
                                </td>
                            </tr>
                            <tr>
                                <td>PRACTICAL:</td>
                                <td>
                                    <?php if ($examdetail->haspractical) { ?>
                                        <input type="text" class="input-sm" name="standardpractical" value="<?= set_value('standardpractical', $standardscoredetail->standardpractical); ?>" required="required"/>%
                                        <?php
                                    } else
                                        echo '<strong>NO PRACTICAL</strong>';
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-cog"></i> Set Standard</button>
                            </div>
                        </div>
                    </div>

                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
