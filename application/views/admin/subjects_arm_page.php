<div class="row">

    <div class="col-md-6 panel-primary">
         
              
                    <div class="content-box-header panel-heading">
                        <div class="panel-title"><i class="glyphicon glyphicon-search"></i> Select Exam And Year</div>
                    </div>
                 <div class="content-box-large box-with-header">
                   <div class="panel-body">
                       <?php if(validation_errors()) echo get_error(validation_errors()); ?>
                       <?= form_open(site_url('admin/subjectarms/search'), 'class="form-inline" role="form"'); ?>
                               <div class="form-group col-sm-4">
                                    <?php 
                                      $select_options = array();
                                      foreach ($exams as $exam) {
                                          $select_options[$exam->examid] = $exam->examname;
                                      }
                                      $select_options[''] = "-- Select Exam --";
                                      echo form_dropdown('examid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : (isset($examid) ? $examid : ''), 'class="form-control" required');
                                    ?>
                            </div>
                            <div class="form-group col-sm-offset-2 col-lg-4">
                                 <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-search"></i> Search</button>
                              </div>   
                       <?= form_close(); ?>
                   </div>
                </div>
            
        
    </div>
    
</div>

<?php if(count($subjects)): ?>
<div class="row">
    <div class="col-md-12">
       
        <div class="content-box-header">
            <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> Setup Subject Arms </div>
        </div>
         <div class="content-box-large box-with-header">
            <div class="panel-body">
                <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                
                <?= form_open(site_url('admin/subjectarms/save/'.$examid.'/'.$arm_detail->armid), 'class="form-horizontal" role="form"'); ?>
                    <div class="col-md-5">
                        
                        <br/>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Parent Subject:</label>
                            <div class="col-sm-8">
                                <?php 
//                                  $select_options = array();
//                                  foreach ($subjects as $subject) {
//                                      $select_options[$exam->examid] = strtoupper($subject->subjectname);
//                                  }
//                                  $select_options[''] = "----- Select Subject -----";
//                                  echo form_dropdown('subjectid', $select_options, $this->input->post('examid') ? $this->input->post('examid') : $arm_detail->examid, 'class="form-control" required="required"');
                                ?>
                                <div class="bfh-selectbox" data-name="subjectid" data-value="<?= $this->input->post('subjectid') ? $this->input->post('subjectid') : $arm_detail->subjectid ?>" data-filter="true">
                                 <?php 
                                  
                                  echo '<div data-value="">----- Select -----</div>';
                                  foreach ($subjects as $subject) {
                                     echo '<div data-value="'.$subject->subjectid.'">'. strtoupper($subject->subjectname). '</div>';
                                  }
                                ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Subject Arm:</label>
                            <div class="col-sm-8">
                                <input type="text" name="armname" value="<?php echo set_value('armname', $arm_detail->armname); ?>" class="form-control" id="inputEmail3" placeholder="Enter Subject Arm" required="required">
                                <input type="hidden" name="examid" value="<?= isset($examid) ? $examid : '' ?>" />
                                <input type="hidden" name="examyear" value="<?= isset($examyear) ? $examyear : '' ?>" />
                            </div>
                        </div>
                        
                         <div class="form-group">
                               <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-saved"></i> Save</button>
                                <a href="" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                              </div>
                            </div>
                    </div>
                 <?= form_close(); ?>
                
                <div class="col-md-7">
                     <?php if($this->session->flashdata('updated_msg')) echo get_success($this->session->flashdata('updated_msg')); ?>
                    <table class="table table-bordered table-condensed" id="example">
                      <thead>
                        <tr>
                           <th>Exam</th>
                           
                          <th>Parent Subject</th>
                          <th>Subject Arm</th>
                          
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php 
                            if(count($arms)):
                                $sn = 0;
                                foreach($arms as $arm):
                          ?>  
                                   <tr>
                                    <td><?= strtoupper($this->subjects_arm_model->getExamName_From_Id($examid)); ?></td>
                                    <td><?= strtoupper($arm->subjectname); ?></td>
                                    <td><?= anchor(site_url('admin/subjectarms/save/'.$examid.'/'.$arm->armid), strtoupper($arm->armname)) ?></td>
                                   
                                    <td>
                                        <?= get_edit_btn(site_url('admin/subjectarms/save/'.$examid.'/'.$arm->armid)); ?>
                                        <?= get_del_btn(site_url('admin/subjectarms/delete/'. $arm->armid . '/' . $examid)); ?>
                                    </td>
                                  </tr> 
                          <?php 
                                endforeach;
                            endif;
                           ?>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>