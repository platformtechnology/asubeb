<div class="row">
    <div class="col-md-12 panel-primary">

            <div class="content-box-header panel-heading">
                <div class="panel-title"><i class="glyphicon glyphicon-cog"></i> <strong>Update Posting Schools</strong></div>
            </div>
            <div class="content-box-large box-with-header">
                <div class="row">
                    <div class="col-md-12">
                    <table class="table table-bordered table-condensed" id="">
                      <thead>
                        <tr>
                          <th width = "">S/N</th>
                          <th width = "50%">School Name</th>
                          <th>LGA</th>
                          <th>Zone</th>
                          <th>IsCentre</th>
                          <th>AllowPosting</th>
                          <th>School Capacity</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php
                            if(count($schools)):
                                $sn = 1;
                                foreach ($schools as $school):
                            ?>
                            <tr>
                                <td><?php echo $sn;?></td>
                                <td><?php echo anchor(site_url('admin/secondaryschools/save/'. $school->schoolid), $school->schoolname) ?> (<?= $school->schoolcode; ?> )</td>
                                 <td><?= $this->schools_model->get_lga_name($school->lgaid); ?></td>
                                <td><?= $this->schools_model->get_zone_name($school->zoneid); ?></td>
                                <td><?= $school->iscentre == 1 ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>'; ?></td>
                                <td> <input type="checkbox" <?php echo $school->ispostingschool ? "checked = checked":''?>  onchange='updatePostingSchool("<?php echo $sn;?>",this.value,"<?php echo $school->schoolid?>")'></td>
                                <td> <input type="text" name="school_capacity" id="school_capacity" class="form-control" maxlength="4" value="<?php echo isset($school->school_capacity) ? $school->school_capacity:''?>" size="4" maxlength ="4" onchange='updateSchoolCapacity("<?php echo $sn;?>",this.value,"<?php echo $school->schoolid?>")'></td>
                                <td> <span id = "status<?php echo $sn;?>"></span></td>
                            </tr>
                            <?php
                                $sn++;
                                endforeach;?>
                            <?php endif; ?>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
    </div>
</div>



<script type="text/javascript">
function updateSchoolCapacity(index,capacity,schoolid){
        var school_capacity = parseInt(capacity);

        if(isNaN(school_capacity)){
            alert('Invalid School Capacity Entered');
        }
        else {
                $('#status'+index).html('<div style = "color:red"> Processing.....</div>');
                $.ajax({url: "<?php echo site_url('admin/secondaryschools/ajax_update_school_capacity') ?>/" + school_capacity + "/" + schoolid,
                            success: function (result_data) {
                            $('#status'+index).html(result_data);
                    }
                });
        }


}
function updatePostingSchool(index,status,schoolid){
        var isPostingSchool;
        if (status === "on"){
            isPostingSchool = 1;
        }
        else {
            isPostingSchool = 0;
        }

                $('#status'+index).html('<div style = "color:red"> Processing.....</div>');
                $.ajax({url: "<?php echo site_url('admin/secondaryschools/ajax_update_posting_school') ?>/" + isPostingSchool + "/" + schoolid,
                            success: function (result_data) {
                            $('#status'+index).html(result_data);
                    }
                });
}
</script>