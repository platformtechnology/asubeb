<?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
<div class="row">
    <div class="col-md-6">
              
                <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title"><i class="glyphicon glyphicon-stats"></i> No Of Candidates Per Exams Per Year </div>
                        <div class="panel-options">
                            <a href="<?= site_url('admin/dashboard');?>" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div id="reg_exams_div" style="height: 230px;"></div>

                    </div>
                </div>
            
    </div>
    
    <div class="col-md-6">
       
                <div class="content-box-header">
                    <div class="panel-title"><i class="glyphicon glyphicon-user"></i> Candidate Summary For <strong>Active Year</strong> (<?= $activeyear; ?>)</div>
                </div>
                <div class="content-box-large box-with-header">
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Exam Type</th>
                            <th>No of Candidates</th>
                            <th>Active Year</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $sn = 0; $totalC =0;
                                if(count($candidate_summary)): 
                                    foreach ($candidate_summary as $summary):
                            ?>
                            <tr>
                                <td><?= ++$sn ?></td>
                                <td><?= $summary->examname; ?></td>
                                <td><?= $summary->candidatecount; ?></td>
                                <td><?= $summary->examyear; ?></td>
                            </tr>
                            
                                    <?php 
                                        $totalC += $summary->candidatecount;
                                    endforeach;
                                    ?>
                            <?php endif; ?>
                        </tbody>
                  </table>
                    <strong>Total Candidates: <span style="color:crimson"><?=$totalC;?></span></strong>
                </div>
            
                   
      </div>

</div> 
<?php 

    if(config_item('isonline')){        
        if(isset($edc_detail->updateavailable) && $edc_detail->updateavailable ): ?>

            <div class="row">
                <div class="col-md-12 panel-danger">
                    <div class="content-box-header panel-heading">
                        <div class="panel-title"><i class="glyphicon glyphicon-arrow-down"></i> Updates Available</div>
                    </div>
                    <div class="content-box-large box-with-header">
                        <div class="panel-body">
                          <?= form_open('', 'class="form-horizontal" role="form"'); ?>  
                            <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-3 control-label">FILE NAME:</label>
                                 <div class="col-sm-9">
                                     <strong><?= url_title(strtoupper($edc_detail->edcname)).'.zip'; ?></strong>
                                 </div>
                             </div>
                            <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-3 control-label">DATE MODIFIED:</label>
                                 <div class="col-sm-9">
                                     <?= $edc_detail->datemodified; ?>
                                 </div>
                             </div>
                            <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-3 control-label">DOWNLOADS:</label>
                                 <div class="col-sm-9">
                                     <?= $edc_detail->downloaded; ?>
                                 </div>
                             </div>
                            <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-3 control-label">ACTION:</label>
                                 <div class="col-sm-9">
                                     <a href="<?= config_item('controller_url').'index.php/super/updates/view/'.$edc_detail->edcid; ?>" ><i class="glyphicon glyphicon-download-alt"></i> Download Update</a>
                                 </div>
                             </div>
                            <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-3 control-label">INSTRUCTIONS:</label>
                                 <div class="col-sm-9">
                                     <?= $edc_detail->updateinstructions; ?>
                                 </div>
                             </div>
                          <?= form_close(); ?>   
                        </div>
                    </div>
                </div>
            </div>
<?php 
        endif;
    }
?>

