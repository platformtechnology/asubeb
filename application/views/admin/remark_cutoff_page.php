<?php $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row(); ?>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-6 panel-primary">
        <h4 style="text-align: center; color: #0077b3; font-weight: bold">
            CUTOFF MARK SETTINGS/ANALYSIS FOR <span style="font-size: 26px"><?= strtoupper($examdetail->examname); ?> </span> <?= $examyear;
; ?>
        </h4>
        <hr/>
        <div class="content-box-header panel-heading">
            <div class="panel-title">
                <strong><i class="glyphicon glyphicon-edit"></i> Set/Analyse Cutoff Mark</strong>
            </div>
        </div>
        <div class="content-box-large box-with-header">
            <?php if ($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
            <?php if (strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
<?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">CutOff Mark:</label>
                <div class="col-sm-8">
                    <input type="text" name="cutoff" value="<?= set_value('cutoff', (count($cutoff) ? $cutoff->cutoff : 0)); ?>" class="form-control" placeholder="Enter CutOff Mark" required="required">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-adjust"></i> Set Cutoff</button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <strong>You can Analyse and set the Cutoff Mark for this exam here.</strong>
                </div>
            </div>
<?php echo form_close(); ?>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="content-box-large">
            <div class="container">
                <button class="label label-info" onclick="printDiv('printdiv')"><i class="glyphicon glyphicon-print"></i> print</button>
            </div>
            <div class="panel-body" id="printdiv">
                <h4 style="text-align: center; color: #0077b3; font-weight: bold">
                    MINISTRY OF EDUCATION ABIA STATE<br/>
                    <img src="<?= $edc_logo; ?>" alt="Edc Logo" />
                    <span style="font-weight: bold; font-size: 20px; text-align: center"><?= ucwords(strtoupper($edc_detail->edcname)); ?></span>
                    <br/>
<?= $examyear; ?> <span><?= strtoupper($examdetail->examdesc) . ' (' . strtoupper($examdetail->examname) . ') '; ?> </span><br/>
                    <hr/>
                </h4>
                <h5 style="text-align: center; font-weight: bold">
                    ANALYSIS USING <span style="font-size: 20px;"><?= (count($cutoff) ? $cutoff->cutoff : 0); ?></span> AS CUTOFF MARK
                </h5>

                <a href="<?php echo site_url('admin/remarks/cutoff/' . $examid . '/' . $examyear.'/1') ?>"> <button type="button" name ="run_analysis" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-adjust"></i> Run Analysis</button></a>

                <?php
                if (count($cutoff) && isset($run_analysis)) {
                    ?>
                    <br/>
                    <table width="100%" class="table table-bordered table-condensed" border = "1">
                        <tr align="center">
                            <td rowspan="2"><strong>SN</strong></td>
                            <td rowspan="2"><strong>L.G.A</strong></td>
                            <td rowspan="<?= 3 + count($lgas); ?>">&nbsp;</td>
                            <td colspan="3"><strong>REGISTERED</strong></td>
                            <td rowspan="<?= 3 + count($lgas); ?>">&nbsp;</td>
                            <td colspan="3"><strong>PRESENT</strong></td>
                            <td rowspan="<?= 3 + count($lgas); ?>">&nbsp;</td>
      <!--                      <td colspan="3"><strong>ABSENT</strong></td>
                            <td rowspan="<?= 3 + count($lgas); ?>">&nbsp;</td>-->
                            <td rowspan="2"><strong>TOTAL PASSED</strong></td>
                            <td rowspan="<?= 3 + count($lgas); ?>">&nbsp;</td>
                            <td rowspan="2"><strong>TOTAL FAILED</strong></td>
                        </tr>
                        <tr align="center">
                            <td><strong>M</strong></td>
                            <td><strong>F</strong></td>
                            <td><strong>TOTAL</strong></td>
                            <td><strong>M</strong></td>
                            <td><strong>F</strong></td>
                            <td><strong>TOTAL</strong></td>
      <!--                      <td><strong>M</strong></td>
                            <td><strong>F</strong></td>
                            <td><strong>TOTAL</strong></td>-->
                        </tr>
                        <?php
                        if (count($lgas)):
                            $sn = 0;

                            if ($examdetail->hasposting)
                                $pass_count = $this->report_model->get_passcount_basedon_criteria_per_lga(null, $examid, $examyear, $examdetail->hasposting, $cutoff->cutoff);
                            $TOTALREG = 0; $total_registered_male = 0;
                            $TOTALPRESENT = 0; $total_registered_female = 0;
                            $TOTALABSENT = 0; $total_present_female = 0;
                            $TOTALPASSED = 0; $total_present_male = 0;
                            $TOTALFAILED = 0;

                            foreach ($lgas as $lga):

                                $registered = $this->cutoff_model->get_registered_perlga_pergender($lga->lgaid, $examid, $examyear);
                                $present_male = $this->cutoff_model->get_present_perlga_pergender($lga->lgaid, $examid, $examyear, 'M');
                                $present_female = $this->cutoff_model->get_present_perlga_pergender($lga->lgaid, $examid, $examyear, 'F');

                                if (!$examdetail->hasposting)
                                    $pass_count = $this->report_model->get_passcount_basedon_criteria_per_lga($lga->lgaid, $examid, $examyear);

                                $total_regtrd = $registered->mcount + $registered->fcount;
                                $total_present = $present_male + $present_female;

                                $total_absent = $total_regtrd - $total_present;

                                $TOTALREG += $total_regtrd;
                                $TOTALPRESENT += $total_present;
                                $TOTALABSENT += $total_absent;

                                $total_registered_male += $registered->mcount;
                                $total_registered_female += $registered->fcount;

                                $total_present_female += $present_female;
                                $total_present_male += $present_male;
                                ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td><?= strtoupper($lga->lganame); ?></td>
                                    <td><?= number_format($registered->mcount); ?></td>
                                    <td><?= number_format($registered->fcount); ?></td>
                                    <td><?= number_format($total_regtrd); ?></td>
                                    <td><?= number_format($present_male); ?></td>
                                    <td><?= number_format($present_female); ?></td>
                                    <td><?= number_format($total_present); ?></td>

                                    <td><?php
                                        $finalPass = 0;

                                        if ($examdetail->hasposting) {
                                            if (count($pass_count)) {
                                                foreach ($pass_count as $passs) {
                                                    $found = false;
                                                    if ($lga->lgaid == $passs['lgaid']) {
                                                        $finalPass = $passs['num'];
                                                        echo number_format($finalPass);
                                                        $found = true;
                                                        break;
                                                    }
                                                }

                                                if (!$found)
                                                    echo $finalPass = 0;
                                            } else
                                                echo $finalPass = 0;
                                        } else
                                            echo $finalPass = number_format($pass_count);

                                        $total_failed = $total_present - $finalPass;

                                        $TOTALPASSED += $finalPass;
                                        $TOTALFAILED += $total_failed;
                                        ?>
                                    </td>
                                    <td><?= number_format($total_failed); ?></td>
                                </tr>
            <?php
        endforeach;
    endif;
    ?>

                        <tr align="center">
                            <td>&nbsp;</td>
                            <td><strong>TOTAL</strong></td>
                            <td><strong><?= number_format($total_registered_male); ?></strong></td>
                            <td><strong><?= number_format($total_registered_female); ?></strong></td>
                            <td><strong><?= number_format($TOTALREG); ?></strong></td>
                            <td><strong><?= number_format($total_present_male); ?></strong></td>
                            <td><strong><?= number_format($total_present_female); ?></strong></td>
                            <td><strong><?= number_format($TOTALPRESENT); ?></strong></td>
                            <td><strong><?= number_format($TOTALPASSED); ?></strong></td>
                            <td><strong><?= number_format($TOTALFAILED); ?></strong></td>
                        </tr>
                    </table>
                    <table align="center" class="table-bordered" cellpadding="7" width="70%">
                        <thead>
                            <tr>
                                <th>TOTAL REGISTERED</th>
                                <th>TOTAL PRESENT</th>
                                <th>TOTAL ABSENT</th>
                                <th colspan="2">TOTAL PASSED</th>
                                <th colspan="2">TOTAL FAILED</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?= number_format($TOTALREG); ?></td>
                                <td><?= number_format($TOTALPRESENT); ?></td>
                                <td><?= number_format($TOTALABSENT); ?></td>
                                <td><?= number_format($TOTALPASSED); ?></td>
                                <td><?= ($TOTALPRESENT == 0 ? '0' : round((($TOTALPASSED * 100) / $TOTALPRESENT), 2)); ?>%</td>
                                <td><?= number_format($TOTALFAILED); ?></td>
                                <td><?= ($TOTALPRESENT == 0 ? '0' : round((($TOTALFAILED * 100) / $TOTALPRESENT), 2)); ?>%</td>
                            </tr>
                        </tbody>
                    </table>
    <?php
}//End IF Count CUtOFF
?>
            </div>
        </div>
    </div>
</div>

<script lang="javascript" type="text/javascript">

    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
                "<html><head></head><body>" +
                divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }

</script>
