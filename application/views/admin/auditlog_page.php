
<div class="row">    
            <div class="col-lg-12">
                <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title">
                             <h3><i class="glyphicon glyphicon-fullscreen"></i> <strong>AUDIT LOG OF ALL ACTIVITIES</strong></h3>
                        </div>
                    </div>
                    <div class="panel-body">
                         <div class="row">
                             <div class="col-lg-12">
                                <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                                <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                                <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                                 <div class="col-lg-6">
                                     <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Action Type:</label>
                                        <div class="col-sm-8">
                                             <?php 
                                                $select_options = array();
                                                foreach ($actiontypes as $actiontype) {
                                                    $select_options[$actiontype->actiontype] = $actiontype->actiontype;
                                                }
                                                $select_options[''] = "-------- All --------";
                                                echo form_dropdown('actiontype', $select_options, $this->input->post('actiontype') ? $this->input->post('actiontype') : '', 'class="form-control"');
                                              ?>
                                        </div>
                                     </div>

                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Performed by:</label>
                                        <div class="col-sm-8">
                                            <?php 
                                                $select_options = array();
                                                foreach ($users as $user) {
                                                    $select_options[$user->userid] = $user->fullname;
                                                }
                                                //$select_options['GUEST'] = "Guest";
                                                $select_options[''] = "-------- All --------";
                                                echo form_dropdown('userid', $select_options, $this->input->post('userid') ? $this->input->post('userid') : '', 'class="form-control"');
                                            ?>
                                        </div>
                                     </div>

                                 </div>
                                 <div class="col-lg-6">
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Date From:</label>
                                        <div class="col-sm-8">
                                            <div class="bfh-datepicker" data-format="y-m-d" data-name="datefrom" data-date="<?= ($this->input->post('datefrom') ? $this->input->post('datefrom') : 'today') ?>"></div>
                                        </div>
                                     </div>

                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Date To:</label>
                                        <div class="col-sm-8">
                                            <div class="bfh-datepicker" data-format="y-m-d" data-name="dateto" data-date="<?= ($this->input->post('dateto') ? $this->input->post('dateto') : 'today') ?>"></div>
                                        </div>
                                        </div>
                                        
                                     <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-8">
                                            <button id="searchAudit_btn" runat="server" type="submit" onserverclick="searchAudit_btn_ServerClick" class="btn btn-sm btn-primary" causesvalidation="False"><i class="glyphicon glyphicon-search"></i> Search </button>
                                            <a href="<?= site_url('admin/auditlog') ?>" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-refresh"></i> Reset </a>
                                        </div>
                                     </div>
                                 </div>
                                 
                                  <?= form_close(); ?>
                             </div>
                              
                        </div>
                        <hr />

                        <div class="row">
                                 <div id="printDIV">
                                     <table class="table" id="example">
                                          <thead>
                                            <tr>
                                              <th>#</th>
                                              <th>Performed By</th>
                                              <th>Action Type</th>
                                              <th>Action Performed</th>
                                              <th>Description</th>
                                              <th>Time Performed</th>
                                                <th></th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php
                                               $sn = 0;
                                               if(count($auditlog)):
                                                   foreach ($auditlog as $audit):
                                              ?>
                                              <tr>
                                                  <td><?= ++$sn; ?></td>
                                                  <td><?= $audit->fullname; ?></td>
                                                  <td><?= $audit->actiontype; ?></td>
                                                  <td><?= $audit->message; ?></td>
                                                  <td><?= $audit->details; ?></td>
                                                  <td><?= $audit->datemodified; ?></td>
                                                  <td><a href="<?= site_url('admin/auditlog/view/'.$audit->id); ?>" title="View Log Details"><i class="glyphicon glyphicon-list"></i></a></td>
                                              </tr>
                                              
                                              <?php 
                                                    endforeach;
                                                endif;
                                              ?>
                                          </tbody>
                                        </table>
                               </div>
                        </div>

                    </div>
                </div>
            </div>
         </div>
