 <h4 style="text-align: center; color: #0077b3; font-weight: bold">
    VARIANCE SETTINGS FOR <br/><?= strtoupper($exam_detail->examdesc. ' ('.$exam_detail->examname.') ' . $examyear) ;?> 
</h4>
<hr/>

<div class="row">
    <div class="col-md-12 panel-primary">
        
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-edit"></i> <strong>Analyse</strong>               
                </div>
            </div>
            <div class="content-box-large box-with-header">
                <?= form_open('', 'class="form-horizontal" name="regform" id="regform" role="form"'); ?>
                <div class="row">
                    <div class="col-md-10">
                        
                    <?php if($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                    <?php if($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                    <?php if(strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                    
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                           <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">SUBJECTS:</label>
                                <div class="col-sm-8">
                                    <?php
                                        $select_options = array();
                                        $select_options[''] = "----- Select A Subject -----";
                                        foreach ($subjects as $subject) {
                                            $select_options[$subject->subjectid] = strtoupper($subject->subjectname);
                                        }
                                        echo form_dropdown('subjectid', $select_options, ($this->input->post('subjectid') ? $this->input->post('subjectid') : ($this->session->flashdata('done') ? $this->session->flashdata('done') : '')), 'id="subjectid" class="form-control" onchange="displaySettings()"'); 
                                    ?>
                                </div>
                            </div>
                            
                            
                        </div>
                
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-8">
                        <h5 style="text-align: center;"><strong>Select a subject above to view and edit its variance settings:</strong></h5>
                        
                    </div>
                </div>

                <div id="display_response"></div>
                
              <?= form_close(); ?>
            </div>
        
    </div>
</div>

<script lang="javascript" type="text/javascript">

        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
            
    </script>
    
<script type="text/javascript">
    function displaySettings(){
        subjectid = document.getElementById("subjectid").value;

        if(subjectid.trim() == ""){
            document.getElementById("display_response").innerHTML = '';
            return;
        }
        
        var url_to_post = "<?= site_url('admin/variance/fetchVariance?subjectid='); ?>";
        url_to_post = url_to_post + subjectid + "&examid=" + "<?= $exam_detail->examid; ?>" + "&examyear=" + "<?= $examyear ?>";
        document.getElementById("display_response").innerHTML = ' <img src="'+"<?= get_img('loader-long.gif'); ?>"+'" />';
        makeRequestx(url_to_post, "display_response");
    }
    
    function makeRequestx(url, displayId) {
         
        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
                
         httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                   document.getElementById(displayId).innerHTML = httpRequest.response;
                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById(displayId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
        httpRequest.send();
    }
   
    function getHttpObject(){
    
        var xmlhtp;
		
        if (window.ActiveXObject) 
        {    
            var aVersions = [ 
              "MSXML2.XMLHttp.9.0","MSXML2.XMLHttp.8.0", "MSXML2.XMLHttp.7.0",
              "MSXML2.XMLHttp.6.0","MSXML2.XMLHttp.5.0","MSXML2.XMLHttp.4.0",
              "MSXML2.XMLHttp.3.0","MSXML2.XMLHttp","Microsoft.XMLHttp"
            ];

            for (var i = 0; i < aVersions.length; i++) 
            {
                try 
                { 
                    xmlhtp = new ActiveXObject(aVersions[i]);
                    return xmlhtp;
                } catch (e) {}
            }
        }
        else  if (typeof XMLHttpRequest != 'undefined') 
        {
                xmlhtp = new XMLHttpRequest();			
                return xmlhtp;
        } 
    	throw new Error("XMLHttp object could be created.");
        
    }
        
    function updateVariance(field, row, option){
        
        //alert('hello'); return;
        min = document.getElementById("min"+row+option).value;
        max = document.getElementById("max"+row+option).value;
        subjectid = document.getElementById("async_subjectid").value;;
        
        population_disp = "population"+row+option;
        percentage_dis = "percentage"+row+option;
        graph = "graph"+option;

        if(min.trim() == "" || max.trim() == ""){
            document.getElementById(population_disp).innerHTML = '----';
            document.getElementById(percentage_dis).innerHTML = '----';
            return;
        }
        
        if(isNaN(min)){
            alert('INVALID ENTRY');
            document.getElementById("min"+row+option).value = '';
            document.getElementById(population_disp).innerHTML = '----';
            document.getElementById(percentage_dis).innerHTML = '----';
            return;
        }
        
        if(isNaN(max)){
            alert('INVALID ENTRY');
            document.getElementById("max"+row+option).value = '';
            document.getElementById(population_disp).innerHTML = '----';
            document.getElementById(percentage_dis).innerHTML = '----';
            return;
        }
        
        mean = document.getElementById("mean").value;
        std = document.getElementById("stddev").value;
        
        //NOT SUPPOSED TO BE
            F = document.getElementById("max1"+option).value;
            P1 = document.getElementById("min2"+option).value;
            P2 = document.getElementById("max2"+option).value;
            C1 = document.getElementById("min3"+option).value;
            C2 = document.getElementById("max3"+option).value;
            A = document.getElementById("min4"+option).value;
            
            if(isNaN(F) || isNaN(P1) || isNaN(P2) || isNaN(C1) || isNaN(C2) || isNaN(A)){
                alert('ONE OR MORE INVALID ENTRY DETECTED');
                document.getElementById("n1"+option).innerHTML = '----';
                document.getElementById("n2"+option).innerHTML = '----';
                document.getElementById("n3"+option).innerHTML = '----';
                return;
            }
        
            N1 = ((2*mean) - P1 - F - 1)/(2*std);
            N2 = (P2 - (2*mean) + C1)/std;
            N3 = (C2 - (2*mean) + A)/std;
            
            document.getElementById("n1"+option).innerHTML = N1.toFixed(2);
            document.getElementById("n2"+option).innerHTML = N2.toFixed(2);
            document.getElementById("n3"+option).innerHTML = N3.toFixed(2);
            
        //END
        
        var url_to_post = "<?= site_url('admin/variance/fetchVariedRemark?subjectid='); ?>";
        url_to_post = url_to_post + subjectid + "&examid=" + "<?= $examid; ?>" + "&examyear=" + "<?= $examyear ?>"
        + "&min=" + min + "&max=" + max;
        
        makeRequestAsynReq(url_to_post, population_disp, percentage_dis, graph, row, option);
    }
    
    function makeRequestAsynReq(url, population_disp, percentage_dis, graph, row, option) {
         
        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
                
         httpRequest.onreadystatechange = function(){
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                   var obj = JSON.parse(httpRequest.response);
                  
                   document.getElementById(population_disp).innerHTML = obj.students_count_per_remark;
                   document.getElementById(percentage_dis).innerHTML = obj.percentage_count_per_remark+'%';
                          
                   updateGraph(row, option);
                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById(population_disp).innerHTML = '----';
                    document.getElementById(percentage_dis).innerHTML = '----';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
        httpRequest.send();
    }

    function updateGraph(row, option){
        
//        var jsondata = new Array();
        var jsondata = [];
        graph = "graph"+option;
        
        jsondata.push({'y': 0, 'a': 0}); 
        for(i = 1; i<= row; i++){
            population_disp = "population"+i+option;
            percentage_dis = "percentage"+i+option;
            score_disp = "max"+i+option;
           
            population = document.getElementById(population_disp).innerHTML;
            percentage = document.getElementById(percentage_dis).innerHTML;
            score = document.getElementById(score_disp).value;
                    
            population = population.replace('%','');
            percentage = percentage.replace('%','');
            
            if(isNaN(percentage) || isNaN(population)){
                alert('INVALID DATA DETECTED - COMPLETE VARIATIONS FIRST');
                return;
            }
            
            jsondata.push({'y': score, 'a': population});          
        }
        
       // d = JSON.stringify(jsondata);
        document.getElementById(graph).innerHTML ='';
        //alert(d);
        morrisLine = new Morris.Line({
            element: graph,
            data: jsondata,
            xkey: 'y',
            ykeys: ['a'],
            labels: ['Candidates: '],
            parseTime: false,
            hideHover: 'auto'
          });
          
         morrisLine.setData(jsondata); 
    }
</script>
