
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">

                        <h4> <span class="glyphicon glyphicon-cog"></span> OMR Processing...</h4>

                    </div>
                    <div class="panel-body">
                        <?php echo isset($error_message)?$error_message:''; ?>
                        <form action="" enctype="multipart/form-data" method="post">
                        <div class="form-group col-md-12">
                            <label class="col-md-4"> Location of Scanned OMR File</label>
                            <div class="col-md-8">
                                <input type="file" required="" class="form-control" name="omr_file">
                            </div>
                        </div>
                        <div class="form-group  col-md-12">
                            <label class="col-md-4"> Destination of Processed files (Saves to desktop y default)</label>
                            <div class="col-md-8">
                                <input type="text" name="file_directory" value='C:\Users\BLACKHAWK\Desktop\\' class="form-control" />
                            </div>
                        </div>
                        <div class="form-group  col-md-12">
                            <label class="col-md-4"> Folder_Name (The name of the folder you want the processed files to be stored)</label>
                            <div class="col-md-8">
                                <input type="text" required="" name="folder_name" value="" class="form-control" />
                            </div>
                        </div>
                            <div class="pull-right">
                                <button type="submit" name="submit" class="btn btn-primary"> Process <span class="glyphicon glyphicon-cog"></span></button>
                            <button type="reset" class="btn btn-danger"> Reset <span class="glyphicon glyphicon-refresh"></span></button>
                            </div>
                    </form>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">

                        <h4> <span class="glyphicon glyphicon-file"></span> Instructions...</h4>

                    </div>
                    <div class="panel-body">
                        This module allows you to process already scanned OMR sheets.<br/>
                        <br/>
                        <strong> It is applicable majorly for EDC Abia GPT. </strong> <br/><br/>
                        You feed in a 100 Questions Scanned OMR data and it breaks it into 4 parts of 25 answers each for subject based grading.
                        <br/><br/>
                        To Process,use the first browse button to browse for the location of the scanned OMR file(Only .txt file is allowed) <br/>
                        <br/>Then use the second button to browse for the folder where you want the processed sheets to be saved then,<br/>
                        click the  Process button<br/><br/>
                        If all went well,you should get a Success message else, you might need to call your Pastor for further prayers.
                    </div>

                </div>
            </div>



        </div>
    </div>


