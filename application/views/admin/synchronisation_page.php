 <?php 
//        if(!$activated)            echo get_activation_prompt();
//        else{
    if(config_item('isonline')){
        
        echo '<div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <i class="glyphicon glyphicon-remove-circle"></i>
                        THIS FEATURE IS ENABLED FOR THE OFFLINE VERSION ONLY - SO FIND YOUR WAY;
                    </div>
                </div>
            </div>';
    }
    else{
  ?> 

<!--SYNCHRONISATION SECTION-->

<div class="row">
    
    <div class="col-md-12 panel-primary">
        <div class="content-box-header panel-heading">
            <div class="panel-title"><i class="glyphicon glyphicon-refresh"></i> DATA SYNCHRONISATION </div>
        </div>
        <div class="content-box-large box-with-header">
            <div class="panel-body">
                <div class="row">
                    <?php if($this->session->flashdata('sync_msg')) echo get_success($this->session->flashdata('sync_msg')); ?>
                    <?php if($this->session->flashdata('sync_error')) echo get_error($this->session->flashdata('sync_error')); ?>
               
                    <div class="col-md-7">
                        <div class="row">
                            <strong>NOTE: <span style="color: crimson;"> ONLY DATA FOR THE ACTIVE YEAR</span> (<?= $activeyear; ?>) <span style="color: crimson;"> ARE SYNCHRONISED!</span></strong><br/><br/>
                            <div class="col-md-6">
                                <a href="<?= site_url('admin/dbsync/get_from_online') ?>" target="_blank" title="GET ONLINE DATA TO OFFLINE" onclick="return showloader('loader1');"><i class="glyphicon glyphicon-cloud-download"></i> GET <strong>ONLINE</strong> DATABASE TO <strong>OFFLINE</strong> </a> 
                                <img id="loader1" src="<?= get_img('loader.gif'); ?>" style="visibility: hidden;" /> <br/><br/>
                               
                            </div>
                            <div class="col-md-6">
                                 <a href="<?= site_url('admin/dbsync/push_db_online') ?>" class="btn btn-sm btn-default" title="PUSH OFFLINE DATA TO ONLINE" onclick="return showloader('loader2');" ><i class="glyphicon glyphicon-cloud-upload"></i> PUSH <strong>OFFLINE</strong> DATABASE TO <strong>ONLINE</strong> </a>
                                <img id="loader2" src="<?= get_img('loader.gif'); ?>" style="visibility: hidden;" /> 

                            </div>
                         </div>
                        <div class="row">
                            <br/>
                            <div class="row">
                             <div class="col-md-12">
                                <strong  class="blink">
                                    <span style="color: crimson; text-align: justify; line-height: 2em"> 
                                        ALL OPERATIONS PERFORMED HERE REQUIRES GOOD INTERNET CONNECTIVITY!
                                        KINDLY ENSURE YOU ARE CONNECTED TO THE INTERNET BEFORE PROCEEDING.
                                        ONLY DATA FOR THE ACTIVE YEAR </span> (<?= $activeyear; ?>) <span style="color: crimson;">
                                        ARE BEING SYNCHRONISED.
                                    </span>
                                </strong>
                            </div>
                        </div>
                           <div class="row">
                            <div class="panel-body">
                                <h4>Process Downloaded Synch File</h4>
                                <hr/>
                            
                            <?php if($this->session->flashdata('onlineSync_msg')) echo get_success($this->session->flashdata('onlineSync_msg')); ?>
                            <?php if($this->session->flashdata('onlineSync_error')) echo get_error($this->session->flashdata('onlineSync_error')); ?>
                            <?= form_open_multipart(site_url('admin/dbsync/process_online_synch_file'), 'class="form-horizontal" role="form"'); ?>
                                
                                <div class="form-group">
                                    <div class="col-sm-7">
                                        <input type="file" name="onlinefile" class="form-control" required="required">
                                        <strong>Only .edp files allowed</strong>
                                    </div>
                                     <div class="col-sm-5">
                                     <button type="submit" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-retweet"></i> Perform Synch</button>
                                     </div>
                                 </div>
                                
                             <?= form_close(); ?>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    
                    <div class="col-md-5">
                       
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        
                                        <th>LAST DATE SYNCHED</th>
                                        <th>SYNCH TYPE</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $sn = 0;
                                        if(count($synch_log)):
                                        foreach ($synch_log as $log): 
                                    ?>

                                    <tr>
                                        
                                        <td><?= $log->synchdate; ?></td>
                                        <td><?= $log->type; ?></td>
                                    </tr>

                                     <?php
                                        endforeach;
                                     endif;
                                     ?>
                                </tbody>
                            </table>
                             
                            <br/><br/>
                            <strong>
                                <span style="text-align: justify; line-height: 2em"> 
                                    HAVE YOU PUSHED YOUR OFFLINE DB TO ONLINE? <span style="color: #002166">
                                        IT IS BOTH IMPORTANT AND RECOMMENDED THAT YOU PERFORM SYNCHRONISATION EVERY DAY.
                                    </span>
                                </span>
                            </strong>
                          
                         
                            
                          
                    </div>
                    
                </div>
                
            </div>
        </div>
                            
    </div>
    
</div>


<script type="text/javascript">
    function showloader(id){
        confirmation = true;
        if(id == 'loader1')confirmation = confirm('ARE YOU SURE YOU WISH TO SYNCH DATA FROM ONLINE?');
        else if(id == 'loader2') confirmation = confirm('ARE YOU SURE YOU WISH TO PUSH YOUR DATABASE ONLINE?');
        else if(id == 'loader4') confirmation = confirm('DO YOU WISH TO DOWNLOAD YOUR CANDIDATES PASSPORT?');
        
        if(confirmation == true){
            document.getElementById(id).style.visibility="visible";
            document.getElementById("bigloader").style.visibility="visible";
            return true;
        }else{
            return false;
        }
    }
    
    function showRestoreLoader(id){
        confirmation = confirm("ARE YOU SURE THAT YOU WANT TO RESTORE THIS DATABASE - OPERATION CANNOT BE UNDONE!");
        if(confirmation){
            document.getElementById(id).style.visibility="visible";
            document.getElementById("bigloader").style.visibility="visible";
            return true;
        }
        
        return false;
    }
</script>

<?php 
    }
?>