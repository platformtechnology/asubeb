 <h4 style="text-align: center; color: #0077b3; font-weight: bold">
    MANUAL EXAM SCORE ENTRY FOR <?= $exam_detail->examname ?> <?= $examyear ?>
</h4>
<hr/>

<div class="row">
    <div class="col-md-12 panel-primary">
        
            <div class="content-box-header panel-heading">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-edit"></i> <strong>Score Students</strong>               
                </div>
            </div>
            <div class="content-box-large box-with-header">
                
            <div class="row">
               
                <?= form_open('', 'class="form-horizontal" name="regform" id="regform" role="form"'); ?>
                <div class="col-md-12">
                <?php
                    if($hastheory) echo "<h4><span style='color: crimson'>THIS EXAM HAS ALREADY BEEN GRADED FOR THEORY</h4></span> <br/>";
                ?>
                   <input type="hidden" name="hastheory" id="hastheory" autofocus="true" value="<?= $hastheory ?>" />

                    <table class="table table-bordered table-condensed" style="font-size: 12px">
                        <thead>
                          <tr>
                            <th>REGNUM</th>
                            <?php 
                                if(count($allsubjects)){
                                    foreach ($allsubjects as $subjects) {
                                        echo '<th style="font-size: 14px">'.  (strtoupper($subjects->subjectname)).'</th>';
                                    }
                                }
                              ?>
                           
                            <th></th> 
                            
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                   <input name="regnum" id="regnum" autofocus="true" value="" /><br/>
                                   <span style="color: crimson">e.g ABC/0000/101</span>
                                </td>
                                <?php 
                                if(count($allsubjects)){
                                    $sn = 0;
                                    foreach ($allsubjects as $subjects) {
                                      ++$sn;
                                      $margins = $maxscoredata[$subjects->subjectid]         ;
                                ?>
                                <td>
                                    <input type="text" style="width: 50px" name="subject<?= $sn; ?>" id="subject<?= $sn; ?>" value="" onblur="doGrade('<?= $sn; ?>')" />
                                    <span id="subject-result<?= $sn; ?>"></span>
                                    <input type="hidden" name="subjectid<?= $sn; ?>" id="subjectid<?= $sn; ?>" value="<?=$subjects->subjectid?>" />
                                    <input type="hidden" name="maxscore<?= $sn; ?>" id="maxscore<?= $sn; ?>" value="<?=$margins['maxexam']?>" />
                                </td>
                                
                                 <?php 
                                    }
                                }
                              ?>
                               
                                <td><input type="button" value="Return" onclick="returnControl(<?= $sn; ?>)" /></td>
                            </tr>
                        </tbody>
                      </table>

                    <br/>

                  </div>
                <?= form_close(); ?>
            </div>
                
             
            </div>
        
    </div>
</div>

<script type="text/javascript">
    
    function returnControl(num){
        for(var i = 1; i<= num; i++){
           document.getElementById("subject"+i).value = ""; 
           document.getElementById("subject-result"+i).innerHTML = ""; 
        }
        document.getElementById("regnum").value = ""; 
        document.getElementById("regnum").focus();
    }
    
    function doGrade(id){
        examid = "<?= $exam_detail->examid ?>";
        examyear = "<?= $examyear ?>";
        regnum = document.getElementById("regnum").value;
       
        subjectid = document.getElementById("subjectid"+id).value;
        score = document.getElementById("subject"+id).value;
        maxscore = document.getElementById("maxscore"+id).value;

        hastheory = document.getElementById("hastheory").value;
       
        result_div = "subject-result"+id;
        if(regnum.trim() == ""){
            alert("INVALID REGNUM - REG CANNOT BE EMPTY");
            document.getElementById(result_div).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }
        
        if(score.trim() == ""){
            alert("INVALID SCORE - SCORE CANNOT BE EMPTY");
            document.getElementById(result_div).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }
        
        if(isNaN(score)){
            alert("INVALID SCORES FOUND - SCORE MUST BE A NUMBER");
            document.getElementById(result_div).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }
        
        
        if(+score > +maxscore){
            alert("INVALID SCORES FOUND - SCORE CANNOT BE GREATER THAN " + maxscore);
            document.getElementById(result_div).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
            return;
        }
        
        //alert("RegNum = " + regnum + " | Examscore = " + examscore);
        
        var url_to_post = "<?= site_url('admin/manualscore/doGrade?examno='); ?>";
        url_to_post = url_to_post + regnum 
        + "&examscore=" + score 
        + "&subjectid=" + subjectid
        + "&examid=" + examid 
        + "&hastheory=" + hastheory 
        + "&examyear=" + examyear;
        
        //alert(url_to_post);
        //return;
        //document.write(url_to_post);
        
        //return;
        document.getElementById(result_div).innerHTML = ' <img src="'+"<?= get_img('loader.gif'); ?>"+'" />';
        makeRequestx(url_to_post, result_div);

    }
  
    function makeRequestx(url, displayId) {
         
        var httpRequest = getHttpObject();
        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
                
         httpRequest.onreadystatechange = function(){
           if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                   var obj = JSON.parse(httpRequest.response);
                   //alert(obj.success);
                   if(obj.err == "0"){
                      alert(obj.success);
                      document.getElementById(displayId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';   
                   }
                   else{
                       document.getElementById(displayId).innerHTML = ' <img src="'+"<?= get_img('available.png'); ?>"+'" />';
                   }
                    
                } else {
                    alert('Inalid Parameter - There was a problem with the request.');
                    document.getElementById(displayId).innerHTML = ' <img src="'+"<?= get_img('not-available.png'); ?>"+'" />';
                }
            }
        };
        httpRequest.open('GET', url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
        httpRequest.send();
    }
//    
    function getHttpObject(){
        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
            httpRequest = new XMLHttpRequest();
            return httpRequest;
        } else if (window.ActiveXObject) { // IE
            try {
                httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                return httpRequest;
            } 
            catch (e) {
                try {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    return httpRequest;
                } 
                catch (e) {}
            }
        }

        return;
    }
</script>
