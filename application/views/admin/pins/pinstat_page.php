<div class="row">

    <div class="col-md-6">



        <div class="row">
            <h4><?= strtoupper($edcname); ?></h4>
            <h5>Fetching Unused Pin and Resetting  Are Transacted for the Current Active Year (<?= $activeyear ?>)</h5>
            <br/>
            <div class="col-md-12">
                <?= form_open('', 'class="form-horizontal" role="form"'); ?> 

                <div class="col-md-4">
                    <div class="form-group">
                        <input type="hidden" name="unusedpin" value="unusedpin" />
                        <a href="<?= site_url('admin/pinstat/fetchpin/' . $edcid) ?>" class="btn btn-small btn-inverse"><i class="glyphicon glyphicon-import"></i> Fetch An Unused PIN</a>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <input type="text" style="font-weight: bold; color: crimson;" class="form-control" value="<?= ($this->session->flashdata('unusedpin') ? $this->session->flashdata('unusedpin') : '') ?>" readonly="readonly" />
                    </div>
                </div>

                <?= form_close(); ?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <?php if ($this->session->flashdata('error_reset')) echo get_error($this->session->flashdata('error_reset')); ?>
                <?php if ($this->session->flashdata('msg')) echo get_success($this->session->flashdata('msg')); ?>
                <?= form_open('', 'class="form-horizontal" role="form"'); ?> 

                <div class="col-md-4">
                    <div class="form-group">
                        <button type="submit" class="btn btn-small btn-inverse btn-block"><i class="glyphicon glyphicon-expand"></i> Reset Used PIN</button>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <input type="text" placeholder="Enter pin to reset" style="font-weight: bold; color: crimson;" name="resetpin" class="form-control" value="<?= set_value('resetpin') ?>" required="required" />
                    </div>
                </div>

                <?= form_close(); ?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="content-box-header">
                    <div class="panel-title"><i class="glyphicon glyphicon-search"></i> Search PIN</div>
                </div>
                <div class="content-box-large box-with-header">
                    <div class="panel-body">
                        <?php if ($this->session->flashdata('msg_active')) echo get_success($this->session->flashdata('msg_active')); ?>
                        <?php if ($this->session->flashdata('error')) echo get_error($this->session->flashdata('error')); ?>
                        <?php if (strlen(trim(validation_errors())) > 0) echo get_error(validation_errors()); ?>
                        <?= form_open('', 'class="form-horizontal" role="form"'); ?>
                        <input type="hidden" name="searchpin" value="searchpin" />
                        <div class="col-md-5">
                            <div class="form-group">
                                Pin:
                                <input type="text" name="pin" value="<?php echo set_value('pin'); ?>" class="form-control" id="inputEmail3" placeholder="Enter Pin">

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                Serial:
                                <input type="text" name="serial" value="<?php echo set_value('serial'); ?>" class="form-control" id="inputEmail3" placeholder="Or Serial">
                            </div>
                        </div>
                        <div class="col-md-offset-1 col-md-2">
                            <div class="form-group">
                                <button type="submit" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-search"></i> Search</button>
                            </div>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="content-box-header">
                    <div class="panel-title"><i class="glyphicon glyphicon-log-in"></i> Summary Of Uploaded <strong>PIN</strong></div>
                </div>
                <div class="content-box-large box-with-header">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Edc</th>
                                <th>Volume</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sn = 0;
                            $totalP = 0;
                            if (count($pin_upload_summary)):
                                foreach ($pin_upload_summary as $summary):
                                    ?>
                                    <tr>
                                        <td><?= ++$sn ?></td>
                                        <td><?= $summary->date; ?></td>
                                        <td><?= $summary->edcname; ?></td>
                                        <td><?= number_format($summary->volume); ?></td>
                                    </tr>

                                    <?php
                                    $totalP += $summary->volume;
                                endforeach;
                                ?>
                            <?php endif; ?>    
                        </tbody>
                    </table>
                    <strong>Total Pins Uploaded: <span style="color:crimson"><?= number_format($totalP); ?></span></strong>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="content-box-header">
                    <div class="panel-title"><i class="glyphicon glyphicon-log-in"></i> Summary Of Used <strong>PINs</strong></div>
                </div>
                <div class="content-box-large box-with-header">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Year</th>
                                <th>Edc</th>
                                <th>Volume</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sn = 0;
                            $totalP = 0;
                            if (count($pinstat_summary)):
                                foreach ($pinstat_summary as $summary):
                                    ?>
                                    <tr>
                                        <td><?= ++$sn ?></td>
                                        <td><?= $summary->examyear; ?></td>
                                        <td><?= $summary->edcname; ?></td>
                                        <td><?= number_format($summary->pincount); ?></td>
                                    </tr>

                                    <?php
                                    $totalP += $summary->pincount;
                                endforeach;
                                ?>
                            <?php endif; ?>    
                        </tbody>
                    </table>
                    <strong>Total USED: <span style="color:crimson"><?= number_format($totalP); ?></span></strong>
                </div>
            </div>
        </div>
        <?php if (isset($variedpins)): ?>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Pin</th>
                                <th>Serial</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sn = 0;
                            $totalP = 0;
                            if (count($variedpins)):
                                foreach ($variedpins as $variedpin):
                                    ?>
                                    <tr>
                                        <td><?= ++$sn ?></td>
                                        <td><a href="<?= site_url('admin/pinstat/search/'.$edcid.'/'.$variedpin->id) ?>"><?= $variedpin->pin; ?></a></td>
                                        <td><?= $variedpin->serial; ?></td>
                                    </tr>

                                    <?php
                                endforeach;
                                ?>
                            <?php endif; ?>    
                        </tbody>
                    </table>
                </div>
            </div>
        <?php endif; ?>

        <?php if (isset($reg_info)): ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-box-header">
                        <div class="panel-title"><i class="glyphicon glyphicon-log-in"></i> Pin Found - <strong>[<?= $this->input->post('pin'); ?>]</strong> - Pin Used By Below Detail</div>
                    </div>
                    <div class="content-box-large box-with-header">
                        <div class="panel-body">
                            <table class="table table-bordered" width="100%">
                                <tr>
                                    <td><strong>Registered Exam:</strong></td>
                                    <td><strong><?= $exam_info->examname; ?></strong></td>
                                    <td rowspan="4" valign="top">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 150px; height: 112px;"><img src="<?= $passport; ?>" alt="Passport" /></div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        </div>              
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Exam Year:</strong></td>
                                    <td><strong><?= $reg_info->examyear; ?></strong></td>
                                </tr> 
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr> 
                                <tr>
                                    <td><strong>Local Govt Area:</strong></td>
                                    <td><?= $this->registrant_model->get_lga($reg_info->lgaid); ?> </td>
                                </tr>
                                <tr>
                                    <td><strong>School:</strong></td>
                                    <td><?= $this->registrant_model->get_school($reg_info->schoolid); ?> </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Exam Centre:</strong></td>
                                    <td><?= $this->registrant_model->get_school($reg_info->centreid); ?> </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td valign="top">
                                        <strong>Exam No:  <span style="color:crimson;"><?= $reg_info->examno; ?></span></strong>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Firstname:</strong></td>
                                    <td><?= $reg_info->firstname; ?></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Othernames:</strong></td>
                                    <td><?= $reg_info->othernames; ?></td>
                                    <td valign="top">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Gender:</strong></td>
                                    <td><?= ($reg_info->gender == 'M' ? 'Male' : 'Female'); ?></td>
                                    <td valign="top">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Dob:</strong></td>
                                    <td><?= $reg_info->dob; ?></td>
                                    <td valign="top">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Phone:</strong></td>
                                    <td><?= $reg_info->phone; ?></td>
                                    <td valign="top">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td valign="top">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td><strong>Subjects:</strong></td>
                                    <td>
                                        <?php
                                        if (count($subjects)):
                                            foreach ($subjects as $subject) {
                                                echo $this->registrant_model->get_subject($subject->subjectid) . ', ';
                                            }
                                        endif;
                                        ?>
                                    </td>
                                    <td valign="top">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td valign="top">&nbsp;  </td>
                                </tr>
                                <?php
                                if ($exam_info->hasposting == 1):
                                    ?>
                                    <tr>
                                        <td><strong>First Choice:</strong></td>
                                        <td><?= $this->registrant_model->get_school($reg_info->firstchoice); ?> </td>
                                        <td valign="top">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Second Choice:</strong></td>
                                        <td><?= $this->registrant_model->get_school($reg_info->secondchoice); ?> </td>
                                        <td valign="top">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td valign="top">&nbsp;</td>
                                    </tr>
                                    <?php
                                endif;
                                ?>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        <?php endif; ?>
    </div>

</div> <!--END MIDDLE ROW-->
