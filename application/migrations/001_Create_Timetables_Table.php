<?php

class Migration_Create_Timetables_Table extends CI_Migration {

    function up() {

        $this->dbforge->add_field(array(
            'id'     => array(
                'type'           => 'INT',
                'unsigned'       => TRUE,
                'constraint'     => 11,
                'auto_increment' => TRUE
            ),
            'edcid'       => array(
                'type'     => 'VARCHAR',
                'constraint'     => 45,
            ),
            'timetable_id'       => array(
                'type'     => 'VARCHAR',
                'constraint'     => 45,
            ),
            'timetable_title'       => array(
                'type'     => 'VARCHAR',
                'constraint'     => 200,
            ),
            'timetable_content'       => array(
                'type'     => 'TEXT',
            ),
            'timetable_file'       => array(
                'type'     => 'TEXT',
            ),
            'examyear'       => array(
                'type'     => 'INT',
                'constraint'     => 4,
            ),
            'datecreated timestamp default now()',
            'datemodified timestamp default now()'
        ));

                $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('t_edc_timetable',$if_not_exists = TRUE);
    }

    function down($param) {
        $this->dbforge->drop_table('t_edc_timetable');
    }

}
