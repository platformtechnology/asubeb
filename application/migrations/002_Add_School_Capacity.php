<?php

class Migration_Add_School_Capacity extends CI_Migration {
    public function up() {
        $fields = array('school_capacity' => array('type' => 'INT','NULL'=>TRUE)
        );

        $this->dbforge->add_column('t_schools', $fields);
    }

    public function down() {
        $this->dbforge->drop_column('t_schools', 'school_capacity');
    }
}
