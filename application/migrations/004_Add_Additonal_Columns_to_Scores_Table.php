<?php

class Migration_Add_Additonal_Columns_to_Scores_Table extends CI_Migration {
    public function up() {
        $fields = array(
            'theory_score' => array('INTEGER' => 3,'NULL'=>TRUE,'DEFAULT'=>0),
            'exam_score' => array('INTEGER' => 3,'NULL'=>TRUE,'DEFAULT'=>0),
        );
        $this->dbforge->add_column('t_scores', $fields,$after_field = 'practical_score');
    }

    public function down() {
        $this->dbforge->drop_column('t_scores', 'theory_score');
        $this->dbforge->drop_column('t_scores', 'exam_score');
    }
}
