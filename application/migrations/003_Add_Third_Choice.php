<?php

class Migration_Add_Third_Choice extends CI_Migration {
    public function up() {
        $fields = array('thirdchoice' => array('VARCHAR' => 50,'NULL'=>TRUE)
        );

        $this->dbforge->add_column('t_candidates', $fields,$after_field = 'secondchoice');
    }

    public function down() {
        $this->dbforge->drop_column('t_candidates', 'thirdchoice');
    }
}
