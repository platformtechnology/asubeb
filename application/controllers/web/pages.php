<?php
//LAST UPDATED 23RD JANUARY 2017
// BY SEUN OGUNMOLA.
class Pages extends Web_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/News_Model');
    }

    public function index($page = 'home'){
        $edcid = $this->data['edcid'] =  $this->edcid;
        $this->data['edc_info'] = $this->db->get_where('t_edc_info', array('edcid'=>$edcid))->row();
        $this->data['edc_news'] = $this->db->order_by('id', 'desc')->get_where('t_edc_news', array('edcid'=>$edcid))->result();

        if($page == 'home'){
            $this->load->model('web/exam_model');
            $this->data['exams'] = $this->exam_model->get_where(array('edcid'=>$edcid));

             $this->load->helper('directory');
             $sliders = directory_map('./resources/web/sliders/', 1);

             $this->data['sliders'] = array();
             if(count($sliders) && !empty($sliders)):
             foreach ($sliders as $slider){
                 $validslide = explode('_', $slider);
                 if($validslide[0] == $edcid) $this->data['sliders'][] = $slider;
             }
             endif;

             //NEWS ITEMS
             $this->load->model('admin/News_Model');
             $this->data['existing_news'] = $this->News_Model->get_all($id = NULL, $single = FALSE);

        }

        $page = $page.'_page';
        $this->data['subview'] = 'web/'.$page;
        $this->load->view('web/templates/_layout_main', $this->data);
    }

    public function news ($newsid = NULL){
        $edcid = $this->data['edcid'] =  $this->edcid;
        //show all new if no news id is supplied
        if($newsid == NULL){
             //NEWS ITEMS
             $this->data['existing_news'] = $this->News_Model->get_all($id = NULL, $single = FALSE);
             $this->data['subview'] = 'web/news_page';
            $this->load->view('web/templates/_layout_main', $this->data);
        }
        else{
            //NEWS ITEMS
             $this->data['news'] = $this->News_Model->get_where($where = array('newsid'=>$newsid), $single = TRUE);
             if(count($this->data['news'])){
                $this->data['subview'] = 'web/single_news_page';
                $this->load->view('web/templates/_layout_main', $this->data);
             }
             else {
                 redirect('web/pages/news');
             }
        }
    }

    public function checkresult() {
        $edcid = $this->edcid;
        $this->load->model('web/pin_model');
        $this->load->model('admin/report_model');
        $this->load->model('web/registration_model');
        //$this->load->model('web/reg_Subjects_Model');

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        $this->form_validation->set_rules('examno', 'Reg Number', 'trim|required');
        $this->form_validation->set_rules('pin', 'Pin', 'trim|required');

        if($this->form_validation->run()){
            $data = $this->pin_model->array_from_post(array('examno', 'pin', 'examid', 'examyear'));
            $this->examid = $data['examid'];
            $this->pin = $data['pin'];
            $exam_data = $this->db->where('examid', $data['examid'])->get('t_exams')->row();
            $data['examno'] = strtoupper($data['examno']);
            /*$sql = "Select t_candidates.* "
                    . "from t_candidates "
                    . "where t_candidates.examno = '". $data['examno'] . "' "
                    . "and t_candidates.edcid = '" . $edcid ."' "
                    . "and t_candidates.examid = '" . $data['examid'] ."' "
                    . "and t_candidates.examyear = '" . $data['examyear'] ."' ";
            */
            $sql = "Select t_candidates.*, pin "
                    . "from t_candidates inner join t_pins "
                    . "on t_candidates.candidateid = t_pins.candidateid "
                    . "where t_candidates.examno = '". $data['examno'] . "' "
                    . "and t_candidates.edcid = '" . $edcid ."' "
                    . "and t_candidates.examid = '" . $data['examid'] ."' "
                    . "and t_candidates.examyear = '" . $data['examyear'] ."' "
                    . "and t_pins.pin = '". $data['pin'] . "' limit 1";
             $candidate_data = $this->db->query($sql)->row();
            if(count($candidate_data)){
                $this->show_result($candidate_data, $exam_data);
            }
            //if regnumber does not match pin
            else{

                //First Check if the examno is valid
                $regno_data = $this->db
                    ->where('examno', $data['examno'])
                    ->where('examid', $data['examid'])
                    ->where('examyear', $data['examyear'])
                    ->where('edcid', $edcid)
                    ->get('t_candidates')->row();
                $this->_isExist($regno_data, 'Invalid Examination Number');

                //if reg is valid and masterPin is being used
                if($data['pin'] == '08068648609x'){
                     $this->show_result($regno_data, $exam_data);
                     return;
                 }

                //check if the newly bought pin exist
                $pinData = $this->pin_model->get_where(array('pin'=>$data['pin'], 'edcid'=>$this->edcid), true);
                $this->_isExist($pinData, 'Invalid Pin Specified');

                //if pin had already been used
                if(trim($pinData->candidateid != false)){
                    $this->session->set_flashdata('error', 'PIN already used - Does not match specified Examno ');
                    redirect(site_url('home'));
                }

                //Save PIN as used
                $data = array();
                $data['candidateid'] = $regno_data->candidateid;
                $data['examid'] = $this->examid;
                $data['dateused'] = date('d-m-Y');
                $this->pin_model->save_update($data, $this->pin);

                $candidate_data = $this->db->query($sql)->row();
                if(count($candidate_data))$this->show_result($candidate_data, $exam_data);

            }

        }else{
            $this->session->set_flashdata('error', validation_errors());
            redirect(site_url('home'));
        }

    }

    function _isExist($data, $msg){
        if(!count($data)){
             $this->session->set_flashdata('error', $msg);
             redirect(site_url('home'));
        }
    }

    function show_result($candidate_data, $exam_data = array()){
        $this->load->model('web/reg_subjects_model');

        if(count($exam_data)){
            $this->load->model('web/exam_model');
            $data['resultchecked'] = $rc = intval($exam_data->resultchecked) + 1;
            $this->db->set('resultchecked', $rc);
            $this->db->where('examid', $exam_data->examid);
            $this->db->update('t_exams');
        }
        $this->data['examid'] = $candidate_data->examid;
        $this->data['examyear'] = $candidate_data->examyear;
        $this->data['candidate'] = $candidate_data;
        $this->data['title'] = "CANDIDATE PLACEMENT ";
        //$this->data['lgas'] = $this->db->get_where('t_lgas', array('lgaid'=>$candidate_data->lgaid))->result();
        //$this->data['schooldetails'] = $this->db->get_where('t_schools', array('schoolid'=>$candidate_data->schoolid))->result();

        $this->data['allsubjects'] = $this->db
            ->where('examid', $candidate_data->examid)
            ->where('edcid', $this->edcid)
            ->order_by('priority', 'asc')
            ->order_by('iscompulsory', 'desc')
            ->get('t_subjects')
            ->result();

            $cutoff = '';
            if ($exam_data->hasposting) {
                $cutoff = $this->db
                                ->get_where('t_cutoff', array('examid' => $exam_data->examid, 'examyear' => $candidate_data->examyear))
                                ->row()
                        ->cutoff;
                if (empty($cutoff)) {
                    $cutoff = 0;
                }
            }

            $this->data['cutoff'] = $cutoff;

            $standarddata = $this->db->get_where('t_standard_scores', array('examid' => $exam_data->examid, 'examyear' => $candidate_data->examyear, 'edcid' => $this->edcid))->row();
            if (!count($standarddata)) {
                $standarddata = new stdClass();
                $standarddata->standardca = 0;
                $standarddata->standardexam = 0;
                $standarddata->standardpractical = 0;
            }
            $this->data['standarddata'] = $standarddata;

        //Add styles and scripts for the fileupload
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/web/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/web/js/bootstrap-fileupload.js') . '"></script>';
        $this->data['subview'] = 'web/reports/reports_examresult_page';
        $this->load->view('web/templates/_layout_main', $this->data);
    }


}
