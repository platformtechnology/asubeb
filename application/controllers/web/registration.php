<?php

class Registration extends Web_Controller{
    //put your code here

    function __construct() {
        parent::__construct();
        $this->load->model('web/pin_model');
        $this->load->model('web/registration_model');
        $this->load->model('web/exam_model');
    }

    public function index(){

        if($this->session->userdata('candidate_loggedin')){
            redirect(site_url('registration/confirm/'.$this->session->userdata('candidateid')));
        }

        $this->data['exams'] = $this->exam_model->get_where(array('edcid'=>$this->edcid));

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        $this->form_validation->set_rules('cardpin', 'Card Pin', 'trim|required');
        $this->form_validation->set_rules('formnumber', 'Form Number', 'trim|required');
        $this->form_validation->set_message('required', '%s Required');

        if($this->form_validation->run()){

            $data = $this->pin_model->array_from_post(array('examid', 'examyear', 'cardpin','formnumber'));

            $this->db->limit(1);
            //AUTHENTICATION LEVEL 1 : CHECK IF PIN EXISTS ELSE TRIGGER ERROR
            $pinExistData = $this->pin_model->get_where(array('pin'=>$data['cardpin'], 'edcid'=>  $this->edcid, 'examyear'=>$data['examyear']), true);
            if(count($pinExistData)){

                //pin exist
                //AUTHENTICATION LEVEL 2 : PIN EXISTS, NOW CHECK IF PIN HAS BEEN USED BY ANOTHER CANDIDATE
                if(trim($pinExistData->candidateid) != false){ //someone has attempted using this pin

                   //if the person has used the pin but the person has not registered, then continue to registration page
                   //else say prompt the person to login instead as he has completed registration.

                   $candidate_info = $this->registration_model->get_all($pinExistData->candidateid, true);
                   if(count($candidate_info)){
                        $this->session->set_flashdata('error', 'Pin Already Used! <br/> <a href="'. site_url('login') .'">Login Instead</a>');
                        redirect(site_url('registration'));
                   }else{
                        $this->session->set_flashdata('error', 'Invalid Candidate');
                        redirect(site_url('registration'));
                   }
                }

                //FORM NUMBER HACK :: DECEMBER 2015 @SEUN
                //FORM NUMBER AUTHENTICATION
                $formNumberStatus = $this->_checkFormNumber($data['formnumber'],$data['cardpin']);
                //var_dump($formNumberStatus);
               // exit;

                if ($formNumberStatus == FALSE) {
                    $this->session->set_flashdata('error', 'Invalid Form Number');
                    redirect(site_url('registration'));
                }
                elseif ($formNumberStatus === 'USED' ) {
                   $this->session->set_flashdata('error', 'Form Number has been used with another pin');
                    redirect(site_url('registration'));
                }
                else { //FORM NUMBER IS FRESH
                $this->session->set_userdata('used_pin', true);
                $this->session->set_userdata('formnumber', $data['formnumber']);
//                $this->session->set_userdata('pin', $data['cardpin']);
                $encryptedPin = encrypt_decrypt('encrypt', $data['cardpin']);
                redirect(site_url('registration/register/'.$data['examid'].'/'.$data['examyear'].'/'.$encryptedPin));
                }
            }
            //pin does not exist
            else{
                $this->session->set_flashdata('error', 'Pin Does Not Exist');
                redirect(site_url('registration'));
            }
        }

        //load registration PIN authentication page;
         $this->load->helper('directory');
             $sliders = directory_map('./resources/web/sliders/', 1);

             $this->data['sliders'] = array();
             foreach ($sliders as $slider){
                 $validslide = explode('_', $slider);
                 if($validslide[0] == $this->edcid) $this->data['sliders'][] = $slider;
             }

        $this->data['subview'] = 'web/registration_page';
        $this->load->view('web/templates/_layout_main', $this->data);
    }

    public function register($examid, $year, $pin, $candidateid = null){

        if($this->_ExamIsClosed($examid)){
            redirect(site_url('registration/closed/'.$examid));
        }

        //IF NEW CANDIATE, CHECK WHETHER ANOTHER CANDIDATE HAS USED THE PIN
        //check if user has used a pin and if exam type is valid - also get exam name for display
        if($candidateid == null){
            //$this->_check_params($examid);
            $used_pin = encrypt_decrypt('decrypt', $pin);//$this->session->userdata('pin');
            $pinExistData = $this->pin_model->get_where(array('pin'=>$used_pin, 'edcid'=>  $this->edcid, 'examyear'=>$year), true);
            if(count($pinExistData)){
                //pin exist
                if(trim($pinExistData->candidateid)){
                    $this->session->set_flashdata('error', 'Enter Pin');
                    redirect(site_url('registration'));
                }
            }
        }

        //Get details of the selected exam.
        $this->data['exam_detail'] = $this->exam_model->get_all($examid);
        count($this->data['exam_detail']) || show_error("INVALID EXAMINATION TYPE SPECIFIED - EXAM TYPE NOT FOUND");

        $this->data['choice2'] = $this->db->order_by('schoolname')->get_where('t_schools', array('ispostingschool' => 1,'edcid'=>$this->edcid))->result();

        $validation_rules = $this->registration_model->_rules;
        if($candidateid == NULL) { //initial new candidate variables
            $this->data['candidate_detail'] = $this->registration_model->is_new();
            $validation_rules['school']['rules'] .= '|required';

            //$validation_rules['lga']['rules'] .= '|required';
        }
        else {
            $this->data['choice1']=  $this->data['choice2'];
            $this->session->unset_userdata('oldprofile');
            $this->data['candidate_detail'] = $this->registration_model->get_all($candidateid, true);

            $this->db->order_by('schoolname');
            $this->data['schools'] = $this->db->get('t_schools')->result();
            $this->data['registered_subjects'] = $this->db->get_where('t_registered_subjects', array('candidateid'=>$candidateid))->result();

            $candidatelga = $this->data['candidate_detail']->lgaid;

        }


        //Get Subjects Data for DDL
        $this->db->where('edcid', $this->edcid);
        $this->db->where('examid', $examid);
        $this->db->where('examyear', '2015');
        $this->db->order_by('priority', 'asc');
        $this->data['subjects'] = $this->db->get('t_subjects')->result();

        //if former profile is requested
        if($this->input->post('former_regnum')){
            $formerreg = $this->input->post('former_regnum');
            $this->db->where('examno', $formerreg);
            $candidate_record = $this->db->get('t_candidates')->row();

            if(count($candidate_record)){
                if($candidate_record->examid == $examid){
                    $this->session->set_flashdata('error', 'EXAM HAS BEEN REGISTERED FOR ALREADY - <a href="'.  site_url('login') . '">PROCEED TO LOGN INSTEAD</a>');
                    redirect(site_url('registration/register/'.$examid.'/'.$year.'/'.$pin));
                }

                $this->data['candidate_detail'] = $this->registration_model->get_all($candidate_record->candidateid, true);

                $this->db->where('lgaid', $this->data['candidate_detail']->lgaid);
                if($this->data['exam_detail']->hassecschool) $this->db->where('issecondary', 1);
                $this->data['schools'] = $this->db->order_by('schoolname')->get('t_schools')->result();

//                if($this->data['exam_detail']->hassecschool){
//                    $this->data['schools'] = array();
//                    $this->data['candidate_detail']->zoneid = '';
//                    $this->data['candidate_detail']->lgaidid = '';
//                }

                $this->session->set_userdata('oldprofile', $candidate_record->candidateid);
            }else{
                $this->session->set_flashdata('error', 'PROFILE NOT FOUND');
                redirect(site_url('registration/register/'.$examid.'/'.$year.'/'.$pin));
            }

        }
        else{
            //else go ahead and register student

            $this->form_validation->set_rules($validation_rules);
            if($this->data['exam_detail']->hasposting == 1){
                $this->form_validation->set_rules('firstchoice', 'First Choice', 'trim|required');
                $this->form_validation->set_rules('secondchoice', 'Second Choice', 'trim|required');
            }

            //REGISTRATION FORM HAS BEEN SUBMITTED
            if($this->form_validation->run() == true){
                if ($this->session->userdata('formnumber')) {
                    $formnumber = $this->session->userdata('formnumber');
                }



                 if($candidateid == null) //generate candidateid
                 {
                    $candidate_id = $this->registration_model->generate_unique_id();
                    // to avoid duplicate while synching/inserting
                    $this->registration_model->delete($candidate_id);
                    $sql  = "delete from t_registered_subjects where candidateid = '" . $candidate_id . "'; ";
                    $sql .= "delete from t_scores where candidateid = '" . $candidate_id . "'; ";
                    $this->db->query($sql);
                    logSql($sql, $this->edcid);
                 }


                //Upload the Candidate's Passport;
                //IF the candidate fetched old profile the do not validated empty passport
                //instead copy the old passport into the supposed new one

                if($this->session->userdata('oldprofile')){
                    $uploadData = $this->_performUpload($candidateid == null ? $candidate_id : $candidateid, $examid, $year, true, $pin);

                    //No file was selected for upload - use the old photo
                    if($uploadData == null){

                        $oldpassport = './passports/'.$this->session->userdata('oldprofile').'.jpg';
                        $newpassport = './passports/'.$candidate_id.'.jpg';

                        if(file_exists($oldpassport)){
                            copy($oldpassport, $newpassport);
                        }
                    }


                }
                else $this->_performUpload($candidateid == null ? $candidate_id : $candidateid, $examid, $year, $candidateid, $pin);

                //collect posted data
                $data = $this->registration_model->array_from_post(array('firstname', 'othernames', 'firstchoice',
                                                'secondchoice', 'gender',
                                                'dob', 'phone'));
                $armid = $this->input->post('armid');
                $data['edcid'] = $this->edcid;
                $data['examid'] = $examid;
                $data['examyear'] = $year;


                if($candidateid == null){

                    $data['lgaid'] = $this->input->post('lgaid');
                    $data['schoolid'] = $this->input->post('schoolid');
                    $data['centreid'] = $data['schoolid'];
                    $data['candidateid'] = $candidate_id;

                    if($this->data['exam_detail']->haszone){
                        //USES ONLY ZONE - TREAT DIFFERENTLY
                        $data['zoneid'] = $this->input->post('zoneid');
                        $data['examno'] = $this->_generate_examNumber_zonebased($data['schoolid'], $examid);
                    }
                    else{
                        //Get Zone ID
                        $this->db->where('lgaid', $this->input->post('lgaid'));
                        $lga_data = $this->db->get('t_lgas')->row();
                        $data['zoneid'] = $lga_data->zoneid;
                        $data['examno'] = $this->_generate_examNumber($lga_data, $data['schoolid'], $examid);
                    }

                     if($data['schoolid'] == "undefined"){
                        $this->session->set_flashdata('error', 'Invalid School Selected');
                        redirect('registration/register/'.$examid.'/'.$year.'/'.$pin);
                     }


                    $data['serial'] = $this->data['serial'];


                    if (isset($formnumber)) {
                        $this->_save_form_number_param($formnumber,$used_pin,$examid);

                    }
                    $this->_save_pin_param($candidate_id, $year, $examid, $used_pin);

                    //save all compulsory subject plus the ones user selected
                    $this->_save_registered_subjects_and_scores($candidate_id, $this->input->post('subject'), $examid, $year, $armid);

                }else{
                   //GOOD IMPLEMENTATION BUT NOT APPROPRIATE FOR OUR CLIENTELE

                    //its an edit - delete all subjects for this candidate and insert newly selected subjects + compulsory ones
                    //$this->db->delete('t_registered_subjects', array('candidateid'=>$candidateid, 'examid'=>$examid, 'examyear'=>$year));
                    //logSql($this->db->last_query(), $this->edcid);

                    //$this->db->delete('t_scores', array('candidateid'=>$candidateid, 'examid'=>$examid, 'examyear'=>$year));
                    //logSql($this->db->last_query(), $this->edcid);

                    //$this->_save_registered_subjects_and_scores($candidateid, $this->input->post('subject'), $examid, $year, $armid);

                }


                //HACK FOR DUPLICATE ENTRIES
                //IF CANDIDATE ID IS NULL,THEN ITS A NEW INSERT
                if ($candidateid == NULL) {
                    $this->db->where('examno',$data['examno']);
                    $this->db->where('firstname',$data['firstname']);
                    $this->db->where('examid',$data['examid']);
                    $this->db->where('edcid',$data['edcid']);
                    $this->db->where('examyear',$data['examyear']);
                    $data_exists = $this->db->get('t_candidates');

                    if ($data_exists->num_rows() == 0) {
                        $this->registration_model->save_update($data, $candidateid, $this->edcid);
                    }

                }
                //ELSE ITS AN UPDATE
                else{
                    $this->registration_model->save_update($data, $candidateid, $this->edcid);
                }

               redirect(site_url('registration/confirm/'.($candidateid == null ? $candidate_id : $candidateid)));
               //  print_r($data); exit;
            }//End if the form posted well

        } //END ELSE OF FETCH PROFILE

        $this->data['exam_year'] = $year;

        $this->db->where('edcid', $this->edcid);
        $this->db->order_by('lganame');
        $this->data['lgs'] = $this->db->get('t_lgas')->result();

        $this->db->where('edcid', $this->edcid);
        $this->db->order_by('zonename');
        $this->data['zones'] = $this->db->get('t_zones')->result();

        $this->data['centres'] = $this->db->order_by('schoolname')->get_where('t_schools', array('iscentre' => 1))->result();


        //Add styles and scripts for the fileupload
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/web/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/web/css/datepicker.css') .'" rel="stylesheet">';

        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/web/js/bootstrap-fileupload.js') . '"></script>';

        //Add styles and scripts for the datetime
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/web/css/forms.css') .'" rel="stylesheet">';
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';

        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/web/js/bootstrap-datepicker.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/web/js/bootstrap-datepicker-init.js') . '"></script>';
        //$this->data['page_level_scripts'] .= '<script src="' . base_url('resources/web/js/forms.js') . '"></script>';
        //$this->data['page_level_scripts'] .= '<script src="' . base_url('resources/web/js/custom.js') . '"></script>';


        $this->data['subview'] = 'web/registration_form_page';
        $this->load->view('web/templates/_layout_main', $this->data);

    }

    public function _ExamIsClosed($examid) {
        $this->data['closuredata'] = $this->db->where('edcid', $this->edcid)
                                ->where('examid', $examid)
                                ->get('t_deadlines')->row();
        if(count($this->data['closuredata'])){
            if(strtotime(date('d-m-Y')) >= strtotime($this->data['closuredata']->closedate)){
                return true;
            }
        }

        return false;
    }

    public function closed($examid) {
        $this->data['closuredata'] = $this->db->where('edcid', $this->edcid)
                                ->where('examid', $examid)
                                ->get('t_deadlines')->row();
        $this->data['exam_detail'] = $this->exam_model->get_all($examid);

        $this->data['subview'] = 'web/registration_closure_page';
        $this->load->view('web/templates/_layout_main', $this->data);
    }

    public function confirm($candidateid, $refresh = null){

        $this->data['reg_info'] = $this->registration_model->get_all($candidateid);
        count($this->data['reg_info']) || show_error('Candidate Detail Not Found - Invalid Param');

        $sql = "select pin from t_pins where candidateid = '" . $candidateid ."' limit 1;";
        $result = $this->db->query($sql)->row();

        $this->data['enc_pin'] = encrypt_decrypt('encrypt', $result->pin);
        // $this->data['enc_pin'] = md5($result->pin);
        $this->data['exam_detail'] = $this->exam_model->get_all($this->data['reg_info']->examid);

        $this->data['subjects'] = $this->db->get_where('t_registered_subjects', array('candidateid'=>$candidateid))->result();

        //Add styles and scripts for the fileupload
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/js/bootstrap-fileupload.js') . '"></script>';

        $this->data['passport'] = base_url('passports/'.$candidateid.'.jpg');

        if($refresh == null){
            redirect(site_url('registration/confirm/'.$candidateid.'/success'), 'refresh');
        }

        $this->data['subview'] = 'web/registration_confirm_page';
        $this->load->view('web/templates/_layout_main', $this->data);

    }

    public function finish($candidateid) {
        $this->data['candidate_info'] = $this->registration_model->get_all($candidateid);
        $this->data['candidatename'] = ucfirst(strtolower($this->data['candidate_info']->firstname)). ' ' .  ucfirst(strtolower($this->data['candidate_info']->othernames));
        $this->data['exam_detail'] = $this->exam_model->get_all($this->data['candidate_info']->examid);

        $this->session->unset_userdata('pin');
        $this->session->unset_userdata('used_pin');

        $this->data['subview'] = 'web/registration_finish_page';
        $this->load->view('web/templates/_layout_main', $this->data);
    }

    public function _check_params($examid){
        if(!$this->session->userdata('used_pin')){
            $this->session->set_flashdata('error', 'PIN REQUIRED');
            redirect(site_url('registration'));
        }
    }

    public function lga_ajaxdrop(){
        if($this->_is_ajax()){
            $zoneid = $this->input->get('zoneid', TRUE);

            $this->db->select('lgaid, lganame');
            $this->db->order_by('lganame');
            $this->db->where('zoneid', $zoneid);
            $query = $this->db->get('t_lgas');

            $data['lgas'] = array();
            if ($query->num_rows() > 0)
            {
              $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        }else{
            echo json_encode(array());
        }
    }

    public function code_ajaxdrop(){
        if($this->_is_ajax()){
            $schoolid = $this->input->get('schoolid', TRUE);

            $this->db->select('schoolcode');
            $this->db->where('schoolid', $schoolid);
            $query = $this->db->get('t_schools')->row();

            $code = count($query) ? $query->schoolcode : 'No Code';

            echo $code;
            //return $data;
        }else{
            echo "Error";
        }
    }

    public function school_ajaxdrop(){
        if($this->_is_ajax()){
            $lgaid = $this->input->get('lgaid', TRUE);
            $hassec = $this->input->get('issec', TRUE);

            $this->db->select('schoolid as lgaid, schoolname as lganame');
            $this->db->where('lgaid', $lgaid);
            if($hassec == 1){
                $this->db->where('issecondary', 1);
            }else{
                 $this->db->where('isprimary', 1);
            }
            $this->db->order_by('schoolname');
            $query = $this->db->get('t_schools');

            $data['lgas'] = array();
            if ($query->num_rows() > 0)
            {
              $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        }else{
            echo json_encode(array());;
        }
    }
    public function sec_school_ajaxdrop(){
     //   if($this->_is_ajax()){
            /*$choiceschoolid = $this->input->get('choiceschoolid', TRUE);
            $this->db->select('lgaid');
            $this->db->where('schoolid',$choiceschoolid);
            $choiceschooldetail = $this->db->get('t_schools')->row();
            var_dump($choiceschooldetail);
            exit;*/
            $lgaid = $this->input->get('lgaid', TRUE);
            $this->db->select('schoolid as secschoollgaid, schoolname as secschoollganame');
            $this->db->where('lgaid', $lgaid);
            $this->db->where('issecondary', 1);
            $this->db->where('ispostingschool', 1);

            $this->db->order_by('schoolname');
            $query = $this->db->get('t_schools');

            $secschooldata['secschoollgas'] = array();
            if ($query->num_rows() > 0)
            {
              $secschooldata['secschoollgas'] = $query->result_array();
            }
            echo json_encode($secschooldata);
            //return $data;
       // }else{
        //    echo json_encode(array());;
       // }
    }

    public function school_ajaxdrop_byzone(){
        if($this->_is_ajax()){
            $zoneid = $this->input->get('zoneid', TRUE);
            $hassec = $this->input->get('issec', TRUE);

            $this->db->select('schoolid as lgaid, schoolname as lganame');
            $this->db->where('zoneid', $zoneid);
            if($hassec == 1){
                $this->db->where('issecondary', 1);
            }else{
                 $this->db->where('isprimary', 1);
            }
            $this->db->order_by('schoolname');
            $query = $this->db->get('t_schools');

            $data['lgas'] = array();
            if ($query->num_rows() > 0)
            {
              $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        }else{
            echo json_encode(array());
        }
    }

    function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

    public function _save_pin_param($candidate_id, $year, $examid, $pin) {

        $sql = "select * from t_pins where edcid = '".$this->edcid."'
               and examyear = '".$year."'
               and candidateid = '" . $candidate_id . "' limit 1";

        if($this->db->query($sql)->num_rows()){
            $this->session->set_flashdata('error', 'Inconsistent Data - Kindly retry');
            redirect('registration/register/'.$examid.'/'.$year.'/'.  encrypt_decrypt('encrypt', $pin));
        }

        $data = array();
        $data['candidateid'] = $candidate_id;
        $data['examid'] = $examid;
        $data['dateused'] = date('d-m-Y');
        $this->pin_model->save_update($data, $pin);
    }


    public function _save_registered_subjects_and_scores($candidateid, $subjects, $examid, $examyear, $armid = null){

        $this->load->model('web/reg_subjects_model');
        $this->load->model('web/scores_model');

        //save all the compulsory subjects first
        //values gotten from db
        foreach ($this->data['subjects'] as $subject) {
            if($subject->iscompulsory == 1){
                $subj_data = array();
                $subj_data['candidateid'] = $candidateid;
                $subj_data['subjectid'] = $subject->subjectid;
                $subj_data['examid'] = $examid;
                $subj_data['examyear'] = $examyear;
                $subj_data['edcid'] = $this->edcid;

                $armid = $this->input->post($subject->subjectid.'armid');
                $armid = (trim($armid) == false) ? '0' : $armid;
                $armdata = $this->db->get_where('t_subjects_arms', array('armid' => $armid))->row();
                if(count($armdata)){
                    if($subject->subjectid == $armdata->subjectid){
                        $subj_data['armid'] = $armdata->armid;
                    }
                }

                $this->reg_subjects_model->save_update($subj_data, null, $this->edcid);

                //insert default Exam and Practical scores for compulsory subjects
                $data = array();
                $data['scoreid'] = $this->scores_model->generate_unique_id();
                $data['subjectid'] = $subject->subjectid;
                $data['edcid'] = $this->edcid;
                $data['examid'] = $examid;
                $data['examyear'] = $examyear;
                $data['candidateid'] = $candidateid;
                $this->scores_model->save_update($data, null, $this->edcid);

            }
        }

        //save the subjects that the student selected
        //The IDs has already being selected
        foreach ($subjects as $subject) {
            $subj_data = array();
            $subj_data['candidateid'] = $candidateid;
            $subj_data['subjectid'] = $subject;
            $subj_data['examid'] = $examid;
            $subj_data['examyear'] = $examyear;
            $subj_data['edcid'] = $this->edcid;

            $armid = $this->input->post($subject.'armid');
            $armid = (trim($armid) == false) ? '0' : $armid;
            $armdata = $this->db->get_where('t_subjects_arms', array('armid' => $armid))->row();
            if(count($armdata)){
                    if($subject == $armdata->subjectid){
                        $subj_data['armid'] = $armdata->armid;
                    }
                }

            $this->reg_subjects_model->save_update($subj_data, null, $this->edcid);

             //insert default scores for selected subjects
            $data = array();
            $data['scoreid'] = $this->scores_model->generate_unique_id();
            $data['subjectid'] = $subject;
            $data['candidateid'] = $candidateid;
            $data['examid'] = $examid;
            $data['examyear'] = $examyear;
            $data['edcid'] = $this->edcid;
            $this->scores_model->save_update($data, null, $this->edcid);

        }

    }

    public function _performUpload($candidateid, $examid, $examyear, $edit_candidate, $pin = null){

        if($edit_candidate == null)
        {
            $config = array(
                 'upload_path' => './passports/',
                 'allowed_types' => 'jpg|png|jpeg',
                 'max_size' => '50',
                 'overwrite' => TRUE,
                 'file_name' => $candidateid . '.jpg',
                 'remove_spaces' => TRUE
             );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('passport'))
            {
               $this->session->set_flashdata('error', $this->upload->display_errors());
               redirect(site_url('registration/register/'.$examid.'/'.$examyear.'/'.$pin));
            }

            return $this->upload->data();

        }else{


            if(empty($_FILES['passport']['name'])){

                return null;
            }else{
                $this->_performUpload($candidateid, $examid, $examyear, null,$pin);

            }
        }
    }

    public function _generate_examNumber($lga_data, $schoolid, $examid){
        $school_data = $this->db->get_where('t_schools', array('schoolid'=>$schoolid))->row();
        $activeyear = $this->data['activeyear'];

        $schoolCode = $school_data->schoolcode;
        $lgaCode = $lga_data->lgainitials;
        $serial_num = '';

        $this->db->order_by('serial', 'desc');
        $this->db->where('lgaid', $lga_data->lgaid);
        $this->db->where('schoolid', $schoolid);
        $this->db->where('examid', $examid);
        $this->db->where('examyear', $activeyear);
        $result_set = $this->db->get('t_candidates');
        $candidate_data = $result_set->first_row();
        if(count($candidate_data)){
            $serial = $candidate_data->serial;
            if(trim($serial) == false){
               $serial_num = sprintf("%03s", 1);
            }else{
                $serial_num = $serial + 1;
                $serial_num = sprintf("%03s", $serial_num);
            }
        }else{
           $serial_num = sprintf("%03s", 1);
        }

        $this->data['serial'] = $serial_num;
        $exam_num = $lgaCode.'/'.$schoolCode.'/'.$serial_num;

        return $exam_num;
    }

    public function _generate_examNumber_zonebased($schoolid, $examid){

        $school_data = $this->db->get_where('t_schools', array('schoolid'=>$schoolid))->row();
        $activeyear = $this->data['activeyear'];

        $schoolCode = $school_data->schoolcode;
        $serial_num = '';

        $this->db->order_by('serial', 'desc');
        $this->db->where('schoolid', $schoolid);
        $this->db->where('lgaid', '0');
        $this->db->where('examid', $examid);
        $this->db->where('examyear', $activeyear);
        $result_set = $this->db->get('t_candidates');
        $candidate_data = $result_set->first_row();
        if(count($candidate_data)){
            $serial = $candidate_data->serial;
            if(trim($serial) == false){
               $serial_num = sprintf("%03s", 1);
            }else{
                $serial_num = $serial + 1;
                $serial_num = sprintf("%03s", $serial_num);
            }
        }else{
           $serial_num = sprintf("%03s", 1);
        }

        $this->data['serial'] = $serial_num;
        $exam_num = $schoolCode.'/'.$serial_num;

        return $exam_num;
    }

    private function _checkFormNumber($formnumber,$pin) {
        $status = '';

        //$this->db->limit('1');
        $this->db->where('formnumber',$formnumber);
        $form_number_query = $this->db->get('t_form_numbers');
        $formNumberExists = $form_number_query->result();

        if (count($formNumberExists)) { //FORM NUMBER EXISTS
            //HACK FOR DUPLICATE FORM NUMBERS
            if (count($formNumberExists)> 1) {
                $freecount = 0;
                foreach($formNumberExists as $formNumber)
                {
                    if ((trim($formNumber->pin) != '') && $formNumber->pin != $pin){ //FORM DATA HAS BEEN USED WITH ANOTHER PIN

                    }
                    else {
                        $freecount += 1;
                    }
                }
                if ($freecount > 0) {
                    $status = TRUE;
                }
                else {
                    $status = "USED";
                }
            }
            else {
                foreach($formNumberExists as $formNumber)
                {
                    $formNumberData = $formNumber;
                }

                if ((trim($formNumberData->pin) != FALSE) && $formNumberData->pin != $pin){ //FORM DATA HAS BEEN USED WITH ANOTHER PIN
                    $status = "USED";
                }
                else {
                    $status = TRUE;
                }
            }

        }
        else {
            $status = FALSE;
        }
        return $status;
    }
    public function _save_form_number_param($formnumber,$pin,$examid) {

        $sql = "UPDATE t_form_numbers "
                . "SET pin = '".$pin."',"
                . " examid = '".$examid."',"
                . " edcid = '".$this->edcid."',"
                . " dateused = '".date('d-m-Y')."'"
                . " where id IN (SELECT id FROM t_form_numbers where formnumber = '".$formnumber."' LIMIT 1)";
        $this->db->query($sql);


        /*$this->db->set('pin',$pin);
        $this->db->set('edcid',$this->edcid);
        $this->db->set('examid',$examid);
        $this->db->set('dateused',date('d-m-Y'));
        $this->db->where('formnumber',$formnumber);
        $this->db->update('t_form_numbers');*/
    }
}

