<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of registration
 *
 * @author Maxwell
 */
class Login extends Web_Controller{
    //put your code here
    
    function __construct() {
        parent::__construct();
                
        $this->load->model('web/registration_model');
        $this->load->model('web/pin_model');
    }
    
    public function index(){
        $this->load->model('web/exam_model');
        $this->data['exams'] = $this->exam_model->get_where(array('edcid'=>$this->edcid));
        
//        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
//        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        $this->form_validation->set_rules('pin', 'PIN', 'trim|required');
        $this->form_validation->set_rules('examno', 'Exam No', 'trim|required');
        $this->form_validation->set_message('required', '%s Required');
        
        if($this->form_validation->run()){
            
            $data = $this->registration_model->array_from_post(array('pin', 'examno'));
            
            $pin = $this->pin_model->get_where(array('pin'=>$data['pin'], 'edcid'=>  $this->edcid), true);
            if(count($pin)){
            
                $sql = "select t_candidates.*, pin from t_candidates inner join t_pins 
                    on t_candidates.candidateid = t_pins.candidateid
                    where t_candidates.examno = '" . $data['examno'] . "'
                    and t_pins.pin = '" .$data['pin'] . "' 
                    and t_candidates.edcid = '" . $this->edcid . "'";
            
                $candidate = $this->db->query($sql)->row();
            
                if(count($candidate)){

                    if($pin->candidateid != $candidate->candidateid){
                        $this->session->set_flashdata('error', 'Authentication Failed - Invalid PIN Entered For Specified Candidate');
                        redirect(site_url('web/login'));
                    }

                    if($candidate->examid == $pin->examid){
                         if($candidate->examyear == $pin->examyear){
                             
                            if($this->_ExamIsClosed($candidate->examid)){
                                redirect(site_url('registration/closed/'.$candidate->examid));
                            }
        
                             $this->session->set_userdata('candidate_loggedin', true);
                             $this->session->set_userdata('candidateid', $candidate->candidateid);
                             redirect('registration/confirm/'.$candidate->candidateid);
                         }else{
                            $this->session->set_flashdata('error', 'Invalid Pin Entered For Specified Candidate');
                            redirect(site_url('web/login'));
                         }
                    }else{
                        $this->session->set_flashdata('error', 'Invalid Pin Entered For Specified Candidate');
                        redirect(site_url('web/login'));
                    }
                }else{
                    $this->session->set_flashdata('error', 'Specified Examination Number Does Not Exist - Kindly Retry!');
                    redirect(site_url('web/login'));
                }
           }else{
               $this->session->set_flashdata('error', 'PIN DOES NOT EXIST! - Verify The Pin and Retry Login Operation');
               redirect(site_url('web/login'));
           }
            
        }
        //load registration PIN authentication page;
        $this->data['subview'] = 'web/login_page';
        $this->load->view('web/templates/_layout_main', $this->data);
    }
    
    public function logout() {
        $this->session->unset_userdata('candidate_loggedin');
        $this->session->unset_userdata('candidateid');
        redirect(site_url('web/login'));
    }
    
    public function _ExamIsClosed($examid) {
        $this->data['closuredata'] = $this->db->where('edcid', $this->edcid)
                                ->where('examid', $examid)
                                ->get('t_deadlines')->row();
        if(count($this->data['closuredata'])){
            if(strtotime(date('d-m-Y')) >= strtotime($this->data['closuredata']->closedate)){
                return true;
            }
        }
        
        return false;
    }
}
