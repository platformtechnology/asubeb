<?php
session_start();

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of registration
 *
 * @author Maxwell
 */
class Schools extends Web_Controller{
    //put your code here

    private $perpage = 30;

    function __construct() {
        parent::__construct();

        $this->load->model('web/school_model');
        $this->load->model('web/registration_model');


    }

    public function index(){
        if($this->session->userdata('school_loggedin')){
            redirect(site_url('web/schools/candidates'));
        }

        $this->form_validation->set_rules($this->school_model->_rules);
        $this->form_validation->set_rules('lgaid', 'Lga/Zone', 'trim|required');
        if($this->form_validation->run()){
            $data = $this->school_model->array_from_post(array('lgaid', 'schoolcode', 'passcode', 'phone', 'genre'));
            $data['edcid'] = $this->edcid;

            if($data['lgaid'] == '0'){
              $this->session->set_flashdata('error', 'Lga/Zone Is Required');
                redirect(site_url('web/schools'));
            }
            if($data['passcode'] == config_item('passcode')){


                $sql = "SELECT * FROM t_schools
                        WHERE (lgaid = '".$data['lgaid']."' OR zoneid = '".$data['lgaid']."')
                        AND schoolcode = '".$data['schoolcode']."'
                        AND edcid = '".$this->edcid."'
                        AND ".$data['genre']." = 1
                        ORDER BY schoolname asc";
                $school  = $this->db->query($sql)->row();
                if(count($school))
				{
                    if(empty($school->phone)){
                        $updatedata['phone'] = $data['phone'];
                        $this->school_model->save_update($updatedata, $school->schoolid);
                    }else{

                        if($school->phone != $data['phone']){
                            $this->session->set_flashdata('error', 'Authentication Failed - Wrong Admin Phone Number For Specified School Code');
                            redirect(site_url('web/schools'));
                        }
                    }

					$this->session->set_userdata('school_loggedin', true);
                    $this->session->set_userdata('school_name', $school->schoolname);
                    $this->session->set_userdata('school_id', $school->schoolid);
                    $this->session->set_userdata('school_lga', $school->lgaid);
                    redirect(site_url('web/schools/candidates'));

                }else{
                    $this->session->set_flashdata('error', 'Authentication Failed - Wrong School Code/Genre Supplied!');
                    redirect(site_url('web/schools'));
                }
            }else{
                $this->session->set_flashdata('error', 'Authentication Failed - Wrong Pass Code Supplied!');
                redirect(site_url('web/schools'));
            }
        }

        $this->db->where('edcid', $this->edcid);
        $this->db->order_by('lganame');
        $this->data['lgs'] = $this->db->get('t_lgas')->result();

        $this->db->where('edcid', $this->edcid);
        $this->db->order_by('zonename');
        $this->data['zones'] = $this->db->get('t_zones')->result();

        $this->data['subview'] = 'web/schools/school_login_page';
        $this->load->view('web/templates/_layout_main', $this->data);
    }

    public function candidates() {

        $this->load->library('pagination');
        $this->db->where('schoolid',$this->session->userdata('school_id'));
        $school_data = $this->db->get('t_schools')->row();
        $issecondary= $school_data->issecondary;
       //GET ACTIVE YEAR
        $activeyear = $this->_getActiveYear();
        $sql = "select t_candidates.* from t_candidates "
                . "where t_candidates.schoolid = '" . $this->session->userdata('school_id') . "' "
                . "and t_candidates.edcid = '" . $this->edcid. "' "
                . " and examyear = '".$activeyear."'"
                . "order by examno asc ";

        $this->data['registrants'] = $this->db->query($sql)->result();//$this->registration_model->get_where(array('schoolid'=>  $this->session->userdata('school_id')));
        $this->data['count'] = count($this->data['registrants']);
        $this->_saveCandidatesInSession(); // save the resultset in session so that it can be exported to excel

        if ($issecondary == 1) {
            $this->db->where('hassecschool',1);
        }
        else {
            $this->db->where('hassecschool',0);
        }
        $this->data['exams'] = $this->db->get_where('t_exams', array('edcid'=>  $this->edcid))->result();

        $config['per_page'] = $this->perpage;
        $config['base_url'] =  site_url('web/schools/candidates');
        $config['total_rows'] = count($this->data['registrants']);

        $sql = "select t_candidates.* from t_candidates "
                . "where t_candidates.schoolid = '" . $this->session->userdata('school_id') . "' "
                . "and examyear = '".$activeyear."'"
                . "order by examno asc LIMIT " . $config['per_page'] . ($this->uri->segment(4) ? (" OFFSET " . $this->uri->segment(4)) : "");

        $this->data['registrants'] = $this->db->query($sql)->result();//$this->registration_model->get_where(array('schoolid'=>  $this->session->userdata('school_id')));

        $this->pagination->initialize($config);

//        $sql = "Select * from t_subjects where examid in (select examid from t_candidates where schoolid = '" . $this->session->userdata('school_id') . "' );";
//        $this->data['exam_subjects'] = $this->db->query($sql)->result();

        $this->data['title'] = "LIST OF ALL CANDIDATES FROM SCHOOL";
        $this->data['subview'] = 'web/schools/school_candidate_page';
        $this->load->view('web/templates/_layout_main', $this->data);
    }

    function confirm_admin_number(){
        $this->form_validation->set_rules('schoolid','School Name','trim|required');
        if($this->form_validation->run() == TRUE){
            $schoolid = $this->input->post('schoolid');
            $this->db->where('schoolid',$schoolid);
            $school_data = $this->db->get('t_schools')->row();

            if(count($school_data)){
                $this->data['school_data'] = $school_data;
                $this->data['subview'] = 'web/schools/admin_number_details_page';
                $this->load->view('web/templates/_layout_main', $this->data);
            }
            else {
                $this->session->set_flashdata('error','Admin Phone not found!');
                redirect('Web/Schools/confirm_admin_number');
            }

        }
        else{
        $this->db->where('edcid', $this->edcid);
        $this->db->order_by('lganame');
        $this->data['lgs'] = $this->db->get('t_lgas')->result();
        $this->data['subview'] = 'web/schools/admin_number_confirm_page';
        $this->load->view('web/templates/_layout_main', $this->data);
        }
    }
    public function _saveCandidatesInSession(){
        //$this->session->set_userdata('candidates', $this->data['registrants']);
        $_SESSION['candidates'] = $this->data['registrants'];
    }

    public function export_to_excel(){

            $this->load->helper('excel');
            export_to_excel($_SESSION['candidates'], 'Candidate_Report');

    }

    public function view($candidateid, $print = false) {
         $this->load->model('web/exam_model');

        $this->data['reg_info'] = $this->registration_model->get_all($candidateid);
        count($this->data['reg_info']) || show_error('Candidate Detail Not Found - Invalid Param');

        $this->data['exam_detail'] = $this->exam_model->get_all($this->data['reg_info']->examid);

        $this->db->where('examid', $this->data['reg_info']->examid);
        $this->db->where('examyear', $this->data['reg_info']->examyear);
        $this->db->where('edcid', $this->edcid);
        $this->data['subjects'] = $this->db->get_where('t_registered_subjects', array('candidateid'=>$candidateid))->result();

        //Add styles and scripts for the fileupload
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';
       // $this->data['page_level_scripts'] = '<script src="' . base_url('resources/js/bootstrap-fileupload.js') . '"></script>';

        $this->data['passport'] = base_url('passports/'.$candidateid.'.jpg');

        if($print) $this->data['print'] = true;
        $this->data['subview'] = 'web/schools/school_candidate_view_page';
       $this->load->view('web/templates/_layout_main', $this->data);
    }

    public function logout() {
        $this->session->unset_userdata('school_loggedin');
        $this->session->unset_userdata('school_name');
        $this->session->unset_userdata('school_id');
        redirect(site_url('web/schools'));
    }

    public function delete($candidateid) {
        $this->registration_model->delete($candidateid);
        $this->session->set_flashdata('msg', 'Candidate Deleted Successfully');
        redirect(site_url('web/schools/candidates'));
    }

    public function filter(){
        $this->load->library('pagination');

        $this->data['exams'] = $this->db->get_where('t_exams', array('edcid'=>  $this->edcid))->result();

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        if($this->form_validation->run()){
            $data = $this->registration_model->array_from_post(array('examid', 'examyear','sheet_type'));
            $examid = $data['examid'];
            $this->data['examyear'] = $examyear = $data['examyear'];
            $sheet_type = $data['sheet_type'];

            $sql = "select t_candidates.* from t_candidates "
                . "where t_candidates.schoolid = '" . $this->session->userdata('school_id') . "' "
                . "and t_candidates.edcid = '" . $this->edcid . "' " . $this->_performFilter2($data)
                . "order by examno asc ";

            $this->data['registrants'] = $this->db->query($sql)->result();//$this->registration_model->get_all();

            $this->_saveCandidatesInSession(); // save the resultset in session so that it can be exported to excel

            $this->data['exam_detail'] = $this->db->where('examid', $examid)->get('t_exams')->row();
            $this->data['exam_subjects'] = $this->db
                                            ->where('examid', $examid)
                                            ->where('edcid', $this->edcid )
                                            ->order_by('priority', 'asc')
                                            ->get('t_subjects')->result();

            $this->data['exam_candidates'] = $this->data['registrants'];
            $this->data['count']= count($this->data['registrants']);


            $this->db->where('edcid', $this->edcid );
            $this->data['school_detail'] = $this->school_model->get_all($this->session->userdata('school_id'), TRUE);

             $this->data['title'] = "LIST OF ALL CANDIDATES FOR ".$this->registration_model->getExamName_From_Id($data['examid'])." ".$data['examyear'];

             if($sheet_type == 'CA') {
                $view = "web/schools/school_candidate_search_page";
             }
             else {
                $view = "web/schools/school_candidate_practical_page";
             }

            $this->load->view('web/templates/layout_head', $this->data);
            $this->load->view($view, $this->data);

        }else{
            $this->session->set_flashdata('error', validation_errors());
            redirect(site_url('schools/candidates'));
        }
    }

    #print PSLAT
    public function filter2(){

        $this->db->where('edcid', $this->edcid);
        $this->data['lgas'] = $this->db->order_by('lganame')->get('t_lgas')->result();

        $this->db->where('edcid', $this->edcid);
        $this->data['schools'] = $this->db->order_by('schoolname')->get('t_schools')->result();

        $this->db->where('edcid', $this->edcid);
        $this->data['examx'] = $this->db->get('t_exams')->result();

        $this->data['examid'] = $exam_id = 'i2v8j7out0';

        if($this->input->post('lgaid')){

            $examid = $this->input->post('examid');
            $examyear = $this->input->post('examyear');
            $lgaid = $this->input->post('lgaid');
            $schoolid = $this->input->post('schoolid');

            $this->data['examid'] = $examid;
            $this->data['examyear'] = $examyear;

            $this->db->where('edcid', config_item('edcid'));
            $this->db->where('isprimary', 1);
            if($schoolid == '')$this->db->where('lgaid', $lgaid);
            else $this->db->where('schoolid', $schoolid);
            $this->db->order_by('schoolcode', 'asc');
            $this->data['school_detail'] = $this->db->get('t_schools')->result();

            $this->data['exam_detail'] = $this->db->where('examid', $examid)->get('t_exams')->row();

//            $this->_performFilter($data);
//            $this->db->where('schoolid', $this->session->userdata('school_id'));
//            //$this->db->where('lgaid', $this->session->userdata('school_lga'));
//            $this->db->where('edcid', config_item('edcid'));
//            $this->db->order_by('examno', 'asc');
//            $this->data['registrants'] = $this->registration_model->get_all();
//
//            $this->_saveCandidatesInSession(); // save the resultset in session so that it can be exported to excel

            $this->data['exam_subjects'] = $this->db
                                            ->where('examid', $examid)
                                            ->where('examyear', $examyear)
                                            //->where('haspractical', 1)
                                            ->where('edcid', $this->edcid)
                                            ->order_by('priority', 'asc')
                                            ->get('t_subjects')->result();

//            $this->data['exam_candidates'] = $this->data['registrants'];
//            $this->data['count']= count($this->data['registrants']);


            //$this->db->where('edcid', config_item('edcid'));
             //$this->db->where('lgaid', $this->session->userdata('school_lga'));


             $this->data['title'] = "ATTENDANCE SHEET FOR ".$this->registration_model->getExamName_From_Id($examid)." ".$examyear;

             //$this->data['subview'] = 'web/schools/school_candidate_filter_page';
            //$this->load->view('web/templates/_layout_main', $this->data);

            //$this->load->view('web/templates/layout_head', $this->data);
        }
            $this->load->view('web/schools/school_candidate_filter_page_2', $this->data);
    }

    public function practical(){

        $this->db->where('edcid', $this->edcid);
        $this->db->where('issecondary', 1);
        $this->db->order_by('schoolname');
        $this->data['schools'] = $this->db->get('t_schools')->result();

        $this->db->where('edcid', $this->edcid);
        $this->db->where('haspractical', 1);
        $this->data['examx'] = $this->db->get('t_exams')->result();

        if($this->input->post('schoolid')){

            $examid = $this->input->post('examid');
            $examyear = $this->input->post('examyear');
            $schoolid = $this->input->post('schoolid');

            $this->data['examid'] = $examid;
            $this->data['examyear'] = $examyear;

            $this->db->where('edcid', $this->edcid);
            $this->db->where('issecondary', 1);
            if($schoolid != "2")$this->db->where('schoolid', $schoolid);
            $this->db->order_by('schoolcode', 'asc');
            $this->data['school_detail'] = $this->db->get('t_schools')->result();


            $this->data['exam_detail'] = $this->db->where('examid', $examid)->get('t_exams')->row();

//            $this->_performFilter($data);
//            $this->db->where('schoolid', $this->session->userdata('school_id'));
//            //$this->db->where('lgaid', $this->session->userdata('school_lga'));
//            $this->db->where('edcid', config_item('edcid'));
//            $this->db->order_by('examno', 'asc');
//            $this->data['registrants'] = $this->registration_model->get_all();
//
//            $this->_saveCandidatesInSession(); // save the resultset in session so that it can be exported to excel

            $this->data['exam_subjects'] = $this->db
                                            ->where('examid', $examid)
                                            ->where('examyear', $examyear)
                                            ->where('haspractical', 1)
                                            ->where('edcid', $this->edcid)
                                            ->order_by('priority', 'asc')
                                            ->get('t_subjects')->result();

//            $this->data['exam_candidates'] = $this->data['registrants'];
//            $this->data['count']= count($this->data['registrants']);


            //$this->db->where('edcid', config_item('edcid'));
             //$this->db->where('lgaid', $this->session->userdata('school_lga'));


             $this->data['title'] = "LIST OF ALL CANDIDATES FOR ".$this->registration_model->getExamName_From_Id($examid)." ".$examyear;

             //$this->data['subview'] = 'web/schools/school_candidate_filter_page';
            //$this->load->view('web/templates/_layout_main', $this->data);

            //$this->load->view('web/templates/layout_head', $this->data);
        }

        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';

            $this->load->view('web/schools/school_candidate_filter_page_3', $this->data);
    }

    public function propractical(){

        $this->db->where('edcid', config_item('edcid'));
        $this->db->where('issecondary', 1);
        $this->db->order_by('schoolname');
        $this->data['schools'] = $this->db->get('t_schools')->result();

        $this->db->where('edcid', config_item('edcid'));
        $this->db->where('haspractical', 1);
        $this->data['examx'] = $this->db->get('t_exams')->result();

        if($this->input->post('schoolid')){

            $examid = $this->input->post('examid');
            $examyear = $this->input->post('examyear');
            $schoolid = $this->input->post('schoolid');
            $fromdate = $this->input->post('fromdate');
            $fromtime = $this->input->post('fromtime');

            $this->data['examid'] = $examid;
            $this->data['examyear'] = $examyear;

            $this->db->where('edcid', config_item('edcid'));
            $this->db->where('issecondary', 1);
            if($schoolid != "2")$this->db->where('schoolid', $schoolid);
            $this->db->order_by('schoolcode', 'asc');
            $this->data['school_detail'] = $this->db->get('t_schools')->result();


            $this->data['exam_detail'] = $this->db->where('examid', $examid)->get('t_exams')->row();

            $interval = $fromdate . " " . $fromtime;

            $this->data['interval'] = $interval;
//            $sql = "SELECT t_candidates.*, t_pins.pin FROM t_candidates "
//                    . "INNER JOIN t_pins ON t_candidates.candidateid = t_pins.candidateid "
//                    . "WHERE t_candidates.edcid = '" . config_item('edcid') . "' "
//                    . "AND t_candidates.schoolid = '" . $schoolid . "' "
//                    . "AND t_candidates.examid = '" . $examid . "' "
//                    . "AND t_candidates.examyear = '" . $examyear . "' "
//                    . "AND substring(to_char(t_candidates.datecreated, 'YYYY-MM-DD HH24:MI:SS') from 1 for 16) >= '" . $interval . "' ";
//            $this->data['exam_candidates'] = $this->db->query($sql)->result();
//
//            $this->_performFilter($data);
//            $this->db->where('schoolid', $this->session->userdata('school_id'));
//            //$this->db->where('lgaid', $this->session->userdata('school_lga'));
//            $this->db->where('edcid', config_item('edcid'));
//            $this->db->order_by('examno', 'asc');
//            $this->data['registrants'] = $this->registration_model->get_all();
//
//            $this->_saveCandidatesInSession(); // save the resultset in session so that it can be exported to excel

            $this->data['exam_subjects'] = $this->db
                                            ->where('examid', $examid)
                                            ->where('examyear', $examyear)
                                            ->where('haspractical', 1)
                                            ->where('edcid', config_item('edcid'))
                                            ->order_by('priority', 'asc')
                                            ->get('t_subjects')->result();

//            $this->data['exam_candidates'] = $this->data['registrants'];
//            $this->data['count']= count($this->data['registrants']);


            //$this->db->where('edcid', config_item('edcid'));
             //$this->db->where('lgaid', $this->session->userdata('school_lga'));


             $this->data['title'] = "LIST OF ALL CANDIDATES FOR ".$this->registration_model->getExamName_From_Id($examid)." ".$examyear;

             //$this->data['subview'] = 'web/schools/school_candidate_filter_page';
            //$this->load->view('web/templates/_layout_main', $this->data);

            //$this->load->view('web/templates/layout_head', $this->data);
        }

        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';

            $this->load->view('web/schools/school_candidate_filter_page_4', $this->data);
    }

    private function _performFilter($data){
        if(empty($data['examyear']) && (!empty($data['examid']))){
            $this->db->where(array('edcid'=>$this->edcid, 'examid'=>$data['examid']));
        }
        else if((!empty($data['examyear'])) && (!empty($data['examid']))){
            $this->db->where(array('examyear'=>$data['examyear'], 'edcid'=>$this->edcid, 'examid'=>$data['examid']));
        }
        else if((!empty($data['examyear'])) && empty($data['examid'])){
            $this->db->where(array('edcid'=>$this->edcid, 'examyear'=>$data['examyear']));
        }else{
            $this->db->where(array('edcid'=>  $this->edcid));
        }
    }

    private function _performFilter2($data){

        if(empty($data['examyear']) && (!empty($data['examid']))){
            return "and t_candidates.edcid = '" . $this->edcid ."' and t_candidates.examid = '" .$data['examid']. "' ";
        }
        else if((!empty($data['examyear'])) && (!empty($data['examid']))){
            return "and t_candidates.examyear = '" .$data['examyear'] ."' and t_candidates.edcid = '" . $this->edcid ."' and t_candidates.examid = '" .$data['examid']. "' ";
        }
        else if((!empty($data['examyear'])) && empty($data['examid'])){
            return "and t_candidates.edcid = '" . $this->edcid ."' and t_candidates.examyear = '" .$data['examyear']. "' ";
        }else{
            return "and t_candidates.edcid = '" . $this->edcid ."' ";
        }
    }

    public function printcandidate($candidateid) {
         $this->load->model('web/exam_model');

        $this->data['reg_info'] = $this->registration_model->get_all($candidateid);
        count($this->data['reg_info']) || show_error('Candidate Detail Not Found - Invalid Param');

        $this->data['exam_detail'] = $this->exam_model->get_all($this->data['reg_info']->examid);

        $this->data['subjects'] = $this->db->get_where('t_registered_subjects', array('candidateid'=>$candidateid))->result();

        //Add styles and scripts for the fileupload
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';

        $this->data['passport'] = base_url('passports/'.$candidateid.'.jpg');

        $this->load->view('web/schools/print_profile_page', $this->data);
    }

    public function printdetails($examid){

        $this->db->where('edcid', config_item('edcid'));
        $this->data['exam_detail'] = $this->db->where('examid', $examid)->get('t_exams')->row();

        $this->db->where('edcid', config_item('edcid'));
        $this->data['exam_subjects'] = $this->db
                                        ->where('examid', $examid)
                                        ->order_by('id', 'asc')
                                        ->get('t_subjects')->result();

        $this->db->order_by('examno', 'asc');
        $this->data['exam_candidates'] = $this->registration_model->get_where(array('examid'=>$examid, 'schoolid'=>  $this->session->userdata('school_id')));

        $this->data['school_detail'] = $this->school_model->get_all($this->session->userdata('school_id'), TRUE);
        $this->load->view('web/schools/print_details_page', $this->data);
    }

    public function school_ajaxdrop(){

            $lgaid = $this->input->get('lgaid', TRUE);

            $this->db->where('lgaid', $lgaid);
            $this->db->order_by('schoolname')->select('schoolid as lgaid, schoolname as lganame');
            $query = $this->db->get('t_schools');


            $data['lgas'] = array();
            if ($query->num_rows() > 0)
            {
              $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;

    }

    public function getSchools($lgaid){
        $this->db->where('lgaid',$lgaid);
        $this->db->order_by('schoolname ASC');
        $query = $this->db->get('t_schools')->result();
        $school_options = array();
        if (count($query)){
            foreach($query as $data){
                $school_options[$data->schoolid]=$data->schoolname;
            }
            echo form_dropdown('schoolid',$school_options,  set_value('schoolid'),'class = "form-control required = "" style = "width:100%"');
        }

    }

}
