<?php
class Timetable extends Web_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/timetable_model');
        $this->data['active_year'] = $this->_getActiveYear();
    }

    public function index() {
        //$edcid || show_404();

        $this->_getEdcData($this->edcid);
        $this->data['edc_timetables'] = $this->timetable_model->get_where(array('edcid'=>$this->edcid));
        $this->data['subview'] = 'web/timetable_page';
        $this->load->view('web/templates/_layout_main', $this->data);
    }

    public function save($edcid, $id = null){

        if($id == null){
             $this->data['info_detail'] = $this->timetable_model->is_new();
        }
        else $this->data['info_detail'] = $this->timetable_model->get_all($id);

        $validation_rules = $this->timetable_model->_rules;
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run() == TRUE){
           //get the posted values
            $data = $this->timetable_model->array_from_post(array('timetable_content', 'timetable_title','examyear'));
            if($id == null){
                $data['timetable_id'] = $this->timetable_model->generate_unique_id();
                $this->timetable_model->delete($data['timetableid'], $edcid);
            }
            $data['edcid'] = $edcid;

            //HANDLE ASSET IMAGE
                            if (isset($_FILES['timetable_file']) && $_FILES['timetable_file']['size'] > 0) {
                                $upload_path ='./resources/uploads/timetable_files/';

                                if (!is_dir($upload_path))
                                    mkdir ($upload_path, 0777);
                                $source_file_name = 'timetable_file';
                                $destination_file_name = mt_rand($min = 0000000000, $max = 999999999);
                                @$name = $_FILES[$source_file_name]["name"];
                                @$ext = end((explode(".", $name)));

                                if ($this->perform_upload($upload_path,$source_file_name,$destination_file_name))
                                {
                                    $data['timetable_file'] = $upload_path.$destination_file_name.".".$ext;;
                                }
                                else {
                                    $this->session->set_flashdata('error',$this->data['upload_error']['error']);
                                    redirect('super/timetable/save');
                                }
                            }
            $this->timetable_model->save_update($data, $id, $edcid);
            $this->session->set_flashdata('msg', $timetableid == null ? 'Timetable Saved Successfully' : 'Timetable Updated Successfully') ;
            redirect(site_url('super/timetable/index/'.$edcid));
        }

        $this->_getEdcData($edcid);

        //Add styles and scripts for the CkEditor
        $this->data['page_level_scripts'] = '<script src="'. base_url('resources/vendors/ckeditor/ckeditor.js'). '"></script>';
        $this->data['page_level_scripts'] .= '<script src="'. base_url('resources/js/editors.js'). '"></script>';

        $this->data['subview'] = 'super/timetable_add_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }

     public function _getEdcData($edcid){

       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
       return $this->data['edcs']->edcid;

    }

    public function delete($edcid, $timetableid){
        if (isset($timetableid)){
            $timetable_data = $this->timetable_model->get_all($timetableid);
            if (count($timetable_data)) {
                if (!empty($timetable_data->timetable_file)){
                    @unlink(base_url($timetable_data->timetable_file));
                }
                    $this->timetable_model->delete($timetableid, $edcid);
                    $this->session->set_flashdata('msg', 'Timetable Deleted Successfully') ;
                    redirect(site_url('super/timetable/index/'.$edcid));
            }
        }
        else {
            redirect(site_url('super/timetable/index/'.$edcid));
        }
    }

        public function perform_upload($upload_path,$source_file_name,$destination_file_name)
        {
            #sourcefilename = the name you gave the uploaded file in the form
            #destinationfilename = the name you want to give the uploaded file in the destination
            #upload path = the directory you wish to upload the file to
            if (!empty($_FILES[$source_file_name]['name'])) {
                $config['file_name']          = $destination_file_name;
                $config['upload_path']          = $upload_path;
                $config['allowed_types']        = 'doc|docx|pdf';
                $config['max_size']             = 10240;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload($source_file_name))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $this->data['message'] = getAlertMessage($error['error']);
                        return false;
                }
                else
                {
                    #NOTHING WAS UPLOADED
                       return true;
                }
            }
            else {
                "Echo I didnt get the name";
                exit;
                return null;
            }
        }

        function _getActiveYear(){
            $this->db->select('activeyear');
            $active_year = $this->db->get('t_activeyear')->row();

            return $active_year->activeyear;
        }

}
