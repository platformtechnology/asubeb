<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pages
 *
 * @author Maxwell
 */
class Support extends Web_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('web/support_model');
        $this->load->model('web/support_comment_model');
    }
    
    public function index(){
        $edcid = $this->edcid;
        
        $this->form_validation->set_rules('phonenum', 'Phone Number', 'trim|required');
        if($this->form_validation->run()){
            $phonenum = $this->input->post('phonenum');
                       
            $this->data['support_data'] = $this->support_model->get_where(array('phone'=>$phonenum, 'edcid'=>$edcid));
            $this->data['phonenumber'] = $phonenum;
            
            $this->data['subview'] = 'web/support/support_request_page';
            $this->load->view('web/templates/_layout_main', $this->data);
            
        }
        else {
            $this->data['subview'] = 'web/support/support_page';
            $this->load->view('web/templates/_layout_main', $this->data);
        }
    }
    
    public function sendrequest() {
        $edcid = $this->edcid;
        
        $validation = $this->support_model->_rules;
        $this->form_validation->set_rules($validation);
        
        if($this->form_validation->run()){
            $data = $this->support_model->array_from_post(array('phone', 'subject', 'name', 'request'));
            $data['edcid'] = $edcid;
            $data['requestid'] = $this->support_model->generate_unique_id('numeric');
            $data['status'] = 'Pending';
            
            $this->support_model->save_update($data);

            $this->session->set_flashdata('msg', 'Request Initiated Successfully');
            redirect(site_url('web/support/requestdetails/'.$data['requestid'].'/'.$data['phone']));
        }else{
            $this->data['subview'] = 'web/support/support_request_page';
            $this->load->view('web/templates/_layout_main', $this->data);
        }
    }
    
    public function reply() {
        $edcid = $this->edcid;
        $this->form_validation->set_rules('comment', 'Reply', 'trim|required');
        
        if($this->form_validation->run()){
            $data['commentid'] = $this->support_comment_model->generate_unique_id();
            $data['comment'] = $this->input->post('comment');
            $data['requestid'] = $this->input->post('requestid');
            $data['edcid'] = $edcid;
            $data['owner'] = 'You';
            
            $this->support_comment_model->save_update($data);
            $this->support_model->save_update(array('status'=>'Pending'), $data['requestid']);
            
            $this->session->set_flashdata('Cmsg', 'Reply Posted Successfully');
            redirect(site_url('web/support/requestdetails/'.$data['requestid'].'/'.$this->input->post('phone')));
        }else{
            $this->session->set_flashdata('error', validation_errors());
            redirect(site_url('web/support/requestdetails/'.$data['requestid'].'/'.$this->input->post('phone')));
        }
    }
    
    public function requestdetails($requestid, $phone) {
        
        $edcid = $this->edcid;
                
        $this->data['request_data'] = $this->support_model->get_all($requestid);
        count($this->data['request_data']) || redirect(site_url('support'));
        
        $this->data['support_data'] = $this->support_model->get_where(array('phone'=>$phone, 'edcid'=>$edcid));
        
        $this->data['subview'] = 'web/support/support_details_page';
        $this->load->view('web/templates/_layout_main', $this->data);
    }
    
}

