<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Sms extends Super_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/scores_model');
        $this->load->model('super/exam_model');
    }

    public function save($edcid = null) {

        $edcid || show_404();

        $this->form_validation->set_rules('examid', 'Exam', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Year', 'trim|required');
        if ($this->form_validation->run() == TRUE) {
            $examid = $this->input->post('examid');
            $examyear = $this->input->post('examyear');
            $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row();

            $xml_data = $this->scores_model->get_candidate_result_xml($examid, $examyear, $edcid);

            $file_name = $examdetail->examname . "_" . $examyear . "_SCORE_DATA.xml";
            $this->load->helper('download');
            force_download($file_name, $xml_data);
        }

        //for populating EDC dropdown
        $this->load->model('super/edc_model');
        $this->data['edcs'] = $this->edc_model->get_all();

        //for displaying all Exams
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->exam_model->get_all();

        $this->_getEdcData($edcid);
        $this->data['subview'] = 'super/sms_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }

    public function resend($edcid = null) {
        $edcid || show_404();
        
        $this->form_validation->set_rules('examid', 'Exam', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Year', 'trim|required');
        $this->form_validation->set_rules('examno', 'Exam No', 'trim|required');
        
        if ($this->form_validation->run() == TRUE) {
            $phone = $this->input->post('phone');
            $examid = $this->input->post('examid');
            $examyear = $this->input->post('examyear');
            $examno = $this->input->post('examno');
            
            if(trim($phone) != false){
             
                #update the students phone number first
                $this->db->where('examid', $examid);
                $this->db->where('examyear', $examyear);
                $this->db->where('examno', $examno);
                $this->db->where('edcid', $edcid);
                $this->db->update('t_candidates', array('phone'=>$phone));
            }
            
            $score_data_array = $this->scores_model->get_candidate_result_xml($examid, $examyear, $edcid, $examno);
            
            $str = ''; $indexName = ''; $phone = '';
            foreach ($score_data_array as $key => $data) {
               $str .= $data['examno'] . "; ";
			   $str .= $data['fullname'] . "; ";
               
               $phone = $data['phone'];
               $indexName = str_replace('/', '', $data['examno']);
                       
               unset($score_data_array[$key]['candidateid']);
               unset($score_data_array[$key]['examno']);
               unset($score_data_array[$key]['phone']);
               unset($score_data_array[$key]['fullname']);
            }
            
            $score_data_array = $score_data_array[$indexName];
            foreach ($score_data_array as $key => $value) {
               #if($key == 'total') continue;
               $str .= strtoupper($key) . "=" . $value . "; ";  //"%0a"; 
            }
            
            $text_msg = rtrim($str, '; ');
            
			//echo $text_msg;
			//exit;
            //Send by SMS
            $sms_settings = config_item('sms_settings');
            $http_url = '%s?cmd=sendquickmsg&owneremail=%s&subacct=%s&subacctpwd=%s&message=%s&sender=%s&sendto=%s&msgtype=0';
            $http_url = sprintf($http_url, $sms_settings['host'], $sms_settings['owner'], $sms_settings['sub_acct'], $sms_settings['sub_acct_pass'], urlencode($text_msg), $sms_settings['sender'], $phone);

            $ch = curl_init();
            @curl_setopt($ch, CURLOPT_URL, $http_url);
            @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $status_code = @curl_exec($ch);
            @curl_close($ch);
                
            if(trim($status_code) != false){
                $codes = explode(':', $status_code);
                if($codes[0] == 'OK')$this->session->set_flashdata('searchmsg', "RESULT SENT SUCCESSFULLY");
                else $this->session->set_flashdata('searcherror', "ERROR OCCURRED - [".$codes[1]."]");
            }else $this->session->set_flashdata('searcherror', "AN ERROR OCCURRED - TRY AGAIN");
            
            redirect(site_url('super/sms/save/'.$edcid));
            
        }else{
            $this->session->set_flashdata('searcherror', validation_errors());
            redirect(site_url('super/sms/save/'.$edcid));
        }
    }
    public function _getEdcData($edcid) {

        $this->db->where('edcid', $edcid);
        $this->data['edcs'] = $this->db->get('t_edcs')->row();
        count($this->data['edcs']) || show_404();
        $this->data['edclogo'] = get_img('edc_logos/' . $this->data['edcs']->edclogo);

        return $this->data['edcs']->edcid;
    }

}
