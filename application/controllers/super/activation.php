<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Activation extends CI_Controller {
    
     function __construct() {
        parent::__construct();
        $this->load->model('super/activation_model');
    }
    
    //CURL SERVER TO GET ACTIVATION REQUEST
    public function receive(){
        $data = $this->activation_model->array_from_post(array('trial_id', 'name', 'url', 'phone', 'logo'));
        $activation_data = $this->activation_model->get_where(array('trial_id' => $data['trial_id']), true);
        if(count($activation_data)){
            $this->activation_model->save_update($data, $data['trial_id']);
        }else{
            $data['activation_id'] = generateId();
            $this->activation_model->save_update($data);
        }
        
        $sms_settings = config_item('sms_settings');
        foreach ($sms_settings['receivers'] as $reciever) {
            $http_url = '%s?cmd=sendquickmsg&owneremail=%s&subacct=%s&subacctpwd=%s&message=%s&sender=%s&sendto=%s&msgtype=0';
            $http_url = sprintf($http_url, 
                $sms_settings['host'],
                $sms_settings['owner'],
                $sms_settings['sub_acct'],
                $sms_settings['sub_acct_pass'],
                urlencode('AN ACTIVATION REQUEST HAS BEEN RECEIVED - GO TO ' . base_url() . ' NOW TO AUTHORISE'),
                $sms_settings['sender'],
                $reciever
            );
            
            $ch = curl_init();

            @curl_setopt($ch, CURLOPT_URL, $http_url);
            @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $status_code = @curl_exec($ch);
            @curl_close($ch);
        }
        
        echo 1;
    }
    
    //CURL SERVER TO GET ACTIVATION LICENSE 
    public function getLicense() {
        $data = $this->activation_model->array_from_post(array('trial_id'));
        $activation_data = $this->activation_model->get_all($data['trial_id'], true);
        if(count($activation_data)){
            if(trim($activation_data->license) != false ){
               echo $activation_data->license;
               return;
            }
        }
        echo 0;
        return;
    }
    
}
