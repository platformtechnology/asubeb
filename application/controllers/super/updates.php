<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Updates extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/edc_model');
    }
    
    public function save($edcid = null){
        
        $this->data['edcs'] = $this->edc_model->get_all();
       if($edcid == null){
           $std = new stdClass();
           $std->updateinstructions = '';
           $std->edcid = '';
           $this->data['edc_detail'] = $std;
        }else $this->data['edc_detail'] = $this->edc_model->get_all($edcid); 
                
        $this->form_validation->set_rules('edcid', 'EDC', 'trim|required');
        $this->form_validation->set_rules('updateinstructions', 'Instructions', 'trim|required');
        
        if($this->form_validation->run() == TRUE){
            $data = $this->edc_model->array_from_post(array('edcid', 'updateinstructions'));
            $data['updateavailable'] = 1;
            $data['downloaded'] = 0;
            
            $upload_data = $this->_perform_upload($data['edcid'], $edcid);
            
            $this->edc_model->save_update($data, $data['edcid'], $edcid);
            
            $this->session->set_flashdata('msg', $edcid == NULL ? 'EDC Update Saved Successfully' : 'EDC Update Was Updated Successfully') ;
            redirect(site_url('super/updates/view'));
        }
        
        //Add styles and scripts for the CkEditor        
        $this->data['page_level_scripts'] = '<script src="'. base_url('resources/vendors/ckeditor/ckeditor.js'). '"></script>';
        //$this->data['page_level_scripts'] .= '<script src="'. base_url('resources/js/editors.js'). '"></script>';
          
        $this->data['subview'] = 'super/update_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }
    
    public function view($edcid = null) {
        $this->data['edcs'] = $this->edc_model->get_where(array('updateavailable'=>1)); 
        if($edcid != null){
             $file_path_name = './resources/uploads/updates/'.$edcid.'.zip';
             if(file_exists($file_path_name)){
                $this->load->helper('download');
                $edcdata = $this->edc_model->get_all($edcid);
                $data = file_get_contents($file_path_name); // Read the file's contents
                $name = url_title(strtoupper($edcdata->edcname)).'.zip';
                
                $downloaded = $edcdata->downloaded + 1;
                $this->edc_model->save_update(array('downloaded'=>$downloaded), $edcid, $edcid);
                force_download($name, $data);
                return;
             }
        }
        $this->data['subview'] = 'super/update_view_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }
    
    private function _perform_upload($edcid, $isupdate){
       
         //if its an update and no file is selected
        if($isupdate != null && empty($_FILES['updatefile']['name'])){
            return null;
        }
        else{
           
            $file = $_FILES['updatefile'];
            if(empty($file['name'])){
              $this->session->set_flashdata('error', 'No File Uploaded - Upload Only Zip Archives');
              redirect(site_url('super/updates/save/'.$edcid)); 
            }
                    
            if($file['type'] != "application/octet-stream" || substr($file['name'], -3) != 'zip'){
               $this->session->set_flashdata('error', 'Invalid File Type - Upload Only Zip Archives');
               redirect(site_url('super/updates/save/'.$edcid));
            }
            
            $dir = './resources/uploads/updates/';
            if(!file_exists($dir)) mkdir ($dir, '0777', true);
            $file_path_name = './resources/uploads/updates/'.$edcid.'.zip';
            if (move_uploaded_file($file['tmp_name'], $file_path_name)) {
               return array('filename'=>$edcid.'.zip');
            }else{
                $this->session->set_flashdata('error', 'Error Ocurred During Upload - Try Later');
                redirect(site_url('super/updates/save/'.$edcid));
            }
            
            return null;
            
        }
    }
    
    public function delete($edcid){
        
        $file_path_name = './resources/uploads/updates/'.$edcid.'.zip';
        if(file_exists($file_path_name)) unlink ($file_path_name);
        
        $this->edc_model->save_update(array('updateavailable'=>0, 'updateinstructions'=>''), $edcid, $edcid);
        $this->session->set_flashdata('msg', 'Update Deleted Successfully');
        redirect(site_url('super/updates/view'));
    }
   
}
