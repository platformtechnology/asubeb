<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Synch_manager extends Super_Controller {

    private $ssh;

    function __construct() {
        parent::__construct();
        $this->load->model('super/synchdetail_model');
    }

    public function index() {

        $join_table_key = array('t_edcs' => 'edcid');
        $join_field = array('edcname');
        $this->data['synchfiles'] = $this->synchdetail_model->get_join($join_table_key, $join_field);

        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') . '" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';

        $this->data['subview'] = 'super/synchronisation_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }

    public function download($id) {
        $id || show_404();
        $join_table_key = array('t_edcs' => 'edcid');
        $join_field = array('edcname');
        $this->db->where('t_synchdetails.id', $id);
        $synch_data = $this->synchdetail_model->get_join($join_table_key, $join_field, true);

        if (count($synch_data)) {
            $this->load->helper('download');
            $data = $synch_data->content;
            $name = url_title('SYNCH_' . strtoupper($synch_data->edcname)) . '_' . '.sql';
            force_download($name, $data);
        }

        redirect(site_url('super/synch_manager'));
    }

    public function process($requestid) {
        $requestid || show_404();
        $request_data = $this->synchdetail_model->get_all($requestid);
        if (!count($request_data)) {
            $this->session->set_flashdata('error', 'Synchronisation Data Not Found');
            redirect(site_url('super/synch_manager'));
            return;
        }

        $db_connection = NULL;

        $dbhost = $this->db->hostname;
        $dbuser = $this->db->username;
        $dbpass = $this->db->password;
        $dbname = $this->db->database;

        $connection_string = "host=" . $dbhost . " user=" . $dbuser . " password=" . $dbpass . " dbname=" . $dbname;
        if (!($db_connection = pg_connect($connection_string))) {
            $this->session->set_flashdata('error', "DB Connection Error - " . pg_errormessage($db_connection));
            redirect(site_url('super/synch_manager'));
            return;
        }

        if(pg_send_query($db_connection, $request_data->content))pg_close($db_connection);
        
        #update the processed status
        $data = array('processed' => 1);
        $this->synchdetail_model->save_update($data, $requestid);

        $this->session->set_flashdata('msg', 'SYNCHRONISATION SCRIPT EXECUTED SUCCESSFULLY');
        redirect(site_url('super/synch_manager'));
    }

    public function delete($id) {
        $id || show_404();

        $this->synchdetail_model->delete($id);
        $this->session->set_flashdata('msg', "SYNCH FILE DELETED SUCCESSFULLY");
        redirect(site_url('super/synch_manager'));
    }

}
