<?php

class Activeyear extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/activeyear_model');
    }
    
    public function save($id = NULL){
        $this->data['year_detail'] = $this->activeyear_model->get_all(NULL, TRUE);
        if(!count($this->data['year_detail'])){
           $this->data['year_detail']  = $this->activeyear_model->is_new();
        }
        
        $validation_rules = $this->activeyear_model->_rules;
        $this->form_validation->set_rules($validation_rules);
        
        if($this->form_validation->run() == TRUE){
          $data = $this->activeyear_model->array_from_post(array('activeyear', 'closedate'));  
          $this->activeyear_model->save_update($data, $id) ;
          
          //reset the check result counter
          $sql = "update t_exams set resultchecked = 0;";
          $this->db->query($sql);
          //logInAll($sql, $this->db->get('t_edcs')->result());
          
          $this->session->set_flashdata('msg', '<strong>Active Year </strong> successfully Updated!');
          redirect(site_url('super/activeyear/save/'.$this->data['year_detail']->id));
        }           
        
        //Add styles and scripts for the datetime
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        $this->data['subview'] = 'super/activeyear_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
}
