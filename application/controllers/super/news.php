<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class News extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/news_model');
    }
    
    public function index($edcid) {
        $edcid || show_404();
        
        $this->_getEdcData($edcid);
        $this->data['news'] = $this->news_model->get_where(array('edcid'=>$edcid));
        
        $this->data['subview'] = 'super/news_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
    public function save($edcid, $newsid = null){
        
        if($newsid == null){ 
             $this->data['info_detail'] = $this->news_model->is_new();
        }
        else $this->data['info_detail'] = $this->news_model->get_all($newsid); 
        
        $validation_rules = $this->news_model->_rules;
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run() == TRUE){
          
           //get the posted values
            $data = $this->news_model->array_from_post(array('newstitle', 'news'));
            if($newsid == null){
                $data['newsid'] = $this->news_model->generate_unique_id();
                $this->news_model->delete($data['newsid'], $edcid);
            }
            $data['edcid'] = $edcid;
                    
            $this->news_model->save_update($data, $newsid, $edcid);
            $this->session->set_flashdata('msg', $newsid == null ? 'News Added Successfully' : 'News Updated Successfully') ;
            redirect(site_url('super/news/index/'.$edcid));
        }  
        
        $this->_getEdcData($edcid);
        
        //Add styles and scripts for the CkEditor        
        $this->data['page_level_scripts'] = '<script src="'. base_url('resources/vendors/ckeditor/ckeditor.js'). '"></script>';
        $this->data['page_level_scripts'] .= '<script src="'. base_url('resources/js/editors.js'). '"></script>';
                
        $this->data['subview'] = 'super/news_add_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
     public function _getEdcData($edcid){
             
       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
      
       return $this->data['edcs']->edcid;
    }
    
    public function delete($edcid, $newsid){
        
        $this->news_model->delete($newsid, $edcid);
        $this->session->set_flashdata('msg', 'News Deleted Successfully') ;
        redirect(site_url('super/news/index/'.$edcid));
    }
    
}
