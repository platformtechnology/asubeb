<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Zones extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/zones_model');
    }
    
    public function _validateZone($edcid, $zonename){
        $result = $this->zones_model->get_where(array('zonename' => $zonename, 'edcid' => $edcid), TRUE);
        if(count($result)>0){
             $this->session->set_flashdata('error', 'The Zone Already Exists - Zone Name must be Unique!');
              redirect(site_url('super/zones/save/'.$edcid));
        }
    }
    
    public function save($edcid, $zoneid = null){
        $validation_rules = $this->zones_model->_rules;
        
        if($zoneid == null){
            $this->data['zone_detail'] = $this->zones_model->is_new();
        }else $this->data['zone_detail'] = $this->zones_model->get_all($zoneid); 
        
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run() == TRUE){
          
           //get the posted values
            $data = $this->zones_model->array_from_post(array('zonename'));
            if($zoneid == NULL){
                $this->_validateZone($edcid, $data['zonename']);
                $data['zoneid'] = $this->zones_model->generate_unique_id();  //if its an insert, generated new categoryid
                $this->zones_model->delete($data['zoneid'], $edcid);
            }
            $data['edcid'] = $edcid;
            
           //validate if the zonename is unique during update
            if($zoneid != null){
                $this->db->where_not_in('zoneid', $zoneid);
                $result = $this->db->get_where('t_zones', array('zonename' => $data['zonename'], 'edcid' => $edcid))->row();
                if(count($result)>0){
                     $this->session->set_flashdata('error', 'The Zone Name Already Exists');
                     redirect(site_url('super/zones/save/'.$edcid.'/'.$zoneid));
                }
            }
            
            $this->zones_model->save_update($data, $zoneid, $edcid);
            $this->session->set_flashdata('msg', $zoneid == NULL ? 'New Zone Added Successfully' : 'Zone Updated Successfully') ;
            redirect(site_url('super/zones/save/'.$edcid));
        }  
        
        //for displaying all zones
        $this->db->where('edcid', $edcid);
        $this->db->order_by('zonename');
        $this->data['zones'] = $this->zones_model->get_all();
         
        $this->_getEdcData($edcid);
        $this->data['subview'] = 'super/zones_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
    public function delete($edcid, $zoneid){
       $this->zones_model->delete($zoneid, $edcid);
       
       $this->db->where('zoneid', $zoneid);
       $this->db->delete('t_lgas');
      logSql($this->db->last_query(), $edcid);

       $this->db->where('zoneid', $zoneid);
       $this->db->delete('t_candidates');
      logSql($this->db->last_query(), $edcid);
      
       $this->session->set_flashdata('msg', 'Zone Data Deleted Successfully');
        redirect(site_url('super/zones/save/'.$edcid));    
    }
    
    public function _getEdcData($edcid){
             
       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
      
       return $this->data['edcs']->edcid;
    }
    
}
