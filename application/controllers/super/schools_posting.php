<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Schools_posting extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/schools_model');
    }
    
    public function save($edcid, $schoolid = null){
        
        //for displaying all schools
        $this->db->where('t_schools.edcid', $edcid);
        $this->db->where('t_schools.ispostingschool', 1);
         $this->db->order_by('t_schools.schoolname');
        $this->data['schools'] = $this->schools_model->get_all();
                
        $this->_getEdcData($edcid);
        
        if($schoolid == null){
            $this->data['school_detail'] = $this->schools_model->is_new();
        }else {
            $this->data['school_detail'] = $this->schools_model->get_all($schoolid); 
        }
        
        $this->form_validation->set_rules('schoolname', 'School Name', 'trim|required');
        $this->form_validation->set_rules('schoolcode', 'School Code', 'trim|required|is_natural_no_zero');
        
        if($this->form_validation->run() == TRUE){
          
           //get the posted values
            $data = $this->schools_model->array_from_post(array('schoolname', 'schoolcode'));

            //if its an insert, generated new schoolid
            if($schoolid == NULL){
                $data['schoolid'] = $this->schools_model->generate_unique_id();  
                $this->schools_model->delete($data['schoolid'], $edcid);
            }
            
            $data['isprimary'] = 0; 
            $data['iscentre'] = 0;
            $data['issecondary'] = 0;
            $data['ispostingschool'] = 1; //it is a secondary school to post people to
            $data['edcid'] = $edcid;
                        
           //validate if the schoolname is unique during update
           $this->_validateSchool($data, $schoolid);
            
            $this->schools_model->save_update($data, $schoolid, $edcid);
            $this->session->set_flashdata('msg', $schoolid == NULL ? 'New School For Posting Added Successfully' : 'School Updated Successfully') ;
            redirect(site_url('super/schools_posting/save/'.$edcid));
        }  
        
        
        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        
        
        $this->data['subview'] = 'super/schools_posting_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
    public function delete($edcid, $schoolid){
       $this->schools_model->delete($schoolid, $edcid);
               
       $this->session->set_flashdata('msg', 'School Data Deleted Successfully');
        redirect(site_url('super/schools_posting/save/'.$edcid));    
    }
    
     public function _getEdcData($edcid){
             
       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
      
       return $this->data['edcs']->edcid;
    }
    
    public function _validateSchool($data, $schoolid){
        if($schoolid == null){
            //checking if schoolcode is a duplicate in posting recipient schools
            $result = $this->schools_model->get_where(array('schoolcode' => $data['schoolcode'], 'ispostingschool' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Code Already Exists! - School Code Must Be Unique');
                  redirect(site_url('super/schools_posting/save/'.$this->data['edcs']->edcid.'/'.$schoolid));
            }
            
            //checking if schoolname is a duplicate in posting recipient schools
            $result = $this->schools_model->get_where(array('schoolname' => $data['schoolname'], 'ispostingschool' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Name Already Exists! - School Name Must Be Unique');
                  redirect(site_url('super/schools_posting/save/'.$this->data['edcs']->edcid.'/'.$schoolid));
            }
            
        }else{
            $this->db->where_not_in('schoolid', $schoolid);
            $result = $this->schools_model->get_where(array('schoolname' => $data['schoolname'], 'ispostingschool' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Name Already Exists!');
                  redirect(site_url('super/schools_posting/save/'.$this->data['edcs']->edcid.'/'.$schoolid));
            }
            
             $this->db->where_not_in('schoolid', $schoolid);
             $result = $this->schools_model->get_where(array('schoolcode' => $data['schoolcode'], 'ispostingschool' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Code Already Exists');
                  redirect(site_url('super/schools_posting/save/'.$this->data['edcs']->edcid.'/'.$schoolid));
            }
        }
    }
    
}
