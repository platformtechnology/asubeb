<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Sliders extends Super_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    public function save($edcid = null) {

        if ($this->input->post('edcid')) {
            $config = array(
                'upload_path' => './resources/web/sliders/',
                'allowed_types' => 'jpg|png',
                'overwrite' => FALSE,
                'remove_spaces' => TRUE,
                'file_name' => $edcid . '_.png'
            );

            $this->load->library('upload', $config);

            $uploadedImages = $_FILES['sliderimage'];

            for ($i = 0; $i < count($uploadedImages['name']); $i++) {

                $_FILES['image']['name'] = $uploadedImages['name'][$i];
                $_FILES['image']['type'] = $uploadedImages['type'][$i];
                $_FILES['image']['tmp_name'] = $uploadedImages['tmp_name'][$i];
                $_FILES['image']['error'] = $uploadedImages['error'][$i];
                $_FILES['image']['size'] = $uploadedImages['size'][$i];

                $this->upload->initialize($config);

                if ($this->upload->do_upload('image')) {
                    $this->upload->data();
                } else
                    return false;
            }
        }
        $this->_getEdcData($edcid);
        $this->load->helper('directory');
        $sliders = directory_map('./resources/web/sliders/', 1);

        $this->data['sliders'] = array();
        foreach ($sliders as $slider) {
            $validslide = explode('_', $slider);
            if ($validslide[0] == $edcid)
                $this->data['sliders'][] = $slider;
        }

        $this->data['subview'] = 'super/slider_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }

    public function delete($slider, $edcid) {
        if (file_exists('./resources/web/sliders/' . $slider))
            @unlink('./resources/web/sliders/' . $slider);

        $this->session->set_flashdata('updated_msg', 'Image Deleted Successfully');
        redirect(site_url('super/sliders/save/' . $edcid));
    }

    public function _getEdcData($edcid) {

        $this->db->where('edcid', $edcid);
        $this->data['edcs'] = $this->db->get('t_edcs')->row();
        count($this->data['edcs']) || show_404();
        $this->data['edclogo'] = get_img('edc_logos/' . $this->data['edcs']->edclogo);

        return $this->data['edcs']->edcid;
    }

}
