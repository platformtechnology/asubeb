<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Printlayout extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/layout_model');
    }

    public function save($edcid){
        
        if($this->input->post('submitTracker')){
            $subjects = $this->input->post('subjectid');
            $numofcols = array_values(array_filter($this->input->post('numofcols')));

            
            if(count($subjects) == count($numofcols)){
                $sql = "DELETE FROM t_printlayout WHERE edcid = '" . $edcid . "';";
                $this->db->query($sql);
                                
                for($i = 0; $i < count($subjects); $i++){
                    $data = array();

                    $data['subjectid'] = $subjects[$i];
                    $data['numofcols'] = $numofcols[$i];
                    $data['edcid'] = $edcid;

                    $this->layout_model->save_update($data);
                }

                $this->session->set_flashdata('msg', 'Layout Settings Saved Successfully');
                redirect('super/printlayout/save/' . $edcid);
            }
            else{
                $sql = "DELETE FROM t_printlayout WHERE edcid = '" . $edcid . "'";
                $this->db->query($sql);
                //$this->data['error'] = 'Input No Of Cols For Each Selected Subject';
            }
        }
        //for displaying all subjects
        $this->db->where('edcid', $edcid);
        $this->data['subjects'] = $this->db->get('t_subjects')->result();
         
        $this->db->where('edcid', $edcid);
        $this->data['criterias'] = $this->layout_model->get_all();
        
        $this->_getEdcData($edcid);
        $this->data['subview'] = 'super/layout_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
    public function _getEdcData($edcid){
             
       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
      
       return $this->data['edcs']->edcid;
    }
    
}
