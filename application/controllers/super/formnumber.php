<?php

class Formnumber extends Super_Controller{

    function __construct() {
        parent::__construct();
 
    }

    function index() {
        //GET FORM NUMBER STAT
        $this->data['form_number_stat'] = $this->_get_formnumber_stat ();

        //SET RULES FOR FORM NUMBER CONFIRMATION
        $this->form_validation->set_rules('formnumber', 'Form Number', 'trim|required');
        if($this->form_validation->run() == TRUE){
            //GET FORM NUMBER FROM POST
            $formnumber = $this->input->post('formnumber');
            //CONFIRM IF FORM NUMBER EXISTSW
            $sql = "SELECT * FROM t_form_numbers WHERE formnumber ='".$formnumber."'";
            $form_number_exists = $this->db->query($sql)->result();
            if (count($form_number_exists)) {
                    //NOW LETS GET FULL DETAILS OF ASSOCIATED CANDIDATE
                    $sql = "SELECT * FROM t_form_numbers "
                            . " INNER JOIN t_pins ON t_pins.pin = t_form_numbers.pin"
                            . " INNER JOIN t_candidates ON t_candidates.candidateid = t_pins.candidateid"
                            . " INNER JOIN t_schools ON t_candidates.schoolid = t_schools.schoolid"
                            . " INNER JOIN t_exams ON t_candidates.examid = t_exams.examid"
                            . " INNER JOIN t_edcs ON t_candidates.edcid = t_edcs.edcid"
                            . " WHERE formnumber ='".$formnumber."'";
                    $form_number_details = $this->db->query($sql)->result();
                    if (count($form_number_details)) {
                        foreach($form_number_details as $details):
                            $details_array = $details;
                        endforeach;
                        $this->data['form_number_details'] = $details_array;
                    }
                    else {
                         $this->data['error_message'] = get_success("Form Number has not been used by any Candidate yet");
                    }
            }
            else {
                //SHOW ERROR
                $this->data['error_message'] = get_error("Invalid Form Number Entered");
            }

        }

        $this->data['subview'] = 'super/formnumber_support_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }

    public function _get_formnumber_stat () {
        $form_number_stat = array();
        $form_number_count = 0;
        $used_form_number_count = 0;
        //GET TOTAL FORM NUMBERS
        $sql = "SELECT count(*) as total_form_numbers from t_form_numbers";
        $form_number_data = $this->db->query($sql)->result();
            if (count($form_number_data)) {
                foreach ($form_number_data as $data):
                $form_number_count = $data->total_form_numbers;
                endforeach;
            }

        //GET USED FORM NUMBERS
        $sql = "SELECT count(*) from t_form_numbers where  pin != '' and examid != '' and edcid != ''";
        $used_form_number_data = $this->db->query($sql)->result();
            if (count($used_form_number_data)) {
                foreach ($used_form_number_data as $used_form_number_data):
                    $used_form_number_count = $used_form_number_data->count;
                endforeach;
            }

        //ASSIGN THE GOTTEN VALUES TO ARRAY ELEMENTS
        $form_number_stat['total'] = $form_number_count;
        $form_number_stat['used'] = $used_form_number_count;


        //RETURN TO THE REQUESTOR
        return $form_number_stat;
    }

}
