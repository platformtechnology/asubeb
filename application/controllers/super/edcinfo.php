<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Edcinfo extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/edc_info_model');
    }
    
    public function save($edcid){
        
        $this->data['info_detail'] = $this->edc_info_model->get_all($edcid); 
        count($this->data['info_detail']) || show_404();
        
        $validation_rules = $this->edc_info_model->_rules;
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run() == TRUE){
          
           //get the posted values
            $data = $this->edc_info_model->array_from_post(array('homeinfo', 'aboutinfo'));
            
            $this->edc_info_model->save_update($data, $edcid);
            
            $this->session->set_flashdata('msg', 'Site Info Updated Successfully') ;
            redirect(site_url('super/edcinfo/save/'.$edcid));
        }  
        
        $this->_getEdcData($edcid);
        
        //Add styles and scripts for the CkEditor        
        $this->data['page_level_scripts'] = '<script src="'. base_url('resources/vendors/ckeditor/ckeditor.js'). '"></script>';
        $this->data['page_level_scripts'] .= '<script src="'. base_url('resources/js/editors.js'). '"></script>';
                
        $this->data['subview'] = 'super/edc_info_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
     public function _getEdcData($edcid){
             
       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
      
       return $this->data['edcs']->edcid;
    }
    
}
