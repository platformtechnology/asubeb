<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Exams extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/exam_model');
    }
    
    public function save($edcid, $examid = null){
                
        if($examid == null){
            $this->data['exam_detail'] = $this->exam_model->is_new();
        }else $this->data['exam_detail'] = $this->exam_model->get_all($examid); 
        
        $validation_rules = $this->exam_model->_rules;
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run() == TRUE){
           //get the posted values
            $data = $this->exam_model->array_from_post(array('examname', 'examdesc', 'edcid', 'hasposting', 'hassecschool', 'hasca', 'haspractical', 'haszone'));
            if($examid == NULL){
                $data['examid'] = $this->exam_model->generate_unique_id();  //if its an insert, generated new id
                $this->exam_model->delete($data['examid'], $data['edcid']);
            }
            if(empty($data['hasposting'])) $data['hasposting'] = 0;
            if(empty($data['hassecschool'])) $data['hassecschool'] = 0;
            if(empty($data['hasca'])) $data['hasca'] = 0;
            if(empty($data['haspractical'])) $data['haspractical'] = 0;
            if(empty($data['haszone'])) $data['haszone'] = 0;
            //if(empty($data['haslga'])) $data['haslga'] = 0;
            
           //validate if the examname is unique within same edc
           $this->_validExam($data, $examid);
            
            $this->exam_model->save_update($data, $examid, $data['edcid']);
            $this->session->set_flashdata('msg', $examid == NULL ? 'New Exam Added Successfully' : 'Exam Updated Successfully') ;
            redirect(site_url('super/exams/save/'.$edcid));
        }  
        
        //for populating EDC dropdown
        $this->load->model('super/edc_model');
        $this->data['edcs'] = $this->edc_model->get_all();
         
        //for displaying all Exams
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->exam_model->get_all();
        
         //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        
         $this->_getEdcData($edcid);
        $this->data['subview'] = 'super/exam_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
    public function delete($examid, $edcid){
       $this->exam_model->delete($examid, $edcid);
       
       $sql = "DELETE FROM t_candidates WHERE examid = '" . $examid . "' AND edcid = '" . $edcid ."'; ";
       $this->db->query($sql);
       logSql($sql, $edcid);
       
       $this->session->set_flashdata('updated_msg', 'Exam Data Deleted Successfully');
       redirect(site_url('super/exams/save/'.$edcid));      
    }    
    
    public function _validExam($data, $examid){
        if($examid == null){
            $result = $this->exam_model->get_where(array('examname' => $data['examname'], 'edcid' => $data['edcid']));
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Exam, ' . $data['examname'] . ', Already Exists In Selected EDC - Exam Must Be Unique per EDC');
                 redirect(site_url('super/exams/save/'.$examid));
            }
        }else{
            $this->db->where_not_in('examid', $examid);
            $result = $this->exam_model->get_where(array('examname' => $data['examname'], 'edcid' => $data['edcid']));
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Exam, ' . $data['examname'] . ', Already Exists In Selected EDC - Exam Must Be Unique per EDC');
                 redirect(site_url('super/exams/save/'.$examid));
            }
        }
    }
    
    public function _getEdcData($edcid){
             
       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
      
       return $this->data['edcs']->edcid;
    }
}
