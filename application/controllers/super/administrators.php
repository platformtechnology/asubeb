<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Administrators extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/users_model');
    }
    
    public function save($userid = null){
        $validation_rules = $this->users_model->_rules;
        if($userid == null){
            $this->data['admin_detail'] = $this->users_model->is_new();
            $validation_rules['email']['rules'] .= '|is_unique[t_users.email]';
        }else $this->data['admin_detail'] = $this->users_model->get_all($userid); 
        
        unset($validation_rules['password']);
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run() == TRUE){
           $default_pass = config_item('default_edcadmin_pass');
           
           //get the posted values
            $data = $this->users_model->array_from_post(array('fullname', 'email', 'phone', 'edcid','privilege'));
            if($userid == NULL){
                $data['userid'] = $this->users_model->generate_unique_id();  //if its an insert, generated new categoryid
                $data['password'] = encrypt($default_pass);
                $this->users_model->delete($data['userid'], $data['edcid']);
            }
            
            if($data['edcid'] == 'platform') $data['role'] = $data['edcid'];
            else $data['role'] = 'Super_Administrator';
            
           //validate if the email is unique during update
            if($userid != null){
                $this->db->where_not_in('userid', $userid);
                $result = $this->db->get_where('t_users', array('email' => $data['email']))->row();
                if(count($result)>0){
                     $this->session->set_flashdata('error', 'The Admin Email, ' . $data['email'] . ', Already Exists - Email Must Be Unique');
                     redirect(site_url('super/administrators/save/'.$userid));
                }
            }
            
            $this->users_model->save_update($data, $userid, $data['edcid']);
            $this->session->set_flashdata('msg', $userid == NULL ? 'New Administrator Added Successfully' : 'Administrator Updated Successfully') ;
            redirect(site_url('super/administrators/save'));
        }  
        
        //for populating EDC dropdown
        $this->load->model('super/edc_model');
        $this->data['edcs'] = $this->edc_model->get_all();
        
        //for displaying all administrators
        $this->data['administrators'] = $this->users_model->get_all();
         
        $this->data['subview'] = 'super/administrators_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
    public function delete($userid){
       $userdata = $this->users_model->get_all($userid); 
       if(count($userdata)){
            $this->users_model->delete($userid, $userdata->edcid);
            $this->session->set_flashdata('updated_msg', 'Administrator Data Deleted Successfully');
            redirect(site_url('super/administrators/save'));      
       }
    }
    
    public function reset($userid){
       $data['password'] = encrypt(config_item('default_edcadmin_pass'));
       $userdata = $this->users_model->get_all($userid); 
       $this->users_model->save_update($data, $userid, $userdata->edcid);
       $this->session->set_flashdata('updated_msg', 'Administrator Password Reset Successfull');
       redirect(site_url('super/administrators/save'));      
    }
}
