<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Dashboard extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
    }
    
    public function index(){
        
        $this->data['page_level_scripts'] = '<link href="' . base_url('resources/vendors/morris/morris.css') .'" rel="stylesheet">';
        
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/raphael-min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/morris/morris.min.js') . '"></script>';
        
        //for populating EDC dropdown
        $this->load->model('super/edc_model');
        $this->db->order_by('id');
        $this->data['edcs'] = $this->edc_model->get_all();
        
        if(count($this->data['edcs'])){
            // get the EDC that would be selected and reported for
            $this->data['selectedEdc'] = $this->input->post('edcid') ? $this->input->post('edcid') : $this->data['edcs'][0]->edcid;
            
            $edcdata = $this->edc_model->get_all($this->data['selectedEdc'], true);
            $this->data['edc_name'] = $edcdata->edcname;
            // select distinct examyear for use in creating JSON data 
            $sql = "select distinct examyear from t_candidates where edcid = '".$this->data['selectedEdc']."'";
            
            //sql query to get count of students per exam registered for all exam year - used for JSON
            $candidsql = "select count(t_candidates.*) as candidatecount, t_exams.examname, t_exams.resultchecked, t_candidates.examyear from t_candidates 
                    inner join t_exams 
                    on t_candidates.examid = t_exams.examid 
                    where t_candidates.edcid = '" . $this->data['selectedEdc'] . "' ";
            
             $examsql = $candidsql . "group by t_exams.examname, t_candidates.examyear, t_exams.resultchecked 
                    order by t_exams.examname";
            
            
            //get student summary - count of student per exam for the active year
            $candidate_summary_sql = $candidsql . "and t_candidates.examyear = '" . $this->data['activeyear'] . "' 
                    group by t_exams.examname, t_candidates.examyear, t_exams.resultchecked
                    order by t_exams.examname";
            $this->data['candidate_summary'] = $this->db->query($candidate_summary_sql)->result();
            
            $objectArray = array();
            $examTypeArray = array();
            
            //prepare JSON data
            $yearResultSet = $this->db->query($sql)->result();
            $examResultSet = $this->db->query($examsql)->result();
            foreach ($yearResultSet as $yeardata) {
                
                $bufferClass = new stdClass();
                $bufferClass->examyear = $yeardata->examyear;
                
                $sn = 0;
                foreach($examResultSet as $examdata){
                    $exam = 'examtype'.++$sn;
                    $count = 'count'.$sn;
                    
                    if($yeardata->examyear == $examdata->examyear){
                        
                        $bufferClass->$exam = intval($examdata->candidatecount);
                        if(!in_array($examdata->examname, $examTypeArray)){
                            $examTypeArray[] = $examdata->examname;
                        }
                        //$bufferClass->$count = intval($examdata->candidatecount);
                    }
                }
                
                $objectArray[] = $bufferClass;
            }
            
            $this->data['reg_exams_by_year'] = json_encode($objectArray);
            $this->data['exam_series_name'] = json_encode($examTypeArray);

            //get sumary of uploaded pins
            $sql = "select t_pins_log.*, edcname from t_pins_log 
                    inner join t_edcs on t_pins_log.edcid = t_edcs.edcid
                    ";
            $this->data['pin_upload_summary'] = $this->db->query($sql)->result();
            
            //Get summary of used pin count per EDC
            $sql = "select count(t_pins.*) as pincount, t_pins.examyear, t_edcs.edcname from t_pins
                    inner join t_edcs
                    on t_pins.edcid = t_edcs.edcid
                    where t_pins.candidateid != '' and t_pins.candidateid is not null
                    group by t_edcs.edcname, t_pins.examyear order by t_edcs.edcname";
            $this->data['pinstat_summary'] = $this->db->query($sql)->result();
                        
        }
        
       // print_r($this->data['usedpin_summary']); exit;
        
        $this->data['subview'] = 'super/dashboard_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }
}
