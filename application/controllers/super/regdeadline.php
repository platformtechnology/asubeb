<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Regdeadline extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/deadline_model');
    }
    
    public function save($edcid = NULL, $id = NULL){
        $edcid || show_404();
        $validation_rules = $this->deadline_model->_rules;
        $this->_getEdcData($edcid);
        
        if($id == NULL){
            $this->data['deadline_detail']  = $this->deadline_model->is_new();
            $validation_rules['exam']['rules'] .= '|is_unique[t_deadlines.examid]';
        }
        else $this->data['deadline_detail'] = $this->deadline_model->get_all($id);
        
        $this->form_validation->set_rules($validation_rules);
        
        if($this->form_validation->run() == TRUE){
          $data = $this->deadline_model->array_from_post(array('examid', 'closedate'));
          $data['edcid'] = $edcid;
          
          if($id != NULL){
              $this->db->where('edcid', $edcid);
              $this->db->where('examid', $data['examid']);
              $this->db->where_not_in('id', $id);
              if(count($this->deadline_model->get_all())){
                  $this->session->set_flashdata('error', 'The Newly Selected Exam Already Has a Deadline');
                  redirect(site_url('super/regdeadline/save/'.$edcid.'/'.$id));
              }
          }
          $this->deadline_model->save_update($data, $id, $edcid) ;
          
          $this->session->set_flashdata('msg', '<strong>Registration Deadline </strong> Successfully Updated!');
          redirect(site_url('super/regdeadline/save/'.$edcid));
        }           
        
        //Add styles and scripts for the datetime
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        //for displaying all deadlines and Exams
        $this->data['deadlines'] = $this->deadline_model->get_where(array('edcid'=>$edcid));
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();
        $this->load->model('super/subjects_model');
        
        $this->data['subview'] = 'super/deadline_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
    public function delete($edcid = null, $id = null) {
        $id || show_404();
        $edcid || show_error("UNIDENTIFIED EDC");
        $this->deadline_model->delete($id, $edcid);
        $this->session->set_flashdata('updated_msg', '<strong>Registration Deadline </strong> DELETED Successfully!');
        redirect(site_url('super/regdeadline/save/'.$edcid));
    }
    
     public function _getEdcData($edcid){
             
       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
      
       return $this->data['edcs']->edcid;
    }
    
}
