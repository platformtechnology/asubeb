<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Pinstat extends Super_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->data['edcs'] = $this->db->get('t_edcs')->result();

        $this->data['page_level_scripts'] = '<link href="' . base_url('resources/vendors/morris/morris.css') . '" rel="stylesheet">';

        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/raphael-min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/morris/morris.min.js') . '"></script>';


        //get sumary of uploaded pins
        $sql = "select t_pins_log.*, edcname from t_pins_log 
                inner join t_edcs on t_pins_log.edcid = t_edcs.edcid
                ";
        $this->data['pin_upload_summary'] = $this->db->query($sql)->result();


        //Get summary of used pin count per EDC
        $sql = "select count(t_pins.*) as pincount, t_pins.examyear, t_edcs.edcname from t_pins
                inner join t_edcs
                on t_pins.edcid = t_edcs.edcid
                where t_pins.candidateid != '' and t_pins.candidateid is not null
                group by t_edcs.edcname, t_pins.examyear order by t_edcs.edcname";
        $this->data['pinstat_summary'] = $this->db->query($sql)->result_array();

        //Get name for the series display in graph
        $temp = array();
        foreach ($this->data['pinstat_summary'] as $value) {
            if (!in_array($value['edcname'], $temp)) {
                $temp[] = $value['edcname'];
            }
        }
        $this->data['pinstat_summary'] = json_encode($this->data['pinstat_summary']);
        $this->data['pinstat_series'] = json_encode($temp);

        //Get usedpin count
        $sql = "select count(*) as pincount from t_pins where candidateid != '' and candidateid is not null ";
        $this->data['pinstat_usedpin_count'] = $this->db->query($sql)->row();

        $this->data['subview'] = 'super/pinlog_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }

    public function select() {

        $edcid = $this->input->post('edcid');
        $edcid || show_404();
        redirect(site_url('super/pinstat/search/' . $edcid));
    }

    public function search($edcid = null, $pinid = null) {
        $edcid || show_404();

        //get sumary of uploaded pins
        $sql = "select t_pins_log.*, edcname from t_pins_log 
                inner join t_edcs on t_pins_log.edcid = t_edcs.edcid
                ";
        $this->data['pin_upload_summary'] = $this->db->query($sql)->result();

        //Get summary of used pin count per EDC
        $sql = "select count(t_pins.*) as pincount, t_pins.examyear, t_edcs.edcname from t_pins
                inner join t_edcs
                on t_pins.edcid = t_edcs.edcid
                where t_pins.candidateid != '' and t_pins.candidateid is not null
                group by t_edcs.edcname, t_pins.examyear order by t_edcs.edcname";
        $this->data['pinstat_summary'] = $this->db->query($sql)->result();


        $this->load->model('super/pin_model');
        $active_year = $this->data['activeyear'];

        if ($this->input->post('resetpin')) {

            $pin = $this->input->post('resetpin');
            $pindata = $this->db
                            ->where('pin', $pin)
                            ->where('edcid', $edcid)
                            ->where('examyear', $active_year)
                            ->limit(1)->get('t_pins')->row();
            //$pindata = $this->pin_model->get_all($pin, TRUE);

            if (count($pindata)) {
                if (trim($pindata->candidateid) == false) {
                    $this->session->set_flashdata('error_reset', 'PIN <strong>' . $pin . '</strong>, HAS <strong>NOT BEEN USED</strong> YET');
                    redirect(site_url('super/pinstat/search/' . $edcid));
                } else {

                    $SQL = "UPDATE t_pins set examid = null, candidateid = null, dateused = null where id = " . $pindata->id;
                    $this->db->query($SQL);

                    $this->session->set_flashdata('msg', 'PIN <strong>' . $pin . '</strong>, HAS <strong>BEEN RESET</strong> SUCCESSFULLLY');
                    redirect(site_url('super/pinstat/search/' . $edcid));
                }
            } else {
                $pindata = $this->db
                                ->where('pin', $pin)
                                ->where('edcid', edcid)
                                ->limit(1)->get('t_pins')->row();
                if (count($pindata)) {
                    $this->session->set_flashdata('error_reset', 'PIN NOT VALID FOR ACTIVE YEAR (' . $active_year . ')');
                    redirect(site_url('super/pinstat/search/' . $edcid));
                }
                $this->session->set_flashdata('error_reset', 'PIN DOES NOT EXIST!');
                redirect(site_url('super/pinstat/search/' . $edcid));
            }

            #PERFORM SEARCH
        } else if ($this->input->post('searchpin')) {

            $this->form_validation->set_rules('pin', 'PIN', 'trim');
            $this->form_validation->set_rules('serial', 'Serial', 'trim');

            if ($this->form_validation->run() == TRUE) {

                $pin = $this->input->post('pin');
                $serial = $this->input->post('serial');

                $sql = "select * from t_pins 
                        where edcid = '".$edcid."'
                        and (pin = '".$pin."' or serial = '".$serial."') ";
                $pin_data = $this->db->query($sql);

                if ($pin_data->num_rows() > 1)
                    $this->data['variedpins'] = $pin_data->result();
                else if ($pin_data->num_rows() == 1) {

                    $pindata = $pin_data->row();
                    $this->_show_response($pindata, $edcid);
                    
                } else {
                    $this->session->set_flashdata('error', 'PIN DOES NOT EXIST!');
                    redirect(site_url('super/pinstat/search/'.$edcid));
                }
            }
        }

        if($pinid != null){
             $this->db->where('id', $pinid);
             $pindata = $this->db->get('t_pins')->row();
        
             $this->_show_response($pindata, $edcid);
        }
        $this->data['edcid'] = $edcid;
        $this->data['edcname'] = $this->db->where('edcid', $edcid)->get('t_edcs')->row()->edcname;
        $this->data['subview'] = 'super/pinstat_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }

    public function _show_response($pindata, $edcid) {
        
        if (trim($pindata->candidateid) == false) {
            $this->session->set_flashdata('msg_active', 'PIN <strong>' . $pindata->pin . '</strong>, <strong>EXIST</strong> - IT HAS <strong>NOT BEEN USED</strong>');
             redirect(site_url('super/pinstat/search/'.$edcid));
        }

        $this->load->model('super/registrant_model');
        //Get Candidate that used pin
        $this->data['reg_info'] = $this->registrant_model->get_all($pindata->candidateid);
        count($this->data['reg_info']) || show_error("GHOST CANDIDATE DETECTED !! - THE CANDIDATE THAT USED THE PIN CANNOT BE FOUND IN THE SYSTEM!");

        //Get Exam Detail that the candidate registered for
        $this->load->model('super/exam_model');
        $this->data['exam_info'] = $this->exam_model->get_all($this->data['reg_info']->examid);

        $this->data['subjects'] = $this->db->get_where('t_registered_subjects', array('candidateid' => $pindata->candidateid))->result();

        $this->data['edcs'] = $this->db->get_where('t_edcs', array('edcid' => $this->data['reg_info']->edcid))->row();
        count($this->data['edcs']) || show_error("UNKNOWN EDC DETECTED - GHOST MODE");

        $edc_url = $this->data['edcs']->edcweburl;
        $this->data['passport'] = $edc_url . '/passports/' . $pindata->candidateid . '.jpg';

        //Add styles for the passport display
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') . '" rel="stylesheet">';
    }

    public function fetchpin($edcid = null) {
        $edcid || show_404();

        $active_year = $this->data['activeyear'];
        $sql = "select * from t_pins 
                where edcid = '" . $edcid . "' 
                and examyear = '" . $active_year . "' 
                and (candidateid = '' or  candidateid is null) 
                limit 1; ";
        $rset = $this->db->query($sql)->row();

        $this->data['fetchedunsedpin'] = "NO UNUSED PIN LEFT!";
        if (count($rset))
            $this->data['fetchedunsedpin'] = $rset->pin;

        $this->session->set_flashdata('unusedpin', $this->data['fetchedunsedpin']);
        redirect(site_url('super/pinstat/search/' . $edcid));
    }

}
