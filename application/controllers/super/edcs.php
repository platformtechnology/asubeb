<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Edcs extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/edc_model');
    }
    
    public function index(){
        $this->data['edcs'] = $this->edc_model->get_all();
        $this->data['subview'] = 'super/edc_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }
    
    public function save($edcid = null){
        
        $validation_rules = $this->edc_model->_rules;
        if($edcid == null):            
            $this->data['edc_detail'] = $this->edc_model->is_new();
            $validation_rules['name']['rules'] .= '|is_unique[t_edcs.edcname]';
            $validation_rules['url']['rules'] .= '|is_unique[t_edcs.edcweburl]';
            $validation_rules['admin_email']['rules'] .= '|is_unique[t_users.email]';
        else:
            $jointable = array('t_users'=>'edcid');
            $joinfields = array('email', 'fullname', 'phone');
            $this->db->where('t_edcs.edcid', $edcid);
            $this->data['edc_detail'] = $this->edc_model->get_join($jointable, $joinfields, TRUE);
        endif;
        $this->form_validation->set_rules($validation_rules);
        
        if($this->form_validation->run() == TRUE){
                        
            $post_data = $this->edc_model->array_from_post(array('edcname', 'edcweburl' ,'edcphone', 'edcaddress', 'themecolor'));
             
//            if(empty($post_data['haszone'])) $post_data['haszone'] = 0;
//            if(empty($post_data['haslga'])) $post_data['haslga'] = 0;
            
            if($edcid == null):      
                $post_data['edcid'] = $this->edc_model->generate_unique_id($numeric = true);
                $this->data['edcid'] = $post_data['edcid'];
                $this->edc_model->delete($post_data['edcid'], $post_data['edcid']);
            else:
                //if its an update check for duplicate in other records apart from current editing record
                $this->_checkunique_details($edcid, $post_data['edcname'], $post_data['edcweburl']);            
            endif;
            
            if(($upload_data = $this->_perform_upload($edcid == null ? $post_data['edcid'] : $edcid)) != null){
                $post_data['edclogo'] = $upload_data['file_name'];
            }
            
            //Save or Update Edc Details depending on ID
            $this->_upload_stamp($edcid == null ? $post_data['edcid'] : $edcid);
            $this->edc_model->save_update($post_data, $edcid, $edcid);
            
            //Save or Update Admin Details
            $this->load->model('super/users_model');
            
            $default_admin_pass = config_item('default_edcadmin_pass');
            $post_data = $this->users_model->array_from_post(array('email', 'phone', 'fullname'));
            $post_data['role'] = 'Super_Administrator';
            $post_data['password'] = encrypt($default_admin_pass);
            
            if($edcid == null):
                $post_data['userid'] = $this->users_model->generate_unique_id();
                $post_data['edcid']  = $this->data['edcid'];
                $this->users_model->delete($post_data['userid'], $post_data['edcid']);
                $this->users_model->save_update($post_data, null, $post_data['edcid']);
                
                //insert the default school info - about, news etc
                $this->load->model('super/info_model');
                $data = array();
                $data['infoid'] = $this->info_model->generate_unique_id();
                $data['edcid'] = $post_data['edcid']; 
                $this->info_model->delete($data['infoid'], $data['edcid']);
                $this->info_model->save_update($data, null, $post_data['edcid']);
                
            else:
                 $this->_checkunique_details($edcid, null, null, $post_data['email']);            
                 $this->db->where('edcid', $edcid);
                 $this->db->update('t_users', $post_data);
                 logSql($this->db->last_query(), $edcid);
            endif; 
            
            $this->session->set_flashdata('msg', $edcid == NULL ? 'EDC Details Saved Successfully' : 'EDC Detail Was Updated Successfully') ;
            redirect(site_url('super/edcs'));   
        }
        
        //Add styles and scripts for the fileupload
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/js/bootstrap-fileupload.js') . '"></script>';
        
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        //load view
        $this->data['subview'] = 'super/edc_add_page';  
        $this->load->view('super/template/_layout_main', $this->data);
    }
    
    public function _checkunique_details($edcid, $edcname = null, $edcurl = null, $adminemail = null){
        
        if($edcname != null){
           $this->db->where(array('edcname' => $edcname));
           $this->db->where_not_in('edcid', $edcid);
           $result = $this->edc_model->get_all();
           if(count($result)>0){
                $this->session->set_flashdata('error', 'The EDC NAME Already Exists - Name Must Be Unique');
                redirect(site_url('super/edcs/save/'.$edcid));
           }
        }

        if($edcurl != null){
           $this->db->where(array('edcweburl' => $edcurl));
           $this->db->where_not_in('edcid', $edcid);
           $result = $this->edc_model->get_all();
           if(count($result)>0){
                $this->session->set_flashdata('error', 'The EDC WEB URL Already Exists - URL Must Be Unique');
                redirect(site_url('super/edcs/save/'.$edcid));
           }
        }
        
        if($adminemail != null){
            $this->db->where_not_in('edcid', $edcid);
            $result = $this->db->get_where('t_users', array('email' => $adminemail))->row();
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Admin Email, ' . $adminemail . ', Already Exists - Administrator Email Must Be Unique');
                 redirect(site_url('super/edcs/save/'.$edcid));
            }
        }
    }
    
    private function _perform_upload($edcid){
       
        if($edcid != null && empty($_FILES['edclogo']['name'])){
            return null;
        }
        else{
            $config = array(
                 'upload_path' => config_item('edc_logo_path'),
                 'allowed_types' => 'jpg|png',
                 'overwrite' => TRUE,
                 'remove_spaces' => TRUE,
                 'file_name' => $edcid.'.png'
             );
            $this->load->library('upload');
             $this->upload->initialize($config);
            if (!$this->upload->do_upload('edclogo'))
            {
               $this->session->set_flashdata('error', $this->upload->display_errors());
               redirect(site_url('super/edcs/save/'.$edcid));
            } 

            return $this->upload->data();
        }
    }
    
    private function _upload_stamp($edcid){
       
        if(empty($_FILES['stamp']['name'])){
            return null;
        }
        else{
            $config = array(
                 'upload_path' => config_item('edc_logo_path'),
                 'allowed_types' => 'jpg|png',
                 'overwrite' => TRUE,
                 'remove_spaces' => TRUE,
                 'file_name' => $edcid.'_stamp.png'
             );
            $this->load->library('upload');
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('stamp'))
            {
               $this->session->set_flashdata('error', 'Edc Stamp - '.$this->upload->display_errors());
               redirect(site_url('super/edcs/save'));
            } 

            return $this->upload->data();
        }
    }
    
    public function delete($edcid){
             
       $files = $this->edc_model->get_all($edcid, TRUE);
       if(count($files)){
           $this->edc_model->delete($edcid, $edcid);

           $logo_name = $files->edclogo;
           $logo_file = APPPATH . config_item('edc_logo_path') . $logo_name;
           
           if(file_exists($logo_file)){
               unlink($logo_file);
           }
           
       }
              
        $tables = $this->db->list_tables();
        foreach ($tables as $tablename) {
            if ($this->db->field_exists('edcid', $tablename)){
                $this->db->where('edcid', $edcid);
                $this->db->delete($tablename);
                logSql($this->db->last_query(), $edcid);
            } 
        }
        
       $this->session->set_flashdata('msg', 'EDC Data Deleted Successfully');
       redirect('super/edcs');
      
    }
    
    public function view(){
     
       $this->data['edcs'] = $this->edc_model->get_all();
       $this->data['subview'] = 'super/edc_view_page';
       $this->load->view('super/template/_layout_main', $this->data);
    }

}
