<?php

class Registration extends Super_Controller{

    function __construct() {
        parent::__construct();
        $this->load->model('Super/registration_model');
    }

    function index() {
        $rules = $this->registration_model->_rules;
        $this->form_validation->set_rules($rules);


        if ($this->form_validation->run() == TRUE) {
            //GET THE PIN
            $pin = $this->input->post('pin');

            //CHECK IF PIN IS VALID
            $this->db->where('pin',$pin);
            $pinData = $this->db->get('t_pins');
                    if($pinData->num_rows() > 0){
                        //PIN EXISTS, PROCEED
                    //SEARCH FOR THE CANDIDATE WITH THE PIN

                            $query = "SELECT * from t_pins "
                                    . " INNER JOIN t_candidates ON t_candidates.candidateid = t_pins.candidateid"
                                    . " INNER JOIN t_schools on t_candidates.schoolid = t_schools.schoolid "
                                    . "LEFT JOIN t_form_numbers on t_form_numbers.pin = t_pins.pin"
                                    . " WHERE t_pins.pin = '".$pin."'";
                            $result = $this->db->query($query)->result();

                            if (count($result)){
                                foreach($result as $data){
                                    $candidate_record = $data;
                                }
                                $this->data['candidate_record'] =$candidate_record;
                            }
                            else {
                                $this->session->set_flashdata('error','Pin is not associated with any candidate yet');
                                redirect('super/registration');
                            }

                    }
                    else {
                        $this->data['error_message'] = get_error('The Pin you entered is invalid');
                    }

        }
        else {
            $this->data['error_message'] = trim(validation_errors()) !=FALSE ? get_error(validation_errors()) : '';
        }
        $this->data['subview'] = 'super/registration_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }

    function reset($candidateid,$pinNumber,$formnumber) {
        isset($candidateid)|| redirect('super/registration');

        //RECURSIVELY DELETE CANDIDATES RECORDS

        //DELETE RECORDS FROM CANDIDATES TABLE
        $deleteQuery = "DELETE FROM t_candidates where candidateid =  '".$candidateid."';";
        $deleteQuery .= "DELETE FROM t_registered_subjects where candidateid =  '".$candidateid."';";
        $deleteQuery .= "DELETE FROM t_scores where candidateid =  '".$candidateid."';";
        $deleteResult = $this->db->query($deleteQuery);

        //RESET THE PIN
        $PinResetQuery = "UPDATE t_pins "
                . "SET "
                . " candidateid = '',"
                . " dateused = NULL"
                . " WHERE pin = '".$pinNumber."'";
        $PinResetResult = $this->db->query($PinResetQuery);

        //RESET THE FORM NUMBER
        $formNumberResetQuery = "UPDATE t_form_numbers "
                . " SET pin = '',"
                . " edcid = '',"
                . " examid = '',"
                . "dateused = NULL"
                . "WHERE formnumber = '".$formnumber."'";
        $formNumberReset = $this->db->query($PinResetQuery);

        if ($deleteResult && $PinResetResult && $formNumberReset ) {
            $this->session->set_flashdata('success','Candidate was Reset Successfully');
            redirect('super/registration');
        }
        else {
            $this->session->set_flashdata('error','An Error Occured While resetting candidate \n Please try again');
            redirect('super/registration');
        }

    }

}
