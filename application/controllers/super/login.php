<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Super_Controller {

    function __construct() {
        parent::__construct();
    }
    
    public function index() {  
      $this->load->model('super/users_model');
      $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
      $this->form_validation->set_rules('password', 'Password', 'trim|required');
      
      if($this->form_validation->run() == TRUE){
            $user = $this->users_model->get_where(array('email' => $this->input->post('email'), 'password' => encrypt($this->input->post('password'))), TRUE);
            if(count($user)){
                if($user->role == 'platform'){
                    $this->session->set_userdata("super_loggedIn", true);
                    $this->session->set_userdata("super_data", $user);
                    redirect(site_url('super/dashboard'));
                }else{
                    $this->session->set_flashdata('error', 'Authentication Failed - Invalid User');
                    redirect(site_url('super/login'));
                }
            }
            else{
                //wrong login details 
                 $this->session->set_flashdata('error', 'Authentication Failed - Wrong Credentials Supplied.');
                 redirect(site_url('super/login'));
            }
      }
      $this->load->view('super/login_page', $this->data);
    }
    
    public function logout() {  
        $this->session->unset_userdata('super_loggedIn');
        $this->session->unset_userdata('super_data');
        redirect(site_url('super/login'));
    }
    
}
