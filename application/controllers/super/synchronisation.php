<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Synchronisation extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function check_available_update() {
        $edcid = $this->input->post('edcid');
        $sql = "SELECT * FROM t_edcs WHERE edcid = '" . $edcid . "' ";
        $query = $this->db->query($sql);
        if ($query->num_rows()) {
            echo json_encode($query->result_array());
        } else
            echo json_encode(array());

        return;
    }

    //CURL SERVER TO COLLECT UPLAODED FILE
    public function collect_synchronisation_file() {

        $edcid = $this->input->post('edcid');


        $synchronisation_file = $_FILES['data'];

        if (trim($synchronisation_file['name']) == false) {
            #if no file was received
            echo '0';
            return;
        }
        $new_name = uniqid() . '_' . $synchronisation_file['name'];

        if (!file_exists(config_item('synch_path') . 'online')) {
            mkdir(config_item('synch_path') . 'online');
        }
        $filePath = config_item('synch_path') . 'online/' . $new_name;


        if ($synchronisation_file["error"] == 0) {
            if (move_uploaded_file($synchronisation_file["tmp_name"], $filePath)) {
                //Send SMS to Administrartors and log Detail.
                $this->load->model('super/edc_model');
                $edc_data = $this->edc_model->get_all($edcid);

                #log the details
                $this->load->model('super/synchdetail_model');
                $data = array();
                $data['edcid'] = $edcid;
                $data['filename'] = $new_name;
                $data['filesize'] = $synchronisation_file["size"];
                $data['path'] = $filePath;
                $data['content'] = file_get_contents($filePath);
                $this->synchdetail_model->save_update($data);
                

                @unlink($filePath);

                //Notify by SMS
                $sms_settings = config_item('sms_settings');
                foreach ($sms_settings['receivers'] as $reciever) {
                    $http_url = '%s?cmd=sendquickmsg&owneremail=%s&subacct=%s&subacctpwd=%s&message=%s&sender=%s&sendto=%s&msgtype=0';
                    $http_url = sprintf($http_url, $sms_settings['host'], $sms_settings['owner'], $sms_settings['sub_acct'], $sms_settings['sub_acct_pass'], urlencode('SYNCHRONISATION REQUEST RECEIVED FROM ' . strtoupper($edc_data->edcname)), $sms_settings['sender'], $reciever
                    );

                    $ch = curl_init();

                    @curl_setopt($ch, CURLOPT_URL, $http_url);
                    @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $status_code = @curl_exec($ch);
                    @curl_close($ch);
                }

                echo '1';
                return;
            }
        }

      //  echo '0';
        return;
    }

    //CURL SERVER TO GIVE SYNCH FILE TO CLIENT
    public function get_synchronisation_file($edcid) {

        $this->load->model('super/edc_model');

        $filepath = './resources/db_sync/' . $edcid . '.edp';
        if (file_exists($filepath)) {

            //backup the synch file before sending it to user for download
            $server_backup_filepath = './resources/backup/' . $edcid . '_' . date("d-m-Y_Hs") . '.edp';
            copy($filepath, $server_backup_filepath);

            $this->load->helper('download');
            $data = file_get_contents($filepath); // Read the file's contents
            $name = $edcid . '_' . date("d-m-Y_Hs") . '.edp';

            //delete afterwards
            @unlink($filepath);
            force_download($name, $data);
            return;
        } else {
            show_error('NO DATA FOUND ONLINE YET');
            return;
        }
    }

}
