<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Activateoffline extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
         $this->load->model('super/activation_model');
    }
    
    public function index(){
               
       $this->data['activations'] = $this->activation_model->get_all();
       $this->data['subview'] = 'super/activation_page';
       $this->load->view('super/template/_layout_main', $this->data);
    }
    
    public function activate($activationid = null) {
        
        //$activationid || show_404();
                
        $this->load->model('super/edc_model');
        $this->load->model('super/users_model');
        $this->load->library('encrypt');

        $this->form_validation->set_rules('fullname', 'Fullname', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required|trim');
        $this->form_validation->set_rules('oldid', 'Old ID', 'trim');
        
        if($this->form_validation->run()){
            
            $this->load->model('info_model');
            
            $user_data = $this->edc_model->array_from_post(array('fullname', 'email', 'phone', 'oldid'));
            $activationid = $this->input->post('trial_id');
            $default_pass = config_item('default_edcadmin_pass');
            
            $activation_data = $this->activation_model->get_all($activationid, true);
            count($activation_data) || show_error("INVALID PARAMETER");
            if($activation_data->activated) redirect(site_url('super/activateoffline'));
        
            if(trim($activation_data->edcid) == false){ //if edcid is empty
                if(trim($this->input->post('oldid')) == false) $edcid = $this->edc_model->generate_unique_id();
                else $edcid = $this->input->post('oldid');

                $license = $this->encrypt->encode($activation_data->trial_id.'^'.$edcid);

                $data = array();
                $data['edcid'] = $edcid;
                $data['license'] = $license;
                $data['activated'] = 1;
                $this->activation_model->save_update($data, $activationid, $edcid);

                $data = array();
                $data['edcid'] = $edcid;
                $data['edcname'] = $activation_data->name;
                $data['edcweburl'] = $activation_data->url;
                $data['edcphone'] = $activation_data->phone;
                $data['edclogo'] = $activation_data->logo;
                $this->edc_model->delete($edcid);//to avoid duplicate in synchronisation
                $this->edc_model->save_update($data, null, $edcid);
                
                //save admin info too
                $user_data['edcid'] = $edcid;
                $user_data['role'] = 'Super_Administrator';
                $user_data['userid'] = $this->users_model->generate_unique_id();  //if its an insert, generated new categoryid
                $user_data['password'] = encrypt($default_pass);
                $this->users_model->delete($user_data['userid']);//to avoid duplicate in synchronisation
                $this->users_model->save_update($user_data, null, $edcid);
                
                //insert the default school info - about, news etc
                $data = array();
                $data['infoid'] = $this->info_model->generate_unique_id();
                $data['edcid'] = $edcid;
                $this->info_model->delete($data['infoid']);//to avoid duplicate in synchronisation
                $this->info_model->save_update($data, null, $edcid);
                
            }else{

                $license = $this->encrypt->encode($activation_data->trial_id.'^'.$activation_data->edcid); 
                $data = array();
                $data['license'] = $license;
                $data['activated'] = 1;
                $this->activation_model->save_update($data, $activationid, $activation_data->edcid);
                
                $this->db->where('edcid', $activation_data->edcid);
                $this->db->delete('t_users');
                logSql($this->db->last_query(), $activation_data->edcid);
                
                //save admin info too
                $user_data['edcid'] = $activation_data->edcid;
                $user_data['role'] = 'Super_Administrator';
                $user_data['userid'] = $this->users_model->generate_unique_id();  //if its an insert, generated new categoryid
                $user_data['password'] = encrypt($default_pass);
                $this->users_model->delete($user_data['userid']);//to avoid duplicate in synchronisation
                $this->users_model->save_update($user_data, null, $activation_data->edcid);
                
                //insert the default school info - about, news etc
                $this->db->where('edcid', $activation_data->edcid);
                $this->db->delete('t_edc_info');
                logSql($this->db->last_query(), $activation_data->edcid);
                
                $data = array();
                $data['infoid'] = $this->info_model->generate_unique_id();
                $data['edcid'] = $activation_data->edcid;; 
                $this->info_model->delete($data['infoid']);//to avoid duplicate in synchronisation
                $this->info_model->save_update($data, null, $activation_data->edcid);
            }


            $sms_settings = config_item('sms_settings');
            $http_url = '%s?cmd=sendquickmsg&owneremail=%s&subacct=%s&subacctpwd=%s&message=%s&sender=%s&sendto=%s&msgtype=0';

            $http_url = sprintf($http_url, 
                $sms_settings['host'],
                $sms_settings['owner'],
                $sms_settings['sub_acct'],
                $sms_settings['sub_acct_pass'],
                urlencode('YOUR ACTIVATION REQUEST HAS BEEN APPROVED - CONNECT ONLINE NOW TO PERFORM SYNCHRONISATION'),
                $sms_settings['sender'],
                $activation_data->phone
            );

            $ch = curl_init();

            @curl_setopt($ch, CURLOPT_URL, $http_url);
            @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $status_code = @curl_exec($ch);
            @curl_close($ch);


            $this->session->set_flashdata('msg', 'OFFLINE E_DEVPRO TRIAL SOFTWARE ACTIVATED SUCCESSFULLY');
            redirect(site_url('super/activateoffline')); 
            
        }else{
             $this->session->set_flashdata('error', validation_errors());
            redirect(site_url('super/activateoffline')); 
        }
        
    }
    
    public function deactivate($activationid) {
        
        $activationid || show_404();
        $activation_data = $this->activation_model->get_all($activationid, true);
        count($activation_data) || show_error("INVALID PARAMETER");
        
        if(!$activation_data->activated) redirect(site_url('super/activateoffline'));
        
        $data = array();
        $data['license'] = '';
        $data['activated'] = 0;
        $this->activation_model->save_update($data, $activationid, $activation_data->edcid);
        
        $this->session->set_flashdata('msg', 'OFFLINE E_DEVPRO SOFTWARE DEACTIVATED SUCCESSFULLY');
        redirect(site_url('super/activateoffline'));
    }
    
    public function delete($activationid) {
        
        $this->activation_model->delete($activationid);
        $this->session->set_flashdata('msg', 'DATA DELETED SUCCESSFULLY');
        redirect(site_url('super/activateoffline')); 
    }
}
