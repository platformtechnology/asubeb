<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Support extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/support_model');
        $this->load->model('super/support_comment_model');
    }
    
    public function index(){

        $this->data['support_data'] = $this->support_model->get_all();
                 
        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        
        
        $this->data['subview'] = 'super/support_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
    public function reply() {
        
        $this->form_validation->set_rules('comment', 'Reply', 'trim');
        
        if($this->form_validation->run()){
            $data['commentid'] = $this->support_comment_model->generate_unique_id();
            $data['comment'] = $this->input->post('comment');
            $data['requestid'] = $this->input->post('requestid');
            $data['edcid'] = $this->input->post('edcid');
            $data['owner'] = 'Admin';
            
            if(trim($data['comment']) != false) $this->support_comment_model->save_update($data);
            $this->support_model->save_update(array('status'=>$this->input->post('status')), $data['requestid']);
            
            $this->session->set_flashdata('msg', 'Reply Posted Successfully');
            redirect(site_url('super/support/requestdetails/'.$data['requestid']));
        }else{
            $this->session->set_flashdata('error', validation_errors());
            redirect(site_url('super/support/requestdetails/'.$data['requestid']));
        }
    }
    
     public function requestdetails($requestid) {
           
        $this->data['request_data'] = $this->support_model->get_all($requestid);
        count($this->data['request_data']) || redirect(site_url('super/support'));
        
        $this->data['subview'] = 'super/support_detail_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }
    
    public function delete($requestid) {
        $this->db->where('requestid', $requestid);
        $this->db->delete('t_support');
        $this->db->where('requestid', $requestid);
        $this->db->delete('t_support_comments');
        
        $this->session->set_flashdata('updated_msg', 'Request Deleted Successfully');
        redirect(site_url('super/support'));
    }
}
