<?php
session_start();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Registrants extends Super_Controller {
    //put your code here
    private $perpage = 3;
    
    function __construct() {
        parent::__construct();
        $this->load->model('super/registrant_model');
         
    }
    
    public function _getEdcData($edcid){
             
       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
      
       return $this->data['edcs']->edcid;
    }
        
    public function filter($edcid = NULL){
        
        $edcid || show_error("INVALID PARAM");
        $this->_getEdcData($edcid);
                
        $this->data['registrants'] = array();
        $this->_saveCandidatesInSession();
        
        //for displaying zones ddl
        $this->db->where('t_zones.edcid', $edcid);
        $this->db->order_by('zonename');
        $this->data['zones'] = $this->db->get('t_zones')->result();
        //for lga ddl
        $this->db->where('edcid', $edcid);
        $this->db->order_by('lganame');
        $this->data['lgas'] = $this->db->get('t_lgas')->result();
        //for exams ddl
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();
         
        
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        
        if($this->form_validation->run()){
            $data = $this->registrant_model->array_from_post(array('examid', 'examyear', 'zoneid', 'lgaid', 'schoolid'));

            //$joinTableNameKey = array('t_pins'=>'candidateid');
            ///$joinField = array('t_pins.pin');
            $this->_performFilter($data, $edcid);
            $this->db->like('t_candidates.zoneid', $data['zoneid']);
            $this->db->like('t_candidates.lgaid', $data['lgaid']);
            $this->db->like('t_candidates.schoolid', $data['schoolid']); 
            $this->data['registrants'] = $this->registrant_model->get_all();
            $this->data['count'] = count($this->data['registrants']);
            
            $this->_saveCandidatesInSession(); // save the resultset in session so that it can be exported to excel

            if(isset($data['lgaid'])){
                $this->db->where('edcid', $edcid);
                $this->db->where('lgaid', $data['lgaid']);
                //$this->db->where('ispostingschool', 0);
                $this->db->order_by('schoolname');
                $this->data['schools'] = $this->db->get('t_schools')->result();
            } 
        }
        
        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';

         //Add styles and scripts for the datetime
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        
        $this->data['subview'] = 'super/registrants_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }
    
    public function prosearch($edcid = NULL){
        
        $edcid || show_error("INVALID PARAM");
        $this->_getEdcData($edcid);
                
        $this->data['registrants'] = array();
        $this->_saveCandidatesInSession();
        
        //for displaying zones ddl
        $this->db->where('t_zones.edcid', $edcid);
         $this->db->order_by('zonename');
        $this->data['zones'] = $this->db->get('t_zones')->result();
        //for lga ddl
        $this->db->where('edcid', $edcid);
         $this->db->order_by('lganame');
        $this->data['lgas'] = $this->db->get('t_lgas')->result();
        //for exams ddl
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();
         
        
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        $this->form_validation->set_rules('examid', 'Exam Year', 'trim|required');
        $this->form_validation->set_rules('lgaid', 'Exam Year', 'trim|required');
        
        if($this->form_validation->run()){
            $data = $this->registrant_model->array_from_post(array('examid', 'examyear', 'lgaid', 'fromdate', 'fromtime'));

            $interval = $data['fromdate'] . " " . $data['fromtime'];
                    
            $sql = "SELECT t_candidates.* FROM t_candidates "
                    //. "INNER JOIN t_pins ON t_candidates.candidateid = t_pins.candidateid "
                    . "WHERE t_candidates.edcid = '" . $edcid . "' "
                    . "AND t_candidates.lgaid = '" . $data['lgaid'] . "' "
                    . "AND t_candidates.examid = '" . $data['examid'] . "' "
                    . "AND t_candidates.examyear = '" . $data['examyear'] . "' "
                    . "AND substring(to_char(t_candidates.datecreated, 'YYYY-MM-DD HH24:MI:SS') from 1 for 16) >= '" . $interval . "' ";
            $this->data['registrants'] = $this->db->query($sql)->result();
           
            $this->data['count'] = count($this->data['registrants']);
            
            $this->_saveCandidatesInSession(); // save the resultset in session so that it can be exported to excel

            if(isset($data['lgaid'])){
                $this->db->where('edcid', $edcid);
                $this->db->where('lgaid', $data['lgaid']);
                //$this->db->where('ispostingschool', 0);
                 $this->db->order_by('schoolname');
                $this->data['schools'] = $this->db->get('t_schools')->result();
            } 
        }
        
        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';

         //Add styles and scripts for the datetime
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        
        $this->data['subview'] = 'super/registrants_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }
    
    private function _performFilter($data, $edcid){
        if(empty($data['examyear']) && (!empty($data['examid']))){
            $this->db->where(array('t_candidates.edcid'=>$edcid, 't_candidates.examid'=>$data['examid']));
        }
        else if((!empty($data['examyear'])) && (!empty($data['examid']))){
            $this->db->where(array('t_candidates.examyear'=>$data['examyear'], 't_candidates.edcid'=>$edcid, 't_candidates.examid'=>$data['examid']));
        }
        else if((!empty($data['examyear'])) && empty($data['examid'])){
            $this->db->where(array('t_candidates.edcid'=>$edcid, 't_candidates.examyear'=>$data['examyear']));
        }else{
            $this->db->where(array('t_candidates.edcid'=> $edcid));               
        }
    }
    
    public function _saveCandidatesInSession(){
        //$this->session->set_userdata('platform_candidates', $this->data['registrants']);
        $_SESSION['platform_candidates'] = $this->data['registrants'];
    }
    
    public function export_to_excel(){
            $this->load->helper('excel');
            export_to_excel($_SESSION['platform_candidates'], 'Candidate_Report');
    }
    
    public function export_to_omr_format(){
            $this->load->helper('omr');
            //export_to_omr_format($this->session->userdata('platform_candidates'), 'Omr_Candidate_Report');
            export_to_omr_format($_SESSION['platform_candidates'], 'Omr_Candidate_Report');
    }
    
    public function delete($edcid, $candidateid) {
        $this->registrant_model->delete($candidateid, $edcid);
        $sql = "delete from t_registered_subjects where candidateid = '" . $candidateid . "'; "
                . "delete from t_scores where candidateid = '" . $candidateid . "'; ";
        $this->db->query($sql);
        logSql($sql, $edcid);
        $this->session->set_flashdata('msg', 'Candidate Deleted Successfully');
        redirect(site_url('super/registrants/filter/'.$edcid));
    }
    
    public function view($candidateid) {
        $this->data['reg_info'] = $this->registrant_model->get_all($candidateid);
        count($this->data['reg_info']) || show_404();
        if (count($this->data['reg_info'])) {
        //GET PIN INFO
            $this->db->where('candidateid',$this->data['reg_info']->candidateid);
            $this->data['candidate_pin_info'] = $this->db->get('t_pins')->row();
        }
        
        //Get Exam Detail that the candidate registered for
        $this->load->model('super/exam_model');
        $this->data['exam_info'] = $this->exam_model->get_all($this->data['reg_info']->examid);
                 
        $this->data['subjects'] = $this->db->get_where('t_registered_subjects', array('candidateid'=>$candidateid))->result();
        
        $this->data['edcs'] = $this->db->get_where('t_edcs', array('edcid'=>$this->data['reg_info']->edcid))->row();
        count($this->data['edcs']) || show_404();
        $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
       
        $edc_url = $this->data['edcs']->edcweburl;
        $this->data['passport'] = $edc_url.'/passports/'.$candidateid.'.jpg';
        
        //Add styles for the passport display
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';
        //$this->data['page_level_scripts'] = '<script src="' . base_url('resources/js/bootstrap-fileupload.js') . '"></script>';
        
        $this->data['subview'] = 'super/registrants_view_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }
    
    public function printcandidate($candidateid) {
        $this->data['reg_info'] = $this->registrant_model->get_all($candidateid);
        count($this->data['reg_info']) || show_404();
        
        //Get Exam Detail that the candidate registered for
        $this->load->model('super/exam_model');
        $this->data['exam_info'] = $this->exam_model->get_all($this->data['reg_info']->examid);
                 
        $this->data['subjects'] = $this->db->get_where('t_registered_subjects', array('candidateid'=>$candidateid))->result();
        
        $this->data['edcs'] = $this->db->get_where('t_edcs', array('edcid'=>$this->data['reg_info']->edcid))->row();
        count($this->data['edcs']) || show_404();
        $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
       
        $edc_url = $this->data['edcs']->edcweburl;
        $this->data['passport'] = $edc_url.'/passports/'.$candidateid.'.jpg';
        
        //Add styles for the passport display
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';
        //$this->data['page_level_scripts'] = '<script src="' . base_url('resources/js/bootstrap-fileupload.js') . '"></script>';
        
        $this->load->view('super/registrants_print_page', $this->data);
    }
    
    public function printdetails($edcid) {
         $edcid || show_error("INVALID PARAM");
        $this->data['printdata'] = $_SESSION['platform_candidates']; 
        $data = $this->data['printdata'][0];
        $this->data['exam_subjects'] = $this->db
                    ->where('examid', $data->examid)
                    ->where('examyear',  $data->examyear)
                    //->where('haspractical', 1)
                    ->where('edcid', $edcid)
                    ->order_by('priority', 'asc')
                    ->get('t_subjects')->result();
       
        $this->_getEdcData($edcid);

        $this->load->view('super/registrants_print_details_page', $this->data);
    
    }
    
    public function school_ajaxdrop(){
        if($this->_is_ajax()){
            $lgaid = $this->input->get('lgaid', TRUE);
            $zoneid = $this->input->get('zoneid', TRUE);
            
            $this->db->select('schoolid as lgaid, schoolname as lganame');
            if(trim($lgaid) == false && trim($zoneid) != false) {
                $this->db->where('zoneid', $zoneid);
            }else{
                $this->db->where('lgaid', $lgaid);
                $this->db->where('lgaid !=', '');
            }
             $this->db->order_by('schoolname');
            $query = $this->db->get('t_schools');
            
            
            $data['lgas'] = array();
            if ($query->num_rows() > 0)
            {
              $data['lgas'] = $query->result_array();
            }            
            echo json_encode($data);
            //return $data;
        }else{
            echo json_encode(array());
        }
    }

    function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }
}
