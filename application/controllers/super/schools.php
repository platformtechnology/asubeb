<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Schools extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/schools_model');
    }

    public function save($edcid, $schoolid = null){

        //for displaying all schools
//        $jointable = array('t_lgas'=>'lgaid');
//        $joinfields = array('t_lgas.lganame', 't_lgas.zoneid');
        $this->db->where('t_schools.edcid', $edcid);
        $this->db->where('t_schools.isprimary', 1);
        $this->db->order_by('t_schools.schoolname');
        $this->data['schools'] = $this->schools_model->get_all();

        $this->_getEdcData($edcid);

        //for displaying all lga ddl
        $this->db->where('edcid', $edcid);
        $this->db->order_by('lganame');
        $this->data['lgs'] = $this->db->get('t_lgas')->result();

        if($schoolid == null)$this->data['school_detail'] = $this->schools_model->is_new();
        else $this->data['school_detail'] = $this->schools_model->get_all($schoolid);

        $validation_rules = $this->schools_model->_rules;
        $this->form_validation->set_rules($validation_rules);

        if($this->form_validation->run() == TRUE){

           //get the posted values
            $data = $this->schools_model->array_from_post(array('schoolname', 'schoolcode', 'lgaid', 'zoneid', 'iscentre'));
            $data['zoneid'] = $this->_validateData($data, $edcid);

            if(trim($data['iscentre']) == false) $data['iscentre'] = 0;

            //if its an insert, generated new categoryid
            if($schoolid == NULL){
                $data['schoolid'] = $this->schools_model->generate_unique_id();
                $this->schools_model->delete($data['schoolid'], $edcid);
            }

            $data['issecondary'] = 0;
            $data['isprimary'] = 1; //it is a primaryschool being setup
            $data['edcid'] = $edcid;

           //validate if the schoolname is unique during update
           $this->_validateSchool($data, $schoolid);

            $this->schools_model->save_update($data, $schoolid, $edcid);
            $this->session->set_flashdata('msg', $schoolid == NULL ? 'New School Added Successfully' : 'School Updated Successfully') ;
            redirect(site_url('super/schools/save/'.$edcid));
        }


         //for displaying zones ddl
        $this->db->where('t_zones.edcid', $edcid);
        $this->db->order_by('t_zones.zonename');
        $this->data['zones'] = $this->db->get('t_zones')->result();

         //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';

        $this->data['subview'] = 'super/schools_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }

    public function _validateData($data, $edcid) {
        if($data['lgaid'] == 'undefined'){
            $this->session->set_flashdata('error', 'LGA Not Selected') ;
            redirect(site_url('super/schools/save/'.$edcid));
        }

        if(trim($data['lgaid']) == false && trim($data['zoneid']) == false){
             $this->session->set_flashdata('error', 'You Must Select Either Lga or Zone or both') ;
             redirect(site_url('super/schools/save/'.$edcid));
        }

        if(trim($data['lgaid']) != false && trim($data['zoneid']) == false){
            //if lga is selected and zone is not selected,
            //then fetch the zoneid from the selected lga
            // - remember all lga are under a zone

            $this->db->where('edcid', $edcid);
            $this->db->where('lgaid', $data['lgaid']);
            $this->db->order_by('lganame');
            $lgadata = $this->db->get('t_lgas')->row();

            return $lgadata->zoneid;
        }

        return $data['zoneid'];
    }

    public function delete($edcid, $schoolid){
        $this->schools_model->delete($schoolid, $edcid);

        $this->db->where('schoolid', $schoolid);
        $this->db->delete('t_candidates');
        logSql($this->db->last_query(), $edcid);

        $this->session->set_flashdata('msg', 'School Data Deleted Successfully');
        redirect(site_url('super/schools/save/'.$edcid));
    }

    public function _getEdcData($edcid){

       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);

       return $this->data['edcs']->edcid;
    }

    public function _validateSchool($data, $schoolid){
        if($schoolid == null){
            $result = $this->schools_model->get_where(array('schoolname' => $data['schoolname'], 'lgaid' => $data['lgaid'], 'isprimary' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Already Exists In Selected LGA!');
                  redirect(site_url('super/schools/save/'.$this->data['edcs']->edcid.'/'.$schoolid));
            }

            $result = $this->schools_model->get_where(array('schoolcode' => $data['schoolcode'], 'lgaid' => $data['lgaid'], 'isprimary' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Code Already Exists In Selected LGA! - School Code Must Be Unique For Each LGA');
                  redirect(site_url('super/schools/save/'.$this->data['edcs']->edcid.'/'.$schoolid));
            }
        }else{
            $this->db->where_not_in('schoolid', $schoolid);
            $result = $this->schools_model->get_where(array('schoolname' => $data['schoolname'], 'lgaid' => $data['lgaid'], 'isprimary' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Already Exists In Selected LGA!');
                  redirect(site_url('super/schools/save/'.$this->data['edcs']->edcid.'/'.$schoolid));
            }

             $this->db->where_not_in('schoolid', $schoolid);
             $result = $this->schools_model->get_where(array('schoolcode' => $data['schoolcode'], 'lgaid' => $data['lgaid'], 'isprimary' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Code Already Exists In Selected LGA! - School Code Must Be Unique For Each LGA');
                  redirect(site_url('super/schools/save/'.$this->data['edcs']->edcid.'/'.$schoolid));
            }
        }
    }

    public function ajaxdrop(){
        if($this->_is_ajax()){

            $zoneid = $this->input->get('zoneid', TRUE);
            $edcid = $this->input->get('edcid', TRUE);

            $data = array();

            //Get Lga Per Zone
            $data['lgas'] = $this->schools_model->get_lgas_from_zone($zoneid);

            //Get Schools Per Zone for displaying second dropdown
            $this->db->select('schoolid as lgaid, schoolname as lganame');
            $this->db->where('edcid', $edcid);
            $this->db->where('zoneid', $zoneid);
            $this->db->order_by('schoolname');
            $data['schools'] = $this->db->get('t_schools')->result();

            if(count($data['lgas'])){
                echo json_encode($data);
            }else{
                $this->db->where('edcid', $edcid);
                $data['lgas'] = $this->db->get('t_lgas')->result();
                echo json_encode($data);
            }

        }else{
            echo json_encode(array());
        }
    }

    function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

    public function printPrimSchool($edcid) {
        $this->_getEdcData($edcid);

//        $joinTableKey = array('t_lgas'=>'lgaid');
//        $joinFields = array('t_lgas.lganame');
        $this->db->join('t_lgas','t_schools.lgaid = t_lgas.lgaid','inner');
        $this->db->join('t_zones','t_lgas.zoneid = t_zones.zoneid','left');
        $this->db->where('t_schools.isprimary', 1);
        $this->db->where('t_schools.edcid', $edcid);
        $this->db->order_by('t_lgas.lganame ASC, t_schools.schoolname ASC');
//        $school_data = $this->schools_model->get_join($joinTableKey, $joinFields);
        $school_data = $this->schools_model->get_all();


        $table_data = '<table class="table table-bordered">';
        $table_data .= '<thead>';
            $table_data .= '<tr>';
                $table_data .= '<th>Sn</th>';
                $table_data .= '<th>School Name</th>';
                $table_data .= '<th>School Code</th>';
                $table_data .= '<th>Zone</th>';
                $table_data .= '<th>Local Government Area</th>';
            $table_data .= '</tr>';
        $table_data .= '</thead>';

        $table_data .= '<tbody>';
            if(count($school_data)){
                $sn=0;
                foreach ($school_data as $value) {
                    $table_data .= '<tr>';
                        $table_data .= '<td>'.++$sn.'</td>';
                        $table_data .= '<td>'.$value->schoolname.'</td>';
                        $table_data .= '<td>'.$value->schoolcode.'</td>';
                        $table_data .= '<td>'.$value->zonename.'</td>';
                        $table_data .= '<td>'.$value->lganame.'</td>';

                    $table_data .= '</tr>';
                }
            }
        $table_data .= '</tbody>';
        $table_data .= '</table>';

        $this->data['report'] = $table_data;
        $this->data['url'] = site_url('super/schools/save/'.$edcid);
        $this->data['title'] = 'PRIMARY SCHOOL LIST REPORT';

        $this->load->view('super/print_page', $this->data);
    }
    
    
    public function printPrimSchoolsWithCandidates($edcid) {
        $this->_getEdcData($edcid);

//        $joinTableKey = array('t_lgas'=>'lgaid');
//        $joinFields = array('t_lgas.lganame');
        $this->db->join('t_lgas','t_schools.lgaid = t_lgas.lgaid','inner');
        $this->db->join('t_zones','t_lgas.zoneid = t_zones.zoneid','left');
        $this->db->join('t_candidates','t_candidates.schoolid = t_schools.schoolid','inner');
        $this->db->where('t_schools.isprimary', 1);
        $this->db->where('t_schools.edcid', $edcid);
        $this->db->order_by('t_lgas.lganame ASC, t_schools.schoolname ASC');
//        $school_data = $this->schools_model->get_join($joinTableKey, $joinFields);
        $school_data = $this->schools_model->get_all();
        echo var_dump($school_data);
        exit;


        $table_data = '<table class="table table-bordered">';
        $table_data .= '<thead>';
            $table_data .= '<tr>';
                $table_data .= '<th>Sn</th>';
                $table_data .= '<th>School Name</th>';
                $table_data .= '<th>School Code</th>';
                $table_data .= '<th>Zone</th>';
                $table_data .= '<th>Local Government Area</th>';
            $table_data .= '</tr>';
        $table_data .= '</thead>';

        $table_data .= '<tbody>';
            if(count($school_data)){
                $sn=0;
                foreach ($school_data as $value) {
                    $table_data .= '<tr>';
                        $table_data .= '<td>'.++$sn.'</td>';
                        $table_data .= '<td>'.$value->schoolname.'</td>';
                        $table_data .= '<td>'.$value->schoolcode.'</td>';
                        $table_data .= '<td>'.$value->zonename.'</td>';
                        $table_data .= '<td>'.$value->lganame.'</td>';

                    $table_data .= '</tr>';
                }
            }
        $table_data .= '</tbody>';
        $table_data .= '</table>';

        $this->data['report'] = $table_data;
        $this->data['url'] = site_url('super/schools/save/'.$edcid);
        $this->data['title'] = 'PRIMARY SCHOOL LIST REPORT';

        $this->load->view('super/print_page', $this->data);
    }





    function fetchLastSchoolCode($lgaid) {
        $this->db->select('max(schoolcode) as maxcode');
        $this->db->where('lgaid',$lgaid);
        $this->db->where('isprimary',1);
        $maxSchoolCodeData = $this->db->get('t_schools')->result();
        foreach ($maxSchoolCodeData as $maxSchoolData) {
            $maxSchoolCode = $maxSchoolData->maxcode;
        }
        echo sprintf('%04d',$maxSchoolCode + 1);
    }
}
