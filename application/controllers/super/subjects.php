<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of subjects
 *
 * @author Maxwell
 */
class Subjects extends Super_Controller{
   
    function __construct() {
        parent::__construct();
        $this->load->model('super/subjects_model');
        $this->load->model('super/edc_model');
        
    }
    
    public function index($id = null){
        $id || show_404();
                
        $this->data['subject_detail'] = $this->subjects_model->is_new();
        
        $this->_getEdcData($id);
        
        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        
        $this->data['subview'] = 'super/subjects_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }
    
    public function correct(){
        $this->load->model('super/scores_margin_model');
        $subjects = $this->subjects_model->get_all();
        foreach ($subjects as $subject) {
          $this->scores_margin_model->save_update(array('subjectid'=>$subject->subjectid, 'edcid'=>$subject->edcid), null);  
        }
    }

    public function save($id, $subjectid = null){
        $edcid = $this->_getEdcData($id);
        
        $validation_rules = $this->subjects_model->_rules;
        if($subjectid == null){
            $this->data['subject_detail'] = $this->subjects_model->is_new();
        }else $this->data['subject_detail'] = $this->subjects_model->get_all($subjectid); 
        
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run() == TRUE){
            $data = $this->subjects_model->array_from_post(array('subjectname', 'priority', 'iscompulsory', 'haspractical', 'examid'));
            if(empty($data['iscompulsory'])) $data['iscompulsory'] = 0;
            if(empty($data['haspractical'])) $data['haspractical'] = 0;
            $data['edcid'] = $edcid;
            
            if($subjectid == null){
                $data['subjectid'] = $this->subjects_model->generate_unique_id();
                $this->subjects_model->delete($data['subjectid'], $edcid);
            }
            
            $this->_validSubject($data, $subjectid);
            
            $this->subjects_model->save_update($data, $subjectid, $edcid);
            
            if($subjectid == null){ //Insert Default Maximum Scores for ca, exam and practical
                $this->load->model('super/scores_margin_model');
                $this->scores_margin_model->save_update(array('subjectid'=>$data['subjectid'], 'edcid'=>$edcid), null, $edcid);
            }
          
            
            $this->session->set_flashdata('msg', $subjectid == NULL ? 'New Subject Added Successfully' : 'Subject Updated Successfully') ;
            redirect(site_url('super/subjects/save/'.$this->data['edcs']->id));
        }
        
        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        
        $this->data['subview'] = 'super/subjects_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }
    
    public function delete($id, $subjectid){
       $edcid = $this->_getEdcData($id);
       $this->subjects_model->delete($subjectid, $edcid);

       $this->db->where('subjectid', $subjectid);
       $this->db->delete('t_scores');
       logSql($this->db->last_query(), $edcid);
       
       $this->db->where('subjectid', $subjectid);
       $this->db->delete('t_subjects_arms');
       logSql($this->db->last_query(), $edcid);
       
       $this->session->set_flashdata('msg', 'Subject Data Deleted Successfully');
       redirect(site_url('super/subjects/save/'.$id));      
    }
    
    public function _getallSubjects($edcid){
        $this->db
                ->select('t_subjects.subjectid, t_subjects.edcid, t_subjects.examid, t_subjects.priority, t_subjects.subjectname, t_subjects.iscompulsory, t_subjects.haspractical, t_subjects.examyear, t_edcs.id')
                ->from('t_subjects')
                ->where('t_subjects.edcid', $edcid)
                ->order_by('t_subjects.examid')
                ->join('t_edcs', 't_edcs.edcid = t_subjects.edcid');  
        $result = $this->db->get()->result();
        return $result;
    }
    
    public function _getEdcData($id){
       $this->load->model('super/exam_model');
      
       $this->db->where('id', $id);
       $this->data['edcs'] = $this->edc_model->get_all(NULL, TRUE);
       count($this->data['edcs']) || show_404();
       
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
       
       // get all subjects for a particular edc and display in table 
       $this->data['all_subjects'] = $this->_getallSubjects($this->data['edcs']->edcid);
       
        $this->data['exams'] = $this->exam_model->get_where(array('edcid'=>$this->data['edcs']->edcid));
        return $this->data['edcs']->edcid;
    }
    
    public function _validSubject($data, $subjectid){
         $this->db->where('edcid', $data['edcid']);
         if($subjectid == null){
            $result = $this->subjects_model->get_where(array('subjectname' => $data['subjectname'], 'examid' => $data['examid']), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Subject, ' . $data['subjectname'] . ', Already Exists In Selected Exam and Year!');
                  redirect(site_url('super/subjects/save/'.$this->data['edcs']->id.'/'.$subjectid));
            }
        }else{
            $this->db->where_not_in('subjectid', $subjectid);
            $result = $this->subjects_model->get_where(array('subjectname' => $data['subjectname'], 'examid' => $data['examid']), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Subject, ' . $data['subjectname'] . ', Already Exists In Selected Exam and Year!');
                  redirect(site_url('super/subjects/save/'.$this->data['edcs']->id.'/'.$subjectid));
            }
        }
    }
    
    public function search($id){
        $this->data['search_subjects'] = '';
        $edcid =  $edcid = $this->_getEdcData($id);
        
        $this->form_validation->set_rules('examid', 'Examination Type', 'trim|required');
        //$this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if($this->form_validation->run() == TRUE){
            $data = $this->subjects_model->array_from_post(array('examid'));
           $this->data['search_subjects'] = $this->subjects_model->get_where(array('edcid'=>$edcid, 'examid'=>$data['examid']));
           
           $this->data['examid'] = $data['examid'];
        }
        
         //$this->data['examid'] => 
                
         $this->load->model('super/exam_model');
         $this->data['exams'] = $this->exam_model->get_where(array('edcid'=>$edcid));
         
         //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        
        $this->data['subview'] = 'super/subjects_search_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }
    
}
