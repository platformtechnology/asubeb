<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Lgas extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/lgas_model');
    }
    
    public function _validateLga($edcid, $data){
        $result = $this->lgas_model->get_where(array('lganame' => $data['lganame'], 'edcid' => $edcid), TRUE);
        if(count($result)>0){
             $this->session->set_flashdata('error', 'The Lga Name Already Exists - Lga Name must be Unique!');
              redirect(site_url('super/lgas/save/'.$edcid));
        }
        
        $result = $this->lgas_model->get_where(array('lgainitials' => $data['lgainitials'], 'edcid' => $edcid), TRUE);
        if(count($result)>0){
             $this->session->set_flashdata('error', 'The Lga Initials Already Exists - Lga Initials must be Unique!');
              redirect(site_url('super/lgas/save/'.$edcid));
        }
    }
    
    public function save($edcid, $lgaid = null){
        $validation_rules = $this->lgas_model->_rules;
        
        if($lgaid == null){
            $this->data['lga_detail'] = $this->lgas_model->is_new();
            //$validation_rules['lga']['rules'] .= '|is_unique[t_lgas.lganame]';
        }else $this->data['lga_detail'] = $this->lgas_model->get_all($lgaid); 
        
        $this->form_validation->set_rules($validation_rules);
        
        if($this->form_validation->run() == TRUE){
          
           //get the posted values
            $data = $this->lgas_model->array_from_post(array('lganame', 'lgainitials', 'zoneid'));
            if($lgaid == NULL){
                $this->_validateLga($edcid, $data);
                $data['lgaid'] = $this->lgas_model->generate_unique_id();  //if its an insert, generated new id
                $this->lgas_model->delete($data['lgaid'], $edcid); //to prevent suynchronisation duplication
            }
            $data['edcid'] = $edcid;
            
           //validate if the lganame is unique during update
           $this->_validateUpdateData($edcid, $lgaid, $data);
            
            $this->lgas_model->save_update($data, $lgaid, $edcid);
            $this->session->set_flashdata('msg', $lgaid == NULL ? 'New LGA Added Successfully' : 'LGA Updated Successfully') ;
            redirect(site_url('super/lgas/save/'.$edcid));
        }  
        
        //for displaying all lgas
        $jointable = array('t_zones'=>'zoneid');
        $joinfields = array('zonename');
        $this->db->where('t_lgas.edcid', $edcid);
        $this->db->order_by('t_lgas.lganame');
        $this->data['lgas'] = $this->lgas_model->get_join($jointable, $joinfields);
         
         //for displaying zones ddl
        $this->db->where('t_zones.edcid', $edcid);
        $this->db->order_by('t_zones.zonename');
        $this->data['zones'] = $this->db->get('t_zones')->result();
        
        $this->_getEdcData($edcid);
        $this->data['subview'] = 'super/lga_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
    public function delete($edcid, $lgaid){

       $this->lgas_model->delete($lgaid, $edcid);
       
       $this->db->where('lgaid', $lgaid);
       $this->db->delete('t_schools');
       logSql($this->db->last_query(), $edcid);
       
        $this->db->where('lgaid', $lgaid);
        $this->db->delete('t_candidates');
        logSql($this->db->last_query(), $edcid);
        
       $this->session->set_flashdata('msg', 'LGA Data Deleted Successfully');
        redirect(site_url('super/lgas/save/'.$edcid));    
    }
    
     public function _getEdcData($edcid){
             
       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
      
       return $this->data['edcs']->edcid;
    }
    
    public function _validateUpdateData($edcid, $lgaid, $data){
         if($lgaid != null){
            $this->db->where_not_in('lgaid', $lgaid);
            $result = $this->db->get_where('t_lgas', array('lganame' => $data['lganame'], 'edcid' => $edcid))->row();
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Lga Name Already Exists');
                 redirect(site_url('super/lgas/save/'.$edcid.'/'.$lgaid));
            }

            $this->db->where_not_in('lgaid', $lgaid);
            $result = $this->db->get_where('t_lgas', array('lgainitials' => $data['lgainitials'], 'edcid' => $edcid))->row();
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Lga Initials Already Exists');
                 redirect(site_url('super/lgas/save/'.$edcid.'/'.$lgaid));
            }
        }
    }
    
}
