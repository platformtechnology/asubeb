<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Password extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/users_model');
    }
    
    public function change(){
        
       $this->form_validation->set_rules('oldpassword', 'Old Password', 'required|trim');
        $this->form_validation->set_rules('newpassword', 'New Password', 'required|trim');
        $this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'required|matches[newpassword]');
       
        if($this->form_validation->run() == TRUE){
           $data = $this->users_model->array_from_post(array('oldpassword','newpassword'));  
           if($data['oldpassword'] == $data['newpassword']){
                $this->session->set_flashdata('error', '<strong>Failure!</strong> - Old password cannot be same with the New one!');
               redirect(site_url('super/password/change'));
           }
           
           $validate = $this->users_model->get_where(array('email'=>$this->session->userdata('super_data')->email, 'password'=>  encrypt($data['oldpassword'])));
           if(!count($validate)){
               $this->session->set_flashdata('error', '<strong>Authentication Failure!</strong> - Verify your current password!');
               redirect(site_url('super/password/change'));
           }
           else{
              $this->users_model->save_update(array('password'=>encrypt($data['newpassword'])), $this->session->userdata('super_data')->userid) ;
              $this->session->set_flashdata('msg', '<strong>Password </strong> successfully changed!');
               redirect(site_url('super/password/change'));
           }
        }           
        $this->data['subview'] = 'super/password_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
}
