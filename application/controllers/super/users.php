<?php

class Users extends Super_Controller{

    function __construct() {
        parent::__construct();
        $this->load->model('super/users_model');
    }

    function index () {
        $this->data['users'] = $this->users_model->get_all();
        $this->data['error_message'] = '';
        $rules = $this->users_model->_rules;
        $this->form_validation->set_rules($rules);#
  
        if ($this->form_validation->run()== TRUE){
            
            $data = $this->users_model->array_from_post('email','password','fullname','phone');
            echo $data;
        }
        else{
            $this->data['error_message'] =  (strlen(trim(validation_errors())) > 0) ? get_error(validation_errors()) : '';
        }
        $this->data['subview'] = 'super/users_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }

}