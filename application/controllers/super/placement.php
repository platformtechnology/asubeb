<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Placement extends Super_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('super/registrant_model');
    }
    
     public function _getEdcData($edcid){
             
       $this->db->where('edcid', $edcid);
       $this->data['edcs'] = $this->db->get('t_edcs')->row();
       count($this->data['edcs']) || show_404();
       $this->data['edclogo'] = get_img('edc_logos/'.$this->data['edcs']->edclogo);
      
       return $this->data['edcs']->edcid;
    }
    
    public function fetch($edcid = NULL) {  
      
       $edcid || show_404();
      
       $this->_getEdcData($edcid);
       $this->db->where('edcid', $edcid);
       $this->db->order_by('lganame');
       $this->data['lgas'] = $this->db->get('t_lgas')->result();
        
       $this->db->where('edcid', $edcid);
       $this->db->where('isprimary', 1);
       $this->db->order_by('schoolname');
       $this->data['schools'] = $this->db->get('t_schools')->result();
       //$this->data['schools'] = array();
               
       $this->db->where('edcid', $edcid);
       $this->db->where('hasposting', 1);
       $this->data['exams'] = $this->db->get('t_exams')->result();
       
       $this->data['candidates'] = array();
                
       //$this->form_validation->set_rules('lgaid', 'Local Govt Area', 'trim|required');
       $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');

       if($this->form_validation->run() == TRUE){
           
           $data = $this->registrant_model->array_from_post(array('schoolid', 'examyear', 'examid'));

           $param = "AND schoolid = '".$data['schoolid']."' ";
           if(empty($data['schoolid']) || (trim($data['schoolid']) == false)){
               $param = "";
           }
           
           //$examid_param = $this->convert_2dimArray_to1dimArray($queryResult);
           $query = "SELECT * FROM t_candidates WHERE examyear = '".$data['examyear']."' "
                   . "AND examid = '" . $data['examid'] . "' "
                   . "AND edcid = '" . $edcid . "' ";
           
           $query .= $param;
           $query .= "ORDER BY examno";
           
           $this->data['candidates'] = $this->db->query($query)->result();
           
           $this->db->where('ispostingschool', 1);
           $this->db->where('edcid', $edcid);
           $this->db->order_by('schoolname');
           $this->data['choiceschool'] = $this->db->get('t_schools')->result();
       }
      
      //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';

        $this->data['subview'] = 'super/placement_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
    
    public function updateplacement() {
        if($this->_is_ajax()){
             $candidateid = $this->input->get('candidateid', TRUE);
             $data['placement'] = $this->input->get('placement', TRUE);
             
             $this->registrant_model->save_update($data, $candidateid);
             $success_data['success'] = 1;
             echo json_encode($success_data);
          }else{
              $success_data['success'] = 0;
              echo json_encode($success_data);
         }
    }
    
   public function school_ajaxdrop()
    {
        if($this->_is_ajax()){
            $lgaid = $this->input->get('lgaid', TRUE);
            
            $this->db->select('schoolid as lgaid, schoolname as lganame');
            $this->db->where('lgaid', $lgaid);
            $this->db->where('ispostingschool', 0);
            $this->db->order_by('schoolname');
            $query = $this->db->get('t_schools');
            
            $data['lgas'] = array();
            if ($query->num_rows() > 0)
            {
              $data['lgas'] = $query->result_array();
            }            
            echo json_encode($data);
            //return $data;
        }else{
            echo json_encode(array());
        }
    }

    function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }
}
