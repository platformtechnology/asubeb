<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Schooladmin extends Super_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/schools_model');
    }
    
    public function index(){
        
        $sql = "select t_schools.*, t_lgas.lganame, t_edcs.edcname from t_schools, t_lgas, t_edcs 
                where t_schools.lgaid = t_lgas.lgaid
                and t_edcs.edcid = t_schools.edcid
                and t_schools.phone is not null 
                and t_schools.phone <> '' 
                ";
        $data = $this->db->query($sql)->result();
        $this->data['schooladmins'] = $data;
                 
        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        
        
        $this->data['subview'] = 'super/schooladmin_page';        
        $this->load->view('super/template/_layout_main', $this->data); 
    }
}
