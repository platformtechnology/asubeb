<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Pinmanager extends Super_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('super/pin_model');
    }

    public function index() {

        $this->data['edcs'] = $this->db->get('t_edcs')->result();
        $this->data['subview'] = 'super/pinupload_page';
        $this->load->view('super/template/_layout_main', $this->data);
    }

    public function uploadpin() {

        $this->form_validation->set_rules('edcid', 'Edc', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Year', 'trim|required');

        if ($this->form_validation->run()) {
            $edcid = $this->input->post('edcid');
            $examyear = $this->input->post('examyear');

            #perform upload
            $upload_data = $this->_do_upload();

            #get existing pins
            $this->db->select('pin');
            $this->db->where('edcid', $edcid);
            $this->db->where('examyear', $examyear);
            $pins_data = $this->db->get('t_pins')->result_array();

            #convert csv to 2 dimension array
            $array_fromCSV = $this->_get2DArrayFromCsv($upload_data['full_path']);

            #get the pins and their serials into a 1 dimensional array
            $newPins = $this->_getOneDimArray($array_fromCSV);
            $newSerials = $this->_getOneDimArray($array_fromCSV, false, true);

            $duplicateData = null;

            #filter duplicates
            if (count($pins_data)) {
                $existingPins = $this->_getOneDimArray($pins_data, 'pin');

                #remove duplicate from uploaded pins
                $duplicateData = array_intersect($newPins, $existingPins);
                $newPins = $uniqueData = array_diff($newPins, $duplicateData);
            }

            #verify that not all pins are duplicates
            if (!count($newPins)) {
                #delete the uploaded file
                @unlink($upload_data['full_path']);
                $this->session->set_flashdata('error', 'Duplicate File Uploaded - All pins already exist!');
                redirect(site_url('super/pinmanager'));
            }

            #build sql query insert data
            $data = array();
            foreach ($newPins as $key => $pin) {
                $data[] = array(
                    'edcid' => $edcid,
                    'examyear' => $examyear,
                    'pin' => $pin,
                    'serial' => $newSerials[$key],
                    'datecreated' => date('Y-m-d H:i:s'),
                    'datemodified' => date('Y-m-d H:i:s')
                );
            }

            #perform a batch insert of the built data
            $this->db->insert_batch('t_pins', $data, false);

            #enter log details
            $log_data = array();
            $log_data['edcid'] = $edcid;
            $log_data['year'] = $examyear;
            $log_data['volume'] = count($data);
            $log_data['date'] = date('Y-m-d H:i:s');
            $this->db->set($log_data);
            $this->db->insert('t_pins_log');
             
            #delete the uploaded file
            @unlink($upload_data['full_path']);

            #inform user of the duplicates and the successfull upload
            $this->_download_successfull_pins($data);

            $this->session->set_flashdata('msg', count($newPins) . ' Pins Uploaded Successfully');
            redirect(site_url('super/pinmanager'));
            
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect(site_url('super/pinmanager'));
        }
    }

    public function _do_upload() {

        if (!file_exists($path = './resources/uploads/pins/')){
            mkdir($path, '0777', true);
        }

        $config = array(
            'upload_path' => $path,
            'allowed_types' => 'txt',
            'overwrite' => FALSE,
            'remove_spaces' => TRUE
        );
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('pinfile')) {
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect(site_url('super/pinmanager'));
        }

        return $this->upload->data();
    }

    public function _get2DArrayFromCsv($file, $delimiter = ',') {
        if (($handle = fopen($file, "r")) !== FALSE) {
            $i = 0;
            while (($lineArray = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                for ($j = 0; $j < count($lineArray); $j++) {
                    $data2DArray[$i][$j] = $lineArray[$j];
                }
                $i++;
            }
            fclose($handle);
        }
        return $data2DArray;
    }

    public function _getOneDimArray($param, $pin = false, $serial = false) {
        $data = array();
        foreach ($param as $index => $pinVal) {
            $data[] = $pinVal[$pin ? $pin : ($serial ? 1 : 0)];
        }

        return $data;
    }

    public function _download_successfull_pins($data) {

        $str = '';
        foreach ($data as $key => $arrayval) {
            $str .= $arrayval['pin'] . ',' . $arrayval['serial'] . "\n";
        }

        $this->load->helper('download');
        force_download('uploaded_pins.txt', $str);
    }

}
