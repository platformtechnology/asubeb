<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grading_Practical extends Admin_Controller {
    
    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Grading', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
        $this->load->model('admin/registration_model');
        $this->load->model('admin/subjects_model');
        $this->load->model('admin/reg_subjects_model');
        $this->load->model('admin/scores_model');
        $this->load->model('admin/scores_margin_model');
    }
    
    public function index() { 
          $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        
        $edcid = $this->data['edc_detail']->edcid;
        
        $this->db->where('edcid', $edcid);
        $this->db->order_by('schoolname');
        $this->data['schools'] = $this->db->get('t_schools')->result();
        
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();
        
        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('schoolid', 'School', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        $this->form_validation->set_rules('subjectid', 'Subject', 'trim');
        
        if($this->form_validation->run()){
            //$data = $this->registration_model->array_from_post(array('examid', 'examyear', 'lgaid', 'schoolid'));
            $data = $this->registration_model->array_from_post(array('examid', 'examyear', 'schoolid', 'subjectid'));
            
            $this->load->model('admin/exam_model');
            $this->load->model('admin/schools_model');
            $this->data['exam_detail'] = $exam_detail = $this->exam_model->get_all($data['examid'], true);
            
            //get lga from selected school
            $this->db->select('t_schools.*, '. ($exam_detail->haszone ? 't_zones.zoneid, t_zones.zonename' : 't_lgas.lgaid, t_lgas.lganame, t_lgas.lgainitials'));
            $this->db->from('t_schools');
            if($exam_detail->haszone) $this->db->join('t_zones', 't_zones.zoneid = t_schools.zoneid');
            else $this->db->join('t_lgas', 't_lgas.lgaid = t_schools.lgaid');
            $this->db->where('schoolid', $data['schoolid']);
            $school_data = $this->db->get()->row();
            $this->data['selected_school'] = $school_data;            
             
            $this->data['posted_values'] = $data;
           
            $this->db->order_by('examno');
            $this->data['candidates'] = $this->registration_model->get_where(array('schoolid'=>$data['schoolid'], 'lgaid'=>$school_data->lgaid, 'examid'=>$data['examid'], 'examyear'=>$data['examyear'], 'edcid'=>$edcid));
                                        
            $this->db->order_by('priority', 'asc');          
            $buffer_sql = "and t_scores.subjectid = '".$data['subjectid']."'";
            if(empty($data['subjectid']) || ($data['subjectid'] == "undefined")){
                $buffer_sql = '';
                $this->data['allsubjects'] = $this->subjects_model->get_where(array('examid'=>$data['examid'], 'edcid'=>$edcid));
            }else $this->data['allsubjects'] = $this->subjects_model->get_where(array('subjectid'=>$data['subjectid']));
            
            #get score margin
            $subject_ids = '';
            foreach ($this->data['allsubjects'] as $subject) {
                 $subject_ids .= "'" . $subject->subjectid . "',";
            }
            $subject_ids = rtrim($subject_ids, ',');
            $sql = "select * from t_scores_margin where subjectid in (".$subject_ids.") and edcid = '".$this->edcid."'";
            $this->data['maxscoredata'] = $maxdata = $this->db->query($sql)->result();
            $m_data = array();
            foreach ($maxdata as $mdata) {
                $m_data[$mdata->subjectid] = array(
                    'maxca' => $mdata->maxca,
                    'maxpractical' => $mdata->maxpractical,
                    'maxexam' => $mdata->maxexam
                    );
            }
            $this->data['maxscoredata'] = $m_data;
            
            #Get scores
            $sql = "select t_scores.* from t_scores 
                    inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                    where t_candidates.examid = '".$data['examid']."'
                    and t_candidates.schoolid = '".$data['schoolid']."' 
                    and t_candidates.examyear = '".$data['examyear']."'
                    and t_candidates.edcid = '".$this->edcid."' " . $buffer_sql; 
            $this->data['c_scores'] = $this->db->query($sql)->result();
            
             $this->data['subview'] = 'admin/Grading/gradesheet_practical_page';
             $this->load->view('admin/template/_layout_main', $this->data);
        }else{

            $this->data['subview'] = 'admin/Grading/grading_practical_page';
            $this->load->view('admin/template/_layout_main', $this->data);
        }
    }
    
    public function subjects_ajaxdrop()
    {
        if($this->_is_ajax()){
            $examid = $this->input->get('examid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);
            
            $this->db->select('subjectid as lgaid, subjectname as lganame');
            $this->db->where('examid', $examid);
            $this->db->where('haspractical', 1);
            $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $query = $this->db->get('t_subjects');
            
            $data['lgas'] = array();
            if ($query->num_rows() > 0)
            {
              $data['lgas'] = $query->result_array();
            }            
            echo json_encode($data);
            //return $data;
        }else{
            echo "Apparently is_ajax returned false!";
            show_error('This method can only be accessed internally.', 404);
        }
    }

    public function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }
 
     public function update_practical_score(){
        if($this->_is_ajax()){
            $data = array();
            $data['candidateid'] = $this->input->get('candidateid', TRUE);
            $data['subjectid'] = $this->input->get('subjectid', TRUE);
            $data['examid'] = $this->input->get('examid', TRUE);
            $data['examyear'] = $this->input->get('examyear', TRUE);
            $practical_score = $this->input->get('practical_score', TRUE);
            
            $score_detail = $this->scores_model->get_where($data, true);
            $ca_score = $score_detail->ca_score;
            $exam_score = $score_detail->exam_score;
            $total_score = (($ca_score < 0) ? 0 : $ca_score) + (($exam_score < 0) ? 0 : $exam_score) + (($practical_score < 0) ? 0 : $practical_score);
            
            if($exam_score < 0) $total_score = 0;
            
            $this->db->where($data);
            $this->db->update('t_scores', array('practical_score'=>$practical_score, 'total_score'=>$total_score));
            logSql($this->db->last_query(), $this->edcid);          
                        
            $success_data['success'] = 1;
            echo json_encode($success_data);
            
        }else{
             $success_data['success'] = 0;
             echo json_encode($success_data);
        }
    }
   
}
