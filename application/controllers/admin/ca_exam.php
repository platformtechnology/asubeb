<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ca_exam extends Admin_Controller {
    
    function __construct() {
        parent::__construct();
        
        $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Edc_Setup', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect(site_url('admin/login/logout'));
        }
        
        $this->load->model('admin/subjects_model');
        $this->load->model('admin/scores_margin_model');
    }
    
    public function index() { 
        $edcid = $this->data['edc_detail']->edcid;
       
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        //$this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        
        if($this->form_validation->run()){
            $this->load->model('admin/registration_model');
            $data = $this->subjects_model->array_from_post(array('examid'));
            $this->data['posted_values'] = $data;
            $this->data['subjects'] = $this->subjects_model->get_where(array('examid'=>$data['examid'], 'edcid'=>$edcid));
                 
            $this->db->where('examid', $data['examid']);
            $this->data['exams_data'] = $this->db->get('t_exams')->row();
             $this->data['subview'] = 'admin/caexam_page';
             $this->load->view('admin/template/_layout_main', $this->data);
        }else{

            $this->data['subview'] = 'admin/caexam_page';
            $this->load->view('admin/template/_layout_main', $this->data);
        }
    }
    

    public function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }
    
    public function update_maxca_score(){
        if($this->_is_ajax()){
            $data = array();
            $data['subjectid'] = $this->input->get('subjectid', TRUE);
            $ca_score = $this->input->get('ca_score', TRUE);
            
            $this->db->where($data);
            $this->db->update('t_scores_margin', array('maxca'=>$ca_score));
            logSql($this->db->last_query(), $this->edcid);
            
            $success_data['success'] = 1;
            echo json_encode($success_data);
            
        }else{
             $success_data['success'] = 0;
             echo json_encode($success_data);
        }
    }
    
     public function update_maxexam_score(){
        if($this->_is_ajax()){
            $data = array();
            
            $data['subjectid'] = $this->input->get('subjectid', TRUE);
            $exam_score = $this->input->get('exam_score', TRUE);
            
            $this->db->where($data);
            $this->db->update('t_scores_margin', array('maxexam'=>$exam_score));
            logSql($this->db->last_query(), $this->edcid);
            
            $success_data['success'] = 1;
            echo json_encode($success_data);
            
        }else{
             $success_data['success'] = 0;
             echo json_encode($success_data);
        }
    }
    
     public function update_maxpractical_score(){
        if($this->_is_ajax()){
            $data = array();
            $data['subjectid'] = $this->input->get('subjectid', TRUE);
            $practical_score = $this->input->get('prac_score', TRUE);
            
            $this->db->where($data);
            $this->db->update('t_scores_margin', array('maxpractical'=>$practical_score));
            logSql($this->db->last_query(), $this->edcid);
                       
            $success_data['success'] = 1;
            echo json_encode($success_data);
            
        }else{
             $success_data['success'] = 0;
             echo json_encode($success_data);
        }
    }
}
