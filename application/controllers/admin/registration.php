<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if(!in_array('Registration', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
        $this->load->model('admin/exam_model');
        $this->load->model('admin/registration_model');

        $this->edcid = $this->data['edc_detail']->edcid;
    }

    public function index() {
        $this->data['exams'] = $this->exam_model->get_where(array('edcid'=>$this->edcid));

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if($this->form_validation->run()){
            $data = $this->registration_model->array_from_post(array('examid', 'examyear'));
            redirect(site_url('admin/registration/register/'.$data['examid'].'/'.$data['examyear']));
        }

        $this->data['subview'] = 'admin/register_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    private function _checkGrading($examid, $year, $candidateid){
        $sql = "select sum(total_score) as total from t_scores where examid = '". $examid ."' "
                . "and examyear = '".$year."' and candidateid = '".$candidateid."'";
        $result = $this->db->query($sql)->row();
        if($result->total > 0){
             $this->session->set_flashdata('error', 'Student Has Already Been Graded - Records CANNOT Be Edited');
             redirect('registration/register/'.$examid.'/'.$year.'/'.$candidateid);
        }
    }

    public function row($examid, $year, $candidateid = null){

        if($candidateid == null) {
            $this->data['candidate_detail'] = $this->registration_model->is_new();
        }
        else {
            $this->data['candidate_detail'] = $this->registration_model->get_all($candidateid, true);
            $this->session->set_flashdata('done', $this->data['candidate_detail']->schoolid);
             $this->session->set_flashdata('donelga', $this->data['candidate_detail']->lgaid);
             $this->session->set_flashdata('donegender', $this->data['candidate_detail']->gender);
        }

        $this->data['exam_year'] = $year;

        $this->db->where('examid', $examid);
        $this->data['exam_detail'] = $this->db->get('t_exams')->row();

        //for lga ddl
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $this->db->order_by('lganame', 'asc');
        $this->data['lgas'] = $this->db->get('t_lgas')->result();

         if($this->data['exam_detail']->hassecschool == 1) $this->db->where('issecondary', 1);
         else $this->db->where('isprimary', 1);

         $this->db->where('edcid', $this->data['edc_detail']->edcid);
         $this->db->order_by('schoolname', 'asc');

         if($this->session->flashdata('donelga')) $this->db->where('lgaid', $this->session->flashdata('donelga'));
         $this->data['schools'] = $this->db->get('t_schools')->result();

        $this->form_validation->set_rules('schoolid', 'School', 'trim|required');
        $this->form_validation->set_rules('firstname', 'Surname', 'trim|required');
        $this->form_validation->set_rules('othernames', 'Othernames', 'trim|required');
        $this->form_validation->set_rules('dob', 'Date Of Birth', 'trim|required');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
        //$this->form_validation->set_rules('phone', 'Phone', 'trim|required|is_natural');

              //Get Subjects
            $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $this->db->where('examid', $examid);
            $this->db->where('examyear', $year);
            $this->data['subjects'] = $this->db->get('t_subjects')->result();

            $select_subjs = array();
            foreach($this->data['subjects'] as $subjs){
                if($subjs->iscompulsory == 0){
                    $select_subjs[] = $subjs->subjectid;
                }
            }

        if($this->form_validation->run()){
            $data = $this->registration_model->array_from_post(array('schoolid', 'firstname', 'othernames', 'dob', 'gender', 'phone'));

            //get lgaid and zoneid
             $this->db->where('schoolid', $data['schoolid']);
             $school_data = $this->db->get('t_schools')->row();

            $data['lgaid'] = $school_data->lgaid;
            $data['zoneid'] = $school_data->zoneid;
            $data['examid'] = $examid;
            $data['examyear'] = $year;
            $data['edcid'] = $this->data['edc_detail']->edcid;
            $data['centreid'] = $data['schoolid'];
            if($candidateid == null){
                $data['candidateid'] = $this->registration_model->generate_unique_id();
                // to avoid duplicate while synching/inserting
                $this->registration_model->delete($data['candidateid']);
                $sql  = "delete from t_registered_subjects where candidateid = '" . $data['candidateid'] . "'; ";
                $sql .= "delete from t_scores where candidateid = '" . $data['candidateid'] . "'; ";
                $this->db->query($sql);
                //logSql($sql, $this->edcid);

                $data['examno'] = $this->_generate_examNumber($this->db->where('lgaid', $school_data->lgaid)->get('t_lgas')->row(), $data['schoolid'], $examid);
                $data['serial'] = $this->data['serial'];
            }

            //Perform Automatic assigning of first/second choice
             if($this->data['exam_detail']->hasposting == 1){

                 $this->db->where('issecondary', 1);
                 $this->db->where('ispostingschool', 1);
                 $this->db->where('edcid', $this->data['edc_detail']->edcid);
                 $schools = $this->db->get('t_schools');
                 $count_rows = count($schools->result());
                 $randval = mt_rand(0, $count_rows);

                 $row = $schools->row_array($randval);
                 $data['firstchoice'] = $row['schoolid'];

                 $randval = mt_rand(0, $count_rows);
                 $row = $schools->row_array($randval);
                 $data['secondchoice'] = $row['schoolid'];

             }else{
                  $data['firstchoice'] = 0;
                  $data['secondchoice'] = 0;
             }

             //save all compulsory subject plus the ones user selected
             if($candidateid == null){
                $this->_save_registered_subjects_and_scores($data['candidateid'] , $select_subjs, $examid, $year);
             }
              $this->registration_model->save_update($data, $candidateid);

            //Enter Audit Log For Registration Action
             $message = 'Candidates Information >>><br/>'
                     . 'Candidate Name: '.ucwords($data['firstname'].' '.$data['othernames']).'<br/>'
                     . 'DOB: '.$data['dob'].'<br/>'
                     . 'Phone: '.$data['phone'].'<br/>'
                     . 'Gender: '.$data['gender'].'.... etc<br/>';

             if($this->data['exam_detail']->hasposting == 1){
                $message .= '<br/>FirstChoice: '.$this->registration_model->get_school($data['firstchoice'] ).'<br/>';
                $message .= 'SecondChoice: '.$this->registration_model->get_school($data['secondchoice'] ).'<br/>';
             }

            //
            //$this->audittrail_model->log_audit($this->session->userdata('user_id'), 'ADD',
             //       $this->session->userdata('user_name'), 'Added a new CANDIDATE for '.$this->registration_model->getExamName_From_Id($examid).' '.$year,
                //    $message, $this->data['edc_detail']->edcid);
             //End Auidt Log Entering

             $this->session->set_flashdata('msg', 'Candidate Registered Succesfully');
             $this->session->set_flashdata('done', $data['schoolid']);
             $this->session->set_flashdata('donelga', $data['lgaid']);
             $this->session->set_flashdata('donegender', $data['gender']);
             redirect(site_url('admin/registration/row/'.$examid.'/'.$year));
        }

        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';


        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';


        $this->data['subview'] = 'admin/registration_rowise_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function register($examid, $year, $candidateid = null){

        //Get details of the selected exam.
        $this->data['exam_detail'] = $this->exam_model->get_all($examid);
        count($this->data['exam_detail']) || show_error("INVALID EXAMINATION TYPE SPECIFIED - EXAM TYPE NOT FOUND");

        $validation_rules = $this->registration_model->_rules;

        if($candidateid == null) {
            $this->data['candidate_detail'] = $this->registration_model->is_new();
            $validation_rules['lga']['rules'] .= '|required';
            $validation_rules['school']['rules'] .= '|required';
        }
        else {
            $this->data['candidate_detail'] = $this->registration_model->get_all($candidateid, true);
            $this->data['schools'] = $this->db->get('t_schools')->result();
            $this->data['registered_subjects'] = $this->db->get_where('t_registered_subjects', array('candidateid'=>$candidateid))->result();
        }

        //Get Subjects Data for DDL
        $this->db->where('edcid', $this->edcid);
        $this->db->where('examid', $examid);
        $this->db->where('examyear', $year);
        $this->data['subjects'] = $this->db->get('t_subjects')->result();

        //
        $this->form_validation->set_rules($validation_rules);
        if($this->data['exam_detail']->hasposting == 1){
            $this->form_validation->set_rules('firstchoice', 'First Choice', 'trim');
            $this->form_validation->set_rules('secondchoice', 'Second Choice', 'trim');
        }

        if($this->form_validation->run() == true){

            if($candidateid == null){
                //Generate Candidate id
                $candidate_id = $this->registration_model->generate_unique_id();

                // to avoid duplicate while synching/inserting
                $this->registration_model->delete($candidate_id);
                $sql  = "delete from t_registered_subjects where candidateid = '" . $candidate_id . "'; ";
                $sql .= "delete from t_scores where candidateid = '" . $candidate_id . "'; ";
                $this->db->query($sql);
                logSql($sql, $this->edcid);

            }else  $this->_checkGrading($examid, $year, $candidateid); // if grading has been done for this student, don't update

            //Upload the Candidate's Passport;
            $id_as_filename = $candidateid == null ? $candidate_id : $candidateid;
            $this->_performUpload($id_as_filename, $examid, $year, 1);

            //collect posted data
            $data = $this->registration_model->array_from_post(array('firstname', 'othernames', 'firstchoice',
                                            'secondchoice', 'gender',
                                            'dob', 'phone'));

            $data['edcid'] = $this->edcid;
            $data['examid'] = $examid;
            $data['examyear'] = $year;

            if($candidateid == null){
                 //Get Zone ID
                $this->db->where('lgaid', $this->input->post('lgaid'));
                $lga_data = $this->db->get('t_lgas')->row();
                $data['zoneid'] = $lga_data->zoneid;
                //end

                $data['lgaid'] = $this->input->post('lgaid');
                $data['schoolid'] = $this->input->post('schoolid');
                if($data['schoolid'] == "undefined"){
                    $this->session->set_flashdata('error', 'Invalid School Selected - Retry Operation');
                    redirect('registration/register/'.$examid.'/'.$year);
                }
                $data['centreid'] = $data['schoolid'];

                $data['candidateid'] = $candidate_id;
                $data['examno'] = $this->_generate_examNumber($lga_data, $data['schoolid'], $examid);
                $data['serial'] = $this->data['serial'];

                //save all compulsory subject plus the ones user selected
                $this->_save_registered_subjects_and_scores($candidate_id, $this->input->post('subject'), $examid, $year);
            }else{
                //its an edit - delete all subjects and scores for this candidate and insert newly selected subjects + compulsory ones
                $this->db->delete('t_registered_subjects', array('candidateid'=>$candidateid, 'examid'=>$examid, 'examyear'=>$year));
                logSql($this->db->last_query(), $this->edcid);

                $this->db->delete('t_scores', array('candidateid'=>$candidateid, 'examid'=>$examid, 'examyear'=>$year));
                logSql($this->db->last_query(), $this->edcid);

                $this->_save_registered_subjects_and_scores($candidateid, $this->input->post('subject'), $examid, $year);
            }

            $this->registration_model->save_update($data, $candidateid);

            //Enter Audit Log For Registration Action
             $message = 'Candidates Information>>><br/>'
                     . 'Candidate Name: '.ucwords($data['firstname'].' '.$data['othernames']).'<br/>'
                     . 'DOB: '.$data['dob'].'<br/>'
                     . 'Phone: '.$data['phone'].'<br/>'
                     . 'Gender: '.$data['gender'].'.... etc<br/>';

             if($this->data['exam_detail']->hasposting == 1){
                $message .= '<br/>FirstChoice: '.$this->registration_model->get_school($this->input->post('firstchoice')).'<br/>';
                $message .= 'SecondChoice: '.$this->registration_model->get_school($this->input->post('secondchoice')).'<br/>';
            }

            $this->audittrail_model->log_audit($this->session->userdata('user_id'), ($candidateid == NULL ? 'ADD' : 'UPDATE'),
                    $this->session->userdata('user_name'), ($candidateid == NULL ? ('Added a new CANDIDATE for '.$this->registration_model->getExamName_From_Id($examid).' '.$year) : ('Updated Details of a CANDIDATE for '.$this->registration_model->getExamName_From_Id($examid).' '.$year)),
                    $message, $this->data['edc_detail']->edcid);
             //End Auidt Log Entering

           redirect(site_url('admin/registration/confirm/'.($candidateid == null ? $candidate_id : $candidateid)));
           //  print_r($data); exit;
        }//End if the form posted well

        $this->data['exam_year'] = $year;

        //Get Lga Data for DDL
        $this->db->where('edcid', $this->edcid);
        $this->data['lgs'] = $this->db->get('t_lgas')->result();


        $this->data['centres'] = $this->db->get_where('t_schools', array('iscentre' => 1))->result();
        $this->data['choice1'] = $this->db->get_where('t_schools', array('ispostingschool' => 1))->result();
        $this->data['choice2'] = $this->data['choice1'];

        //Add styles and scripts for the fileupload
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/js/bootstrap-fileupload.js') . '"></script>';

        //Add styles and scripts for the datetime

        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';

        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';


        $this->data['subview'] = 'admin/registration_form_page';
        $this->load->view('admin/template/_layout_main', $this->data);

    }

    public function _performUpload($candidateid, $examid, $examyear, $edit_candidate){

        if($edit_candidate == null)
        {
            $config = array(
                 'upload_path' => './resources/images/passports/',
                 'allowed_types' => 'jpg|png|jpeg',
                 'max_size' => '500',
                 'overwrite' => TRUE,
                 'file_name' => $candidateid . '.jpg',
                 'remove_spaces' => TRUE
             );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('passport'))
            {
               $this->session->set_flashdata('error', $this->upload->display_errors());
               redirect(site_url('admin/registration/register/'.$examid.'/'.$examyear));
            }

            return $this->upload->data();

        }else{
            if(empty($_FILES['passport']['name'])){
                return;
            }else{
                $this->_performUpload($candidateid, $examid, $examyear, null);
            }
        }
    }

    public function confirm($candidateid, $refresh = null){

        $this->data['reg_info'] = $this->registration_model->get_all($candidateid);
        count($this->data['reg_info']) || show_error('Candidate Detail Not Found - Invalid Param');

        $this->data['exam_info'] = $this->exam_model->get_all($this->data['reg_info']->examid);

        $this->data['subjects'] = $this->db->get_where('t_registered_subjects', array('candidateid'=>$candidateid))->result();

        //Add styles and scripts for the fileupload
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';

        $this->data['passport'] = get_img('no_image.jpg');
        if(file_exists('./resources/images/passports/'.$candidateid.'.jpg')){
           $this->data['passport'] = base_url('resources/images/passports/'.$candidateid.'.jpg');
        }

        if($refresh == null){
            redirect(site_url('admin/registration/confirm/'.$candidateid.'/success'), 'refresh');
        }

        $this->data['subview'] = 'admin/registration_confirm_page';
        $this->load->view('admin/template/_layout_main', $this->data);

    }

     public function _save_registered_subjects_and_scores($candidateid, $subjects, $examid, $examyear){


        $this->load->model('admin/reg_subjects_model');
        $this->load->model('admin/scores_model');

        //save all the compulsory subjects first
        foreach ($this->data['subjects'] as $subject) {
            if($subject->iscompulsory == 1){
                $subj_data = array();
                $subj_data['candidateid'] = $candidateid;
                $subj_data['subjectid'] = $subject->subjectid;
                $subj_data['examid'] = $examid;
                $subj_data['examyear'] = $examyear;
                $subj_data['edcid'] = $this->edcid;
                $this->reg_subjects_model->save_update($subj_data);

                //insert default Exam, CA and Practical scores for compulsory subjects
                $data = array();
                $data['scoreid'] = $this->scores_model->generate_unique_id();
                $data['subjectid'] = $subject->subjectid;
                $data['edcid'] = $this->edcid;
                $data['examid'] = $examid;
                $data['examyear'] = $examyear;
                $data['candidateid'] = $candidateid;
                $this->scores_model->save_update($data);

            }
        }

        //save the subjects that the student selected
        if(is_array($subjects)):
        foreach ($subjects as $subject) {
            $subj_data = array();
            $subj_data['candidateid'] = $candidateid;
            $subj_data['subjectid'] = $subject;
            $subj_data['examid'] = $examid;
            $subj_data['examyear'] = $examyear;
            $subj_data['edcid'] = $this->edcid;
            $this->reg_subjects_model->save_update($subj_data);

             //insert default scores for selected subjects
            $data = array();
            $data['scoreid'] = $this->scores_model->generate_unique_id();
            $data['subjectid'] = $subject;
            $data['candidateid'] = $candidateid;
            $data['examid'] = $examid;
            $data['examyear'] = $examyear;
            $data['edcid'] = $this->edcid;
            $this->scores_model->save_update($data);

        }
        endif;
    }

    public function _generate_examNumber($lga_data, $schoolid, $examid){
        $school_data = $this->db->get_where('t_schools', array('schoolid'=>$schoolid))->row();
        $activeyear = $this->data['activeyear'];

        if(count($lga_data)){
            $schoolCode = $school_data->schoolcode;
            $lgaCode = $lga_data->lgainitials;
            $serial_num = '';

            $this->db->order_by('serial', 'desc');
            $this->db->where('lgaid', $lga_data->lgaid);
            $this->db->where('schoolid', $schoolid);
            $this->db->where('examid', $examid);
            $this->db->where('examyear', $activeyear);
            $result_set = $this->db->get('t_candidates');
            $candidate_data = $result_set->first_row();
            if(count($candidate_data)){
                $serial = $candidate_data->serial;
                if(trim($serial) == false){
                   $serial_num = sprintf("%03s", 1);
                }else{
                    $serial_num = $serial + 1;
                    $serial_num = sprintf("%03s", $serial_num);
                }
            }else{
               $serial_num = sprintf("%03s", 1);
            }

            $this->data['serial'] = $serial_num;
            $exam_num = $lgaCode.'/'.$schoolCode.'/'.$serial_num;

            return $exam_num;
        }else{
            $schoolCode = $school_data->schoolcode;
            $serial_num = '';

            $this->db->order_by('serial', 'desc');
            $this->db->where('schoolid', $schoolid);
            $this->db->where('examid', $examid);
             $this->db->where('examyear', $activeyear);
            $result_set = $this->db->get('t_candidates');
            $candidate_data = $result_set->first_row();
            if(count($candidate_data)){
                $serial = $candidate_data->serial;
                if(trim($serial) == false){
                   $serial_num = sprintf("%03s", 1);
                }else{
                    $serial_num = $serial + 1;
                    $serial_num = sprintf("%03s", $serial_num);
                }
            }else{
               $serial_num = sprintf("%03s", 1);
            }

            $this->data['serial'] = $serial_num;
            $exam_num = $schoolCode.'/'.$serial_num;

            return $exam_num;
        }
    }

    public function code_ajaxdrop(){
        if($this->_is_ajax()){
            $schoolid = $this->input->get('schoolid', TRUE);

            $this->db->select('schoolcode');
            $this->db->where('schoolid', $schoolid);
            $query = $this->db->get('t_schools')->row();

            $code = count($query) ? $query->schoolcode : 'No Code';

            echo $code;
            //return $data;
        }else{
            echo "Apparently is_ajax returned false!";
            show_error('This method can only be accessed internally.', 404);
        }
    }

    public function school_ajaxdrop(){
        if($this->_is_ajax()){
            $lgaid = $this->input->get('lgaid', TRUE);
            $hassec = $this->input->get('issec', TRUE);

            $this->db->select('schoolid as lgaid, schoolname as lganame');
            $this->db->where('lgaid', $lgaid);
             $this->db->order_by('schoolname');

            if($hassec == 1){
                $this->db->where('issecondary', 1);
            }else{
                 $this->db->where('isprimary', 1);
            }
            $query = $this->db->get('t_schools');

            $data['lgas'] = array();
            if ($query->num_rows() > 0)
            {
              $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        }else{
            echo "Apparently is_ajax returned false!";
            show_error('This method can only be accessed internally.', 404);
        }
    }

    function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

    public function fetchCandidates(){
        if($this->_is_ajax()){
            $schoolid = $this->input->get('schoolid', TRUE);
            $examid = $this->input->get('examid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);

            $this->db->where('schoolid', $schoolid);
            $this->db->where('examid', $examid);
            $this->db->where('examyear', $examyear);
            $this->db->order_by('examno', 'asc');
            $candidates = $this->db->get('t_candidates')->result();

            $table_data = "<h4> " . $this->registration_model->get_school($schoolid) . " </h4><hr/>";
            $table_data .= '<table class="table table-bordered table-condensed" id="example"> '
                      .'<thead>'
                      .'  <tr>'
                       .'   <th>Sn</th>'
                        .'   <th>Examno</th>'
                       .'   <th>Surname</th>'
                        .'  <th>Othernames</th>'
                        .'  <th>Gender</th>'
                        .'  <th>DOB</th>   '
                        .'  <th>Phone</th>   '
                        .'  <th></th>   '
                       .' </tr>'
                      .'</thead>'
                     .' <tbody>';

                if(count($candidates)){
                    $sn =0;
                    foreach ($candidates as $candidate) {
                         $table_data .= '<tr>';
                            $table_data .= '<td>' . ++$sn .'</td>';
                             $table_data .= '<input type="hidden" id="candidateid'.$sn.'" name="candidateid'.$sn.'" value="'.$candidate->candidateid.'" />';
                             $table_data .= '<td><input onblur="examPost(\''.$sn.'\')" type="text" id="examno'.$sn.'" name="examno'.$sn.'" value="'.$candidate->examno.'" style="border: 0px;" /><span id="exam-result'.$sn.'"></span></td>';
                            $table_data .= '<td><input onblur="firstnamePost(\''.$sn.'\')" type="text" id="firstname'.$sn.'" name="firstname'.$sn.'" value="' . (strtoupper($candidate->firstname)) .'" style="border: 0px;" /><span id="fname-result'.$sn.'"></span></td>';
                            $table_data .= '<td><input onblur="othernamePost(\''.$sn.'\')" type="text" id="othername'.$sn.'" name="othername'.$sn.'" value="' . (strtoupper($candidate->othernames)) .'" style="border: 0px;" /><span id="oname-result'.$sn.'"></span></td>';
                            $table_data .= '<td>' . strtoupper($candidate->gender) .'</td>';
                            $table_data .= '<td>' . $candidate->dob .'</td>';
                            $table_data .= '<td><input onblur="phonePost(\''.$sn.'\')" type="text" id="phone'.$sn.'" name="phone'.$sn.'" value="' . $candidate->phone .'" style="border: 0px;" /><span id="phone-result'.$sn.'"></span></td>';
                            $table_data .= '<td>'
                                    . '<a href="'.site_url('admin/registration/delete/'. $candidate->candidateid.'/'.$examid.'/'.$examyear).'" onclick="return confirm(\'You are about to delete a record. Action cannot be undone. Are you sure you want to proceed?\');"><i class="glyphicon glyphicon-remove"></i></a> | '
                                    . '<a href="'.site_url('admin/registration/row/'.$examid.'/'.$examyear.'/'.$candidate->candidateid).'" onclick="return confirm(\'You are about to Edit a Candidate. Are you sure you want to Edit?\');"><i class="glyphicon glyphicon-edit"></i></a>'
                                    . '</td>';
                         $table_data .= '</tr>';
                    }
                }

            $table_data .= '</tbody>'
                    . '</table>';

            echo $table_data;
        }
    }


     public function delete($candidateid, $examid, $examyear) {
        $student = $this->registration_model->get_all($candidateid, true);
        $this->registration_model->delete($candidateid);

        $this->db->where('candidateid', $candidateid);
        $this->db->where('examid', $examid);
        $this->db->where('examyear', $examyear);
        $this->db->delete('t_scores');
        logSql($this->db->last_query(), $this->edcid);

        $this->db->where('candidateid', $candidateid);
        $this->db->where('examid', $examid);
        $this->db->where('examyear', $examyear);
        $this->db->delete('t_registered_subjects');
        logSql($this->db->last_query(), $this->edcid);

        //Enter Audit Log For Registration Action
             $message = 'Candidates Information>>><br/>'
                     . 'Candidate Name: '.ucwords($student->firstname.' '.$student->othernames).'<br/>'
                     . 'DOB: '.$student->dob.'<br/>'
                     . 'Phone: '.$student->phone.'<br/>'
                     . 'Gender: '.$student->gender.'<br/>'
                    . 'School: '.$this->registration_model->get_school($student->schoolid).'<br/>';

            $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'DELETE',
                    $this->session->userdata('user_name'), 'Deleted a CANDIDATE',
                    $message, $this->data['edc_detail']->edcid);
             //End Auidt Log Entering

        $this->session->set_flashdata('msg', 'Candidate Deleted Successfully');
        $this->session->set_flashdata('done', $student->schoolid);
        $this->session->set_flashdata('donelga', $student->lgaid);
        $this->session->set_flashdata('donegender', $student->gender);
        redirect(site_url('admin/registration/row/'.$examid.'/'.$examyear));
    }

    public function update_examno(){
        if($this->_is_ajax()){
            $data = array();
            $data['candidateid'] = $this->input->get('candidateid', TRUE);

            $examno = $this->input->get('exam_no', TRUE);
            $splitData = explode('/', $examno);
            if(!count($splitData)){
                 $success_data['success'] = 0;
                echo json_encode($success_data);
                return;
            }

            $serial = $splitData[count($splitData) - 1];

            if(!isset($serial) || empty($serial)){
                $success_data['success'] = 0;
                echo json_encode($success_data);
                return;
            }

            $serial = intval($serial);

            $this->db->where($data);
            $this->db->update('t_candidates', array('examno'=>$examno, 'serial'=>$serial));
            logSql($this->db->last_query(), $this->edcid);

            $success_data['success'] = 1;
            echo json_encode($success_data);

        }else{
             $success_data['success'] = 0;
             echo json_encode($success_data);
        }
    }

    public function update_phone(){
        if($this->_is_ajax()){
            $data = array();
            $data['candidateid'] = $this->input->get('candidateid', TRUE);

            $phone = $this->input->get('phone', TRUE);

            $this->db->where($data);
            $this->db->update('t_candidates', array('phone'=>$phone));
            logSql($this->db->last_query(), $this->edcid);

            $success_data['success'] = 1;
            echo json_encode($success_data);

        }else{
             $success_data['success'] = 0;
             echo json_encode($success_data);
        }
    }

    public function update_firstname(){
        if($this->_is_ajax()){
            $data = array();
            $data['candidateid'] = $this->input->get('candidateid', TRUE);

            $firstname = $this->input->get('firstname', TRUE);

            $this->db->where($data);
            $this->db->update('t_candidates', array('firstname'=>$firstname));
            logSql($this->db->last_query(), $this->edcid);

            $success_data['success'] = 1;
            echo json_encode($success_data);

        }else{
             $success_data['success'] = 0;
             echo json_encode($success_data);
        }
    }

    public function update_othernames(){
        if($this->_is_ajax()){
            $data = array();
            $data['candidateid'] = $this->input->get('candidateid', TRUE);

            $othernames = $this->input->get('othernames', TRUE);

            $this->db->where($data);
            $this->db->update('t_candidates', array('othernames'=>$othernames));
            logSql($this->db->last_query(), $this->edcid);

            $success_data['success'] = 1;
            echo json_encode($success_data);

        }else{
             $success_data['success'] = 0;
             echo json_encode($success_data);
        }
    }

#UPDATES
    public function registration_errors($examid,$examyear) {
        $this->data['exam_year'] = $examyear;
        $this->db->where('examid', $examid);
        $this->data['exam_detail'] = $this->db->get('t_exams')->row();

         if($this->data['exam_detail']->hassecschool == 1)
            {
                $this->db->where('issecondary', 1);
            }
         else {
                $this->db->where('isprimary', 1);
         }
         $this->db->where('edcid', $this->data['edc_detail']->edcid);
         $this->db->order_by('schoolname', 'asc');
         $this->data['schools'] = $this->db->get('t_schools')->result();

        $rules = array('schoolid'=>array('field'=>'schoolid',
                                          'label' => 'School',
                                          'rules'=>'trim|required'));
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == TRUE){
            #GET THE SCHOOL ID OF THE SELECTED SCHOOL
            $data = $this->registration_model->array_from_post(array('schoolid'));
            $this->data['schoolinfo'] = $this->db->where('schoolid',$data['schoolid'])->get('t_schools')->row();
            #GRAB CANDIDATES WHO REGISTERED IN THE SCHOOL FOR THE EXAMS
            $this->db->where('schoolid', $data['schoolid']);
            $this->db->where('examid', $examid);
            $this->db->where('examyear', $examyear);
            $this->db->where('edcid', $this->edcid);
            $this->data['candidates'] = $this->db->get('t_candidates')->result();
        }

        $this->data['subview'] = 'admin/registration_errors_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function errors() {
        #GET EXAMS
        $this->data['exams'] = $this->exam_model->get_where(array('edcid'=>$this->edcid));
        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if($this->form_validation->run()){
            $data = $this->registration_model->array_from_post(array('examid', 'examyear'));
            redirect(site_url('admin/registration/registration_errors/'.$data['examid'].'/'.$data['examyear']));
        }

        $this->data['subview'] = 'admin/registration_errors_select_exam_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function clear_registration_errors($schoolid,$examid,$examyear) {

        //GET CANDIDATES
            #GRAB CANDIDATES WHO REGISTERED IN THE SCHOOL FOR THE EXAMS
            $this->db->where('schoolid', $schoolid);
            $this->db->where('examid', $examid);
            $this->db->where('examyear', $examyear);
            $this->db->where('edcid', $this->edcid);
            $this->data['candidates'] = $candidates = $this->db->get('t_candidates')->result();

            #CONVERT CANDIDATE ID INTO STRINGS FOR SQL QUERY
            $candidate_id_string = '';
            if (count($candidates)) {
                foreach($candidates as $candidate){
                    $candidate_id_string .= "'". $candidate->candidateid . "',";
                }

                $candidate_id_string = rtrim($candidate_id_string,",");
            }

            #RUN DELETE QUERIES

            #DELETE FROM CANDIDATES TABLE
            $sql = "DELETE FROM t_candidates where candidateid IN (".$candidate_id_string.")";
            $candidates_table_result = $this->db->query($sql);
            #DELETE FROM REGISTRED SUBJECTS TABLE
            $sql = "DELETE FROM t_registered_subjects where candidateid IN (".$candidate_id_string.")";
            $reg_subjects_table_result = $this->db->query($sql);
            #DELETE FROM SCORES TABLE
            $sql = "DELETE FROM t_scores where candidateid IN (".$candidate_id_string.")";
            $scores_table_result = $this->db->query($sql);

            #DID EVERYTHING GO WELL?
            if ($candidates_table_result && $reg_subjects_table_result && $scores_table_result) {
                $this->session->set_flashdata('success','Candidates Cleared Successfully');
                redirect('admin/registration/registration_errors');
            }
            else {
                $this->session->set_flashdata('error','An Error Occured, Please try again');
                redirect('admin/registration/registration_errors');
            }

    }


    public function edit_school_of_choice_index() {
        $this->db->where('hasposting',1);
        $this->data['exams'] = $this->exam_model->get_where(array('edcid'=>$this->edcid));

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if($this->form_validation->run()){
            $data = $this->registration_model->array_from_post(array('examid', 'examyear'));
            redirect(site_url('admin/registration/edit_school_of_choice/'.$data['examid'].'/'.$data['examyear']));
        }

        $this->data['subview'] = 'admin/edit_school_of_choice_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function edit_school_of_choice($examid, $year, $candidateid = null){

        if($candidateid == null) {
            $this->data['candidate_detail'] = $this->registration_model->is_new();
        }
        else {
            $this->data['candidate_detail'] = $this->registration_model->get_all($candidateid, true);
            $this->session->set_flashdata('done', $this->data['candidate_detail']->schoolid);
             $this->session->set_flashdata('donelga', $this->data['candidate_detail']->lgaid);
             $this->session->set_flashdata('donegender', $this->data['candidate_detail']->gender);
        }

        $this->data['exam_year'] = $year;

        $this->db->where('examid', $examid);
        $this->data['exam_detail'] = $this->db->get('t_exams')->row();

        //for lga ddl
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $this->db->order_by('lganame', 'asc');
        $this->data['lgas'] = $this->db->get('t_lgas')->result();

         if($this->data['exam_detail']->hassecschool == 1) $this->db->where('issecondary', 1);
         else $this->db->where('isprimary', 1);

         $this->db->where('edcid', $this->data['edc_detail']->edcid);
         $this->db->order_by('schoolname', 'asc');

         if($this->session->flashdata('donelga')) $this->db->where('lgaid', $this->session->flashdata('donelga'));
         $this->data['schools'] = $this->db->get('t_schools')->result();

        $this->form_validation->set_rules('schoolid', 'School', 'trim|required');
        $this->form_validation->set_rules('firstname', 'Surname', 'trim|required');
        $this->form_validation->set_rules('othernames', 'Othernames', 'trim|required');
        $this->form_validation->set_rules('dob', 'Date Of Birth', 'trim|required');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
        //$this->form_validation->set_rules('phone', 'Phone', 'trim|required|is_natural');

              //Get Subjects
            $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $this->db->where('examid', $examid);
            $this->db->where('examyear', $year);
            $this->data['subjects'] = $this->db->get('t_subjects')->result();

            $select_subjs = array();
            foreach($this->data['subjects'] as $subjs){
                if($subjs->iscompulsory == 0){
                    $select_subjs[] = $subjs->subjectid;
                }
            }

        if($this->form_validation->run()){
            $data = $this->registration_model->array_from_post(array('schoolid', 'firstname', 'othernames', 'dob', 'gender', 'phone'));

            //get lgaid and zoneid
             $this->db->where('schoolid', $data['schoolid']);
             $school_data = $this->db->get('t_schools')->row();

            $data['lgaid'] = $school_data->lgaid;
            $data['zoneid'] = $school_data->zoneid;
            $data['examid'] = $examid;
            $data['examyear'] = $year;
            $data['edcid'] = $this->data['edc_detail']->edcid;
            $data['centreid'] = $data['schoolid'];
            if($candidateid == null){
                $data['candidateid'] = $this->registration_model->generate_unique_id();
                // to avoid duplicate while synching/inserting
                $this->registration_model->delete($data['candidateid']);
                $sql  = "delete from t_registered_subjects where candidateid = '" . $data['candidateid'] . "'; ";
                $sql .= "delete from t_scores where candidateid = '" . $data['candidateid'] . "'; ";
                $this->db->query($sql);
                //logSql($sql, $this->edcid);

                $data['examno'] = $this->_generate_examNumber($this->db->where('lgaid', $school_data->lgaid)->get('t_lgas')->row(), $data['schoolid'], $examid);
                $data['serial'] = $this->data['serial'];
            }

            //Perform Automatic assigning of first/second choice
             if($this->data['exam_detail']->hasposting == 1){

                 $this->db->where('issecondary', 1);
                 $this->db->where('ispostingschool', 1);
                 $this->db->where('edcid', $this->data['edc_detail']->edcid);
                 $schools = $this->db->get('t_schools');
                 $count_rows = count($schools->result());
                 $randval = mt_rand(0, $count_rows);

                 $row = $schools->row_array($randval);
                 $data['firstchoice'] = $row['schoolid'];

                 $randval = mt_rand(0, $count_rows);
                 $row = $schools->row_array($randval);
                 $data['secondchoice'] = $row['schoolid'];

             }else{
                  $data['firstchoice'] = 0;
                  $data['secondchoice'] = 0;
             }

             //save all compulsory subject plus the ones user selected
             if($candidateid == null){
                $this->_save_registered_subjects_and_scores($data['candidateid'] , $select_subjs, $examid, $year);
             }
              $this->registration_model->save_update($data, $candidateid);

            //Enter Audit Log For Registration Action
             $message = 'Candidates Information >>><br/>'
                     . 'Candidate Name: '.ucwords($data['firstname'].' '.$data['othernames']).'<br/>'
                     . 'DOB: '.$data['dob'].'<br/>'
                     . 'Phone: '.$data['phone'].'<br/>'
                     . 'Gender: '.$data['gender'].'.... etc<br/>';

             if($this->data['exam_detail']->hasposting == 1){
                $message .= '<br/>FirstChoice: '.$this->registration_model->get_school($data['firstchoice'] ).'<br/>';
                $message .= 'SecondChoice: '.$this->registration_model->get_school($data['secondchoice'] ).'<br/>';
             }

            //
            //$this->audittrail_model->log_audit($this->session->userdata('user_id'), 'ADD',
             //       $this->session->userdata('user_name'), 'Added a new CANDIDATE for '.$this->registration_model->getExamName_From_Id($examid).' '.$year,
                //    $message, $this->data['edc_detail']->edcid);
             //End Auidt Log Entering

             $this->session->set_flashdata('msg', 'Candidate Registered Succesfully');
             $this->session->set_flashdata('done', $data['schoolid']);
             $this->session->set_flashdata('donelga', $data['lgaid']);
             $this->session->set_flashdata('donegender', $data['gender']);
             redirect(site_url('admin/registration/row/'.$examid.'/'.$year));
        }

        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';


        $this->data['subview'] = 'admin/edit_school_of_choice_main_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function fetchChoiceCandidates(){
        if($this->_is_ajax()){
            $schoolid = $this->input->get('schoolid', TRUE);
            $examid = $this->input->get('examid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);
            $posting_schools = $this->getPostingSchools($schoolid);
            $this->db->where('schoolid', $schoolid);
            $this->db->where('examid', $examid);
            $this->db->where('examyear', $examyear);
            $this->db->order_by('examno', 'asc');
            $candidates = $this->db->get('t_candidates')->result();


            $table_data = "<h4> " . $this->registration_model->get_school($schoolid) . " </h4><hr/>";
            $table_data .= '<table class="table table-bordered table-condensed" id="example"> '
                      .'<thead>'
                      .'  <tr>'
                       .'   <th>Sn</th>'
                        .'   <th>Examno</th>'
                       .'   <th>Surname</th>'
                        .'  <th>Othernames</th>'
                        .'  <th>First Choice</th>'
                        .'  <th>Second Choice</th>   '
                        .'  <th>Third Choice</th>   '
                       .' </tr>'
                      .'</thead>'
                     .' <tbody>';

                if(count($candidates)){
                    $sn =0;
                    foreach ($candidates as $candidate) {
                         $table_data .= '<tr>';
                            $table_data .= '<td>' . ++$sn .'</td>';
                             $table_data .= '<input type="hidden" id="candidateid'.$sn.'" name="candidateid'.$sn.'" value="'.$candidate->candidateid.'" />';
                             $table_data .= '<td><input onblur="examPost(\''.$sn.'\')" type="text" id="examno'.$sn.'" name="examno'.$sn.'" value="'.$candidate->examno.'" style="border: 0px;" /><span id="exam-result'.$sn.'"></span></td>';
                            $table_data .= '<td><input onblur="firstnamePost(\''.$sn.'\')" type="text" id="firstname'.$sn.'" name="firstname'.$sn.'" value="' . (strtoupper($candidate->firstname)) .'" style="border: 0px;" /><span id="fname-result'.$sn.'"></span></td>';
                            $table_data .= '<td><input onblur="othernamePost(\''.$sn.'\')" type="text" id="othername'.$sn.'" name="othername'.$sn.'" value="' . (strtoupper($candidate->othernames)) .'" style="border: 0px;" /><span id="oname-result'.$sn.'"></span></td>';
                            $table_data .= '<td> '
                                    . '<select name = "" value = "" onchange = "updateFirstChoice(\''.$sn.'\')" id = "firstChoice'.$sn.'" class = "form-control">'
                                    . '<option value = ""> Select First Choice </option>';
                                    foreach($posting_schools as $school):
                                        $table_data.= '<option ';
                                        $table_data.= ($candidate->firstchoice == $school->schoolid) ? "selected = selected" : "";
                                        $table_data .= 'value ="'.$school->schoolid.'"'. '>'.$school->schoolname.'- ('.$school->schoolcode.')</option>';
                                    endforeach;
                          $table_data .= '</select> <span id="firstChoiceResult'.$sn.'"></span></td>';
                          //SECOND CHOICE
                            $table_data .= '<td> '
                                    . '<select name = "" value = "" onchange = "updateSecondChoice(\''.$sn.'\')" id = "secondChoice'.$sn.'" class = "form-control">'
                                    . '<option value = ""> Select Second Choice </option>';
                                    foreach($posting_schools as $school):
                                        $table_data.= '<option ';
                                        $table_data.= ($candidate->secondchoice == $school->schoolid) ? "selected = selected" : "";
                                        $table_data .= 'value ="'.$school->schoolid.'"'. '>'.$school->schoolname.'- ('.$school->schoolcode.')</option>';
                                    endforeach;
                          $table_data .= '</select> <span id="secondChoiceResult'.$sn.'"></span></td>';
                          //THIRD CHOICE
                            $table_data .= '<td> '
                                    . '<select name = "" value = "" onchange = "updateThirdChoice(\''.$sn.'\')" id = "thirdChoice'.$sn.'" class = "form-control">'
                                    . '<option value = ""> Select Third Choice </option>';
                                    foreach($posting_schools as $school):
                                        $table_data.= '<option ';
                                        $table_data.= ($candidate->thirdchoice == $school->schoolid) ? "selected = selected" : "";
                                        $table_data .= 'value ="'.$school->schoolid.'"'. '>'.$school->schoolname.'- ('.$school->schoolcode.')</option>';
                                    endforeach;
                          $table_data .= '</select> <span id="thirdChoiceResult'.$sn.'"></span></td>';
                         $table_data .= '</tr>';
                    }
                }

            $table_data .= '</tbody>'
                    . '</table>';

            echo $table_data;
        }
    }

    public function getPostingSchools($schoolid){

        //USE SCHOOLID TO GET ZONE ID
        $zoneid = $this->getZoneIDWithSchoolID($schoolid);
        if(isset($zoneid)){
            $this->db->where('zoneid',$zoneid);
        }
        $this->db->where('edcid',$this->edcid);
        $this->db->where('ispostingschool',1);
        $this->db->order_by('schoolname ASC');
        $data = $this->db->get('t_schools')->result();


        if(count($data)){
            return $data;
        }
        else{
            return false;
        }
    }

    function getZoneIDWithSchoolID($schoolid){
        $this->db->select('zoneid');
        $this->db->where('schoolid',$schoolid);
        $zoneid_data = $this->db->get('t_schools')->row();
        if(count($zoneid_data)){
            return $zoneid_data->zoneid;
        }
    }

    public function updateFirstChoice($firstChoice,$candidateid){
                if(isset($firstChoice) && isset($candidateid)){
                 $this->db->where('candidateid',$candidateid);
                 $updatedata = array('firstchoice'=>$firstChoice);
                 $this->db->set($updatedata);
                 $updated = $this->db->update('t_candidates',$data);
                 if($updated){
                   ?>
                    <img src="<?php echo base_url('resources/images/available.png');?>">
                <?php
                 }
                 else{
                     ?>
                    <img src="<?php echo base_url('resources/images/not-available.png');?>">
                    <?php

                 }
                }
        }
    public function updateSecondChoice($secondChoice,$candidateid){
                if(isset($secondChoice) && isset($candidateid)){
                 $this->db->where('candidateid',$candidateid);
                 $updatedata = array('secondchoice'=>$secondChoice);
                 $this->db->set($updatedata);
                 $updated = $this->db->update('t_candidates',$data);
                 if($updated){
                   ?>
                    <img src="<?php echo base_url('resources/images/available.png');?>">
                <?php
                 }
                 else{
                     ?>
                    <img src="<?php echo base_url('resources/images/not-available.png');?>">
                    <?php

                 }
                }
        }
    public function updateThirdChoice($thirdChoice,$candidateid){
                if(isset($thirdChoice) && isset($candidateid)){
                 $this->db->where('candidateid',$candidateid);
                 $updatedata = array('thirdchoice'=>$thirdChoice);
                 $this->db->set($updatedata);
                 $updated = $this->db->update('t_candidates',$data);
                 if($updated){
                   ?>
                    <img src="<?php echo base_url('resources/images/available.png');?>">
                <?php
                 }
                 else{
                     ?>
                    <img src="<?php echo base_url('resources/images/not-available.png');?>">
                    <?php

                 }
                }
        }



}

