<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Schooladmin extends Admin_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('admin/schools_model');
    }
    
    public function index(){
        
        $sql = "select t_schools.*, t_lgas.lganame from t_schools, t_lgas 
                where t_schools.lgaid = t_lgas.lgaid
                and t_schools.phone is not null 
                and t_schools.phone <> '' 
                and t_schools.edcid = '".$this->data['edc_detail']->edcid."'
                ";
        $data = $this->db->query($sql)->result();
        $this->data['schooladmins'] = $data;
                 
        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        
        
        $this->data['subview'] = 'admin/schooladmin_page';        
        $this->load->view('admin/template/_layout_main', $this->data); 
    }
}
