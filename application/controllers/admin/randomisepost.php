<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Randomisepost extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if (!in_array('Super_Administrator', $priviledges)) {
            show_error('YOU ARE NOT AUTHORISED TO PERFORM THIS OPERATION - KINDLY CONTACT EDEVPRO ADMINISTRATORS');
            return;
        }

        $this->load->model('admin/registration_model');
    }

    public function index() {
set_time_limit(0);
        $this->load->helper('array');
        $edcid = $this->data['edc_detail']->edcid;

        $this->db->where('edcid', $edcid);
        $this->db->where('hasposting', 1);
        $this->data['exams'] = $this->db->get('t_exams')->result();

        $this->data['candidates'] = array();

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        
        if ($this->form_validation->run() == TRUE) {

            $examid = $this->input->post('examid');
            $examyear = $this->input->post('examyear');


            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (trim($cutoff) == false) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('admin/randomisepost'));
            }
            $this->load->model('admin/report_model');

           /* $sql = "update t_candidates set placement = null where
                t_candidates.examid = '" . $examid . "'
                and t_candidates.examyear = '" . $examyear . "'
                and t_candidates.edcid = '" . $this->data["edc_detail"]->edcid . "' ";
            //logSql($sql, $this->edcid);
            $this->db->query($sql);*/

            $sql = "select t_candidates.candidateid, t_candidates.firstchoice, t_candidates.secondchoice
                from t_candidates inner join t_scores
                on t_candidates.candidateid = t_scores.candidateid
                where t_candidates.examid = t_scores.examid
                and t_candidates.examyear = t_scores.examyear
                and t_candidates.edcid = t_scores.edcid
                and t_candidates.examid = '" . $examid . "'
                and t_candidates.examyear = '" . $examyear . "'
                and t_candidates.edcid = '" . $this->data["edc_detail"]->edcid . "'
                group by t_candidates.candidateid, t_candidates.firstchoice, t_candidates.secondchoice
                having sum(t_scores.total_score) >= " . $cutoff;
            $candidates = $this->db->query($sql)->result();

            foreach ($candidates as $candidate) {
                $rand_pick = random_element(array('firstchoice', 'secondchoice'));
                $this->registration_model->save_update(array('placement' => $candidate->$rand_pick), $candidate->candidateid);
            }

            #PROCESS FOR REMAINING CANDIDATES NOT PLACED
            /*
            $sql = "select t_candidates.* from t_candidates inner join t_scores
                on t_candidates.candidateid = t_scores.candidateid
                where t_candidates.examid = t_scores.examid
                and t_candidates.examyear = t_scores.examyear
                and t_candidates.edcid = t_scores.edcid
                and
                ( t_candidates.placement not in
                    (
                        select schoolid from t_schools where ispostingschool = 1 and edcid = '" . $this->data["edc_detail"]->edcid . "'
                     )
                    or t_candidates.placement is null or t_candidates.placement = ''
                )
                and t_candidates.examid = '" . $examid . "'
                and t_candidates.examyear = '" . $examyear . "'
                and t_candidates.edcid = '" . $this->data["edc_detail"]->edcid . "'
                group by t_candidates.id
                having sum(t_scores.total_score) >= " . $cutoff;
            $candidates_not_placed = $this->db->query($sql)->result();

            $sql = "select * from t_schools "
                    . "where edcid = '" . $this->data["edc_detail"]->edcid . "' "
                    . "and ispostingschool = 1";
            $postingschool = $this->db->query($sql);

            foreach ($candidates_not_placed as $candidate) {
                $rand_pick = $postingschool->row(mt_rand(0, $postingschool->num_rows()));
                $this->registration_model->save_update(array('placement' => $rand_pick->schoolid), $candidate->candidateid);
            }
*/
            $this->session->set_flashdata('msg', 'POSTING PERFORMED SUCCESSFULLY');
            redirect(site_url('admin/randomisepost'));
        }

        $this->data['subview'] = 'admin/randomise_post_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function reset_posts() {

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {

            $examid = $this->input->get('examid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);
            $sql = "update t_candidates set placement = null
                where examid = '" . $examid . "'
                and examyear = '" . $examyear . "'
                and edcid = '" . $this->edcid . "' ";

            $this->db->query($sql);

            $this->session->set_flashdata('msg', 'ALL POSTING HAS BEEN RESET SUCCESSFULLY');
            $success_data['success'] = "Successfull";
            $success_data['err'] = "1";
            echo json_encode($success_data);
            return;
        }
    }

}
