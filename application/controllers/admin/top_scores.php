<?php

class Top_scores extends Admin_Controller {

    //put your code here

    function __construct() {
        parent::__construct();
    }


    public function index() {

		
		$res = $this->db->query("select t_candidates.examno,firstname,othernames,schoolname,zonename,sum( round((cast(ca_score as float) * 30) / 100 ) + round((cast(exam_score as float) * 70) / 100 )) as totalmarksobtained from t_scores 
		inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
		inner join t_schools on t_schools.schoolid = t_candidates.schoolid
		inner join t_zones on t_zones.zoneid = t_schools.zoneid
		where t_scores.subjectid IN (select subjectid from t_subjects where examid = '1bfbi2ejdp' and (subjectid != 'sfd4qrv0zm' and subjectid != 'i405jul3jj' and subjectid != '9bi99x5uj9' and subjectid != '0qilali57b')) 
		and t_scores.examid = '1bfbi2ejdp'
		and t_scores.edcid = 'pc09aas8c4'
		group by   t_candidates.examno,firstname,othernames,schoolname,zonename order by totalmarksobtained desc limit 30")->result();
	
	     
	    $this->data["students"] = $res;

	
		$this->data['subview'] = 'admin/reports/top_scores';
        $this->load->view('admin/template/_layout_main', $this->data);
	
       
    }
    
  

}
