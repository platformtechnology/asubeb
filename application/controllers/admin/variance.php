<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Variance extends Admin_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
         $priviledges = explode("^", $this->session->userdata('user_role'));
        if(!in_array('Statistics', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
        $this->load->model('admin/exam_model');
    }

    public function index(){
        $this->db->where('hasposting', 0);
        $this->data['exams'] = $this->exam_model->get_where(array('edcid'=>$this->data['edc_detail']->edcid));

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if($this->form_validation->run()){

            $examid = $this->input->post('examid');
            $examyear = $this->input->post('examyear');
            redirect(site_url('admin/variance/settings/'.$examid.'/'.$examyear));
        }


        $this->data['subview'] = 'admin/variance_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function settings($examid, $examyear){
        $examid || show_404(); $examyear || show_404();

        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $this->data['exam_detail'] = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;

        $this->load->model('admin/subjects_model');
        $this->data['subjects'] = $this->subjects_model->get_where(array('examid'=>$examid));

        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/morris/morris.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/raphael-min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/morris/morris.min.js') . '"></script>';
        
        $this->data['subview'] = 'admin/variance_settings_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function fetchVariance()
    {
//        if($this->_is_ajax()){
            $subjectid = $this->input->get('subjectid', TRUE);
            $examid = $this->input->get('examid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);

            $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $this->data['subject_detail'] = $this->db->where('subjectid', $subjectid)->get('t_subjects')->row();
            $this->data['examid'] = $examid;
            $this->data['examyear'] = $examyear;

            $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $this->db->where('subjectid', $subjectid);
            $this->db->where('examid', $examid);
            $this->db->where('examyear', $examyear);
            $this->db->order_by('minimum ASC');
            $this->data['remarks'] = $this->db->get('t_remarks')->result();

            $sql = "
                    select AVG(total_score) as mean, STDDEV(total_score) as stddeviation from t_scores
                    inner join t_candidates on t_scores.candidateid = t_candidates.candidateid
                    inner join t_registered_subjects on t_registered_subjects.candidateid=t_candidates.candidateid
                    where t_scores.examid = '".$examid."' and t_scores.examyear = '".$examyear."'
                    and t_scores.subjectid = '". $subjectid ."'
                    and t_registered_subjects.subjectid = '". $subjectid ."'
                    and t_candidates.examyear = '". $examyear ."'
                    and t_scores.edcid = '". $this->data['edc_detail']->edcid ."'
                ";
            $query = $this->db->query($sql)->row();

            $this->data['mean'] = round($query->mean, 2);
            $this->data['stddeviation'] = round($query->stddeviation, 2);

            $this->load->model('admin/remarks_model');
            $this->data['total_candidates_per_subject'] = $this->remarks_model->total_students_per_subject($subjectid, $examid, $examyear);
            $asyncpage = $this->load->view('admin/Async/subjectvariance_page', $this->data, true);
            echo $asyncpage;
//        }

    }

    public function fetchVariedRemark()
    {
        //if($this->_is_ajax()){
            $subjectid = $this->input->get('subjectid', TRUE);
            $examid = $this->input->get('examid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);
            $min = $this->input->get('min', TRUE);
            $max = $this->input->get('max', TRUE);

            $this->load->model('admin/remarks_model');

            $data = $this->remarks_model->get_stat_per_remark($subjectid, $examid, $examyear, $min, $max);
            $data = json_encode($data);
            echo $data;
        //}
    }

    function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

}
