<?php

class Gradesheet extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if(!in_array('Registration', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
        $this->load->model('admin/exam_model');
        $this->load->model('admin/registration_model');
        $this->load->model('web/school_model');

        $this->edcid = $this->data['edc_detail']->edcid;
    }

    public function index() {
        #GET EXAMS
        $this->data['exams'] = $this->exam_model->get_where(array('edcid'=>$this->edcid));
        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('schoolid', 'School', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if($this->form_validation->run()){
            $data = $this->registration_model->array_from_post(array('examid', 'examyear','schoolid'));
            redirect(site_url('admin/gradesheet/Print_gradesheet/'.$data['examid'].'/'.$data['examyear'].'/'.$data['schoolid']));
        }

        $this->data['subview'] = 'admin/gradesheet_exam_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function Print_gradesheet($examid,$examyear,$schoolid) {
            $this->data['examid'] = $examid;
            $this->data['examyear'] = $examyear;

            $this->db->where('edcid', $this->edcid);
            $this->db->where('issecondary', 1);
            if ($schoolid != 'allschools')
                $this->db->where('schoolid', $schoolid);
            $this->db->order_by('schoolcode', 'asc');
            $this->data['school_detail'] = $this->db->get('t_schools')->result();

            $this->data['exam_detail'] = $this->db->where('examid', $examid)->get('t_exams')->row();

//            $this->_performFilter($data);
//            $this->db->where('schoolid', $this->session->userdata('school_id'));
//            //$this->db->where('lgaid', $this->session->userdata('school_lga'));
//            $this->db->where('edcid', config_item('edcid'));
//            $this->db->order_by('examno', 'asc');
//            $this->data['registrants'] = $this->registration_model->get_all();
//
//            $this->_saveCandidatesInSession(); // save the resultset in session so that it can be exported to excel

            $this->data['exam_subjects'] = $this->db
                                            ->where('examid', $examid)
                                            ->where('examyear', $examyear)
                                            ->where('haspractical', 1)
                                            ->where('edcid', $this->edcid)
                                            ->order_by('priority', 'asc')
                                            ->get('t_subjects')->result();

//            $this->data['exam_candidates'] = $this->data['registrants'];
//            $this->data['count']= count($this->data['registrants']);


            //$this->db->where('edcid', config_item('edcid'));
             //$this->db->where('lgaid', $this->session->userdata('school_lga'));


             $this->data['title'] = "LIST OF ALL CANDIDATES FOR ".$this->registration_model->getExamName_From_Id($examid)." ".$examyear;

             //$this->data['subview'] = 'web/schools/school_candidate_filter_page';
            //$this->load->view('web/templates/_layout_main', $this->data);

            //$this->load->view('web/templates/layout_head', $this->data);


        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';

            $this->load->view('admin/gradesheet_filter_page', $this->data);
    }

        public function getSchools($examid){
      #GET EXAM DATA TO TOGGLE BETWEEN SECONDARY AND PRIMARY SCHOOLS
     $exam_data = $this->db->where('examid',$examid)->get('t_exams')->row();
     $this->db->where('issecondary',$exam_data->hassecschool);
     $this->db->where('edcid',$this->edcid);
     $this->db->order_by('schoolname ASC');
     $schools = $this->db->get('t_schools')->result();
     $schools_array = array();

     if(count($schools)) {
         foreach($schools as $school):
             $schools_array[$school->schoolid] = $school->schoolname;
         endforeach;
         echo form_dropdown('schoolid',$schools_array,set_value('schoolid'),'class = "form-control"');
     }


    }



}