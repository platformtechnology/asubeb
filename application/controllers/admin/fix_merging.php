<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of a_correct_edcid
 *
 * @author Maxwell
 */
class Fix_merging extends CI_Controller {

    //put your code here

    function __construct() {
        parent::__construct();
    }


    public function _getOnlineDb() {

        $config['hostname'] = "localhost";
        $config['username'] = "postgres";
        $config['password'] = "chameleon";
        $config['database'] = "edcs_online";
        $config['dbdriver'] = "postgre";
        $config['dbprefix'] = "";
        $config['pconnect'] = TRUE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";

        $db = $this->load->database($config, true);
        return $db;
    }
    
    public function mergescore() {

        $onlineDB = $this->_getOnlineDb();
        $edcid = '610zzzveoj';
        $examid = '2n11sray1c';
        $examyear = '2015';

        $offline_db_scores = $this->db
                ->where('edcid', $edcid)
                ->where('examid', $examid)
                ->where('examyear', $examyear)
                ->get('t_scores')
                ->result();

        $data = array();
        foreach ($offline_db_scores as $score) {
            
            $data[] = array(
                'scoreid' => $score->scoreid,
                'subjectid' => $score->subjectid,
                'exam_score' => $score->exam_score,
                'examid' => $score->examid,
                'examyear' => $score->examyear,
                'practical_score' => $score->practical_score,
                'total_score' => $score->total_score,
                'candidateid' => $score->candidateid,
                'edcid' => $score->edcid,
                'ca_score' => $score->ca_score,
                'datecreated' => date('Y-m-d H:i:s'),
                'datemodified' => date('Y-m-d H:i:s')
            );
        }

        #put the scores into the online db
        $onlineDB->insert_batch('t_scores', $data);

        //print_r(count($data));
        echo 'SCORES MERGED SUCCESSFULLY';
        exit;
    }


}
