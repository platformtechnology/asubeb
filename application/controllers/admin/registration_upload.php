<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."/third_party/PHPExcel.php";
 
class Registration_upload extends Admin_Controller {
    
    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Registration', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
        $this->load->model('admin/registration_model');
        $this->load->model('admin/scores_model');
        $this->load->model('admin/scores_margin_model');
//        $this->load->model('admin/subjects_model');
//        $this->load->model('admin/reg_subjects_model');

    }
    
    public function index() { 
        $edcid = $this->data['edc_detail']->edcid;
        
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();
        
        $this->db->where('edcid', $edcid);
        $this->db->order_by('schoolname');
        $this->data['schools'] = $this->db->get('t_schools')->result();
        
        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        $this->form_validation->set_rules('schoolid', 'School', 'trim|required');
        
        if($this->form_validation->run()){
            $this->load->helper('array');
            $this->benchmark->mark('code_start');
            
            $data = $this->registration_model->array_from_post(array('examid', 'examyear', 'schoolid'));
            
            $uploaded_data = $this->_upload_file();
            $reg_file = $uploaded_data['full_path'];
            if(file_exists($reg_file)){
                try{
                    $excelReader = new PHPExcel_Reader_Excel2007();
                    $excelObject = $excelReader->load($reg_file);
                    
                    $sheet = $excelObject->getSheet(0); 
                    $highestRow = $sheet->getHighestRow(); 
                    $highestColumn = $sheet->getHighestColumn();

                    $headerData = $sheet->rangeToArray('A1:'.$highestColumn.'1',
                                                        NULL,
                                                        TRUE,
                                                        FALSE);
                    
                    $headerData = $headerData[0];
                    
                    if(!in_array('NAME', $headerData)){
                        $this->session->set_flashdata('error', 'Invalid Excel Score Template Uploaded');
                        
                        @unlink($reg_file);
                        redirect(site_url('admin/registration_upload')); 
                        return;
                    }

                    //  Loop through each row of the worksheet in turn
                    $success = 0;
                    $examdata = $this->db->get_where('t_exams', array('examid'=>$data['examid']))->row();
                    $this->db->where('schoolid', $data['schoolid']);
                    $school_data = $this->db->get('t_schools')->row();
                        
                    for ($row = 2; $row <= $highestRow; $row++){ 
                       $candidatename = $sheet->getCell('A'.$row)->getValue();
                       $names = explode(' ', $candidatename, 2);
                       
                       $regdata = array();
                       $regdata['candidateid'] = $this->registration_model->generate_unique_id();
                       
                       // to avoid duplicate while synching/inserting
                       $this->registration_model->delete($regdata['candidateid']);
                       $sql  = "delete from t_registered_subjects where candidateid = '" . $regdata['candidateid'] . "'; ";
                       $sql .= "delete from t_scores where candidateid = '" . $regdata['candidateid'] . "'; ";
                       $this->db->query($sql); logSql($sql, $this->edcid);
                       
                       $regdata['examid'] = $data['examid'];
                       $regdata['examyear'] = $data['examyear'];
                       $regdata['schoolid'] = $data['schoolid'];
                       $regdata['centreid'] = $data['schoolid'];
                       $regdata['firstname'] = $names[0];
                       $regdata['othernames'] = $names[1];
                       $regdata['edcid'] = $this->data['edc_detail']->edcid;
                       $regdata['lgaid'] = $school_data->lgaid;
                       $regdata['zoneid'] = $school_data->zoneid;
                       $regdata['dob'] = '2004-10-10';
                       $regdata['gender'] = 'M';
                       $regdata['phone'] = '';
                       $regdata['examno'] = $this->_generate_examNumber($this->db->where('lgaid', $school_data->lgaid)->get('t_lgas')->row(), $data['schoolid'], $data['examid']);
                       $regdata['serial'] = $this->data['serial'];
                       
                       //Auto Assign First and Second Choice
                       if($examdata->hasposting == 1){
                            $this->db->where('issecondary', 1);
                            $this->db->where('ispostingschool', 1);
                            $this->db->where('edcid', $this->data['edc_detail']->edcid);
                            $schools = $this->db->get('t_schools');
                            $count_rows = count($schools->result());
                            $randval1 = mt_rand(0, $count_rows);
                            $randval2 = mt_rand(0, $count_rows);
                            
                            $row1 = $schools->row_array($randval1);
                            $row2= $schools->row_array($randval2);
                            
                            $regdata['firstchoice'] = $row1['schoolid'];
                            $regdata['secondchoice'] = $row2['schoolid'];
                            
                        }else{
                             $regdata['firstchoice'] = 0;
                             $regdata['secondchoice'] = 0;
                        }
                        
                        $this->_register_subjects_and_scores_for_candidate($regdata['candidateid'], $regdata['examid'], $regdata['examyear']);
                        $id = $this->registration_model->save_update($regdata);
                        if($id) ++$success;
                    }
                    //End looping through each record
                 
                    //Enter Audit Log For Registration Action
                    $this->auditLog($regdata, $examdata->hasposting);
                    //End Auidt Log Entering
                }
                catch(Exception $e){
                    $this->session->set_flashdata('error', $e->getMessage());
                    redirect(site_url('admin/registration_upload'));  
                }
                
                @unlink($reg_file);
                
            }//end if file_exist
            
            $this->benchmark->mark('code_end');
            
            $this->session->set_flashdata('total', $success);
            
            $this->session->set_flashdata('msg', $success . ' Registrants Processed Successfully in ' . $this->benchmark->elapsed_time('code_start', 'code_end') . ' Seconds');
            redirect(site_url('admin/registration_upload'));                    
            
        }//End form posted validation run
        
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        $this->data['subview'] = 'admin/registration_upload_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }
    
    public function _register_subjects_and_scores_for_candidate($candidateid, $examid, $examyear){
        $this->load->model('admin/reg_subjects_model');
        $this->load->model('admin/scores_model');
        
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $this->db->where('examid', $examid);
       // $this->db->where('examyear', $examyear);
        $all_subjects = $this->db->get('t_subjects')->result();
        
         foreach ($all_subjects as $subject) {
            $subj_data = array();
            $subj_data['candidateid'] = $candidateid;
            $subj_data['subjectid'] = $subject->subjectid;
            $subj_data['examid'] = $examid;
            $subj_data['examyear'] = $examyear;
            $subj_data['edcid'] = $this->data['edc_detail']->edcid;
            $this->reg_subjects_model->save_update($subj_data);

            //insert default Exam, CA and Practical scores for compulsory subjects
            $data = array();
            $data['scoreid'] = $this->scores_model->generate_unique_id();
            $data['subjectid'] = $subject->subjectid;
            $data['edcid'] = $this->data['edc_detail']->edcid;
            $data['examid'] = $examid;
            $data['examyear'] = $examyear;
            $data['candidateid'] = $candidateid;
            $this->scores_model->save_update($data);
        }
    }
    
    public function _upload_file() {
        $config = array(
             'upload_path' => './resources/',
             'allowed_types' => 'xls|xlsx',
             'overwrite' => TRUE,
             'remove_spaces' => TRUE
         );
        
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('regfile'))
        {
           $this->session->set_flashdata('error', $this->upload->display_errors());
           redirect(site_url('admin/registration_upload'));   
        } 

        return $this->upload->data();
    }
    
    public function _generate_examNumber($lga_data, $schoolid, $examid){
        
        $school_data = $this->db->get_where('t_schools', array('schoolid'=>$schoolid))->row();
        $activeyear = $this->data['activeyear'];
                
        if(count($lga_data)){
            $schoolCode = $school_data->schoolcode;
            $lgaCode = $lga_data->lgainitials;
            $serial_num = '';

            $this->db->order_by('serial', 'desc');
            $this->db->where('lgaid', $lga_data->lgaid);
            $this->db->where('schoolid', $schoolid);
            $this->db->where('examid', $examid);
            $this->db->where('examyear', $activeyear);
            $result_set = $this->db->get('t_candidates');
            $candidate_data = $result_set->first_row();
            if(count($candidate_data)){
                $serial = $candidate_data->serial;
                if(trim($serial) == false){
                   $serial_num = sprintf("%03s", 1);
                }else{
                    $serial_num = $serial + 1;
                    $serial_num = sprintf("%03s", $serial_num);
                }
            }else{
               $serial_num = sprintf("%03s", 1); 
            }

            $this->data['serial'] = $serial_num;
            $exam_num = $lgaCode.'/'.$schoolCode.'/'.$serial_num;

            return $exam_num;
        }else{
            $schoolCode = $school_data->schoolcode;
            $serial_num = '';

            $this->db->order_by('serial', 'desc');
            $this->db->where('schoolid', $schoolid);
            $this->db->where('examid', $examid);
             $this->db->where('examyear', $activeyear);
            $result_set = $this->db->get('t_candidates');
            $candidate_data = $result_set->first_row();
            if(count($candidate_data)){
                $serial = $candidate_data->serial;
                if(trim($serial) == false){
                   $serial_num = sprintf("%03s", 1);
                }else{
                    $serial_num = $serial + 1;
                    $serial_num = sprintf("%03s", $serial_num);
                }
            }else{
               $serial_num = sprintf("%03s", 1); 
            }

            $this->data['serial'] = $serial_num;
            $exam_num = $schoolCode.'/'.$serial_num;

            return $exam_num;
        }
    }
    
    public function auditLog($regdata, $isposting) {
        $this->load->model('admin/audittrail_model');
        $message = 'Candidates Information>>><br/>'
                . 'Candidate Name: '.ucwords($regdata['firstname'].' '.$regdata['othernames']).'<br/>'
                . 'DOB: '.$regdata['dob'].'<br/>'
                . 'Phone: '.$regdata['phone'].'<br/>'
                . 'Gender: '.$regdata['gender'].'.... etc<br/>';

        if($isposting){
           $message .= '<br/>FirstChoice: '.$this->registration_model->$regdata($regdata['firstchoice'] ).'<br/>';
           $message .= 'SecondChoice: '.$this->registration_model->$regdata($regdata['secondchoice'] ).'<br/>';
        }

       $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'ADD', 
               $this->session->userdata('user_name'), 'Added a new CANDIDATE for '.$this->registration_model->getExamName_From_Id($regdata['examid']).' '.$regdata['examyear'],
               $message, $this->data['edc_detail']->edcid);
    }
    
}

