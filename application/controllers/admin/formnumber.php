<?php

class Formnumber extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Formnumber_model');
    }

    public function index() {
        $rules = array(
            'start'=>array(
                'field'=>'serial_start',
                'label'=>'Starting Point for Serial Number',
                'rules'=>'numeric|trim|required'
            ),
            'end'=>array(
                'field'=>'serial_end',
                'label'=>'Ending Point for Serial Number',
                'rules'=>'numeric|trim|required'
            ),
        );
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == TRUE ) {
            $serial_start = $this->input->post('serial_start');
            $serial_end = $this->input->post('serial_end');

            $where = "id between '".$serial_start."' and '".$serial_end."'";
            $this->db->where('formnumber !=','NULL');
            $this->db->where($where);
            $formnumberdata = $this->db->get('t_form_numbers');

        $data = array();
        if(count($formnumberdata)){
            foreach($formnumberdata->result() as $formnumber){
                $data[] =  $formnumber;
            }
        }
        $this->data['formnumbers'] = $data;
            $this->load->view('admin/formnumber_output',$this->data);
        }
        else{
            $this->data['subview'] = 'admin/formnumber';
           $this->load->view('admin/template/_layout_main',$this->data);
        }
    }

}

