<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Lgas extends Admin_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
         $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Edc_Setup', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect(site_url('admin/login/logout'));
        }
        $this->load->model('admin/lgas_model');
    }
    
    public function save($lgaid = null){
        $validation_rules = $this->lgas_model->_rules;
        
        if($lgaid == null){
            $this->data['lga_detail'] = $this->lgas_model->is_new();
            $validation_rules['lga']['rules'] .= '|is_unique[t_lgas.lganame]';
        }else $this->data['lga_detail'] = $this->lgas_model->get_all($lgaid); 
        
        $this->form_validation->set_rules($validation_rules);
        
        if($this->form_validation->run() == TRUE){
          
           //get the posted values
            $data = $this->lgas_model->array_from_post(array('lganame', 'lgainitials', 'zoneid'));
            if($lgaid == NULL){
                $data['lgaid'] = $this->lgas_model->generate_unique_id();  //if its an insert, generated new id
                $this->lgas_model->delete($data['examid']); //to ensure no duplicate when inserting and when synching db
            }
            $data['edcid'] = $this->data['edc_detail']->edcid;
            
           //validate if the lganame is unique during update
           $this->_validateUpdateData($data['edcid'], $lgaid, $data);
            
            $this->lgas_model->save_update($data, $lgaid);
            
            $this->audittrail_model->log_audit($this->session->userdata('user_id'), ($lgaid == NULL ? 'ADD' : 'UPDATE'), 
                    $this->session->userdata('user_name'), $lgaid == NULL ? 'Added a new LOCAL GOVT AREA' : 'Updated Details of a LOCAL GOVT AREA',
                     'LGA Name: '.$data['lganame'].'<br/>LGA Initials: '.$data['lgainitials'], $this->data['edc_detail']->edcid);
            
            $this->session->set_flashdata('msg', $lgaid == NULL ? 'New LGA Added Successfully' : 'LGA Updated Successfully') ;
            redirect(site_url('admin/lgas/save/'));
        }  
        
        //for displaying all lgas
        $jointable = array('t_zones'=>'zoneid');
        $joinfields = array('zonename');
        $this->db->order_by('t_lgas.lganame');
        $this->db->where('t_lgas.edcid', $this->data['edc_detail']->edcid);
        $this->data['lgas'] = $this->lgas_model->get_join($jointable, $joinfields);
         
         //for displaying zones ddl
        $this->db->where('t_zones.edcid', $this->data['edc_detail']->edcid);
        $this->db->order_by('t_zones.zonename');
        $this->data['zones'] = $this->db->get('t_zones')->result();
        
        $this->data['subview'] = 'admin/lga_page';        
        $this->load->view('admin/template/_layout_main', $this->data); 
    }
    
    public function delete($lgaid){
        $this->load->model('admin/registration_model');
        $lgadata = $this->registration_model->get_lga($lgaid, true);
       $this->lgas_model->delete($lgaid);
       
        $this->db->where('lgaid', $lgaid);
        $this->db->delete('t_schools');
        logSql($this->db->last_query(), $this->edcid);          
                  
        $this->db->where('lgaid', $lgaid);
        $this->db->delete('t_candidates');
        logSql($this->db->last_query(), $this->edcid);          
                    
        $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'DELETE', 
                    $this->session->userdata('user_name'), 'DELETED details of a LOCAL GOVT AREA',
                    'LGA Name: '.$lgadata->lganame.'<br/>LGA Initials: '.$lgadata->lgainitials, $this->data['edc_detail']->edcid);
        
       $this->session->set_flashdata('msg', 'LGA Data Deleted Successfully');
        redirect(site_url('admin/lgas/save/'));    
    }
    
    public function _validateUpdateData($edcid, $lgaid, $data){
         if($lgaid != null){
            $this->db->where('edcid', $edcid);
            $this->db->where_not_in('lgaid', $lgaid);
            $result = $this->db->get_where('t_lgas', array('lganame' => $data['lganame']))->row();
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Lga Name Already Exists');
                 redirect(site_url('admin/lgas/save/'.$lgaid));
            }

             $this->db->where('edcid', $edcid);
            $this->db->where_not_in('lgaid', $lgaid);
            $result = $this->db->get_where('t_lgas', array('lgainitials' => $data['lgainitials']))->row();
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Lga Initials Already Exists');
                 redirect(site_url('admin/lgas/save/'.$lgaid));
            }
        }
    }
    
}
