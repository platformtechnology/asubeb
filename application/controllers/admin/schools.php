<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Schools extends Admin_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if (!in_array('Edc_Setup', $priviledges) && !in_array('Arb_School', $priviledges) && !in_array('Super_Administrator', $priviledges)) {
            redirect(site_url('admin/login/logout'));
        }
        $this->load->model('admin/schools_model');
    }

    public function arbitrary($schoolid = null) {
        $edcid = $this->data['edc_detail']->edcid;

        $this->form_validation->set_rules('lgaid', 'Local Govt', 'trim|required');
        $this->form_validation->set_rules('schoolname', 'School Name', 'trim|required');
        if ($this->form_validation->run() == TRUE) {
            $data = $this->schools_model->array_from_post(array('schoolname', 'lgaid', 'ispostingschool', 'genre'));
            $data['iscentre'] = 1;
            $data['schoolid'] = $this->schools_model->generate_unique_id();
            $data['isprimary'] = 0;
            $data['issecondary'] = 0;
            $data['edcid'] = $edcid;
            if (empty($data['ispostingschool']))
                $data['ispostingschool'] = 0;
            if ($data['genre'] == 'primary') {
                $data['isprimary'] = 1;
                $data['ispostingschool'] = 0;
                $query = "and isprimary = 1";
            } else {
                $data['issecondary'] = 1;
                $query = "and issecondary = 1";
            }

            //Get zone id
            $this->db->where('edcid', $edcid);
            $this->db->where('lgaid', $data['lgaid']);
            $lgadata = $this->db->get('t_lgas')->row();
            $data['zoneid'] = $lgadata->zoneid;

            //Generate School Code
            $sql = "select max(schoolcode) as maxcode from t_schools "
                    . "where edcid = '" . $edcid . "' "
                    . "and lgaid = '" . $data['lgaid'] . "' ";
            $sql .= $query;
            $schoolcode = $this->db->query($sql)->row()->maxcode;
            $schoolcode = intval($schoolcode) + 1;
            $schoolcode = sprintf("%04s", $schoolcode);
            $data['schoolcode'] = $schoolcode;

            //validate if the schoolname is unique during update
            $this->db->where('edcid', $edcid);
            $result = $this->schools_model->get_where(array('schoolname' => $data['schoolname'], 'lgaid' => $data['lgaid']), TRUE);
            if (count($result) > 0) {
                $this->session->set_flashdata('error', 'The School Already Exists In Selected LGA!');
                redirect(site_url('admin/dashboard'));
            }

            $genre = $data['genre'];
            unset($data['genre']);

            $this->schools_model->delete($data['schoolid']);
            $this->schools_model->save_update($data);


            if ($genre == 'primary')
                $this->session->set_flashdata('msg', 'New Primary School Added Successfully - NEW CODE: ' . $schoolcode);
            else
                $this->session->set_flashdata('msg', 'New Secondary School Added Successfully - NEW CODE: ' . $schoolcode);

            redirect(site_url('admin/dashboard'));
        }
    }

    public function save($schoolid = null) {
        $edcid = $this->data['edc_detail']->edcid;

        //for displaying all schools
        $this->db->where('t_schools.edcid', $edcid);
        $this->db->where('t_schools.isprimary', 1);
        $this->data['schools'] = $this->schools_model->get_all();

        $this->db->order_by('lganame');
        $this->data['lgs'] = $this->db->where('edcid', $edcid)->get('t_lgas')->result();

        if ($schoolid == null)
            $this->data['school_detail'] = $this->schools_model->is_new();
        else
            $this->data['school_detail'] = $this->schools_model->get_all($schoolid);

        $validation_rules = $this->schools_model->_rules;
        $this->form_validation->set_rules($validation_rules);
        if ($this->form_validation->run() == TRUE) {

            //get the posted values
            $data = $this->schools_model->array_from_post(array('schoolname', 'schoolcode', 'lgaid', 'zoneid', 'iscentre'));
            $data['zoneid'] = $this->_validateData($data);

            if (empty($data['iscentre']))
                $data['iscentre'] = 0;

            //if its an insert, generated new schoolid
            if ($schoolid == NULL) {
                $data['schoolid'] = $this->schools_model->generate_unique_id();
                $this->schools_model->delete($data['schoolid']);
            }

            $data['issecondary'] = 0;
            $data['isprimary'] = 1; //it is a primaryschool being setup
            $data['edcid'] = $edcid;

            //validate if the schoolname is unique during update
            $this->_validateSchool($data, $schoolid);

            $this->schools_model->save_update($data, $schoolid);

            $this->load->model('admin/registration_model');
            $message = 'School Name: ' . $data['schoolname'] . '<br/>'
                    . 'School Code: ' . $data['schoolcode'] . '<br/>'
                    . 'Lga: ' . $this->schools_model->get_lga_name($data['lgaid']) . '<br/>'
                    . 'Zone: ' . $this->schools_model->get_zone_name($data['zoneid']) . '<br/>';

            $this->audittrail_model->log_audit($this->session->userdata('user_id'), ($schoolid == NULL ? 'ADD' : 'UPDATE'), $this->session->userdata('user_name'), $schoolid == NULL ? 'Added a new PRIMARY SCHOOL with default scores' : 'Updated Details of a PRIMARY SCHOOL', $message, $this->data['edc_detail']->edcid);

            $this->session->set_flashdata('msg', $schoolid == NULL ? 'New School Added Successfully' : 'School Updated Successfully');
            redirect(site_url('admin/schools/save/'));
        }


        //for displaying zones ddl
        $this->db->where('t_zones.edcid', $edcid);
        $this->db->order_by('zonename');
        $this->data['zones'] = $this->db->get('t_zones')->result();

        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') . '" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';

        $this->data['subview'] = 'admin/schools_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function delete($schoolid) {
        $this->load->model('admin/registration_model');
        $schoolname = $this->registration_model->get_school($schoolid);
        $this->schools_model->delete($schoolid);

        $this->db->where('schoolid', $schoolid);
        $this->db->delete('t_candidates');
        logSql($this->db->last_query(), $this->edcid);

        $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'DELETE', $this->session->userdata('user_name'), 'DELETED a details of a PRIMARY SCHOOL', 'School Name: ' . $schoolname, $this->data['edc_detail']->edcid);

        $this->session->set_flashdata('msg', 'School Data Deleted Successfully');
        redirect(site_url('admin/schools/save/'));
    }

    public function _validateData($data) {
        if ($data['lgaid'] == 'undefined') {
            $this->session->set_flashdata('error', 'LGA Not Selected');
            redirect(site_url('admin/schools/save/'));
        }

        if (trim($data['lgaid']) == false && trim($data['zoneid']) == false) {
            $this->session->set_flashdata('error', 'You Must Select Either Lga or Zone or both');
            redirect(site_url('admin/schools/save/'));
        }

        if (trim($data['lgaid']) != false && trim($data['zoneid']) == false) {
            //if lga is selected and zone is not selected, 
            //then fetch the zoneid from the selected lga 
            // - remember all lga are under a zone

            $edcid = $this->data['edc_detail']->edcid;
            $this->db->where('edcid', $edcid);
            $this->db->where('lgaid', $data['lgaid']);
            $lgadata = $this->db->get('t_lgas')->row();

            return $lgadata->zoneid;
        }

        return $data['zoneid'];
    }

    public function _validateSchool($data, $schoolid) {
        $edcid = $this->data['edc_detail']->edcid;

        if ($schoolid == null) {
            $this->db->where('edcid', $edcid);
            $result = $this->schools_model->get_where(array('schoolname' => $data['schoolname'], 'lgaid' => $data['lgaid'], 'isprimary' => 1), TRUE);
            if (count($result) > 0) {
                $this->session->set_flashdata('error', 'The School Already Exists In Selected LGA!');
                redirect(site_url('admin/schools/save/' . $schoolid));
            }

            $this->db->where('edcid', $edcid);
            $result = $this->schools_model->get_where(array('schoolcode' => $data['schoolcode'], 'lgaid' => $data['lgaid'], 'isprimary' => 1), TRUE);
            if (count($result) > 0) {
                $this->session->set_flashdata('error', 'The School Code Already Exists In Selected LGA! - School Code Must Be Unique For Each LGA');
                redirect(site_url('admin/schools/save/' . $schoolid));
            }
        } else {
            $this->db->where('edcid', $edcid);
            $this->db->where_not_in('schoolid', $schoolid);
            $result = $this->schools_model->get_where(array('schoolname' => $data['schoolname'], 'lgaid' => $data['lgaid'], 'isprimary' => 1), TRUE);
            if (count($result) > 0) {
                $this->session->set_flashdata('error', 'The School Already Exists In Selected LGA!');
                redirect(site_url('admin/schools/save/' . $schoolid));
            }

            $this->db->where('edcid', $edcid);
            $this->db->where_not_in('schoolid', $schoolid);
            $result = $this->schools_model->get_where(array('schoolcode' => $data['schoolcode'], 'lgaid' => $data['lgaid'], 'isprimary' => 1), TRUE);
            if (count($result) > 0) {
                $this->session->set_flashdata('error', 'The School Code Already Exists In Selected LGA! - School Code Must Be Unique For Each LGA');
                redirect(site_url('admin/schools/save/' . $schoolid));
            }
        }
    }

    public function ajaxdrop() {
        if ($this->_is_ajax()) {

            $zoneid = $this->input->get('zoneid', TRUE);
            $edcid = $this->edcid;

            $data = array();

            //Get Lga Per Zone
            $data['lgas'] = $this->schools_model->get_lgas_from_zone($zoneid);

            //Get Schools Per Zone for displaying second dropdown
            $this->db->select('schoolid as lgaid, schoolname as lganame');
            $this->db->where('edcid', $edcid);
            $this->db->where('zoneid', $zoneid);
            $this->db->order_by('schoolname');
            $data['schools'] = $this->db->get('t_schools')->result();

            if (count($data['lgas'])) {
                echo json_encode($data);
            } else {
                $this->db->where('edcid', $edcid);
                $data['lgas'] = $this->db->get('t_lgas')->result();
                echo json_encode($data);
            }
        } else {
            echo json_encode(array());
        }
    }

    function _is_ajax() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

    public function printPrimSchool() {

        $this->db->where('t_schools.isprimary', 1);
        $this->db->where('t_schools.edcid', $this->data['edc_detail']->edcid);
        $this->db->order_by('t_schools.schoolname');
        $school_data = $this->schools_model->get_all();

        $table_data = '<table class="table table-bordered">';
        $table_data .= '<thead>';
        $table_data .= '<tr>';
        $table_data .= '<th>Sn</th>';
        $table_data .= '<th>School Name</th>';
        $table_data .= '<th>School Code</th>';
        $table_data .= '<th>Zone</th>';
        $table_data .= '<th>Local Government Area</th>';
        $table_data .= '</tr>';
        $table_data .= '</thead>';

        $table_data .= '<tbody>';
        if (count($school_data)) {
            $sn = 0;
            foreach ($school_data as $value) {
                $table_data .= '<tr>';
                $table_data .= '<td>' . ++$sn . '</td>';
                $table_data .= '<td>' . $value->schoolname . '</td>';
                $table_data .= '<td>' . $value->schoolcode . '</td>';
                $table_data .= '<td>' . $this->schools_model->get_zone_name($value->zoneid) . '</td>';
                $table_data .= '<td>' . $this->schools_model->get_lga_name($value->lgaid) . '</td>';

                $table_data .= '</tr>';
            }
        }
        $table_data .= '</tbody>';
        $table_data .= '</table>';

        $this->data['report'] = $table_data;
        $this->data['url'] = site_url('admin/schools/save');
        $this->data['title'] = 'PRIMARY SCHOOL LIST REPORT';

        $this->load->view('admin/print_page', $this->data);
    }

}
