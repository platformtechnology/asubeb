<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Optional extends Admin_Controller {
    
    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Edc_Setup', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect(site_url('admin/login/logout'));
        }
        $this->load->model('admin/optional_comp_model');
        //$this->load->model('admin/optional_arb_model');
    }
    
    public function index() { 
        $edcid = $this->data['edc_detail']->edcid;
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        
        if($this->form_validation->run()){
            
            $data = $this->optional_comp_model->array_from_post(array('examid', 'examyear'));
            $this->db->where('examid', $data['examid']);
            $exam_data= $this->db->get('t_exams')->row();
            if($exam_data->hasposting == 1){
                redirect(site_url('admin/remarks/cutoff/'.$data['examid'].'/'.$data['examyear']));
            }
            redirect(site_url('admin/optional/settings/'.$data['examid'].'/'.$data['examyear']));
            
        }
        
        $this->data['subview'] = 'admin/optional_exam_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }
    
    public function settings($examid, $examyear){
        $edcid = $this->data['edc_detail']->edcid;
        $examid || show_404(); $examyear || show_404();
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->load->model('admin/subjects_model');
        $this->load->model('admin/registration_model'); //for getting examname in view file
        
        $this->db->where('edcid', $edcid);
        $this->data['subjects'] = $this->subjects_model->get_where(array('examid'=>$examid));
        
        $this->db->where('edcid', $edcid);
        $this->data['optionals'] = $this->optional_comp_model->get_where(array('examid'=>$examid, 'examyear'=>$examyear));
        
        $this->form_validation->set_rules('subjectid', 'Compulsory Subject To Be Passed', 'required');
        if($this->form_validation->run() == true){
            $compulsory_subjects = $this->input->post('subjectid');
            $compulsory_pass_score = array_values(array_filter($this->input->post('passscore')));
            
            $arbitrary_number = $this->input->post('arbitrarynum');
            //$arbitrary_passscore = $this->input->post('arbitrarypassscore') ? $this->input->post('arbitrarypassscore') : '0';
            

                //$arbitrary_passscore = (intval($arbitrary_number) == 0 ? 0 : $arbitrary_passscore);
                
                $selected_subjects = count($compulsory_subjects);
                if($selected_subjects == count($compulsory_pass_score)){

                    //Save Compulsory Subjects and their passscores using one Criteria ID
                    //But first delete all
                    $sql =  "Delete from t_optional_subjects where examid = '".$examid."' and examyear = '".$examyear."'; ";
                    $this->db->query($sql);
                    logSql($sql, $this->edcid);
                    
                    $optional_id= $this->optional_comp_model->generate_unique_id();
                    $this->optional_comp_model->delete($optional_id); //to ensure no duplicate when inserting and when synching db
                    
                    for($i = 0; $i<$selected_subjects; $i++){
                        $data = array();
                        $data['optionalid'] = $optional_id;
                        $data['subjectid'] = $compulsory_subjects[$i];
                        $data['passscore'] = $compulsory_pass_score[$i];
                        $data['examid'] = $examid;
                        $data['examyear'] = $examyear;

                        $data['arbitrarynum'] = $arbitrary_number;
                        $data['arbitrarypassscore'] = empty($arbitrary_passscore) ? 0 : $arbitrary_passscore;

                        
                        $data['edcid'] = $this->data['edc_detail']->edcid;
                        
                        $this->optional_comp_model->save_update($data);
                    }

                   /* $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'UPDATE', 
                        $this->session->userdata('user_name'), 'Updated RESULT CRITERIA Details for '
                        .$this->registration_model->getExamName_From_Id($examid).' '.$examyear,
                        '', $this->data['edc_detail']->edcid);*/

                    $this->session->set_flashdata('msg', 'Criteria Saved Successfully');
                    redirect(site_url('admin/optional/settings/'.$examid.'/'.$examyear));
                
                }
                else{
                    $this->data['error'] = 'Each Selected Compulsory Subject Must Have A Pass Score';
                    $this->data['subview'] = 'admin/optional_settings_page';
                    $this->load->view('admin/template/_layout_main', $this->data);
                }
                
            //end check for invalid aribrary passscore entry
        }
        else{
            $this->data['subview'] = 'admin/optional_settings_page';
            $this->load->view('admin/template/_layout_main', $this->data);
        }
    }

    
}
