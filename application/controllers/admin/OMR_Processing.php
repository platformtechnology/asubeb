<?php

class OMR_Processing extends Admin_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
       
        $submitted = $this->input->post('submit');

        if (isset($submitted)) {

            if (isset($_FILES['omr_file'])) {
                $destination_folder_directory = $this->input->post('file_directory');
                $destination_folder_name = $this->input->post('folder_name');
                $destination_path = $destination_folder_directory.$destination_folder_name;
                if (!is_dir($destination_path))
                    mkdir ($destination_path);

                $filename = mt_rand(0000000000, 9999999999);
                $upload_path = './resources/omr_uploads/';
                if (!is_dir($upload_path))
                    mkdir($upload_path);

                if ($this->perform_upload($upload_path, $source_file_name = "omr_file", $destination_file_name = $filename)) {
                    $members = array();
                    $file_to_process = $upload_path.$filename.'.txt';
                    $file = fopen($file_to_process, 'r');
                    while (!feof($file)) {
                        $members[] = fgets($file);
                    }
                    $mathematics = "";
                    $quantitative = "";
                    $english = "";
                    $verbal = "";
                    if(count($members)){
                        foreach($members as $member){
                            $candidate_code = substr($member,0,10);
                            $mathematics .= $candidate_code.' '.substr($member,11,25).PHP_EOL;
                            $quantitative .= $candidate_code.' '.substr($member,36,25).PHP_EOL;
                            $english .= $candidate_code.' '.substr($member,61,25).PHP_EOL;
                            $verbal .= $candidate_code.' '.substr($member,86,25).PHP_EOL;
                        }

                        $save_maths = fopen($destination_path.'/MATHEMATICS.txt', 'w');
                        $save_quantitative = fopen($destination_path.'/QUANTITATIVE.txt', 'w');
                        $save_english = fopen($destination_path.'/ENGLISH.txt', 'w');
                        $save_verbal = fopen($destination_path.'/VERBAL.txt', 'w');

                        fwrite($save_maths, $mathematics);
                        fwrite($save_quantitative, $quantitative);
                        fwrite($save_english, $english);
                        fwrite($save_verbal,$verbal);
                        
                        @unlink($file_to_process);
                        $this->data['error_message'] = get_success($msg = "Files processed successfully.. Check the destination folder ");

                    }
                    else {
                        $this->data['error_message'] = get_error($msg = "An Error Occured  while processing, Please try again later");
                    }


                }
                else {
                    $this->data['error_message'] = get_error($msg = "An Error Occured, Please try again later");
                }
            }
            else {
                $this->data['error_message'] = get_error($msg = "No File Uploaded");
            }
        }
        $this->data['subview'] = 'admin/OMR_processing_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public
            function perform_upload($upload_path, $source_file_name, $destination_file_name) {
        #sourcefilename = the name you gave the uploaded file in the form
        #destinationfilename = the name you want to give the uploaded file in the destination
        #upload path = the directory you wish to upload the file to
        if (!empty($_FILES[$source_file_name]['name'])) {
            $config['file_name'] = $destination_file_name;
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'txt';
            $config['max_size'] = 10240;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload($source_file_name)) {
                $error = array('error' => $this->upload->display_errors());
                $this->data['message'] = get_error($error);
                return false;
            }
            else {
                #NOTHING WAS UPLOADED
                return true;
            }
        }
        else {
            return null;
        }
    }

}
