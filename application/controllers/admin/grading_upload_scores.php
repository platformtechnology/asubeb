<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."/third_party/PHPExcel.php";

class Grading_upload_scores extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if(!in_array('Grading', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
        $this->load->model('admin/registration_model');
        $this->load->model('admin/scores_model');
        $this->load->model('admin/scores_margin_model');
//        $this->load->model('admin/subjects_model');
//        $this->load->model('admin/reg_subjects_model');

    }

    public function index() {
        $edcid = $this->data['edc_detail']->edcid;

        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        $this->form_validation->set_rules('subjectid', 'Subject', 'trim|required');

        if($this->form_validation->run()){
            $this->load->helper('array');
            $this->benchmark->mark('code_start');

            $data = $this->registration_model->array_from_post(array('examid', 'examyear', 'subjectid', 'hastheory'));

            $uploaded_data = $this->_upload_omr();
            $omr_file = $uploaded_data['full_path'];
            if(file_exists($omr_file)){
                try{
                    $excelReader = new PHPExcel_Reader_Excel2007();
                    $excelObject = $excelReader->load($omr_file);

                    $sheet = $excelObject->getSheet(0);
                    $highestRow = $sheet->getHighestRow();
                    $highestRow = $highestRow-1;
                    $highestColumn = $sheet->getHighestColumn();

                    $headerRowNum = 1;
                    $headerData = $sheet->rangeToArray('B1:'.$highestColumn.'1',
                                                        NULL,
                                                        TRUE,
                                                        FALSE);
                    $headerData = $headerData[0];
                    //  print_r($headerData); exit;
                    if(!in_array('LG CODE.', $headerData)
                            || !in_array('SCHOOL CODE', $headerData)
                            || !in_array('STUDENT ID', $headerData)
                            || !in_array('Score', $headerData)){
                        @unlink($omr_file);
                        $this->session->set_flashdata('error', 'Inavalid Excel Score Template was Uploaded');
                        redirect(site_url('admin/grading_upload_scores'));
                    }


                    //  Loop through each row of the worksheet in turn
                    $notFoundList = array();
                    $notRegisteredList = array();
                    $invalidScoreList = array();

                    $success = 0;
                    $updateSql = '';

                    $score_data = array(); $exam_numbers = '';
                    for ($row = 2; $row <= $highestRow; $row++){
                        //  Read a row of data

                        $lgcode = $sheet->getCell('B'.$row)->getValue();
                        $schoolcode = $sheet->getCell('C'.$row)->getValue();
                        $studentid = $sheet->getCell('D'.$row)->getValue();

                        $examno = $lgcode.'/'.$schoolcode.'/'.$studentid;
                        $score = $sheet->getCell('F'.$row)->getValue();
                        $exam_score = $score;

                        $score_data[$examno] = $exam_score;
                        $exam_numbers .= "'" . $examno . "',";
                    }
                    #END looping through the Excel rows

                    if(!count($score_data)){
                        @unlink($omr_file);
                         $this->session->set_flashdata('error', 'No Data In Score Template Uploaded');
                        redirect(site_url('admin/grading_upload_scores'));
                    }

                    $exam_numbers = rtrim($exam_numbers, ",");

                    $sql = "select t_scores.id, t_scores.candidateid, t_candidates.examno, t_scores.exam_score,t_scores.theory_score,t_scores.omr_score,
                            t_scores.ca_score, t_scores.practical_score
                            from t_scores inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                            where t_candidates.examid = '".$data['examid']."'
                            and t_candidates.examyear = '".$data['examyear']."'
                            and t_scores.subjectid = '".$data['subjectid']."'
                            and t_candidates.examno in (".$exam_numbers.")";
                    $candidate_score_data = $this->db->query($sql)->result();
                    $update_array = array();
                    foreach ($candidate_score_data as $scoredata) {
                        $success++;
                        //OMR SCORE
                        $t_examScore = $score_data[$scoredata->examno];
                        //CHECK IF THEORY HAS BEEN GRADED
                        if($data['hastheory'] == 1){
                            //HACK FOR BECE ABIA
                            //THERE ARE 3 SCORES,
                            //1. THEORY SCORE ENTERED MANUALLY
                            //2. OMR SCORE COMING FROM EXCEL UPLOAD AND
                            //3. EXAM SCORE WHICH IS THEORY SCORE + OMR SCORE
                            if($scoredata->theory_score > 0){
                                $omr_score = (($score_data[$scoredata->examno] < 0) ? 0 : $score_data[$scoredata->examno]);
                                $t_examScore = (($scoredata->theory_score < 0) ? 0 : $scoredata->theory_score) + $omr_score;
                                $total_score = (($scoredata->ca_score < 0) ? 0 : $scoredata->ca_score) + $t_examScore + (($scoredata->practical_score < 0) ? 0 : $scoredata->practical_score);
                            }
                            else{
                                $omr_score = (($score_data[$scoredata->examno] < 0) ? 0 : $score_data[$scoredata->examno]);
                                $t_examScore = 0;
                                $total_score = 0;
                            }

                        }
                        else{
                            $total_score = (($scoredata->ca_score < 0) ? 0 : $scoredata->ca_score) + $t_examScore + (($scoredata->practical_score < 0) ? 0 : $scoredata->practical_score);
                        }
                        $update_array[] = array(
                            'id' => $scoredata->id,
                            'exam_score' => $t_examScore,
                            'omr_score' => $omr_score,
                            'total_score' => $total_score
                        );
                        $updateSql .= "update t_scores set
                            exam_score = ".$t_examScore.",
                            omr_score = ".$omr_score.",
                            total_score = $total_score
                            where id = '" . $scoredata->id . "';";
                    }


                    @unlink($omr_file);

                    //$this->db->update_batch('t_scores', $update_array, 'id');
                    //echo $this->db->last_query();
                    //exit;

                    //$this->session->set_flashdata('msg', $success . ' Scores Processed Successfully in ' . $this->benchmark->elapsed_time('code_start', 'code_end') . ' Seconds');
                    //redirect(site_url('admin/grading_upload_scores'));

                    #$this->load->helper('download');
                    #force_download($uploaded_data['full_path'].'.sql', $updateSql);

                    $db_connection = NULL;

                    $dbhost = $this->db->hostname;
                    $dbuser = $this->db->username;
                    $dbpass = $this->db->password;
                    $dbname = $this->db->database;

                    $connection_string = "host=" . $dbhost . " user=" . $dbuser . " password=" . $dbpass . " dbname=" . $dbname;
                    if (!($db_connection = pg_connect($connection_string))) {
                        $this->session->set_flashdata('error', "DB Connection Error - " . pg_errormessage($db_connection));
                        redirect(site_url('admin/grading_upload_scores'));
                        return;
                    }

                    if(pg_send_query($db_connection, $updateSql))pg_close($db_connection);
                }
                catch(Exception $e){
                    @unlink($omr_file);
                    $this->session->set_flashdata('error', $e->getMessage());
                    redirect(site_url('admin/grading_upload_scores'));
                }

                @unlink($omr_file);
            }//end if file_exist

            $this->benchmark->mark('code_end');

            #$this->session->set_flashdata('error', 'An Error Ocurred - Kindly retry later or contact admin');
            $this->session->set_flashdata('msg', $success . ' Scores Processed Successfully in ' . $this->benchmark->elapsed_time('code_start', 'code_end') . ' Seconds');
            redirect(site_url('admin/grading_upload_scores'));

        }//End form posted validation run

        $this->data['subview'] = 'admin/Grading/grading_upload_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function subjects_ajaxdrop(){
        if($this->_is_ajax()){
            $examid = $this->input->get('examid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);

            $this->db->select('subjectid as lgaid, subjectname as lganame');
            $this->db->where('examid', $examid);
            $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $query = $this->db->get('t_subjects');

            $data['lgas'] = array();
            if ($query->num_rows() > 0)
            {
              $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        }else{
            echo json_encode(array());
        }
    }

    public function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

    public function _upload_omr() {
        $config = array(
             'upload_path' => './resources/',
             'allowed_types' => 'xls|xlsx',
             'overwrite' => FALSE,
             'remove_spaces' => TRUE
         );

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('omr'))
        {
           $this->session->set_flashdata('error', $this->upload->display_errors());
           redirect(site_url('admin/grading_upload_scores'));
        }

        return $this->upload->data();
    }
}
