<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Schools_edit extends Admin_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
         $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Edc_Setup', $priviledges) && !in_array('Arb_School', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect(site_url('admin/login/logout'));
        }
        $this->load->model('admin/schools_model');
    }
    
        
    public function index(){
       $edcid = $this->data['edc_detail']->edcid;
       
       //for displaying all schools
        $this->db->where('t_schools.edcid', $edcid);
        $this->data['schools'] = $this->schools_model->get_all();
        
         //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        
        $this->data['subview'] = 'admin/schools_list_page';        
        $this->load->view('admin/template/_layout_main', $this->data); 
    }
    
   public function save($schoolid = null){
       $schoolid || show_404();
       
        $edcid = $this->data['edc_detail']->edcid;
        $this->data['school_detail'] = $schoolData = $this->schools_model->get_all($schoolid); 
        
        $this->form_validation->set_rules('schoolname', 'School Name', 'trim|required');
        if($this->form_validation->run() == TRUE){
          
           //get the posted values
            $data = $this->schools_model->array_from_post(array('schoolname', 'iscentre'));
                      
            if(empty($data['iscentre'])) $data['iscentre'] = 0;
     
           //validate if the schoolname is unique during update
            $this->db->where('edcid', $edcid);
            $this->db->where_not_in('schoolid', $schoolid);
            $result = $this->schools_model->get_where(array('schoolname' => $data['schoolname']), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Already Exists In Selected LGA!');
                  redirect(site_url('admin/schools_edit/save/'.$schoolid));
            }
            
            $this->schools_model->save_update($data, $schoolid);
          
            $this->session->set_flashdata('msg', 'School Updated Successfully') ;
            redirect(site_url('admin/schools_edit/'));
            
        }  
         
         //for displaying zones ddl
        $this->db->where('t_zones.edcid', $edcid);
        $this->data['zones'] = $this->db->get('t_zones')->result();
        
        $this->data['lgs'] = $this->db->where('edcid', $edcid)->get('t_lgas')->result();
        
        $this->data['subview'] = 'admin/schools_edit_page';        
        $this->load->view('admin/template/_layout_main', $this->data); 
    }
   
}
