<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Zones extends Admin_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
         $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Edc_Setup', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect(site_url('admin/login/logout'));
        }
        $this->load->model('admin/zones_model');
    }
    
    public function save($zoneid = null){
        
        $validation_rules = $this->zones_model->_rules;
        
        if($zoneid == null){
            $this->data['zone_detail'] = $this->zones_model->is_new();
            $validation_rules['zone']['rules'] .= '|is_unique[t_zones.zonename]';
        }else $this->data['zone_detail'] = $this->zones_model->get_all($zoneid); 
        
        $this->form_validation->set_rules($validation_rules);
        
        if($this->form_validation->run() == TRUE){
          
           //get the posted values
            $data = $this->zones_model->array_from_post(array('zonename'));
            if($zoneid == NULL){
                $data['zoneid'] = $this->zones_model->generate_unique_id();  //if its an insert, generated new categoryid
                 //prevent duplicate while synching DB
                $this->zones_model->delete($data['zoneid']);
            }
            $data['edcid'] = $this->data['edc_detail']->edcid;
            
           //validate if the zonename is unique during update
            if($zoneid != null){
                $this->db->where('edcid', $this->data['edc_detail']->edcid);
                $this->db->where_not_in('zoneid', $zoneid);
                $result = $this->db->get_where('t_zones', array('zonename' => $data['zonename']))->row();
                if(count($result)>0){
                     $this->session->set_flashdata('error', 'The Zone Name Already Exists');
                     redirect(site_url('admin/zones/save/'.$zoneid));
                }
            }
            
            $this->zones_model->save_update($data, $zoneid);
            
            $this->audittrail_model->log_audit($this->session->userdata('user_id'), ($zoneid == NULL ? 'ADD' : 'UPDATE'), 
                    $this->session->userdata('user_name'), $zoneid == NULL ? 'Added a new ZONE' : 'Updated ZONE Detail',
                    'Zone Name: '.$data['zonename'], $this->data['edc_detail']->edcid);
            
            $this->session->set_flashdata('msg', $zoneid == NULL ? 'New Zone Added Successfully' : 'Zone Updated Successfully') ;
            redirect(site_url('admin/zones/save/'));
        }  
        
        //for displaying all zones
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $this->db->order_by('zonename');
        $this->data['zones'] = $this->zones_model->get_all();
         
        $this->data['subview'] = 'admin/zones_page';        
        $this->load->view('admin/template/_layout_main', $this->data); 
    }
    
    public function delete($zoneid){
        
       $zone_detail =  $this->zones_model->get_all($zoneid, true);
       $this->zones_model->delete($zoneid);
       
       $this->db->where('zoneid', $zoneid);
       $ct = $this->db->delete('t_lgas');
       logSql($this->db->last_query(), $this->edcid);
               
       $this->db->where('zoneid', $zoneid);
        $this->db->delete('t_candidates');
        logSql($this->db->last_query(), $this->edcid);
        
       //save audit trail
          $audit = array();
          $audit['userid'] = $this->session->userdata('user_id');
          $audit['actiontype'] = 'DELETE';
          $audit['message'] = 'User ['.$this->session->userdata('user_name').'] DELETED a ZONE and Its LGAs';
          $audit['details'] = 'Zonename: '.$zone_detail->zonename .'<br/>'
                  . 'Associated LGAs: '.$ct;
          $audit['edcid'] = $this->data['edc_detail']->edcid;
          $this->audittrail_model->save_update($audit);
          //End Audit Entry
          
       $this->session->set_flashdata('umsg', 'Zone and All Associated LGA Deleted Successfully');
        redirect(site_url('admin/zones/save/'));    
    }
        
}
