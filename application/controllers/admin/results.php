<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Results extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if (!in_array('Report_Viewing', $priviledges) && !in_array('Super_Administrator', $priviledges)) {
            redirect(site_url('admin/login/logout'));
        }
        $this->edcid = $this->data['edc_detail']->edcid;

        $this->load->model('admin/registration_model');
        $this->load->model('admin/report_model');
        $this->load->model('admin/remarks_model');
        $this->load->model('admin/exam_model');

        //$this->data['sql'] = $this->load->database('sql', TRUE);
    }

    public function index() {

        $this->data['exams'] = $this->exam_model->get_where(array('edcid' => $this->edcid));

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if ($this->form_validation->run()) {
            $data = $this->exam_model->array_from_post(array('examid', 'examyear'));
            $exam_detail = $this->exam_model->get_all($data['examid']);

            redirect(site_url('admin/results/reporting/' . $data['examid'] . '/' . $data['examyear']));
        }

        $this->data['subview'] = 'admin/result2sms/result_to_sms_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }


    public function reporting($examid = null, $examyear = null) {
        $examid || show_404();
        $examyear || show_404();

        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;

        $this->db->where('edcid', $this->edcid);
        $this->db->order_by('lganame');
        $this->data['lgas'] = $this->db->get('t_lgas')->result();

        $this->data['zones'] = $this->db->get_where('t_zones', array('edcid' => $this->data['edc_detail']->edcid))->result();

        $this->data['reportdetail'] = new stdClass();
        $this->data['reportdetail']->zoneid = '';
        $this->data['reportdetail']->lgaid = '';

        $this->data['subview'] = 'admin/result2sms/report_select_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }


    public function dynamicreport($examid, $examyear) {
        $examid || show_404();
        $examyear || show_404();
        $this->form_validation->set_rules('reporttype', 'Report Type', 'trim|required');

        if ($this->form_validation->run() == true) {
            $excludefailure = $this->input->post('excludestatus');
            $separategender = $this->input->post('separategender');
            $this->data['page_number_start'] = $this->input->post('page_number_start');
            $this->data['excludefailure'] = 1;
            if (trim($excludefailure) == false)
                $this->data['excludefailure'] = 0;

            $this->data['separategender'] = 1;
            if ($separategender != 1)
                $this->data['separategender'] = 0;

            $data = $this->remarks_model->array_from_post(array('lgaid', 'zoneid', 'schoolid', 'reporttype'));
            $this->$data['reporttype']($examid, $examyear, $data['lgaid'], $data['schoolid'], (trim($data['zoneid']) == false) ? null : $data['zoneid']);
        }
    }

    //RAW SCORE SHEET REPORT
    private function SSR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'RAW SCORE SHEET REPORT';

        $cutoff = '';
        if ($examdetail->hasposting) {
            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('admin/reports/reporting/' . $examid . '/' . $examyear));
            }
        }

        $this->data['cutoff'] = $cutoff;
        $this->load->model('admin/reg_subjects_model');
        $this->load->model('admin/scores_model');

        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();
        $buffer_sql = '';
        if ($examdetail->haszone) {
            $buffer_sql = $this->_zonalfilter($zoneid, $schoolid);
        } else
            $buffer_sql = $this->_filter($lgaid, $schoolid);

        $st_param = 'total_score';

        #Get scores
        $sql = "select t_candidates.candidateid, t_candidates.schoolid, t_candidates.examno,
		(t_candidates.firstname || ' ' || t_candidates.othernames) as fullname, t_candidates.placement,
		t_candidates.gender, " . $st_param . " as tscore, t_scores.*
		from t_scores
		inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
		where t_candidates.examid = '" . $examid . "'
		" . $buffer_sql . "
		and t_candidates.examyear = '" . $examyear . "'
		and t_candidates.edcid = '" . $this->edcid . "'
		order by t_candidates.schoolid, t_candidates.examno";
        $query_data = $this->db->query($sql)->result();
        //        echo "<br/>";
        //        print_r($sort_array) . "<br/>";
        //        print_r($sortedarray);
        //        exit;


        $this->data['standard_report_scores'] = $query_data;


        $this->load->view('admin/reports/reports_raw_scoresheet_page', $this->data);
    }
    //STANDARD SCORE SHEET REPORT
    private function SSSR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {

        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'STANDARD SCORE SHEET REPORT';
        $cutoff = '';

        if ($examdetail->hasposting) {
            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('admin/results/reporting/' . $examid . '/' . $examyear));
            }
        }

        $this->data['cutoff'] = $cutoff;
        $this->load->model('admin/reg_subjects_model');
        $this->load->model('admin/scores_model');

        $this->db->where('edcid', $this->edcid);
        if($examyear == 2015)
            $this->db->where('examyear',$examyear);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();
        $buffer_sql = '';
        if ($examdetail->haszone) {
            $buffer_sql = $this->_zonalfilter($zoneid, $schoolid);
        } else
            $buffer_sql = $this->_filter($lgaid, $schoolid);

        $st_param = get_standardscore_rep($examid, $examyear);


        #Get scores
        $sql = "select t_candidates.candidateid, t_candidates.schoolid, t_candidates.examno,t_candidates.phone as phone_number,
				(t_candidates.firstname || ' ' || t_candidates.othernames) as fullname, t_candidates.placement,
				t_candidates.gender, " . $st_param . " as tscore, t_scores.*
                    from t_scores
                    inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                    where t_candidates.examid = '" . $examid . "'
                    " . $buffer_sql . "
                    and t_candidates.examyear = '" . $examyear . "'
                    and t_candidates.edcid = '" . $this->edcid . "'
                    order by t_candidates.schoolid, t_candidates.examno ";
        $this->data['query_data'] = $this->db->query($sql)->result();
        $this->load->view('admin/result2sms/reports_standard_scoresheet_page', $this->data);

    }
    private function _filter($lgaid, $schoolid, $isposting = false) {
        $this->load->model('admin/lgas_model');
        //ALL LGAS
        if (trim($lgaid) == false) {
            $joinTable = array('t_zones' => 'zoneid');
            $joinTableFields = array('zonename');
            $this->db->where('t_lgas.edcid', $this->edcid);
            $this->data['lgas'] = $this->lgas_model->get_join($joinTable, $joinTableFields);
            return '';
        }
        //ALL SCHOOLS IN ONE LGA
        else if ((trim($lgaid) != false) && (trim($schoolid) == false)) {
            $joinTable = array('t_zones' => 'zoneid');
            $joinTableFields = array('zonename');
            $this->db->where('t_lgas.lgaid', $lgaid);
            $this->db->where('t_lgas.edcid', $this->edcid);
            $this->data['lgas'] = $this->lgas_model->get_join($joinTable, $joinTableFields);

            return "and t_candidates.lgaid = '" . $lgaid . "' ";
        }
        //ONE SCHOOL IN ONE LGA
        else if ((trim($lgaid) != false) && (trim($schoolid) != false)) {
            $this->data['schoolid'] = $schoolid;
            $this->db->where('edcid', $this->edcid);
            if ($isposting)
                $this->db->where('ispostingschool', 1);
            $this->data['schooldetails'] = $this->db->where('schoolid', $schoolid)->get('t_schools')->result();

            $joinTable = array('t_zones' => 'zoneid');
            $joinTableFields = array('zonename');
            $this->db->where('t_lgas.lgaid', $lgaid);
            $this->db->where('t_lgas.edcid', $this->edcid);
            $this->data['lgas'] = $this->lgas_model->get_join($joinTable, $joinTableFields);

            return "and t_candidates.schoolid = '" . $schoolid . "' ";
        }
    }
    private function _zonalfilter($zoneid, $schoolid) {
        $this->load->model('admin/zones_model');
        //ALL ZONES
        if ((trim($zoneid) == false)) {
            $this->db->where('edcid', $this->edcid);
            $this->data['zones'] = $this->zones_model->get_all();
            return '';
        }
        //ALL SCHOOLS IN ONE ZONE
        else if ((trim($zoneid) != false) && (trim($schoolid) == false)) {
            $this->db->where('edcid', $this->edcid);
            $this->db->where('zoneid', $zoneid);
            $this->data['zones'] = $this->zones_model->get_all();
            return "and t_candidates.zoneid = '" . $zoneid . "' ";
        }
        //ONE SCHOOL IN ONE ZONE
        else if ((trim($zoneid) != false) && (trim($schoolid) != false)) {
            $this->data['schoolid'] = $schoolid;
            $this->db->where('edcid', $this->edcid);
            $this->data['schooldetails'] = $this->db->where('schoolid', $schoolid)->get('t_schools')->result();

            $this->db->where('edcid', $this->edcid);
            $this->db->where('zoneid', $zoneid);
            $this->data['zones'] = $this->zones_model->get_all();
            return "and t_candidates.schoolid = '" . $schoolid . "' ";
        }
    }
    public function school_ajaxdrop() {
        //if ($this->_is_ajax()) {
            $lgaid = $this->input->get('lgaid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);
            $examid = $this->input->get('examid', TRUE);
            $exam_detail = $this->exam_model->get_all($examid);
            $has_sec = $exam_detail->hassecschool;
            if ($has_sec == 1) {
                $sec = 1;
                $pri = 0;
            } else {
                $sec = 0;
                $pri = 1;
            }
            if ($lgaid != '' && !$exam_detail->hasposting) {
                $this->db->select('schoolid as lgaid, schoolname as lganame');
                $this->db->where('lgaid', $lgaid);
                $this->db->where('issecondary', $sec);
                $this->db->where('isprimary', $pri);
                $this->db->order_by('schoolname');
                $query = $this->db->get('t_schools');
            } elseif ($lgaid != '' && $exam_detail->hasposting) {
                $this->db->select('schoolid as lgaid, schoolname as lganame');
                $this->db->where('lgaid', $lgaid);
                $this->db->order_by('schoolname');
                $query = $this->db->get('t_schools');
            }

            $data['lgas'] = array();
            if ($query->num_rows() > 0) {
                $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        //} else {
        //    echo "Apparently is_ajax returned false!";
       //     show_error('This method can only be accessed internally.', 404);
       // }
    }
    public function school_ajaxdrop_zone() {
        if ($this->_is_ajax()) {

            $zoneid = $this->input->get('zoneid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);
            $examid = $this->input->get('examid', TRUE);


            $exam_detail = $this->exam_model->get_all($examid);

            $has_sec = $exam_detail->hassecschool;

            if ($has_sec) {
                $sec = 1;
                $pri = 0;
            } else {
                $sec = 0;
                $pri = 1;
            }
            $this->db->select('schoolid as lgaid, schoolname as lganame');
            $this->db->where('zoneid', $zoneid);
            $this->db->where('issecondary', $sec);
            $this->db->where('isprimary', $pri);
            $this->db->order_by('schoolname');
            $query = $this->db->get('t_schools');

            $data['lgas'] = array();
            if ($query->num_rows() > 0) {
                $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        } else {
            echo "Apparently is_ajax returned false!";
            show_error('This method can only be accessed internally.', 404);
        }
    }
    public function _is_ajax() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

}
