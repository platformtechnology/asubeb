<?php
class Website extends Admin_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if (!in_array('Edc_Setup', $priviledges) && !in_array('Arb_School', $priviledges) && !in_array('Super_Administrator', $priviledges)) {
            redirect(site_url('admin/login/logout'));
        }
        $this->load->model('admin/News_Model');
        $this->edcid = $this->data["edc_detail"]->edcid;
    }

    public function index($newsid = NULL,$delete = NULL){
        if($delete == NULL){
                if($newsid != NULL){
                    $this->data['news'] = $this->News_Model->get_where($where = array('newsid'=>$newsid), $single = TRUE);
                    $id = $this->data['news']->newsid;
                }
                else {
                    $id = NULL;
                    $this->data['news'] = $this->News_Model->is_new();
                }
                $this->form_validation->set_rules('newstitle', 'News Title', 'trim|alpha_numeric_spaces');
                $this->form_validation->set_rules('news', 'News Content', 'trim|required|alpha_numeric_spaces');
                if ($this->form_validation->run() == TRUE)
                    {
                    $data = $this->input->post();
                    $data['newsid'] = $this->News_Model->generate_unique_id($numeric = false, $len = 10);
                    $data['edcid'] = $this->edcid;
                    $saved = $this->News_Model->save_update($data, $id, $edcid);
                    if($saved){
                        //SUCCESS
                        $message = ($newsid == NULL) ? 'News Saved Successfully' : 'News Updated Successfully';
                        $this->session->set_flashdata('success',$message);
                        redirect('admin/website/index');
                    }
                    else {
                        //ERROR
                        $this->session->set_flashdata('error','An Error Occurred, Please try again later');
                        redirect('admin/website/index');
                    }
                }
        }
        else{

        }
        //EXISTING NEWS
        $where = array('edcid'=>$this->edcid);
        $this->data['existing_news'] = $this->News_Model->get_where($where, $single = FALSE);
        $this->data['subview'] = 'admin/website/index_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

}
