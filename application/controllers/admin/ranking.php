<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ranking extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if (!in_array('Statistics', $priviledges) && !in_array('Super_Administrator', $priviledges)) {
            redirect('login/logout');
        }
        $this->edcid = $this->data['edc_detail']->edcid;
        $this->load->model('admin/exam_model');
    }

    public function index() {

        $this->data['exams'] = $this->exam_model->get_where(array('edcid' => $this->edcid, 'hasposting'=>1));
        $this->data['subview'] = 'admin/ranking_select_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function ranks() {

        $this->load->model('admin/report_model');
        $this->load->model('admin/registration_model');

        $this->form_validation->set_rules('noranks', 'No Of Ranks', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');

        if ($this->form_validation->run() == true) {
		
            $data = $this->audittrail_model->array_from_post(array('examid', 'examyear', 'noranks', 'gender', 'ranktype'));

            $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($data['examid']);
			
			

            if($examdetail->hasposting){
                $cutoff = $this->db
                                ->get_where('t_cutoff', array('examid' => $data["examid"], 'examyear' => $data['examyear']))
                                ->row()
                        ->cutoff;
                if (trim($cutoff) == false) {
                    $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                    redirect(site_url('admin/ranking'));
                }
            }

            $this->data['cutoff'] = $cutoff;

            $sum_param = get_standardscore_rep($data['examid'], $data['examyear']);

            $standarddata = $this->db->get_where('t_standard_scores', array('examid' => $data['examid'], 'examyear' => $data['examyear'], 'edcid' => $this->edcid))->row();
            if (!count($standarddata)) {
                $standarddata = new stdClass();
                $standarddata->standardca = 0;
                $standarddata->standardexam = 0;
                $standarddata->standardpractical = 0;
            }
            $this->data['standarddata'] = $standarddata;

            if ($data['ranktype'] == 'Aranks') {
                $this->_getTopRanks($data, $sum_param, $cutoff);
                $this->_getLowRanks($data, $sum_param, $cutoff);
            } else if ($data['ranktype'] == 'Tranks')
                $this->_getTopRanks($data, $sum_param, $cutoff);
            else if ($data['ranktype'] == 'Lranks')
                $this->_getLowRanks($data, $sum_param, $cutoff);

            $this->data['examid'] = $data['examid'];
            $this->data['examyear'] = $data['examyear'];
            $this->data['title'] = 'RANKING REPORT';

            $this->load->model('admin/reg_subjects_model');
            $this->load->model('admin/scores_model');

            $this->data['allsubjects'] = $this->db
                    ->where(array('examid' => $data['examid']))
                    ->order_by('priority', 'asc')
                    ->get('t_subjects')
                    ->result();

            $this->load->view('admin/Reports/reports_ranking_page', $this->data);
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect(site_url('admin/ranking'));
        }
    }

    public function _getTopRanks($data, $sum_param, $cutoff) {

        $gender = '';
        if ($data['gender'] != 'X') {
            $gender = "and t_candidates.gender = '" . $data['gender'] . "'";
        }
        $topRankSql = "select * from 
                (select t_scores.candidateid, t_candidates.examno, t_candidates.firstname, t_candidates.othernames,
                    t_candidates.gender, t_candidates.schoolid, sum(" . $sum_param . ") as total 
                    from t_scores 
                    inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                    where t_scores.examid = '" . $data['examid'] . "' 
                    and t_scores.examyear = '" . $data['examyear'] . "' 
                    and t_scores.edcid = '" . $this->edcid . "' 
                    and t_scores.exam_score >= 0 
                    " . $gender . "
                    group by t_scores.candidateid, t_candidates.examno, t_candidates.firstname, t_candidates.othernames,
                    t_candidates.gender, t_candidates.schoolid order by total desc
                 ) as subquey1
                 where total >= " . $cutoff . "
                 limit " . $data['noranks'];
        $topranks = $this->db->query($topRankSql)->result();

        $this->data['ranks'] = array();
        $candidate_ids = '';
        foreach ($topranks as $trank) {
            $this->data['ranks'][] = $trank;
            $candidate_ids .= "'" . $trank->candidateid . "',";
            if (count($this->data['ranks']) == $data['noranks'])
                break;
        }
        $candidate_ids = rtrim($candidate_ids, ",");

        $trankSql = "select * from t_scores 
                    where t_scores.examid = '" . $data['examid'] . "' 
                    and examyear = '" . $data['examyear'] . "' 
                    and edcid = '" . $this->edcid . "' 
                    and candidateid in (" . $candidate_ids . ")";
        $this->data['toprank_scores'] = $this->db->query($trankSql)->result();
    }

    public function _getLowRanks($data, $sum_param, $cutoff) {
        $gender = '';
        if ($data['gender'] != 'X') {
            $gender = "and t_candidates.gender = '" . $data['gender'] . "'";
        }
        $LowRankSql = "select * from 
                (select t_scores.candidateid, t_candidates.examno, t_candidates.firstname, t_candidates.othernames,
                    t_candidates.gender, t_candidates.schoolid, sum(" . $sum_param . ") as total 
                    from t_scores 
                    inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                    where t_scores.examid = '" . $data['examid'] . "' 
                    and t_scores.examyear = '" . $data['examyear'] . "' 
                    and t_scores.edcid = '" . $this->edcid . "' 
                    and t_scores.exam_score >= 0 
                    " . $gender . "
                    group by t_scores.candidateid, t_candidates.examno, t_candidates.firstname, t_candidates.othernames,
                    t_candidates.gender, t_candidates.schoolid order by total desc
                 ) as subquey1
                 where total > 0 and total < " . $cutoff . "
                 limit " . $data['noranks'];

        //echo $LowRankSql; exit;
        $lowranks = $this->db->query($LowRankSql)->result();
        $this->data['lowranks'] = array();

        $candidate_ids = '';
        foreach ($lowranks as $lrank) {
            $candidate_ids .= "'" . $lrank->candidateid . "',";
            $this->data['lowranks'][] = $lrank;
            if (count($this->data['lowranks']) == $data['noranks'])
                break;
        }
        $candidate_ids = rtrim($candidate_ids, ",");
        $lrankSql = "select * from t_scores 
                    where t_scores.examid = '" . $data['examid'] . "' 
                    and examyear = '" . $data['examyear'] . "' 
                    and edcid = '" . $this->edcid . "' 
                    and candidateid in (" . $candidate_ids . ")";
        $this->data['lrank_scores'] = $this->db->query($lrankSql)->result();
    }

}
