<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Standardscore extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if (!in_array('Edc_Setup', $priviledges) && !in_array('Super_Administrator', $priviledges)) {
            redirect(site_url('admin/login/logout'));
        }
      
        $this->load->model('admin/standardscore_model');
    }

    public function index() {

        $this->load->model('admin/exam_model');

        $this->data['exams'] = $this->exam_model->get_where(array('edcid' => $this->edcid));

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if ($this->form_validation->run()) {
            $data = $this->exam_model->array_from_post(array('examid', 'examyear'));
            $this->data['examdetail'] = $exam_data = $this->exam_model->get_all($data['examid'], true);
            $this->data['examyear'] = $data['examyear'];

            $this->db->where('examid', $data['examid']);
            $this->db->where('examyear', $data['examyear']);
            $this->db->where('edcid', $this->edcid);
            $this->data['standardscoredetail'] = $standardised_data = $this->standardscore_model->get_all(null, true);
            if (!count($standardised_data)) {
                $this->data['standardscoredetail'] = $this->standardscore_model->is_new();
            }

            #check if the standard scores has been posted
            if (isset($_POST['standardca'])) {
                #first time to insert
                $scoredata = $this->standardscore_model->array_from_post(array('standardca', 'standardexam', 'standardpractical', 'examid', 'examyear'));
                $scoredata['edcid'] = $this->edcid;
                
                if (!count($standardised_data)) {
                    $this->standardscore_model->save_update($scoredata);
                }else $this->standardscore_model->save_update($scoredata, $standardised_data->id);
                
                 $this->data['success'] = 'Update Applied Successfully';
            }
            
            $this->db->where('examid', $data['examid']);
            $this->db->where('examyear', $data['examyear']);
            $this->db->where('edcid', $this->edcid);
            $this->data['standardscoredetail'] = $standardised_data = $this->standardscore_model->get_all(null, true);
            if (!count($standardised_data)) {
                $this->data['standardscoredetail'] = $this->standardscore_model->is_new();
            }
        }

        $this->data['subview'] = 'admin/standardscore_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }
    
}
