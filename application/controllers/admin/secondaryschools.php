<?php
class Secondaryschools extends Admin_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
         $priviledges = explode("^", $this->session->userdata('user_role'));
        if(!in_array('Edc_Setup', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect(site_url('admin/login/logout'));
        }
        $this->load->model('admin/schools_model');
    }

    public function save($schoolid = null){

        $edcid = $this->data['edc_detail']->edcid;

        if($schoolid == null) $this->data['school_detail'] = $this->schools_model->is_new();
        else $this->data['school_detail'] = $this->schools_model->get_all($schoolid);


        $validation_rules = $this->schools_model->_rules;
        $this->form_validation->set_rules($validation_rules);

        if($this->form_validation->run() == TRUE){

           //get the posted values
            $data = $this->schools_model->array_from_post(array('schoolname', 'schoolcode', 'lgaid', 'iscentre', 'zoneid', 'ispostingschool'));
            $data['zoneid'] = $this->_validateData($data, $edcid);

            if(empty($data['iscentre'])) $data['iscentre'] = 0;
            if(empty($data['ispostingschool'])) $data['ispostingschool'] = 0;

            //if its an insert, generated new categoryid
            if($schoolid == NULL) {
                $data['schoolid'] = $this->schools_model->generate_unique_id();
                //prevent duplicate while synching DB
                $this->schools_model->delete($data['schoolid']);
            }

            $data['isprimary'] = 0;
            $data['issecondary'] = 1; //it is a secondary school being setup
            $data['edcid'] = $edcid;

           //validate if the schoolname is unique during update
           $this->_validateSchool($data, $schoolid);

            $this->schools_model->save_update($data, $schoolid);

            $this->load->model('admin/registration_model');
            $message = 'School Name: '.$data['schoolname'].'<br/>'
                    . 'School Code: '.$data['schoolcode'].'<br/>'
                     . 'Lga: '.$this->schools_model->get_lga_name($data['lgaid']).'<br/>'
                    . 'Zone: '.$this->schools_model->get_zone_name($data['zoneid']).'<br/>';
            $this->audittrail_model->log_audit($this->session->userdata('user_id'), ($schoolid == NULL ? 'ADD' : 'UPDATE'),
                    $this->session->userdata('user_name'), $schoolid == NULL ? 'Added a new SECONDARY SCHOOL with default scores' : 'Updated Details of a SECONDARY SCHOOL',
                    $message, $this->data['edc_detail']->edcid);

            $this->session->set_flashdata('msg', $schoolid == NULL ? 'New Secondary School Added Successfully' : 'School Updated Successfully') ;
            redirect(site_url('admin/secondaryschools/save/'));
        }

         //for displaying all schools
        $this->db->where('t_schools.edcid', $edcid);
        $this->db->where('t_schools.issecondary', 1);
        $this->data['schools'] = $this->schools_model->get_all();

        $this->db->where('edcid', $edcid);
        $this->db->order_by('lganame');
        $this->data['lgs'] = $this->db->get('t_lgas')->result();

         //for displaying zones ddl
        $this->db->where('t_zones.edcid', $edcid);
        $this->db->order_by('zonename');
        $this->data['zones'] = $this->db->get('t_zones')->result();

         //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';

        $this->data['subview'] = 'admin/secondaryschool_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }
    public function updatePostingSchools($schoolid = null){

        $edcid = $this->data['edc_detail']->edcid;

        if($schoolid == null) $this->data['school_detail'] = $this->schools_model->is_new();
        else $this->data['school_detail'] = $this->schools_model->get_all($schoolid);


        $validation_rules = $this->schools_model->_rules;
        $this->form_validation->set_rules($validation_rules);

        if($this->form_validation->run() == TRUE){

           //get the posted values
            $data = $this->schools_model->array_from_post(array('schoolname', 'schoolcode', 'lgaid', 'iscentre', 'zoneid', 'ispostingschool'));
            $data['zoneid'] = $this->_validateData($data, $edcid);

            if(empty($data['iscentre'])) $data['iscentre'] = 0;
            if(empty($data['ispostingschool'])) $data['ispostingschool'] = 0;

            //if its an insert, generated new categoryid
            if($schoolid == NULL) {
                $data['schoolid'] = $this->schools_model->generate_unique_id();
                //prevent duplicate while synching DB
                $this->schools_model->delete($data['schoolid']);
            }

            $data['isprimary'] = 0;
            $data['issecondary'] = 1; //it is a secondary school being setup
            $data['edcid'] = $edcid;

           //validate if the schoolname is unique during update
           $this->_validateSchool($data, $schoolid);

            $this->schools_model->save_update($data, $schoolid);

            $this->load->model('admin/registration_model');
            $message = 'School Name: '.$data['schoolname'].'<br/>'
                    . 'School Code: '.$data['schoolcode'].'<br/>'
                     . 'Lga: '.$this->schools_model->get_lga_name($data['lgaid']).'<br/>'
                    . 'Zone: '.$this->schools_model->get_zone_name($data['zoneid']).'<br/>';
            $this->audittrail_model->log_audit($this->session->userdata('user_id'), ($schoolid == NULL ? 'ADD' : 'UPDATE'),
                    $this->session->userdata('user_name'), $schoolid == NULL ? 'Added a new SECONDARY SCHOOL with default scores' : 'Updated Details of a SECONDARY SCHOOL',
                    $message, $this->data['edc_detail']->edcid);

            $this->session->set_flashdata('msg', $schoolid == NULL ? 'New Secondary School Added Successfully' : 'School Updated Successfully') ;
            redirect(site_url('admin/secondaryschools/save/'));
        }

         //for displaying all schools
        $this->db->select('t_schools.*,t_zones.zonename');
        $this->db->join('t_zones','t_zones.zoneid = t_schools.zoneid','inner');
        $this->db->where('t_schools.edcid', $edcid);
        $this->db->where('t_schools.issecondary', 1);
        $this->db->order_by('t_zones.zonename ASC,t_schools.schoolname ASC');
        $this->data['schools'] = $this->schools_model->get_all();



         //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';

        $this->data['subview'] = 'admin/update_postingschool_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function delete($schoolid){
        $this->load->model('admin/registration_model');
        $schoolname = $this->registration_model->get_school($schoolid);
        $this->schools_model->delete($schoolid);

         $this->db->where('schoolid', $schoolid);
        $this->db->delete('t_candidates');
        logSql($this->db->last_query(), $this->edcid);

        $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'DELETE',
                    $this->session->userdata('user_name'), 'DELETED a details of a SECONDARY SCHOOL',
                    'School Name: '.$schoolname, $this->data['edc_detail']->edcid);
       $this->session->set_flashdata('msg', 'School Data Deleted Successfully');
        redirect(site_url('admin/secondaryschools/save/'));
    }

    public function _validateData($data) {
        if($data['lgaid'] == 'undefined'){
            $this->session->set_flashdata('error', 'LGA Not Selected') ;
            redirect(site_url('admin/schools/save/'));
        }

        if(trim($data['lgaid']) == false && trim($data['zoneid']) == false){
             $this->session->set_flashdata('error', 'You Must Select Either Lga or Zone or both') ;
             redirect(site_url('admin/schools/save/'));
        }

        if(trim($data['lgaid']) != false && trim($data['zoneid']) == false){
            //if lga is selected and zone is not selected,
            //then fetch the zoneid from the selected lga
            // - remember all lga are under a zone

            $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $this->db->where('lgaid', $data['lgaid']);
            $lgadata = $this->db->get('t_lgas')->row();

            return $lgadata->zoneid;
        }

        return $data['zoneid'];
    }

    public function _validateSchool($data, $schoolid){
        if($schoolid == null){
             $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $result = $this->schools_model->get_where(array('schoolname' => $data['schoolname'], 'lgaid' => $data['lgaid'], 'issecondary' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Already Exists In Selected LGA!');
                  redirect(site_url('admin/secondaryschools/save/'.$schoolid));
            }

             $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $result = $this->schools_model->get_where(array('schoolcode' => $data['schoolcode'], 'lgaid' => $data['lgaid'], 'issecondary' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Code Already Exists In Selected LGA! - School Code Must Be Unique For Each LGA');
                  redirect(site_url('admin/secondaryschools/save/'.$schoolid));
            }
        }else{
            $this->db->where_not_in('schoolid', $schoolid);
            $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $result = $this->schools_model->get_where(array('schoolname' => $data['schoolname'], 'lgaid' => $data['lgaid'], 'issecondary' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Already Exists In Selected LGA!');
                  redirect(site_url('admin/secondaryschools/save/'.$schoolid));
            }

             $this->db->where_not_in('schoolid', $schoolid);
              $this->db->where('edcid', $this->data['edc_detail']->edcid);
             $result = $this->schools_model->get_where(array('schoolcode' => $data['schoolcode'], 'lgaid' => $data['lgaid'], 'issecondary' => 1), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The School Code Already Exists In Selected LGA! - School Code Must Be Unique For Each LGA');
                  redirect(site_url('admin/secondaryschools/save/'.$schoolid));
            }
        }
    }

    public function ajaxdrop()
    {
        if($this->_is_ajax()){

            $data = array();
            $zoneid = $this->input->get('zoneid', TRUE);
            $data['lgas'] = $this->schools_model->get_lgas_from_zone($zoneid);
            echo json_encode($data);
            //return $data;
        }else{
             echo json_encode(array());
        }
    }
    function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

    public function printSecSchool() {
        $this->db->join('t_zones','t_schools.zoneid = t_zones.zoneid','inner');
        $this->db->join('t_lgas','t_schools.lgaid = t_lgas.lgaid','left');
        $this->db->where('t_schools.issecondary', 1);
        $this->db->where('t_schools.edcid', $this->data['edc_detail']->edcid);
        $this->db->order_by('t_zones.zonename ASC,t_lgas.lganame,t_schools.schoolname ASC');
        $school_data = $this->schools_model->get_all();

        $table_data = '<table class="table table-bordered">';
        $table_data .= '<thead>';
             $table_data .= '<tr>';
                $table_data .= '<th>Sn</th>';
                $table_data .= '<th>School Name</th>';
                $table_data .= '<th>School Code</th>';
                $table_data .= '<th>Zone</th>';
                $table_data .= '<th>Local Government Area</th>';

            $table_data .= '</tr>';
        $table_data .= '</thead>';

        $table_data .= '<tbody>';
            if(count($school_data)){
                $sn=0;
                foreach ($school_data as $value) {
                    $table_data .= '<tr>';
                        $table_data .= '<td>'.++$sn.'</td>';
                        $table_data .= '<td>'.strtoupper($value->schoolname).'</td>';
                        $table_data .= '<td>'.$value->schoolcode.'</td>';
                        $table_data .= '<td>'.$value->zonename.'</td>';
                        $table_data .= '<td>'.$value->lganame.'</td>';
                    $table_data .= '</tr>';
                }
            }
        $table_data .= '</tbody>';
        $table_data .= '</table>';

        $this->data['report'] = $table_data;
        $this->data['url'] = site_url('admin/secondaryschools/save');
        $this->data['title'] = 'SECONDARY SCHOOL LIST REPORT';

        $this->load->view('admin/print_page', $this->data);
    }
    public function printSecSchoolForChecking() {
        $this->db->join('t_zones','t_schools.zoneid = t_zones.zoneid','inner');
        $this->db->join('t_lgas','t_schools.lgaid = t_lgas.lgaid','left');
        $this->db->where('t_schools.issecondary', 1);
        $this->db->where('t_schools.edcid', $this->data['edc_detail']->edcid);
        $this->db->order_by('t_zones.zonename ASC,t_lgas.lganame,t_schools.schoolname ASC');
        $school_data = $this->schools_model->get_all();

        $table_data = '<table class="table table-bordered">';
        $table_data .= '<thead>';
             $table_data .= '<tr>';
                $table_data .= '<th>Sn</th>';
                $table_data .= '<th>School Name</th>';
                $table_data .= '<th>School Code</th>';
                $table_data .= '<th>Zone</th>';
                $table_data .= '<th>No of Candidates</th>';
                $table_data .= '<th>Entered By</th>';
                $table_data .= '<th>Checked By</th>';
                $table_data .= '<th>QC Passed</th>';

            $table_data .= '</tr>';
        $table_data .= '</thead>';

        $table_data .= '<tbody>';
            if(count($school_data)){
                $sn=0;
                foreach ($school_data as $value) {
                    $table_data .= '<tr>';
                        $table_data .= '<td>'.++$sn.'</td>';
                        $table_data .= '<td>'.strtoupper($value->schoolname).'</td>';
                        $table_data .= '<td>'.$value->schoolcode.'</td>';
                        $table_data .= '<td>'.$value->zonename.'</td>';
                        $table_data .= '<td></td>';
                        $table_data .= '<td></td>';
                        $table_data .= '<td></td>';
                        $table_data .= '<td></td>';
                    $table_data .= '</tr>';
                }
            }
        $table_data .= '</tbody>';
        $table_data .= '</table>';

        $this->data['report'] = $table_data;
        $this->data['url'] = site_url('admin/secondaryschools/save');
        $this->data['title'] = 'SECONDARY SCHOOL LIST REPORT';

        $this->load->view('admin/print_page', $this->data);
    }

    public function printSecSchool_posting() {
        $this->db->select('t_schools.*,t_lgas.lganame');
        $this->db->join('t_lgas','t_lgas.lgaid = t_schools.lgaid','inner');
        $this->db->where('t_schools.issecondary', 1);
        $this->db->where('t_schools.ispostingschool', 1);
        $this->db->where('t_schools.edcid', $this->data['edc_detail']->edcid);
        $this->db->order_by('t_lgas.lganame ASC,t_schools.schoolname ASC');
        $school_data = $this->db->get('t_schools')->result();

        $table_data = '<table class="table table-bordered">';
        $table_data .= '<thead>';
            $table_data .= '<tr>';
                $table_data .= '<th>Sn</th>';
                $table_data .= '<th>School Name</th>';
                $table_data .= '<th>School Code</th>';
                $table_data .= '<th>Zone</th>';
                $table_data .= '<th>Local Government Area</th>';

            $table_data .= '</tr>';
        $table_data .= '</thead>';

        $table_data .= '<tbody>';
            if(count($school_data)){
                $sn=0;
                foreach ($school_data as $value) {
                    $table_data .= '<tr>';
                        $table_data .= '<td>'.++$sn.'</td>';
                        $table_data .= '<td>'.$value->schoolname.'</td>';
                        $table_data .= '<td>'.$value->schoolcode.'</td>';
                        $table_data .= '<td>'.$this->schools_model->get_zone_name($value->zoneid).'</td>';
                        $table_data .= '<td>'.$this->schools_model->get_lga_name($value->lgaid).'</td>';
                        $table_data .= '</tr>';
                        if($sn % 25 == 0 ){
                           $table_data.= '<tr> <td> <div style = "page-break-before:always"></div> </td> </tr>';
                        }
                }
            }
        $table_data .= '</tbody>';
        $table_data .= '</table>';

        $this->data['report'] = $table_data;
        $this->data['url'] = site_url('admin/secondaryschools/save');
        $this->data['title'] = 'SECONDARY SCHOOLS FOR POSTING REPORT';

        $this->load->view('admin/print_page', $this->data);
    }

    function ajax_update_posting_school($isPostingSchool,$schoolid){
        $this->db->where('schoolid',$schoolid);
        $updatedata = array('ispostingschool'=>$isPostingSchool);
        $this->db->set($updatedata);
        $updated = $this->db->update('t_schools',$data);


        if ($updated){
            echo '<div style = "color:green">Saved</div>';
        }
        else {
            echo '<div style = "color:green">An Error Occurred</div>';
        }
    }
    function ajax_update_school_capacity($school_capacity,$schoolid){
        $this->db->where('schoolid',$schoolid);
        $updatedata = array('school_capacity'=>$school_capacity);
        $this->db->set($updatedata);
        $updated = $this->db->update('t_schools',$data);


        if ($updated){
            echo '<div style = "color:green">School Capacity Updated</div>';
        }
        else {
            echo '<div style = "color:green">An Error Occurred</div>';
        }
    }
}
