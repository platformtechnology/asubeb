<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Exams extends Admin_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
         $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Edc_Setup', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
        $this->load->model('admin/exam_model');
    }
    
    public function save($examid = null){
        $validation_rules = $this->exam_model->_rules;
        if($examid == null){
            $this->data['exam_detail'] = $this->exam_model->is_new();
        }else $this->data['exam_detail'] = $this->exam_model->get_all($examid); 
        
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run() == TRUE){
            //get the posted values
            $data = $this->exam_model->array_from_post(array('examname', 'examdesc', 'hasca', 'haspractical', 'hasposting', 'hassecschool', 'haszone'));
            if($examid == NULL){
                $data['examid'] = $this->exam_model->generate_unique_id();  //if its an insert, generated new id
                $this->exam_model->delete($data['examid']); //to ensure no duplicate when inserting and when synching db
            }
            if(empty($data['hasposting'])) $data['hasposting'] = 0;
            if(empty($data['hassecschool'])) $data['hassecschool'] = 0;
            if(empty($data['hasca'])) $data['hasca'] = 0;
            if(empty($data['haspractical'])) $data['haspractical'] = 0;
            if(empty($data['haszone'])) $data['haszone'] = 0;
            //if(empty($data['haslga'])) $data['haslga'] = 0;
            
            $data['edcid'] = $this->data['edc_detail']->edcid;
            
           //validate if the examname is unique within same edc
           $this->_validExam($data, $examid);
            
            $this->exam_model->save_update($data, $examid, $this->edcid);
            
            $message = 'Exam Details>>><br/>'
                    . 'Exam Name: '.$data['examname'].'<br/>'
                    . 'Description: '.$data['examdesc'].'<br/>'
                    . 'HasPosting: '.(empty($data['hasposting'])?'NO':'Yes').'<br/>'
                    . 'IsExamForSecondarySchool: '.(empty($data['hassecschool'])?'NO':'Yes').'<br/>'
                    . 'HasCAScoreEntry: '.(empty($data['hasca'])?'NO':'Yes').'<br/>'
                    . 'HasPracticalScoreEntry: '.(empty($data['haspractical'])?'NO':'Yes').'<br/>';
             $this->audittrail_model->log_audit($this->session->userdata('user_id'), ($examid == NULL ? 'ADD' : 'UPDATE'), 
                    $this->session->userdata('user_name'), $examid == NULL ? 'Added a new EXAM' : 'Updated Details of an EXAM',
                    $message, $this->data['edc_detail']->edcid);
             
            $this->session->set_flashdata('msg', $examid == NULL ? 'New Exam Added Successfully' : 'Exam Updated Successfully') ;
            redirect(site_url('admin/exams/save'));
        }  
        
        //for displaying all Exams
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $this->data['exams'] = $this->exam_model->get_all();
        
        $this->data['subview'] = 'admin/exam_page';        
        $this->load->view('admin/template/_layout_main', $this->data); 
    }
    
    public function delete($examid){
        $this->load->model('admin/registration_model');
        $examname = $this->registration_model->getExamName_From_Id($examid);
        $this->exam_model->delete($examid);
       
        $this->db->where('examid', $examid);
        $this->db->delete('t_candidates');
        logSql($this->db->last_query(), $this->edcid);
        
        $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'DELETE', 
                    $this->session->userdata('user_name'), 'DELETED details of an Exam',
                    'Exam Name: '.$examname, $this->data['edc_detail']->edcid);
       $this->session->set_flashdata('updated_msg', 'Exam Deleted Successfully');
       redirect(site_url('admin/exams/save'));      
    }    
    
    public function _validExam($data, $examid){
        if($examid == null){
            $result = $this->exam_model->get_where(array('examname' => $data['examname'], 'edcid' => $data['edcid']));
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Exam, ' . $data['examname'] . ', Already Exists, Exam Must Be Unique');
                 redirect(site_url('admin/exams/save/'.$examid));
            }
        }else{
            $this->db->where_not_in('examid', $examid);
            $result = $this->exam_model->get_where(array('examname' => $data['examname'], 'edcid' => $data['edcid']));
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Exam, ' . $data['examname'] . ', Already Exists, Exam Must Be Unique');
                 redirect(site_url('admin/exams/save/'.$examid));
            }
        }
    }
}
