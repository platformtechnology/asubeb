<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dbsync extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if(!in_array('Database_Sync', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }

        $this->load->model('admin/synchdetail_model');
        $this->load->model('admin/synchlog_model');
    }

    public function index() {

        $this->data['synch_log'] = $this->synchlog_model->get_where(array('edcid'=>  $this->data['edc_detail']->edcid));
        $this->data['subview'] = 'admin/synchronisation_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function get_from_online(){

       //Send request to get file
       $url = config_item('synch_get_url') . $this->edcid;
       redirect($url);
   }

    public function push_db_online(){

        $path = config_item('synch_path') . $this->edcid . '.edp';
        if(file_exists($path)){
            $url = config_item('synch_push_url');
            $cfile = curl_file_create(realpath($path),'application/octet-stream', $this->edcid.'.edp');
            $response_data = $this->_sendExternalRequest($url, $extraData = $cfile);
            $ch = $response_data['curl_obj'];
            if(!curl_errno($ch)){

                if($response_data['response'] == '1'){
                   #the synch file was sent online successfully
                    #clear the sync file and start logging sql afresh.
                    @unlink ($path);

                    $data = array();
                    $data['edcid'] = $this->data["edc_detail"]->edcid;
                    $data['synchdate'] = date('D, d M Y H:i:s');
                    $data['type'] = 'PUSHED_TO_ONLINE';
                    $this->db->query("delete from t_synchlog where edcid = '".$this->data["edc_detail"]->edcid."';");
                  //  $this->synchlog_model->save_update($data);

                    $this->session->set_flashdata('sync_msg', 'OFFLINE DB HAS BEEN SUCCESSFULLY PUSHED ONLINE');
                    redirect(site_url('admin/dbsync'));

                }else{


                    #synch file did not upload well - unknown error
                    $this->session->set_flashdata('sync_error', 'AN ERROR OCCURRED DURING SYNCHRONISATION - RETRY');
                    redirect(site_url('admin/dbsync'));
                }

            }else{
                //there was error with CURL request.
                $this->session->set_flashdata('sync_error', 'ERROR '. curl_errno($ch).' - ' . strtoupper(curl_error($ch)));
                redirect(site_url('admin/dbsync'));
            }
        }else{
            $this->session->set_flashdata('sync_error', 'NO SYNCHRONISATION NEEDED YET');
            redirect(site_url('admin/dbsync'));
        }


    }

    public function _sendExternalRequest($url, $extraData = null) {

       $post_fields = array(
            'edcid' => $this->data["edc_detail"]->edcid
           );
       if($extraData != null) $post_fields['data'] = $extraData;

       //SEND REQUEST
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_POST, count($post_fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        $response = curl_exec($ch);

        $data['response'] = $response;
        $data['curl_obj'] = $ch;
        return $data;
   }

    public function process_online_synch_file() {

       $supposed_file_location = '';
       $temp_upload_path = '';

       if(trim($_FILES['onlinefile']['name']) == false){
          $this->session->set_flashdata('onlineSync_error', "No file Uploaded - Try Again");
          redirect(site_url('admin/dbsync'));
       }
       else{
            $edcid = $this->edcid;
            $temp_upload_path = './resources/'.$edcid.'.edp';

           //IF ITS NOT AN EDP FILE
            if($_FILES['onlinefile']['type'] != "application/octet-stream"){
                $this->session->set_flashdata('onlineSync_error', "Corrupted file type - only .edp alllowed");
                redirect(site_url('admin/dbsync'));
                return;
            }

            if(substr($_FILES['onlinefile']['name'], -4) != ".edp"){
                $this->session->set_flashdata('onlineSync_error', "Invalid file found - only .edp alllowed");
                redirect(site_url('admin/dbsync'));
                return;
            }

            if (move_uploaded_file($_FILES['onlinefile']['tmp_name'], $temp_upload_path)) {

                if (file_exists($temp_upload_path)){

                    $file_contents = file_get_contents($temp_upload_path);

                    @unlink($temp_upload_path);
                    #$this->db->query($file_contents);

                    #experiment asynchronous execution
                    $db_connection = NULL;

                    $dbhost = $this->db->hostname;
                    $dbuser = $this->db->username;
                    $dbpass = $this->db->password;
                    $dbname = $this->db->database;

                    $connection_string = "host=" . $dbhost . " user=" . $dbuser . " password=" . $dbpass . " dbname=" . $dbname;
                    if(!($db_connection = pg_connect($connection_string))){
                        $this->session->set_flashdata('onlineSync_error', "DB Connection Error");
                        redirect(site_url('admin/dbsync'));
                        return;
                    }
                    if(pg_send_query($db_connection, $file_contents))pg_close($db_connection);;

                    $data = array();
                    $data['edcid'] = $edcid;
                    $data['synchdate'] = date('D, d M Y H:i:s');
                    $data['type'] = 'DOWNLOADED_FROM_ONLINE';
                    $this->db->query("delete from t_synchlog where edcid = '".$this->edcid."';");
                    $this->synchlog_model->save_update($data);

                    $this->session->set_flashdata('onlineSync_msg', "SYNCHRONISATION PROCESSED SUCCESSFULLY");
                    redirect(site_url('admin/dbsync'));

                }
            }
       }

       //Just house keeping. Below statement might not exexute.
       if(file_exists($temp_upload_path))@unlink($temp_upload_path);


    }

}
