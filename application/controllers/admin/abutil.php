<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of a_correct_edcid
 *
 * @author Maxwell
 */
class Abutil extends CI_Controller {

    //put your code here

    function __construct() {
        parent::__construct();
    }

    public function _generate_unique_id() {
        $mstime = str_replace(".", "", microtime());
        $times = explode(" ", $mstime);
        $id = str_replace(".", "", uniqid("", true)) . $times[0];
        return $id;
    }

    public function _getOnlineDb() {

        $config['hostname'] = "localhost";
        $config['username'] = "postgres";
        $config['password'] = "chameleon";
        $config['database'] = "abia_online";
        $config['dbdriver'] = "postgre";
        $config['dbprefix'] = "";
        $config['pconnect'] = TRUE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";

        $db = $this->load->database($config, true);
        return $db;
    }

    public function _getOneDimArray($param) {
        $data = array();
        foreach ($param as $index => $pinVal) {
            $data[] = $pinVal['candidateid'];
        }

        return $data;
    }

    public function concatenate_ids($ids = array()) {
        if (count($ids)) {
            $c_id = '';
            foreach ($ids as $value) {
                $c_id .= "'" . $value->candidateid . "',";
            }
            return rtrim($c_id, ",");
        }
    }

    public function work_on_ca() {

        $sql = "select t_candidates.examno, t_scores.candidateid, sum(ca_score) from t_scores
            inner join t_candidates on t_scores.candidateid = t_candidates.candidateid
            where t_scores.examid = '60djn6zke0' and t_scores.edcid = 'pc09aas8c4'
            group by t_scores.candidateid, t_candidates.examno
            having sum(ca_score) = 0";
        $wrongdata = $this->db->query($sql)->result();
        foreach ($wrongdata as $data) {
            echo "'" . $data->examno . "',<br/>";
        }
    }

    public function mergestudent() {
        $serverDB = $this->_getOnlineDb();
        $edcid = '610zzzveoj';
        $examid = '2n11sray1c';
        $examyear = '2015';

        $online_candidates = $serverDB
                //->select('candidateid')
                ->where('edcid', $edcid)
                ->where('examid', $examid)
                ->where('examyear', $examyear)
                ->get('t_candidates')
                ->result_array();

        $offline_candidates = $this
                ->db
                ->where('edcid', $edcid)
                ->where('examid', $examid)
                ->where('examyear', $examyear)
                ->get('t_candidates')
                ->result_array();

        #convert to one-D array
        $id_from_online = $this->_getOneDimArray($online_candidates);
        $ids_from_offline = $this->_getOneDimArray($offline_candidates);

        //$online_concatenated_ids = $this->concatenate_ids($id_from_online);
//        echo count($id_from_online) . '<br/>';
//        echo $online_concatenated_ids;
//        exit;
        //$sql = "select * from t_candidates where candidateid in (".$online_concatenated_ids.")";
        //$online_full_data = $this->db->query($sql)->result_array();
        #get ids online but not offline
        $original_candidates = array_diff($id_from_online, $ids_from_offline);

        #get ids that exist online and offline as well
        $duplicated_candidates = array_intersect($id_from_online, $ids_from_offline);

        $data = array();
        $c_id = '';
        foreach ($original_candidates as $key => $candidate) {
            $data[] = array(
                'candidateid' => $online_candidates[$key]['candidateid'],
                'edcid' => $online_candidates[$key]['edcid'],
                'examid' => $online_candidates[$key]['examid'],
                'examyear' => $online_candidates[$key]['examyear'],
                'examno' => $online_candidates[$key]['examno'],
                'serial' => $online_candidates[$key]['serial'],
                'firstname' => $online_candidates[$key]['firstname'],
                'othernames' => $online_candidates[$key]['othernames'],
                'gender' => $online_candidates[$key]['gender'],
                'dob' => $online_candidates[$key]['dob'],
                'phone' => $online_candidates[$key]['phone'],
                'zoneid' => $online_candidates[$key]['zoneid'],
                'lgaid' => $online_candidates[$key]['lgaid'],
                'schoolid' => $online_candidates[$key]['schoolid'],
                'centreid' => $online_candidates[$key]['centreid'],
                'firstchoice' => $online_candidates[$key]['firstchoice'],
                'secondchoice' => $online_candidates[$key]['secondchoice'],
                'placement' => $online_candidates[$key]['placement'],
                'datecreated' => date('Y-m-d H:i:s'),
                'datemodified' => date('Y-m-d H:i:s')
            );

            //$c_id .= "'" .   $online_candidates[$key]['candidateid'] . "',";
        }

//        echo "online canidates = " . count($online_candidates) . "<br/>";
//        echo "offline canidates = " . count($offline_candidates) . "<br/>";
//        echo "candidates to Synchronise (As gotten from Algorithm) = " . count($original_candidates) . "<br/>";
//        echo "candidates online and already Offline = " . count($duplicated_candidates) . "<br/>";
        #Merge the Candidates
        $this->db->insert_batch('t_candidates', $data);

        $sql = "select * from t_subjects where edcid = '" . $edcid . "' and examid = '" . $examid . "'";
        $subjects = $serverDB->query($sql)->result();

        $regdata = array();
        foreach ($subjects as $subject) {
            foreach ($data as $val) {
                $regdata[] = array(
                    'subjectid' => $subject->subjectid,
                    'edcid' => $edcid,
                    'examid' => $examid,
                    'examyear' => $examyear,
                    'candidateid' => $val['candidateid'],
                    'datecreated' => date('Y-m-d H:i:s'),
                    'datemodified' => date('Y-m-d H:i:s')
                );
            }
        }

        $this->db->insert_batch('t_registered_subjects', $regdata);

        $scoredata = array();
        foreach ($subjects as $subject) {
            foreach ($data as $val) {
                $scoredata[] = array(
                    'scoreid' => $this->_generate_unique_id(),
                    'subjectid' => $subject->subjectid,
                    'edcid' => $edcid,
                    'examid' => $examid,
                    'examyear' => $examyear,
                    'candidateid' => $val['candidateid'],
                    'datecreated' => date('Y-m-d H:i:s'),
                    'datemodified' => date('Y-m-d H:i:s')
                );
            }
        }

        $this->db->insert_batch('t_scores', $scoredata);

        echo 'CANDIDATES MERGED SUCCESSFULLY<br/>';
    }

    public function _fixscores() {
        $serverDB = $this->_getOnlineDb();
        $edcid = '610zzzveoj';
        $examid = '2n11sray1c';
        $examyear = '2015';

        $sql = "select subjectid from t_subjects where edcid = '" . $edcid . "' and examid = '" . $examid . "'";
        $subjects = $this->db->query($sql)->result_array();

        $subj_ar = array();
        foreach ($subjects as $subject) {
            $subj_ar[] = $subject['subjectid'];
        }

        $faulty_candidates_sql = "select candidateid
                            from t_scores where examid = '" . $examid . "'
                            and examyear = '" . $examyear . "'
                            group by candidateid
                            having count(*) < 9";
        $faulty_candidates = $this->db->query($faulty_candidates_sql)->result();
        $linear_ids = $this->concatenate_ids($faulty_candidates);
        $linear_ids = rtrim($linear_ids, ",");

        $sql_scores = "select * from t_scores where candidateid in (" . $linear_ids . ")";
        $score_data = $this->db->query($sql_scores)->result();

        $score_data_array = array();
        $temp_array = array();

        foreach ($score_data as $sdata) {
            $score_data_array[$sdata->candidateid][] = $sdata->subjectid;
        }

        $state_ment = 'INSERT INTO t_scores '
                . '(scoreid, subjectid, examid, examyear, edcid, candidateid, datecreated, datemodified) VALUES ';
        foreach ($score_data_array as $key => $value) {
            foreach ($subj_ar as $subjid) {
                if(!in_array($subjid, $value)){
                   $state_ment .= "('".$this->_generate_unique_id()."', '".$subjid."', '".$examid."', '".$examyear."', '".$edcid."', '".$key."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."'),<br/>"; 
                }
            }
        }
        
        echo $state_ment;
        exit;
    }

}
