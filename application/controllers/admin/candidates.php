<?php
session_start();
class Candidates extends Admin_Controller {
    //put your code here

    function __construct() {
        parent::__construct();

        $priviledges = explode("^", $this->session->userdata('user_role'));
        if(!in_array('Report_Viewing', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
        $this->load->model('admin/registration_model');
    }

    public function search(){
        $edcid = $this->data['edc_detail']->edcid;

        $this->data['registrants'] = array();
        $this->_saveCandidatesInSession();

       //for displaying zones ddl
        $this->db->where('t_zones.edcid', $edcid);
        $this->data['zones'] = $this->db->get('t_zones')->result();
        //for lga ddl
        $this->db->where('edcid', $edcid);
        $this->db->order_by('lganame');
        $this->data['lgas'] = $this->db->get('t_lgas')->result();
        //for exams ddl
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();

        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');

        if($this->form_validation->run()){
            $data = $this->registration_model->array_from_post(array('examid', 'examyear', 'zoneid', 'lgaid', 'schoolid'));

            $this->_performFilter($data, $edcid);
            $this->db->like('zoneid', $data['zoneid']);
            $this->db->like('lgaid', $data['lgaid']);
            $this->db->like('schoolid', $data['schoolid']);
            $this->data['registrants'] = $this->registration_model->get_all();
            $this->data['count'] = count($this->data['registrants']);

            $this->_saveCandidatesInSession(); // save the resultset in session so that it can be exported to excel

            if(isset($data['lgaid'])){
                $this->db->where('edcid', $edcid);
                $this->db->where('lgaid', $data['lgaid']);
                //$this->db->where('ispostingschool', 0);
                $this->db->order_by('schoolname');
                $this->data['schools'] = $this->db->get('t_schools')->result();
            }
        }

                 //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
         $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';


        $this->data['subview'] = 'admin/candidates_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    private function _performFilter($data, $edcid){
        if(empty($data['examyear']) && (!empty($data['examid']))){
            $this->db->where(array('edcid'=>$edcid, 'examid'=>$data['examid']));
        }
        else if((!empty($data['examyear'])) && (!empty($data['examid']))){
            $this->db->where(array('examyear'=>$data['examyear'], 'edcid'=>$edcid, 'examid'=>$data['examid']));
        }
        else if((!empty($data['examyear'])) && empty($data['examid'])){
            $this->db->where(array('edcid'=>$edcid, 'examyear'=>$data['examyear']));
        }else{
            $this->db->where(array('edcid'=> $edcid));
        }
    }

    public function _saveCandidatesInSession(){
        $_SESSION['platform_candidates'] = $this->data['registrants'];
        //$this->session->set_userdata('platform_candidates', $this->data['registrants']);
    }

    public function export_to_excel(){
            $this->load->helper('excel');
            export_to_excel($_SESSION['platform_candidates'], 'Candidate_Report');
            //print_r($this->session->userdata('platform_candidates')); exit;
    }

    public function delete($candidateid) {
        $this->registration_model->delete($candidateid);
        $sql = "delete from t_registered_subjects where candidateid = '" . $candidateid . "'; "
             . "delete from t_scores where candidateid = '" . $candidateid . "';";
        $this->db->query($sql);
        logSql($sql, $this->edcid);

        $this->session->set_flashdata('msg', 'Candidate Deleted Successfully');
        redirect(site_url('admin/candidates/search'));
    }

    public function view($candidateid, $printcandidate = null) {
        $this->data['reg_info'] = $this->registration_model->get_all($candidateid);
        if (count($this->data['reg_info'])) {
        //GET PIN INFO
            $this->db->where('candidateid',$this->data['reg_info']->candidateid);
            $this->data['candidate_pin_info'] = $this->db->get('t_pins')->row();
        }

        count($this->data['reg_info']) || show_404();

        //Get Exam Detail that the candidate registered for
        $this->load->model('admin/exam_model');
        $this->data['exam_info'] = $this->exam_model->get_all($this->data['reg_info']->examid);

        $this->data['subjects'] = $this->db->get_where('t_registered_subjects', array('candidateid'=>$candidateid))->result();

        $this->data['passport'] = base_url('resources/images/passports/'.$candidateid.'.jpg');
        if(!file_exists('./resources/images/passports/'.$candidateid.'.jpg')){
            $this->data['passport'] = get_img('no_image.jpg');
        }

        //Add styles for the passport display
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';
        //$this->data['page_level_scripts'] = '<script src="' . base_url('resources/js/bootstrap-fileupload.js') . '"></script>';

        if($printcandidate == null){
            $this->data['subview'] = 'admin/registration_confirm_page';
            $this->load->view('admin/template/_layout_main', $this->data);
        }else{
            $this->load->view('admin/print_profile_page', $this->data);
        }
    }

    public function printdetails() {
        $this->data['printdata'] = $_SESSION['platform_candidates'];
        $this->load->view('admin/print_batch_page', $this->data);
    }

    public function school_ajaxdrop(){
        if($this->_is_ajax()){
            $lgaid = $this->input->get('lgaid', TRUE);
            $zoneid = $this->input->get('zoneid', TRUE);

            $this->db->select('schoolid as lgaid, schoolname as lganame');
            if(trim($lgaid) == false && trim($zoneid) != false) {
                $this->db->where('zoneid', $zoneid);
            }else{
                $this->db->where('lgaid', $lgaid);
                $this->db->where('lgaid !=', '');
            }
            $this->db->order_by('schoolname');
            $query = $this->db->get('t_schools');


            $data['lgas'] = array();
            if ($query->num_rows() > 0)
            {
              $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        }else{
            echo "Apparently is_ajax returned false!";
            show_error('This method can only be accessed internally.', 404);
        }
    }

    function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

    public function practical(){

        if($this->input->post('schoolid')){

            $examid = $this->input->post('examid');
            $examyear = $this->input->post('examyear');
            $schoolid = $this->input->post('schoolid');

            $this->data['examid'] = $examid;
            $this->data['examyear'] = $examyear;

            $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $this->db->where('issecondary', 1);
            if($schoolid != "2")$this->db->where('schoolid', $schoolid);
            $this->db->order_by('schoolcode', 'asc');
            $this->data['school_detail'] = $this->db->get('t_schools')->result();

            $this->data['exam_detail'] = $this->db->where('examid', $examid)->get('t_exams')->row();

             $this->data['title'] = "LIST OF CANDIDATES FOR ".$this->registration_model->getExamName_From_Id($examid)." ".$examyear;
        }

        $this->data['practical'] = true;
        $this->load->view('admin/Reports/reports_practical_page', $this->data);
    }

    public function propractical(){

        if($this->input->post('schoolid')){

            $examid = $this->input->post('examid');
            $examyear = $this->input->post('examyear');
            $schoolid = $this->input->post('schoolid');
            $fromdate = $this->input->post('fromdate');
            $fromtime = $this->input->post('fromtime');

            $this->data['examid'] = $examid;
            $this->data['examyear'] = $examyear;

            $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $this->db->where('issecondary', 1);
            if($schoolid != "2")$this->db->where('schoolid', $schoolid);
            $this->db->order_by('schoolcode', 'asc');
            $this->data['school_detail'] = $this->db->get('t_schools')->result();

            $this->data['exam_detail'] = $this->db->where('examid', $examid)->get('t_exams')->row();

            $this->data['interval'] = $fromdate . " " . $fromtime;

             $this->data['title'] = "LIST OF CANDIDATES FOR ".$this->registration_model->getExamName_From_Id($examid)." ".$examyear;
        }


        $this->data['practical'] = false;
        $this->load->view('admin/Reports/reports_practical_page', $this->data);
    }

}
