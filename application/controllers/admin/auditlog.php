<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auditlog extends Admin_Controller {
    
    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Super_Administrator', $priviledges)){
            redirect(site_url('admin/login/logout'));
        }
        
        //$this->db->where('edcid', config_item('edcid'));
    }
    
    public function index() { 
        $this->load->model('admin/users_model');
        $this->data['actiontypes'] = $this->db->query("select distinct actiontype from t_audittrail where edcid = '" . $this->edcid ."' ")->result();
        
        $this->db->where('edcid', $this->edcid);
        $this->data['users'] = $this->users_model->get_all(); 
        $this->data['auditlog'] = $this->_defaultSearch();
        
         if($this->input->post('datefrom')){
             $data = $this->audittrail_model->array_from_post(array('userid','actiontype','datefrom','dateto'));
             $this->data['auditlog'] = $this->_performSearch($data);
         }
                
         //Add styles and scripts for the datable
        $this->data['page_level_scripts'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
           
           //Add styles and scripts for the datetime
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        $this->data['subview'] = 'admin/auditlog_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }
    
    private function _defaultSearch(){
        $sql =  "select t_audittrail.*, fullname from t_audittrail 
                inner join t_users on t_audittrail.userid = t_users.userid
                where  to_char(t_audittrail.datecreated, 'YYYY-MM-DD') = to_char(current_date, 'YYYY-MM-DD')
                and t_audittrail.edcid = '" . $this->edcid ."' 
                order by t_audittrail.datecreated desc";
        $query = $this->db->query($sql);
        $resultSet = $query->result();
        return $resultSet;
    }
    
    private function _performSearch($data) {
        
        $sql =  "select t_audittrail.*, fullname from t_audittrail 
                inner join t_users on t_audittrail.userid = t_users.userid
                where  
                to_char(t_audittrail.datecreated, 'YYYY-MM-DD') >= '". $data['datefrom']. "'
                and 
                to_char(t_audittrail.datecreated, 'YYYY-MM-DD') <= '". $data['dateto']. "'
                and 
                t_audittrail.edcid = '" . $this->edcid ."' 
                ";
        if (empty($data['actiontype']) && (!empty($data['userid'])))
        {
            $sql .= " and t_audittrail.userid = '". $data['userid'] ."' ";
        }
        else if ((!empty($data['actiontype'])) && empty($data['userid']))
        {
           $sql .= " and t_audittrail.actiontype = '". $data['actiontype'] ."' ";
        }
        else if ((!empty($data['actiontype'])) && (!empty($data['userid'])))
        {
            $sql .= " and t_audittrail.userid = '". $data['userid'] ."' ";
            $sql .= " and t_audittrail.actiontype = '". $data['actiontype'] ."' ";
        }
         
        $sql .= "order by t_audittrail.datecreated desc";
         
        //$sql = sprintf($sql, $data['datefrom'], $data['dateto']);
        $query = $this->db->query($sql);
        $resultSet = $query->result();
        return $resultSet;
    }
    
    public function view($id) {
        $id || show_404();
        
       $sql =  "select t_audittrail.*, fullname, email, role from t_audittrail 
                inner join t_users on t_audittrail.userid = t_users.userid
                where t_audittrail.id = '". $id ."'";
       $query = $this->db->query($sql);
       $this->data['audit'] = $query->row();
       
       if(count( $this->data['audit'])){
           $this->data['subview'] = 'admin/auditlog_view_page';
            $this->load->view('admin/template/_layout_main', $this->data);
       }else{
           redirect(site_url('admin/auditlog'));
       }
       
    }
}
