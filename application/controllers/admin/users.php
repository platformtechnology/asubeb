<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Users extends Admin_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
         $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('User_Mgt', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
        $this->load->model('admin/users_model');
    }
    
    public function save($userid = null){
        $validation_rules = $this->users_model->_rules;
        if($userid == null){
            $this->data['admin_detail'] = $this->users_model->is_new();
            $validation_rules['email']['rules'] .= '|is_unique[t_users.email]';
        }else $this->data['admin_detail'] = $this->users_model->get_all($userid); 
        
        unset($validation_rules['password']);
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run() == TRUE){
           $default_pass = config_item('default_edcadmin_pass');
           
           $super_admin_role = $this->input->post('Super_Administrator');
           $role = '';
           
           if(empty($super_admin_role)){
                $priviledges = $this->input->post('role');
                if(empty($priviledges)){
                    $this->session->set_flashdata('error', 'Kindly Select Priviledges For User');
                    redirect(site_url('admin/users/save')); 
                }
                
                foreach($priviledges as $priviledge){
                    $role .= $priviledge .'^';
                }
                
           }else{
              $role =  $super_admin_role;
           }
           
           
           //get the posted values
            $data = $this->users_model->array_from_post(array('fullname', 'email', 'phone'));
            if($userid == NULL){
                $data['userid'] = $this->users_model->generate_unique_id();  //if its an insert, generated new categoryid
                //prevent duplicate while synching DB
                $this->users_model->delete($data['userid']);
                $data['password'] = encrypt($default_pass);
            }
            
            $data['edcid'] = $this->data['edc_detail']->edcid;
            $data['role'] = str_lreplace('^', '', $role);
            
           //validate if the email is unique during update
            if($userid != null){
                $this->db->where('edcid', $this->data['edc_detail']->edcid);
                $this->db->where_not_in('userid', $userid);
                $result = $this->db->get_where('t_users', array('email' => $data['email']))->row();
                if(count($result)>0){
                     $this->session->set_flashdata('error', 'The Admin Email, ' . $data['email'] . ', Already Exists - Email Must Be Unique');
                     redirect(site_url('admin/users/save/'.$userid));
                }
            }
            
            $this->users_model->save_update($data, $userid, $this->edcid);
            
            $message = 'User\'s Name: '.$data['fullname'].'<br/>'
                    . 'User\'s Email: '.$data['email'].'<br/>'
                    . 'User\'s Phone: '.$data['phone'].'<br/>'
                    . 'User\'s Priviledge: '.$data['role'].'<br/>';
            
            $this->audittrail_model->log_audit($this->session->userdata('user_id'), ($userid == NULL ? 'ADD' : 'UPDATE'), 
                    $this->session->userdata('user_name'), $userid == NULL ? 'Added a new System USER' : 'Updated USER\'S Detail',
                    'User Details:>>> <br/> '.$message, $this->data['edc_detail']->edcid);
            
            $this->session->set_flashdata('msg', $userid == NULL ? 'New User Added Successfully' : 'User Updated Successfully') ;
            redirect(site_url('admin/users/save'));
        }  
        
        //for displaying all administrators
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $this->data['administrators'] = $this->users_model->get_all();
         
        $this->data['subview'] = 'admin/users_page';        
        $this->load->view('admin/template/_layout_main', $this->data); 
    }
    
    public function delete($userid){
       $user_detail = $this->users_model->get_all($userid, true);
       $message = 'User\'s Name: '.$user_detail->fullname.'<br/>'
               . 'User\'s Email: '.$user_detail->email.'<br/>'
               . 'User\'s Priviledge: '.$user_detail->role.'<br/>';
       $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'DELETE', 
                    $this->session->userdata('user_name'), 'DELETED a USER of the system',
                    $message, $this->data['edc_detail']->edcid);
       
       $this->users_model->delete($userid);
       $this->session->set_flashdata('updated_msg', 'User Deleted Successfully');
      redirect(site_url('admin/users/save'));      
    }
    
    public function reset($userid){
       $data['password'] = encrypt(config_item('default_edcadmin_pass'));
       $this->users_model->save_update($data, $userid);
       $this->session->set_flashdata('updated_msg', 'User\'s Password Reset Successfull');
       redirect(site_url('admin/users/save'));      
    }
}
