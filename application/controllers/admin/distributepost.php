<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distributepost extends Admin_Controller {
    
    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Posting', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
        
        $this->load->model('admin/registration_model');
    }

    public function index() {  
      
       $edcid = $this->data['edc_detail']->edcid;
      
       $this->db->where('edcid', $edcid);
       $this->db->where('ispostingschool', 1);
       $this->db->order_by('schoolname');
       $this->data['schools'] = $this->db->get('t_schools')->result();
       
       $this->db->where('edcid', $edcid);
       $this->db->where('hasposting', 1);
       $this->data['exams'] = $this->db->get('t_exams')->result();

       $this->form_validation->set_rules('schoolfrom', 'School From', 'trim|required');
       $this->form_validation->set_rules('schoolto', 'School To', 'trim|required');
       $this->form_validation->set_rules('nocandidates', 'No of Candidates', 'trim|required|is_natural_no_zero');
       $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
       $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
       $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
       
       if($this->form_validation->run() == TRUE){
           
         $schoolfrom = $this->input->post('schoolfrom');
         $schoolto = $this->input->post('schoolto');
         $nocandidates = $this->input->post('nocandidates');
         $examid = $this->input->post('examid');
         $examyear = $this->input->post('examyear');
         $gender = $this->input->post('gender');
         
         //get candidates to be moved
         $sql = "select * from t_candidates 
                where examid = '".$examid."' 
                and examyear = '".$examyear."' 
                and edcid = '".$this->data["edc_detail"]->edcid."'
                and placement = '" .$schoolfrom."' ";
         if($gender != "X") $sql .= "and gender = '" . $gender ."' ";
         
         $super_set_of_candidates_to_move = $this->db->query($sql);
         $updateSql = '';
                 
         if($super_set_of_candidates_to_move->num_rows() > 0){
            for($i=0; $i< $nocandidates; $i++){
   //            $randVal = mt_rand(1, $super_set_of_candidates_to_move->num_rows());
   //            $randValArray[] = $randVal;
   //            while(in_array($randVal, $randValArray)){
   //                    $randVal = mt_rand(1, $super_set_of_candidates_to_move->num_rows());
   //            }
               if($i > $super_set_of_candidates_to_move->num_rows()) break;

               $rand_candidate_record = $super_set_of_candidates_to_move->row($i);
               $updateSql .= "update t_candidates set placement = '" . $schoolto . "' where candidateid = '" . $rand_candidate_record->candidateid ."'; ";

             }
            if($updateSql != ''){
                $this->db->query($updateSql);
                logSql($updateSql, $this->edcid);
            }
             
              $this->session->set_flashdata('msg', 'CANDIDATES MOVED SUCCESSFULLY');
              redirect(site_url('admin/distributepost'));
          
         }else{
             $this->data['error']  = 'CANDIDATES NOT MOVED - NO CANDIDATES/CHECK GENDER';
//            redirect(site_url('admin/distributepost')); 
         }
         
       }
      
        //Add styles and scripts 
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        $this->data['subview'] = 'admin/placement_distribution_page';        
        $this->load->view('admin/template/_layout_main', $this->data); 
    }
    
  
}
