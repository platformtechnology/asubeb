<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of subjects
 *
 * @author Maxwell
 */
class Subjects extends Admin_Controller{
   
    function __construct() {
        parent::__construct();
         $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Edc_Setup', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect(site_url('admin/login/logout'));
        }
        $this->load->model('admin/subjects_model');   
    }
    
    public function save($subjectid = null){

        $validation_rules = $this->subjects_model->_rules;
        if($subjectid == null){
            $this->data['subject_detail'] = $this->subjects_model->is_new();
        }else $this->data['subject_detail'] = $this->subjects_model->get_all($subjectid); 
        
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run() == TRUE){
            $data = $this->subjects_model->array_from_post(array('subjectname', 'iscompulsory', 'haspractical', 'examid', 'priority'));
            
            if(empty($data['iscompulsory'])) $data['iscompulsory'] = 0;
            if(empty($data['haspractical'])) $data['haspractical'] = 0;
            
            $data['edcid'] = $this->data['edc_detail']->edcid;
            
            if($subjectid == null) {
                $data['subjectid'] = $this->subjects_model->generate_unique_id();
                //prevent duplicate while synching DB
                $this->subjects_model->delete($data['subjectid']);
            }
 
            $this->_validSubject($data, $subjectid);
            
            $this->subjects_model->save_update($data, $subjectid);
            if($subjectid == null){ //Insert Default Maximum Scores for ca, exam and practical
                $this->load->model('admin/scores_margin_model');
                $this->scores_margin_model->save_update(array('subjectid'=>$data['subjectid'], 'edcid'=>$this->data['edc_detail']->edcid), null);
            }
            
            /**
             * NO NEED TO INSERT THE DEFAULT SCORES PER SUBJECT
             * IT SHOULD BE DEFAULT SCORE PER CANDIDATE FOR CANDIDATE'S 
             * REGISTERED SUBJECT
             */
            $this->load->model('admin/registration_model');
            $message = 'Subject Name: '.$data['subjectname'].'<br/>'
                    . 'Exam Name: '.$this->registration_model->getExamName_From_Id($data['examid']).'<br/>'
                    . 'IsCompulsory: '.(empty($data['iscompulsory']) ? 'No':'Yes').'<br/>'
                    . 'HasPractical: '.(empty($data['haspractical']) ? 'No':'Yes');
            $this->audittrail_model->log_audit($this->session->userdata('user_id'), ($subjectid == NULL ? 'ADD' : 'UPDATE'), 
                    $this->session->userdata('user_name'), $subjectid == NULL ? 'Added a new SUBJECT with default scores' : 'Updated Details of a SUBJECT',
                    $message, $this->data['edc_detail']->edcid);
            
                       
            $this->session->set_flashdata('msg', $subjectid == NULL ? 'New Subject Added Successfully' : 'Subject Updated Successfully') ;
            redirect(site_url('admin/subjects/save/'));
        }
        
        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();
        
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $this->data['all_subjects'] = $this->db->get('t_subjects')->result();
        
        $this->data['subview'] = 'admin/subjects_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }
    
    public function delete($subjectid){
       $subject_detail = $this->subjects_model->get_all($subjectid);
        //WOULD DELETE THAT SUBJECT
       $this->subjects_model->delete($subjectid);
       
       //WOULD DELETE ALL SCORE TIED TO THAT SUBJECT
       $this->db->where('subjectid', $subjectid);
       $this->db->delete('t_scores');
       logSql($this->db->last_query(), $this->edcid);
       
       //WOULD DELETE ALL MARGIN TIED TO THAT SUBJECT
       $this->db->where('subjectid', $subjectid);
       $this->db->delete('t_scores_margin');
       logSql($this->db->last_query(), $this->edcid);
       
       //WOULD DELETE ALL REGISTERED SUBJECT
       $this->db->where('subjectid', $subjectid);
       $this->db->delete('t_registered_subjects');
       logSql($this->db->last_query(), $this->edcid);

       $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'DELETE', 
                    $this->session->userdata('user_name'), 'DELETED a Subject and all of its associated scores',
                    'Subject Name: '.$subject_detail->subjectname, $this->data['edc_detail']->edcid);
            
       $this->session->set_flashdata('msg', 'Subject Deleted Successfully');
       redirect(site_url('admin/subjects/save/'));      
    }
     
    public function _validSubject($data, $subjectid){
		$this->db->where('edcid', $this->data['edc_detail']->edcid);
        if($subjectid == null){
            $result = $this->subjects_model->get_where(array('subjectname' => $data['subjectname'], 'examid' => $data['examid'], 'examyear' => $data['examyear']), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Subject, ' . $data['subjectname'] . ', Already Exists In Selected Exam and Year!');
                  redirect(site_url('admin/subjects/save/' .$subjectid));
            }
        }else{
            $this->db->where_not_in('subjectid', $subjectid);
            $result = $this->subjects_model->get_where(array('subjectname' => $data['subjectname'], 'examid' => $data['examid'], 'examyear' => $data['examyear']), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Subject, ' . $data['subjectname'] . ', Already Exists In Selected Exam and Year!');
                  redirect(site_url('admin/subjects/save/' . $subjectid));
            }
        }
    }
    
}
