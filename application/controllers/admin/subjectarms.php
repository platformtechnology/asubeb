<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of subjects
 *
 * @author Maxwell
 */
class Subjectarms extends Admin_Controller{
   
    function __construct() {
        parent::__construct();
        $this->load->model('admin/subjects_arm_model');
        $this->load->model('admin/subjects_model');
    }
    
    public function index(){
        $edcid = $this->data['edc_detail']->edcid;
         
        $this->data['arm_detail'] = $this->subjects_arm_model->is_new();
        $this->data['subjects'] = array();
        
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();
        
        $this->data['subview'] = 'admin/subjects_arm_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }
    
    public function search(){
        $edcid = $this->data['edc_detail']->edcid;
        $this->load->model('admin/exam_model');
                
        $this->data['subjects'] = array();
        
        $this->form_validation->set_rules('examid', 'Examination Type', 'trim|required');
        if($this->form_validation->run() == TRUE){
           $data = $this->subjects_arm_model->array_from_post(array('examid'));
           $data['edcid'] = $edcid;
           $this->data['subjects'] = $this->subjects_model->get_where(array('edcid'=>$edcid, 'examid'=>$data['examid']));
           $this->data['examid'] = $data['examid'];
           $this->data['arms'] = $this->subjects_arm_model->getArmData($data);
        }
   
        $this->data['exams'] = $this->exam_model->get_where(array('edcid'=>$edcid));
        $this->data['arm_detail'] = $this->subjects_arm_model->is_new();
        
         //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        //Add styles and scripts for 
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        $this->data['subview'] = 'admin/subjects_arm_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }
    
    public function save($examid = null, $armid = null){
        $edcid = $this->data['edc_detail']->edcid;
        
        if($armid == null) $this->data['arm_detail'] = $this->subjects_arm_model->is_new();
        else $this->data['arm_detail'] = $this->subjects_arm_model->get_all($armid); 
            
        $this->db->where('edcid', $edcid);
        $this->data['exams'] = $this->db->get('t_exams')->result();
        
        $this->data['subjects'] = $this->subjects_model->get_where(array('edcid'=>$edcid, 'examid'=>$examid));
        $this->data['examid'] = $examid;
        $this->data['arms'] = $this->subjects_arm_model->getArmData(array('examid' => $examid, 'edcid' => $edcid));
            
        $validation_rules = $this->subjects_arm_model->_rules;
        $this->form_validation->set_rules($validation_rules);
        if($this->form_validation->run() == TRUE){
            
            $data = $this->subjects_arm_model->array_from_post(array('armname', 'subjectid', 'examid'));
            $data['edcid'] = $edcid;
            
            if($armid == null){
                $data['armid'] = $this->subjects_arm_model->generate_unique_id();
                //prevent duplicate while synching DB
                $this->subjects_arm_model->delete($data['armid']);
            }
            
            $this->_validArm($data, $armid, $examid);
            
            $this->subjects_arm_model->save_update($data, $armid);
 
            $this->session->set_flashdata('msg', $armid == NULL ? 'New Subject Arm Added Successfully' : 'Subject Arm Updated Successfully') ;
            redirect(site_url('admin/subjectarms/save/'.$examid));
            
        }
        
        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        //Add styles and scripts for the datetime
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        $this->data['subview'] = 'admin/subjects_arm_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }
    
    public function delete($armid, $examid){
        
       $this->subjects_arm_model->delete($armid);

       $this->session->set_flashdata('msg', 'Subject Arm Data Deleted Successfully');
       redirect(site_url('admin/subjectarms/save/'.$examid));
    }
    
    public function _validArm($data, $armid, $examid){
        $this->db->where('edcid', $data['edcid']);
        if($armid == null){
            $result = $this->subjects_arm_model->get_where(array('armname' => $data['armname'], 'subjectid' => $data['subjectid'], 'examid' => $data['examid']), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Subject Arm, ' . $data['armname'] . ', Already Exists');
                  redirect(site_url('admin/subjectarms/save/'.$examid.'/'.$armid));
            }
        }else{
            $this->db->where_not_in('armid', $armid);
            $result = $this->subjects_arm_model->get_where(array('armname' => $data['armname'], 'subjectid' => $data['subjectid'], 'examid' => $data['examid']), TRUE);
            if(count($result)>0){
                 $this->session->set_flashdata('error', 'The Subject Arm, ' . $data['armname'] . ', Already Exists!');
                  redirect(site_url('admin/subjectarms/save/'.$examid.'/'.$armid));
                  
            }
        }
    }
    
    
}
