<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Remarks extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if (!in_array('Statistics', $priviledges) && !in_array('Super_Administrator', $priviledges)) {
            redirect(site_url('admin/login/logout'));
        }
        $this->edcid = $this->data['edc_detail']->edcid;
        $this->load->model('admin/remarks_model');
        $this->load->model('admin/exam_model');
		$this->load->model('admin/registration_model');
    }

    public function index() {

        $this->load->model('admin/exam_model');

        $this->data['exams'] = $this->exam_model->get_where(array('edcid' => $this->edcid));

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if ($this->form_validation->run()) {
            $data = $this->exam_model->array_from_post(array('examid', 'examyear'));
            $exam_data = $this->exam_model->get_all($data['examid'], true);
            if ($exam_data->hasposting == 1) {
                redirect(site_url('admin/remarks/cutoff/' . $data['examid'] . '/' . $data['examyear']));
            }
            redirect(site_url('admin/remarks/subjectdistribution/' . $data['examid'] . '/' . $data['examyear']));
        }

        $this->data['subview'] = 'admin/remark_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function cutoff($examid = null, $examyear = null,$run_analysis = NULL) {

        $examid || show_404();
        $examyear || show_404();

        $this->load->model('admin/cutoff_model');
        $this->load->model('admin/report_model');

        $this->data['cutoff'] = $this->cutoff_model->get_where(array('edcid' => $this->edcid, 'examid' => $examid, 'examyear' => $examyear), true);
        $this->db->order_by('lganame ASC');
        $this->data['lgas'] = $this->db->where('edcid', $this->data['edc_detail']->edcid)->get('t_lgas')->result();

        $validation_rules = $this->cutoff_model->_rules;
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run() == true) {
            $updateCutoff = $this->input->post('cutoff');
            if (isset($updateCutoff)) {
                $data = $this->cutoff_model->array_from_post(array('cutoff'));
                $data['edcid'] = $this->edcid;
                $data['examid'] = $examid;
                $data['examyear'] = $examyear;

                $this->db->where(array('examid' => $examid, 'examyear' => $examyear))->delete('t_cutoff');
                $this->cutoff_model->save_update($data);

                $message = 'CutOff Details>>><br/>'
                        . 'Exam Name: ' . $this->registration_model->getExamName_From_Id($examid) . '<br/>'
                        . 'Exam Year: ' . $examyear . '<br/>'
                        . 'CutOff Mark: ' . $data['cutoff'] . '<br/>';

                $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'UPDATE', $this->session->userdata('user_name'), 'Updated CUT OFF MARK For '
                        . $this->registration_model->getExamName_From_Id($examid) . ' ' . $examyear, $message, $this->data['edc_detail']->edcid);

                $this->session->set_flashdata('msg', 'CutOff Mark Updated Successfully - * REMEMBER TO RE-POST CANDIDATES ');
                redirect(site_url('admin/remarks/cutoff/' . $examid . '/' . $examyear));
            }
        }


        if ($run_analysis != NULL) {
            $this->data['run_analysis'] = TRUE;
        }
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        //$this->data['exam_detail']  = $this->db->get_where('t_exams', array('examid'=>$examid))->row();

        $this->data['subview'] = 'admin/remark_cutoff_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function subjectdistribution($examid = null, $examyear = null) {
        $examid || show_404();
        $examyear || show_404();

		$examdetail = $this->exam_model->get_all($examid);

        $this->load->model('admin/registration_model');
        $this->load->model('admin/subjects_model');
        $this->data['subjects'] = $this->subjects_model->get_where(array('edcid' => $this->edcid, 'examid' => $examid));

        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;

		$this->data['edcid'] = $this->edcid;

		$data['edcid'] = $this->edcid;

        $this->form_validation->set_rules($this->remarks_model->_rules);

        if($this->form_validation->run() == true)
		  {
				$data = $this->remarks_model->array_from_post(array('subjectid', 'minimum', 'maximum', 'grade', 'remark'));

				$data['examid'] = $examid;
				$data['examyear'] = $examyear;
				$data['remarkid'] = $this->remarks_model->generate_unique_id();

				$this->remarks_model->delete($data['remarkid']); // to avoid duplicate while synching/inserting
				$this->remarks_model->save_update($data);

				$message = 'Remark Details>>><br/>'
						. 'Exam Name: ' . $this->registration_model->getExamName_From_Id($examid) . '<br/>'
						. 'Exam Year: ' . $examyear . '<br/>'
						. 'Subject: ' . $this->registration_model->get_subject($data['subjectid']) . '<br/>'
						. 'Min: ' . $data['minimum'] . '<br/>'
						. 'Max: ' . $data['maximum'] . '<br/>'
						. 'Grade: ' . $data['grade'] . '<br/>'
						. 'Remark: ' . $data['remark'] . '<br/>';


				$this->audittrail_model->log_audit($this->session->userdata('user_id'), 'ADD', $this->session->userdata('user_name'), 'Added SUBJECT DISTRIBUTED REMARK SETTING Details for '
			   . $this->registration_model->getExamName_From_Id($examid) . ' ' . $examyear, $message, $this->data['edc_detail']->edcid);


				//$this->session->set_flashdata('msg', 'Remark Added Successfully');
				//redirect(site_url('admin/remarks/subjectdistribution/'.$examid.'/'.$examyear));

				$this->data['msg'] = 'Remark Added Successfully';
				$this->data['subview'] = 'admin/remark_subjectdistribution_page';
				$this->load->view('admin/template/_layout_main', $this->data);

        }
   else {
           if(validation_errors()) $this->data['errors'] = validation_errors();

		   if(!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone)
			   {

				   $this->data["subjects"] = "";

				   $subj = $this->db->get_where('t_remarks',array('edcid'=>$this->edcid,'examid'=>$examid,'examyear'=>$examyear));

				   if($subj->num_rows()>=1)
					 {
					     $this->data["subjects"] = $subj->result();
				     }


					$this->data['subview'] = 'admin/remark_subjectdistribution_page_exception';

			   }
			 else
			  {
		           $this->data['subview'] = 'admin/remark_subjectdistribution_page';
			  }

             $this->load->view('admin/template/_layout_main', $this->data);
        }

    }
    public function edit_remark_subjectdistribution_page_exception($rid)
	{
		$subj = $this->db->get_where('t_remarks',array('remarkid'=>$rid));

		if($subj->num_rows()<1)
			{

				show_404();

			}
		else
			{

				$sub = $subj->result()[0];

				$this->data["edit"] = true;

				$this->data['edcid'] = $this->edcid;

				$this->data['examid'] = $sub->examid;

				$this->data['examyear'] = $sub->examyear;

				$this->data["subj"] = $sub;

				$this->data["subjects"] = "";

				$subb = $this->db->get_where('t_remarks',array('edcid'=>$this->edcid,'examid'=>$sub->examid,'examyear'=>$sub->examyear));

				if($subj->num_rows()>=1)
				{
					$this->data["subjects"] = $subb->result();
				}


				$this->data['subview'] = 'admin/remark_subjectdistribution_page_exception';

				$this->load->view('admin/template/_layout_main', $this->data);
			}

	}
    public function delete_remark_subjectdistribution_page_exception($rid,$examid,$examyear)
  {
		if($this->db->delete('t_remarks', array('remarkid' => $rid)))
	     {
			 $subj = $this->db->get_where('t_remarks',array('remarkid'=>$rid));

			 $this->session->set_flashdata('error1', "Remark deleted, try again");

			 redirect('admin/remarks/subjectdistribution/'.$examid.'/'.$examyear);

		 }


   }
  
    public function scoredistributionexception()
	  {
		  $this->form_validation->set_rules($this->remarks_model->_rules_remark);

		  if($this->form_validation->run() == true)
		  {

			 if($this->input->post('edit'))
			  {


				  $data =

				  array(
						  'minimum'=>$this->input->post('minimum',true),
						  'maximum'=>$this->input->post('maximum',true),
						  'grade'=>$this->input->post('grade',true),
						  'remark'=>$this->input->post('remark',true),
						  'datemodified'=>date('Y-m-d G:i:s'),
				      );

					  $this->db->where('remarkid',$this->input->post('remarkid'));

					  $this->db->update('t_remarks',$data);


				  if($this->db->affected_rows()>=1)
				  {
					  $this->session->set_flashdata('msg', 'Remark Updated Successfully');
					  redirect('admin/remarks/subjectdistribution/'.$this->input->post('examid').'/'.$this->input->post('examyear'));
				  }
				  else
				  {
					  $this->session->set_flashdata('error1', "Remark not updated, try again");
					  redirect('admin/remarks/subjectdistribution/'.$this->input->post('examid').'/'.$this->input->post('examyear'));
				  }

			    }
			 else
			  {

			    $data =

				 array(
				        'remarkid'=>$this->remarks_model->generate_unique_id(),
						'minimum'=>$this->input->post('minimum',true),
						'maximum'=>$this->input->post('maximum',true),
						'grade'=>$this->input->post('grade',true),
						'remark'=>$this->input->post('remark',true),
						'edcid'=>$this->input->post('edcid'),
						'examid'=>$this->input->post('examid'),
						'datecreated'=>date('Y-m-d H:i:s'),
						'datemodified'=>date('Y-m-d H:i:s'),
						'examyear'=>$this->input->post('examyear')
				      );

			         $this->db->insert('t_remarks',$data);


				  if($this->db->affected_rows()>=1)
				  {
					  $this->session->set_flashdata('msg', 'Remark Added Successfully');
					  redirect('admin/remarks/subjectdistribution/'.$this->input->post('examid').'/'.$this->input->post('examyear'));
				  }
				  else
				  {
					  $this->session->set_flashdata('error1', "Remark not added, try again");
					  redirect('admin/remarks/subjectdistribution/'.$this->input->post('examid').'/'.$this->input->post('examyear'));
				  }



			  }


		  }

		 else
		  {

			  $this->data['subview'] = 'admin/remark_subjectdistribution_page_exception';

		  }

  }
    public function scoredistribution($examid = null, $examyear = null) {
        $examid || show_404();
        $examyear || show_404();

        $this->load->model('admin/registration_model');

        $this->data['remarks'] = $this->remarks_model->get_where(array('edcid' => $this->edcid, 'examid' => $examid, 'examyear' => $examyear));

        $validation_rules = $this->remarks_model->_rules;
        $validation_rules['subject'] = null;
        $this->form_validation->set_rules($validation_rules);
        if ($this->form_validation->run() == true) {
            $data = $this->remarks_model->array_from_post(array('minimum', 'maximum', 'grade', 'remark'));
            $data['edcid'] = $this->edcid;
            $data['subjectid'] = '';
            $data['examid'] = $examid;
            $data['examyear'] = $examyear;
            $data['remarkid'] = $this->remarks_model->generate_unique_id();

            $this->remarks_model->delete($data['remarkid']); // to avoid duplicate while synching/inserting
            $this->remarks_model->save_update($data);

            $message = 'Remark Details>>><br/>'
                    . 'Exam Name: ' . $this->registration_model->getExamName_From_Id($examid) . '<br/>'
                    . 'Exam Year: ' . $examyear . '<br/>'
                    . 'Min: ' . $data['minimum'] . '<br/>'
                    . 'Max: ' . $data['maximum'] . '<br/>'
                    . 'Grade: ' . $data['grade'] . '<br/>'
                    . 'Remark: ' . $data['remark'] . '<br/>';

            $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'ADD', $this->session->userdata('user_name'), 'Added SCORE DISTRIBUTED REMARK SETTING Details for '
                    . $this->registration_model->getExamName_From_Id($examid) . ' ' . $examyear, $message, $this->data['edc_detail']->edcid);

            $this->session->set_flashdata('msg', 'Remark Added Successfully');
            redirect(site_url('admin/remarks/scoredistribution/' . $examid . '/' . $examyear));
        }

        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;

        $this->data['subview'] = 'admin/remark_scoredistribution_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function delete_score_remark($remarkid, $examid, $examyear) {
        $remark_set = $this->remarks_model->get_all($remarkid);
        $this->remarks_model->delete($remarkid);

        $this->load->model('admin/registration_model');
        $message = 'Remark Details>>><br/>'
                . 'Exam Name: ' . $this->registration_model->getExamName_From_Id($examid) . '<br/>'
                . 'Exam Year: ' . $examyear . '<br/>'
                . 'Min: ' . $remark_set->minimum . '<br/>'
                . 'Max: ' . $remark_set->maximum . '<br/>'
                . 'Grade: ' . $remark_set->grade . '<br/>'
                . 'Remark: ' . $remark_set->remark . '<br/>';

        $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'DELETE', $this->session->userdata('user_name'), 'Deleted SCORE DISTRIBUTED REMARK SETTING Details for '
                . $this->registration_model->getExamName_From_Id($examid) . ' ' . $examyear, $message, $this->data['edc_detail']->edcid);

        $this->session->set_flashdata('msg1', 'Remark Deleted Successfully');
        redirect(site_url('admin/remarks/scoredistribution/' . $examid . '/' . $examyear));
    }

    public function update_score_remark($examid = null, $examyear = null) {
        $examid || show_404();
        $examyear || show_404();

        $validation_rules = $this->remarks_model->_rules;
        $validation_rules['subject'] = null;
        $this->form_validation->set_rules($validation_rules);
        if ($this->form_validation->run() == true) {
            $data = $this->remarks_model->array_from_post(array('minimum', 'maximum', 'grade', 'remark'));
            $remarkid = $this->input->post('remarkid');
            $this->remarks_model->save_update($data, $remarkid);

            $this->load->model('admin/registration_model');
            $message = 'Remark Details>>><br/>'
                    . 'Exam Name: ' . $this->registration_model->getExamName_From_Id($examid) . '<br/>'
                    . 'Exam Year: ' . $examyear . '<br/>'
                    . 'Min: ' . $data['minimum'] . '<br/>'
                    . 'Max: ' . $data['maximum'] . '<br/>'
                    . 'Grade: ' . $data['grade'] . '<br/>'
                    . 'Remark: ' . $data['remark'] . '<br/>';

            $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'UPDATE', $this->session->userdata('user_name'), 'Updated SCORE DISTRIBUTED REMARK SETTING Details for '
                    . $this->registration_model->getExamName_From_Id($examid) . ' ' . $examyear, $message, $this->data['edc_detail']->edcid);

            $this->session->set_flashdata('msg1', 'Remark Updated Successfully');
            redirect(site_url('admin/remarks/scoredistribution/' . $examid . '/' . $examyear));
        } else {
            $this->session->set_flashdata('error1', validation_errors());
            redirect(site_url('admin/remarks/scoredistribution/' . $examid . '/' . $examyear));
        }
    }

    public function delete_subject_remark($remarkid, $examid, $examyear) {
        $remark_set = $this->remarks_model->get_all($remarkid);
        $this->remarks_model->delete($remarkid);

        $this->load->model('admin/registration_model');
        $message = 'Remark Details>>><br/>'
                . 'Exam Name: ' . $this->registration_model->getExamName_From_Id($examid) . '<br/>'
                . 'Exam Year: ' . $examyear . '<br/>'
                . 'Subject: ' . $this->registration_model->get_subject($remark_set->subjectid) . '<br/>'
                . 'Min: ' . $remark_set->minimum . '<br/>'
                . 'Max: ' . $remark_set->maximum . '<br/>'
                . 'Grade: ' . $remark_set->grade . '<br/>'
                . 'Remark: ' . $remark_set->remark . '<br/>';

        $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'DELETE', $this->session->userdata('user_name'), 'Deleted SUBJECT DISTRIBUTED REMARK SETTING Details for '
                . $this->registration_model->getExamName_From_Id($examid) . ' ' . $examyear, $message, $this->data['edc_detail']->edcid);
        $this->session->set_flashdata('msg', 'Remark Deleted Successfully');
        redirect(site_url('admin/remarks/subjectdistribution/' . $examid . '/' . $examyear));
    }

    public function update_subject_remark($examid = null, $examyear = null, $subject_row = null) {
        $examid || show_404();
        $examyear || show_404();
        $subject_row || show_404();

        $validation_rules = $this->remarks_model->_rules;
        $validation_rules['subject'] = null;
        $this->form_validation->set_rules($validation_rules);
        if ($this->form_validation->run() == true) {
            $data = $this->remarks_model->array_from_post(array('minimum', 'maximum', 'grade', 'remark'));
            $remarkid = $this->input->post('remarkid');
            $this->remarks_model->save_update($data, $remarkid);

            //Enter Audit Log
            $this->load->model('admin/registration_model');
            $message = 'Remark Details>>><br/>'
                    . 'Exam Name: ' . $this->registration_model->getExamName_From_Id($examid) . '<br/>'
                    . 'Exam Year: ' . $examyear . '<br/>'
                    . 'Min: ' . $data['minimum'] . '<br/>'
                    . 'Max: ' . $data['maximum'] . '<br/>'
                    . 'Grade: ' . $data['grade'] . '<br/>'
                    . 'Remark: ' . $data['remark'] . '<br/>';

            $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'UPDATE', $this->session->userdata('user_name'), 'Updated SUBJECT DISTRIBUTED REMARK SETTING Details for '
                    . $this->registration_model->getExamName_From_Id($examid) . ' ' . $examyear, $message, $this->data['edc_detail']->edcid);

            //End Audit Log Entry

            $this->session->set_flashdata('msg' . $subject_row, 'Remark Updated Successfully');
            redirect(site_url('admin/remarks/subjectdistribution/' . $examid . '/' . $examyear));
        } else {
            $this->session->set_flashdata('error' . $subject_row, validation_errors());
            redirect(site_url('admin/remarks/subjectdistribution/' . $examid . '/' . $examyear));
        }
    }

}
