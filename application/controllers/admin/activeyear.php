<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Activeyear extends Admin_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('admin/activeyear_model');        
        $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Active_Year', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
    }
    
    public function save($id = NULL){
        $this->data['year_detail'] = $this->activeyear_model->get_all(NULL, TRUE);
        if(!count($this->data['year_detail'])){
           $this->data['year_detail']  = $this->activeyear_model->is_new();
        }
        
        $validation_rules = $this->activeyear_model->_rules;
        $this->form_validation->set_rules($validation_rules);
        
        if($this->form_validation->run() == TRUE){
          $data = $this->activeyear_model->array_from_post(array('activeyear'));
          
          $this->activeyear_model->save_update($data, $id) ;
          
          //save audit trail
          $audit = array();
          $audit['userid'] = $this->session->userdata('user_id');
          $audit['actiontype'] = 'UPDATE';
          $audit['message'] = 'User ['.$this->session->userdata('user_name').'] Updated The ACTIVE YEAR';
          $audit['details'] = 'Newly Updated Value = '.$data['activeyear'];
          $audit['edcid'] = $this->data['edc_detail']->edcid;
          $this->audittrail_model->save_update($audit);
          //End Audit Entry
          
          $this->session->set_flashdata('msg', '<strong>Active Year</strong> successfully Updated!');
          redirect(site_url('admin/activeyear/save/'.$this->data['year_detail']->id));
        }           
        
        $this->data['subview'] = 'admin/activeyear_page';        
        $this->load->view('admin/template/_layout_main', $this->data); 
    }
}

