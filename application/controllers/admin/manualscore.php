<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Manualscore extends Admin_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->model('admin/exam_model');
        $priviledges = explode("^", $this->session->userdata('user_role'));
        if (!in_array('Active_Year', $priviledges) && !in_array('Super_Administrator', $priviledges)) {
            redirect('login/logout');
        }
    }

    public function index() {

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if ($this->form_validation->run()) {
            $data = $this->exam_model->array_from_post(array('examid', 'examyear', 'hastheory'));
            $exam_detail = $this->exam_model->get_all($data['examid']);

            redirect(site_url('admin/manualscore/process/' . $data['examid'] . '/' . $data['examyear'] . '/' . $data['hastheory'] ));
        }

        $this->data['exams'] = $this->db
                ->where('edcid', $this->edcid)
                ->get('t_exams')
                ->result();
        $this->data['subview'] = 'admin/manual_score_select_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function process($examid, $examyear, $hastheory = 0) {
        $this->data['exam_detail'] = $this->db
                ->where('examid', $examid)
                ->get('t_exams')
                ->row();
        $this->data['examyear'] = $examyear;
        $this->data['hastheory'] = $hastheory;

        $this->data['allsubjects'] = $this->db
                ->where('examid', $examid)
                ->order_by('priority')
                ->get('t_subjects')
                ->result();

        #get score margin
        $subject_ids = '';
        foreach ($this->data['allsubjects'] as $subject) {
            $subject_ids .= "'" . $subject->subjectid . "',";
        }
        $subject_ids = rtrim($subject_ids, ',');

        $sql = "select * from t_scores_margin where subjectid in (" . $subject_ids . ") and edcid = '" . $this->edcid . "'";
        $this->data['maxscoredata'] = $maxdata = $this->db->query($sql)->result();
        $m_data = array();
        foreach ($maxdata as $mdata) {
            $m_data[$mdata->subjectid] = array(
                'maxca' => $mdata->maxca,
                'maxpractical' => $mdata->maxpractical,
                'maxexam' => $mdata->maxexam
            );
        }
        //print_r( $m_data);
        //exit;
        $this->data['maxscoredata'] = $m_data;

        $this->data['subview'] = 'admin/manual_score_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }

    public function doGrade() {
        if ($this->_is_ajax()) {

            $data = array();

            $examno = $this->input->get('examno', TRUE);
            $examid = $this->input->get('examid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);
            $exam_score = $this->input->get('examscore', TRUE);
            $subjectid = $this->input->get('subjectid', TRUE);
            $hastheory = $this->input->get('hastheory', TRUE);

            $sql = "select t_scores.candidateid, ca_score, exam_score, practical_score, firstchoice, secondchoice from t_scores 
                    inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                    where t_candidates.examid = '" . $examid . "' 
                    and t_candidates.examyear = '" . $examyear . "'
                    and t_scores.subjectid = '" . $subjectid . "'
                    and t_candidates.examno = '" . $examno . "'
                    and t_scores.edcid = '" . $this->edcid . "' ";

            $records = $this->db->query($sql)->row();
          
            $updateSql = "";
            if (count($records)) {

                        if($hastheory == 1){
                          $exam_score = (($records->exam_score < 0) ? 0 : $records->exam_score) + (($exam_score < 0) ? 0 : $exam_score);
                        }
                $total_score = (($records->ca_score < 0) ? 0 : $records->ca_score) + (($exam_score < 0) ? 0 : $exam_score) + (($records->practical_score < 0) ? 0 : $records->practical_score);
                if ($exam_score < 0)
                    $total_score = 0;

                //UPDATE SCORE
                $updateSql .= "update t_scores set 
                                exam_score = " . $exam_score . ", total_score = " . $total_score . " 
                                where subjectid = '" . $subjectid . "' 
                                and examid = '" . $examid . "' 
                                and examyear = '" . $examyear . "' 
                                and edcid = '" . $this->edcid . "' 
                                and candidateid = '" . $records->candidateid . "'";

                $this->db->query($updateSql);
                logSql($updateSql, $this->edcid);

                #if exam allows posting, then perform posting
                $exam_detail = $this->db
                        ->where('examid', $examid)
                        ->get('t_exams')
                        ->row();

                if ($exam_detail->hasposting) {
                    
                    $cutoff = $this->db
                                    ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                                    ->row()
                            ->cutoff;
                    if ($cutoff) {
                        $sum_param = get_standardscore_rep($examid, $examyear);
                        
                        $sql = "select sum(".$sum_param.") as over_total from t_scores 
                                where examid = '" . $examid . "' 
                                and examyear = '" . $examyear . "' 
                                and edcid = '" . $this->edcid . "' 
                                and candidateid = '" . $records->candidateid . "'";
                        $scoreData = $this->db->query($sql)->row();
                        if ($scoreData->over_total >= $cutoff) {

                            $this->load->helper('array');
                            $rand_pick = random_element(array('firstchoice', 'secondchoice'));
                            $sql = "update t_candidates set placement = '" . $records->$rand_pick . "' where candidateid = '" . $records->candidateid . "'; ";
                            $this->db->query($sql);
                            logSql($sql, $this->edcid);
                        }
                    }
                }

                $success_data['success'] = "Successfull";
                $success_data['err'] = "1";
                echo json_encode($success_data);
                return;
            } else {
                $success_data['success'] = $examno . " NOT FOUND";
                $success_data['err'] = "0";
                echo json_encode($success_data);
                return;
            }
        } else {
            $success_data['success'] = "AJAX REQUEST ERROR";
            $success_data['err'] = "0";
            echo json_encode($success_data);
            return;
        }
    }

    public function _is_ajax() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

}
