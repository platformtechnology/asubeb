<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Maxwell
 */
class Regdeadline extends Admin_Controller {
    //put your code here
    function __construct() {
        parent::__construct();
        $priviledges = explode("^", $this->session->userdata('user_role')); 
        if(!in_array('Reg_Deadline', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
        $this->load->model('admin/deadline_model');
    }
    
    public function save($id = NULL){
       
        $validation_rules = $this->deadline_model->_rules;
        
        if($id == NULL) {
            $this->data['deadline_detail']  = $this->deadline_model->is_new();
            $validation_rules['exam']['rules'] .= '|is_unique[t_deadlines.examid]';
        }   
        else $this->data['deadline_detail'] = $this->deadline_model->get_all($id);
        
        $this->form_validation->set_rules($validation_rules);
        
        if($this->form_validation->run() == TRUE){
          $data = $this->deadline_model->array_from_post(array('examid', 'closedate'));
          $data['edcid'] = $this->data['edc_detail']->edcid;
          
          //CHECK FOR DUPLICATE INORDER RECORDS WHEN UPDATING
          if($id != NULL){
              $this->db->where('examid', $data['examid']);
              $this->db->where_not_in('id', $id);
              if(count($this->deadline_model->get_all())){
                  $this->session->set_flashdata('error', 'The Newly Selected Exam Already Has a Deadline');
                  redirect(site_url('admin/regdeadline/save/'.$id));
              }
          }
          
          $this->deadline_model->save_update($data, $id) ;
          
          $this->load->model('admin/registration_model');
          $messsage = 'Exam Name: '.$this->registration_model->getExamName_From_Id($data['examid']).'<br/>'
                  . 'Deadline: '.$data['examid'];
          $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'UPDATE', 
                    $this->session->userdata('user_name'), 'UPDATED Registration Deadline for Exam',
                    $messsage, $this->data['edc_detail']->edcid);
          
          $this->session->set_flashdata('msg', '<strong>Registration Deadline </strong> Successfully Updated!');
          redirect(site_url('admin/regdeadline/save/'));
        }           
        
        //Add styles and scripts for the datetime
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
        
        //for displaying all deadlines and Exams
        $this->data['deadlines'] = $this->db->where('edcid', $this->data['edc_detail']->edcid)->get('t_deadlines')->result();
        $this->data['exams'] = $this->db->get('t_exams')->result();
        $this->load->model('admin/subjects_model');
        
        $this->data['subview'] = 'admin/deadline_page';        
        $this->load->view('admin/template/_layout_main', $this->data); 
    }
    
    public function delete($id = null) {
        $id || show_404();
        $deadline_detail = $this->deadline_model->get_all($id);
        $this->deadline_model->delete($id);
        
        $this->load->model('admin/registration_model');
          $messsage = 'Exam Name: '.$this->registration_model->getExamName_From_Id($deadline_detail->examid).'<br/>'
                  . 'Deadline: '.$deadline_detail->closedate;
          $this->audittrail_model->log_audit($this->session->userdata('user_id'), 'DELETE', 
                    $this->session->userdata('user_name'), 'DELETED The Registration Deadline for Exam',
                    $messsage, $this->data['edc_detail']->edcid);
          
        $this->session->set_flashdata('updated_msg', '<strong>Registration Deadline </strong> DELETED Successfully!');
        redirect(site_url('admin/regdeadline/save/'));
    }
}
