<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends Admin_Controller {
    
    function __construct() {
        parent::__construct();
        
        $priviledges = explode("^", $this->session->userdata('user_role')); 
         if(!in_array('Report_Viewing', $priviledges) && !in_array('Super_Administrator', $priviledges)){
            redirect('login/logout');
        }
    }
    
    public function index() {  
        $this->data['subview'] = 'admin/search_page';
        $this->load->view('admin/template/_layout_main', $this->data);
    }
    
    function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }
    
    public function fetchCandidates(){
        if($this->_is_ajax()){
            $param = $this->input->get('param', TRUE);
            $pinSearch = $this->input->get('isPinSearch', TRUE);
           
            $sql = "select t_candidates.*, t_exams.examname, t_edcs.edcname, t_schools.schoolname  
                from t_candidates
                inner join t_exams on t_candidates.examid = t_exams.examid 
                inner join t_edcs on t_candidates.edcid = t_edcs.edcid 
                inner join t_schools on t_candidates.schoolid = t_schools.schoolid
                ";
            if($pinSearch == "1") $sql .= "inner join t_pins on t_candidates.candidateid = t_pins.candidateid ";

            $sql .= "where t_candidates.examno ilike '%" . $param . "%' ";
            if($pinSearch == "1") $sql .= "or t_pins.pin like '%" . $param . "%' ";
            $sql .= "or (t_candidates.firstname || ' ' || t_candidates.othernames) ilike '%" . $param . "%' order by t_candidates.examno; ";
            
            $candidates = $this->db->query($sql)->result();
            
            
            if(count($candidates)){
          
            $table_data = '<table class="table table-bordered table-condensed" id="example"> '
                      .'<thead>'
                      .'  <tr>'
                       .'   <th>Sn</th>'
                       .'   <th>Examno</th>'
                       .'   <th>Pin</th>'
                        .'   <th>Exam</th>'
                       .'   <th>Fullname</th>'
                        .'  <th>Gender</th>'
                        .'  <th>School</th>   '
                        .'  <th>Edc</th>   '
                       .' </tr>'
                      .'</thead>'
                     .' <tbody>';
                    
                    $sn =0;
                    foreach ($candidates as $candidate) {
                         $table_data .= '<tr>';
                            $table_data .= '<td>' . ++$sn .'</td>';
                            $table_data .= '<td><strong><a href="'. site_url('admin/candidates/view/'.$candidate->candidateid).'" target="_blank">' . $candidate->examno .'</a></strong></td>';
                            $table_data .= '<td><strong>' . $this->_get_pin($candidate->candidateid) .'</strong></td>';
                            $table_data .= '<td>' . strtoupper($candidate->examname) .'</td>';
                            $table_data .= '<td><strong>' . ucwords(strtolower($candidate->firstname . ' ' . $candidate->othernames)) .'</strong></td>';
                            $table_data .= '<td>' . strtoupper($candidate->gender) .'</td>';
                            $table_data .= '<td>' . ucwords(strtolower($candidate->schoolname)) .'</td>';
                            $table_data .= '<td>' . ucwords(strtolower($candidate->edcname)) .'</td>';
//                             $table_data .= '<input type="hidden" id="candidateid'.$sn.'" name="candidateid'.$sn.'" value="'.$candidate->candidateid.'" />';
//                             $table_data .= '<td><input onKeyUp="examPost(\''.$sn.'\')" type="text" id="examno'.$sn.'" name="examno'.$sn.'" value="'.$candidate->examno.'" style="border: 0px;" /><span id="exam-result'.$sn.'"></span></td>';
//                            $table_data .= '<td><input onKeyUp="firstnamePost(\''.$sn.'\')" type="text" id="firstname'.$sn.'" name="firstname'.$sn.'" value="' . (strtoupper($candidate->firstname)) .'" style="border: 0px;" /><span id="fname-result'.$sn.'"></span></td>';
//                            $table_data .= '<td><input onKeyUp="othernamePost(\''.$sn.'\')" type="text" id="othername'.$sn.'" name="othername'.$sn.'" value="' . (strtoupper($candidate->othernames)) .'" style="border: 0px;" /><span id="oname-result'.$sn.'"></span></td>';
//                            $table_data .= '<td>' . strtoupper($candidate->gender) .'</td>';
//                            $table_data .= '<td>' . $candidate->dob .'</td>';
//                            $table_data .= '<td><input onKeyUp="phonePost(\''.$sn.'\')" type="text" id="phone'.$sn.'" name="phone'.$sn.'" value="' . $candidate->phone .'" style="border: 0px;" /><span id="phone-result'.$sn.'"></span></td>';
//                            $table_data .= '<td>'
//                                    . '<a href="'.site_url('admin/registration/delete/'. $candidate->candidateid.'/'.$examid.'/'.$examyear).'" onclick="return confirm(\'You are about to delete a record. Action cannot be undone. Are you sure you want to proceed?\');"><i class="glyphicon glyphicon-remove"></i></a> | '
//                                    . '<a href="'.site_url('admin/registration/row/'.$examid.'/'.$examyear.'/'.$candidate->candidateid).'" onclick="return confirm(\'You are about to Edit a Candidate. Are you sure you want to Edit?\');"><i class="glyphicon glyphicon-edit"></i></a>'
//                                    . '</td>';
                         $table_data .= '</tr>';
                    }
                
                $table_data .= '</tbody>'
                    . '</table>';
            
                echo $table_data;
            }
            else echo " -- NOTHING FOUND -- ";
        }
    }
    
    function _get_pin($candidateid){
        $sql = "select pin from t_pins where candidateid = '" . $candidateid . "' limit 1";
        $pin_data = $this->db->query($sql)->row();
        if(count($pin_data)){
            return $pin_data->pin;
        }
        
        return ' --- ';
    }
    
}
