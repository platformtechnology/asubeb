<?php
session_start();
class Candidates extends Asubeb_Controller {
    //put your code here

    function __construct() {
        parent::__construct();
       $this->load->model('admin/registration_model');
       $this->load->model('asubeb/candidate_model');
    }

    public function searchcandidates(){

		$this->data['registrants'] = array();

        $edcid = $this->data['edc_detail']->edcid;
       //for displaying zones ddl
        $this->db->where('t_zones.edcid', $edcid);
        $this->data['zones'] = $this->db->get('t_zones')->result();

        //for lga ddl
        $this->db->where('edcid', $edcid);
        $this->db->order_by('lganame');
        $this->data['lgas'] = $this->db->get('t_lgas')->result();


        //for exams ddl
        $this->db->where('edcid', $edcid);
        $this->db->where('hasposting', 1);
        $this->data['exams'] = $this->db->get('t_exams')->result();

        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');

        if($this->form_validation->run()){

            $data = $this->registration_model->array_from_post(array('examid', 'examyear', 'zoneid', 'lgaid', 'schoolid'));

            $this->_performFilter($data, $edcid);
            $this->db->like('zoneid', $data['zoneid']);
            $this->db->like('lgaid', $data['lgaid']);

			if($data["schoolid"]!=""):
            $this->db->like('schoolid', $data['schoolid']);
			endif;

            $this->data['registrants'] = $this->registration_model->get_all();
            $this->data['count'] = count($this->data['registrants']);

            $this->_saveCandidatesInSession(); // save the resultset in session so that it can be exported to excel


            if(isset($data['lgaid'])){
                $this->db->where('edcid', $edcid);
                $this->db->where('lgaid', $data['lgaid']);
                //$this->db->where('ispostingschool', 0);
                $this->db->order_by('schoolname');
                $this->data['schools'] = $this->db->get('t_schools')->result();
            }
        }

                 //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';


        $this->data['subview'] = 'asubeb/candidates_page';
        $this->load->view('asubeb/template/_layout_main', $this->data);

    }



	public function searchpendingcandidates(){
            $this->data['registrants'] = array();
            $edcid = $this->data['edc_detail']->edcid;
            //for displaying zones ddl
            $this->db->where('t_zones.edcid', $edcid);
            $this->data['zones'] = $this->db->get('t_zones')->result();
            //for lga ddl
            $this->db->where('edcid', $edcid);
            $this->db->order_by('lganame');
            $this->data['lgas'] = $this->db->get('t_lgas')->result();
            //for exams ddl
            $this->db->where('edcid', $edcid);
            $this->db->where('hasposting', 1);
            $this->data['exams'] = $this->db->get('t_exams')->result();
            $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
            if($this->form_validation->run()){

                    $cutoff_data = $this->db->get('t_cutoff')->row();
                    if (count($cutoff_data)){
                        $this->data['cutoff'] = $cutoff_data->cutoff;
                    }

                $data = $this->registration_model->array_from_post(array('examid', 'examyear', 'zoneid', 'lgaid', 'schoolid'));

                $sum_param = get_standardscore_rep($data['examid'], $data['examyear']);
               // $this->_performFilter($data, $edcid);
                $school_filter = "";
                $lga_filter = "";
                $zonal_filter = "";
                if($data["schoolid"]!=""):
                    $school_filter = "AND t_candidates.schoolid = '".$data["schoolid"]."'";
                endif;
                if($data["lgaid"]!=""):
                    $lga_filter = "AND t_candidates.lgaid = '".$data["lgaid"]."'";
                endif;
                if($data["zoneid"]!=""):
                    $zonal_filter = "AND t_candidates.zoneid = '".$data["zoneid"]."'";
                endif;


                $sql = "select t_candidates.candidateid,t_candidates.firstname,t_candidates.othernames,t_candidates.examno,t_candidates.firstchoice,t_candidates.secondchoice,t_candidates.thirdchoice,t_candidates.placement,t_lgas.lganame,sum(" . $sum_param . ") as totalscore from t_candidates
                            INNER JOIN t_scores ON t_scores.candidateid = t_candidates.candidateid
                            INNER JOIN t_lgas ON t_candidates.lgaid = t_lgas.lgaid
                            where placement = '' AND
                            t_candidates.examyear = '".$data['examyear']."' AND
                            t_candidates.examid = '".$data['examid']."' "
                           . $school_filter
                           . $lga_filter
                           . $zonal_filter.
                            " group by t_candidates.candidateid,t_candidates.firstname,t_candidates.othernames,t_candidates.examno,t_candidates.firstchoice,t_candidates.secondchoice,t_candidates.thirdchoice,t_candidates.placement,t_lgas.lganame
                            having sum(" . $sum_param . ")  > '".$this->data['cutoff']."'"
                            . " ORDER BY t_candidates.examno ASC, t_lgas.lganame ASC";
                $this->data['registrants'] = $this->db->query($sql)->result();


                /*
                $this->db->like('zoneid', $data['zoneid']);
                $this->db->like('lgaid', $data['lgaid']);
                $this->db->join('t_scores','t_scores.candidateid = t_candidates.candidateid','inner');
                if($data["schoolid"]!=""):
                    $this->db->where('schoolid', $data['schoolid']);
                endif;
                $this->db->where('placement','');
                $this->data['registrants'] = $this->registration_model->get_all();
                 *
                 */


                $this->data['count'] = count($this->data['registrants']);
                $this->_saveCandidatesInSession(); // save the resultset in session so that it can be exported to excel


                if(isset($data['lgaid'])){
                    $this->db->where('edcid', $edcid);
                    $this->db->where('lgaid', $data['lgaid']);
                    //$this->db->where('ispostingschool', 0);
                    $this->db->order_by('schoolname');
                    $this->data['schools'] = $this->db->get('t_schools')->result();
                }
            }
                 //Add styles and scripts for the datable
            $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') .'" rel="stylesheet">';
            $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
            $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
            $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
            $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';
            $data = array();
            if($this->input->post('zoneid')){
                $this->db->like('zoneid',$this->input->post('zoneid'));
            }

            $this->db->where('edcid',$edcid);
            $this->db->where('ispostingschool',1);
            $this->db->where('issecondary',1);
            $this->db->order_by('schoolname');
            $this->data['postingschools'] = $this->db->get('t_schools')->result();
            $this->data['subview'] = 'asubeb/pending_candidates_page';
            $this->load->view('asubeb/template/_layout_main', $this->data);

    }


    private function _performFilter($data, $edcid){
		// exam year not set  but examid is set
        if(empty($data['examyear']) && (!empty($data['examid']))){
            $this->db->where(array('edcid'=>$edcid, 'examid'=>$data['examid']));
        }

		//exam year set and examid set
        else if((!empty($data['examyear'])) && (!empty($data['examid']))){
            $this->db->where(array('examyear'=>$data['examyear'], 'edcid'=>$edcid, 'examid'=>$data['examid']));
        }

      else if((!empty($data['examyear'])) && empty($data['examid'])){
            $this->db->where(array('edcid'=>$edcid, 'examyear'=>$data['examyear']));
        }
	else{
              $this->db->where(array('edcid'=> $edcid));
        }
    }



    public function _saveCandidatesInSession(){
        $_SESSION['platform_candidates'] = $this->data['registrants'];
        //$this->session->set_userdata('platform_candidates', $this->data['registrants']);
    }

    public function export_to_excel(){
            $this->load->helper('excel');
            export_to_excel($_SESSION['platform_candidates'], 'Candidate_Report');
            //print_r($this->session->userdata('platform_candidates')); exit;
    }

    public function view($candidateid, $printcandidate = null) {
        $this->data['reg_info'] = $this->registration_model->get_all($candidateid);
        if (count($this->data['reg_info'])) {
        //GET PIN INFO
            $this->db->where('candidateid',$this->data['reg_info']->candidateid);
            $this->data['candidate_pin_info'] = $this->db->get('t_pins')->row();
        }

        count($this->data['reg_info']) || show_404();

        //Get Exam Detail that the candidate registered for
        $this->load->model('admin/exam_model');
        $this->data['exam_info'] = $this->exam_model->get_all($this->data['reg_info']->examid);

        $this->data['subjects'] = $this->db->get_where('t_registered_subjects', array('candidateid'=>$candidateid))->result();

        $this->data['passport'] = base_url('resources/images/passports/'.$candidateid.'.jpg');
        if(!file_exists('./resources/images/passports/'.$candidateid.'.jpg')){
            $this->data['passport'] = get_img('no_image.jpg');
        }

        //Add styles for the passport display
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';
        //$this->data['page_level_scripts'] = '<script src="' . base_url('resources/js/bootstrap-fileupload.js') . '"></script>';

        $this->data['subview'] = 'asubeb/view_candidate_page';
        $this->load->view('asubeb/template/_layout_main', $this->data);

    }

 public function postingslip() {
        $this->data['registrants'] = array();
        $edcid = $this->data['edc_detail']->edcid;
       //for displaying zones ddl
        $this->db->where('t_zones.edcid', $edcid);
        $this->data['zones'] = $this->db->get('t_zones')->result();
        //for lga ddl
        $this->db->where('edcid', $edcid);
        $this->db->order_by('lganame');
        $this->data['lgas'] = $this->db->get('t_lgas')->result();

        //for exams ddl
        $this->db->where('edcid', $edcid);
        $this->db->where('hasposting', 1);
        $this->data['exams'] = $this->db->get('t_exams')->result();

        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if($this->form_validation->run()){
            $data = $this->registration_model->array_from_post(array('examid', 'examyear', 'zoneid', 'lgaid', 'schoolid'));
            $zonal_filter = $data['zoneid'];
            $school_filter = $data['schoolid'];
        
            $sum_param = get_standardscore_rep($data['examid'],$data['examyear']);
            $zonal_filter_query = $zonal_filter != 'All'? " AND t_candidates.zoneid = '".$zonal_filter."'": '';
            
            //TOGGLE SCHOOL FILTER
            $school_filter_query = $school_filter!='' ? " AND t_candidates.schoolid = '".$school_filter."'": '';
                
            //GET CANDIDATES
		    // to break up the process incase of timeout, select oly candidates that has not been posted by add where t_candidates.placement==null
            $sql="SELECT t_candidates.candidateid, t_candidates.examno, t_candidates.placement, t_candidates.othernames, t_candidates.firstname,sum(".$sum_param .") as totalscore "
                .",t_schools.schoolname FROM t_candidates INNER JOIN t_scores
                 ON t_candidates.candidateid = t_scores.candidateid".
                " LEFT JOIN t_schools on t_schools.schoolid = t_candidates.placement 
                 WHERE t_candidates.examid = '" . $data['examid'] . "'
                 AND t_candidates.examyear = '" . $data['examyear'] . "'
                 AND t_candidates.placement!=''
                 $zonal_filter_query
                 $school_filter_query
                 GROUP BY t_candidates.schoolid, t_candidates.firstname,t_candidates.examno,t_candidates.candidateid,t_candidates.placement,t_candidates.othernames ".
                 ",t_schools.schoolname
                 ORDER BY t_candidates.firstname DESC";
                $candidates = $this->db->query($sql)->result();
                $this->data["registrants"] = $candidates;
              if(isset($data['lgaid'])){
                $this->db->where('edcid', $edcid);
                $this->db->where('lgaid', $data['lgaid']);
                //$this->db->where('ispostingschool', 0);
                $this->db->order_by('schoolname');
                $this->data['schools'] = $this->db->get('t_schools')->result();
              }
        }

        $this->data["examyear"] = isset($data['examyear'])? substr($data['examyear'],2,3): ''; 
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';

        $this->data['subview'] = 'asubeb/postingslip';
        $this->load->view('asubeb/template/_layout_main', $this->data);

    }


	public function postcandidate() {

      if($this->input->post('candidateid'))
	  {
		$data["placement"]  = $this->input->post('schoolid');
		$this->db->where(array('candidateid'=>$this->input->post('candidateid')));
		$this->db->update('t_candidates',$data);
		if($this->db->affected_rows()>= 1)
		{
			echo 1;
		}
		else
		{
			echo 0;
		}

	  }
	  else
	  {
		  redirect('asubeb/candidates/searchpendingcandidates');
	  }

    }


    public function printdetails() {
        $this->data['printdata'] = $_SESSION['platform_candidates'];
        $this->load->view('admin/print_batch_page', $this->data);
    }

    public function school_ajaxdrop(){
        if($this->_is_ajax()){
            $lgaid = $this->input->get('lgaid', TRUE);
            $zoneid = $this->input->get('zoneid', TRUE);

            $this->db->select('schoolid as lgaid, schoolname as lganame');
            if(trim($lgaid) == false && trim($zoneid) != false) {
                $this->db->where('zoneid', $zoneid);
            }else{
                $this->db->where('lgaid', $lgaid);
                $this->db->where('lgaid !=', '');
            }
            $this->db->order_by('schoolname');
            $query = $this->db->get('t_schools');


            $data['lgas'] = array();
            if ($query->num_rows() > 0)
            {
              $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        }else{
            echo "Apparently is_ajax returned false!";
            show_error('This method can only be accessed internally.', 404);
        }
    }

    function _is_ajax(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }




}
