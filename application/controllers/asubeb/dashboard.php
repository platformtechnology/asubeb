<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Asubeb_Controller {
    
    function __construct() {
        parent::__construct();
    }
    
    public function index(){
        $this->data['exams']  = $this->db->get('t_exams')->result();
        
        $this->data['page_level_scripts'] = '<link href="' . base_url('resources/vendors/morris/morris.css') .'" rel="stylesheet">';
        
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/raphael-min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/morris/morris.min.js') . '"></script>';
        
       
        // get the EDC that would be selected and reported for
        $this->data['selectedEdc'] = $this->data['edc_detail']->edcid;

        // select distinct examyear for use in creating JSON data 
        $sql = "select distinct examyear from t_candidates where edcid = '".$this->data['selectedEdc']."'";

        //sql query to get count of students per exam registered for all exam year - used for JSON
        $candidsql = "select count(t_candidates.*) as candidatecount, t_exams.examname, t_candidates.examyear from t_candidates 
                inner join t_exams 
                on t_candidates.examid = t_exams.examid "
                //inner join t_pins on t_candidates.candidateid = t_pins.candidateid 
                ."where t_candidates.edcid = '" . $this->data['selectedEdc'] . "' ";

         $examsql = $candidsql . "group by t_exams.examname, t_candidates.examyear 
                order by t_exams.examname";


        //get student summary - count of student per exam for the active year
        $candidate_summary_sql = $candidsql . "and t_candidates.examyear = '" . $this->data['activeyear'] . "' group by t_exams.examname, t_candidates.examyear 
                order by t_exams.examname";
        $this->data['candidate_summary'] = $this->db->query($candidate_summary_sql)->result();

        $objectArray = array();
        $examTypeArray = array();
        $ykey = array();

        //prepare JSON data
        $yearResultSet = $this->db->query($sql)->result();
        $examResultSet = $this->db->query($examsql)->result();
        foreach ($yearResultSet as $yeardata) {

            $bufferClass = new stdClass();
            $bufferClass->examyear = $yeardata->examyear;

            $sn = 0;
            foreach($examResultSet as $examdata){
                $exam = 'examtype'.++$sn;
                $count = 'count'.$sn;

                if($yeardata->examyear == $examdata->examyear){

                    $bufferClass->$exam = intval($examdata->candidatecount);
                    $ykey[] = $exam;
                    if(!in_array($examdata->examname, $examTypeArray)){
                        $examTypeArray[] = $examdata->examname;
                    }
                    //$bufferClass->$count = intval($examdata->candidatecount);
                }
            }

            $objectArray[] = $bufferClass;
        }

        $this->data['reg_exams_by_year'] = json_encode($objectArray);
        $this->data['exam_series_name'] = json_encode($examTypeArray);
        $this->data['ykey'] = json_encode($ykey);
        
        
        
        //Add styles and scripts for the fileupload
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/css/bootstrap-fileupload.min.css') .'" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/js/bootstrap-fileupload.js') . '"></script>';

        $this->data['subview'] = 'asubeb/dashboard_page';
        $this->load->view('asubeb/template/_layout_main', $this->data);
    }
    
    public function update(){
        
        $this->form_validation->set_rules('edcname', 'Edc Name', 'trim|required');
        $this->form_validation->set_rules('activeyear', 'Active Year', 'trim|required');
        $this->form_validation->set_rules('edcweburl', 'Registration Website Url', 'trim|required');
        
        if($this->form_validation->run()){
            $edcname = $this->input->post('edcname');
            $edcweburl = $this->input->post('edcweburl');
            $activeyear = $this->input->post('activeyear');
            
            $this->db->update('t_activeyear', array('activeyear' => $activeyear));
            logSql($this->db->last_query(), $this->edcid);
            
            $this->db->where('edcid', $this->data['edc_detail']->edcid);
            $this->db->update('t_edcs', array('edcname' => $edcname, 'edcweburl' => $edcweburl));
            logSql($this->db->last_query(), $this->edcid);
            
            if(($upload_data = $this->_upload_edclogo()) != null){
                 $this->db->where('edcid', $this->data['edc_detail']->edcid);
                 $this->db->update('t_edcs', array('edclogo' => $upload_data['file_name']));
                 logSql($this->db->last_query(), $this->edcid);
            }
            
            $this->_upload_stamp();
            
            $this->session->set_flashdata('msg', 'SETUP UPDATED SUCCESSFULLY');
            redirect(site_url('asubeb/dashboard'));
        }else{
            $this->session->set_flashdata('error', validation_errors());
            redirect(site_url('asubeb/dashboard'));
        }
        
    }
    
    private function _upload_edclogo(){
       
        if(empty($_FILES['edclogo']['name'])){
            return null;
        }
        else{
            $config = array(
                 'upload_path' => APPPATH . config_item('edc_logo_path'),
                 'allowed_types' => 'jpg|png',
                 'overwrite' => FALSE,
                 'remove_spaces' => TRUE
             );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('edclogo'))
            {
               $this->session->set_flashdata('error', 'Edc Logo - '.$this->upload->display_errors());
               redirect(site_url('asubeb/dashboard'));
            } 

            return $this->upload->data();
        }
    }
    
    private function _upload_stamp(){
       
        if(empty($_FILES['stamp']['name'])){
            return null;
        }
        else{
            $config = array(
                 'upload_path' => config_item('edc_logo_path'),
                 'allowed_types' => 'jpg|png',
                 'overwrite' => TRUE,
                 'remove_spaces' => TRUE,
                 'file_name' => 'stamp.png'
             );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('stamp'))
            {
               $this->session->set_flashdata('error', 'Edc Stamp - '.$this->upload->display_errors());
               redirect(site_url('asubeb/dashboard'));
            } 

            return $this->upload->data();
        }
    }
    
    
}
