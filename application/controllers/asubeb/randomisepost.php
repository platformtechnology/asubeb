<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Randomisepost extends Asubeb_Controller {

    function __construct() {
        parent::__construct();
		$this->load->model('admin/registration_model');
		$this->load->model('admin/report_model');
		$this->load->model('admin/zones_model');

        }

    public function index(){
        set_time_limit(0);
        $this->load->helper('array');
        $edcid = $this->data['edc_detail']->edcid;
        $this->db->where('edcid', $edcid);
        $this->db->where('hasposting', 1);
        $this->data['exams'] = $this->db->get('t_exams')->result();

        $this->db->where('edcid', $edcid);
        $this->db->order_by('zonename ASC');
        $this->data['zones'] = $this->zones_model->get_all();

        $this->data['candidates'] = array();
        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        $this->form_validation->set_rules('zoneid', 'Zone ID', 'trim|required');
        if($this->form_validation->run() == TRUE){
            $examid = $this->input->post('examid');
            $examyear = $this->input->post('examyear');
            $zonal_filter = $this->input->post('zoneid');

            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;

            $sum_param = get_standardscore_rep($examid, $examyear);
            if(trim($cutoff) == false) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('asubeb/randomisepost'));
            }

                //TOGGLE ZONAL FILTER
                if($zonal_filter != 'All'){
                    $zonal_filter_query = " and t_candidates.zoneid = '".$zonal_filter."'";
                }
                else{
                    $zonal_filter_query = "";
                }

                //GET CANDIDATES
		// to break up the process incase of timeout, select oly candidates that has not been posted by add where t_candidates.placement==null
                $sql="select t_candidates.candidateid, t_candidates.firstchoice, t_candidates.secondchoice, t_candidates.thirdchoice
                from t_candidates inner join t_scores
                on t_candidates.candidateid = t_scores.candidateid
                where t_candidates.examid = t_scores.examid
                and t_candidates.examyear = t_scores.examyear
                and t_candidates.edcid = t_scores.edcid
                and t_candidates.examid = '" . $examid . "'
                and t_candidates.examyear = '" . $examyear . "'
                and t_candidates.edcid = '" . $this->data["edc_detail"]->edcid . "'
                and t_candidates.placement=''
                and (t_candidates.firstchoice != '0'
                or t_candidates.secondchoice != '0'
                or t_candidates.thirdchoice != '0')
                and t_candidates.placement=''
                $zonal_filter_query
                group by t_candidates.candidateid, t_candidates.firstchoice, t_candidates.secondchoice, t_candidates.thirdchoice
                having sum(" . $sum_param . ") >= " . $cutoff
                        ."order by random()";
                $candidates = $this->db->query($sql)->result()d;
                //DO SOME HOUSEKEEPING... UPDATE SCHOOL CAPACITY TO 50 FOR SCHOOLS WITHOUT CAPACITIES
			// set school capacities for schools not set to default of 50
                $data['school_capacity'] = 170;
                $this->db->where('ispostingschool','1');
                $this->db->where('school_capacity',null);
                $this->db->or_where('school_capacity',0);
                $this->db->update('t_schools',$data);
                //GET POSTING SCHOOLS
                //TOGGLE WITH ZONE
                //if($zonal_filter != 'All'){
                 //   $this->db->where('zoneid',$zonal_filter);
                //}
                $this->db->where('ispostingschool','1');
                $this->db->select('schoolid,school_capacity');
                $schools = $this->db->get('t_schools')->result();

                foreach($schools as $school):
			 // set schools without capacity value to take 50 students by default
                    $schoolcapacities[$school->schoolid] = $school->school_capacity!=NULL || $school->school_capacity!=0? $school->school_capacity: 50;
                    $schoolallocatedstudent[$school->schoolid] = 0;
		endforeach;

          $postcount = 0;
          foreach($candidates as $candidate)
		    {
			 // check if first choice school is not filled
			  if($candidate->firstchoice != '' && $candidate->firstchoice != '0' && $schoolallocatedstudent[$candidate->firstchoice] < $schoolcapacities[$candidate->firstchoice])
				{
				  // post candidate to first choice school
				  $schoolallocatedstudent[$candidate->firstchoice] = $schoolallocatedstudent[$candidate->firstchoice] + 1;
				  $this->registration_model->save_update(array('placement' =>$candidate->firstchoice),$candidate->candidateid);
                                  $postcount++;
				  continue;
				}
			 else if($candidate->secondchoice!= '' && $candidate->secondchoice != '0'  && $schoolallocatedstudent[$candidate->secondchoice] < $schoolcapacities[$candidate->secondchoice])
			  {
				 // post candidate to second choice school
                                    $schoolallocatedstudent[$candidate->secondchoice] = $schoolallocatedstudent[$candidate->secondchoice] + 1;
                                    $this->registration_model->save_update(array('placement' =>$candidate->secondchoice),$candidate->candidateid);
                                    $postcount++;
                                    continue;
			  }
			  elseif($candidate->thirdchoice != '' && $candidate->thirdchoice != '0' &&  $schoolallocatedstudent[$candidate->thirdchoice] < $schoolcapacities[$candidate->thirdchoice])
			  {
				  $schoolallocatedstudent[$candidate->thirdchoice] = $schoolallocatedstudent[$candidate->thirdchoice] + 1;
				  $this->registration_model->save_update(array('placement' =>$candidate->thirdchoice),$candidate->candidateid);
                                  $postcount++;
				  continue;
			  }
			  else
			  {
				  $this->registration_model->save_update(array('placement' =>''),$candidate->candidateid);
			  }
           }

            $this->session->set_flashdata('msg', $postcount.' POSTING PERFORMED SUCCESSFULLY');
            redirect(site_url('asubeb/randomisepost'));
        }

        $this->data['subview'] = 'asubeb/randomise_post_page';
        $this->load->view('asubeb/template/_layout_main', $this->data);
    }

    public function reset_posts() {

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {

            $examid = $this->input->get('examid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);
            $sql = "update t_candidates set placement = ''
                where examid = '" . $examid . "'
                and examyear = '" . $examyear . "'
                and edcid = '" . $this->edcid . "' ";

            $this->db->query($sql);

            $this->session->set_flashdata('msg', 'ALL POSTING HAS BEEN RESET SUCCESSFULLY');
            $success_data['success'] = "Successfull";
            $success_data['err'] = "1";
            echo json_encode($success_data);
            return;
        }
    }

}
