<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends Asubeb_Controller {

    public function __construct() {
        parent::__construct();
        $this->edcid = $this->data['edc_detail']->edcid;
        $this->load->model('asubeb/registration_model');
        $this->load->model('asubeb/report_model');
        $this->load->model('asubeb/remarks_model');
        $this->load->model('asubeb/exam_model');

        //$this->data['sql'] = $this->load->database('sql', TRUE);
    }

    public function index() {

        $this->data['exams'] = $this->exam_model->get_where(array('edcid' => $this->edcid,'hasposting'=>'1'));

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        if ($this->form_validation->run()) {
            $data = $this->exam_model->array_from_post(array('examid', 'examyear'));
            $exam_detail = $this->exam_model->get_all($data['examid']);

            redirect(site_url('asubeb/reports/reporting/' . $data['examid'] . '/' . $data['examyear']));
        }

        $this->data['subview'] = 'asubeb/report_page';
        $this->load->view('asubeb/template/_layout_main', $this->data);
    }

    public function attendance($examid, $examyear) {
        $examid || show_404();
        $examyear || show_404();
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->form_validation->set_rules('columns', 'columns', 'trim');
        if ($examdetail->haszone)
            $this->form_validation->set_rules('attendancezoneid', 'Zone', 'trim|required');
        else
            $this->form_validation->set_rules('attendancelgaid', 'Lga', 'trim|required');

        if ($this->form_validation->run()) {
            $data = $this->exam_model->array_from_post(array('attendancelgaid', 'attendancezoneid', 'attendanceschoolid', 'columns'));
            $sql = '';
            if ($examdetail->haszone) {
                $sql = "select t_zones.zoneid, t_zones.zonename, t_schools.schoolid, t_schools.schoolname, t_schools.schoolcode, count(*) as total
                    from t_candidates inner join t_schools on t_candidates.schoolid = t_schools.schoolid
                    inner join t_zones on t_candidates.zoneid = t_zones.zoneid
                    where t_zones.zoneid = t_schools.zoneid
                    and t_candidates.examid = '" . $examid . "' ";

                if ($data['attendanceschoolid'] == '')
                    $sql .= "and t_candidates.zoneid = '" . $data['attendancezoneid'] . "' ";
                else
                    $sql .= "and t_candidates.schoolid = '" . $data['attendanceschoolid'] . "' ";

                $sql .= "and t_candidates.edcid = '" . $this->edcid . "'
                    and t_candidates.examyear = '" . $examyear . "'
                    group by t_zones.zoneid, t_zones.zonename, t_schools.schoolid, t_schools.schoolname, t_schools.schoolcode";
                $this->data['group'] = $this->db->where('edcid', $this->edcid)->get('t_zones')->result();
            }else {
                $sql = "select t_lgas.lgaid, t_lgas.lganame, t_schools.schoolid, t_schools.schoolname, t_schools.schoolcode, count(*) as total
                    from t_candidates inner join t_schools on t_candidates.schoolid = t_schools.schoolid
                    inner join t_lgas on t_candidates.lgaid = t_lgas.lgaid
                    where t_lgas.lgaid = t_schools.lgaid
                    and t_candidates.examid = '" . $examid . "' ";

                if ($data['attendanceschoolid'] == '')
                    $sql .= "and t_candidates.lgaid = '" . $data['attendancelgaid'] . "' ";
                else
                    $sql .= "and t_candidates.schoolid = '" . $data['attendanceschoolid'] . "' ";

                $sql .= "and t_candidates.edcid = '" . $this->edcid . "'
                    and t_candidates.examyear = '" . $examyear . "'
                    group by t_lgas.lgaid, t_lgas.lganame, t_schools.schoolid, t_schools.schoolname, t_schools.schoolcode";
                $this->data['group'] = $this->db->where('edcid', $this->edcid)->get('t_lgas')->result();
            }

            $this->data['attendance_data'] = $this->db->query($sql)->result();
            $this->db->where('examid',$examid);
            $this->data['exam_subjects'] = $this->db->get('t_subjects')->result();


            if (trim($data['columns']) == '')
                $this->data['columns'] = array();
            else
                $this->data['columns'] = explode(",", $data['columns']);

            $this->data['title'] = 'ATTENDANCE REPORT';
            $this->data['examid'] = $examid;
            $this->data['examyear'] = $examyear;

           if ($examdetail->haszone){
               //ANAMBRA
               $this->load->view('asubeb/reports/reports_attendance_zone_page', $this->data);
           }
           else{
               //ABIA
                $this->load->view('asubeb/reports/reports_attendance_page', $this->data);
           }

        }else {
            $this->session->set_flashdata('error', validation_errors());
            redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
        }
    }

    public function search($examid, $examyear) {
        $examid || show_404();
        $examyear || show_404();

        $this->load->model('asubeb/report_model');
        $this->load->model('asubeb/registration_model');

        $this->form_validation->set_rules('examno', 'Exam No', 'trim|required');
        if ($this->form_validation->run() == true) {

            $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row();
            $cutoff = '';
            if ($examdetail->hasposting) {
                $cutoff = $this->db
                                ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                                ->row()
                        ->cutoff;
                if (empty($cutoff)) {
                    $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                    redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
                }
            }

            $this->data['cutoff'] = $cutoff;
            $this->data['candidates'] = $this->db
                    ->where('examno', $this->input->post('examno'))
                    ->where('examid', $examid)
                    ->where('edcid', $this->edcid)
                    ->get('t_candidates')
                    ->result();
            if (!count($this->data['candidates'])) {
                $this->session->set_flashdata('error1', 'Candidate Not Found For Selected Exam');
                redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
            }

            $this->data['title'] = 'CANDIDATE EXAMINATION REPORT';
            $this->data['examid'] = $examid;
            $this->data['examyear'] = $examyear;

            $this->load->model('asubeb/reg_subjects_model');
            $this->load->model('asubeb/scores_model');
            $this->load->model('asubeb/schools_model');

            $this->data['allsubjects'] = $this->db
                    ->where(array('examid' => $examid))
                    ->where('edcid', $this->edcid)
                    ->order_by('priority', 'asc')
                    ->get('t_subjects')
                    ->result();

            $this->load->view('asubeb/reports/reports_candidatesearch_page', $this->data);
        } else {
            $this->session->set_flashdata('error1', validation_errors());
            redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
        }
    }

    public function subjectanalysis($examid = null, $examyear = null) {
        $examid || show_404();
        $examyear || show_404();

        $this->data['title'] = 'SUBJECT ANALYSIS REPORT';
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;

        $lgaid = $this->input->post('lgaid');

        // $this->data['lgas'] = $this->db->get_where('t_lgas', array('edcid'=>  $this->data['edc_detail']->edcid))->result();
        // $this->data['zones'] = $this->db->get_where('t_zones', array('edcid'=>  $this->data['edc_detail']->edcid))->result();
        $this->data['lgas'] = $this->db->get_where('t_lgas', array('lgaid' => $lgaid))->result();

        $sql = "select subjectid, subjectname from t_subjects where subjectid "
                . "in (select distinct subjectid from t_remarks where examid = '" . $examid . "' and examyear = '" . $examyear . "' and edcid = '" . $this->edcid . "') "
                . "and edcid = '" . $this->edcid . "' order by priority asc";
        $this->data['subjects'] = $this->db->query($sql)->result();

        $sql = "select count(*) as span from t_remarks
                inner join t_subjects
                on t_remarks.subjectid = t_subjects.subjectid
                where t_remarks.examid = '" . $examid . "' "
                . "and t_remarks.examyear = '" . $examyear . "' "
                . "and t_remarks.edcid = '" . $this->edcid . "' ";

        $this->data['span'] = $this->db->query($sql)->row();

        $this->db->where('edcid', $this->edcid);
        $exam_detail = $this->exam_model->get_all($examid);
        $this->data['exam_detail'] = $exam_detail;
        if ($exam_detail->haszone)
            $this->load->view('asubeb/reports/reports_analysis_zone_page', $this->data);
        else
            $this->load->view('asubeb/reports/reports_analysis_page', $this->data);
    }

    public function registrationanalysis($examid = null, $examyear = null) {
        $examid || show_404();
        $examyear || show_404();

        $this->load->model('asubeb/subjects_model');
        $this->data['title'] = 'REGISTRATION ANALYSIS REPORT';
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;

        $this->db->order_by('priority');
        $this->db->where('examid', $examid);
        $this->data['subjects'] = $this->subjects_model->get_all();

        $this->db->where('edcid', $this->edcid);
        $this->data['exam_detail'] = $exam_detail = $this->exam_model->get_all($examid);

        $report_sql = "select " . ($exam_detail->haszone ? "t_zones.zoneid" : "t_lgas.lgaid") . ", " . ($exam_detail->haszone ? "t_zones.zonename" : "t_lgas.lganame") . ", t_schools.schoolid , t_schools.schoolname , t_registered_subjects.subjectid, count(*) as num
                    from t_registered_subjects
                    inner join t_candidates on t_candidates.candidateid = t_registered_subjects.candidateid
                    inner join t_schools on t_candidates.schoolid = t_schools.schoolid
                    inner join " . ($exam_detail->haszone ? "t_zones" : "t_lgas") . " on " . ($exam_detail->haszone ? "t_zones.zoneid" : "t_lgas.lgaid") . " = t_schools." . ($exam_detail->haszone ? "zoneid" : "lgaid") . "
                    where t_registered_subjects.examid = '" . $examid . "'
                    and t_registered_subjects.examyear = '" . $examyear . "'
                    and t_registered_subjects.edcid = '" . $this->edcid . "' ";

        if ($exam_detail->hassecschool)
            $report_sql .= "and t_schools.issecondary = 1 ";
        else
            $report_sql .= "and t_schools.isprimary = 1 ";

        $report_sql .= " group by " . ($exam_detail->haszone ? "t_zones.zoneid" : "t_lgas.lgaid") . ", " . ($exam_detail->haszone ? "t_zones.zonename" : "t_lgas.lganame") . ", t_schools.schoolid, t_schools.schoolname, t_registered_subjects.subjectid
                        order by t_schools.schoolid";

        $result_query = $this->db->query($report_sql)->result();

        $data_array = array();
        foreach ($result_query as $value) {

            $data_array[($exam_detail->haszone ? $value->zoneid : $value->lgaid)] = array(
                ($exam_detail->haszone ? 'zonename' : 'lganame') => ($exam_detail->haszone ? $value->zonename : $value->lganame)
            );
        }

        foreach ($result_query as $schooldata) {
            $data_array[($exam_detail->haszone ? $schooldata->zoneid : $schooldata->lgaid)]
                    ['schooldata'][$schooldata->schoolid] = array(
                'schoolname' => $schooldata->schoolname
            );
        }

        foreach ($result_query as $subjectdata) {
            $data_array[($exam_detail->haszone ? $subjectdata->zoneid : $subjectdata->lgaid)]
                    ['schooldata'][$subjectdata->schoolid][$subjectdata->subjectid] = $subjectdata->num;
        }

        $this->data['report_data'] = $data_array;

        if ($exam_detail->haszone) {
            //$this->data['zones'] = $this->db->get_where('t_zones', array('edcid' => $this->data['edc_detail']->edcid))->result();
            $this->load->view('asubeb/reports/reports_registration_analysis_zone_page', $this->data);
        } else {
            //$this->data['lgas'] = $this->db->get_where('t_lgas', array('edcid' => $this->data['edc_detail']->edcid))->result();
            $this->load->view('asubeb/reports/reports_registration_analysis_page', $this->data);
        }
    }

    public function reporting($examid = null, $examyear = null) {
        $examid || show_404();
        $examyear || show_404();

        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;

        $this->db->where('edcid', $this->edcid);
        $this->db->order_by('lganame');
        $this->data['lgas'] = $this->db->get('t_lgas')->result();

        $this->data['zones'] = $this->db->get_where('t_zones', array('edcid' => $this->data['edc_detail']->edcid))->result();

        $this->data['reportdetail'] = new stdClass();
        $this->data['reportdetail']->zoneid = '';
        $this->data['reportdetail']->lgaid = '';

        //Add styles and scripts
//        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') .'" rel="stylesheet">';
//        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';

        $this->data['subview'] = 'asubeb/report_select_page';
        $this->load->view('asubeb/template/_layout_main', $this->data);
    }

    public function candidates($sort_type, $examid, $examyear) {
        $sort_type || show_404();
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['exam_detail'] = $exam_detail = $this->exam_model->get_all($examid);
        $sql = '';
        $this->data['report'] = array();

        switch ($sort_type) {
            case 'zone':
                $exam_detail = $this->exam_model->get_all($examid);

                $has_sec = $exam_detail->hassecschool;

                if ($has_sec) {
                    $sec = 1;
                    $pri = 0;
                } else {
                    $sec = 0;
                    $pri = 1;
                }
                $this->data['title'] = 'ZONE BASED REPORT';
                $sql = "select t_zones.zoneid, zonename, count(*) as schoolcount from t_zones
                        inner join t_schools on t_zones.zoneid = t_schools.zoneid
                        where t_zones.edcid = '" . $this->edcid . "' and t_schools.isprimary='$pri' and t_schools.issecondary='$sec'
                        group by t_zones.zoneid, zonename order by zonename";
                $this->data['report'] = $this->db->query($sql)->result();
                $this->load->view('asubeb/reports/general/zone_base_report_page', $this->data);
                break;

            case 'lga':
                $this->data['title'] = 'LGA BASED REPORT';

                $exam_detail = $this->exam_model->get_all($examid);

                $has_sec = $exam_detail->hassecschool;

                if ($has_sec) {
                    $sec = 1;
                    $pri = 0;
                } else {
                    $sec = 0;
                    $pri = 1;
                }

                $sql = "select zonename, lganame, lgainitials,
                        (select count(*) from t_schools where t_schools.lgaid = t_lgas.lgaid and t_schools.edcid = '" . $this->edcid . "' and t_schools.isprimary='$pri' and t_schools.issecondary='$sec') as schoolcount,
                        (select count(*) from t_candidates
                        where t_candidates.edcid = '" . $this->edcid . "' and t_candidates.examid = '" . $examid . "' and t_candidates.examyear = '" . $examyear . "' and t_candidates.lgaid = t_lgas.lgaid) as candidatecount
                        from t_zones inner join t_lgas on t_zones.zoneid = t_lgas.zoneid
                        where t_zones.edcid = '" . $this->edcid . "'
                        group by  zonename, lganame, lgainitials, schoolcount, candidatecount order by zonename, lganame";
                $result = $this->db->query($sql)->result();
                $this->data['report'] = $result;
                $this->load->view('asubeb/reports/general/lga_base_report_page', $this->data);
                break;

            case 'school':
                $exam_detail = $this->exam_model->get_all($examid);
                $this->data['title'] = 'SCHOOL/CENTRE BASED REPORT';
                $sql = "select " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname, schoolcode, count(*) as candidatecount from t_candidates
                        inner join t_schools on t_candidates.schoolid = t_schools.schoolid
                        inner join " . ($exam_detail->haszone ? "t_zones" : "t_lgas") . " on t_schools." . ($exam_detail->haszone ? "zoneid" : "lgaid") . " = " . ($exam_detail->haszone ? "t_zones.zoneid" : "t_lgas.lgaid") . "
                        where t_candidates.edcid = '" . $this->edcid . "'
                        and t_candidates.examid = '" . $examid . "'
                        and t_candidates.examyear = '" . $examyear . "'
                        group by " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname, schoolcode
                        order by " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname";

                $result = $this->db->query($sql)->result();
                $this->data['report'] = $result;
                $this->load->view('asubeb/reports/general/school_base_report_page', $this->data);
                break;
            case 'school_late':
                $this->load->view('asubeb/reports/general/late_school_base_report_page', $this->data);
                break;
            case 'qa':
                $this->load->view('asubeb/reports/general/late_school_base_report_page', $this->data);
                break;
            case 'good_schools':
                $exam_detail = $this->exam_model->get_all($examid);
                $this->data['title'] = 'SCHOOL/CENTRE BASED REPORT';
                $sql = "select " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname, schoolcode, count(*) as candidatecount from t_candidates
                        inner join t_schools on t_candidates.schoolid = t_schools.schoolid
                        inner join " . ($exam_detail->haszone ? "t_zones" : "t_lgas") . " on t_schools." . ($exam_detail->haszone ? "zoneid" : "lgaid") . " = " . ($exam_detail->haszone ? "t_zones.zoneid" : "t_lgas.lgaid") . "
                        where t_candidates.edcid = '" . $this->edcid . "'
                        and t_candidates.examid = '" . $examid . "'
                        and t_candidates.examyear = '" . $examyear . "'
                            and t_schools.ispostingschool = '0'
                        group by " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname, schoolcode
                            having (count(*) > 150)

                        order by candidatecount DESC, " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname";

                $result = $this->db->query($sql)->result();
                $this->data['report'] = $result;
                $this->load->view('asubeb/reports/general/good_schools_report_page', $this->data);
                break;

            case 'placement_report':
                $exam_detail = $this->exam_model->get_all($examid);
                $this->data['title'] = 'PLACEMENT/POSTING REPORT';
                $sql = "select schoolname, schoolcode,zonename, count(*) as candidatecount from t_schools"
                        . " inner join t_candidates on t_candidates.placement = t_schools.schoolid "
                        . " inner join t_zones on t_schools.zoneid  = t_zones.zoneid"
                        . " where t_schools.ispostingschool = 1 "
                        . " and t_candidates.examyear = '".$examyear."'"
                        . " and t_schools.edcid = '" . $this->edcid . "' "
                        . " group by schoolname, schoolcode, zonename order by zonename, schoolname";
                $result = $this->db->query($sql)->result();
                $this->data['report'] = $result;
                $this->load->view('asubeb/reports/general/placement_base_report_page', $this->data);
                break;

            case 'summary_schools':
                $this->data['exam_detail'] = $exam_detail = $this->exam_model->get_all($examid);
                $cutoff = '';
                if ($exam_detail->hasposting) {
                    $cutoff = $this->db
                                    ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                                    ->row()->cutoff;
                    if (trim($cutoff) == false) {
                        $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                        redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
                    }
                }
                $this->data['cutoff'] = $cutoff;
                $this->data['title'] = 'SCHOOL BASED RESULT SUMMARY';
                $exam_detail = $this->exam_model->get_all($examid);
                $sql = "select t_lgas.lganame, t_schools.schoolid, schoolname, count(*) as registeredcount ,
                        (select count(*)
                            from (
                                select sum(t_scores.total_score) as total, t_scores.candidateid
                                from
                                t_scores
                                inner join t_candidates on t_scores.candidateid = t_candidates.candidateid
                                where t_scores.examid = '" . $examid . "'
                                and t_scores.examyear = '" . $examyear . "'
                                and t_candidates.schoolid = t_schools.schoolid
                                and t_candidates.edcid = '" . $this->edcid . "'
                                group by t_scores.candidateid
                            ) as subquery1 where total > 0
                        )as numsat
                        from t_schools
                        inner join t_candidates on t_candidates.schoolid = t_schools.schoolid
                        inner join t_lgas on t_schools.lgaid = t_lgas.lgaid
                        where t_schools.edcid = t_candidates.edcid
                        and t_candidates.edcid = '" . $this->edcid . "'
                        and t_candidates.examid = '" . $examid . "'
                        and t_candidates.examyear = '" . $examyear . "'
                        group by t_lgas.lganame, t_schools.schoolid, schoolname";

                $result = $this->db->query($sql)->result();
                $this->data['report'] = $result;
                $this->load->view('asubeb/reports/general/summary_school_base_report_page', $this->data);
                break;

            case 'summary_lgas':
                $this->data['exam_detail'] = $exam_detail = $this->exam_model->get_all($examid);

                $cutoff = '';

                //check if exam has  posting
                if ($exam_detail->hasposting) {
                    $cutoff = $this->db
                                    ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                                    ->row()->cutoff;
                    if (trim($cutoff) == false) {
                        $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                        redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
                    }
                }

                $this->data['cutoff'] = $cutoff;
                $this->data['title'] = 'LGA BASED RESULT SUMMARY';
                $sql = "select t_lgas.lgaid, lganame, count(*) as registeredcount ,
			(select count(*) from
			(select sum(t_scores.total_score) as total, t_scores.candidateid
			from t_scores
			inner join t_candidates on t_scores.candidateid = t_candidates.candidateid
			where t_scores.examid = '" . $examid . "' and t_scores.examyear = '" . $examyear . "'
			and t_candidates.lgaid = t_lgas.lgaid
			and t_candidates.edcid = '" . $this->edcid . "'
			group by t_scores.candidateid
			) as subquery1
			where total > 0
			)as numsat
			from t_lgas
			inner join t_candidates on t_candidates.lgaid = t_lgas.lgaid
			where t_lgas.edcid = t_candidates.edcid
			and t_candidates.edcid  = '" . $this->edcid . "'
			and t_candidates.examid = '" . $examid . "'
			and t_candidates.examyear = '" . $examyear . "'
			group by t_lgas.lgaid, lganame";
                $result = $this->db->query($sql)->result();

                $this->data['report'] = $result;

                $this->load->view('asubeb/reports/general/summary_lga_base_report_page', $this->data);
                break;
            case 'eligibility':
                $this->data['exam_detail'] = $exam_detail = $this->exam_model->get_all($examid);
                $cutoff = '';
                //check if exam has  posting
                if ($exam_detail->hasposting) {
                    $cutoff = $this->db
                                    ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                                    ->row()->cutoff;
                    if (trim($cutoff) == false) {
                        $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                        redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
                    }
                }

                $this->data['cutoff'] = $cutoff;
                $this->data['title'] = 'LGA BASED RESULT SUMMARY';
                $sql = "select t_lgas.lgaid, lganame, count(*) as registeredcount ,
			(select count(*) from
			(select sum(t_scores.total_score) as total, t_scores.candidateid
			from t_scores
			inner join t_candidates on t_scores.candidateid = t_candidates.candidateid
			where t_scores.examid = '" . $examid . "' and t_scores.examyear = '" . $examyear . "'
			and t_candidates.lgaid = t_lgas.lgaid
			and t_candidates.edcid = '" . $this->edcid . "'
			group by t_scores.candidateid
			) as subquery1
			where total > 0
			)as numsat
			from t_lgas
			inner join t_candidates on t_candidates.lgaid = t_lgas.lgaid
			where t_lgas.edcid = t_candidates.edcid
			and t_candidates.edcid  = '" . $this->edcid . "'
			and t_candidates.examid = '" . $examid . "'
			and t_candidates.examyear = '" . $examyear . "'
			group by t_lgas.lgaid, lganame";
                $result = $this->db->query($sql)->result();
                $this->data['report'] = $result;
                $this->load->view('asubeb/reports/general/eligible_candidates_report_page', $this->data);
                break;

            case 'summary_zones':
                $exam_detail = $this->exam_model->get_all($examid);

                $this->data['title'] = 'ZONE BASED RESULT SUMMARY';
                $sql = "select t_zones.zoneid, zonename, count(*) as registeredcount ,
                        (select count(*) from
                          (select sum(t_scores.total_score) as total, t_scores.candidateid
                                from t_scores
			inner join t_candidates on t_scores.candidateid = t_candidates.candidateid
			where t_scores.examid = '" . $examid . "' and t_scores.examyear = '" . $examyear . "'
			and t_candidates.zoneid = t_zones.zoneid
			and t_candidates.edcid = '" . $this->edcid . "'
			group by t_scores.candidateid
			) as subquery1
			where total > 0
			)as numsat
			from t_zones
			inner join t_candidates on t_candidates.zoneid = t_zones.zoneid
			where t_zones.edcid = t_candidates.edcid
			and t_candidates.edcid  = '" . $this->edcid . "'
			and t_candidates.examid = '" . $examid . "'
			and t_candidates.examyear = '" . $examyear . "'
			group by t_zones.zoneid, zonename";
                $result = $this->db->query($sql)->result();

                $this->data['report'] = $result;
                $this->load->view('asubeb/reports/general/summary_zone_base_report_page', $this->data);
                break;
            case 'school_list':
                $exam_detail = $this->exam_model->get_all($examid);
                $this->data['title'] = 'LIST OF REGISTERED SCHOOLS';
                $sql = "select " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname, schoolcode, count(*) as candidatecount from t_candidates
                        inner join t_schools on t_candidates.schoolid = t_schools.schoolid
                        inner join " . ($exam_detail->haszone ? "t_zones" : "t_lgas") . " on t_schools." . ($exam_detail->haszone ? "zoneid" : "lgaid") . " = " . ($exam_detail->haszone ? "t_zones.zoneid" : "t_lgas.lgaid") . "
                        where t_candidates.edcid = '" . $this->edcid . "'
                        and t_candidates.examid = '" . $examid . "'
                        and t_candidates.examyear = '" . $examyear . "'

                        group by " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname, schoolcode
                        having count(*) > 0
                        order by " . ($exam_detail->haszone ? "zonename" : "lganame") . ", schoolname";

                $result = $this->db->query($sql)->result();
                $this->data['report'] = $result;
                $this->load->view('asubeb/reports/general/school_list_report_page', $this->data);
                break;

            default:
                show_404();

                break;
        }

        //$this->load->view('asubeb/reports/reports_general_page', $this->data);
    }

    public function dynamicreport($examid, $examyear) {
        $examid || show_404();
        $examyear || show_404();
        $this->form_validation->set_rules('reporttype', 'Report Type', 'trim|required');

        if ($this->form_validation->run() == true) {
            $excludefailure = $this->input->post('excludestatus');
            $separategender = $this->input->post('separategender');
            $this->data['page_number_start'] = $this->input->post('page_number_start');



            $this->data['excludefailure'] = 1;
            if (trim($excludefailure) == false)
                $this->data['excludefailure'] = 0;

            $this->data['separategender'] = 1;
            if ($separategender != 1)
                $this->data['separategender'] = 0;

            $data = $this->remarks_model->array_from_post(array('lgaid', 'zoneid', 'schoolid', 'reporttype'));
            $this->$data['reporttype']($examid, $examyear, $data['lgaid'], $data['schoolid'], (trim($data['zoneid']) == false) ? null : $data['zoneid']);
        }
    }
    //CANDIDATE LIST REPORT
    private function CLR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'CANDIDATE LIST REPORT';

        if ($examdetail->haszone) {
            $this->_zonalfilter($zoneid, $schoolid);
            $this->load->view('asubeb/reports/reports_candidatelist_zone_page', $this->data);
        } else {
            $this->_filter($lgaid, $schoolid);
            $this->load->view('asubeb/reports/reports_candidatelist_page', $this->data);
        }
    }
    //RAW SCORE SHEET REPORT
    private function SSR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'RAW SCORE SHEET REPORT';

        $cutoff = '';
        if ($examdetail->hasposting) {
            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
            }
        }

        $this->data['cutoff'] = $cutoff;
        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/scores_model');

        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();
        $buffer_sql = '';
        if ($examdetail->haszone) {
            $buffer_sql = $this->_zonalfilter($zoneid, $schoolid);
        } else
            $buffer_sql = $this->_filter($lgaid, $schoolid);

        $st_param = 'total_score';

        #Get scores
        $sql = "select t_candidates.candidateid, t_candidates.schoolid, t_candidates.examno,
		(t_candidates.firstname || ' ' || t_candidates.othernames) as fullname, t_candidates.placement,
		t_candidates.gender, " . $st_param . " as tscore, t_scores.*
		from t_scores
		inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
		where t_candidates.examid = '" . $examid . "'
		" . $buffer_sql . "
		and t_candidates.examyear = '" . $examyear . "'
		and t_candidates.edcid = '" . $this->edcid . "'
		order by t_candidates.schoolid, t_candidates.examno";
        $query_data = $this->db->query($sql)->result();
        //        echo "<br/>";
        //        print_r($sort_array) . "<br/>";
        //        print_r($sortedarray);
        //        exit;


        $this->data['standard_report_scores'] = $query_data;


        $this->load->view('asubeb/reports/reports_raw_scoresheet_page', $this->data);
    }
    private function ARR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'AWAITING RESULTS REPORT';

        $cutoff = '';
        if ($examdetail->hasposting) {
            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
            }
        }

        $this->data['cutoff'] = $cutoff;
        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/scores_model');

        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();
        $buffer_sql = '';
        if ($examdetail->haszone) {
            $buffer_sql = $this->_zonalfilter($zoneid, $schoolid);
        } else
            $buffer_sql = $this->_filter($lgaid, $schoolid);

        $st_param = 'total_score';

        #Get scores
        $sql = "select t_candidates.candidateid, t_candidates.schoolid, t_candidates.examno,
		(t_candidates.firstname || ' ' || t_candidates.othernames) as fullname, t_candidates.placement,
		t_candidates.gender, " . $st_param . " as tscore, t_scores.*,t_lgas.lganame,t_schools.schoolname
		from t_scores
		inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
		inner join t_lgas on t_candidates.lgaid = t_lgas.lgaid
		inner join t_schools on t_candidates.schoolid = t_schools.schoolid
		where t_candidates.examid = '" . $examid . "'
		" . $buffer_sql . "
		and t_candidates.examyear = '" . $examyear . "'
                and t_scores.exam_score = 0
		and t_candidates.edcid = '" . $this->edcid . "'

		order by t_candidates.schoolid, t_candidates.examno,lganame,schoolname";
        $query_data = $this->db->query($sql)->result();
        $this->data['standard_report_scores'] = $query_data;
        $this->load->view('asubeb/reports/reports_awaiting_result_report', $this->data);
    }
    //GRADE SHEET REPORT
    private function GSR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {

        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'GRADE SHEET REPORT';

        $cutoff = '';
        if ($examdetail->hasposting) { //POSTING EXAMS
            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
            }
        }

        $this->data['cutoff'] = $cutoff;
        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/scores_model');

        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();



        if ($examdetail->haszone) {
            $this->_zonalfilter($zoneid, $schoolid);
            $this->load->view('asubeb/reports/reports_gradesheet_zone_page', $this->data);
        } elseif ((!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone)){
                        $this->_filter($lgaid, $schoolid);
            $this->load->view('asubeb/reports/reports_gradesheet_page_pslat', $this->data);
        }
        else {
            $this->_filter($lgaid, $schoolid);
            $this->load->view('asubeb/reports/reports_gradesheet_page', $this->data);
        }
    }
    //STANDARD SCORE SHEET REPORT
    private function SSSR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {

        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'STANDARD SCORE SHEET REPORT';

        $cutoff = '';

        if ($examdetail->hasposting) {

            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
            }
        }

        $this->data['cutoff'] = $cutoff;
        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/scores_model');

        $this->db->where('edcid', $this->edcid);
        if($examyear == 2015)
            $this->db->where('examyear',$examyear);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();
        $buffer_sql = '';
        if ($examdetail->haszone) {
            $buffer_sql = $this->_zonalfilter($zoneid, $schoolid);
        } else
            $buffer_sql = $this->_filter($lgaid, $schoolid);

        $st_param = get_standardscore_rep($examid, $examyear);


        #Get scores
        $sql = "select t_candidates.candidateid, t_candidates.schoolid, t_candidates.examno,
				(t_candidates.firstname || ' ' || t_candidates.othernames) as fullname, t_candidates.placement,
				t_candidates.gender, " . $st_param . " as tscore, t_scores.*
                    from t_scores
                    inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                    where t_candidates.examid = '" . $examid . "'
                    " . $buffer_sql . "
                    and t_candidates.examyear = '" . $examyear . "'
                    and t_candidates.edcid = '" . $this->edcid . "'
					order by t_candidates.schoolid, t_candidates.examno ";

        $this->data['query_data'] = $this->db->query($sql)->result();
        if ($examdetail->haszone)
            $this->load->view('asubeb/reports/reports_standard_scoresheet_page', $this->data);
        else
            $this->load->view('asubeb/reports/reports_standard_scoresheet_page', $this->data);
    }

    //CANDIDATE PLACEMENT REPORT WITH SCHOOL OF CHOICE
    public function CPRWS($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);

        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'CANDIDATES PLACEMENT REPORT';

        if (trim($schoolid) == false) { #school empty
            $this->session->set_flashdata('error', 'SCHOOL MUST BE SELECTED FOR THIS (PLACEMENT) REPORT TYPE');
            redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
        }
        $sum_param = get_standardscore_rep($examid, $examyear);

        $sql = "select t_candidates.examno, t_candidates.firstname, t_candidates.othernames,
                t_candidates.gender,t_candidates.firstchoice,t_candidates.secondchoice, t_schools.schoolname, sum(" . $sum_param . ") as total
                from t_scores
                inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                inner join t_schools on t_candidates.schoolid = t_schools.schoolid
                where t_candidates.examid = '" . $examid . "'
                and t_candidates.edcid = '" . $this->edcid . "'
                and t_candidates.examyear = '" . $examyear . "'
                and t_candidates.placement = '" . $schoolid . "'
                group by  t_candidates.examno, t_candidates.firstname, t_candidates.gender,
                t_candidates.othernames, t_schools.schoolname,t_candidates.firstchoice,t_candidates.secondchoice order by gender desc";
        $this->data['placement_data'] = $this->db->query($sql)->result();

        if ($examdetail->haszone)
            $this->_zonalfilter($zoneid, $schoolid);
        else
            $this->_filter($lgaid, $schoolid);

        $this->db->where('schoolid', $schoolid);
        $this->data['school'] = $this->db->get('t_schools')->row();

        $this->load->view('asubeb/reports/reports_candidate_placement_with_school_page', $this->data);
    }
    //EXAM RESULT REPORT
    private function ERR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'EXAMINATION RESULT REPORT';

        $cutoff = '';
        if ($examdetail->hasposting) {
            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
            }
        }

        $this->data['cutoff'] = $cutoff;

        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/scores_model');

        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();

        $buffer_sql = '';
        if ($examdetail->haszone) {
            $buffer_sql = $this->_zonalfilter($zoneid, $schoolid);
        } else
            $buffer_sql = $this->_filter($lgaid, $schoolid);

        #Get scores
        $sql = "select t_scores.*,t_candidates.* from t_scores
                    inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                    where t_candidates.examid = '" . $examid . "'
                    " . $buffer_sql . "
                    and t_candidates.examyear = '" . $examyear . "'
                    and t_candidates.edcid = '" . $this->edcid . "' ORDER BY t_candidates.schoolid,t_candidates.examno";

        $this->data['c_scores'] = $this->db->query($sql)->result();


        $standarddata = $this->db->get_where('t_standard_scores', array('examid' => $examid, 'examyear' => $examyear, 'edcid' => $this->edcid))->row();


        if (!count($standarddata)) {
            $standarddata = new stdClass();
            $standarddata->standardca = 0;
            $standarddata->standardexam = 0;
            $standarddata->standardpractical = 0;
        }
        $this->data['standarddata'] = $standarddata;


        if ($examdetail->haszone)
            $this->load->view('asubeb/reports/reports_examresult_zone_page', $this->data);
        else
            $this->load->view('asubeb/reports/reports_examresult_page', $this->data);
    }
    //UNGRADED CANDIDATES SHEET REPORT
    private function UCS($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {

        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'UNGRADED CANDIDATE SHEET';

        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/scores_model');

        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();


        $buffer_sql = '';
        if ($examdetail->haszone) {
            $buffer_sql = $this->_zonalfilter($zoneid, $schoolid);
        } else
            $buffer_sql = $this->_filter($lgaid, $schoolid);

        $not_graded_score = "0";

        $st_param = 'total_score';

        $sql = "select t_candidates.candidateid, t_candidates.schoolid, t_candidates.examno, t_candidates.firstname,t_candidates.othernames, t_candidates.placement,
		t_candidates.gender,t_schools.schoolcode, " . $st_param . " as tscore, t_scores.*
		from t_scores inner join t_candidates on t_candidates.candidateid = t_scores.candidateid inner join t_schools ON t_schools.schoolid = t_candidates.schoolid
		where t_scores.examid = '" . $examid . "'
		" . $buffer_sql . "
		and t_scores.examyear = '" . $examyear . "'
		and t_scores.edcid = '" . $this->edcid . "'
		and t_scores.exam_score = '" . $not_graded_score . "'
		order by t_candidates.examno, t_candidates.firstname";

        $query_data = $this->db->query($sql)->result();

        $this->data['not_graded'] = $query_data;

        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') . '" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') . '" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';

        $this->load->view('asubeb/reports/report_not_graded_page', $this->data);
    }
    private function USS($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {

        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'UNGRADED CANDIDATE SHEET';

        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/scores_model');

        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();


        $buffer_sql = '';
        if ($examdetail->haszone) {
            $buffer_sql = $this->_zonalfilter($zoneid, $schoolid);
        } else
            $buffer_sql = $this->_filter($lgaid, $schoolid);

        $not_graded_score = "0";

        $st_param = 'total_score';

        $sql = "select * from t_schools where schoolid in (select distinct(t_candidates.schoolid)
		from t_scores
                inner join t_candidates on t_candidates.candidateid = t_scores.candidateid

		where t_scores.examid = '" . $examid . "'
		" . $buffer_sql . "
		and t_scores.examyear = '" . $examyear . "'
		and t_scores.edcid = '" . $this->edcid . "'
		and t_scores.exam_score = '" . $not_graded_score . "') order by schoolname asc
		";

        $query_data = $this->db->query($sql)->result();

        $this->data['not_graded'] = $query_data;


        //Add styles and scripts for the datable
        $this->data['page_level_styles'] = '<link href="' . base_url('resources/vendors/datatables/dataTables.bootstrap.css') . '" rel="stylesheet">';
        $this->data['page_level_scripts'] = '<script src="' . base_url('resources/vendors/datatables/js/jquery.dataTables.min.js') . '"></script>';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/datatables/dataTables.bootstrap.js') . '"></script>';
        $this->data['page_level_styles'] .= '<link href="' . base_url('resources/vendors/form-helpers/css/bootstrap-formhelpers.min.css') . '" rel="stylesheet">';
        $this->data['page_level_scripts'] .= '<script src="' . base_url('resources/vendors/form-helpers/js/bootstrap-formhelpers.js') . '"></script>';

        $this->load->view('asubeb/reports/report_not_graded_school_page', $this->data);
    }

    private function _filter($lgaid, $schoolid, $isposting = false) {
        $this->load->model('asubeb/lgas_model');
        //ALL LGAS
        if (trim($lgaid) == false) {
            $joinTable = array('t_zones' => 'zoneid');
            $joinTableFields = array('zonename');
            $this->db->where('t_lgas.edcid', $this->edcid);
            $this->data['lgas'] = $this->lgas_model->get_join($joinTable, $joinTableFields);
            return '';
        }
        //ALL SCHOOLS IN ONE LGA
        else if ((trim($lgaid) != false) && (trim($schoolid) == false)) {
            $joinTable = array('t_zones' => 'zoneid');
            $joinTableFields = array('zonename');
            $this->db->where('t_lgas.lgaid', $lgaid);
            $this->db->where('t_lgas.edcid', $this->edcid);
            $this->data['lgas'] = $this->lgas_model->get_join($joinTable, $joinTableFields);

            return "and t_candidates.lgaid = '" . $lgaid . "' ";
        }
        //ONE SCHOOL IN ONE LGA
        else if ((trim($lgaid) != false) && (trim($schoolid) != false)) {
            $this->data['schoolid'] = $schoolid;
            $this->db->where('edcid', $this->edcid);
            if ($isposting)
                $this->db->where('ispostingschool', 1);
            $this->data['schooldetails'] = $this->db->where('schoolid', $schoolid)->get('t_schools')->result();

            $joinTable = array('t_zones' => 'zoneid');
            $joinTableFields = array('zonename');
            $this->db->where('t_lgas.lgaid', $lgaid);
            $this->db->where('t_lgas.edcid', $this->edcid);
            $this->data['lgas'] = $this->lgas_model->get_join($joinTable, $joinTableFields);

            return "and t_candidates.schoolid = '" . $schoolid . "' ";
        }
    }
    private function _zonalfilter($zoneid, $schoolid) {
        $this->load->model('asubeb/zones_model');
        //ALL ZONES
        if ((trim($zoneid) == false)) {
            $this->db->where('edcid', $this->edcid);
            $this->data['zones'] = $this->zones_model->get_all();
            return '';
        }
        //ALL SCHOOLS IN ONE ZONE
        else if ((trim($zoneid) != false) && (trim($schoolid) == false)) {
            $this->db->where('edcid', $this->edcid);
            $this->db->where('zoneid', $zoneid);
            $this->data['zones'] = $this->zones_model->get_all();
            return "and t_candidates.zoneid = '" . $zoneid . "' ";
        }
        //ONE SCHOOL IN ONE ZONE
        else if ((trim($zoneid) != false) && (trim($schoolid) != false)) {
            $this->data['schoolid'] = $schoolid;
            $this->db->where('edcid', $this->edcid);
            $this->data['schooldetails'] = $this->db->where('schoolid', $schoolid)->get('t_schools')->result();

            $this->db->where('edcid', $this->edcid);
            $this->db->where('zoneid', $zoneid);
            $this->data['zones'] = $this->zones_model->get_all();
            return "and t_candidates.schoolid = '" . $schoolid . "' ";
        }
    }
    public function school_ajaxdrop() {
        //if ($this->_is_ajax()) {
            $lgaid = $this->input->get('lgaid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);
            $examid = $this->input->get('examid', TRUE);
            $exam_detail = $this->exam_model->get_all($examid);
            $has_sec = $exam_detail->hassecschool;
            if ($has_sec == 1) {
                $sec = 1;
                $pri = 0;
            } else {
                $sec = 0;
                $pri = 1;
            }
            if ($lgaid != '' && !$exam_detail->hasposting) {
                $this->db->select('schoolid as lgaid, schoolname as lganame');
                $this->db->where('lgaid', $lgaid);
                $this->db->where('issecondary', $sec);
                $this->db->where('isprimary', $pri);
                $this->db->order_by('schoolname');
                $query = $this->db->get('t_schools');
            } elseif ($lgaid != '' && $exam_detail->hasposting) {
                $this->db->select('schoolid as lgaid, schoolname as lganame');
                $this->db->where('lgaid', $lgaid);
                $this->db->order_by('schoolname');
                $query = $this->db->get('t_schools');
            }

            $data['lgas'] = array();
            if ($query->num_rows() > 0) {
                $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        //} else {
        //    echo "Apparently is_ajax returned false!";
       //     show_error('This method can only be accessed internally.', 404);
       // }
    }
    public function school_ajaxdrop_zone() {
        if ($this->_is_ajax()) {

            $zoneid = $this->input->get('zoneid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);
            $examid = $this->input->get('examid', TRUE);


            $exam_detail = $this->exam_model->get_all($examid);

            $has_sec = $exam_detail->hassecschool;

            if ($has_sec) {
                $sec = 1;
                $pri = 0;
            } else {
                $sec = 0;
                $pri = 1;
            }
            $this->db->select('schoolid as lgaid, schoolname as lganame');
            $this->db->where('zoneid', $zoneid);
            $this->db->where('issecondary', $sec);
            $this->db->where('isprimary', $pri);
            $this->db->order_by('schoolname');
            $query = $this->db->get('t_schools');

            $data['lgas'] = array();
            if ($query->num_rows() > 0) {
                $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        } else {
            echo "Apparently is_ajax returned false!";
            show_error('This method can only be accessed internally.', 404);
        }
    }
    public function sec_school_ajaxdrop_zone() {
        if ($this->_is_ajax()) {

            $zoneid = $this->input->get('zoneid', TRUE);
            $examyear = $this->input->get('examyear', TRUE);
            $examid = $this->input->get('examid', TRUE);
            $exam_detail = $this->exam_model->get_all($examid);
            $sec = 1;
            $this->db->select('schoolid as lgaid, schoolname as lganame');
            $this->db->where('zoneid', $zoneid);
            $this->db->where('issecondary', $sec);
            $this->db->where('ispostingschool', '1');
            $this->db->order_by('schoolname ASC');
            $query = $this->db->get('t_schools');
            $data['lgas'] = array();
            if ($query->num_rows() > 0) {
                $data['lgas'] = $query->result_array();
            }
            echo json_encode($data);
            //return $data;
        } else {
            echo "Apparently is_ajax returned false!";
            show_error('This method can only be accessed internally.', 404);
        }
    }
    public function _is_ajax() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }
    //UPDATES TO HANDLE RESIT CANDIDATES
    //RESIT RAW SCORE SHEET REPORT
    private function RSSR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'RESIT RAW SCORE SHEET REPORT';

        $cutoff = '';
        if ($examdetail->hasposting) {
            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
            }
        }

        $this->data['cutoff'] = $cutoff;
        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/resit_scores_model');

        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();

        $buffer_sql = '';
        if ($examdetail->haszone) {
            $buffer_sql = $this->_zonalfilter($zoneid, $schoolid);
        } else
            $buffer_sql = $this->_filter($lgaid, $schoolid);

        $st_param = 'total_score';

        #Get scores
        $sql = "select t_candidates.candidateid, t_candidates.schoolid, t_candidates.examno,
		(t_candidates.firstname || ' ' || t_candidates.othernames) as fullname, t_candidates.placement,
		t_candidates.gender, " . $st_param . " as tscore, t_resit_scores.*
		from t_resit_scores
		inner join t_candidates on t_candidates.candidateid = t_resit_scores.candidateid
		where t_candidates.examid = '" . $examid . "'
		" . $buffer_sql . "
		and t_candidates.examyear = '" . $examyear . "'
		and t_candidates.edcid = '" . $this->edcid . "'
		order by t_candidates.schoolid, t_candidates.examno";


        $query_data = $this->db->query($sql)->result();



        //        echo "<br/>";
        //        print_r($sort_array) . "<br/>";
        //        print_r($sortedarray);
        //        exit;


        $this->data['standard_report_scores'] = $query_data;


        $this->load->view('asubeb/reports/reports_raw_scoresheet_page', $this->data);
    }
    //RESIT STANDARD SCORE SHEET REPORT
    private function RSSSR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'RESIT STANDARD SCORE SHEET REPORT';

        $cutoff = '';

        if ($examdetail->hasposting) {

            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
            }
        }

        $this->data['cutoff'] = $cutoff;
        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/resit_scores_model');

        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();

        $buffer_sql = '';
        if ($examdetail->haszone) {
            $buffer_sql = $this->_zonalfilter($zoneid, $schoolid);
        } else
            $buffer_sql = $this->_filter($lgaid, $schoolid);

        $st_param = get_standardscore_rep($examid, $examyear);


        #Get scores
        $sql = "select t_candidates.candidateid, t_candidates.schoolid, t_candidates.examno,
				(t_candidates.firstname || ' ' || t_candidates.othernames) as fullname, t_candidates.placement,
				t_candidates.gender, " . $st_param . " as tscore, t_resit_scores.*
                    from t_resit_scores
                    inner join t_candidates on t_candidates.candidateid = t_resit_scores.candidateid
                    where t_candidates.examid = '" . $examid . "'
                    " . $buffer_sql . "
                    and t_candidates.examyear = '" . $examyear . "'
                    and t_candidates.edcid = '" . $this->edcid . "'
					order by t_candidates.schoolid, t_candidates.examno ";


        $this->data['query_data'] = $this->db->query($sql)->result();

        if ($examdetail->haszone)
            $this->load->view('asubeb/reports/resit_reports_standard_scoresheet_page', $this->data);
        else
            $this->load->view('asubeb/reports/resit_reports_standard_scoresheet_page', $this->data);
    }
    //GRADE SHEET REPORT
    private function RGSR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'RESIT GRADE SHEET REPORT';

        $cutoff = '';
        if ($examdetail->hasposting) { //POSTING EXAMS
            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
            }
        }

        $this->data['cutoff'] = $cutoff;
        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/resit_scores_model');
        $this->load->model('asubeb/resit_report_model');

        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();



        if ($examdetail->haszone) {
            $this->_zonalfilter($zoneid, $schoolid);
            $this->load->view('asubeb/reports/resit_reports_gradesheet_zone_page', $this->data);
        } else {
            $this->_filter($lgaid, $schoolid);
            $this->load->view('asubeb/reports/resit_reports_gradesheet_page', $this->data);
        }
    }
    //COMBINED GRADE SHEET REPORT
    //HACK FOR EDC ANAMBRA...COMBINES PREVIOUS GRADES WITH CURRENT GRADE
    private function CGSR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'RESIT GRADE SHEET REPORT';

        $cutoff = '';
        if ($examdetail->hasposting) { //POSTING EXAMS
            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
            }
        }

        //SET CUTOFF
        $this->data['cutoff'] = $cutoff;

        //LOAD MODELS THAT WILL HANDLE NORMAL EXAMRESULTS
        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/scores_model');

        //LOAD MODELS THAT WILL HANDLE RESIT EXAM RESULTS
        $this->load->model('asubeb/resit_scores_model');
        $this->load->model('asubeb/resit_report_model');

        //FETCH SUBJECT RECORDS FOR NORMAL EXAMS
        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();

        //TOGGLE VIEWS FOR DIFFERENT EXAMS
        if ($examdetail->haszone) { //EDC ANAMBRA
            $this->_zonalfilter($zoneid, $schoolid);
            $this->load->view('asubeb/reports/combined_reports_gradesheet_zone_page', $this->data);
        }
        //EDC ABIA PSLAT
        elseif ((!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone)){
                        $this->_filter($lgaid, $schoolid);
            $this->load->view('asubeb/reports/reports_gradesheet_page_pslat', $this->data);
        }//OTHER EXAMS
        else {
            $this->_filter($lgaid, $schoolid);
            $this->load->view('asubeb/reports/reports_gradesheet_page', $this->data);
        }
    }
    private function CERT($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'RESIT GRADE SHEET REPORT';

        $cutoff = '';
        if ($examdetail->hasposting) { //POSTING EXAMS
            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('asubeb/reports/reporting/' . $examid . '/' . $examyear));
            }
        }

        //SET CUTOFF
        $this->data['cutoff'] = $cutoff;
        //LOAD MODELS THAT WILL HANDLE NORMAL EXAM RESULTS
        $this->load->model('asubeb/reg_subjects_model');
        $this->load->model('asubeb/scores_model');
        //LOAD MODELS THAT WILL HANDLE RESIT EXAM RESULTS
        $this->load->model('asubeb/resit_scores_model');
        $this->load->model('asubeb/resit_report_model');
        //FETCH SUBJECT RECORDS FOR NORMAL EXAMS
        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();

        //TOGGLE VIEWS FOR DIFFERENT EXAMS
        if ($examdetail->haszone) { //EDC ANAMBRA
            $this->_zonalfilter($zoneid, $schoolid);
            $this->load->view('asubeb/reports/combined_reports_gradesheet_zone_page', $this->data);
        }
        //EDC ABIA PSLAT
        elseif ((!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone)){
                        $this->_filter($lgaid, $schoolid);
            $this->load->view('asubeb/reports/certificate_pslat_page', $this->data);
        }//OTHER EXAMS
        else {
            $this->_filter($lgaid, $schoolid);
            $this->load->view('asubeb/reports/certificate_bece_page', $this->data);
        }
    }
/*
    public function grading_statistics($examid,$examyear){
        if (isset($examid) && isset($examyear)) {

            $subjects_query  = "select * from t_subjects where examid = 'ss97q7bi08'";
            $this->data['subjects'] = $this->db->query($subjects_query)->result_array();

            $candidate_count_array =array();
            $query = "select t_lgas.lgaid,count(t_candidates.candidateid) from t_candidates
                        inner join t_lgas on t_lgas.lgaid = t_candidates.lgaid
                        where t_candidates.examid = 'ss97q7bi08' and t_candidates.examyear = '2015'
                        group by t_lgas.lgaid";
            $candidates_count = $this->db->query($query)->result();

            foreach ($candidates_count as $candidates){
                $candidate_count_array[$candidates->lgaid] = $candidates->count;
            }

            $this->data['candidate_count'] = $candidate_count_array;

            $stats_array = array();
            $sql = "
                    select t_lgas.lgaid,t_lgas.lganame,t_scores.subjectid,count (t_scores.candidateid) as candidate_count,subjectname from t_scores
                    inner join t_subjects on t_subjects.subjectid = t_scores.subjectid
                    inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                    inner join t_lgas on t_candidates.lgaid = t_lgas.lgaid
                    where t_scores.examid = '".$examid."' and t_scores.examyear = '".$examyear."'
                        and t_scores.total_score > 0
                    group by t_lgas.lgaid,t_scores.subjectid,t_subjects.subjectname,t_lgas.lganame
    ";
            $stats = $this->db->query($sql)->result();

            foreach ($stats as $statistics){
                $stats_array[$statistics->lgaid][] = array($statistics->subjectid => array($statistics->subjectname,$statistics->candidate_count ));
                $stats_array[$statistics->lgaid]['lganame'] = $statistics->lganame;
                }


           $this->data['stats'] = $stats_array;

            $this->load->view('asubeb/reports/grading_stats',$this->data);
        }
        else {
            redirect('asubeb/login');
        }
    }
 *
 */
    public function grading_statistics($examid,$examyear){
        if (isset($examid) && isset($examyear)) {
            $candidate_count_array =array();
            $query = "select t_lgas.lgaid,count(t_candidates.candidateid) from t_candidates
                        inner join t_lgas on t_lgas.lgaid = t_candidates.lgaid
                        where t_candidates.examid = '".$examid."' and t_candidates.examyear = '".$examyear."'
                        group by t_lgas.lgaid";
            $candidates_count = $this->db->query($query)->result();

            foreach ($candidates_count as $candidates){
                $candidate_count_array[$candidates->lgaid] = $candidates->count;
            }

            $this->data['candidate_count'] = $candidate_count_array;


            $sql = "
                    select t_lgas.lgaid,t_lgas.lganame,t_scores.subjectid,count (t_scores.candidateid) as candidate_count,subjectname from t_scores
                    inner join t_subjects on t_subjects.subjectid = t_scores.subjectid
                    inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                    inner join t_lgas on t_candidates.lgaid = t_lgas.lgaid
                    where t_scores.examid = '".$examid."' and t_scores.examyear = '".$examyear."'
                        and t_scores.total_score > 0
                    group by t_lgas.lgaid,t_scores.subjectid,t_subjects.subjectname,t_lgas.lganame

                        ORDER BY t_lgas.lganame
    ";
            $stats = $this->db->query($sql)->result();

           $this->data['stats'] = $stats;

            $this->load->view('asubeb/reports/grading_stats',$this->data);
        }
        else {
            redirect('asubeb/login');
        }
    }


    ////UPDATES LAST UPDATED WED 13TH JULY 2016

    //SCHOOL OF CHOICE REPORT
    private function CHOICES ($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'CANDIDATE SCHOOL OF CHOICE REPORT';
        if ($examdetail->haszone) {
            $this->_zonalfilter($zoneid, $schoolid);
            $this->load->view('asubeb/reports/reports_school_of_choice_zone_page', $this->data);
        } else {
            $this->_filter($lgaid, $schoolid);
            $this->load->view('asubeb/reports/reports_school_of_choice_zone_page', $this->data);
        }
    }

    //CANDIDATE PLACEMENT REPORT // FOR THE PRIMARY SCHOOLS
    public function CPR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {

        $this->db->where('edcid', $this->edcid);
        $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'CANDIDATES PLACEMENT REPORT';
        if ($examdetail->haszone) {
            $this->_zonalfilter($zoneid, $schoolid);
            $this->load->view('asubeb/reports/reports_candidate_placement_report', $this->data);
        } else {
            $this->_filter($lgaid, $schoolid);
            $this->load->view('asubeb/reports/reports_candidate_placement_report', $this->data);
        }

    }

    //CANDIDATE PLACEMENT REPORT // FOR THE SECONDARY SCHOOLS
    public function SSCPR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'CANDIDATES PLACEMENT REPORT';
        $this->_zonalfilter($zoneid, $schoolid);
        $this->load->view('asubeb/reports/reports_secondary_candidate_placement_report', $this->data);
    }

    //CANDIDATE PLACEMENT REPORT // FOR THE SECONDARY SCHOOLS
    public function SSPR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'CANDIDATES PLACEMENT REPORT';
        if ($examdetail->haszone) {
            $this->_zonalfilter($zoneid, $schoolid);
            $this->load->view('asubeb/reports/reports_secondary_candidate_placement_report', $this->data);
        } else {
            $this->_filter($lgaid, $schoolid);
            $this->load->view('asubeb/reports/reports_secondary_candidate_placement_report', $this->data);
        }
        /*
        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'CANDIDATES PLACEMENT REPORT';

        if (trim($schoolid) == false) { #school empty
            $this->session->set_flashdata('error', 'SCHOOL MUST BE SELECTED FOR THIS (PLACEMENT) REPORT TYPE');
            redirect(site_url('admin/reports/reporting/' . $examid . '/' . $examyear));
        }
        $sum_param = get_standardscore_rep($examid, $examyear);

        $sql = "select t_candidates.examno, t_candidates.firstname, t_candidates.othernames,
                t_candidates.gender, t_schools.schoolname, sum(" . $sum_param . ") as total
                from t_scores
                inner join t_candidates on t_candidates.candidateid = t_scores.candidateid
                inner join t_schools on t_candidates.schoolid = t_schools.schoolid
                where t_candidates.examid = '" . $examid . "'
                and t_candidates.edcid = '" . $this->edcid . "'
                and t_candidates.examyear = '" . $examyear . "'
                and t_candidates.placement = '" . $schoolid . "'
                group by  t_candidates.examno, t_candidates.firstname, t_candidates.gender,
                t_candidates.othernames, t_schools.schoolname order by gender desc";
        $this->data['placement_data'] = $this->db->query($sql)->result();

        if ($examdetail->haszone)
            $this->_zonalfilter($zoneid, $schoolid);
        else
            $this->_filter($lgaid, $schoolid);

        $this->db->where('schoolid', $schoolid);
        $this->data['school'] = $this->db->get('t_schools')->row();

        $this->load->view('admin/reports/reports_candidate_placement_page', $this->data);
         *
         */
    }

    //CANDIDATE PLACEMENT REPORT // FOR THE SECONDARY SCHOOLS
    public function SCAPR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
        $this->db->where('edcid', $this->edcid);
        $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'SCHOOL CAPACITY PLACEMENT REPORT';
        $this->_zonalfilter($zoneid, $schoolid);
        $this->load->view('asubeb/reports/reports_secondary_school_capacity_report', $this->data);
    }

    //UN-POSTED CANDIDATES
    private function UPC($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
            $this->db->where('edcid', $this->edcid);
            $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
            $this->data['examid'] = $examid;
            $this->data['examyear'] = $examyear;
            $this->data['title'] = 'UNPOSTED CANDIDATES SHEET';
            $cutoff_data = $this->db->get('t_cutoff')->row();
            if (count($cutoff_data)){
                $this->data['cutoff'] = $cutoff_data->cutoff;
                if ($lgaid != ''){
                    $lga_filter = " AND t_candidates.lgaid ='".$lgaid."'";
                    $this->data['lganame'] = $this->db->where('lgaid',$lgaid)->get('t_lgas')->row()->lganame;
                }
                else {
                    $lga_filter = '';
                }

                $school_filter = $schoolid != '' ? " AND t_candidates.schoolid ='".$schoolid."'" : '';
                $this->data['schooname'] = $schoolid != '' ? $this->db->where('schoolid',$schoolid)->get('t_schools')->row()->schoolname:'';
                    if ($examdetail->haszone) {
                                $this->_zonalfilter($zoneid, $schoolid);
                            } else {
                                $this->_filter($lgaid, $schoolid);
                            }
                    $this->load->view('asubeb/reports/report_unposted_candidates', $this->data);

            }
            else {
                redirect('Asubeb/Reports/Reporting/'.$examid.'/'.$examyear);
            }

        }
        
    //INELIGIBLE CANDIDATES
    private function ICR($examid, $examyear, $lgaid, $schoolid, $zoneid = null) {
            $this->db->where('edcid', $this->edcid);
            $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
            $this->data['examid'] = $examid;
            $this->data['examyear'] = $examyear;
            $this->data['title'] = 'INELIGIBLE CANDIDATES SHEET';
            $cutoff_data = $this->db->get('t_cutoff')->row();
            if (count($cutoff_data)){
                $this->data['cutoff'] = $cutoff_data->cutoff;
                if ($lgaid != ''){
                    $lga_filter = " AND t_candidates.lgaid ='".$lgaid."'";
                    $this->data['lganame'] = $this->db->where('lgaid',$lgaid)->get('t_lgas')->row()->lganame;
                }
                else {
                    $lga_filter = '';
                }

                $school_filter = $schoolid != '' ? " AND t_candidates.schoolid ='".$schoolid."'" : '';
                $this->data['schooname'] = $schoolid != '' ? $this->db->where('schoolid',$schoolid)->get('t_schools')->row()->schoolname:'';
                    if ($examdetail->haszone) {
                                $this->_zonalfilter($zoneid, $schoolid);
                            } else {
                                $this->_filter($lgaid, $schoolid);
                            }
                    $this->load->view('asubeb/reports/report_ineligible_candidates', $this->data);

            }
            else {
                redirect('Asubeb/Reports/Reporting/'.$examid.'/'.$examyear);
            }

        }
}
