<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {
    
    function __construct() {
        parent::__construct();
		
		$this->data["edc_detail"] = $edc_data = $this->analyse_url();
		$this->data["edc_logo"] = base_url(str_replace('./', '', config_item('edc_logo_path')).$edc_data->edclogo);
        $this->data['edcname'] = $edc_data->edcname;
        $this->edcid = $edc_data->edcid;

    }
    
    public function index($installed = null) {  
      
	 
      $this->load->model('admin/users_model');
           
      $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
      $this->form_validation->set_rules('password', 'Password', 'trim|required');
      
      if($this->form_validation->run() == TRUE){
      
	 
            $this->db->where('edcid', $this->edcid);
            $user = $this->users_model->get_where(array('email' => $this->input->post('email'), 'password' => encrypt($this->input->post('password'))), TRUE);

    
		   if(count($user) === 1){
                // successfull      
                    $this->session->set_userdata("admin_loggedin", true);
                    $this->session->set_userdata("user_id", $user->userid);
                    $this->session->set_userdata("user_name", $user->fullname);
                    $this->session->set_userdata("user_email", $user->email);
                    $this->session->set_userdata("user_role", $user->role);
                    $this->session->set_userdata("user_privilege", $user->privilege);
               
	  
                    //save audit trail
                      $audit = array();
                      $audit['userid'] = $this->session->userdata('user_id');
                      $audit['actiontype'] = 'LOGIN';
                      $audit['message'] = 'User ['.$user->fullname.'] LOGGED IN Successfully@'. date('D, d M Y H:i:s');
                      $audit['details'] = 'Login Details:<br/>'
                              . 'Email: '.$user->email.'<br/>'
                              . 'Fullname: '.$user->fullname.'<br/>'
                              . 'Privilegde: '.$user->role.'';
                      $audit['edcid'] = $this->data['edc_detail']->edcid;
                      $this->audittrail_model->save_update($audit);
                      //End Audit Entry
 
                   redirect(site_url('asubeb/dashboard'));
            }
            else
            {
                //wrong login details 
                
                 //save audit trail
                  $audit = array();
                 $audit['userid'] = 'GUEST';
                  $audit['actiontype'] = 'LOGIN ATTEMPT';
                  $audit['message'] = 'A Failed Login Attempt Made @'. date('D, d M Y H:i:s');
                  $audit['details'] = 'Attempt Details:<br/>'
                          . 'Email: '.$this->input->post('email').'<br/>';
                         
                  $audit['edcid'] = $this->data['edc_detail']->edcid;
                  $this->audittrail_model->save_update($audit);
                  //End Audit Entry
                  
                 $this->session->set_flashdata('error', 'Authentication Failed - Wrong Credentials Supplied.');
                 redirect(site_url('asubeb/login'));
            }
      }
      $this->load->view('asubeb/login_page', $this->data);
    }
	
	
    public function logout() {  
        $this->session->unset_userdata('admin_loggedin');
        $this->session->unset_userdata('user_id');
        redirect(site_url('asubeb/login'));
    }
    
}
