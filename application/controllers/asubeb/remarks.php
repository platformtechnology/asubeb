<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Remarks extends Asubeb_Controller {

    function __construct() {
        parent::__construct();
       
        $this->edcid = $this->data['edc_detail']->edcid;
        $this->load->model('admin/remarks_model');
        $this->load->model('admin/exam_model');
		$this->load->model('admin/registration_model');
    }

    public function index(){

        $this->load->model('admin/exam_model');

		$this->db->where('hasposting','1');
        $this->data['exams'] = $this->exam_model->get_where(array('edcid' => $this->edcid));

        $this->form_validation->set_rules('examid', 'Exam Type', 'trim|required');
        $this->form_validation->set_rules('examyear', 'Exam Year', 'trim|required');
        
		if($this->form_validation->run()){
            $data = $this->exam_model->array_from_post(array('examid', 'examyear'));
            $exam_data = $this->exam_model->get_all($data['examid'], true);
            if($exam_data->hasposting == 1) {
                redirect(site_url('asubeb/remarks/cutoff/' . $data['examid'] . '/' . $data['examyear']));
            }
            redirect(site_url('asubeb/remarks/subjectdistribution/' . $data['examid'] . '/' . $data['examyear']));
        }

        $this->data['subview'] = 'asubeb/remark_page';
        $this->load->view('asubeb/template/_layout_main', $this->data);
    }
	
	
  public function cutoff($examid = null, $examyear = null,$run_analysis = NULL) {

        $examid || show_404();
        $examyear || show_404();

        $this->load->model('admin/cutoff_model');
        $this->load->model('admin/report_model');

        $this->data['cutoff'] = $this->cutoff_model->get_where(array('edcid' => $this->edcid, 'examid' => $examid, 'examyear' => $examyear), true);
        $this->db->order_by('lganame ASC');
        $this->data['lgas'] = $this->db->where('edcid', $this->data['edc_detail']->edcid)->get('t_lgas')->result();

        $validation_rules = $this->cutoff_model->_rules;
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run() == true) {
            $updateCutoff = $this->input->post('cutoff');
            if (isset($updateCutoff)) {
                $data = $this->cutoff_model->array_from_post(array('cutoff'));
                $data['edcid'] = $this->edcid;
                $data['examid'] = $examid;
                $data['examyear'] = $examyear;

                $this->db->where(array('examid' => $examid, 'examyear' => $examyear))->delete('t_cutoff');
                $this->cutoff_model->save_update($data);

                $message = 'CutOff Details>>><br/>'
                        . 'Exam Name: ' . $this->registration_model->getExamName_From_Id($examid) . '<br/>'
                        . 'Exam Year: ' . $examyear . '<br/>'
                        . 'CutOff Mark: ' . $data['cutoff'] . '<br/>';

                //$this->audittrail_model->log_audit($this->session->userdata('user_id'), 'UPDATE', $this->session->userdata('user_name'), 'Updated CUT OFF MARK For '
                  //      . $this->registration_model->getExamName_From_Id($examid) . ' ' . $examyear, $message, $this->data['edc_detail']->edcid);

                $this->session->set_flashdata('msg', 'CutOff Mark Updated Successfully - * REMEMBER TO RE-POST CANDIDATES ');
                redirect(site_url('asubeb/remarks/cutoff/' . $examid . '/' . $examyear));
            }
        }


        if ($run_analysis != NULL) {
            $this->data['run_analysis'] = TRUE;
        }
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        //$this->data['exam_detail']  = $this->db->get_where('t_exams', array('examid'=>$examid))->row();

        $this->data['subview'] = 'asubeb/remark_cutoff_page';
        $this->load->view('asubeb/template/_layout_main', $this->data);
    }
	
}
