<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "web/pages";

//if(!config_item('isonline')) $route['default_controller'] = "admin/login";

$route['404_override'] = "";
$route['home']  = "web/pages/index/home";
$route['about'] = "web/pages/index/about";
$route['news']  = "web/pages/index/news";
$route['registration']  = "web/registration";
$route['registration/(:any)']  = "web/registration/$1";

$route['admin']  = "admin/login";
$route['asubeb']  = "asubeb/login";

