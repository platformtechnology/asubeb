<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of registration_model
 *
 * @author Maxwell
 */
class Scores_Model extends MY_Model{
    
    protected $_table_name = 't_scores';
    protected $_primary_key = 'scoreid';
    protected $_order_by = 't_scores.id desc';
    
       
    function __construct() {
        parent::__construct();
    }
    
    public function get_exam_data($candidateid, $subjectid, $examid, $examyear){
        $exam_data = $this->get_where(array('candidateid'=>$candidateid, 'subjectid'=>$subjectid, 'examid'=>$examid, 'examyear'=>$examyear), true);
        return $exam_data;
    }
    
    public function get_total_score($candidateid, $examid, $examyear){
        $sql = "select sum(total_score) as total from t_scores where 
                candidateid = '".$candidateid."'
                and examid = '".$examid."'
                and examyear = '".$examyear."'
                and edcid = '".$this->data['edc_detail']->edcid."'";
        $data = $this->db->query($sql)->row();
        if(count($data)) return $data->total;
        
        return 0;
    }
}
