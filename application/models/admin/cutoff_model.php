 <?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Cutoff_model extends MY_Model{

    protected $_table_name = 't_cutoff';
    protected $_primary_key = 'id';

    public $_rules = array(
        'cutoff' => array(
                'field' => 'cutoff',
                'label' => 'CutOff Mark',
                'rules' => 'trim|required|is_natural_no_zero|max_length[4]'
                )
        );

    function __construct() {
        parent::__construct();
    }

     public function get_registered_perlga_pergender($lgaid, $examid, $examyear){
        $sql = "select "
                . "(select count(*) from t_candidates"
                //. " inner join t_pins on t_candidates.candidateid = t_pins.candidateid "
                . " where t_candidates.edcid = '" . $this->data['edc_detail']->edcid . "' and t_candidates.gender = 'M' "
                . " and t_candidates.lgaid = '".$lgaid."' "
                . " and t_candidates.examid = '".$examid."' "
                . " and t_candidates.examyear = '".$examyear."') as mcount,
                (select count(*) from t_candidates "
                //. " inner join t_pins on t_candidates.candidateid = t_pins.candidateid "
                . " where t_candidates.edcid = '" . $this->data['edc_detail']->edcid . "' "
                . " and t_candidates.gender = 'F' and t_candidates.lgaid = '".$lgaid."' "
                . " and t_candidates.examid = '".$examid."' "
                . " and t_candidates.examyear = '".$examyear."') as fcount ";

        return $this->db->query($sql)->row();
    }

    public function get_present_perlga_pergender($lgaid, $examid, $examyear, $gender){
        $sum_param = get_standardscore_rep($examid, $examyear);
        /*
        $sql = "select count(*) from
              (	select sum(".$sum_param.") as total, a.candidateid
                        from t_scores a, t_candidates b
                        where a.candidateid = b.candidateid
                        and a.examid = '".$examid."' and a.examyear = '".$examyear."'
                        and b.lgaid = '".$lgaid."'
                        and b.gender = '".$gender."'
                        and b.edcid = '" . $this->data['edc_detail']->edcid . "'
                         group by a.candidateid
                ) as subquery1
                where total > 0  ";
         * get_registered_perlga_pergender
         */

        $checkql = "select count(*) from
			(select sum(t_scores.total_score) as total, t_scores.candidateid
			from t_scores
			inner join t_candidates on t_scores.candidateid = t_candidates.candidateid
			where t_scores.examid = '" . $examid . "' "
                        . "and t_scores.examyear = '" . $examyear . "'
                        and t_candidates.lgaid = '" . $lgaid . "'
                        and t_candidates.gender = '" . $gender . "'
			and t_candidates.edcid = '" . $this->data['edc_detail']->edcid . "'
			group by t_scores.candidateid
			) as subquery1
                        where total > 0";
        return $this->db->query($checkql)->row()->count;
    }
}
