<?php
class Resit_Report_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    
    //THIS METHODS GETS THE NUMBER OF CANDIDATES THAT PASSED IN EACH ZONE BASED ON THE SET CRITERIA
    public function count_criteria_pass_per_zone($examid, $examyear) {
        //GET THE COMPULSORY SUBJECTS
        $this->db->where('edcid', $this->data["edc_detail"]->edcid);
        $this->db->where('examid', $examid);
        $this->db->where('examyear', $examyear);
        $query = $this->db->get('t_criteria_compulsory');

        // IF COMPULSORY SUBJECT IS NOT SET, TRIGGER ERROR
        if (!$query->num_rows()) {
            $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET PASS CRITERIA FIRST');
            redirect(site_url('admin/reports/reporting/' . $examid . '/' . $examyear));
        }

        $criteria = $query->result();
        if (!count($criteria))
            show_error("CRITERIA NOT SET - INVALID DATA");


        //GET THE IDS OF THE COMPULSORY SUBJECTS
        $criteria_ids = '';
        $criteria_score = 0;
        $criteria_arb_count = 0;
        $criteria_arb_score = 0;

        foreach ($criteria as $value) {
            $criteria_ids .= "'" . $value->subjectid . "',";
            $criteria_score += $value->passscore;

            #get arbitrary data (plus any other ...) - its same for all rows
            $criteria_arb_count = $value->arbitrarynum;
            $criteria_arb_score = $value->arbitrarypassscore;
        }

        $criteria_ids = rtrim($criteria_ids, ",");
        $sum_param = get_standardscore_rep($examid, $examyear); #get standardise sql
        //GET CANDIDATES THAT PASSED COMPULSORY SUBJECTS
        $sql = "select zoneid,count(*) as passtotal from 
               (select  count(candidateid) as candidatecount ,candidateid,zoneid from 
                    (select zoneid, t_resit_scores.candidateid, count(t_resit_scores.candidateid) from t_resit_scores                                    
                                inner join t_candidates on  t_candidates.candidateid  = t_resit_scores.candidateid
                                where t_resit_scores.examid = '" . $examid . "' 
                                and t_resit_scores.examyear = '" . $examyear . "' 
                                and t_resit_scores.edcid = '" . $this->data['edc_detail']->edcid . "' 
                                and t_resit_scores.subjectid in(select subjectid from t_criteria_compulsory) 
                                group by zoneid, t_resit_scores.candidateid,t_resit_scores.scoreid,t_resit_scores.subjectid   
                                having sum(" . $sum_param . ") >= (select passscore from t_criteria_compulsory where t_criteria_compulsory.subjectid = t_resit_scores.subjectid)
                    ) as subquery1
                group by candidateid,zoneid
                ) as subquery2
                where candidatecount = (select count(*) from t_criteria_compulsory)
                group by zoneid
                ";


        $candidatescore_data = $this->db->query($sql)->result_array();





        //FETCH ALL ZONES   
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $zones = $this->db->get('t_zones')->result();
        $zone_score_data = array();

        //LINK CANDIDATES COUNT TO THEIR RESPECTIVE ZONES
        if (count($candidatescore_data)) {
            foreach ($zones as $zone) {
                foreach ($candidatescore_data as $data) {
                    if ($zone->zoneid == $data['zoneid']) {
                        $zone_score_data[$zone->zoneid] = $data['passtotal'];
                        break;
                    }
                }
            }

            if (!isset($zone_score_data[$zone->zoneid])) {
                $zone_score_data[$zone->zoneid] = 0;
            }
        }


        //CONDITION 2
        //GET OPTIONAL SUBJECTS 
        //IF OPTIONAL SUBJECTS EXISTS THEN FILTER OUT CANDIDATES THAT DID NOT MEET THE CRITERIA//
        $optional = $this->_getOptionalSubjects($examid,$examyear,$this->data["edc_detail"]->edcid);

        $candidates_with_compulsory_subjects = $sql = "select candidateid from 
               (select  count(candidateid) as candidatecount ,candidateid,zoneid from 
                    (select zoneid, t_resit_scores.candidateid, count(t_resit_scores.candidateid) from t_resit_scores                                    
                                inner join t_candidates on t_resit_scores.candidateid = t_candidates.candidateid 
                                where t_resit_scores.examid = '" . $examid . "' 
                                and t_resit_scores.examyear = '" . $examyear . "' 
                                and t_resit_scores.edcid = '" . $this->data['edc_detail']->edcid . "' 
                                and t_resit_scores.subjectid in(select subjectid from t_criteria_compulsory) 
                                group by zoneid, t_resit_scores.candidateid,t_resit_scores.scoreid,t_resit_scores.subjectid   
                                having sum(" . $sum_param . ") >= (select passscore from t_criteria_compulsory where t_criteria_compulsory.subjectid = t_resit_scores.subjectid)
                    ) as subquery1
                group by candidateid,zoneid
                ) as subquery2
                where candidatecount = (select count(*) from t_criteria_compulsory)
                group by candidateid
                ";

        if (count($optional)) {
            $sql = "select count(candidateid) as optionalpassed, zoneid from 
                      (select t_resit_scores.candidateid,t_resit_scores.subjectid,t_zones.zoneid from t_resit_scores
                  inner join t_candidates on t_candidates.candidateid = t_resit_scores.candidateid
                  inner join t_zones on t_zones.zoneid = t_candidates.zoneid
                  where t_resit_scores.candidateid in (" . $candidates_with_compulsory_subjects . ")
                  and t_resit_scores.subjectid in (select subjectid from t_optional_subjects)
                  and t_resit_scores.examid = '" . $examid . "' and t_resit_scores.examyear = '" . $examyear . "'
                  and t_resit_scores.edcid = '" . $this->data['edc_detail']->edcid . "'
                  group by t_resit_scores.candidateid,t_resit_scores.subjectid,t_zones.zoneid
                  having sum(" . $sum_param . ") > (select passscore from t_optional_subjects where t_optional_subjects.subjectid = t_resit_scores.subjectid group by passscore)) as subquery group by candidateid,zoneid";

            $results = $this->db->query($sql)->result_array();
            if (count($results)) {
                $failed_counter = 0;
                foreach ($results as $key => $value) {
                    if ($value['optionalpassed'] < 4) {
                        $zone_score_data[$value['zoneid']]-=1;
                        $failed_counter++;
                    }
                }
            }
        }

        return $zone_score_data;
    }

    public function count_cutoff_pass_per_zone($examid, $examyear) {
        $cutoff = $this->db
                        ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                        ->row()
                ->cutoff;
        if (empty($cutoff)) {
            $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
            redirect(site_url('admin/reports/reporting/' . $examid . '/' . $examyear));
        }

        $sum_param = get_standardscore_rep($examid, $examyear);
        $sql = "select zoneid, count(candidateid) as num from
		(
		select t_candidates.zoneid, t_candidates.candidateid, sum(" . $sum_param . ") from t_candidates inner join t_resit_scores 
		on t_candidates.candidateid = t_resit_scores.candidateid 
		where t_candidates.examid = t_resit_scores.examid
		and t_candidates.examyear = t_resit_scores.examyear
		and t_candidates.edcid = t_resit_scores.edcid
		and t_candidates.examid = '" . $examid . "' 
		and t_candidates.examyear = '" . $examyear . "' 
		and t_candidates.edcid = '" . $this->data["edc_detail"]->edcid . "'
		group by t_candidates.zoneid, t_candidates.candidateid
		having sum(" . $sum_param . ") >= " . $cutoff . " 
		) as getPassQuery
		group by zoneid";

        $result_set = $this->db->query($sql)->result_array();
        return $result_set;
    }

    public function get_schools_per_lga($lgaid) {

        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $schools = $this->db->get_where('t_schools', array('lgaid' => $lgaid))->result();
        return $schools;
    }

    // as used on reports grade sheet page 
    public function get_schools_details_per_lga($lgaid) {

        $lgids = "'" . implode("', '", $lgaid) . "'";

        $schools = $this->db->query("select schoolid,schoolcode,lgaid,schoolname,zoneid from t_schools WHERE lgaid IN (" . $lgids . ") AND edcid='" . $this->data['edc_detail']->edcid . "' order by schoolid desc")->result();
      
        return $schools;
    }

// as used on reports grade sheet page, parameter 1 expects an array
    public function get_all_candidates_per_lga($lgids, $examid, $examyear) {
        $lgids = "'" . implode("', '", $lgids) . "'";

        $candidates = $this->db->query("select candidateid,examno,firstname,othernames,gender,schoolid FROM t_candidates where edcid='" . $this->data['edc_detail']->edcid . "' AND lgaid IN (" . $lgids . ") AND examid='" . $examid . "' AND examyear='" . $examyear . "' order by schoolid,examno ASC")->result();

        return $candidates;
    }

    public function get_school_details($schoolid, $all = false) {

        $schoolids = "'" . implode("', '", $schoolid) . "'";


        $schools = $this->db->query("SELECT t_schools.schoolid,t_schools.schoolcode,t_schools.schoolname,t_zones.zonename,t_lgas.lganame FROM t_schools JOIN t_zones ON t_zones.zoneid = t_schools.zoneid JOIN t_lgas ON t_lgas.lgaid=t_schools.lgaid AND t_schools.schoolid IN (" . $schoolids . ")")->result();

        if (!count($schools)) {
            $schools = $this->db->query("SELECT t_schools.schoolid,t_schools.schoolcode,t_schools.schoolname,t_lgas.lganame FROM t_schools JOIN t_lgas ON t_lgas.lgaid=t_schools.lgaid AND t_schools.schoolid IN (" . $schoolids . ")")->result();
        }
        if (!count($schools)) {
            $schools = $this->db->query("SELECT t_schools.schoolid,t_schools.schoolcode,t_schools.schoolname,t_zones.zonename FROM t_schools JOIN t_zones ON t_zones.zoneid = t_schools.zoneid AND t_schools.schoolid IN (" . $schoolids . ")")->result();
        }

        if (!count($schools)) {
            $schools = $this->db->query("SELECT t_schools.schoolid,t_schools.schoolcode,t_schools.schoolname FROM t_schools WHERE t_schools.schoolid IN (" . $schoolids . ")")->result();
        }

        $school = array();

        if (count($schools)):
            foreach ($schools as $key => $value) {
                $school[$value->schoolid]['schoolid'] = $value->schoolid;
                if (isset($value->lganame)) {
                    $school[$value->schoolid]['lgaid'] = $value->lganame;
                }
                if (isset($value->zonename)) {
                    $school[$value->schoolid]['zoneid'] = $value->zonename;
                }
                $school[$value->schoolid]['schoolname'] = @$value->schoolname;
                $school[$value->schoolid]['schoolcode'] = @$value->schoolcode;
            }

        endif;

        return $school;
    }

    public function get_schools_per_zone($zoneid) {
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        //$this->db->where('lgaid', '');
        $schools = $this->db->get_where('t_schools', array('zoneid' => $zoneid))->result();
        return $schools;
    }

// new function
    public function get_all_schools_per_zone($zoneid) {

        $zoneids = "'" . implode("', '", $zoneid) . "'";

        $schools = $this->db->query("select schoolid,schoolcode,lgaid,schoolname,zoneid from t_schools WHERE zoneid IN (" . $zoneids . ") AND edcid='" . $this->data['edc_detail']->edcid . "' order by schoolid desc")->result();

        return $schools;
    }

    public function get_candidate($candidateid) {
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $candidate_data = $this->db->get_where('t_candidates', array('candidateid' => $candidateid))->result();
        return $candidate_data;
    }

    public function get_subject_remark_per_candidate($subjectid, $candidateid, $examid, $examyear) {
        $examdetail = $this->exam_model->get_all($examid);
        $sum_param = get_standardscore_rep($examid, $examyear);
        
        //HACK FOR PSLAT
        if(!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone)
	{
            $sql = "select t_resit_scores.candidateid,sum(total_score) as total_score,
                t_resit_remarks.remarkid,t_resit_remarks.minimum,t_resit_remarks.maximum,t_resit_remarks.grade,t_resit_remarks.remark from t_resit_scores 
	    inner join t_resit_remarks on t_resit_remarks.examid = t_resit_scores.examid and t_resit_remarks.examyear = t_resit_scores.examyear
	    where candidateid =  '" . $candidateid . "'
	    and t_resit_scores.examid='" . $examid . "' and t_resit_scores.ca_score != 0
            group by t_resit_scores.candidateid,t_resit_remarks.remarkid,t_resit_remarks.minimum,t_resit_remarks.maximum,t_resit_remarks.grade,t_resit_remarks.remark
	    having sum(" . $sum_param . ") between t_resit_remarks.minimum and t_resit_remarks.maximum";


            $query = $this->db->query($sql)->row();
            
           
        }
        //OTHER  EXAMS
        else {
        $sql = "
                select t_resit_remarks.remarkid,t_resit_remarks.minimum,t_resit_remarks.maximum,t_resit_remarks.grade,t_resit_remarks.remark, t_resit_scores.ca_score,t_resit_scores.exam_score from t_resit_remarks 
                inner join t_resit_scores on t_resit_remarks.subjectid = t_resit_scores.subjectid
                where t_resit_scores.candidateid = '" . $candidateid . "'
                and t_resit_scores.subjectid = '" . $subjectid . "'
                and t_resit_scores.examid = '" . $examid . "'
                and t_resit_scores.examyear = '" . $examyear . "' 
                    and t_resit_scores.ca_score != 0
                group by t_resit_remarks.remarkid,t_resit_remarks.minimum,t_resit_remarks.maximum,t_resit_remarks.grade,t_resit_remarks.remark, t_resit_scores.ca_score,t_resit_scores.exam_score 
                having sum(" . $sum_param . ") between t_resit_remarks.minimum and t_resit_remarks.maximum                
                ";
        
        $query = $this->db->query($sql)->row();
        }
        return $query;
    }

//as used on report gradesheet page

  public function get_subject_remark_for_candidates($subjectids, $candidateids, $examid, $examyear) 
	  {
       
			 $examdetail = $this->exam_model->get_all($examid);
			 //PSLAT HACK
			 if(!$examdetail->hasposting && !$examdetail->hassecschool && $examdetail->hasca && $examdetail->haspractical && !$examdetail->haszone)
			 {
				 
				 $remark_details = array();
				 
				 // for pslat, dont get any pass status.. 
				 
				 $sum_param = get_standardscore_rep($examid, $examyear);  
				 
				 $candidateid = "'".implode("', '",$candidateids)."'";
				
				 $subjectid = "'".implode("', '",$subjectids)."'";
				
				 $sql = "select t_resit_scores.candidateid,sum(total_score) as total_score,t_resit_remarks.* from t_resit_scores 
				 inner join t_resit_remarks on t_resit_remarks.examid = t_resit_scores.examid and t_resit_remarks.examyear = t_resit_scores.examyear
				 where candidateid IN  (".$candidateid.")
				 and t_resit_scores.examid='".$examid."' and t_resit_scores.ca_score != 0 and t_resit_scores.exam_score != 0
				 group by t_resit_scores.candidateid,t_resit_remarks.id
				 having sum(".$sum_param.") between t_resit_remarks.minimum and t_resit_remarks.maximum";
				
			 
				 $query = $this->db->query($sql);			 
				 
				 
				   if($query->num_rows()>1)
					 {
						 $remark_detail = $query->result();
						
						 foreach($remark_detail as $key=>$value)
						 {
							 $remark_details[$value->candidateid]['total_score'] =  '<strong>'.(intval(count($value) ? $value->total_score : 0) < 0 ? '<span style="font-size: 10px; color: crimson;">ABS </span>' : (count($value) ? strtoupper($value->total_score) : '***') ).'</strong>';
							 $remark_details[$value->candidateid]['grade'] = $value->total_score <= 0 ? 'NES' : $value->grade;
							 $remark_details[$value->candidateid]['remark'] = $value->remark;
						 }  
					 }

				 // select at total scores for each subject
				 
				 $sql = "select t_resit_scores.subjectid, t_resit_scores.total_score, t_resit_scores.candidateid
				 from t_resit_scores where t_resit_scores.candidateid IN (".$candidateid.")
				 and t_resit_scores.subjectid IN (".$subjectid.")
				 and t_resit_scores.examid = '".$examid."'
				 and t_resit_scores.examyear = '".$examyear."'";
			 
				 $query = $this->db->query($sql);
				 
				 if($query->num_rows()>1)
				  {
					 $sub_details = $query->result();
					 
					
					 foreach($sub_details as $key=>$value)
					 {
						 $remark_details[$value->candidateid][$value->subjectid] =  '<strong>'.(intval(count($value) ? $value->total_score : 0) < 0 ? '<span style="font-size: 10px; color: crimson;">ABS </span>' : (count($value) ? strtoupper($value->total_score) : '***') ).'</strong>';
			
						 $remark_details[$value->candidateid]['subjectkeys'][] = $value->subjectid;
					 }
					  
				  }
				 
				 
				 return $remark_details;
				 
			 }
				
			 
			 
		  else {
			  $sum_param = get_standardscore_rep($examid, $examyear);
			  
			  $candidateid = "'" . implode("', '", $candidateids) . "'";
			  
			  $subjectid = "'" . implode("', '", $subjectids) . "'";
			  
			  $sql = "select t_resit_remarks.remarkid,t_resit_remarks.minimum,t_resit_remarks.maximum,t_resit_remarks.grade,t_resit_remarks.remark, t_resit_scores.ca_score,t_resit_scores.exam_score, t_resit_scores.candidateid,t_resit_scores.subjectid from t_resit_remarks 
			  inner join t_resit_scores on t_resit_remarks.subjectid = t_resit_scores.subjectid
			  where t_resit_scores.candidateid IN (" . $candidateid . ")
			  and t_resit_scores.subjectid IN (" . $subjectid . ")
			  and t_resit_scores.examid = '" . $examid . "'
			  and t_resit_scores.examyear = '" . $examyear . "' 
			  and t_resit_scores.exam_score > 0
			  group by t_resit_scores.candidateid,t_resit_scores.subjectid,t_resit_remarks.remarkid,t_resit_remarks.minimum,t_resit_remarks.maximum,t_resit_remarks.grade,t_resit_remarks.remark,t_resit_scores.ca_score, t_resit_scores.exam_score 
			  having sum(" . $sum_param . ") between t_resit_remarks.minimum and t_resit_remarks.maximum";
			  $query = $this->db->query($sql)->result();                          
			  return $query;
		  }
		
				
	}
	


    public function get_remakrs_per_subject($subjectid) {
        $this->db->order_by('grade', 'asc');
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $remarks = $this->db->get_where('t_resit_remarks', array('subjectid' => $subjectid))->result();
        return $remarks;
    }

    public function get_absentia_count_per_lga($lgaid, $examid, $examyear, $zoneid = null) {
        //Absentia sql

        $sql = "
                select count(*) from 
                (	select sum(t_resit_scores.total_score) as total, t_resit_scores.candidateid 
                        from t_resit_scores 
                        inner join t_candidates on t_resit_scores.candidateid = t_candidates.candidateid 
                        where t_resit_scores.examid = '" . $examid . "' and t_resit_scores.examyear = '" . $examyear . "' 
                        and t_resit_scores.candidateid in 
                        (
                            select candidateid from t_candidates where " . ( $zoneid != null ? ("zoneid = '" . $zoneid . "' ") : ("lgaid = '" . $lgaid . "'") ) . " and
                            examid = '" . $examid . "' and examyear = '" . $examyear . "'   
                        )
                        group by t_resit_scores.candidateid

                ) as getTotalSubQuery 

                where total = 0 

               ";

        $query = $this->db->query($sql)->row();
        return $query->count;
    }

    public function get_passcount_basedon_criteria_per_lga($lgaid, $examid, $examyear, $hasposting = 0, $cutoff = 0) {
        if ($hasposting) {

            $sum_param = get_standardscore_rep($examid, $examyear);
            $sql = "select lgaid, count(candidateid) as num from
                    (
                            select t_candidates.lgaid, t_candidates.candidateid, sum(" . $sum_param . ") from t_candidates 
                            inner join t_resit_scores 
                            on t_candidates.candidateid = t_resit_scores.candidateid 
                            where t_candidates.examid = t_resit_scores.examid
                            and t_candidates.examyear = t_resit_scores.examyear
                            and t_candidates.edcid = t_resit_scores.edcid
                            and t_candidates.examid = '" . $examid . "' 
                            and t_candidates.examyear = '" . $examyear . "' 
                            and t_candidates.edcid = '" . $this->data["edc_detail"]->edcid . "'
                            group by t_candidates.lgaid, t_candidates.candidateid
                            having sum(" . $sum_param . ") >= " . $cutoff . " 
                    ) as getPassQuery
                    group by lgaid";

            $result_set = $this->db->query($sql)->result_array();
            return $result_set;
        } else {
            $sql = "SELECT * FROM getLgaPassedCount('" . $lgaid . "', '" . $examid . "', '" . $examyear . "');";
            $data = $this->db->query($sql)->row();
            return $data->getlgapassedcount;
        }

        return 0;
    }

    public function getLgaUngradedCount($lgaid, $examid, $examyear) {

        $this->db->where('edcid', $this->edcid);
        $this->data['examdetail'] = $examdetail = $this->exam_model->get_all($examid);
        $this->data['examid'] = $examid;
        $this->data['examyear'] = $examyear;
        $this->data['title'] = 'UNGRADED CANDIDATE SHEET';

        $this->load->model('admin/reg_subjects_model');
        $this->load->model('admin/scores_model');

        $this->db->where('edcid', $this->edcid);
        $this->data['allsubjects'] = $this->db
                ->where(array('examid' => $examid))
                ->order_by('priority', 'asc')
                ->get('t_subjects')
                ->result();


        $buffer_sql = '';
        if ($examdetail->haszone) {
            $buffer_sql = $this->_zonalfilter($zoneid, $schoolid);
        } else
            $buffer_sql = $this->_filter($lgaid, $schoolid);

        $not_graded_score = "0";

        $st_param = 'total_score';

        $sql = "select t_candidates.candidateid, t_candidates.schoolid, t_candidates.examno, t_candidates.firstname,t_candidates.othernames, t_candidates.placement,
		 t_candidates.gender,t_schools.schoolcode, " . $st_param . " as tscore, t_resit_scores.* 
		 from t_resit_scores inner join t_candidates on t_candidates.candidateid = t_resit_scores.candidateid inner join t_schools ON t_schools.schoolid = t_candidates.schoolid
		 where t_resit_scores.examid = '" . $examid . "'
		 " . $buffer_sql . "
		 and t_resit_scores.examyear = '" . $examyear . "' 
		 and t_resit_scores.edcid = '" . $this->edcid . "' 
		 and t_resit_scores.exam_score = '" . $not_graded_score . "'
		 order by t_candidates.examno, t_candidates.firstname";

        $query_data = $this->db->query($sql)->result();
    }

    public function getLgaPassedCount($lgaid, $examid, $examyear, $hasposting = 0, $cutoff = 0, $compsubcount = '', $num_of_subjects = '', $arbnum = '', $arbpassscore = '') {

        if ($hasposting) {

            $sum_param = get_standardscore_rep($examid, $examyear);

            $sql = "select lgaid, count(candidateid) as num from
			(
			select t_candidates.lgaid, t_candidates.candidateid, sum(" . $sum_param . ") from t_candidates 
			inner join t_resit_scores 
			on t_candidates.candidateid = t_resit_scores.candidateid 
			where t_candidates.examid = t_resit_scores.examid
			and t_candidates.examyear = t_resit_scores.examyear
			and t_candidates.edcid = t_resit_scores.edcid
			and t_candidates.examid = '" . $examid . "' 
			and t_candidates.examyear = '" . $examyear . "' 
			and t_candidates.edcid = '" . $this->data["edc_detail"]->edcid . "'
			group by t_candidates.lgaid, t_candidates.candidateid
			having sum(" . $sum_param . ") >= " . $cutoff . " 
			) as getPassQuery
			group by lgaid";
			
            $result_set = $this->db->query($sql)->result_array();

            return $result_set;
        } 
        
        //PSLAT
        elseif (!$hasposting and $ispslat = true)
        {
            $lgaid = "'" . implode("', '", $lgaid) . "'";
            $candidatess = $this->db->query("SELECT t_candidates.candidateid,t_candidates.lgaid FROM t_candidates WHERE lgaid IN (" . $lgaid . ") AND t_candidates.examid='$examid' and t_candidates.examyear='$examyear'")->result_array();
            // rearrange the candidates result into a single array
            $candidates = array_column($candidatess, 'candidateid');
            $candidateid = "'" . implode("', '", $candidates) . "'";
            $sql = "SELECT min(maximum) from t_resit_remarks "
                    . "where examid = '$examid' and  examyear = '$examyear' and edcid =  '".$this->data["edc_detail"]->edcid ."'";
			
            $getPassScore = $this->db->query($sql)->result();
            
            if ($getPassScore) {
                $passScore = $getPassScore[0]->min;

            }
            
            $sql = "SELECT lgaid, count(*) FROM ( "
                    . "select t_resit_scores.candidateid,t_lgas.lgaid from t_resit_scores "
                    . " INNER JOIN t_candidates on t_candidates.candidateid = t_resit_scores.candidateid"
                    . " INNER JOIN t_lgas on t_lgas.lgaid = t_candidates.lgaid"
                    . " where t_resit_scores.candidateid in ($candidateid) "
                        . " and t_lgas.lgaid in ($lgaid)"
                    . "and t_resit_scores.examid = '$examid' and t_resit_scores.examyear = '$examyear'"
                    . "and t_resit_scores.edcid  ='".$this->data["edc_detail"]->edcid ."'"
                    . " group by t_resit_scores.candidateid,t_lgas.lgaid having sum(total_score) > $passScore) as subquery1 
                        group by lgaid
                        ";
           echo $sql;
		   exit;
            $lgapassresult = $this->db->query($sql)->result();
            if ($lgapassresult) {
                $lg = array();
                foreach($lgapassresult as $value) {
                    $lg[$value->lgaid] = $value->count;
                }
            }
            
        }

        else {

            $lgaid = "'" . implode("', '", $lgaid) . "'";


            // get all candidates for the local government
            $candidatess = $this->db->query("SELECT t_candidates.candidateid,t_candidates.lgaid FROM t_candidates WHERE lgaid IN (" . $lgaid . ") AND t_candidates.examid='$examid' and t_candidates.examyear='$examyear'")->result_array();

            // get the compulsory subjects to pass
            $criteria = $this->db->query("SELECT subjectid FROM t_criteria_compulsory WHERE t_criteria_compulsory.examid='$examid' and t_criteria_compulsory.examyear='$examyear'")->result_array();

            // rearrange the candidates result into a single array
            $candidates = array_column($candidatess, 'candidateid');

            // rearrange the subject result into a single array
            $subjects = array_column($criteria, 'subjectid');

            $subjectid = "'" . implode("', '", $subjects) . "'";

            $candidateid = "'" . implode("', '", $candidates) . "'";

            // get all candidates that fufilled first criteria, i.e they must get d pass mark in the resit subjects
            $candidates1 = $this->db->query("SELECT t_resit_scores.candidateid FROM t_resit_scores WHERE t_resit_scores.subjectid IN (" . $subjectid . ") AND t_resit_scores.candidateid IN (" . $candidateid . ") AND t_resit_scores.examid='$examid' and t_resit_scores.examyear = '$examyear' AND t_resit_scores.total_score >=(SELECT t_criteria_compulsory.passscore FROM t_criteria_compulsory WHERE t_criteria_compulsory.subjectid=t_resit_scores.subjectid AND t_criteria_compulsory.examid='$examid' and t_criteria_compulsory.examyear='$examyear')")->result_array();

            // rearrange all candidates that fufilled first criteria
            $criteria1_candidates = array_column($candidates1, 'candidateid');


            // count all candidates and num of occurence, if occurence is less than the compulsory subject count, the student would be eliminated
            $vals = array_count_values($criteria1_candidates);


            //$compsubcount is compulsory subject counts, get all students that met the criteria
            $criteria1_candidates = array_keys($vals, $compsubcount);

            if ($arbnum > 0) {

                // student that survive the first elimination round 
                $candidateid = "'" . implode("', '", $criteria1_candidates) . "'";

                // second eleimination conditions, student must get atleast the pass score for other subjects, the arbitrary pass score
                $candidates2 = $this->db->query("SELECT t_resit_scores.candidateid FROM t_resit_scores WHERE t_resit_scores.candidateid IN (" . $candidateid . ") AND t_resit_scores.subjectid NOT IN (" . $subjectid . ") AND t_resit_scores.total_score>='$arbpassscore' and t_resit_scores.examid='$examid' and t_resit_scores.examyear='$examyear'")->result_array();


                // rearragange the candidates that survive the condition 
                $criteria2_candidates = array_column($candidates2, 'candidateid');

                $candidates = array_count_values($criteria2_candidates);


                $new_candidates = array();
                // get all candidates that pass in at least number of other subjects required to pass

                for ($i = $compsubcount; $i <= $num_of_subjects; $i++) {
                    $new_candidates = array_merge($new_candidates, array_keys($candidates, $i));
                }
            } else {

                $new_candidates = $criteria1_candidates;
            }


            // find what is one array and not in the other

            $lg = array();


            foreach ($candidatess as $key => $value) {

                if (in_array($value['candidateid'], $new_candidates)) {

                    if (!isset($lg[$value['lgaid']])) {
                        $lg[$value['lgaid']] = 1;
                    } $lg[$value['lgaid']] ++;
                }
            }

            
        }
        return $lg;
    }

    public function selectAllSchoolsById($schoolids) {

        $school_ids = "'" . implode("', '", $schoolids) . "'";


        $schools = $this->db->query("SELECT t_schools.schoolid,t_schools.schoolname,t_schools.schoolcode FROM t_schools WHERE t_schools.schoolid IN (" . $school_ids . ")")->result_array();


        return $schools;
    }

    public function get_passcount_basedon_criteria_per_school($schoolid, $examid, $examyear, $hasposting = 0, $cutoff = 0) {

        if ($hasposting) {

            $sum_param = get_standardscore_rep($examid, $examyear);
            $sql = "select schoolid, count(candidateid) as num from
                    (
                            select t_candidates.schoolid, t_candidates.candidateid, sum(" . $sum_param . ") from t_candidates inner join t_resit_scores 
                            on t_candidates.candidateid = t_resit_scores.candidateid 
                            where t_candidates.examid = t_resit_scores.examid
                            and t_candidates.examyear = t_resit_scores.examyear
                            and t_candidates.edcid = t_resit_scores.edcid
                            and t_candidates.examid = '" . $examid . "' 
                            and t_candidates.examyear = '" . $examyear . "' 
                            and t_candidates.edcid = '" . $this->data["edc_detail"]->edcid . "'
                            group by t_candidates.schoolid, t_candidates.candidateid
                            having sum(" . $sum_param . ") >= " . $cutoff . " 
                    ) as getPassQuery
                    group by schoolid";
            $result_set = $this->db->query($sql)->result_array();
            return $result_set;
        } else {
            $sql = "SELECT * FROM getSchoolPassedCount('" . $schoolid . "', '" . $examid . "', '" . $examyear . "');";
            $d = $this->db->query($sql)->row();
            return $d->getschoolpassedcount;
        }

        return 0;
    }

    public function get_candidates_per_school($schoolid, $examid, $examyear, $placement = false) {

        $this->db->order_by('examno', 'asc');
        $this->db->order_by('gender', 'asc');
        $this->db
                ->where('examid', $examid)
                ->where('edcid', $this->data['edc_detail']->edcid)
                ->where('examyear', $examyear);
        if ($placement)
            $this->db->where('placement', $schoolid);
        else
            $this->db->where('schoolid', $schoolid);

        $candidates = $this->db->get('t_candidates')->result();

        return $candidates;
    }

    public function get_registered_subjects($candidates, $examid, $examyear) {

        $candidateid = "'" . implode("', '", $candidates) . "'";

        $result = $this->db->query("SELECT candidateid,subjectid FROM t_registered_subjects WHERE candidateid IN (" . $candidateid . ") AND examid='$examid' AND examyear='$examyear'")->result();

        return $result; 
    }

// as used on in the report grade sheet page
    public function check_if_registered_subjects($candidateid, $subjectid, $examid, $examyear) {

        $this->db->select('id');
        $this->db->from('t_registered_subjects');
        $this->db->where('candidateid', $candidateid);
        $this->db->where('subjectid', $subjectid);
        $this->db->where('examid', $examid);
        $this->db->where('examyear', $examyear);
        $rows = $this->db->get()->num_rows();

        if ($rows >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public function get_candidates_per_school_demacate_gender($schoolid, $examid, $examyear, $placement = false) {

        $this->db->order_by('examno', 'asc');
        $this->db->order_by('gender', 'asc');
        $this->db
                ->where('examid', $examid)
                ->where('edcid', $this->data['edc_detail']->edcid)
                ->where('examyear', $examyear);
        if ($placement)
            $this->db->where('placement', $schoolid);
        else
            $this->db->where('schoolid', $schoolid);

        $candidates = $this->db->get('t_candidates')->result();

        $gender_array = array();

        foreach ($candidates as $candidate) {
            if ($candidate->gender == 'M') {
                $gender_array['MALE'][] = $candidate;
            } else
                $gender_array['FEMALE'][] = $candidate;
        }

        //print_r($gender_array);
        //exit;
        return $gender_array;
    }

    public function get_passcount_basedon_criteria_per_candidate($candidateid, $examid, $examyear, $cutoff, $use_standard = true) {
        $exam_details = $this->db->get_where('t_exams', array('examid' => $examid))->row();
        if ($exam_details->hasposting) {

            $sum_param = 'total_score';
            if ($use_standard)
                $sum_param = get_standardscore_rep($examid, $examyear);

            $sql = "select * from ("
                    . "select sum(" . $sum_param . ") as total from t_resit_scores where "
                    . "candidateid = '" . $candidateid . "' "
                    . "and examid = '" . $examid . "' "
                    . "and examyear = '" . $examyear . "' "
                    . ") as getTotalQuery";
            //. "where total >= ".$cutoff;
            $result_set = $this->db->query($sql)->row();

            if (count($result_set) && ($result_set->total >= $cutoff))
                return "PASSED";
            else if (count($result_set) && ($result_set->total == 0))
                return '<span style="color: crimson">ABS</span>';
            else
                return '<span style="color: crimson">FAILED</span>';
        }
        else {
            //Condition 1: if student did not register any of the compulsory subject - he failed
            $sql = "select * from t_registered_subjects 
                    where candidateid = '" . $candidateid . "' and examid = '" . $examid . "' and examyear = '" . $examyear . "'
                    and subjectid in (select subjectid from t_criteria_compulsory where examid = '" . $examid . "' and examyear = '" . $examyear . "') ";
            $compulsory_subjects_registered = $this->db->query($sql)->result();
            $compulsory_subjects_to_pass = $this->db->query("select * from t_criteria_compulsory where examid = '" . $examid . "' and examyear = '" . $examyear . "'")->result();

            if (!count($compulsory_subjects_to_pass)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET <strong>EXAMINATION RESULT CRITERIAS</strong> FIRST');
                redirect(site_url('admin/criteria/settings/' . $examid . '/' . $examyear));
            }

            if (count($compulsory_subjects_registered) != count($compulsory_subjects_to_pass)) {
                return '<span style="color: crimson">FAILED</span>';
            }

            //Condition 2: foreach of the compulsory subjects 
            //if student did not reach any of their passscore - he failed
            $sql = "select * from t_resit_scores where candidateid = '" . $candidateid . "' 
                    and examid = '" . $examid . "' 
                    and examyear = '" . $examyear . "' 
                    and edcid = '" . $this->edcid . "'";
            $score_data = $this->db->query($sql)->result();
            foreach ($compulsory_subjects_to_pass as $subjects) {
                foreach ($score_data as $sdata) {
                    if ($subjects->subjectid = $sdata->subjectid) {
                        if ($sdata->total_score < $subjects->passscore) {
                            return '<span style="color: crimson">FAILED</span>';
                            break 2;
                        }
                    }
                }
            }

            //Condition 3: passing the arbitrary pass criteria

            $sql = "
                select * from t_resit_scores where subjectid not in 
                (select subjectid from t_criteria_compulsory where examid = '" . $examid . "' and examyear = '" . $examyear . "')
                and candidateid = '" . $candidateid . "' and total_score > (select arbitrarypassscore from t_criteria_compulsory where examid = '" . $examid . "' and examyear = '" . $examyear . "' limit 1)    
                ";
            $arbitrary_subjects_passed = $this->db->query($sql)->result();

            if (count($arbitrary_subjects_passed) < ($compulsory_subjects_to_pass[0]->arbitrarynum)) {
                return '<span style="color: crimson">FAILED</span>';
            }

            return "PASSED";
        }
    }

    public function getPasscountBasedonCriteriaPerCandidate($candidateid, $examid, $examyear, $cutoff, $use_standard = true) {
        $candidates = array();
        $candidateid = "'" . implode("', '", $candidateid) . "'";
        
        $exam_details = $this->db->get_where('t_exams', array('examid' => $examid))->row();
        // FOR EXAMS THAT HAVE POSTING
        if ($exam_details->hasposting) {

            $sum_param = 'total_score';
            if ($use_standard)
                $sum_param = get_standardscore_rep($examid, $examyear);

            $sql = "select candidateid, sum(" . $sum_param . ") as total from t_resit_scores where "
                    . "candidateid IN (" . $candidateid . ")"
                    . "and examid = '" . $examid . "' "
                    . "and examyear = '" . $examyear . "' group by candidateid";

            //. "where total >= ".$cutoff;

            $result_set = $this->db->query($sql)->result();

            foreach ($result_set as $sdata) {
                //$sdata???
                if ($sdata->total >= $cutoff) {

                    $candidates[$sdata->candidateid] = 'PASSED';
                } elseif ($sdata->total == 0) {
                    $candidates[$sdata->candidateid] = '<span style="color: crimson">ABS</span>';
                } else {
                    $candidates[$sdata->candidateid] = '<span style="color: crimson">FAILED</span>';
                }
            }


            return $candidates;
        } 
        // FOR EXAMS THAT DO NOT HAVE  POSTING
        else {
                
            $sum_param = 'total_score';
            //CHECK WHETHER EXAM HAS STANDARDIZATION
            if ($use_standard)
                            $sum_param = get_standardscore_rep($examid, $examyear);
            
            
            //CONDITION 1: DID THE CANDIDATE REGISTER FOR A COMPULSORY SUBJECT?
            $sql = "select t_registered_subjects.candidateid,count(t_registered_subjects.subjectid) as count from t_registered_subjects 
                where t_registered_subjects.candidateid IN (" . $candidateid . ") and t_registered_subjects.examid = '" . $examid . "' and t_registered_subjects.examyear = '" . $examyear . "'
                and t_registered_subjects.subjectid in (select t_criteria_compulsory.subjectid from t_criteria_compulsory where t_criteria_compulsory.examid = '" . $examid . "' and t_criteria_compulsory.examyear = '" . $examyear . "') GROUP BY t_registered_subjects.candidateid";
            
           
            $candidates_with_compulsory_subjects_registered = $this->db->query($sql)->result();
            $compulsory_subjects_to_pass = $this->db->query("select * from t_criteria_compulsory where examid = '" . $examid . "' and examyear = '" . $examyear . "'")->result();
            $count = 0;
            $failed = array();

            foreach ($compulsory_subjects_to_pass as $subject) {
                $subjectids[$count] = $subject->subjectid;
                $subjectscore[$subject->subjectid] = $subject->passscore;
                $count++;
            }

            $subjectids = "'" . implode("', '", $subjectids) . "'";

            if (!count($compulsory_subjects_to_pass)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET <strong>EXAMINATION RESULT CRITERIAS</strong> FIRST');
                redirect(site_url('admin/criteria/settings/' . $examid . '/' . $examyear));
            }


            foreach ($candidates_with_compulsory_subjects_registered as $key => $value) {
                if ($value->count < count($compulsory_subjects_to_pass)) {
                    $failed[] = $value->candidateid;
                }
            }
            
            //CHECK IF CANDIDATE WAS ABSENT ALL THROUGH EXAM.
           $sql = "SELECT t_resit_scores.candidateid, count(t_resit_scores.candidateid) as absentCount from t_resit_scores where t_resit_scores.candidateid IN (". $candidateid.")"
                   . "and t_resit_scores.ca_score > 0 and t_resit_scores.exam_score < 0 and t_resit_scores.total_score = 0 and t_resit_scores.examid = '".$examid."'"
                   . "and t_resit_scores.examyear = '".$examyear."' and t_resit_scores.edcid = '".$this->edcid."' group by t_resit_scores.candidateid";
            $absentCandidates = $this->db->query($sql)->result();
            
            if (count($absentCandidates)) {
                
                foreach ($absentCandidates as $absentees) {
                    
                    if ($absentees->absentcount >= 12 ) {
                        $absentCandidates[] = $absentees->candidateid;
                        $candidateid = str_replace("'".$absentees->candidateid."',","", $candidateid);
                                
                    }
                }
            }
                          

            //CONDITION 2: DID THE CANDIDATE MEET THE CONDITIONS FOR THE COMPULSORY SUBJECTS
            $sql = "select t_resit_scores.subjectid,t_resit_scores.candidateid,".$sum_param." as total_score from t_resit_scores where candidateid IN (" . $candidateid . ") AND subjectid IN (" . $subjectids . ") 
                and examid = '" . $examid . "' 
                and examyear = '" . $examyear . "' 
                and edcid = '" . $this->edcid . "'";

            $score_data = $this->db->query($sql)->result();

            foreach ($score_data as $sdata) {
                if ($sdata->total_score < $subjectscore[$sdata->subjectid]) {
                    $failed[] = $sdata->candidateid;
                }
            }

            //CONDITION 3: DID THE CANDIDATE PASS THE MINIMUM OPTIONAL / ARBITRAY SUBJECTS
            // GET OPTIONAL SUBJECTS
            //CHECK IF OPTIONAL SUBJECTS WAS SPECIFIED FOR THE EXAM ELSE USE ARBITRARY SUBJECTS
            $optional = $this->_getOptionalSubjects($examid,$examyear,$this->data["edc_detail"]->edcid);
            
            if (count($optional)) {
                
            $optionalSubjectsToBePassed = ($optional[0]->arbitrarynum);  
            
            $sql = "
            select candidateid,count(candidateid)as optionalPassed from ( select t_resit_scores.candidateid,t_resit_scores.subjectid  from t_resit_scores 
            where t_resit_scores.subjectid  IN (select t_optional_subjects.subjectid from t_optional_subjects where t_optional_subjects.examid = '" . $examid . "' "
            . "and t_optional_subjects.examyear = '" . $examyear . "' and t_optional_subjects.edcid = '".$this->data["edc_detail"]->edcid."' )
                and t_resit_scores.candidateid IN (" . $candidateid . ") "
                    . "GROUP BY t_resit_scores.candidateid,t_resit_scores.subjectid"
                    . " having sum(" . $sum_param . ") >= (select passscore from t_optional_subjects where t_optional_subjects.subjectid = t_resit_scores.subjectid group by subjectid,passscore )"
                    . " order by t_resit_scores.candidateid) as subquery2 group by candidateid";
 
            $candidatesOptionalSubjectsData = $this->db->query($sql)->result();
            foreach ($candidatesOptionalSubjectsData as $candidateOptionalSubjectsData) {
                if ($candidateOptionalSubjectsData->optionalpassed < $optionalSubjectsToBePassed) {
                    if (!empty($candidatesOptionalSubjectsData->candidateid)) {
                        $failed[] = $candidatesOptionalSubjectsData->candidateid;
                    }
                }
            }
            }
            else {
                $sql = "
            select candidateid,count(candidateid)as optionalPassed from ( select t_resit_scores.candidateid,t_resit_scores.subjectid  from t_resit_scores 
            where t_resit_scores.subjectid NOT IN (select t_criteria_compulsory.subjectid from t_criteria_compulsory where t_criteria_compulsory.examid = '" . $examid . "' "
            . "and t_criteria_compulsory.examyear = '" . $examyear . "' and t_criteria_compulsory.edcid = '".$this->data["edc_detail"]->edcid."' )
                and t_resit_scores.candidateid IN (" . $candidateid . ") "
                    . "GROUP BY t_resit_scores.candidateid,t_resit_scores.subjectid"
                    . " having sum(" . $sum_param . ") >= (select arbitrarypassscore from t_criteria_compulsory where t_criteria_compulsory.examid = '" . $examid . "' and t_criteria_compulsory.examyear = '" . $examyear ."'"
                        . "and t_criteria_compulsory.edcid = '".$this->data["edc_detail"]->edcid."' limit 1)"
                    . " order by t_resit_scores.candidateid) as subquery2 group by candidateid";
                
            $candidatesOptionalSubjectsData = $this->db->query($sql)->result();
            foreach ($candidatesOptionalSubjectsData as $candidateOptionalSubjectsData) {
                if ($candidateOptionalSubjectsData->optionalpassed < $compulsory_subjects_to_pass[0]->arbitrarynum) {
                    $failed[] = $candidatesOptionalSubjectsData->candidateid;
                }
            }
            }
            
            

            foreach ($candidates_with_compulsory_subjects_registered as $key => $value) {
                if (in_array($value->candidateid, $failed)) {
                    $candidates[$value->candidateid] = '<span style="color: crimson">FAILED</span>';
                }
                elseif (in_array($value->candidateid,$absentCandidates)) {
                    $candidates[$value->candidateid] = 'ABS';
                }
                    
                else {
                    $candidates[$value->candidateid] = 'PASSED';
                }
            }

            return $candidates;
        }
    }

    //SPECIFICALLY FOR EDC ANAMBRA
    public function getIndividualPasscountBasedonCriteriaPerCandidate($candidateid, $examid, $examyear, $cutoff, $use_standard = true) {    
        $status = '';
        $exam_details = $this->db->get_where('t_exams', array('examid' => $examid))->row();
        // FOR EXAMS THAT HAVE POSTING
        if ($exam_details->hasposting) {

            $sum_param = 'total_score';
            if ($use_standard)
                $sum_param = get_standardscore_rep($examid, $examyear);

            $sql = "select candidateid, sum(" . $sum_param . ") as total from t_resit_scores where "
                    . "candidateid IN (" . $candidateid . ")"
                    . "and examid = '" . $examid . "' "
                    . "and examyear = '" . $examyear . "' group by candidateid";

            //. "where total >= ".$cutoff;

            $result_set = $this->db->query($sql)->result();

            foreach ($result_set as $sdata) {
                //$sdata???
                if ($sdata->total >= $cutoff) {

                    $candidates[$sdata->candidateid] = 'PASSED';
                } elseif ($sdata->total == 0) {
                    $candidates[$sdata->candidateid] = '<span style="color: crimson">ABS</span>';
                } else {
                    $candidates[$sdata->candidateid] = '<span style="color: crimson">FAILED</span>';
                }
            }


            return $candidates;
        } 
        // FOR EXAMS THAT DO NOT HAVE  POSTING
        else {
            $sum_param = 'total_score';
            //CHECK WHETHER EXAM HAS STANDARDIZATION
            if ($use_standard)
                            $sum_param = get_standardscore_rep($examid, $examyear);
              
            //CONDITION 1: DID THE CANDIDATE PASS ALL COMPULSORY SUBJECTS?
            $sql = "select count(candidateid)as compulsorySubjectsPassed from
                ( select t_resit_scores.candidateid from t_resit_scores 
                where t_resit_scores.candidateid = '$candidateid' 
                and t_resit_scores.subjectid in (select t_criteria_compulsory.subjectid from t_criteria_compulsory where t_criteria_compulsory.examid = '$examid' and t_criteria_compulsory.examyear = '$examyear')
            group by t_resit_scores.candidateid,t_resit_scores.exam_score,t_resit_scores.ca_score,t_resit_scores.subjectid
            having  $sum_param >= (select passscore from t_criteria_compulsory where t_criteria_compulsory.subjectid = t_resit_scores.subjectid group by t_criteria_compulsory.subjectid,passscore )
            ) as subquery2";
            
            $query = $this->db->query($sql)->result();
            
            if (count($query)) {
                $compulsorySubjectsPassed = $query[0]->compulsorysubjectspassed; //NUMBER OF COMPULSORY SUBJECTS THE CANDIDATE PASSED
            }
            
            //GET THE NUMBER OF COMPULSORY SUBJECTS SPECIFIED
            $compulsorySubjectsSpecified = count($this->_getCompulsorySubjects($examid,$examyear,$this->data["edc_detail"]->edcid));
                if ($compulsorySubjectsPassed >= $compulsorySubjectsSpecified ) {
                        //STUDENT PASSED SPECIFIED COMPULSORY SUBJECTS CHECK FOR OPTIONALS
           //CONDITION 2 DID CANDIDATE PASS THE MINIMUM OPTIONAL SUBJECTS 
           //NOW GET THE OPTIONAL SUBJECTS
                    $optional = $this->_getOptionalSubjects($examid,$examyear,$this->data["edc_detail"]->edcid);            
                    if (count($optional)) {  
                         $optionalSubjectsToBePassed = ($optional[0]->arbitrarynum);  //MINIMUM OPTIONAL SUBJECTS TO BE PASSED
            
                        $sql = "
                        select candidateid,count(candidateid)as optionalPassed from ( select t_resit_scores.candidateid,t_resit_scores.subjectid  from t_resit_scores 
                        where t_resit_scores.subjectid  IN (select t_optional_subjects.subjectid from t_optional_subjects where t_optional_subjects.examid = '" . $examid . "' "
                        . "and t_optional_subjects.examyear = '" . $examyear . "' and t_optional_subjects.edcid = '".$this->data["edc_detail"]->edcid."' )
                            and t_resit_scores.candidateid = '$candidateid' "
                                . "GROUP BY t_resit_scores.candidateid,t_resit_scores.subjectid"
                                . " having sum(" . $sum_param . ") >= (select passscore from t_optional_subjects where t_optional_subjects.subjectid = t_resit_scores.subjectid group by subjectid,passscore )"
                                . " order by t_resit_scores.candidateid) as subquery2 group by candidateid";
                     
                        $candidatesOptionalSubjectsData = $this->db->query($sql)->result();

                    if (count($candidatesOptionalSubjectsData)) {
                        $optionalSubjectsPassed = $candidatesOptionalSubjectsData[0]->optionalpassed;
                        
                        if ($optionalSubjectsPassed >= $optionalSubjectsToBePassed) {
                            $status = '<strong style="color: green;">PASSED</strong>';
                        }
                        else {
                            $status = '<strong style="color: crimson;">FAILED</strong>';
                        }
                    } 
                    }
            }
            else {
                //STUDENT DID NO PASS THE COMPULSORY SUBJECTS HE FAILED WOEFULLY
                $status = '<strong style="color: crimson;">FAILED</strong>';
            }
            
    } 
    return $status;
  }

        
    
    public function get_score_per_subject($candidateid, $subjectid, $examid, $examyear, $all_data = false) {
        if ($all_data) {
            $sql = "select * from t_resit_scores where "
                    . "candidateid = '" . $candidateid . "' "
                    . "and subjectid = '" . $subjectid . "' "
                    . "and examid = '" . $examid . "' "
                    . "and examyear = '" . $examyear . "'";

            return $this->db->query($sql)->row();
        } else {
            $sum_param = get_standardscore_rep($examid, $examyear);
            $sql = "select sum(" . $sum_param . ") as total from t_resit_scores where "
                    . "candidateid = '" . $candidateid . "' "
                    . "and subjectid = '" . $subjectid . "' "
                    . "and examid = '" . $examid . "' "
                    . "and examyear = '" . $examyear . "'";
            $total = $this->db->query($sql)->row();
            return $total->total;
        }
    }
    
    
    private function _getOptionalSubjects($examid,$examyear,$edcid)
    {
        $this->db->where('edcid',$edcid);
        $this->db->where('examid', $examid);
        $this->db->where('examyear', $examyear);
        $query = $this->db->get('t_optional_subjects');
        $result = $query->result();
        
        return $result;
    }
    
        private function _getCompulsorySubjects($examid,$examyear,$edcid)
    {
        $this->db->where('edcid',$edcid);
        $this->db->where('examid', $examid);
        $this->db->where('examyear', $examyear);
        $query = $this->db->get('t_criteria_compulsory');
        $result = $query->result();
        
        return $result;
    }
    
        public function getCompulsorySubjectsToPass($examid,$examyear) {
    	$compulsorySubjects = array();
    	$compulsory_subjects_to_pass = $this->db->query("select * from t_criteria_compulsory where examid = '" . $examid . "' and examyear = '" . $examyear . "'")->result();
    	foreach ($compulsory_subjects_to_pass as $subject){
    		$compulsorySubjects[]=$subject->subjectid;

    	}
    	return $compulsorySubjects;
    }

}
