<?php
class News_Model extends MY_Model{
    
    protected $_table_name = 't_edc_news';
    protected $_primary_key = 'newsid';
    protected $_order_by = 't_edc_news.id desc';                    
    function __construct() {
        parent::__construct();
    }
    
    public function is_new(){
        $detail = new stdClass();
        $detail->newstitle = '';
        $detail->news = '';
        return $detail;
    }
}
