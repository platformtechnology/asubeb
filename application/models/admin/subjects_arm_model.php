<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Subjects_Arm_Model extends MY_Model{
    
    protected $_table_name = 't_subjects_arms';
    protected $_primary_key = 'armid';
    protected $_order_by = 't_subjects_arms.id desc';
    
    public $_rules = array(
        'subjectarm' => array(
                'field' => 'armname',
                'label' => 'Subject Arm',
                'rules' => 'trim|required'
                )
     );
        
    function __construct() {
        parent::__construct();
    }
    
    public function is_new(){
        
      $edc_detail = new stdClass();
      $edc_detail->armname = '';
      $edc_detail->subjectid = '';
      $edc_detail->examid = '';
      $edc_detail->armid = null;

      return $edc_detail;
    }
    
    public function getExamName_From_Id($examid) {
        $result = $this->db->get_where('t_exams', array('examid'=>$examid))->row();
        if(count($result)){
            return $result->examname;
        }
        return '';
    }
    
    public function getArmData($data) {
                
        $sql = "SELECT t_subjects_arms.*, t_subjects.subjectname FROM t_subjects_arms 
                INNER JOIN t_subjects ON t_subjects.subjectid = t_subjects_arms.subjectid 
                WHERE t_subjects_arms.edcid = '" . $data['edcid'] ."' "
                . "AND t_subjects_arms.examid = '" . $data['examid'] ."' ";
        
        return $this->db->query($sql)->result();
                
    }
}
