<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Users_model extends MY_Model{
    
    protected $_table_name = 't_users';
    protected $_primary_key = 'userid';
    
    public $_rules = array(
        'email' => array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email'
                ),
        'password' => array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|trim'
            ),
         'name' => array(
            'field' => 'fullname',
            'label' => 'Name',
            'rules' => 'required'
            ),
         'phone' => array(
            'field' => 'phone',
            'label' => 'Phone Number',
            'rules' => 'required'
            )
        );
    
    function __construct() {
        parent::__construct();
    }
    
    public function is_new() {
        $userdetail = new stdClass();
        $userdetail->email = '';
        $userdetail->phone = '';
        $userdetail->fullname = '';
        $userdetail->role = '';
                
        return $userdetail;
    }

}
