<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class StandardScore_Model extends MY_Model{
    
    protected $_table_name = 't_standard_scores';
    protected $_primary_key = 'id';
    protected $_order_by = 't_standard_scores.id desc';
    
    function __construct() {
        parent::__construct();
    }
   
    public function is_new(){
        
      $detail = new stdClass();
      $detail->examid = '';
      $detail->examyear = '';
      $detail->standardca = 0;
      $detail->standardexam = 0;
      $detail->standardpractical = 0;      
      return $detail;
    }
    
}
