<?php

class Criteria_Comp_Model extends MY_Model{
    
    protected $_table_name = 't_criteria_compulsory';
    protected $_primary_key = 'criteriaid';
    protected $_order_by = 't_criteria_compulsory.id desc';
    
    function __construct() {
        parent::__construct();
    }
    
    public function get_arbitrarydetail($criteriaid){
        return $this->get_where(array('criteriaid'=>$criteriaid), true);
    }
}
