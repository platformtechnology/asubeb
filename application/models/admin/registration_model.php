<?php

class Registration_Model extends MY_Model{
    
    protected $_table_name = 't_candidates';
    protected $_primary_key = 'candidateid';
    protected $_order_by = 't_candidates.id desc';
    
    public $_rules = array(
        'firstname' => array(
                'field' => 'firstname',
                'label' => 'First Name',
                'rules' => 'trim|required'
                ),
        'othernames' => array(
                'field' => 'othernames',
                'label' => 'Other Names',
                'rules' => 'trim|required'
                ),
        'lga' => array(
                'field' => 'lgaid',
                'label' => 'Local Govt Area',
                'rules' => 'trim'
                ),
        'school' => array(
                'field' => 'schoolid',
                'label' => 'School',
                'rules' => 'trim'
                ),
        'dob' => array(
                'field' => 'dob',
                'label' => 'Date Of Birth',
                'rules' => 'trim|required'
                )
     );
    
    function __construct() {
        parent::__construct();
    }

    
    public function is_new(){
        
        $candidate_detail = new stdClass();
        $candidate_detail->zoneid = '';
        $candidate_detail->lgaid = '';
        $candidate_detail->schoolid = '';
        $candidate_detail->centreid = '';
        $candidate_detail->firstname = '';
        $candidate_detail->othernames = '';
        $candidate_detail->dob = '2004-01-01';//date('Y-m-d');
        $candidate_detail->phone = '';
        $candidate_detail->candidateid = '';
        $candidate_detail->firstchoice = '';
        $candidate_detail->secondchoice = '';
        $candidate_detail->gender = 'M';
        
        return $candidate_detail;
    }
    
    public function get_practical_candidates($examid, $examyear, $schoolid){
         $sql = "select t_candidates.* from t_candidates "
                //. "inner join t_pins on t_pins.candidateid = t_candidates.candidateid "
                . "where t_candidates.schoolid = '" . $schoolid. "' "
                . "and t_candidates.edcid = '" . $this->data['edc_detail']->edcid . "' " 
                . "and t_candidates.examid = '" . $examid . "' " 
                . "and t_candidates.examyear = '" . $examyear . "' " 
                . "order by examno asc ";
         return $this->db->query($sql)->result();
    }
    
    public function get_pro_practical_candidates($examid, $examyear, $schoolid, $interval){
          $sql = "SELECT t_candidates.* FROM t_candidates "
                   // . "INNER JOIN t_pins ON t_candidates.candidateid = t_pins.candidateid "
                    . "WHERE t_candidates.edcid = '" . $this->data['edc_detail']->edcid . "' "
                    . "AND t_candidates.schoolid = '" . $schoolid . "' "
                    . "AND t_candidates.examid = '" . $examid . "' "
                    . "AND t_candidates.examyear = '" . $examyear . "' "
                    . "AND substring(to_char(t_candidates.datecreated, 'YYYY-MM-DD HH24:MI:SS') from 1 for 16) >= '" . $interval . "' ";
         return $this->db->query($sql)->result();
    }
    
    public function get_lga($lgaid, $all = false) {
        $lga_data = $this->db->get_where('t_lgas', array('lgaid'=>$lgaid))->row();
        if($all){
           if(count($lga_data)) return $lga_data;
           
           $newClass = new stdClass();
           $newClass->lganame = " ---- ";
           $newClass->lgainitials = " ---- ";
           return $newClass;
        }
        return $lga_data->lganame;
    }
	
	public function get_zone($zoneid, $all = false) {
        $lga_data = $this->db->get_where('t_zones', array('zoneid'=>$zoneid))->row();
        if($all){
           if(count($lga_data)) return $lga_data;
           
           $newClass = new stdClass();
           $newClass->zonename = " ---- ";
           return $newClass;
        }
        return $lga_data->zonename;
    }
    
    public function get_school($schoolid, $all = false) {
        $school_data = $this->db->get_where('t_schools', array('schoolid'=>$schoolid))->row();
        if($all)return $school_data;
        if(count($school_data))return $school_data->schoolname;
        return '';
    }
    
     public function get_subject($subjectid) {
        $subject_data = $this->db->get_where('t_subjects', array('subjectid'=>$subjectid))->row();
        return empty($subject_data->subjectname) ? '' : $subject_data->subjectname;
    }
    
    public function getExamName_From_Id($examid) {
        $result = $this->db->get_where('t_exams', array('examid'=>$examid))->row();
        if(count($result)){
            return $result->examname;
        }
        return '';
    }
    

}
