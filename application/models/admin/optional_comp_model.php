<?php

class Optional_Comp_Model extends MY_Model{
    
    protected $_table_name = 't_optional_subjects';
    protected $_primary_key = 'optionalid';
    protected $_order_by = 't_optional_subjects.id desc';
    
    function __construct() {
        parent::__construct();
    }
    
    public function get_arbitrarydetail($criteriaid){
        return $this->get_where(array('criteriaid'=>$criteriaid), true);
    }
}
