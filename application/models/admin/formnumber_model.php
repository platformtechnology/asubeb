<?php

class Formnumber_model extends MY_Model{
    protected $_table_name = 't_form_numbers';
    protected $_primary_key = 'id';
    protected $_order_by = 't_form_numbers.id asc';
    
    function __construct() {
        parent::__construct();
    }
    
}
