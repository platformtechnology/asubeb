<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of audittrail_model
 *
 * @author Maxwell
 */
class Audittrail_model extends MY_Model{
   
    protected $_table_name = 't_audittrail';
    protected $_primary_key = 'id';
    
    function __construct() {
        parent::__construct();
    }
    
    public function log_audit($userid, $action, $username, $message, $details, $edcid){
        //save audit trail
          $audit = array();
          $audit['userid'] = $userid;
          $audit['actiontype'] = $action;
          $audit['message'] = 'User ['.$username.'] ' . $message.' @'.date('D, d M Y H:i:s');
          $audit['details'] = $details;
          $audit['edcid'] = $edcid;
          $this->save_update($audit);
          //End Audit Entry
    }
}
