<?php

class Resit_Remarks_Model extends MY_Model{
    
    protected $_table_name = 't_resit_remarks';
    protected $_primary_key = 'remarkid';
    protected $_order_by = 't_resit_remarks.remarkid desc';
    
    public $_rules = array(
        'subject' => array(
                'field' => 'subjectid',
                'label' => 'Subject',
                'rules' => 'trim|required'
                ),
        'min' => array(
                'field' => 'minimum',
                'label' => 'Minimum Score',
                'rules' => 'trim|required|is_natural'
                ),
        'max' => array(
                'field' => 'maximum',
                'label' => 'Maximum Score',
                'rules' => 'trim|required|is_natural_no_zero'
                ),
        'grade' => array(
                'field' => 'grade',
                'label' => 'Grade',
                'rules' => 'trim|required'
                ),
        'remarks' => array(
                'field' => 'remark',
                'label' => 'Remark',
                'rules' => 'trim|required'
                )
     );
 
 
 
	public $_rules_remark = array(
	
							'min' => array(
											'field' => 'minimum',
											'label' => 'Minimum Score',
											'rules' => 'trim|required|is_natural'
										  ),	
							'max' => array(
											'field' => 'maximum',
											'label' => 'Maximum Score',
											'rules' => 'trim|required|is_natural_no_zero'
										  ),
							'grade' => array(
												'field' => 'grade',
												'label' => 'Grade',
												'rules' => 'trim|required'
											),
							'remarks' => array(
												'field' => 'remark',
												'label' => 'Remark',
												'rules' => 'trim|required'
											)
	          );
    
 
    
    function __construct() {
        parent::__construct();
    }     
    
    public function get_subject_remarks($subjectid, $examyear) {
        $subject_remark = $this->get_where(array('subjectid'=>$subjectid, 'examyear'=>$examyear));
        
        return $subject_remark;
    }
    
    public function get_stat_per_remark($subjectid, $examid, $examyear, $minimum, $maximum, $schoolid = null){
        
        #Get standard scorel
        $sum_param = get_standardscore_rep($examid, $examyear); 
        $getSubjectCountQuery = "select t_resit_scores.*  from t_resit_scores inner join t_candidates "
                                . "on t_resit_scores.candidateid = t_candidates.candidateid "
                                . "where t_resit_scores.edcid = '" . $this->data['edc_detail']->edcid . "' "
                                . "and t_resit_scores.subjectid = '".$subjectid."' and t_resit_scores.examid = '".$examid."' "
                                . "and t_resit_scores.examyear = '".$examyear."' ";
        if($schoolid != null) $getSubjectCountQuery .= " and t_candidates.schoolid = '". $schoolid ."' ";
        
        $getRemarkSql = $getSubjectCountQuery . "group by t_resit_scores.id,t_resit_scores.candidateid having sum(" . $sum_param . ") between ".$minimum." and ".$maximum ; 
        
        
        $query = $this->db->query($getSubjectCountQuery);
        $rquery = $this->db->query($getRemarkSql);
        $total_students_for_subject = $query->num_rows();
        $students_count_per_remark = $rquery->num_rows();
        
        if($total_students_for_subject == 0) $percentage_count = 0;
        else $percentage_count = ($students_count_per_remark * 100) / $total_students_for_subject;
                
        $data['students_count_per_remark'] = $students_count_per_remark;
        $data['percentage_count_per_remark'] = round($percentage_count, 2);
        
        return $data;
    }
    

    public function get_exception_stat_per_remark($examid, $examyear, $minimum, $maximum, $schoolid = null){
        
        #Get standard scorel
        $sum_param = get_standardscore_rep($examid, $examyear); 
        $getSubjectCountQuery = "select distinct(t_resit_scores.candidateid)  from t_resit_scores inner join t_candidates "
                                . "on t_resit_scores.candidateid = t_candidates.candidateid "
                                . "where t_resit_scores.edcid = '" . $this->data['edc_detail']->edcid . "' "
                                . " and t_resit_scores.examid = '".$examid."' "
                                . " and t_candidates.examid = '".$examid."' "
                                . "and t_resit_scores.examyear = '".$examyear."'";

        
        $getRemarkSql = $getSubjectCountQuery . "group by t_resit_scores.candidateid having round(sum(" . $sum_param . ")) between ".$minimum." and ".$maximum ; 
        
        $query = $this->db->query($getSubjectCountQuery);
        $rquery = $this->db->query($getRemarkSql);
        $total_students_for_subject = $query->num_rows();
        $students_count_per_remark = $rquery->num_rows();
        
        if($total_students_for_subject == 0) $percentage_count = 0;
        else $percentage_count = ($students_count_per_remark * 100) / $total_students_for_subject;
                
        $data['students_count_per_remark'] = $students_count_per_remark;
        $data['percentage_count_per_remark'] = round($percentage_count, 2);
        $data['total_students'] = $total_students_for_subject;
        return $data;
    }

    
    
    public function total_students_per_subject($subjectid, $examid, $examyear){
        if($subjectid == '') {
              $sql = "select count(*) as ct from t_resit_scores inner join t_candidates "
                . "on t_resit_scores.candidateid = t_candidates.candidateid "
                . "where t_resit_scores.edcid = '" . $this->data['edc_detail']->edcid . "' "
                . "and t_resit_scores.examid = '".$examid."' and t_resit_scores.examyear = '".$examyear."' ";
        }
        else{
             $sql = "select count(*) as ct from t_resit_scores inner join t_candidates "
                . "on t_resit_scores.candidateid = t_candidates.candidateid "
                . "where t_resit_scores.edcid = '" . $this->data['edc_detail']->edcid . "' "
                . "and t_resit_scores.subjectid = '".$subjectid."' and t_resit_scores.examid = '".$examid."' "
                . "and t_resit_scores.examyear = '".$examyear."' ";
        
        }
        $total_students_for_subject = $this->db->query($sql)->row()->ct;
        return $total_students_for_subject;
    }
    
   
}
