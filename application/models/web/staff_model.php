<?php

class Staff_Model extends MY_Model{
    
    protected $_table_name = 't_users';
    protected $_primary_key = 'userid';
    protected $_order_by = 't_users.id desc';
    
    public $_rules = array(
        'email' => array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
                ),
        'password' => array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
                )
     );
    
    function __construct() {
        parent::__construct();
    }
    
}