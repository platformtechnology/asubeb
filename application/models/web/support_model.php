<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Support_model extends MY_Model{
    
    protected $_table_name = 't_support';
    protected $_primary_key = 'requestid';
    protected $_order_by = 't_support.id desc';
    public $_rules = array(
        'subject' => array(
                'field' => 'subject',
                'label' => 'Subject of Request',
                'rules' => 'trim|required|max_length[200]'
                ),
        'name' => array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
                ),
        'request' => array(
                'field' => 'request',
                'label' => 'Youe Request',
                'rules' => 'trim|required'
                )
        );
                    
    function __construct() {
        parent::__construct();
    }
    
    
   
}
