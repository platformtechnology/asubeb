<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class News_Model extends MY_Model{
    
    protected $_table_name = 't_edc_news';
    protected $_primary_key = 'newsid';
    protected $_order_by = 't_edc_news.id desc';
    public $_rules = array(
        'title' => array(
                'field' => 'newstitle',
                'label' => 'News Title',
                'rules' => 'trim|required'
                ),
        'news' => array(
                'field' => 'news',
                'label' => 'News Detail',
                'rules' => 'trim|required'
                )
        );
                    
    function __construct() {
        parent::__construct();
    }
    
    public function is_new(){
        
        $detail = new stdClass();
        $detail->newstitle = '';
        $detail->news = '';
        
        return $detail;
    }
}
