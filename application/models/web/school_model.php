<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of registration_model
 *
 * @author Maxwell
 */

class School_Model extends MY_Model{
    
    protected $_table_name = 't_schools';
    protected $_primary_key = 'schoolid';
    protected $_order_by = 't_schools.schoolname asc';
    
    public $_rules = array(
        'schoolcode' => array(
                'field' => 'schoolcode',
                'label' => 'School Code',
                'rules' => 'trim|required'
                ),
        'passcode' => array(
                'field' => 'passcode',
                'label' => 'Pass Code',
                'rules' => 'trim|required|numeric'
                ),
        'phone' => array(
                'field' => 'phone',
                'label' => 'Phone Number',
                'rules' => 'trim|required|numeric'
                )
     );
    
    function __construct() {
        parent::__construct();
    }
    
    public function get_lga($lgaid) {
        $data = $this->db->where('lgaid', $lgaid)->get('t_lgas')->row();
        if(count($data)) return $data->lganame;
        
        return '--';
    }
}

