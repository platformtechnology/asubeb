<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of registration_model
 *
 * @author Maxwell
 */
class Exam_Scores_Model extends MY_Model{
    
    protected $_table_name = 't_ca_scores';
    protected $_primary_key = 'caid';
    protected $_order_by = 't_ca_scores.id desc';
    
       
    function __construct() {
        parent::__construct();
    }
}
