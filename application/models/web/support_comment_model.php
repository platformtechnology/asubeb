<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Support_Comment_Model extends MY_Model{
    
    protected $_table_name = 't_support_comments';
    protected $_primary_key = 'commentid';
    protected $_order_by = 't_support_comments.id asc';
    public $_rules = array(
        'comment' => array(
                'field' => 'comment',
                'label' => 'Your Comment',
                'rules' => 'trim|required'
                )
        );
                    
    function __construct() {
        parent::__construct();
    }
    
    public function get_comments($requestid){
        return $this->get_where(array('requestid'=>$requestid));
    }
   
}
