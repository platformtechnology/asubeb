<?php
//LASTED UPDATE 6TH DEC 2015
class Registration_Model extends MY_Model{
    
    protected $_table_name = 't_candidates';
    protected $_primary_key = 'candidateid';
    protected $_order_by = 't_candidates.id desc';
    
    public $_rules = array(
        'firstname' => array(
                'field' => 'firstname',
                'label' => 'First Name',
                'rules' => 'trim|required'
                ),
        'othernames' => array(
                'field' => 'othernames',
                'label' => 'Other Names',
                'rules' => 'trim|required'
                ),
        'lga' => array(
                'field' => 'lgaid',
                'label' => 'Local Govt Area',
                'rules' => 'trim'
                ),
        'school' => array(
                'field' => 'schoolid',
                'label' => 'School',
                'rules' => 'trim'
                ),
        'dob' => array(
                'field' => 'dob',
                'label' => 'Date Of Birth',
                'rules' => 'trim|required'
                )
     );
    
    function __construct() {
        parent::__construct();
    }
    
    public function is_new(){
        
        $candidate_detail = new stdClass();
        $candidate_detail->zoneid = '';
        $candidate_detail->lgaid = '';
        $candidate_detail->schoolid = '';
        $candidate_detail->centreid = '';
        $candidate_detail->firstname = '';
        $candidate_detail->othernames = '';
        $candidate_detail->dob = '2004-01-01';//date('Y-m-d');
        $candidate_detail->phone = '';
        $candidate_detail->candidateid = '';
         $candidate_detail->firstchoice = '';
        $candidate_detail->secondchoice = '';
        $candidate_detail->gender = 'M';
        
        return $candidate_detail;
    }
    
    public function get_lga($lgaid) {
        $lga_data = $this->db->get_where('t_lgas', array('lgaid'=>$lgaid))->row();
        if(count($lga_data)) return $lga_data->lganame;
        
        return '--';
    }
    
    public function get_zone($zoneid) {
        $zone_data = $this->db->get_where('t_zones', array('zoneid'=>$zoneid))->row();
        if(count($zone_data)) return $zone_data->zonename;
        
        return '--';
    }
    
    public function get_school($schoolid) {
        $school_data = $this->db->get_where('t_schools', array('schoolid'=>$schoolid))->row();
        if(count($school_data))return $school_data->schoolname;
        return '';
    }
    
     public function get_subject($subjectid) {
        $subject_data = $this->db->get_where('t_subjects', array('subjectid'=>$subjectid))->row();
        if(count($subject_data))return $subject_data->subjectname;
        return '';
    }
    
    public function get_sub_subject($subjectid, $single = null) {
        if($single){
            $subject_data = $this->db->get_where('t_subjects_arms', array('armid'=>$subjectid))->row();
        }
        else{
            $subject_data = $this->db->get_where('t_subjects_arms', array('subjectid'=>$subjectid))->result();
        }
        return $subject_data;
    }
    
    public function getExamName_From_Id($examid) {
        $result = $this->db->get_where('t_exams', array('examid'=>$examid))->row();
        if(count($result)){
            return $result->examname;
        }
        return '';
    }
    
    public function getEdcName_From_Id($edcid) {
        $result = $this->db->get_where('t_edcs', array('edcid'=>$edcid))->row();
        if(count($result)){
            return $result->edcname;
        }
        return '';
    }
    
    public function getCloseDate_Id($examid) {
        $result = $this->db->get_where('t_deadlines', array('examid'=>$examid))->row();
        return $result;
    }
    
        
    public function get_pin($candidateid) {
        $pinInfo = $this->db->get_where('t_pins',array('candidateid'=>$candidateid))->row();
        if(count($pinInfo)) {
            return $pinInfo->pin;
        }
        return '';
        
    }
    public function get_form_number($pin) {
        $formNumberInfo = $this->db->get_where('t_form_numbers',array('pin'=>$pin))->row();
        if(count($formNumberInfo)) {
            return $formNumberInfo->formnumber;
        }
        return '';
        
    }
}
        


