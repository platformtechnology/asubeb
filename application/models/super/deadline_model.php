<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Deadline_Model extends MY_Model{
    
    protected $_table_name = 't_deadlines';
    protected $_primary_key = 'id';
    protected $_order_by = 't_deadlines.id desc';
    
    public $_rules = array(
        'exam' => array(
                'field' => 'examid',
                'label' => 'Exam',
                'rules' => 'trim|required'
                ),
        'closedate' => array(
                'field' => 'closedate',
                'label' => 'Deadline Date',
                'rules' => 'trim|required'
                )
     );
    
    function __construct() {
        parent::__construct();
    }
   
    public function is_new(){
        
      $detail = new stdClass();
      $detail->examid = '';
      $detail->closedate = date('d-m-Y');
     
      return $detail;
    }
    
}
