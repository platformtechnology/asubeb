<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Activation_Model extends MY_Model{
    
    protected $_table_name = 't_activation';
    protected $_primary_key = 'trial_id';
    protected $_order_by = 't_activation.id desc';
            
    function __construct() {
        parent::__construct();
    }
   
}
