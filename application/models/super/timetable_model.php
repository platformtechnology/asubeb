<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Timetable_Model extends MY_Model{

    protected $_table_name = 't_edc_timetable';
    protected $_primary_key = 'timetable_id';
    protected $_order_by = 't_edc_timetable.id desc';
    public $_rules = array(
        'timetable_title' => array(
                'field' => 'timetable_title',
                'label' => 'TImetable Title',
                'rules' => 'trim|required'
                ),
        'timetable_content' => array(
                'field' => 'timetable_content',
                'label' => 'Timetable Content',
                'rules' => 'trim|required'
                )
        );

    function __construct() {
        parent::__construct();
    }

    public function is_new(){

        $detail = new stdClass();
        $detail->timetable_title = '';
        $detail->timetable_content = '';
        $detail->examyear = '';

        return $detail;
    }
}
