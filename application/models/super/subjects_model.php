<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Subjects_Model extends MY_Model{
    
    protected $_table_name = 't_subjects';
    protected $_primary_key = 'subjectid';
    protected $_order_by = 't_subjects.id desc';
    
    public $_rules = array(
        'subject' => array(
                'field' => 'subjectname',
                'label' => 'Subject Name',
                'rules' => 'trim|required'
                )
     );
        
    function __construct() {
        parent::__construct();
    }
    
    public function is_new(){
        
      $edc_detail = new stdClass();
      $edc_detail->subjectname = '';
      $edc_detail->examid = '';
      $edc_detail->subjectid = null;
      $edc_detail->iscompulsory = '';
      $edc_detail->haspractical = '';
      $edc_detail->priority = 1;
      $edc_detail->examyear = date('Y');

      return $edc_detail;
    }
    
    public function getExamName_From_Id($examid) {
        $result = $this->db->get_where('t_exams', array('examid'=>$examid))->row();
        if(count($result)){
            return $result->examname;
        }
        return '';
    }
}
