<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of registration_model
 *
 * @author Maxwell
 */
class Scores_Margin_Model extends MY_Model{
    
    protected $_table_name = 't_scores_margin';
    protected $_primary_key = 'id';
    protected $_order_by = 't_scores_margin.id desc';
    
       
    function __construct() {
        parent::__construct();
    }
    
    public function getMaxScores($subjectid){
        
        $scoredata = $this->get_where(array('subjectid'=>$subjectid), true);
        if(count($scoredata)){
            return $scoredata;
        }
        else{
            $this->save_update(array('subjectid'=>$subjectid, 'edcid'=>$this->data['edc_detail']->edcid), null, $this->data['edc_detail']->edcid);
            $this->getMaxScores($subjectid);
        }
        return null;
    }
}
