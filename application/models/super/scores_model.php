<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Scores_Model extends MY_Model {

    protected $_table_name = 't_scores';
    protected $_primary_key = 'scoreid';
    protected $_order_by = 't_scores.id desc';

    function __construct() {
        parent::__construct();
    }

    public function get_candidate_result_xml($examid, $examyear, $edcid, $examno = false) {

        $examdetail = $this->db->where('examid', $examid)->get('t_exams')->row();
        $subjectdetail = $this->db
                        ->where('examid', $examid)
                        ->where('edcid', $edcid)
                        ->get('t_subjects')->result();

        $cutoff = 0;
        if ($examdetail->hasposting) {
            $cutoff = $this->db
                            ->get_where('t_cutoff', array('examid' => $examid, 'examyear' => $examyear))
                            ->row()
                    ->cutoff;
            if (empty($cutoff)) {
                $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET CUTOFF MARK FIRST');
                redirect(site_url('super/sms/save/' . $edcid));
            }
        }

        #get standardise sql
        $sum_param = get_standardscore_rep($examid, $examyear);
        $sql = "select t_candidates.candidateid, examno, firstname, (firstname || ' ' || othernames) as fullname, 
                phone, t_subjects.subjectname, sum(" . $sum_param . ") as totalscore
                from t_candidates 
                inner join t_scores on t_candidates.candidateid = t_scores.candidateid 
                inner join t_subjects on t_subjects.subjectid = t_scores.subjectid
                where t_candidates.examid = '" . $examid . "'
                and t_candidates.examyear = '" . $examyear . "'
                and t_candidates.edcid = '" . $edcid . "' ";
        if($examno) $sql .= "and t_candidates.examno = '" . $examno . "' ";
        $sql .= "group by t_candidates.candidateid, examno, firstname, othernames, phone, t_subjects.subjectname
                order by t_candidates.examno";

        //echo $sql; exit;
        $query = $this->db->query($sql);

        $data_array = array();
        $subj_count = count($subjectdetail);

        if ($query->num_rows()) {

            $query = $query->result();
            foreach ($query as $value) {
                $indexName = str_replace('/', '', $value->examno);
                $data_array[$indexName] = array(
                    'candidateid' => $value->candidateid,
                    'examno' => $value->examno,
                    'fullname' => $value->fullname,
                    'phone' => $value->phone
                );
            }

            $sum = 0;
            $count = 1;
            $status = 'FAILED';
            foreach ($query as $value) {
                $sum += $value->totalscore;
                $indexName = str_replace('/', '', $value->examno);
                $subjectname = substr($value->subjectname, 0, 4);

                $data_array[$indexName][$subjectname] = $value->totalscore;

                if (($count % $subj_count) == 0) {

                    $data_array[$indexName]['total'] = $sum;

                    #check status
                    if ($examdetail->hasposting) {
                        $status = ($sum >= $cutoff ? 'PASSED' : 'FAILED');
                    } else
                        $status = $this->_get_status($value->candidateid, $examid, $examyear, $edcid);

                    $data_array[$indexName]['status'] = $status;
                    $sum = 0;
                }
                $count++;
            }
        }

        if($examno) return $data_array;
            
        $score_array = array();
        foreach ($data_array as $value) {
            $score_array[] = array(
                'data' => $value
            );
        }
        
		#$score_array = array_slice($score_array, 0, 10);
		
        $xml = new SimpleXMLElement('<root/>');
        $this->_array_to_xml($score_array, $xml);

        return $xml->asXML();
        
    }

   public function _array_to_xml($array, &$xml_object) {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml_object->addChild("$key");
                    $this->_array_to_xml($value, $subnode);
                } else {
                    //$subnode = $xml_object->addChild("item$key");
                    $this->_array_to_xml($value, $xml_object);
                }
            } else {
                $xml_object->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }

    public function _get_status($candidateid, $examid, $examyear, $edcid) {

        //Condition 1: if student did not register any of the compulsory subject - he failed
        $sql = "select * from t_registered_subjects 
                    where candidateid = '" . $candidateid . "' and examid = '" . $examid . "' and examyear = '" . $examyear . "'
                    and subjectid in (select subjectid from t_criteria_compulsory where examid = '" . $examid . "' and examyear = '" . $examyear . "') ";
        $compulsory_subjects_registered = $this->db->query($sql)->result();
        $compulsory_subjects_to_pass = $this->db->query("select * from t_criteria_compulsory where examid = '" . $examid . "' and examyear = '" . $examyear . "'")->result();

        if (!count($compulsory_subjects_to_pass)) {
            $this->session->set_flashdata('error', 'YOU ARE REQUIRED TO SET <strong>EXAMINATION RESULT CRITERIAS</strong> FIRST');
            redirect(site_url('super/sms/save/' . $edcid));
        }

        if (count($compulsory_subjects_registered) != count($compulsory_subjects_to_pass)) {
            return 'FAILED';
        }

        //Condition 2: foreach of the compulsory subjects 
        //if student did not reach any of their passscore - he failed
        $sql = "select * from t_scores where candidateid = '" . $candidateid . "' 
                    and examid = '" . $examid . "' 
                    and examyear = '" . $examyear . "' 
                    and edcid = '" . $this->edcid . "'";
        $score_data = $this->db->query($sql)->result();
        foreach ($compulsory_subjects_to_pass as $subjects) {
            foreach ($score_data as $sdata) {
                if ($subjects->subjectid = $sdata->subjectid) {
                    if ($sdata->total_score < $subjects->passscore) {
                        return 'FAILED';
                        break 2;
                    }
                }
            }
        }

        //Condition 3: passing the arbitrary pass criteria

        $sql = "
                select * from t_scores where subjectid not in 
                (select subjectid from t_criteria_compulsory where examid = '" . $examid . "' and examyear = '" . $examyear . "')
                and candidateid = '" . $candidateid . "' and total_score > (select arbitrarypassscore from t_criteria_compulsory where examid = '" . $examid . "' and examyear = '" . $examyear . "' limit 1)    
                ";
        $arbitrary_subjects_passed = $this->db->query($sql)->result();

        if (count($arbitrary_subjects_passed) < ($compulsory_subjects_to_pass[0]->arbitrarynum)) {
            return 'FAILED';
        }

        return "PASSED";
    }

}
