<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Registrant_Model extends MY_Model{
    
    protected $_table_name = 't_candidates';
    protected $_primary_key = 'candidateid';
    protected $_order_by = 't_candidates.examno asc';
            
    function __construct() {
        parent::__construct();
    }
    
    public function getExamName_From_Id($examid) {
        $result = $this->db->get_where('t_exams', array('examid'=>$examid))->row();
        if(count($result)){
            return $result->examname;
        }
        return '';
    }
    
     public function getEdcName_From_Id($edcid) {
        $result = $this->db->get_where('t_edcs', array('edcid'=>$edcid))->row();
        if(count($result)){
            return $result->edcname;
        }
        return '';
    }
    
    public function get_lga($lgaid) {
        $lga_data = $this->db->get_where('t_lgas', array('lgaid'=>$lgaid))->row();
        return $lga_data->lganame;
    }
    
    public function get_school($schoolid) {
        $school_data = $this->db->get_where('t_schools', array('schoolid'=>$schoolid))->row();
        return $school_data->schoolname;
    }
    
     public function get_subject($subjectid) {
        $subject_data = $this->db->get_where('t_subjects', array('subjectid'=>$subjectid))->row();
        return $subject_data->subjectname;
    }
}
