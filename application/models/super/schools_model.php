<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Schools_model extends MY_Model{
    
    protected $_table_name = 't_schools';
    protected $_primary_key = 'schoolid';
     protected $_order_by = 't_schools.schoolname asc';
    
    public $_rules = array(
        'school' => array(
                'field' => 'schoolname',
                'label' => 'School Name',
                'rules' => 'trim|required'
                ),
        'code' => array(
                'field' => 'schoolcode',
                'label' => 'School Code',
                'rules' => 'trim|required'
                ),
        
//        'lga' => array(
//                'field' => 'lgaid',
//                'label' => 'LGA',
//                'rules' => 'trim|required'
//                )
        );
    
    function __construct() {
        parent::__construct();
    }
    
    public function is_new() {
        $detail = new stdClass();
        $detail->schoolname = '';
        $detail->schoolcode = '';
        $detail->lgaid = '';
        $detail->zoneid = '';
        $detail->iscentre = 1;
        $detail->ispostingschool = 0;
       
        return $detail;
    }
    
    public function get_lgas_from_zone($zoneid){
        $this->db->select('lgaid, lganame');
        $this->db->where('zoneid', $zoneid);
        $this->db->order_by('lganame');
        $query = $this->db->get('t_lgas');
        
        if ($query->num_rows() > 0)
        {
          return $query->result_array();
        }else{
          return array();
        }
    }

    public function get_zone_name($zoneid){
        $result = $this->db->get_where('t_zones', array('zoneid'=>$zoneid))->row();
        if(count($result)){
            return $result->zonename;
        }
        return '--';
    }
    
    public function get_lga_name($lgaid){
        $result = $this->db->get_where('t_lgas', array('lgaid'=>$lgaid))->row();
        if(count($result)){
            return $result->lganame;
        }
        return '--';
    }
}
