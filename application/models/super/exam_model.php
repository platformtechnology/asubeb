<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Exam_Model extends MY_Model{
    
    protected $_table_name = 't_exams';
    protected $_primary_key = 'examid';
    protected $_order_by = 't_exams.id desc';
    
    public $_rules = array(
        'name' => array(
                'field' => 'examname',
                'label' => 'Exam Name',
                'rules' => 'trim|required'
                ),
//        'edc' => array(
//                'field' => 'edcid',
//                'label' => 'EDC',
//                'rules' => 'trim|required'
//            )
     );
    
    function __construct() {
        parent::__construct();
    }
   
    public function is_new(){
        
      $detail = new stdClass();
      $detail->examname = '';
      $detail->examdesc = '';
      $detail->edcid = '';
      $detail->hasposting = 0;
      $detail->hassecschool = 0;
      $detail->hasca = 1;
      $detail->haspractical = 1;
      $detail->haszone = 0;
      $detail->haslga = 1;
      
      return $detail;
    }
    
     public function get_edc_from_exam($examid){
        
        $examdetail = $this->get_all($examid);
        if(count($examdetail)){
            $edcdetail = $this->db
                    ->select('edcname')
                    ->where('edcid', $examdetail->edcid)
                    ->get('t_edcs')
                    ->row();
            
            if(count($edcdetail)){
                return $edcdetail->edcname;
            }
        }
        
        return '';
    }
}
