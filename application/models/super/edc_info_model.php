<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Edc_info_Model extends MY_Model{
    
    protected $_table_name = 't_edc_info';
    protected $_primary_key = 'edcid';
    protected $_order_by = 't_edc_info.id desc';
    public $_rules = array(
        'home' => array(
                'field' => 'homeinfo',
                'label' => 'Home',
                'rules' => 'trim|required'
                ),
        'about' => array(
                'field' => 'aboutinfo',
                'label' => 'About',
                'rules' => 'trim|required'
                )
        );
                    
    function __construct() {
        parent::__construct();
    }
    
    
   
}
