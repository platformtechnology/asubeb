<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Activeyear_model extends MY_Model{
    
    protected $_table_name = 't_activeyear';
    protected $_primary_key = 'id';
    
    public $_rules = array(
        'activeyear' => array(
                'field' => 'activeyear',
                'label' => 'Active Year',
                'rules' => 'trim|required|is_natural_no_zero|max_length[4]'
                )
        );
    
    function __construct() {
        parent::__construct();
    }
    
    public function is_new() {
        $detail = new stdClass();
        $detail->activeyear = date('Y');
        $detail->closedate = date('d-m-Y');
        $detail->id = NULL;
        return $detail;
    }
    
}
