<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Edc_Model extends MY_Model{
    
    protected $_table_name = 't_edcs';
    protected $_primary_key = 'edcid';
    protected $_order_by = 't_edcs.id desc';
    
    public $_rules = array(
        'name' => array(
                'field' => 'edcname',
                'label' => 'EDC Names',
                'rules' => 'trim|required'
                ),
        'url' => array(
                'field' => 'edcweburl',
                'label' => 'EDC Web Url',
                'rules' => 'trim|required'
               ),
        'phone' => array(
                'field' => 'edcphone',
                'label' => 'EDC Contact Number',
                'rules' => 'trim|required'
                ),
        'address' => array(
                'field' => 'edcaddress',
                'label' => 'Address',
                'rules' => 'trim|required'
                ),
        'admin_email' => array(
                'field' => 'email',
                'label' => 'Admin Email',
                'rules' => 'trim|required|valid_email'
                ),
        'admin_name' => array(
            'field' => 'fullname',
            'label' => 'Admin Name',
            'rules' => 'trim|required'
            ),
        'admin_phone' => array(
            'field' => 'phone',
            'label' => 'Admin Phone',
            'rules' => 'trim|required'
            )
     );
        
    function __construct() {
        parent::__construct();
    }
    
    public function is_new(){
        
      $edc_detail = new stdClass();
      $edc_detail->edcname = '';
      $edc_detail->edcaddress = '';
      $edc_detail->edcphone = '';
      $edc_detail->edcweburl = '';
      
      $edc_detail->email = '';
      $edc_detail->phone = '';
      $edc_detail->fullname = '';
      $edc_detail->edcid = '';
      $edc_detail->themecolor = '#0081c2';
       
      return $edc_detail;
    }
}
