<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Lgas_model extends MY_Model{
    
    protected $_table_name = 't_lgas';
    protected $_primary_key = 'lgaid';
    protected $_order_by = 't_lgas.id desc';
    
    public $_rules = array(
        'lga' => array(
                'field' => 'lganame',
                'label' => 'LGA',
                'rules' => 'trim|required'
                ),
         'initials' => array(
                'field' => 'lgainitials',
                'label' => 'Initials',
                'rules' => 'trim|required'
                ),
        'zone' => array(
                'field' => 'zoneid',
                'label' => 'Zone',
                'rules' => 'trim|required'
                )
        );
    
    function __construct() {
        parent::__construct();
    }
    
    public function is_new() {
        $detail = new stdClass();
        $detail->zoneid = '';
        $detail->lganame = '';
        $detail->lgainitials = '';
               
        return $detail;
    }

}
