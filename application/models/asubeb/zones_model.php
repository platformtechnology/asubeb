<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Maxwell
 */
class Zones_model extends MY_Model{

    protected $_table_name = 't_zones';
    protected $_primary_key = 'zoneid';

    public $_rules = array(
        'zone' => array(
                'field' => 'zonename',
                'label' => 'Zone',
                'rules' => 'trim|required'
                )
        );

    function __construct() {
        parent::__construct();
    }

    public function is_new() {
        $detail = new stdClass();
        $detail->zonename = '';

        return $detail;
    }

    public function getZoneName($zoneid){
        $where = array('zoneid'=>$zoneid);
        $data = $this->get_where($where, $single = TRUE);
        if(count($data)){
            return $data->zonename;
        }
    }

}
