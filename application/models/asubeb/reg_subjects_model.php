<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of registration_model
 *
 * @author Maxwell
 */
class Reg_Subjects_Model extends MY_Model{
    
    protected $_table_name = 't_registered_subjects';
    protected $_primary_key = 'id';
    protected $_order_by = 't_registered_subjects.id desc';
    
       
    function __construct() {
        parent::__construct();
    }
    
    public function get_registered_subjects($candidateid, $examid, $examyear){
        $this->db->where('edcid', $this->data['edc_detail']->edcid);
        $registeredsubjects = $this->get_where(array('candidateid'=>$candidateid, 'examid'=>$examid, 'examyear'=>$examyear));
        return $registeredsubjects;
    }
}
