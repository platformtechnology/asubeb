<?php

class MY_Controller extends CI_Controller{
    
    public $data = array();
    public $edcid = '';
    
    function __construct() {
        parent::__construct();
        
        $this->data['site_name'] = config_item('site_name');
        
        $url = current_url();
        $current_url = str_replace(uri_string() . config_item('url_suffix'), '', $url);
        
        $this->data['current_url'] = $cur_url = $current_url = rtrim($current_url, '/');
        $this->data['current_url'] = ((trim(config_item('index_page')) != false) ? str_replace(config_item('index_page'), '', $cur_url) : $cur_url);
        $this->data['current_url'] = $current_url = str_replace('www.', '', $this->data['current_url']);
        $this->data['current_url'] = rtrim($current_url, '/');
                        
        #GLOBAL VARIABLES FOR PAGE LEVEL STYLES AND SCRIPTS
        $this->data['page_level_styles'] = "";
        $this->data['page_level_scripts'] = "";

        #ACTIVE CALENDAR
        $activeyear_data = $this->db->get('t_activeyear')->row();
        
        if(count($activeyear_data)) $this->data['activeyear'] = $activeyear_data->activeyear; 
        else$this->data['activeyear'] = date('Y'); 
        
        $this->data['startyear'] = '2015';
        /*
        $this->load->library('migration');
                if ($this->migration->current() === FALSE)
                {
                        show_error($this->migration->error_string());
                }
         * *
         */     
    }
    
    public function analyse_url($is_superadmin = false) {

        $this->db->where('edcweburl', $this->data['current_url']);
        $query = $this->db->get('t_edcs');

        #if no edc has the url go to super admin
        if ($query->num_rows()){
            if($is_superadmin){
               redirect(site_url('home'));
            }
            return $query->row();
        }
        else{
            
            #little hack
            if($this->data['current_url'] == 'http://edc.platformtechltd.com')  redirect(site_url('super/login'));
                
            if(!$is_superadmin){
                //redirect(site_url('super/login'));
                show_error("THE DETECTED URL HAS NOT BEEN CONFIGURED YET! - KINDLY CONTACT THE EDEVPRO TEAM FOR MORE INFO!");
                return;
            }
        }
        
        
    }
}

