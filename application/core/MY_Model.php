<?php
class MY_Model extends CI_Model {

    protected $_table_name = '';
    protected $_primary_key = '';
    protected $_order_by = 'id DESC';
    public $_rules = array();

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->set_message('is_unique', '%s already exists - The record must be unique!');
    }

    public function get_all($id = NULL, $single = FALSE) {

        if ($id != NULL) {
            $this->db->where($this->_primary_key, $id);
            $method = 'row';
        } else if ($single == TRUE) {
            $method = 'row';
        } else {
            $method = 'result';
        }

        if (!count($this->db->ar_orderby)) {
            $this->db->order_by($this->_order_by);
        }
        $query = $this->db->get($this->_table_name)->$method();


        return $query;
    }

    public function get_join($join_table_name_and_key, $join_table_fields, $single = FALSE) {
        $data_fields = '';

        if (is_array($join_table_fields)) {
            foreach ($join_table_fields as $fields) {
                $data_fields .= ',' . $fields;
            }
            $data_fields = substr_replace($data_fields, '', 0, 1);
        } else
            $data_fields = $join_table_fields;

        //remove the begining comma from the table_fields text


        $this->db->select($this->_table_name . '.*, ' . $data_fields);

        foreach ($join_table_name_and_key as $join_table_name => $join_table_key) {

            $this->db->join($join_table_name, $join_table_name . '.' . $join_table_key . ' = ' . $this->_table_name . '.' . $join_table_key);
        }

        return $this->get_all(NULL, $single);
    }

    public function get_where($where, $single = FALSE) {
        $this->db->where($where);
        return $this->get_all(NULL, $single);
    }

    public function save_update($data, $id = NULL, $edcid = NULL) {

        $insert_id = 0;
        $now = 'now()'; //'UTC_TIMESTAMP()';
        $id || $this->db->set('datecreated', $now, false);
        $this->db->set('datemodified', $now, false);

        // Insert
        if ($id === NULL) {
            $this->db->set($data);
            $insert_id = $this->db->insert($this->_table_name);

            //$this->logSynchFile($edcid);

            //$insert_id = $this->db->insert_id();
        }
        // Update
        else {
            $this->db->set($data);
            $this->db->where($this->_primary_key, $id);
            $insert_id = $this->db->update($this->_table_name);

            $this->logSynchFile($edcid);
        }

        return $insert_id;
    }

    public function delete($id, $edcid = NULL) {

        if (!$id) {
            return FALSE;
        }
        $this->db->where($this->_primary_key, $id);
        $success = $this->db->delete($this->_table_name);
        $this->logSynchFile($edcid);

        return $success;
    }

    public function array_from_post($fields) {

        $data = array();
        foreach ($fields as $field) {
            $data[$field] = $this->input->post($field);
        }
        return $data;
    }

    public function generate_unique_id($numeric = false, $len = 10) {

        if ($numeric) {
            $source = ""
                    . "012345678901234567890123456789"
                    . "012345678901234567890123456789"
                    . "012345678901234567890123456789";
            $range = strlen($source);
            $output = '';
            for ($i = 0; $i < $len; $i++) {
                $output .= substr($source, rand(0, $range - 1), 1);
            }

            while (count($this->get_where(array($this->_primary_key => $output), true)) > 0) {
                $this->generate_unique_id($numeric, $len);
            }

            return $output;
        }

        $mstime = str_replace(".", "", microtime());
        $times = explode(" ", $mstime);
        $id = str_replace(".", "", uniqid("", true)) . $times[0];
        return $id;
    }

    public function logSynchFile($edcid) {

        $exception_tables = array(
            't_audittrail', 't_pins', 't_support', 't_support_comment',
            't_edc_news', 't_edc_info', 't_activation', 't_pins',
            't_synchlog', 't_synchdetails');

        if ($edcid == null)
            $edcid = $this->_get_edcid_from_url();
        if (!in_array($this->_table_name, $exception_tables) && $edcid != NULL) {
            logSql($this->db->last_query(), $edcid);
        }
    }

    public function _get_edcid_from_url() {
        $url = current_url();
        $current_url = str_replace(uri_string() . config_item('url_suffix'), '', $url);
        $current_url = rtrim($current_url, '/');
        $current_url = ((trim(config_item('index_page')) != false) ? str_replace(config_item('index_page'), '', $current_url) : $current_url);
        $current_url = str_replace('www.', '', $current_url);
        $current_url = rtrim($current_url, '/');

        $dbhost = $this->db->hostname;
        $dbuser = $this->db->username;
        $dbpass = $this->db->password;
        $dbname = $this->db->database;

        $connection_string = "host=" . $dbhost . " user=" . $dbuser . " password=" . $dbpass . " dbname=" . $dbname;
        if (($db_connection = pg_connect($connection_string))) {
            $sql = "select edcid from t_edcs where edcweburl = '" . $current_url . "'";
            $rset = pg_query($db_connection, $sql);
            if ((pg_num_rows($rset) > 0)) {
                $edc_id = pg_fetch_result($rset, 0, 0);
                return $edc_id;
            }

            pg_close($db_connection);
        }


        return null;
    }

}
