<?php
    
    $CI =& get_instance();
    
    function export_to_excel($resultSetobject, $filename) {
       header('Content-type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename='.$filename.'.xls');

        //Filter all keys, they'll be table headers
        $h = array();
        
        foreach($resultSetobject as $row){
            $row = get_object_vars($row); //Convert the Object to 2-dim Array
            
            foreach($row as $key=>$val){
                if($key == 'firstname' || $key == 'othernames'){
                     if(!in_array('CandidateName', $h)){
                        $h[] = 'CandidateName';   
                    }
                }
                else if($key == 'examid'){
                     if(!in_array('RegisteredExam', $h)){
                        $h[] = 'RegisteredExam ';   
                    }
                }
                else if($key == 'schoolid'){
                     if(!in_array('School', $h)){
                        $h[] = 'School';   
                    }
                }
                else if($key == 'examno' || $key == 'gender' || $key == 'dob' || $key == 'examyear' || $key == 'phone' || $key == 'firstchoice' || $key == 'secondchoice'){
                    if(!in_array(ucfirst($key), $h)){
                        $h[] = ucfirst($key);   
                    }
                }
            }
        }
               
        $h = sort_headers($h);
        
        //print_r($h);exit;
        
        //echo the entire table headers
        echo '<table border="1"><tr>';
        foreach($h as $value) {
            $value = ucwords($value);
            echo '<th>'.$value.'</th>';
        }
        echo '</tr>';

        foreach($resultSetobject as $row){
            echo '<tr>';
            $row = get_object_vars($row);//Convert the Object to 2-dim Array
            $data = array();
            
            foreach($row as $key=>$val){
              if($key == 'examid'){
                    $data[] = $val;
                }
                else if($key == 'schoolid'){
                    $data[] = $val;
                }
                else if($key == 'examno' || $key == 'gender' || $key == 'dob' || $key == 'examyear' || $key == 'phone' || $key == 'firstname' || $key == 'othernames' || $key == 'firstchoice' || $key == 'secondchoice'){
                    $data[] = $val;
                }
            }
           
            $data = sort_values($data);
             
            foreach ($data as $value) {
                writeRow($value);
            }
        }
        echo '</tr>';
        echo '</table>';                                    
                  
    }
    
    function sort_headers($headers){
        $data = array();
        $data['examno'] = $headers[2];
        $data['name'] = $headers[3];
        $data['gender'] = $headers[4];
        $data['dob'] = $headers[5];
        $data['phone'] = $headers[6];
        $data['examname'] = $headers[0];
        $data['examyear'] = $headers[1];
        $data['school'] = $headers[7];
        $data['firstchoice'] = $headers[8];
        $data['secondchoice'] = $headers[9];
        
        return $data;
    }
    
    function sort_values($values){
        $data = array();
        $data['examno'] = $values[2];
        $data['name'] = ucfirst($values[3]).' '.ucfirst($values[4]);
        $data['gender'] = $values[5];
        $data['dob'] = $values[6];
        $data['phone'] = $values[7];
        $data['examname'] = getExamName_From_Id($values[0]);
        $data['examyear'] = $values[1];
        $data['school'] = getSchoolName_From_Id($values[8]);
        $data['firstchoice'] = (($values[9] == 0) ? '' : getSchoolName_From_Id($values[9]));
        $data['secondchoice'] = (($values[10] == 0) ? '' : getSchoolName_From_Id($values[10]));
        
        return $data;
    }
    
    function writeRow($val) {
        echo '<td>'.utf8_decode($val).'</td>';              
    }
   
    function getExamName_From_Id($examid) {
        global $CI;
        
        $result = $CI->db->get_where('t_exams', array('examid'=>$examid))->row();
        if(count($result)){
            return $result->examname;
        }
        return '';
       // show_error("Internal Error Occurred While Getting Examname From ID");
    }
    
    function getSchoolName_From_Id($schoolid) {
        global $CI;
        
        $school_data = $CI->db->get_where('t_schools', array('schoolid'=>$schoolid))->row();
         if(count($school_data)) return $school_data->schoolname;
         return '';
    }

