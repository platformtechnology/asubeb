<?php

function get_edc_logo($edclogo, $edcname, $web = false) {

    $str = '<div style="float: left; margin-right: 10px;">
                      <a href="#" title="Examination Development Centre"><img src="' . $edclogo . '" alt="Logo" /></a>
                  </div>
            ';
    if ($web)
        $str .= '<span style="color: #F5F5F5; font-weight: bold;">' . strtoupper($edcname) . '</span>';
    else
        $str.= '<h4 style="font-weight: bold;">' . strtoupper($edcname) . '</h4>';

    $str .= '<br/>';
    return $str;
}

function get_status($status) {
    switch ($status) {
        case "Pending":
            return '<label class="label label-danger">Pending</label>';
        case "Processing":
            return '<label class="label label-warning">Processing</label>';
        case "Resolved":
            return '<label class="label label-info">Resolved</label>';
    }
}

function get_individual_standard($score, $standard) {

    if ($standard > 0)
        return round(($score * $standard) / 100);

    if ($standard < 0) return 0;

    return $score;
}

function get_standardscore_rep($examid, $examyear) {
    $CI = & get_instance();
    $edcid = $CI->edcid;

    $scoredata = $CI->db->get_where('t_standard_scores', array('examid' => $examid, 'examyear' => $examyear, 'edcid' => $edcid))->row();

    if (!count($scoredata)) {
        return 'total_score';
    } else {
        $examdata = $CI->db->get_where('t_exams', array('examid' => $examid))->row();
        count($examdata) || show_404();


        #if the exam does not support ca and practical
        if (!$examdata->hasca && !$examdata->haspractical) {

            #if the standard exam is 0 use total score instead
            if ($scoredata->standardexam > 0) {
                return 'round((cast(t_scores.exam_score as float) * ' . $scoredata->standardexam . ') / 100 )';
            } else
                return 'total_score';
        }

        #if the exam support ca but not practical
        if ($examdata->hasca && !$examdata->haspractical) {
            if ($scoredata->standardexam > 0 && $scoredata->standardca > 0) {
                return 'round((cast(t_scores.exam_score as float) * ' . $scoredata->standardexam . ') / 100 )'
                        . '+'
                        . 'round((cast(t_scores.ca_score as float) * ' . $scoredata->standardca . ') / 100 )';
            } else
                return 'total_score';
        }

        #if the exam support practical but not ca
        if (!$examdata->hasca && $examdata->haspractical) {
            if ($scoredata->standardexam > 0 && $scoredata->standardpractical > 0) {
                return 'round((cast(t_scores.exam_score as float) * ' . $scoredata->standardexam . ') / 100 )'
                        . '+'
                        . 'round((cast(t_scores.practical_score as float) * ' . $scoredata->standardpractical . ') / 100 )';
            } else
                return 'total_score';
        }

        #if the exam support practical and ca
        if ($examdata->hasca && $examdata->haspractical) {
            if ($scoredata->standardexam > 0  && $scoredata->standardca > 0) {
                return 'round((cast(t_scores.exam_score as float) * ' . $scoredata->standardexam . ') / 100 )'
                        . '+'
                        . 'round((cast(t_scores.practical_score as float) * ' . $scoredata->standardpractical . ') / 100 )'
                        . '+'
                        . 'round((cast(t_scores.ca_score as float) * ' . $scoredata->standardca . ') / 100 )';
            } else
                return 'total_score';
        }

        return 'total_score';
    }
}

function generateId($len = 10) {

    $source = "abcdefghijklm0123456789nopqrstuvwxyz0123456789abcdefghijklmn0123456789opqrstuvwxyz0123456789";
    $range = strlen($source);
    $output = '';
    for ($i = 0; $i < $len; $i++) {
        $output .= substr($source, rand(0, $range - 1), 1);
    }
    return $output;
}

function get_error($msg) {
    return '<div class="alert alert-danger alert-dismissable">' . $msg . ' <span class = "glyphicon glyphicon-remove" data-dismiss = "alert"> </span></div>';
}

function get_activation_prompt() {
    return '<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger">
            <i class="glyphicon glyphicon-remove-circle"></i>
            YOU ARE STILL USING A TRIAL VERSION OF THE E-DEVPRO SOFTWARE SUITE
            - KINDLY CONNECT ONLINE AND <a href="' . site_url('dbsync') . '"> ACTIVATE</a>
        </div>
    </div>
</div>';
}

function get_success($msg) {
    return '<div class="alert alert-info alert-dismissable">' . $msg . '<span class = "glyphicon glyphicon-remove" data-dismiss = "alert"> </span></div>';
}

function get_img($imagename, $web = false) {

    if ($web)
        return base_url('resources/web/img/' . $imagename);
    return base_url('resources/images/' . $imagename);
}

function encrypt($string) {
    return hash('sha512', $string . config_item('encryption_key'));
}

function get_del_btn($url) {

    return anchor($url, '<i class="glyphicon glyphicon-remove"></i> ', array(
        'title' => 'Delete this record',
        'onclick' => "return confirm('You are about to delete a record. Action cannot be undone. Are you sure you want to proceed?');"
            )
    );
}

function get_edit_btn($url) {

    return anchor($url, '<i class="glyphicon glyphicon-edit"></i> ', array(
        'title' => 'Edit this record'
            )
    );
}

function str_lreplace($search, $replace, $subject) {
    $pos = strrpos($subject, $search);

    if ($pos !== false) {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }
    return $subject;
}

function logSql($sql, $edcid) {

    $sql = $sql . '; ' . "\n";
    $filepath = './resources/db_sync/' . $edcid . '.edp';

    if (!$fp = @fopen($filepath, FOPEN_WRITE_CREATE)) {
        return FALSE;
    }

    flock($fp, LOCK_EX);
    fwrite($fp, $sql);
    flock($fp, LOCK_UN);
    fclose($fp);

    return true;
}

function parse_edcs_for_display($edcs) {
    $string = '';

    foreach ($edcs as $edc) {
        $string .= '<div class="col-sm-2">';
        $string .= '<div class="content-box-header">';
        $string .= '<div class="panel-title">'
                //$string .= '<div class="panel-options">'
                . '<a href="' . site_url('super/edcs/save/' . $edc->edcid) . '" data-rel="collapse" title="Edit EDC"><i class="glyphicon glyphicon-edit"></i></a> '
                . get_del_btn(site_url('super/edcs/delete/' . $edc->edcid))
                . '<a href="' . site_url('super/exams/save/' . $edc->edcid) . '" data-rel="collapse" title="Setup EDC"><i class="glyphicon glyphicon-cog"></i></a> '
                . '</div>';
        $string .= '</div>';

        $string .= '<div class="content-box-large box-with-header" >';
        $string .= '<a href="' . site_url('super/exams/save/' . $edc->edcid) . '" title="Click To View/Setup Edc Details"> <i class="glyphicon glyphicon-certificate"></i> ' . strtoupper($edc->edcname) . '</a>';
        $string .= '<br/><strong>ID:</strong> <span style="color: crimson;">' . $edc->edcid . '</span>';
        $string .= '</div>';

        $string .= '</div>';
    }

    return $string;
}

function encrypt_decrypt($action, $string) {
    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key = '1234567';
    $secret_iv = '12345678';

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        //$output = openssl_encrypt($string, $encrypt_method, $key);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        //$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key);
    }

    return $output;
}
