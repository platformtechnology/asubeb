<?php
    
    $CI =& get_instance();
    
    function export_to_omr_format($resultSetobject, $filename) {
       header('Content-type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename='.$filename.'.xls');

        //Filter all keys, they'll be table headers
        $h = array();
        
        foreach($resultSetobject as $row){
            $row = get_object_vars($row); //Convert the Object to 2-dim Array
            
            foreach($row as $key=>$val){
                if($key == 'firstname' || $key == 'othernames'){
                     if(!in_array('Name', $h)){
                        $h[] = 'Name';   
                    }
                }
                else if($key == 'examid'){
                     if(!in_array('Exam Type', $h)){
                        $h[] = 'Exam Type';   
                    }
                }
                else if($key == 'schoolid'){
                     if(!in_array('School', $h)){
                        $h[] = 'School';   
                    }
                }
                else if($key == 'examno'){
                    if(!in_array(ucfirst($key), $h)){
                        $h[] = ucfirst($key);   
                    }
                }
            }
        }
               
        //echo the entire table headers
        echo '<table border="1"><tr>';
        echo '<th>LG CODE</th>';
        echo '<th>SCHOOL CODE</th>';
        echo '<th>STUDENT ID</th>';
        echo '<th>NAME</th>';
        echo '<th>SCHOOL</th>';
        echo '<th>SUBJECT TITLE</th>';
        echo '<th>EXAM TYPE</th>';
        echo '</tr>';

        foreach($resultSetobject as $row){
            echo '<tr>';
            $row = get_object_vars($row);//Convert the Object to 2-dim Array
            $data = array();
            
            foreach($row as $key=>$val){
              if($key == 'examid'){
                    $data[] = $val;
                }
                else if($key == 'schoolid'){
                    $data[] = $val;
                }
                else if($key == 'examno' || $key == 'firstname' || $key == 'othernames'){
                    $data[] = $val;
                }
            }
            
            
            $data = sort_values($data);
            
            
            foreach ($data as $value) {
                writeRow($value);
            }
        }
        echo '</tr>';
        echo '</table>';                                    
                  
    }

    function sort_values($values){
        $data = array();
        $examno_split = explode('/', $values[1]);
        $data['lgcode'] = strtoupper($examno_split[0]);
        $data['schoolcode'] = $examno_split[1];
        $data['stid'] = $examno_split[2];
        
        $data['name'] = strtoupper($values[2]).' '.strtoupper($values[3]);
        $data['school'] = getSchoolName_From_Id($values[4]);
        $data['subject'] = '';
        $data['examtype'] = getExamName_From_Id($values[0]);

        
        return $data;
    }
    
    function writeRow($val) {
        echo '<td>'.utf8_decode($val).'</td>';              
    }
   
    function getExamName_From_Id($examid) {
        global $CI;
        
        $result = $CI->db->get_where('t_exams', array('examid'=>$examid))->row();
        if(count($result)){
            return $result->examname;
        }
        show_error("Internal Error Occurred While Getting Examname From ID");
    }
    
    function getSchoolName_From_Id($schoolid) {
        global $CI;
        
        $school_data = $CI->db->get_where('t_schools', array('schoolid'=>$schoolid))->row();
        return $school_data->schoolname;
    }

