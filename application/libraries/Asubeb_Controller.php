<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_Controller
 *
 * @author Maxwell
 */
class Asubeb_Controller extends MY_Controller{

    function __construct() {
        parent::__construct();

        $this->data["edc_detail"] = $edc_data = $this->analyse_url();

		if(!in_array(uri_string(), array('asubeb/login', 'asubeb/login/logout')))
        if(!$this->session->userdata('admin_loggedin')){
            redirect('asubeb/login/logout');
        }

	$priviledges = explode("^", $this->session->userdata('user_role'));
        if(!in_array('Asubeb', $priviledges)){
		redirect('asubeb/login/logout');
        }
        $this->data['user_privilege'] = $this->session->userdata('user_privilege');
        $this->data["edc_logo"] = base_url(str_replace('./', '', config_item('edc_logo_path')).$edc_data->edclogo);
        $this->data['edcname'] = $edc_data->edcname;
        $this->edcid = $edc_data->edcid;

    }



	 public function analyse_url($is_superadmin = false) {

        $this->db->where('edcweburl', $this->data['current_url']);
        $query = $this->db->get('t_edcs');

        #if no edc has the url go to super admin
        if ($query->num_rows()){
            if($is_superadmin){
               redirect(site_url('home'));
            }
            return $query->row();
        }
        else{

            #little hack
            if($this->data['current_url'] == 'http://edc.platformtechltd.com')  redirect(site_url('super/login'));

            if(!$is_superadmin){
                //redirect(site_url('super/login'));
                show_error("THE DETECTED URL HAS NOT BEEN CONFIGURED YET! - KINDLY CONTACT THE EDEVPRO TEAM FOR MORE INFO!");
                return;
            }
        }

    }


}
