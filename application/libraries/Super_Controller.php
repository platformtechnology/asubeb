<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_Controller
 *
 * @author Maxwell
 */
class Super_Controller extends MY_Controller{
    
    function __construct() {
        parent::__construct();
                
        $curr_url = $this->uri->segment(2);
        $exception_uri = array('login', 'updates');
        
        #if url belongs to client bounce the guy
        $this->analyse_url($is_admin = true);
        
        if (!in_array($curr_url, $exception_uri)) {
                        
            if (!$this->session->userdata('super_loggedIn')) {
                redirect(site_url('super/login'));
            }
        } 
        
        $this->data['super_data'] = $this->session->userdata("super_data");
    }
}
