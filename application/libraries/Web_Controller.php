<?php
class Web_Controller extends MY_Controller{

    public $recentitems = 10;

    function __construct() {
        parent::__construct();

        #GET EDC DATA
        $this->data['edc_data'] = $site_data  = $this->analyse_url();
        $this->data['edc_detail'] = $site_data;

        $this->edcid = $site_data->edcid;

        $this->data["edclogo"] = base_url('resources/images/edc_logos/'.$this->edcid.'.png');
        $this->data['edcname'] = $site_data->edcname;

        $exceptionUrl = array(
          'staff/dashboard',
          'staff/candidates',
          'staff/printing',
          'staff/search',
          'staff/view',
          'staff/password',
          'staff/website',
          'staff/filter',
          'staff/savenews',
          'staff/batchprintfilter'
        );

        if (in_array(uri_string(), $exceptionUrl)) {
            if (!$this->session->userdata('staff_loggedin')) {
                    redirect(site_url('web/staff'));
            }
        }

        $exceptionUrl = array(
          'schools/dashboard',
          'schools/candidates',
          'schools/filter',
          'schools/printdetails',
          'schools/view',
          'schools/printcandidate'
        );

        if (in_array(uri_string(), $exceptionUrl)) {
            if (!$this->session->userdata('school_loggedin')) {
                    redirect(site_url('web/schools'));
            }
        }
    }

    public function _getActiveYear(){
        $activeyear= "";
        $this->db->limit(1);
        $this->db->select('activeyear');
        $result = $this->db->get('t_activeyear');

        if (count($result->num_rows())) {
            foreach( $result->result() as $resultdata){
                $activeyear = $resultdata->activeyear;
            }
        }
        return $activeyear;
    }
}
