<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_Controller
 *
 * @author Maxwell
 */
class Admin_Controller extends MY_Controller{

    function __construct() {
        parent::__construct();

        $this->data["edc_detail"] = $edc_data = $this->analyse_url();

        if(!in_array(uri_string(), array('admin/login', 'admin/login/logout')))
        if(!$this->session->userdata('admin_loggedin')){
            redirect('admin/login/logout');
        }

        $this->data["edc_logo"] = base_url(str_replace('./', '', config_item('edc_logo_path')).$edc_data->edclogo);
        $this->data['edcname'] = $edc_data->edcname;
        $this->edcid = $edc_data->edcid;

    }
}
